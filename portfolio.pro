TEMPLATE = subdirs

SUBDIRS += \
    3rdparty \
    qchirp \
    jlee \
    qfractal \
    unittests \
    render \
    examples

jlee.depends = 3rdparty
qchirp.depends = jlee
render.depends = jlee
unittests.depends = jlee
examples.depends = jlee
