#-------------------------------------------------
#
# Project created by QtCreator 2019-09-17T21:24:15
#
#-------------------------------------------------
include(../vars.pri)

TARGET = 3rdparty
TEMPLATE = lib
CONFIG += staticlib
CONFIG -= qt c++11 c++1z c++14 c++17 c++2a

SOURCES += \
    predicates.c

HEADERS += \
    predicates.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
