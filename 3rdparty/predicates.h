/** @brief Robust geometric predicates by Jonathan Shewchuk
 *
 * This is a header file that documents the usage of functions provided by predicates.c.
 *
 * @todo Implement these ourselves. In particular, the Dekker/Veltkamp functions (Two_Sum, etc.) could be leveraged
 * elsewhere. And some improvements have been made to these predicates. See "Adaptive and Efficient Algorithmfor 2D
 * Orientation Problem" by Katsuhisa Ozaki, Takeshi Ogita, Siegfried M.Rump and Shin’ichi Oishi
 * https://projecteuclid.org/download/pdf_1/euclid.jjiam/1265033779
 */
#ifndef PREDICATES_H
#define PREDICATES_H

#ifdef __cplusplus
extern "C"
{
#endif
/** @brief Orientation of C relative to the line defined by AB
 *
 * Mathematically, the orientation of C is given by the determinant:
 *
 * | ax ay 1 |   | ax-cx ay-cy |
 * | bx by 1 | = | bx-cx by-cy |
 * | cx cy 1 |
 *
 * The returned value of this function will be approximately equal to this determinant, but not exact. The sign,
 * however, will be correct. This function employs an adaptive method of computing the value, doing as little work as
 * necessary to determine the sign, since this is usually what we are interested in. Especially for this particular
 * function.
 *
 * @sa orient2dexact
 */
double orient2d(const double* pa, const double* pb, const double* pc);

/** @brief Exactly calculates the 2D orientation determinant
 *
 * Unlike orient2d, does the full determinant calculation using robust arithmetic. The result is exact up to floating
 * point precision.
 */
double orient2dexact(const double* pa, const double* pb, const double* pc);

/** @brief Check if D is inside the circle determined by ABC
 *
 * Mathematically, a point D is in, on, or outside the circle based on the sign of the following determinant:
 *
 * | ax ay a.a 1 |
 * | bx by b.b 1 |
 * | cx cy c.c 1 |
 * | dx dy d.d 1 |
 *
 * The returned value of this function will be approximately equal to this determinant, but not exact. The sign,
 * however, will be correct. This function employs an adaptive method of computing the value, doing as little work as
 * necessary to determine the sign, since this is usually what we are interested in. Especially for this particular
 * function.
 */
double incircle(const double* pa, const double* pb, const double* pc, const double* pd);

/** @brief Orientation of D relative to plane defined by ABC
 *
 * Approximately calculates (a-d).((b-d)x(c-d)). Equivalent to the determinant
 *
 *  | ax ay az 1 |   | ax-dx ay-dy az-dz |
 *  | bx by bz 1 | = | bx-dx by-dy bz-dz |
 *  | cx cy cz 1 |   | cx-dx cy-dy cz-dz |
 *  | dx dy dz 1 |
 *
 * Interpret value as negative, positive, or zero value corresponding to above, below, or on the plane (NB: This is
 * probably counter to what one would expect). Order of the points ABC can flip the sign (i.e., interpreting the output
 * depends on the orientation of the triangle -- clockwise or counterclockwise).
 *
 * In particular, orient3d({1,0,0},{0,1,0},{0,0,1},{0,0,0}) == 1.
 *
 * @todo Specialize for D == O; the scalar_triple_product. Which is also the 3x3 determinant
 * @todo Can also be used for coplanarity test of four points
 * @todo Can also be used to check for linear dependence of three points (D == 0)
 * @todo Can also be used for calculating the volume of a tetrahedron defined by the four points
 * @todo Expose the exact version
 * @todo Clarify the orientation of triangles and planes with enums
 */
double orient3d(const double* pa, const double* pb, const double* pc, const double* pd);
double orient3dexact(const double* pa, const double* pb, const double* pc, const double* pd);

/** @brief Check if pe is in the sphere defined by pa,pb,pc,pd
 *
 * The points pa, pb, pc, pd must not be coplanar.
 *
 * If pe is inside the sphere, the returned value is positive. If pe is outside the sphere, the returned value is
 * negative. If pe is on the sphere, returns 0.
 *
 * Calculates the 5x5 determinant
 *
 * | pa[0] pa[1] pa[2] pa.pa 1 |
 * | pb[0] pb[1] pb[2] pb.pb 1 |
 * | pc[0] pc[1] pc[2] pc.pc 1 |
 * | pd[0] pd[1] pd[2] pd.pd 1 |
 * | pe[0] pe[1] pe[2] pe.pe 1 |
 *
 * @todo In calculating this result, the function seems to do a coplanarity calculation, which might be duplicated
 * with orient3d
 */
double insphere(const double* pa, const double* pb, const double* pc, const double* pd, const double* pe);
#ifdef __cplusplus
}
#endif
#endif // PREDICATES_H
