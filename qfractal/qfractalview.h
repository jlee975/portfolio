#ifndef QFRACTALVIEW_H
#define QFRACTALVIEW_H

#include <QTimer>
#include <QWidget>

class Mandelbrot;
class DoCalculation;

namespace Ui
{
class QFractalView;
}

class QFractalView
	: public QWidget
{
	Q_OBJECT
public:
	explicit QFractalView(QWidget* parent = nullptr);
	~QFractalView() override;
public slots:
	void redraw();
	void zoomout();
	void resizeFractal();
private slots:
	void calc_finished();
	void zoomin(int x0, int y0);
	void on_save_clicked();
	void on_go_clicked();
	void update_view();
private:
	struct zoom
	{
		double centerx;
		double centery;
		double scale;
	};

	void serializeState();

	Ui::QFractalView*   ui;
	QTimer*             timer;
	int                 w{ 400 };
	int                 h{ 400 };
	double              centerx{ -.75 };
	double              centery{ 0. };
	double              scale{ 1.25 };
	Mandelbrot*         f;
	DoCalculation*      calc_thread;
	std::vector< zoom > history;
	unsigned char*      pixels;
};

#endif // QFRACTALVIEW_H
