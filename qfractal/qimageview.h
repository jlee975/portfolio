#ifndef QIMAGEVIEW_H
#define QIMAGEVIEW_H

#include <QImage>
#include <QWidget>

class QImageView
	: public QWidget
{
	Q_OBJECT
public:
	explicit QImageView(QWidget* parent = nullptr);
	~QImageView() override;

	void paintEvent(QPaintEvent*) override;
	void setData(const unsigned char* p, int w, int h);
	void mouseDoubleClickEvent(QMouseEvent* e) override;
	void save();
signals:

	void doubleClicked(int, int);
public slots:
private:
	QImage* image{ nullptr };
};

#endif // QIMAGEVIEW_H
