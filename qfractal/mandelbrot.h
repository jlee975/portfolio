#ifndef MANDELBROT_H
#define MANDELBROT_H

#include <utility>

class Mandelbrot
{
public:
	Mandelbrot();
	std::pair< bool, int > operator()(double, double) const;
private:
	const double eps;
};

#endif // MANDELBROT_H
