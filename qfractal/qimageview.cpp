#include "qimageview.h"

#include <QFileDialog>
#include <QMouseEvent>
#include <QPainter>

QImageView::QImageView(QWidget* parent)
	: QWidget(parent)
{
}

QImageView::~QImageView()
{
	QImage* p = image;

	image = nullptr;
	delete p;
}

void QImageView::setData(
	const unsigned char* p,
	int                  w,
	int                  h
)
{
	QImage* old = image;

	if ( p != nullptr )
	{
		image = new QImage(p, w, h, QImage::Format_RGB32);
		setMinimumSize(w, h);
		resize(w, h);
	}
	else
	{
		image = nullptr;
	}

	delete old;
}

void QImageView::paintEvent(QPaintEvent*)
{
	if ( image != nullptr )
	{
		QPainter painter(this);

		painter.drawImage(0, 0, *image);
	}
}

void QImageView::mouseDoubleClickEvent(QMouseEvent* e)
{
	emit doubleClicked(e->position().x(), e->position().y() );
}

void QImageView::save()
{
	if ( image != nullptr )
	{
		QString filename = QFileDialog::getSaveFileName(this, "Choose a file name");

		if ( !filename.isEmpty() )
		{
			image->save(filename, "PNG");
		}
	}
}
