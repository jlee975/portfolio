#ifndef QCATALOGVIEW_H
#define QCATALOGVIEW_H

#include <string>

#include <QFrame>

class QCatalogModel;
class QTreeView;
class QModelIndex;
class Playlist;
class MetaData;

class QCatalogView
	: public QFrame
{
	Q_OBJECT
private:
	QCatalogModel* qcm;
	QTreeView*     treDir;

	void gettreeitems(std::vector< std::string >&, const QModelIndex&);
	void findfiles(const std::string&);

	QCatalogView(const QCatalogView&);            // non-copyable
	QCatalogView& operator=(const QCatalogView&); // non-copyable
public:
	QCatalogView(QWidget*, const Playlist&);
	~QCatalogView() override;
	void remove(const std::string&);
	void update(const std::string&, const MetaData&);
	std::string missingMeta() const;
	Playlist getplaylist() const;
	bool getmeta(const std::string&, MetaData&) const;
public slots:
	void treContextMenu(const QPoint&);
	void addTreeItem();
	void replaceItems();
	void scanPressed();
signals:
	void needmeta();
	void addtoplaylist(std::vector< std::string >, bool);
};

#endif // QCATALOGVIEW_H
