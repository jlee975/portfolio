#ifndef QCATALOGITEM_H
#define QCATALOGITEM_H

#include <string>
#include <vector>

#include <QString>

class MetaData;
class Playlist;

class QCatalogItem
{
	std::vector< QCatalogItem* > childItems;
	QCatalogItem*                parentItem;
	QString                      label_;
	std::string                  data_;
	MetaData*                    meta;
	bool                         leaf;

	QCatalogItem(const QCatalogItem&)            = delete; // prevent copying
	QCatalogItem& operator=(const QCatalogItem&) = delete; // prevent copying
public:
	QCatalogItem(QCatalogItem*, QString, std::string, bool);
	~QCatalogItem();

	void appendChild(QCatalogItem*);
	void          insert(std::size_t, QCatalogItem*);
	QCatalogItem* child(std::size_t) const;
	QCatalogItem* parent() const;
	void erase(std::size_t);
	std::size_t childCount() const;

	void traverse(Playlist&) const;

	QString label() const;
	std::string data() const;
	std::size_t row() const;
	void setmeta(const MetaData&);
	bool isworthupdating(const MetaData&) const;
	MetaData getmeta() const;
	bool needsmeta() const;
	bool isleaf() const;
	void insertGroup(std::size_t, const std::string&);
};

#endif // QCATALOGITEM_H
