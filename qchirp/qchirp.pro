#-------------------------------------------------
#
# Project created by QtCreator 2018-11-07T17:38:48
#
#-------------------------------------------------
include(../vars.pri)

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = qchirp
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG -= c++11 c++1z c++14 c++17 c++2a

SOURCES += \
        main.cpp \
    alsaout.cpp \
    qchirp.cpp \
    qplaylistmodel.cpp \
    qplaylistview.cpp \
    qcatalogitem.cpp \
    qcatalogmodel.cpp \
    qcatalogview.cpp

HEADERS += \
    alsaout.h \
    qchirp.h \
    qplaylistmodel.h \
    qplaylistview.h \
    qcatalogitem.h \
    qcatalogmodel.h \
    qcatalogview.h

FORMS +=

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../jlee/release/ -ljlee
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../jlee/debug/ -ljlee
else:unix: LIBS += -L$$OUT_PWD/../jlee/ -ljlee

INCLUDEPATH += $$PWD/../jlee
DEPENDPATH += $$PWD/../jlee

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../jlee/release/libjlee.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../jlee/debug/libjlee.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../jlee/release/jlee.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../jlee/debug/jlee.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../jlee/libjlee.a

RESOURCES += \
    qchirp.qrc

unix: CONFIG += link_pkgconfig
unix: PKGCONFIG += alsa
