/***************************************************************************/ /**
   \file        qchirp.cpp

   \note This source code expects "decoder" and "reporter" to exist elsewhere.
 *******************************************************************************/
#include "qchirp.h"

#include <stdexcept>
#ifndef NDEBUG
#include <iostream>
#endif

#include <QAction>
#include <QCoreApplication>
#include <QHBoxLayout>
#include <QLabel>
#include <QMenu>
#include <QModelIndex>
#include <QPushButton>
#include <QSlider>
#include <QSplitter>
#include <QStatusBar>
#include <QToolBar>
#include <utility>

#include "chapp/settings.h"

#include "qcatalogview.h"
#include "qplaylistview.h"

extern Chirp    decoder;
extern Chirp    reporter;
extern Playlist catalog;
extern Settings settings;

/*=========================================================================*//**
   \class       QChirp

   \todo Lock down the thread safety. It may be possible that decoder and reporter
      are trying to access this after it has been destructed.
   \todo See QEvent::registerEventType()
   \todo Qt v4.6 supporst QIcon::fromTheme()
   \todo Move history into playthread. One less thing to have in the Qt files
   \todo Move catalog stuff out of QChirp. One less thing to have in the Qt files
*//*==========================================================================*/
QChirp::QChirp(QWidget* parent_)
	: QWidget(parent_), qcv(nullptr), qpv(nullptr), sliSeek(nullptr), btnTime(nullptr), songlength(-1),
	stopped(true), playcallback(nullptr), metacallback(nullptr), actPlay(nullptr), actPause(nullptr),
	actStop(nullptr), actPrev(nullptr), actNext(nullptr), actQuit(nullptr), trayIcon(nullptr), lblStatus(nullptr)
{
	// Create the playlist view and controls
	std::string playdir;

	{
		const auto v = settings["dirs/playlists"];

		if ( auto p = std::get_if< std::string >(&v) )
		{
			playdir = *p;
		}
	}

	qpv = new QPlaylistView(this, QString::fromStdString(playdir) );
	connect(qpv, SIGNAL(needmeta()), SLOT(ensureMeta()));
	connect(qpv, SIGNAL(play(std::string)), SLOT(play(std::string)));

	// create the catalog view and controls
	qcv = new QCatalogView(this, catalog);
	connect(qcv, SIGNAL(needmeta()), SLOT(ensureMeta()));

	qpv->connect(qcv, SIGNAL(addtoplaylist(std::vector<std::string>,bool)),
		SLOT(insert(std::vector<std::string>,bool)));

	// create actions, toolbars, system tray icon
	actPlay = new QAction(QIcon(":/icons/play"), tr("Play"), this);
	actPlay->setShortcut(Qt::CTRL | Qt::Key_P);
	connect(actPlay, SIGNAL(triggered()), SLOT(playPressed()));
	actPause = new QAction(QIcon(":/icons/pause"), tr("Pause"), this);
	actPause->setShortcut(Qt::CTRL | Qt::Key_Space);
	connect(actPause, SIGNAL(triggered()), SLOT(pausePressed()));
	actStop = new QAction(QIcon(":/icons/stop"), tr("Stop"), this);
	actStop->setShortcut(Qt::CTRL | Qt::Key_S);
	connect(actStop, SIGNAL(triggered()), SLOT(stopPressed()));
	actPrev = new QAction(QIcon(":/icons/prev"), tr("Previous"), this);
	actPrev->setShortcut(Qt::CTRL | Qt::Key_Left);
	qpv->connect(actPrev, SIGNAL(triggered()), SLOT(prevPressed()));
	actNext = new QAction(QIcon(":/icons/next"), tr("Next"), this);
	actNext->setShortcut(Qt::CTRL | Qt::Key_Right);
	qpv->connect(actNext, SIGNAL(triggered()), SLOT(nextPressed()));
	actQuit = new QAction(QIcon(":/icons/quit"), tr("Quit"), this);
	actQuit->setShortcut(QKeySequence::Quit);
	connect(actQuit, SIGNAL(triggered()), SLOT(close()));

	sliSeek = new QSlider(Qt::Horizontal, this);
	sliSeek->setTracking(false);
	connect(sliSeek, SIGNAL(sliderReleased()), SLOT(seekReleased()));

	btnTime = new QPushButton("--:--", this);
	btnTime->setFlat(true);

	initSystemTray();

	// layout the window
	auto* qs = new QSplitter;

	qs->addWidget(qcv);
	qs->addWidget(qpv);
	QList< int > widths;

	widths.append(100);
	widths.append(400);
	qs->setSizes(widths);

	lblStatus = new QLabel("Welcome to QChirp", this);

	playcallback = new QChirp::Callback(this);
	decoder.setCallback(playcallback);
	metacallback = new QChirp::Callback(this);
	reporter.setCallback(metacallback);
	show();

	auto* h = new QHBoxLayout;

	h->addWidget(qs);
	setLayout(h);
}

/***************************************************************************/ /**
   \fn          ~QChirp
   \brief       Destructor

   Disconnect "decoder" and "reporter" from *this by erasing callback
 *******************************************************************************/
QChirp::~QChirp()
{
	catalog = qcv->getplaylist();
	decoder.setCallback(nullptr);
	decoder.stop();
	delete playcallback;
	reporter.setCallback(nullptr);
	delete metacallback;
}

void QChirp::buildStatusbar(QStatusBar* sbar)
{
	sbar->addWidget(lblStatus);
}

/***************************************************************************/ /**
   \fn          void QChirp::initControlBar()
   \brief       Build the toolbar of controls
 *******************************************************************************/
QToolBar* QChirp::initControlBar()
{
	auto* controlbar = new QToolBar("Controls", this);

	controlbar->addAction(actPlay);
	controlbar->addAction(actPause);
	controlbar->addWidget(sliSeek);
	controlbar->addWidget(btnTime);
	controlbar->addAction(actStop);
	controlbar->addAction(actPrev);
	controlbar->addAction(actNext);
	return controlbar;
}

/***************************************************************************/ /**
   \fn          void QChirp::initSystemTray()
   \brief       Build the system tray

   \bug Keys don't work
   \todo Parent should probably set this up
 *******************************************************************************/
void QChirp::initSystemTray()
{
	if ( !QSystemTrayIcon::isSystemTrayAvailable() )
	{
		return;
	}

	auto* m = new QMenu(tr("Context Menu"), this);

	m->addAction(actPlay);
	m->addAction(actPause);
	m->addAction(actStop);
	m->addAction(actPrev);
	m->addAction(actNext);
	m->addSeparator();
	m->addAction(actQuit);

	trayIcon = new QSystemTrayIcon(QIcon(":/icons/app"), this);

	trayIcon->setContextMenu(m);
	trayIcon->show();

	connect(trayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)),
		SLOT(showhide(QSystemTrayIcon::ActivationReason)));
}

/***************************************************************************/ /**
   \fn          void QChirp::showhide()
   \brief       Shows or hides the app. window if the system tray icon is clicked
 *******************************************************************************/
void QChirp::showhide(QSystemTrayIcon::ActivationReason r)
{
	if ( r == QSystemTrayIcon::Trigger )
	{
		if ( auto* mw = dynamic_cast< QMainWindow* >( parent() ) )
		{
			mw->setVisible(mw->isHidden() );
		}
	}
}

/***************************************************************************/ /**
   \todo Could send meta and update data w/ a subclass of QEvent
   \todo The updating of the slider is expensive, esp. w/ null output
 *******************************************************************************/
bool QChirp::event(QEvent* ev)
{
	if ( ev->type() != QEvent::User )
	{
		return QWidget::event(ev);
	}

	auto* qce = static_cast< QChirp::Event* >( ev );

	switch ( qce->chirptype )
	{
	case Chirp::UpdatePos:
		updateSlider(qce->progress, qce->total);
		break;
	case Chirp::Finished:

		if ( !stopped )
		{
			qpv->nextPressed();
		}
		else
		{
			resetSlider();
		}

		break;
	case Chirp::HaveMeta:
		qpv->update(qce->filename, qce->meta);
		qcv->update(qce->filename, qce->meta);
		ensureMeta();
		break;
	case Chirp::MetaError:
		qcv->remove(qce->filename);
		qpv->remove(qce->filename);
		ensureMeta();
		break;
	} // switch

	return true;
}

/***************************************************************************/ /**
   \fn          void QChirp::ensureMeta()
   \brief       Requests the researcher thread to fill in any meta info is missing

   \todo Why ask the widget for catalog info when I could ask the catalog directly?
 *******************************************************************************/
void QChirp::ensureMeta()
{
	MetaData m;

	// Get as much meta from catalog as possible
	std::string filename = qpv->missingMeta();

	while ( !filename.empty() && qcv->getmeta(filename, m) )
	{
		qpv->update(filename, m);
		filename = qpv->missingMeta();
	}

	// Go to disk for any other missing meta
	if ( filename.empty() ) // playlist is up-to-date, check catalog
	{
		filename = qcv->missingMeta();

		if ( !filename.empty() )
		{
			reporter.getMeta(filename);
		}
		else
		{
			reporter.sleep();
		}
	}
	else
	{
		reporter.getMeta(filename);
	}
}

/***************************************************************************/ /**
   \fn          void QChirp::play(const std::string&, bool)
   \brief       Plays a file
 *******************************************************************************/
void QChirp::play(const std::string& s)
{
	songlength = -1;

	resetSlider();

	if ( !s.empty() )
	{
		stopped = false;
		decoder.play(s);
		qpv->addtohistory(s);
	}
	else
	{
		stopped = true;
		decoder.stop();
	}
}

/***************************************************************************/ /**
   \fn          void QChirp::playPressed()
   \brief       User pressed the "play" icon on the controls toolbar
 *******************************************************************************/
void QChirp::playPressed()
{
	QModelIndex idx = qpv->currentIndex();

	if ( idx.isValid() )
	{
		QVariant    v = idx.data(Qt::UserRole);
		std::string s = v.toString().toStdString();
		play(s);
	} // \todo else play first

}

/***************************************************************************/ /**
   \fn          void QChirp::pausePressed()
   \brief       User pressed the "pause" icon on the controls toolbar
 *******************************************************************************/
void QChirp::pausePressed()
{
	decoder.pause();
}

/***************************************************************************/ /**
   \fn          void QChirp::stopPressed()
   \brief       User pressed the "stop" icon on the controls toolbar
 *******************************************************************************/
void QChirp::stopPressed()
{
	stopped    = true;
	songlength = -1;
	decoder.stop();
	resetSlider();
}

/***************************************************************************/ /**
   \fn          void QChirp::updateSlider(long, long)
   \brief       Updates the slider with the current play position

   \todo Handle long->int better for slider
 *******************************************************************************/
void QChirp::updateSlider(
	long cur,
	long end
)
{
	if ( !sliSeek->isSliderDown() && !stopped )
	{
		if ( end != songlength )
		{
			sliSeek->setRange(0, static_cast< int >( end ) );
			songlength = end;
		}

		sliSeek->setValue(static_cast< int >( cur ) );
	}

	if ( !stopped )
	{
		btnTime->setText(QString("%1:%2").arg(cur / 60).arg(cur % 60, 2, 10, QChar('0') ) );
	}
}

/***************************************************************************/ /**
   \fn          void QChirp::seekReleased()
   \brief       User let go of the slider

   \todo Translate int to sample_off
 *******************************************************************************/
void QChirp::seekReleased()
{
	if ( !stopped )
	{
		int i = sliSeek->sliderPosition();
		decoder.seekTime(i);
	}
	else
	{
		sliSeek->setValue(0);
	}
}

/***************************************************************************/ /**
 *******************************************************************************/
void QChirp::resetSlider()
{
	sliSeek->setRange(0, 99);
	sliSeek->setValue(0);
	btnTime->setText("--:--");
}

/***************************************************************************/ /**
   \fn          void QChirp::addPlaylist(std::string, std::string)
   \brief       Add a playlist to the combobox

   Add a playlist of given name and path to the combobox
 *******************************************************************************/
void QChirp::addPlaylist(
	const std::string& name,
	const std::string& path
)
{
	qpv->addPlaylist(name, path);
}

/*
   void QChirp::closeEvent(QCloseEvent * e) {
    if (trayIcon->isVisible()) {
        hide();
        e->ignore();
    } else e->accept();
   }
 */
/*=========================================================================*//**
   \class       QChirpCallback
   \brief       Callback class to pass to Chirp so that QChirp can be notified
*//*==========================================================================*/
/***************************************************************************/ /**
 *******************************************************************************/
QChirp::Callback::Callback(QChirp* w)
	: ui(w), qa(nullptr)
{
	qa = QCoreApplication::instance();

	if ( qa == nullptr )
	{
		throw std::runtime_error("Qt not initialized");
	}
}

/***************************************************************************/ /**
 *******************************************************************************/
void QChirp::Callback::time(
	long x,
	long y
)
{
	qa->postEvent(ui, new QChirp::Event(Chirp::UpdatePos, x, y) );
}

/***************************************************************************/ /**
 *******************************************************************************/
void QChirp::Callback::meta(
	std::string filename,
	MetaData    m,
	bool        success
)
{
	if ( success )
	{
		qa->postEvent(ui, new QChirp::Event(Chirp::HaveMeta, filename, m) );
	}
	else
	{
		qa->postEvent(ui, new QChirp::Event(Chirp::MetaError, filename) );
	}
}

/***************************************************************************/ /**
 *******************************************************************************/
void QChirp::Callback::done()
{
	qa->postEvent(ui, new QChirp::Event(Chirp::Finished, 0, 0) );
}

/*=========================================================================*//**
   \class       QChirpEvent
   \brief       Subclass of QEvent for passing data from Chirp
*//*==========================================================================*/
/***************************************************************************/ /**
 *******************************************************************************/
QChirp::Event::Event(
	Chirp::Notifications n,
	long                 x,
	long                 y
)
	: QEvent(QEvent::User), chirptype(n), progress(x), total(y)
{
}

/***************************************************************************/ /**
 *******************************************************************************/
QChirp::Event::Event(
	Chirp::Notifications n,
	std::string          s,
	MetaData             m
)
	: QEvent(QEvent::User), chirptype(n), progress(0), total(0), meta(std::move(m) ), filename(std::move(s) )
{
}

/***************************************************************************/ /**
 *******************************************************************************/
QChirp::Event::Event(
	Chirp::Notifications n,
	std::string          s
)
	: QEvent(QEvent::User), chirptype(n), progress(0), total(0), filename(std::move(s) )
{
}
