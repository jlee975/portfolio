#ifndef QCHIRP_HPP
#define QCHIRP_HPP

#include <string>

#include <QEvent>
#include <QMainWindow>
#include <QSystemTrayIcon>
class QCoreApplication;
class QSlider;
class QPushButton;
class QStatusBar;
class QLabel;

#include "chirp/chirp.h"

class QChirp;
class QCatalogView;
class QPlaylistView;

class QChirp
	: public QWidget
{
	Q_OBJECT
private:
	class Event
		: public QEvent
	{
public:
		Event(Chirp::Notifications, std::string);
		Event(Chirp::Notifications, long, long);
		Event(Chirp::Notifications, std::string, MetaData);

		Chirp::Notifications chirptype;
		long                 progress;
		long                 total;
		MetaData             meta;
		std::string          filename;
	};

	class Callback
		: public ChirpCallback
	{
		QChirp*           ui;
		QCoreApplication* qa;
		Callback(const Callback&)            = delete; // non-copyable
		Callback& operator=(const Callback&) = delete; // non-copyable
public:
		explicit Callback(QChirp*);
		void time(long, long) override;
		void meta(std::string, MetaData, bool) override;
		void done() override;
	};
public:
	explicit QChirp(QWidget*);
	~QChirp() override;
	void addPlaylist(const std::string&, const std::string&);
	void buildStatusbar(QStatusBar*);
	QToolBar* initControlBar();
public slots:
	void showhide(QSystemTrayIcon::ActivationReason);

	void playPressed();
	void pausePressed();
	void stopPressed();
	void seekReleased();

	void ensureMeta();
	void play(const std::string&);

	bool event(QEvent*) override;
protected:
	// void closeEvent(QCloseEvent *);
private:
	QChirp(const QChirp&);            // non-copyable
	QChirp& operator=(const QChirp&); // non-copyable

	void initSystemTray();
	void resetSlider();
	void updateSlider(long, long);

	// Widgets - These must exist for the constructor to finish
	QCatalogView*  qcv;
	QPlaylistView* qpv;

	QSlider*     sliSeek;
	QPushButton* btnTime;

	long songlength;

	bool stopped;

	Callback* playcallback;
	Callback* metacallback;

	QAction* actPlay;
	QAction* actPause;
	QAction* actStop;
	QAction* actPrev;
	QAction* actNext;
	QAction* actQuit;

	QSystemTrayIcon* trayIcon;
	QLabel*          lblStatus;
};

#endif // ifndef QCHIRP_HPP
