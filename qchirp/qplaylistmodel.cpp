/*=========================================================================*//**
   \todo Look into setUpdatesEnabled for views. Might speed up large operations

*//*==========================================================================*/
#include <algorithm>
#include <cstddef>
#include <set>
#include <utility>

#include "qplaylistmodel.h"

/*=========================================================================*//**
   \class       QPlaylistModel
   \brief       Qt wrapper for using a Playlist in the Model/View framework
*//*==========================================================================*/
QPlaylistModel::QPlaylistModel(QObject* par)
	: QAbstractTableModel(par)
{
	initHeader();
}

/***************************************************************************/ /**
   \fn          QPlaylistModel::QPlaylistModel(const Playlist&)
   \brief       Constructor from playlist object

   \todo Maybe return QString, and other appropriate Qt types. Since this model
      is meant to bridge between standard C++ and Qt.

   The majority of this code directly calls Playlist, with some small Qt fixups.
 *******************************************************************************/
QPlaylistModel::QPlaylistModel(
	Playlist p,
	QObject* par
)
	: QAbstractTableModel(par), tabledata(std::move(p) )
{
	initHeader();
}

/***************************************************************************/ /**
 *******************************************************************************/
void QPlaylistModel::initHeader()
{
	// Create header information for each field available through MetaData
	for (int i = 0, n = MetaData::numFields; i < n; ++i)
	{
		MetaData::Field f = MetaData::intToField(i);

		if ( f != MetaData::Invalid_ )
		{
			std::string s = displayName(f);
			headerinfo.emplace_back(f, tr(s.c_str() ) );
		}
	}
}

/***************************************************************************/ /**
   \fn          QModelIndex QPlaylistModel::index(int, int, const QModelIndex)
   \brief       Create a QModelIndex for the given row and column
 *******************************************************************************/
QModelIndex QPlaylistModel::index(
	int                row,
	int                column,
	const QModelIndex& par
) const
{
	if ( !hasIndex(row, column, par) )
	{
		return QModelIndex();
	}

	return createIndex(row, column);
}

/***************************************************************************/ /**
   \fn          QVariant QPlaylistModel::data(const QModelIndex&, int)

 *******************************************************************************/
QVariant QPlaylistModel::data(
	const QModelIndex& ind,
	int                role
) const
{
	QVariant v;

	auto i = static_cast< std::size_t >( ind.row() );

	if ( !ind.isValid() || i >= tabledata.size() )
	{
		// do nothing
	}
	else if ( role == Qt::DisplayRole )
	{
		const std::string& s = tabledata.field(i, headerinfo[ind.column()].first);
		v = s.c_str();
	}
	else if ( role == Qt::UserRole )
	{
		const std::string& s = tabledata.filename(i);
		v = s.c_str();
	}

	return v;
}

/***************************************************************************/ /**
   \fn          QVariant QPlaylistModel::headerData(int, Qt::Orientation, int)
 *******************************************************************************/
QVariant QPlaylistModel::headerData(
	int             section,
	Qt::Orientation dir,
	int             role
) const
{
	QVariant v;

	if ( role == Qt::DisplayRole )
	{
		if ( dir == Qt::Horizontal )
		{
			v = headerinfo[section].second;
		}
		else
		{
			v = section + 1;
		}
	}

	return v;
}

/***************************************************************************/ /**
 *******************************************************************************/
int QPlaylistModel::rowCount(const QModelIndex&) const
{
	std::size_t n = tabledata.size();

	return n > INT_MAX ? INT_MAX : static_cast< int >( n );
}

/***************************************************************************/ /**
 *******************************************************************************/
int QPlaylistModel::columnCount(const QModelIndex&) const
{
	std::size_t n = headerinfo.size();

	return n > INT_MAX ? INT_MAX : 3 /*static_cast<int>(n)*/;
}

/***************************************************************************/ /**
 *******************************************************************************/
void QPlaylistModel::sort(
	int           col,
	Qt::SortOrder order
)
{
	beginResetModel();
	tabledata.sort(headerinfo[col].first, ( order == Qt::AscendingOrder ) );
	endResetModel();
}

/***************************************************************************/ /**
   \todo alternative is to copy tabledata, insert into copy, swap in copy
   \todo accept by value instead of reference. Erase unacceptable values
 *******************************************************************************/
void QPlaylistModel::insertSongs(const std::vector< std::string >& l)
{
	Playlist pl(tabledata);

	for (const auto& i : l)
	{
		pl.insert(i);
	}

	if ( pl.size() != tabledata.size() )
	{
		beginResetModel();
		tabledata.swap(pl);
		endResetModel();
	}
}

/***************************************************************************/ /**
 *******************************************************************************/
void QPlaylistModel::remove(const std::string& s)
{
	std::size_t k = tabledata.find(s);

	if ( k < tabledata.size() && k <= INT_MAX )
	{
		beginRemoveRows(QModelIndex(), static_cast< int >( k ), static_cast< int >( k ) );
		tabledata.erase(k);
		endRemoveRows();
	} /// \todo else

}

/***************************************************************************/ /**
 *******************************************************************************/
void QPlaylistModel::remove(const QModelIndexList& r)
{
	std::set< int > rows;

	for (const auto& jt : r)
	{
		rows.insert(jt.row() );
	}

	// Remove each row from highest numbered to least numbered
	beginResetModel();
	auto it = rows.rbegin();

	for (; it != rows.rend(); ++it)
	{
		int i = *it;

		if ( i >= 0 )
		{
			tabledata.erase(static_cast< std::size_t >( i ) );
		}
	}

	endResetModel();
}

/***************************************************************************/ /**
 *******************************************************************************/
void QPlaylistModel::clear()
{
	beginResetModel();
	tabledata.clear();
	endResetModel();
}

/***************************************************************************/ /**
   \todo Might be able to implement QAbstractItemModel::match()
 *******************************************************************************/
int QPlaylistModel::rowfromfilename(const std::string& f) const
{
	std::size_t k = 0;

	std::size_t n = tabledata.size();

	while ( k < n && tabledata.filename(k) != f )
	{
		++k;
	}

	return k < n && k <= INT_MAX ? static_cast< int >( k ) : -1;
}

/***************************************************************************/ /**
   \todo Might use QAbstractItemModel::setData() to accomplish this
 *******************************************************************************/
void QPlaylistModel::updateMeta(
	const std::string& f,
	const MetaData&    m
)
{
	std::size_t k = tabledata.updateMeta(f, m);

	if ( k < tabledata.size() && k <= INT_MAX )
	{
		QModelIndex left  = index(static_cast< int >( k ), 0, QModelIndex() );
		QModelIndex right = index(static_cast< int >( k ), columnCount(QModelIndex() ) - 1, QModelIndex() );
		emit        dataChanged(left, right);
	}
}

/***************************************************************************/ /**
 *******************************************************************************/
std::string QPlaylistModel::missingMeta() const
{
	return tabledata.missingMeta();
}

/***************************************************************************/ /**
 *******************************************************************************/
const Playlist& QPlaylistModel::playlist() const
{
	return tabledata;
}
