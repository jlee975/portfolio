/***************************************************************************/ /**
   \class       QCatalogItem

   \todo In general, could be leaking memory due to unsuccessful adds. Especially
      if the to-be-added item has no existing parent
 *******************************************************************************/
#include "qcatalogitem.h"

#include <stdexcept>

#include <QObject>
#include <utility>

#include "chirp/metadata.h"
#include "playlist/playlist.h"

QCatalogItem::QCatalogItem(
	QCatalogItem* p,
	QString       s,
	std::string   f,
	bool          node
)
	: parentItem(p), label_(std::move(s) ), data_(std::move(f) ), meta(nullptr), leaf(!node)
{
	if ( p != nullptr )
	{
		if ( p->leaf )
		{
			throw std::runtime_error("A leaf cannot be a parent.");
		}

		p->childItems.push_back(this);
	}

	if ( label_.isEmpty() )
	{
		if ( leaf )
		{
			label_ = QString::fromStdString(data_);
		}
		else
		{
			label_ = QObject::tr("Unknown");
		}
	}
}

QCatalogItem::~QCatalogItem()
{
	delete meta;

	while ( !childItems.empty() )
	{
		QCatalogItem* qci = childItems.back();
		childItems.pop_back();
		// prevents qci from trying to remove itself from this
		qci->parentItem = nullptr;
		delete qci;
	}
}

/**
 * \todo Remove from original parent
 */
void QCatalogItem::appendChild(QCatalogItem* c)
{
	if ( !leaf && ( c != nullptr ) )
	{
		childItems.push_back(c);

		if ( QCatalogItem* p = c->parentItem )
		{
			p->erase(c->row() );
		}

		c->parentItem = this;
	}
}

/**
 * \todo Remove from original parent
 */
void QCatalogItem::insert(
	std::size_t   i,
	QCatalogItem* q
)
{
	if ( !leaf && ( q != nullptr ) )
	{
		if ( QCatalogItem* p = q->parentItem )
		{
			p->erase(q->row() );
		}

		if ( i < childItems.size() )
		{
			childItems.insert(childItems.begin() + i, q);
		}
		else
		{
			childItems.push_back(q);
		}

		q->parentItem = this;
	}
}

/**
 * \bug not deleted if insert fails
 */
void QCatalogItem::insertGroup(
	std::size_t        i,
	const std::string& s
)
{
	insert(i, new QCatalogItem(nullptr, QString::fromStdString(s), s, true) );
}

void QCatalogItem::erase(std::size_t i)
{
	QCatalogItem* item = childItems[i];

	childItems.erase(childItems.begin() + i);
	delete item;
}

std::size_t QCatalogItem::childCount() const
{
	return childItems.size();
}

/**
 * \todo These are internal.. can probably use std::size_t
 */
QCatalogItem* QCatalogItem::child(std::size_t i) const
{
	return childItems.at(i);
}

QCatalogItem* QCatalogItem::parent() const
{
	return parentItem;
}

std::size_t QCatalogItem::row() const
{
	std::size_t i = 0;

	if ( parentItem != nullptr )
	{
		std::size_t n = parentItem->childItems.size();

		while ( i < n && parentItem->childItems[i] != this )
		{
			++i;
		}

		/// \bug if (i == n) throw
	}

	return i;
}

QString QCatalogItem::label() const
{
	return label_;
}

bool QCatalogItem::needsmeta() const
{
	return meta == nullptr;
}

MetaData QCatalogItem::getmeta() const
{
	return meta != nullptr ? *meta : MetaData();
}

bool QCatalogItem::isleaf() const
{
	return leaf;
}

/**
 * \todo Rename
 */
bool QCatalogItem::isworthupdating(const MetaData& m) const
{
	return meta == nullptr || m != *meta;
}

std::string QCatalogItem::data() const
{
	return data_;
}

void QCatalogItem::setmeta(const MetaData& m)
{
	if ( meta != nullptr )
	{
		*meta = m;
	}
	else
	{
		meta = new MetaData(m);
	}
}

/**
 * \todo Remove from class
 * \todo The traversal and non-empty data are pretty much mutually exclusive.
 *   Especially since they are supposed to capture the idea of node and leaf
 *   respectively.
 */
void QCatalogItem::traverse(Playlist& p) const
{
	if ( !leaf )
	{
		for (const auto* childItem : childItems)
		{
			childItem->traverse(p);
		}
	}
	else
	{
		if ( meta != nullptr )
		{
			p.insert(data_, *meta);
		}
		else
		{
			p.insert(data_);
		}
	}
}
