#ifndef ALSAOUT_HPP
#define ALSAOUT_HPP

#include <string>
#include <vector>

#if ( !defined( linux ) && !defined( __linux ) )
#error "ALSA is only available on Linux systems. Remove this component from your build."
#endif

#include <alsa/asoundlib.h>

#include "audio/sink.h"

class AlsaOut
	: public audio::Sink
{
	snd_pcm_t*                      handle;
	snd_output_t*                   out;
	std::string                     device;
	std::vector< signed short int > outbuf;

	AlsaOut(const AlsaOut&)            = delete; // non-copyable
	AlsaOut& operator=(const AlsaOut&) = delete; // non-copyable
public:
	explicit AlsaOut(std::string);
	~AlsaOut() override;
	bool open() override;
	bool isOpen() override;
	void stop() override;
	void pause() override;
	void resume() override;
	void write(const samples_type&) override;
};

#endif // ALSAOUT_HPP
