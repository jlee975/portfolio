#ifndef QPLAYLISTVIEW_H
#define QPLAYLISTVIEW_H

#include <string>

#include <QFrame>
#include <QString>
class QTableView;
class QComboBox;
class QToolButton;
class QModelIndex;

class QPlaylistModel;
class MetaData;

class QPlaylistView
	: public QFrame
{
	Q_OBJECT
private:
	QPlaylistModel*            tracksmodel;
	QTableView*                tracksview;
	QComboBox*                 cbxLists;
	bool                       shuffle;
	bool                       repeat;
	std::vector< std::string > history;
	QString                    playlistdir;

	QToolButton* addAction(const QIcon&, const QString&, const char*);
	QToolButton* addToggle(const QIcon&, const QString&, const char*);
	QString prep_name(const QString&) const;
	QString unprep_name(const QString&) const;

	QPlaylistView(const QPlaylistView&);            // non-copyable
	QPlaylistView& operator=(const QPlaylistView&); // non-copyable
public slots:
	void rowClicked(const QModelIndex&);
	void addPressed();
	void removePressed();
	void savePressed();
	void exportPressed();
	void newPressed();
	void loadPlaylist(int);
	void nextPressed();
	void prevPressed();
	void shufflePressed(bool);
	void repeatPressed(bool);
	void insert(const std::vector< std::string >&, bool);
signals:
	void needmeta();
	void play(std::string);
public:
	QPlaylistView(QWidget*, QString);
	~QPlaylistView() override;

	std::string missingMeta() const;
	void selectSong(const std::string&);
	void addtohistory(const std::string&);
	void update(const std::string&, const MetaData&);
	void addPlaylist(const std::string&, const std::string&);
	void remove(const std::string&);
	QModelIndex currentIndex() const;
};

#endif // QPLAYLISTVIEW_H
