#include "qplaylistview.h"

#include <fstream>
#include <iostream>

#include <QComboBox>
#include <QDir>
#include <QFileDialog>
#include <QFileInfoList>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QInputDialog>
#include <QMenu>
#include <QTableView>
#include <QToolButton>
#include <utility>

#include "qplaylistmodel.h"

/*=========================================================================*//**
   \class       QPlaylistView

   invariant: tracksmodel != 0
*//*==========================================================================*/
/***************************************************************************/ /**
 *******************************************************************************/
QPlaylistView::QPlaylistView(
	QWidget* p,
	QString  pldir
)
	: QFrame(p), tracksmodel(nullptr), tracksview(nullptr), cbxLists(nullptr), shuffle(false), repeat(false),
	playlistdir(std::move(pldir) )
{
	setFrameStyle(QFrame::StyledPanel | QFrame::Plain);

	tracksview = new QTableView(this);
	tracksview->setAlternatingRowColors(true);
	tracksview->horizontalHeader()->setStretchLastSection(true);
	// tracksview->horizontalHeader()->setMovable(true);
	tracksview->setSelectionBehavior(QAbstractItemView::SelectRows);
	tracksview->setShowGrid(false);
	tracksview->setSortingEnabled(true);
	connect(tracksview, SIGNAL(doubleClicked(const QModelIndex&)), SLOT(rowClicked(const QModelIndex&)));

	tracksmodel = new QPlaylistModel(tracksview);
	tracksview->setModel(tracksmodel);

	auto* hlayout = new QHBoxLayout;

	cbxLists = new QComboBox(this);
	cbxLists->addItem("[New Playlist]"); // empty item
	cbxLists->addItem(tr("Load from Disk") );
	cbxLists->insertSeparator(2);
	/// \todo Remove empty item when one is actually selected
	/// \todo naming sucks
	/// \todo Sort
	QDir          dirPlaylists(playlistdir);
	QFileInfoList diskplaylists = dirPlaylists.entryInfoList(QDir::Files);

	for (const auto& diskplaylist : diskplaylists)
	{
		cbxLists->addItem(unprep_name(diskplaylist.baseName() ), diskplaylist.absoluteFilePath() );
	}

	connect(cbxLists, SIGNAL(currentIndexChanged(int)), SLOT(loadPlaylist(int)));

	QToolButton* savebutton = addAction(QIcon(":/icons/save"), tr("Save Playlist"), SLOT(savePressed()));
	// savebutton->setPopupMode(QToolButton::MenuButtonPopup);
	auto* savemenu = new QMenu(this);

	savemenu->addAction(tr("Export"), this, SLOT(exportPressed()));
	savebutton->setMenu(savemenu);
	QToolButton* newbutton    = addAction(QIcon(":/icons/new"), tr("New Playlist"), SLOT(newPressed()));
	QToolButton* addbutton    = addAction(QIcon(":/icons/add"), tr("Add Files"), SLOT(addPressed()));
	QToolButton* rembutton    = addAction(QIcon(":/icons/remove"), tr("Remove Files"), SLOT(removePressed()));
	QToolButton* shufbutton   = addToggle(QIcon(":/icons/shuffle"), tr("Shuffle"), SLOT(shufflePressed(bool)));
	QToolButton* repeatbutton = addToggle(QIcon(":/icons/repeat"), tr("Repeat"), SLOT(repeatPressed(bool)));

	hlayout->addWidget(cbxLists);
	hlayout->addWidget(savebutton);
	hlayout->addWidget(newbutton);
	hlayout->addWidget(addbutton);
	hlayout->addWidget(rembutton);
	hlayout->addWidget(shufbutton);
	hlayout->addWidget(repeatbutton);

	auto* vlayout = new QVBoxLayout(this);

	vlayout->addLayout(hlayout);
	vlayout->addWidget(tracksview);
}

/***************************************************************************/ /**
 *******************************************************************************/
QPlaylistView::~QPlaylistView() = default;

/***************************************************************************/ /**
 *******************************************************************************/
QToolButton* QPlaylistView::addAction(
	const QIcon&   icon,
	const QString& text,
	const char*    method
)
{
	auto* act = new QAction(icon, text, this);

	connect(act, SIGNAL(triggered()), method);
	auto* btn = new QToolButton(this);

	btn->setAutoRaise(true);
	btn->setDefaultAction(act);
	return btn;
}

/***************************************************************************/ /**
 *******************************************************************************/
QToolButton* QPlaylistView::addToggle(
	const QIcon&   icon,
	const QString& text,
	const char*    method
)
{
	auto* act = new QAction(icon, text, this);

	act->setCheckable(true);
	connect(act, SIGNAL(triggered(bool)), method);
	auto* btn = new QToolButton(this);

	btn->setAutoRaise(true);
	btn->setDefaultAction(act);
	return btn;
}

/***************************************************************************/ /**
 *******************************************************************************/
void QPlaylistView::addtohistory(const std::string& s)
{
	history.push_back(s);
}

/***************************************************************************/ /**
 *******************************************************************************/
void QPlaylistView::selectSong(const std::string& s)
{
	int r = tracksmodel->rowfromfilename(s);

	if ( r >= 0 )
	{
		tracksview->selectRow(r);
	}
}

/***************************************************************************/ /**
 *******************************************************************************/
void QPlaylistView::insert(
	const std::vector< std::string >& items,
	bool                              add
)
{
	if ( !add )
	{
		tracksmodel->clear();
	}

	tracksmodel->insertSongs(items);
	emit needmeta();
}

/***************************************************************************/ /**
 *******************************************************************************/
void QPlaylistView::update(
	const std::string& s,
	const MetaData&    m
)
{
	tracksmodel->updateMeta(s, m);
}

/***************************************************************************/ /**
 *******************************************************************************/
std::string QPlaylistView::missingMeta() const
{
	return tracksmodel->missingMeta();
}

/***************************************************************************/ /**
 *******************************************************************************/
void QPlaylistView::newPressed()
{
	tracksmodel->clear();
}

/***************************************************************************/ /**
   \todo Check if already exists
   \bug Go over this closely
 *******************************************************************************/
void QPlaylistView::savePressed()
{
	bool    ok   = false;
	QString name = QInputDialog::getText(this, tr("Enter a name for your playlist"), tr("Name:"), QLineEdit::Normal,
		QString(), &ok);

	if ( ok && !name.isEmpty() )
	{
		const std::string& spl = tracksmodel->playlist().serialize(Playlist::M3U);

		std::string   goodname = playlistdir.toStdString() + '/' + prep_name(name).toStdString() + ".m3u";
		std::ofstream output(goodname.c_str() );
		output << spl;

		cbxLists->addItem(name, QString::fromStdString(goodname) );
	}
}

/***************************************************************************/ /**
   These functions encode "name" so that it is safe to be a filename, and
   restore the name.

   \todo Could be static or anonymous
 *******************************************************************************/
QString QPlaylistView::prep_name(const QString& name) const
{
	return QString(name.toUtf8().toHex() );
}

QString QPlaylistView::unprep_name(const QString& name) const
{
	return QString::fromUtf8(QByteArray::fromHex(name.toLatin1() ) );
}

/***************************************************************************/ /**
   \fn          void QPlaylistView::exportPressed()
   \brief       User pressed the "save" icon on the controls toolbar

   \todo Keep the filename and save to that, otherwise ask
   \bug Needs to be rewritten
   \todo Wish there were a better way of doing ->playlist
 *******************************************************************************/
void QPlaylistView::exportPressed()
{
	QString s = QFileDialog::getSaveFileName(this, tr("Save Playlist"), QString(), "Playlists (*.pls, *.m3u)");

	if ( !s.isEmpty() )
	{
		const std::string& spl   = tracksmodel->playlist().serialize(Playlist::M3U);
		std::string        fname = s.toStdString();
		std::ofstream      output(fname.c_str() );
		output << spl;
	}
}

/***************************************************************************/ /**
   \fn          void QPlaylistView::loadPlaylist(int)
   \brief       Load the playlist selected by the user via the combobox
 *******************************************************************************/
void QPlaylistView::loadPlaylist(int i)
{
	if ( 0 <= i && i < cbxLists->count() )
	{
		std::string s = cbxLists->itemData(i).toString().toStdString();

		std::ifstream ifs(s.c_str(), std::ios_base::in | std::ios_base::binary);

		if ( ifs.good() )
		{
			Playlist             p = Playlist::loadM3U(ifs);
			auto*                t = new QPlaylistModel(p, tracksview);
			QItemSelectionModel* m = tracksview->selectionModel();
			tracksview->setModel(t);

			std::swap(tracksmodel, t);

			delete m;
			delete t;
			ifs.close();
			emit needmeta();
		}
	}
}

/***************************************************************************/ /**
   \todo Not really used externally... consolidate with various cbx adds
 *******************************************************************************/
void QPlaylistView::addPlaylist(
	const std::string& name,
	const std::string& path
)
{
	cbxLists->addItem(QString::fromStdString(name), QString::fromStdString(path) );
}

/***************************************************************************/ /**
   \todo Automatically add supported extensions to dialog box
 *******************************************************************************/
void QPlaylistView::addPressed()
{
	QStringList files = QFileDialog::getOpenFileNames(this, tr("Add some music"), QString(),
		"Audio (*.ogg *.oga *.flac *.wav)");

	if ( !files.isEmpty() )
	{
		std::vector< std::string > items;
		QStringList                list(files);
		QStringList::iterator      it = list.begin();

		while ( it != list.end() )
		{
			std::string s = it->toStdString();
			items.push_back(s);
			++it;
		}

		tracksmodel->insertSongs(items);

		emit needmeta();
	}
}

/***************************************************************************/ /**
   \todo remove from history
 *******************************************************************************/
void QPlaylistView::removePressed()
{
	QModelIndexList li = tracksview->selectionModel()->selectedIndexes();

	tracksmodel->remove(li);
}

/***************************************************************************/ /**
   \fn          void QPlaylistView::shufflePressed(bool)
   \brief       User toggled the "shuffle" button
 *******************************************************************************/
void QPlaylistView::shufflePressed(bool s)
{
	shuffle = s;
}

/***************************************************************************/ /**
   \fn          void QPlaylistView::repeatPressed(bool)
   \brief       User toggled the "repeat" button
 *******************************************************************************/
void QPlaylistView::repeatPressed(bool r)
{
	repeat = r;
}

/***************************************************************************/ /**
 *******************************************************************************/
void QPlaylistView::rowClicked(const QModelIndex& ind)
{
	QString s = tracksmodel->data(ind, Qt::UserRole).toString();
	emit    play(s.toStdString() );
}

/***************************************************************************/ /**
   \fn          void QPlaylistView::prevPressed()
   \brief       User pressed the "prev" button

   \todo Check that file still exists
 *******************************************************************************/
void QPlaylistView::prevPressed()
{
	std::string s;

	if ( !history.empty() )
	{
		s = history.back();
		history.pop_back();

		if ( !history.empty() )
		{
			s = history.back();
			history.pop_back();
		}
	}

	selectSong(s);
	emit play(s);
}

/***************************************************************************/ /**
   \fn          void QPlaylistView::nextPressed()
   \brief       User pressed the "next" button

   \todo Handle end of list i.e., repeat
   \todo Handle empty model gracefully instead of explicitly
 *******************************************************************************/
void QPlaylistView::nextPressed()
{
	if ( tracksmodel->rowCount(QModelIndex() ) == 0 )
	{
		return;
	}

	std::string s;

	if ( !shuffle ) /// \todo Better handling of shuffle
	{
		std::string t;

		if ( !history.empty() )
		{
			t = history.back();
		}

		int i = 0;

		if ( !t.empty() )
		{
			i = tracksmodel->rowfromfilename(t) + 1;
		}

		QModelIndex qmi = tracksmodel->index(i, 0, QModelIndex() );
		s = tracksmodel->data(qmi, Qt::UserRole).toString().toStdString();
	}
	else
	{
		int      i = rand() % tracksmodel->rowCount(QModelIndex() );
		QVariant v = tracksmodel->data(tracksmodel->index(i, 0, QModelIndex() ), Qt::UserRole);
		s = v.toString().toStdString();
	}

	selectSong(s);
	emit play(s);
}

/***************************************************************************/ /**
   \todo remove from history
 *******************************************************************************/
void QPlaylistView::remove(const std::string& s)
{
	tracksmodel->remove(s);
}

/***************************************************************************/ /**
 *******************************************************************************/
QModelIndex QPlaylistView::currentIndex() const
{
	return tracksview->currentIndex();
}
