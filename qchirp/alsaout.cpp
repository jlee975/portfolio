/***************************************************************************/ /**
   \file        alsaout.cpp
   \brief       Provides C++ wrapper for interfacing with ALSA
   \author      Jonathan Lee

   This file provides the class AlsaOut, which can be used as a Chirp audio sink.

   \todo Technically, handle could be 0 on success. Store an isopen bool that
      records if snd_pcm_open had success or not.

 *******************************************************************************/
#include "alsaout.h"

#include <utility>

#include "audio/samples.h"

AlsaOut::AlsaOut(std::string d)
	: handle(nullptr), out(nullptr), device(std::move(d) )
{
}

AlsaOut::~AlsaOut()
{
	stop();

	if ( handle != nullptr )
	{
		snd_pcm_close(handle);
	}
}

bool AlsaOut::open()
{
	int err = snd_pcm_open(&handle, device.c_str(), SND_PCM_STREAM_PLAYBACK, 0);

	if ( err != 0 )
	{
		handle = nullptr;
		return false;
	}

	/// \todo Error checking
	snd_pcm_hw_params_t* hw_params;
	unsigned int         rrate = 44100;

	snd_pcm_hw_params_malloc(&hw_params);
	snd_pcm_hw_params_any(handle, hw_params);

	snd_pcm_hw_params_set_access(handle, hw_params, SND_PCM_ACCESS_RW_INTERLEAVED);
	snd_pcm_hw_params_set_format(handle, hw_params, SND_PCM_FORMAT_S16_LE);
	snd_pcm_hw_params_set_rate_near(handle, hw_params, &rrate, nullptr);
	snd_pcm_hw_params_set_channels(handle, hw_params, 2);
	snd_pcm_hw_params_set_periods(handle, hw_params, 2, 0);
	snd_pcm_hw_params_set_buffer_size(handle, hw_params, 8192 * 2 / 4);

	snd_pcm_hw_params(handle, hw_params);
	snd_pcm_hw_params_free(hw_params);

	return true;
}

bool AlsaOut::isOpen()
{
	return handle != nullptr;
}

void AlsaOut::stop()
{
	if ( handle != nullptr )
	{
		snd_pcm_drop(handle);
	}
}

void AlsaOut::pause()
{
	if ( handle != nullptr )
	{
		snd_pcm_pause(handle, 1);
	}
}

void AlsaOut::resume()
{
	if ( handle != nullptr )
	{
		snd_pcm_pause(handle, 0);
	}
}

/**
 * \todo Better error handling
 * \todo handle all states
 * \todo Buffering is horrible
 */
void AlsaOut::write(const samples_type& data_)
{
	std::vector< std::int_least16_t > data = data_.data(); /// \todo Fix this usage

	if ( handle == nullptr || data.empty() )
	{
		return;
	}

	int               err = 0;
	snd_pcm_sframes_t nframes;

	std::size_t n = data.size();

	for (std::size_t i = 0; i < n; ++i)
	{
		outbuf.push_back(data[i]);
	}

	switch ( snd_pcm_state(handle) )
	{
	case SND_PCM_STATE_SETUP:
	case SND_PCM_STATE_XRUN:
		err = snd_pcm_prepare(handle);

		if ( err != 0 )
		{
			break;
		}

	case SND_PCM_STATE_PREPARED:
		err = snd_pcm_start(handle);

		if ( err != 0 )
		{
			break;
		}

	case SND_PCM_STATE_RUNNING:

		/// \todo Don't like the reference operator
		if ( outbuf.size() > 4096 )
		{
			nframes = snd_pcm_writei(handle, &outbuf[0], outbuf.size() / 2);

			if ( nframes >= 0 )
			{
				outbuf.erase(outbuf.begin(), outbuf.begin() + ( 2 * nframes ) );
			}
			else if ( nframes == -EPIPE )
			{
				snd_pcm_recover(handle, -EPIPE, 1);
				snd_pcm_writei(handle, &data[0], data.size() / 2);
			}
		}

		break;
	case SND_PCM_STATE_PAUSED:
		snd_pcm_pause(handle, 0);
		/// \todo goto SND_PCM_STATE_RUNNING
		nframes = snd_pcm_writei(handle, &data[0], data.size() / 2);
		break;
	case SND_PCM_STATE_OPEN:
	case SND_PCM_STATE_DRAINING:
	case SND_PCM_STATE_SUSPENDED:
	case SND_PCM_STATE_DISCONNECTED:
	default:
		break;
	} // switch

}
