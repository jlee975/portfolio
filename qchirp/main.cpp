/***************************************************************************/ /**
   \todo Display name of song
   \todo Close to system tray
   \todo Create playlist directory

   \todo I don't think "section global Global variables" is right for doxygen

   Some objects exist independent of the GUI.

   decoder  - should be start()-ed before any GUI is loaded
   reporter - should be start()-ed before any GUI is loaded
   catalog  - should be initialized from disk before any GUI is loaded. It should
        be written to disk after any GUI is unloaded. If the contents of
        the catalog change by user interaction, the GUI code should update
        catalog before it finishes.

 *******************************************************************************/
#include <fstream>
#include <iostream>
#include <string>

#include <QApplication>

#include "chapp/settings.h"
#include "qchirp.h"
#if ( defined( linux ) || defined( __linux ) )
#include "alsaout.h"
#endif

int qtui(int, char**);

bool     use_qt = true;     /// \todo Might want to change to an enum of UIs
Chirp    decoder;           // For audio decode/playback
Chirp    reporter;          // For finding meta data
Settings settings("chirp"); // Contains application-wide settings
Playlist catalog;           // Lists known media

int main(
	int    argc,
	char** argv
)
{
	/// \todo If Settings object _created_ the dir, then set defaults
	/// otherwise, read the values from the settings file
	settings.insert("dirs/catalog", settings.appdir() );
	settings.insert("dirs/playlists", settings.appdir() + "/playlists");

	// Initialize non-UI objects -------------------------------------------
	std::string catfile;
	{
		const auto v = settings["dirs/catalog"];

		if ( auto p = std::get_if< std::string >(&v) )
		{
			catfile = *p + "/catalog.cat";
		}
	}

	std::fstream cat;

	cat.open(catfile.c_str(), std::ios_base::in);

	if ( !cat.fail() )
	{
		catalog = Playlist::loadCAT(cat);
		cat.close();
	}

	decoder.start();
	reporter.start();

	// Switch to GUI -------------------------------------------------------
	int status = 0;

	if ( use_qt )
	{
		status = qtui(argc, argv);
	}

	// Clean up non-UI objects ---------------------------------------------
	decoder.quit(true);
	reporter.quit(true);
	decoder.join();
	reporter.join();

	cat.open(catfile.c_str(), std::ios_base::out | std::ios_base::trunc);

	if ( !cat.fail() )
	{
		cat << catalog.serialize(Playlist::CAT);
		cat.close();
	}
	else
	{
		// Need to create the chirp directory
	}

	return status;
}

/** \brief       Load the Qt user interface
 *
 * \todo Load playlists
 * \todo If nothing on command line, load last or new playlist
 */
int qtui(
	int    argc,
	char** argv
)
{
	QApplication app(argc, argv);
	QMainWindow  ui;

	ui.setWindowIcon(QIcon(":/icons/app") );
	ui.setWindowTitle("QChirp");

	auto* center = new QChirp(&ui);

	ui.setCentralWidget(center);
	ui.addToolBar(center->initControlBar() );
	center->buildStatusbar(ui.statusBar() );

	ui.resize(700, 400);
	ui.show();

	decoder.openoutput(new AlsaOut("default") );

	return app.exec();
}

/*
   First time program is run
   Create a directory for storing user files
   Create subdirectories for
    - playlists
    - catalog
 */
