/*=========================================================================*//**
   \class       QCatalogView
   \brief       Widget for viewing a collection of songs

   \todo Destructor
*//*==========================================================================*/
#include "qcatalogview.h"

#include <vector>

#include <dirent.h>   /// \todo Wrap in environment #ifs
#include <sys/stat.h> /// \todo Wrap in ifdef __linux__ or whatever

#include <QAction>
#include <QComboBox>
#include <QFileDialog>
#include <QHBoxLayout>
#include <QMenu>
#include <QToolButton>
#include <QTreeView>

#include "playlist/playlist.h"
#include "qcatalogmodel.h"

QCatalogView::QCatalogView(
	QWidget*        p,
	const Playlist& pl
)
	: QFrame(p), qcm(nullptr), treDir(nullptr)
{
	setFrameStyle(QFrame::StyledPanel | QFrame::Plain);

	qcm    = new QCatalogModel(this, pl);
	treDir = new QTreeView(this);
	treDir->setModel(qcm);
	treDir->setContextMenuPolicy(Qt::CustomContextMenu);
	connect(treDir, SIGNAL(customContextMenuRequested(const QPoint&)), SLOT(treContextMenu(const QPoint&)));

	auto* hlayout   = new QHBoxLayout;
	auto* colllist  = new QComboBox;
	auto* diraction = new QAction(QIcon(":/icons/scan"), tr("Scan"), this);

	connect(diraction, SIGNAL(triggered()), SLOT(scanPressed()));

	auto* dirbutton = new QToolButton(this);

	dirbutton->setAutoRaise(true);
	dirbutton->setDefaultAction(diraction);

	colllist->addItem(tr("Local Collection") );

	hlayout->addWidget(colllist);
	hlayout->addWidget(dirbutton);

	auto* vlayout = new QVBoxLayout(this);

	vlayout->addLayout(hlayout);
	vlayout->addWidget(treDir);
}

QCatalogView::~QCatalogView() = default;

Playlist QCatalogView::getplaylist() const
{
	return qcm->getplaylist();
}

void QCatalogView::treContextMenu(const QPoint& pt)
{
	auto* menu = new QMenu;

	menu->addAction(QIcon(":/icons/add"), tr("Add to playlist"), this, SLOT(addTreeItem()));
	menu->addAction(QIcon(":/icons/new"), tr("Replace playlist"), this, SLOT(replaceItems()));

	menu->exec(treDir->mapToGlobal(pt) );
}

/**
 * \todo We might have meta data => No need to bother with ensureMeta()
 * \bug QModelIndex's aren't necessarily the same as the model
 */
void QCatalogView::addTreeItem()
{
	QModelIndex idx = treDir->selectionModel()->currentIndex();

	if ( idx.isValid() )
	{
		std::vector< std::string > items;
		gettreeitems(items, idx);
		emit addtoplaylist(items, true);
	}
}

/**
 * \todo Same code (essentially) as addTreeItem
 */
void QCatalogView::replaceItems()
{
	QModelIndex idx = treDir->selectionModel()->currentIndex();

	if ( idx.isValid() )
	{
		std::vector< std::string > items;
		gettreeitems(items, idx);
		emit addtoplaylist(items, false);
	}
}

void QCatalogView::gettreeitems(
	std::vector< std::string >& items,
	const QModelIndex&          idx
)
{
	if ( !qcm->isleaf(idx) )
	{
		for (int i = 0, n = qcm->rowCount(idx); i < n; ++i)
		{
			gettreeitems(items, qcm->index(i, 0, idx) );
		}
	}
	else
	{
		std::string s = qcm->data(idx, Qt::UserRole).toString().toStdString();
		items.push_back(s);
	}
}

void QCatalogView::remove(const std::string& s)
{
	qcm->remove(s);
}

void QCatalogView::update(
	const std::string& s,
	const MetaData&    m
)
{
	qcm->update(s, m);
}

std::string QCatalogView::missingMeta() const
{
	return qcm->missingMeta();
}

bool QCatalogView::getmeta(
	const std::string& filename,
	MetaData&          m
) const
{
	return qcm->getmeta(filename, m);
}

/**
 * \todo Notify parent that meta should be updated
 */
void QCatalogView::scanPressed()
{
	QString dir = QFileDialog::getExistingDirectory(this, tr("Select a directory") );

	if ( !dir.isEmpty() )
	{
		findfiles(dir.toStdString() );
		emit needmeta();
	}
}

void QCatalogView::findfiles(const std::string& s)
{
	struct stat filestats;

	DIR* d = opendir(s.c_str() );

	if ( d == nullptr )
	{
		return;
	}

	dirent* de = nullptr;

	while ( ( de = readdir(d) ) != nullptr )
	{
		std::string name(de->d_name);

		if ( name == "." || name == ".." ) // head off an endless loop
		{
			continue;
		}

		std::string t = s + '/' + name;
		stat(t.c_str(), &filestats);

		if ( S_ISDIR(filestats.st_mode) )
		{
			findfiles(t);
		}
		else
		{
			qcm->addItem(t);
		}
	}

	closedir(d);
}
