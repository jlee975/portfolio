/***************************************************************************/ /**
   \class       QCatalogModel

   \todo Consider using currentitem instead of selectedIndexes
 *******************************************************************************/
#include "qcatalogmodel.h"

#include <stdexcept>
#include <string>

#include "playlist/playlist.h"
#include "qcatalogitem.h"

QCatalogModel::QCatalogModel(
	QObject*        par,
	const Playlist& pl
)
	: QAbstractItemModel(par), topItem(nullptr)
{
	topItem = new QCatalogItem(nullptr, QString(), std::string(), true);
	groupings.push_back(MetaData::Artist);
	groupings.push_back(MetaData::Album);

	for (std::size_t i = 0, n = pl.size(); i < n; ++i)
	{
		addItem(pl.filename(i), pl.meta(i) );
	}
}

QCatalogModel::~QCatalogModel()
{
	delete topItem;
}

int QCatalogModel::columnCount(const QModelIndex&) const
{
	return 1;
}

int QCatalogModel::rowCount(const QModelIndex& idx) const
{
	QCatalogItem* parentItem = nullptr;

	if ( idx.column() > 0 )
	{
		return 0;
	}

	if ( idx.isValid() )
	{
		parentItem = static_cast< QCatalogItem* >( idx.internalPointer() );
	}
	else
	{
		parentItem = topItem;
	}

	std::size_t i = parentItem->childCount();

	return i > INT_MAX ? INT_MAX : static_cast< int >( i );
}

QVariant QCatalogModel::data(
	const QModelIndex& idx,
	int                role
) const
{
	QVariant v;

	if ( !idx.isValid() )
	{
		// do nothing
	}
	else if ( role == Qt::DisplayRole )
	{
		v = static_cast< QCatalogItem* >( idx.internalPointer() )->label();
	}
	else if ( role == Qt::UserRole )
	{
		v = QString::fromStdString(static_cast< QCatalogItem* >( idx.internalPointer() )->data() );
	}

	return v;
}

QVariant QCatalogModel::headerData(
	int             section,
	Qt::Orientation orientation,
	int             role
) const
{
	QVariant d;

	if ( role == Qt::DisplayRole )
	{
		if ( orientation == Qt::Horizontal )
		{
			d = tr("Group");
		}
		else if ( orientation == Qt::Vertical )
		{
			d = ( section + 1 );
		}
	}

	return d;
}

QModelIndex QCatalogModel::index(
	int                row,
	int                column,
	const QModelIndex& idx // parent item
) const
{
	if ( !hasIndex(row, column, idx) )
	{
		return QModelIndex();
	}

	QCatalogItem* parentItem;

	if ( idx.isValid() )
	{
		parentItem = static_cast< QCatalogItem* >( idx.internalPointer() );
	}
	else
	{
		parentItem = topItem;
	}

	if ( QCatalogItem* childItem = parentItem->child(row) )
	{
		return createIndex(row, column, childItem);
	}
	else
	{
		return QModelIndex();
	}
}

QModelIndex QCatalogModel::parent(const QModelIndex& idx) const
{
	if ( idx.isValid() )
	{
		auto*         item = static_cast< QCatalogItem* >( idx.internalPointer() );
		QCatalogItem* p    = item->parent();

		if ( p != topItem )
		{
			std::size_t r = p->row();

			if ( r <= INT_MAX )
			{
				return createIndex(static_cast< int >( r ), 0, p);
			}
		}
	}

	return QModelIndex();
}

/**
 * \todo Don't add duplicates
 */
void QCatalogModel::addItem(const std::string& filename)
{
	// Find/create the correct parent and add the item in
	QCatalogItem* item = topItem;
	QModelIndex   idx  = QModelIndex();

	for (std::size_t i = 0, n = groupings.size(); i < n; ++i)
	{
		std::size_t m = item->childCount();

		if ( m == 0 || !item->child(0)->data().empty() )
		{
			// group not found
			beginInsertRows(idx, 0, 0);
			item->insertGroup(0, std::string() );
			endInsertRows();
		}

		item = item->child(0);
		idx  = index(0, 0, idx);
	}

	std::size_t r = item->childCount();

	if ( r <= INT_MAX )
	{
		beginInsertRows(idx, static_cast< int >( r ), static_cast< int >( r ) );
		new QCatalogItem(item, QString::fromStdString(filename), filename, false);
		endInsertRows();
	}
	else
	{
		new QCatalogItem(item, QString::fromStdString(filename), filename, false);
	}
}

/**
 * \todo Fix the INT_MAX restriction
 */
void QCatalogModel::addItem(
	const std::string& filename,
	const MetaData&    meta
)
{
	// Find/create the correct parent and add the item in
	QCatalogItem* item = topItem;
	QModelIndex   idx  = QModelIndex();

	for (const auto& grouping : groupings)
	{
		std::string s = meta[grouping];

		std::size_t j = 0;

		std::size_t m = item->childCount();

		while ( j < m && item->child(j)->data() < s )
		{
			++j;
		}

		if ( j == m || item->child(j)->data() != s )
		{
			// group not found
			if ( j <= INT_MAX )
			{
				int k = static_cast< int >( j );
				beginInsertRows(idx, k, k);
				item->insertGroup(j, s);
				endInsertRows();
			}
			else
			{
				item->insertGroup(j, s);
			}
		}

		item = item->child(j);

		if ( j <= INT_MAX )
		{
			idx = index(static_cast< int >( j ), 0, idx);
		}
		else
		{
			throw std::runtime_error("Qt cannot display this model");
		}
	}

	std::size_t k = item->childCount();

	if ( k <= INT_MAX )
	{
		beginInsertRows(idx, static_cast< int >( k ), static_cast< int >( k ) );
		auto* qci = new QCatalogItem(item, QString::fromStdString(meta[MetaData::Title]), filename, false);
		qci->setmeta(meta);
		endInsertRows();
	}
}

/**
 * \todo Could use beginMoveRows()
 * \todo Label should use a formatting std::string. Sort the actual insertion
 * \todo Case insensitive compare
 */
void QCatalogModel::update(
	const std::string& filename,
	const MetaData&    meta
)
{
	QModelIndex idx = find(QModelIndex(), filename);

	if ( idx.isValid() )
	{
		auto* item = static_cast< QCatalogItem* >( idx.internalPointer() );

		if ( ( item != nullptr ) && item->isworthupdating(meta) )
		{
			beginRemoveRows(idx.parent(), idx.row(), idx.row() );
			QCatalogItem* p = item->parent();
			p->erase(static_cast< std::size_t >( idx.row() ) );
			endRemoveRows();
			addItem(filename, meta);
		}
	}
}

bool QCatalogModel::isleaf(const QModelIndex& idx) const
{
	if ( idx.isValid() )
	{
		if ( auto* item = static_cast< QCatalogItem* >( idx.internalPointer() ) )
		{
			return item->isleaf();
		}
	}

	return false;
}

/**
 * \todo Funny calling ::index() when we could just create the index directly
 */
QModelIndex QCatalogModel::find(
	const QModelIndex& idx,
	const std::string& filename
) const
{
	if ( idx.isValid() )
	{
		auto* item = static_cast< QCatalogItem* >( idx.internalPointer() );

		if ( item == nullptr )
		{
			// do nothing
		}
		else if ( item->isleaf() )
		{
			if ( item->data() == filename )
			{
				return idx;
			}
		}
		else // node
		{
			std::size_t i = 0;

			std::size_t n = item->childCount();

			for (; i < n && i <= INT_MAX; ++i)
			{
				QModelIndex jdx = find(index(i, 0, idx), filename);

				if ( jdx.isValid() )
				{
					return jdx;
				}
			}
		}
	}
	else
	{
		std::size_t i = 0;

		std::size_t n = topItem->childCount();

		for (; i < n && i <= INT_MAX; ++i)
		{
			QModelIndex jdx = find(index(static_cast< int >( i ), 0, QModelIndex() ), filename);

			if ( jdx.isValid() )
			{
				return jdx;
			}
		}
	}

	return QModelIndex();
}

/**
 * \todo Recursive. Put a flag on a node to indicate if *any* child needs meta
 */
std::string QCatalogModel::missingMeta() const
{
	return test_missing(topItem);
}

std::string QCatalogModel::test_missing(QCatalogItem* item) const
{
	if ( item == nullptr )
	{
		// do nothing
	}
	else if ( !item->isleaf() )
	{
		for (std::size_t i = 0, n = item->childCount(); i < n; ++i)
		{
			std::string s = test_missing(item->child(i) );

			if ( !s.empty() )
			{
				return s;
			}
		}
	}
	else if ( item->needsmeta() )
	{
		return item->data();
	}

	return std::string();
}

/**
 * \todo Find it anywhere. Recursive.
 */
void QCatalogModel::remove(const std::string& s)
{
	if ( s.empty() )
	{
		return;
	}

	QModelIndex idx = find(QModelIndex(), s);

	if ( !idx.isValid() )
	{
		return;
	}

	if ( auto* item = static_cast< QCatalogItem* >( idx.internalPointer() ) )
	{
		if ( QCatalogItem* p = item->parent() )
		{
			beginRemoveRows(idx.parent(), idx.row(), idx.row() );
			p->erase(item->row() );
			endRemoveRows();
		}
	}
}

bool QCatalogModel::getmeta(
	const std::string& filename,
	MetaData&          m
) const
{
	QModelIndex idx = find(QModelIndex(), filename);

	if ( idx.isValid() )
	{
		auto* item = static_cast< QCatalogItem* >( idx.internalPointer() );

		if ( ( item != nullptr ) && !item->needsmeta() )
		{
			m = item->getmeta();
		}

		return true;
	}

	return false;
}

Playlist QCatalogModel::getplaylist() const
{
	Playlist pl;

	topItem->traverse(pl);
	return pl;
}
