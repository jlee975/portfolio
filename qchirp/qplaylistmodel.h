#ifndef QPLAYLIST_HPP
#define QPLAYLIST_HPP

#include <string>
#include <utility>
#include <vector>

#include <QAbstractTableModel>
#include <QModelIndex>
#include <QString>
#include <QVariant>

#include "chirp/metadata.h"
#include "playlist/playlist.h"

class QPlaylistModel
	: public QAbstractTableModel
{
public:
	explicit QPlaylistModel(QObject*           = nullptr);
	explicit QPlaylistModel(Playlist, QObject* = nullptr);

	// Qt functions
	int rowCount(const QModelIndex&) const override;
	int columnCount(const QModelIndex&) const override;
	QVariant data(const QModelIndex&, int) const override;
	QVariant headerData(int, Qt::Orientation, int) const override;
	void insertSongs(const std::vector< std::string >&);
	QModelIndex index(int, int, const QModelIndex&) const override;
	void sort(int, Qt::SortOrder) override;

	// Playlist accessors
	int rowfromfilename(const std::string&) const;
	void updateMeta(const std::string&, const MetaData&);
	void remove(const std::string&);
	void remove(const QModelIndexList&);
	void clear();
	std::string missingMeta() const;
	const Playlist& playlist() const;
private:
	void initHeader();

	Playlist                                             tabledata;
	std::vector< std::pair< MetaData::Field, QString > > headerinfo;
};

#endif // ifndef QPLAYLIST_HPP
