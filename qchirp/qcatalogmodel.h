#ifndef QCATALOG_HPP
#define QCATALOG_HPP

#include <vector>

#include <QAbstractItemModel>

#include "chirp/metadata.h"
class Playlist;

class QCatalogItem;

class QCatalogModel
	: public QAbstractItemModel
{
public:
	QCatalogModel(QObject*, const Playlist&);
	~QCatalogModel() override;

	int columnCount(const QModelIndex&) const override;
	int rowCount(const QModelIndex&) const override;
	QVariant data(const QModelIndex&, int) const override;
	QVariant headerData(int, Qt::Orientation, int) const override;
	QModelIndex index(int, int, const QModelIndex&) const override;
	QModelIndex parent(const QModelIndex&) const override;

	void addItem(const std::string&);
	void addItem(const std::string&, const MetaData&);
	Playlist getplaylist() const;
	bool isleaf(const QModelIndex&) const;
	void remove(const std::string&);
	void update(const std::string&, const MetaData&);
	std::string missingMeta() const;
	bool getmeta(const std::string&, MetaData&) const;
private:
	QModelIndex find(const QModelIndex&, const std::string&) const;
	std::string test_missing(QCatalogItem*) const;
	std::vector< MetaData::Field > groupings;
	QCatalogItem*                  topItem;
};

#endif // QCATALOG_HPP
