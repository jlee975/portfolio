/** @note If you make changes to this file, you should "rebuild all" because Qt creator
 *  doesn't see the change. It has to do with the templating, it seems. MAY HAVE
 *  BEEN FIXES AS OF 2.7.0
 */
#ifndef POLICY_HPP
#define POLICY_HPP

/// @brief Optimization selection
enum class Optimize
{
	Speed,     ///< Use the fastest code
	Precision, ///< Use the most robust code, closest to the true math
	Memory     ///< Use as little memory as possible
};

struct DefaultPolicy
{
	virtual ~DefaultPolicy() /* This is just here to silence a -Weffc warning */
	= default;

	static const bool render = true;
};

struct DebugPolicy
	: public DefaultPolicy
{
};

struct ProfilePolicy
	: public DefaultPolicy
{
	static const bool render = false;
};

using Policy = DefaultPolicy;
#endif // POLICY_HPP
