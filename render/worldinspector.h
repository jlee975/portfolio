#ifndef WORLDINSPECTOR_H
#define WORLDINSPECTOR_H

#include <condition_variable>
#include <mutex>

#include "tools/camera.h"

class WorldInspector
	: public Camera::Inspector
{
public:
	explicit WorldInspector(World& world_);

	void start();

	Scene view(std::chrono::nanoseconds t_) override;
private:
	World& world;

	mutable std::mutex              mut;
	mutable std::condition_variable cv;
};

#endif // WORLDINSPECTOR_H
