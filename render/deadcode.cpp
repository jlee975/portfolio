// Code that is no longer used, but may prove useful

#if 0
/// @todo Weird that in/out are embedded in 4x4 matrices. Flatten, or something
/// @todo 4 and higher. We don't need it now b/c we never call dir when nW == 4
template< typename R >
void adjugate(
	const matrix< R, 4, 4 >& M,
	std::size_t              nW,
	matrix< R, 4, 4 >&       A
)
{
	switch ( nW )
	{
	case 1:
		A(0, 0) = 1;
		break;
	case 2:
		A(0, 0) = M(1, 1);
		A(0, 1) = -M(0, 1);
		A(1, 0) = -M(1, 0);
		A(1, 1) = M(0, 0);
		break;
	case 3:
		A(0, 0) = M(1, 1) * M(2, 2) - M(1, 2) * M(2, 1);
		A(0, 1) = M(2, 0) * M(1, 2) - M(1, 0) * M(2, 2);
		A(0, 2) = M(1, 0) * M(2, 1) - M(2, 0) * M(1, 1);

		/*
		 * ** products are actually dot products, so no cancellation
		   (W[1] ** W[1]) * ((W[2] ** (W[2] - W[0])))
		   (W[2] ** W[1]) * ((W[2] ** (W[0] - W[1])))
		   (W[1] ** W[0]) * ((W[2] ** (W[1] - W[2])))
		 */

		// v.u * w.v - w.u * v.v
		// v.(w.v * u - w.u * v)
		// v.(w cross (u cross v)) // vector triple product identity

		// do same for all and sum... probably something like (u+v+w).(u cross v cross w)



		A(1, 0) = M(0, 2) * M(2, 1) - M(0, 1) * M(2, 2);
		A(1, 1) = M(0, 0) * M(2, 2) - M(0, 2) * M(2, 0);
		A(1, 2) = M(0, 1) * M(2, 0) - M(0, 0) * M(2, 1);
		A(2, 0) = M(0, 1) * M(1, 2) - M(0, 2) * M(1, 1);
		A(2, 1) = M(1, 0) * M(0, 2) - M(0, 0) * M(1, 2);
		A(2, 2) = M(0, 0) * M(1, 1) - M(0, 1) * M(1, 0);
		break;
	} // switch

}

#endif // if 0

#if 0
/**  Remove v from u. Orthoganalize u wrt v. Etc.
 *
 * This returns a vector w that is perpendicular to v and u = av+bw for some
 * scalars a,b. The returned vector is not guaranteed to be of any particular
 * length. Specifically, it is not guaranteed to be normal, or the altitute of
 * u (that is, b is not guaranteed to be 1 in the preceding).
 *
 * @todo In 3 dimensions, equal to vector triple product cross(v, cross(u, v))
 * @todo Document as applicable to any inner product space (?)
 * @todo Rename as vector rejection (vis a vis vector projection)
 *
 * Derivation
 *  Assume u = av + bw. Then u.v = a(v.v) + b(w.v) = a(v.v) => a = (u.v)/(v.v)
 *    => bw = u - v[(u.v)/(v.v)]
 *  Without the length restriction, we actually return [(v.v)b]w = (v.v)u-(u.v)v
 */
/*
   template< unsigned D, typename Real >
   point< D, Real > remove_projection(
    const point< D, Real >& u,
    const point< D, Real >& v
   )
   {
    return ( v * v ) * u - ( v * u ) * v;
   }
   //*/
// altitude(u,v) = (v.v-v.u)u+(u.u-u.v)v. Use v.norm2(), u.norm(), u.v twice

/*
   template< unsigned D, typename Real = float >
   struct simplex
   {
    typedef Real real_type;
    static const unsigned dimension = D;

    point< D, Real > v[D + 1];
   };
 */

// Find where line pq intersects the plane perpindicular to n and containing x
/// @todo Reuse the calculations from the collision test
geo::point< 3 > crossing(
	const geo::point< 3 >& p,
	const geo::point< 3 >& q,
	const geo::point< 3 >& n,
	const geo::point< 3 >& x
)
{
	// p, q, n, x
	return p + ( q - p ) * ( ( n * ( x - p ) ) / ( n * ( q - p ) ) );
}

void test_sec()
{
	std::vector< point2 > p;

	for (std::size_t i = 0; i < 100; ++i)
	{
		point2 q = {
			{ static_cast< float >( rand() ), static_cast< float >( rand() ) }};

		p.push_back(q);
	}

	const circle c = bounding_circle( &p[0], p.size() );

	for (std::size_t i = 0; i < p.size(); ++i)
	{
		if ( ( c.x - p[i].p[0] ) * ( c.y - p[i].p[1] ) > c.r * c.r )
		{
			std::cout << "ERROR" << std::endl;
		}
	}
}

#endif // if 0

/* Construct a rotation transformation from a quaternion
 * If this is redone, q should be guaranteed to be unit
   Affine::Affine(const quaternion& q) : origin_()
   {
    const real_type r = 2 / q.norm();

    data[0][0] = 1.f - r * ( q.Q[2] * q.Q[2] + q.Q[3] * q.Q[3] );
    data[0][1] = r * ( q.Q[1] * q.Q[2] - q.Q[0] * q.Q[3] );
    data[0][2] = r * ( q.Q[1] * q.Q[3] + q.Q[0] * q.Q[2] );
    data[1][0] = r * ( q.Q[1] * q.Q[2] + q.Q[0] * q.Q[3] );
    data[1][1] = 1.f - r * ( q.Q[1] * q.Q[1] + q.Q[3] * q.Q[3] );
    data[1][2] = r * ( q.Q[2] * q.Q[3] - q.Q[0] * q.Q[1] );
    data[2][0] = r * ( q.Q[1] * q.Q[3] - q.Q[0] * q.Q[2] );
    data[2][1] = r * ( q.Q[2] * q.Q[3] + q.Q[0] * q.Q[1] );
    data[2][2] = 1.f - r * ( q.Q[1] * q.Q[1] + q.Q[2] * q.Q[2] );
   }
 */

/// @todo Laderman algorithm 27 multiplies http://www.csd.uwo.ca/~mislam63/ms_thesis.pdf
/// Probably slow due to vectorization of naive algorithm. See https://stackoverflow.com/q/10827209
/// @todo Quaternion multiplication 18 multiplies http://physicsforgames.blogspot.ca/2010/03/quaternion-tricks.html

/* Laderman 3x3 matrix multiplication
    const real_type g1 = (b.data[0][2] + data[0][2] - data[1][2]) * (data[0][0] + b.data[2][0] - b.data[2][1] + b.data[2][2]);
    const real_type g2 = (b.data[0][1] + data[0][1] + data[1][1]) * (data[1][0] - b.data[1][0] + b.data[1][1] - b.data[1][2]);
    const real_type g3 = (b.data[0][1] + data[0][1] + data[2][1]) * (data[2][0] - b.data[1][0] + b.data[1][1] - b.data[1][2]);
    const real_type g4 = (b.data[0][2] - data[1][2] - data[2][2]) * (data[2][0] - b.data[2][0] + b.data[2][1] - b.data[2][2]);
    const real_type g5 = (b.data[0][0] - data[0][2] + data[1][2]) * data[0][0];
    const real_type g6 = (b.data[0][0] + data[0][1] + data[1][1]) * data[1][0];
    const real_type g7 = (b.data[0][0] + data[0][1] + data[2][1] + data[1][2] + data[2][2]) * data[2][0];
    const real_type g8 = b.data[0][1] * (data[0][0] + b.data[1][0] - b.data[1][1] + b.data[1][2]);
    const real_type g9 = b.data[0][2] * (data[1][0] + b.data[2][0] - b.data[2][1] + b.data[2][2]);
    const real_type g10 = data[0][1] * b.data[1][0];
    const real_type g11 = data[1][2] * b.data[2][0];
    const real_type g12 = (data[0][1] - data[1][2]) * (data[0][0] + b.data[2][0]);
    const real_type g13 = (data[0][1] + data[1][1]) * (b.data[1][0] - data[1][0]);
    const real_type g14 = (data[0][1] + b.data[0][1]) * (b.data[1][0] - b.data[1][1] + b.data[1][2]);
    const real_type g15 = data[1][1] * b.data[1][2];
    const real_type g16 = (b.data[0][2] - data[1][2]) * (b.data[2][0] - b.data[2][1] + b.data[2][2]);
    const real_type g17 = data[1][2] * b.data[2][1];
    const real_type g18 = (data[2][1] - data[1][2] - data[2][2]) * b.data[2][1];
    const real_type g19 = (data[0][2] + data[2][2] - data[0][1] - data[2][1]) * b.data[2][1];
    const real_type g20 = (data[0][1] + data[2][1]) * (b.data[1][0] - data[2][0] + b.data[1][2] + b.data[2][1]);
    const real_type g21 = (data[1][2] + data[2][2]) * (data[2][0] + b.data[1][2] - b.data[2][0] + b.data[2][1]);
    const real_type g22 = (data[1][2] + data[2][2] - data[0][1] - data[2][1]) * (b.data[1][2] + b.data[2][1]);

    c.data[0][0] = g5 + g10 + g11 + g12;
    c.data[0][1] = g8 + g10 - g14 + g17 - g18 + g19 - g22;
    c.data[0][2] = g1 - g11 - g12 - g16 + g17 - g18 + g19 - g22;
    c.data[1][0] = g6 - g10 + g11 + g13;
    c.data[1][1] = g2 - g10 + g13 + g14 + g15 + g17;
    c.data[1][2] = g9 - g11 + g15 - g16 + g17;
    c.data[2][0] = g7 - g10 - g11 + g20 - g21 + g22;
    c.data[2][1] = g3 - g10 + g14 - g17 + g18 + g20 + g22;
    c.data[2][2] = g4 + g11 + g16 - g17 + g18 + g21;

 */

/** @brief Construct an affine transformation from rotation
 *
 * Arguments given in radians
 *
 * @todo Simple rotation fixes D - 2 subspace. The other two dimensions rotate
 *    Ex., the origin is fixed in 2 dimensions, a line is fixed in 3
 *    dimensions, a plane is fixed in 4 dimensional rotations
 */
/*
   Affine(
    real_type dpitch, // rotate in YZ plane
    real_type dyaw,   // rotate in XZ plane
    real_type droll   // rotate in YX plane
   ) : Affine()
   {
    // rotate(dpitch, 1, 2)
    // rotate(dyaw, 2, 0)
    // rotate(droll, 0, 1)

    const real_type cx = std::cos(dpitch), sx = std::sin(dpitch);
    const real_type cy = std::cos(dyaw), sy = std::sin(dyaw);
    const real_type cz = std::cos(droll), sz = std::sin(droll);

    data[0][0] = cy * cz;
    data[0][1] = -cy * sz;
    data[0][2] = sy;
    data[1][0] = cz * sx * sy + cx * sz;
    data[1][1] = cx * cz - sx * sz * sy;
    data[1][2] = -cy * sx;
    data[2][0] = sx * sz - cx * cz * sy;
    data[2][1] = cz * sx + cx * sz * sy;
    data[2][2] = cx * cy;
   }
 */

#if 0
geo::point< 3 > center(
	const geo::point< 3 >* p,
	std::size_t            n
)
{
	if ( p && n != 0 )
	{
		geo::point< 3 > v = p[0];

		for (std::size_t i = 1; i < n; ++i)
		{
			v += p[i];
		}

		return v * static_cast< float >( 1. / static_cast< double >( n ) );
	}
	else
	{
		return geo::point< 3 >::ORIGIN;
	}
}

// Find the plane that best fits the 6 points
/// @todo D-dimensional
geo::plane< 3 > fit_plane(
	const geo::point< 3 >* points,
	std::size_t            num
)
{
	if ( !points || num < 3 )
	{
		throw std::invalid_argument("Need 3 or more points to fit a plane");
	}

	// center of mass
	const geo::point< 3 > v = center(points, num);

	/// @todo is a11..a33 the covariance matrix of a..f? Saw it refered to that way.
	float a11 = 0, a22 = 0, a33 = 0, a12 = 0, a13 = 0, a23 = 0;

	for (std::size_t i = 0; i < num; ++i)
	{
		const geo::point< 3 > q = points[i] - v;

		a11 += q[0] * q[0];
		a22 += q[1] * q[1];
		a33 += q[2] * q[2];
		a12 += q[0] * q[1];
		a13 += q[0] * q[2];
		a23 += q[1] * q[2];
	}

	/* Find the eigensystem of this (real, symmetric) matrix:
	   const float M[3][3] = {
	    { a11, a12, a13 },
	    { a12, a22, a23 },
	    { a13, a23, a33 }
	   };
	 */

	// characteristic polynomial is x^3 + c2 * x^2 + c1 * x + c0 = 0
	const float de = a12 * a23;
	const float dd = a12 * a12;
	const float ee = a23 * a23;
	const float ff = a13 * a13;
	const float m  = a11 + a22 + a33;
	const float c1 = a11 * a22 + a11 * a33 + a22 * a33 - ( dd + ee + ff );
	const float c0 = a11 * dd + a22 * ff - a11 * a22 * a33 - 2 * a13 * de;

	// solve the cubic
	const float p     = m * m - 3 * c1;
	const float q     = m * ( p - 1.5f * c1 ) - 13.5f * c0;
	const float sqrtp = std::sqrt( std::fabs(p) );

	const float u   = 27.f * ( ( .25f * c1 * c1 * ( p - c1 ) ) + ( c0 * ( q + ( 27.f / 4.f ) * c0 ) ) );
	const float phi = std::atan2(std::sqrt( std::fabs(u) ), q) / 3.f;

	// Constant
	const float M_SQRT3 = 1.73205080756887729352744634151f; // sqrt(3)

	const float cphi = sqrtp * std::cos(phi);
	/// phi always in [0, pi) so sphi >= 0
	const float sphi = ( 1.0f / M_SQRT3 ) * sqrtp * std::sin(phi);

	// Want the smallest eigenvalue, and it's corresponding eigenvector
	float lambda[3] = { cphi, ( m - cphi ) / 3.f, sphi };

	lambda[0] += lambda[1];
	lambda[2] += lambda[1];
	lambda[1] -= sphi;

	// lambda[1] <= lambda[2]

	// x + cos(phi)   can cos(phi) < -sin(phi)? Yes if phi > 3/4 pi
	// x - sin(phi)      sin(phi) > 0

	/// @todo lambda[1] always seems to be the smallest. Prove. Remove any
	/// unnecessary calculations. Ex. can we prove sphi >= 0 by showing
	/// phi in [0, pi). Or that cphi >= 0?

	const float l = std::min( lambda[0], std::min(lambda[1], lambda[2]) );

	geo::point< 3 > n = {
		{ a12* a23 - a13 * a22 + a13 * l,
		  a13* a12 - a23 * a11 + a23 * l,
		  ( a11 - l ) * ( a22 - l ) - a12 * a12 }};

	n = normalize(n);

	return geo::plane< 3 >{ -( v * n ), {{ n.p[0], n.p[1], n.p[2] }}}
}

#endif // if 0

/** bounding box is essentially 6 planes, perpindicular to the axes of the
    local affine transformation. So we translate the axes of a1 to a2
    and measure distances

    // forget about translation
    b1 = a1;
    b1.data[0][3] = b1.data[1][3] = b1.data[2][3] = 0;

    b2 = a2;
    b2.data[0][3] = b2.data[1][3] = b2.data[2][3] = 0;
    b2.transpose();

    // Directions of axes of a1 in global coords
    //	 a1     =>                  global
    // { 1, 0, 0 } => { { data[0][0], data[1][0], data[2][0] } };
    // { 0, 1, 0 } => { { data[0][1], data[1][1], data[2][1] } };
    // { 0, 0, 1 } => { { data[0][2], data[1][2], data[2][2] } };
    const point<3> X = b2(b1(1,0,0));  // normals to bounding planes
    const point<3> Y = b2(b1(0,1,0));
    const point<3> Z = b2(b1(0,0,1));

    P = a2.inverse(a1(0,0,0)); // translation of origin

    Q is in the bounding box if

        xmin * (X * X) + X * P <= X * Q <= xmax * (X * X) + X * P

        X { point<3>, float, float }

 */
/// @todo Rewrite as a member of Affine
/// @todo Improve speed and precision
#if 0
geo::OBB< float > transform_box(
	const geo::affine< 3, float >& t,
	const geo::box< 3 >&           bb
)
{
	const geo::point< 3 > P = t.origin();

	const geo::point< 3 > X = t(geo::ray< 3, float >{{ 1, 0, 0 }}).topoint(1);
	const geo::point< 3 > Y = t(geo::ray< 3, float >{{ 0, 1, 0 }}).topoint(1);
	const geo::point< 3 > Z = t(geo::ray< 3, float >{{ 0, 0, 1 }}).topoint(1);

	// the bounding box of *this in a2
	const geo::OBB< float > obb = { X, Y, Z,
									{{ X* ( P + X * bb.min[0] ), Y* ( P + Y * bb.min[1] ), Z* ( P + Z * bb.min[2] ) }},
									{{ X* ( P + X * bb.max[0] ), Y* ( P + Y * bb.max[1] ), Z* ( P + Z * bb.max[2] ) }}};

	return obb;
}

#endif // if 0

/// @todo Can probably generalize to be like Body: an object with an affine transform
/// Decorator for extremum of a body under transformation
/** @todo Wrapper for moving object. Form convex hull of moving object as
 *     M = { vertices at t0 } union { vertices at (t0 + dt) }
 *    Practically, extremum(d) will calculate max { d * v } for v in M. But
 *    we can fake the vertices from the (t0 + dt) set. Suppose v in M, speed s
 *    v = v0, or v = v0 + dt * s for some v0 in the original object. We need
 *    to calculate d * v0 and d * (v0 + dt * s) = d * v0 + dt * s * v for all
 *    v0. Since dt * s * v is constant, the max is O(1) harder for a moving
 *    object than a still object
 */

/**
 * @brief TriangleMesh::collision
 * @param that
 * @param a1
 * @param a2
 * @return
 * The "first" member of the return is true iff there was a collision. The
 * "second" member of the return is a collision plane if "first" was true, and
 * a separating plane if not. If the separating plane is not valid, then the
 * objects did not collide, but no plane was found that separates the two
 * meshes.
 *
 * @todo Precision: Perfect testing of box-triangle intersection
 * @todo Precision: Calculate plane of collision using all points
 * @todo Precision: Determine if two or more impacts between same objects
 * @todo Clip more than one face at a time
 * @todo Faster clipping
 * @todo Faster intersection testing
 * @todo Do not test faces that cannot intersect
 * @todo Remember calculations/separating faces from previous frames
 * @todo Do not copy as much data
 * @todo Visit faces in a way that reduces loading of vertices
 * @todo Do not transform vertices. Work the transformation into the test
 * @todo Cache awareness. Store vertices in a way that they are grouped by
 *    faces. Store faces so that there is a common edge between one face and
 *    the next. Orient faces. When moving from one face to the next, check if
 *    we are reusing points, and keep the relevant calculations
 * @todo Classify faces as separating or not by whether whole object lies in
 *    half space defined by face. Then use property to whittle down the test
 *    list
 * @todo Comparing two separating faces. For each separating face, find the
 *    projection of all points onto the plane of the separating face. Find the
 *    bounding circle of the projected points. Store the center and radius. When
 *    comparing two separating faces that are front facing, if the two circles
 *    are disjoing, the objects are disjoint. Determining the circles are
 *    disjoint can be done by finding the line of intersection of the two planes
 *    containing the faces, then testing the distrance from that line to the
 *    circle centers. If either is far enough away, the circles do not intersect.
 * @todo Bounding circle: http://www.personal.kent.edu/~rmuhamma/Compgeometry/MyCG/CG-Applets/Center/centercli.htm
 * @todo A version of collision that will try a hint first, if it exists
 */
/** @todo TO improve separability, bucket sort thatclip based on how close
 *    the normal is to (this->center - that->center). The more "front
 *    facing" a face is, the more likely it will separate
 */
#if 0
intersection_result< 3 > TriangleMesh::collision(
	const TriangleMesh&            that,
	const geo::affine< 3, float >& a1,
	const geo::affine< 3, float >& a2
) const
{
	#if 0

	// Brute force. Test every face against every face
	for (std::size_t i = 0, n = faces.size(); i < n; ++i)
	{
		// Let abc and def be the two triangles
		const point< 3 > a = a1(vertices[faces[i].u]);
		const point< 3 > b = a1(vertices[faces[i].v]);
		const point< 3 > c = a1(vertices[faces[i].w]);

		for (std::size_t j = 0, m = t.faces.size(); j < m; ++j)
		{
			const point< 3 >& d = a2(t.vertices[t.faces[j].u]);
			const point< 3 >& e = a2(t.vertices[t.faces[j].v]);
			const point< 3 >& f = a2(t.vertices[t.faces[j].w]);

			if ( triangle_triangle_intersect(a, b, c, d, e, f) )
			{
				const plane_t p = fit_plane(a, b, c, d, e, f);

				return p;
			}
		}
	}

	#elif 1
	typedef geo::triangle< 3 > triangle_t;

	const geo::affine< 3, float > t    = a1.to(a2);
	const geo::affine< 3, float > tinv = a2.to(a1);

	// Clip that to the bounding box of this
	std::forward_list< triangle_t > thatclip;

	{
		// Use bounding boxes to perform clipping
		const geo::OBB obb = transform_box(t, bb);

		for (std::size_t i = 0, n = that.faces.size(); i < n; ++i)
		{
			const triangle_t tri = {
				{ that.vertices[that.faces[i][0]],
				  that.vertices[that.faces[i][1]],
				  that.vertices[that.faces[i][2]] }};

			if ( geo::test_intersect(obb, tri) )
			{
				thatclip.emplace_front( tinv(tri) );
			}
		}
	}

	if ( !thatclip.empty() )
	{
		const geo::OBB obb = transform_box(tinv, that.bb);

		// for each face in *this, check for intersection with something from "tclip"
		for (std::size_t i = 0, n = faces.size(); i < n; ++i)
		{
			// clip face of this to bounding box of "t"
			const triangle_t face = {
				{ vertices[faces[i][0]], vertices[faces[i][1]], vertices[faces[i][2]] }};

			if ( geo::test_intersect(obb, face) )
			{
				for (const triangle_t& tri : thatclip)
				{
					// Do the two triangles intersect?
					if ( test_intersect(face, tri) )
					{
						/// @todo list initialization
						const geo::point< 3 > p[6] = { face[0], face[1], face[2], tri[0], tri[1], tri[2] };

						return intersection_result< 3 >{ a1( fit_plane(p, 6) ) };
					}
				}
			}
		}
	}

	#endif // if 0

	return intersection_result< 3 >{ geo::plane< 3 >::INVALID };
}

#endif // if 0

#if 0
/// @todo Seems like this could be called recursively, and specialized for nW == 1, 2
template< dimension_type D, typename R >
void orthonormalize(
	const point< D, R >* W,
	dimension_type       nW,
	point< D, double >*  X
)
{
	for (dimension_type i = 0; i < nW; ++i)
	{
		point< D, double > x = -W[0].template embed< double >();

		if ( i + 1 < nW )
		{
			x += W[i + 1].template embed< double >();
		}

		for (dimension_type j = 0; j < i; ++j)
		{
			x -= ( X[j] * x ) * X[j];
		}

		X[i] = normalize(x);
	}
}

/// @todo May be cheaper if we don't normalize
template< dimension_type D, typename R >
void orthogonalize(
	const point< D, R >* W,
	dimension_type       nW,
	point< D, double >*  X
)
{
	orthonormalize(W, nW, X);
}

#endif // if 0

/** Find the vector in the null space of the simplex that contains O
 *
 * @throws runtime_error if empty() is true
 *
 * This function finds a vector perpendicular to the simplex and "in the
 * direction of" O.
 *
 * Formally, we think of the simplex as defining an affine subspace. The
 * vector that we are after, then, extends this affine subspace to include
 * the origin O (thus, making it a linear subspace).
 *
 * The function finds this vector by treating this as an orthogonalization.
 * We translate the affine space to a linear one by subtracting W[0] from
 * each vertex of the simplex. We also do the same to the origin. Then, we
 * want to orthogonalize the linear subspace, and finally orthogonalize
 * O-W[0] to the simplex. This is done using a modified Gram-Schmidt
 * process.
 *
 * @todo Householder reflections for improved stability
 * @todo Define the behavior when O is in the simplex
 * @todo Extend to any arbitrary point.
 * @todo Template to provide specialization for 3 dimensions, where we can
 *    use cross product for nW = Triangle in particular
 * @todo The matrix we orthogonalize looks suspiciously like the barycentric
 *    transformation matrix.
 * @todo Closest point to origin will be unique
 * @todo Equivalent to finding a vector in null space
 */
#if 0
template< dimension_type D, typename R >
ray< D, R > dir(
	const point< D, R >* W,
	dimension_type       nW
)
{
	if ( nW == 0 )
	{
		throw std::runtime_error("Operation is not defined for an empty simplex");
	}

	#if 1
	// Doubles are needed for numeric stability. Floats are abyssmal
	point< D, double > X[D + 1];
	orthogonalize(W, nW, X);

	const ray< D, R > u = make_ray< D, double, R >(X[nW - 1]);

	return u;

	#if 0
	// Adjugate method
	/// @todo Has problems when W is linearly dependent --------------------

	float M[4][4];

	for (dimension_type i = 0; i < nW; ++i)
	{
		for (dimension_type j = i; j < nW; ++j)
		{
			const float t = W[i] * W[j];
			M[i][j] = t;
			M[j][i] = t;
		}
	}

	float lambda[4];

	/// @todo We can calculate lambda[i] explicitly from W by simply backsubstitution through A and M.
	/// May be able to simplify or cancel out some terms.
	float A[4][4];
	adjugate(M, nW, A);

	for (dimension_type i = 0; i < nW; ++i)
	{
		float x = 0;

		for (dimension_type j = 0; j < nW; ++j)
		{
			x += A[i][j];
		}

		lambda[i] = x;
	}

	point< 3 > u2 = {{ 0, 0, 0 }};

	for (dimension_type i = 0; i < nW; ++i)
	{
		u2 += lambda[i] * W[i];
	}

	u2 = normalize(u2);
	point< 3 > u_ = {{ u.p[0], u.p[1], u.p[2] }};

	float y = ( u_ + u2 ).length();

	if ( y > 1e-5f )
	{
		std::cout << "---" << std::endl;

		for (dimension_type i = 0; i < nW; ++i)
		{
			std::cout << ( W[i] * u2 ) << " " << ( W[i] * u_ ) << std::endl;
		}
	}

	return u;

	#endif // if 0

	#else

	#endif // if 0
}

#endif // if 0
