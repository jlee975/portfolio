#include "glwindow.h"

#include <cmath>
#include <fstream>
#include <iostream>

#include "math/geometry/geometry.h"
#include "physics/light.h"
#include "tools/surface.h"

const float RADIANS_PER_DEGREE = 0.00872664625997164788461845384f;

GLWindow::GLWindow()
	: camera(1)
{
}

GLWindow::~GLWindow()
{
	destroy_vertex_shader();
}

void GLWindow::setTextures(const Gallery* t)
{
	gallery = t;
}

GLuint GLWindow::lookup_texture(texture_id i)
{
	auto it = textures.find(i);

	if ( it != textures.end() )
	{
		return it->second;
	}

	if ( gallery != nullptr )
	{
		const auto image = gallery->getBitmap(i, RGB8);

		// use the texture library to load the texture
		GLuint tx[1];

		glGenTextures(1, tx);

		glBindTexture(GL_TEXTURE_2D, tx[0]);
		glTexImage2D(GL_TEXTURE_2D, 0, 3, static_cast< GLsizei >( image.width() ),
			static_cast< GLsizei >( image.height() ), 0, GL_RGB, GL_UNSIGNED_BYTE, image.get() );
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glBindTexture(GL_TEXTURE_2D, 0);

		textures[i] = tx[0];
		return tx[0];
	}
	else
	{
		return 0;
	}
}

void GLWindow::adjustPerspective(
	float fovy,
	float aspect,
	float zNear,
	float zFar
)
{
	const float ymax = zNear * std::tan(fovy * RADIANS_PER_DEGREE);
	const float ymin = -ymax;
	const float xmin = ymin * aspect;
	const float xmax = ymax * aspect;

	glFrustum(xmin, xmax, ymin, ymax, zNear, zFar);
}

// general OpenGL initialization function
bool GLWindow::init(
	unsigned w_,
	unsigned h_,
	unsigned bitdepth
)
{
	glEnable(GL_TEXTURE_2D);
	glShadeModel(GL_SMOOTH);
	glClearColor(0.2f, 1.0f, 0.2f, 0.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	resize(w_, h_);
	glEnable(GL_LIGHTING);
	glFlush();
	depth_ = bitdepth;

	return true;
}

// function called when our window is resized (should only happen in window mode)
void GLWindow::resize(
	unsigned int w_,
	unsigned int h_
)
{
	if ( w_ != width_ || h_ != height_ )
	{
		const float aspect = ( h_ != 0 ? ( static_cast< float >( w_ ) / static_cast< float >( h_ ) ) : 1.f );

		GLint oldmatrixmode;

		// Reset The Current Viewport And Perspective Transformation
		glViewport(0, 0, w_, h_);

		glGetIntegerv(GL_MATRIX_MODE, &oldmatrixmode);
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		adjustPerspective(45.0f, aspect, 0.1f, 200.0f);
		glMatrixMode(oldmatrixmode);

		width_  = w_;
		height_ = h_;
	}
}

void GLWindow::beginScene(const geo::affine< 3, float >& pov)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);

	camera = pov;

	// Turn off all lights
	for (unsigned long i = 0; i < ilight; ++i)
	{
		glDisable(static_cast< GLenum >( GL_LIGHT0 + i ) );
	}

	ilight = 0;
}

void GLWindow::endScene()
{
	update();
}

/// @todo Use Light to pass colour information
void GLWindow::addLight(const Light& l)
{
	float m[16];

	camera.copy_col_major(m);
	glLoadMatrixf(m);

	float bright_white[4] = { 10.f, 10.f, 10.f, 1.0f };
	float black[4]        = { 0, 0, 0, 1 };

	// add light
	glLightfv(static_cast< GLenum >( GL_LIGHT0 + ilight ), GL_DIFFUSE, black);
	glLightfv(static_cast< GLenum >( GL_LIGHT0 + ilight ), GL_SPECULAR, black);
	glLightfv(static_cast< GLenum >( GL_LIGHT0 + ilight ), GL_POSITION, l.pos);
	glLightfv(static_cast< GLenum >( GL_LIGHT0 + ilight ), GL_AMBIENT, bright_white);
	glEnable(static_cast< GLenum >( GL_LIGHT0 + ilight ) );

	++ilight;
}

void GLWindow::draw(
	const geo::Geometry&           geom,
	const geo::affine< 3, float >& s,
	const Surface&                 m
)
{
	float m2[16];

	camera.copy_col_major(m2);

	glLoadMatrixf(m2);

	float m1[16];

	s.copy_col_major(m1);
	glMultMatrixf(m1);
	draw(geom, m);
}

void GLWindow::draw(const geo::Geometry& t)
{
	glBegin(GL_TRIANGLES);

	for (std::size_t i = 0, n = t.num_faces(); i < n; ++i)
	{
		const auto& face = t.face(i);
		glVertex3fv(t.vertex(face.vertices[0]).raw() );
		glVertex3fv(t.vertex(face.vertices[1]).raw() );
		glVertex3fv(t.vertex(face.vertices[2]).raw() );
	}

	glEnd();
}

void GLWindow::draw(
	const geo::Geometry& g,
	const Surface&       s
)
{
	const auto tid = s.getTextureId();

	if ( tid != INVALID_TEXTURE )
	{
		glBindTexture(GL_TEXTURE_2D, lookup_texture(tid) );
	}

	// Draw an icosahedron
	glBegin(GL_TRIANGLES);

	for (std::size_t i = 0, n = g.num_faces(); i < n; ++i)
	{
		/// @todo Just one call to get the face
		const auto&       f = g.face(i);
		const std::size_t u = f.vertices[0];
		const std::size_t v = f.vertices[1];
		const std::size_t w = f.vertices[2];
		glTexCoord2fv(s.getTexture(u).raw() );
		glVertex3fv(g.vertex(u).raw() );

		glTexCoord2fv(s.getTexture(v).raw() );
		glVertex3fv(g.vertex(v).raw() );

		glTexCoord2fv(s.getTexture(w).raw() );
		glVertex3fv(g.vertex(w).raw() );
	}

	glEnd();

	glBindTexture(GL_TEXTURE_2D, 0);
}

void GLWindow::destroy_vertex_shader()
{
	/// @bug Should be detaching and deleting the shader
	glDeleteProgram(glsl_program);
}

void GLWindow::load_vertex_shader(const std::string& glsl)
{
	std::ifstream vsfile(glsl.c_str() );

	if ( !vsfile.fail() )
	{
		vsfile.seekg(0, std::ios_base::end);

		const std::size_t         n = static_cast< std::size_t >( std::streamsize(std::streamoff(vsfile.tellg() ) ) );
		std::unique_ptr< char[] > p(new char [n + 1]);
		vsfile.seekg(0, std::ios_base::beg);
		vsfile.read(p.get(), n);
		p[n] = '\0';
		const char* q[] = { p.get() };

		GLuint prog = glCreateProgram();

		if ( prog != 0 )
		{
			GLuint vertexshader = glCreateShader(GL_VERTEX_SHADER);

			if ( vertexshader != 0 )
			{
				glShaderSource(vertexshader, 1, q, nullptr);
				glCompileShader(vertexshader);

				glAttachShader(prog, vertexshader);
				glLinkProgram(prog);
				glValidateProgram(prog);

				GLint stat;
				glGetProgramiv(prog, GL_VALIDATE_STATUS, &stat);

				if ( stat == GL_TRUE )
				{
					glUseProgram(prog);

					if ( glsl_program != 0 )
					{
						destroy_vertex_shader();
					}

					glsl_program = prog;
					return;
				}
			}

			glDeleteProgram(prog);
		}
	}
}
