#include "worldinspector.h"

WorldInspector::WorldInspector(World& world_)
	: world(world_)
{
}

void WorldInspector::start()
{
	while ( true )
	{
		std::unique_lock< std::mutex > lk(mut);

		world.step();

		lk.unlock();
		cv.notify_one();
	}
}

Scene WorldInspector::view(std::chrono::nanoseconds t_)
{
	/// @todo Only need a shared mutex on the information
	std::unique_lock< std::mutex > lk(mut);

	cv.wait(lk, [&](){ return !world.ready(t_); });

	return world.view(t_);
}
