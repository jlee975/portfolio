#ifndef CAMERA_HPP
#define CAMERA_HPP

#include <atomic>
#include <forward_list>
#include <unordered_map>

#include "keyboard.h"
#include "physics/body.h"
#include "physics/world.h"
#include "platform.h"
#include "surface.h"

class Gallery;

/** @todo Split class for user controlled camera and automatic camera
 */
class Camera
{
public:
	/** An object that allows the camera to acquire a scene
	 */
	class Inspector
	{
public:
		virtual ~Inspector()                         = default;
		virtual Scene view(std::chrono::nanoseconds) = 0;
	};

	Camera()                         = default;
	Camera(const Camera&)            = delete;
	Camera(Camera&&)                 = delete;
	Camera& operator=(const Camera&) = delete;
	Camera& operator=(Camera&&)      = delete;

	void start(Platform& fe, const Gallery& te, Inspector&, std::unordered_map< std::size_t, std::shared_ptr< Surface > >);
private:
	Affine move(Key::State) const;

	// Key mappings
	Key::Key key_quit       = Key::Esc;
	Key::Key key_up         = Key::R;
	Key::Key key_down       = Key::F;
	Key::Key key_left       = Key::A;
	Key::Key key_right      = Key::D;
	Key::Key key_forward    = Key::W;
	Key::Key key_backward   = Key::S;
	Key::Key key_look_left  = Key::Left;
	Key::Key key_look_right = Key::Right;
	Key::Key key_look_up    = Key::Up;
	Key::Key key_look_down  = Key::Down;
	Key::Key key_look_cw    = Key::Q;
	Key::Key key_look_ccw   = Key::E;
	Key::Key key_fs         = Key::F1;

	float step        = .5;
	float delta_yaw   = .07f;
	float delta_pitch = .07f;
	float delta_roll  = .07f;
};

#endif // CAMERA_HPP
