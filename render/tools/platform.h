#ifndef PLATFORM_H
#define PLATFORM_H

class View;
class Keyboard;

class Platform
{
public:
	virtual ~Platform() = default;

	virtual bool ready()              = 0;
	virtual View* switch_window()     = 0;
	virtual View* switch_fullscreen() = 0;
	virtual Keyboard* get_keyboard()  = 0;
};

#endif // PLATFORM_H
