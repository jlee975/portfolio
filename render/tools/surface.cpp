#include "surface.h"

Surface::Surface()
	: textureid(INVALID_TEXTURE)
{
}

Surface::Surface(
	std::vector< uvcoord_t > uv_,
	texture_id               x
)
	: uv(std::move(uv_) ), usetexture(true), textureid(x)
{
}

texture_id Surface::getTextureId() const
{
	return textureid;
}

const uvcoord_t& Surface::getTexture(std::size_t i) const
{
	return uv[i];
}
