#ifndef KEYBOARD_H
#define KEYBOARD_H

/// @todo enum class
namespace Key
{
using State = unsigned long long;

enum Key
{
	Invalid,
	A,
	B,
	C,
	D,
	E,
	F,
	G,
	H,
	I,
	J,
	K,
	L,
	M,
	N,
	O,
	P,
	Q,
	R,
	S,
	T,
	U,
	V,
	W,
	X,
	Y,
	Z,
	Left,
	Right,
	Up,
	Down,
	Esc,
	Space,
	PageUp,
	PageDown,
	Home,
	End,
	Insert,
	Delete,
	F1,
	F2,
	F3,
	F4,
	F5,
	F6,
	F7,
	F8,
	F9,
	F10,
	F11,
	F12,
	DIGIT0,
	DIGIT1,
	DIGIT2,
	DIGIT3,
	DIGIT4,
	DIGIT5,
	DIGIT6,
	DIGIT7,
	DIGIT8,
	DIGIT9
};
}

class Keyboard
{
public:
	virtual ~Keyboard() = default;

	virtual Key::State getKeyState() const = 0;
};

#endif // KEYBOARD_H
