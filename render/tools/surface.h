#ifndef SURFACE_HPP
#define SURFACE_HPP

#include "chill/gallery.h"
#include "uvcoord.h"

class Surface
{
public:
	Surface();
	Surface(std::vector< uvcoord_t > uv_, texture_id x);

	texture_id getTextureId() const;
	const uvcoord_t& getTexture(std::size_t) const;
private:
	std::vector< uvcoord_t > uv;
	bool                     usetexture{ false };
	texture_id               textureid;
};

#endif // SURFACE_HPP00000000
