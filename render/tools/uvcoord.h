#ifndef UVCOORD_HPP
#define UVCOORD_HPP

#include "math/geometry/primitives/point.h"

using uvcoord_t = geo::point< 2 >; // Typically for textures

#endif // UVCOORD_HPP
