#include "camera.h"

#include <chrono>
#include <iostream>

#include "view.h"

namespace
{
bool key_pressed(
	Key::State s,
	Key::Key   k
)
{
	return ( ( s >> k ) & 1 ) != 0;
}

int key_movement(
	Key::State keys,
	Key::Key   k1,
	Key::Key   k2
)
{
	return ( key_pressed(keys, k1) ? 1 : 0 ) - ( key_pressed(keys, k2) ? 1 : 0 );
}

}

// import physical models every 1 in FRAME_RATE calls to update
/** @todo The frame_rate needs considerable tweaking and theory behind it. "By feel" it
 * seems to be fine, though, and dramatically changes how much they physics
 * loop spends updating its observers (i.e., this camera). The value should be
 * adjusted so the display is never "very old". But should strike a balance of
 * not importing everything (which is very costly, when most will simply be
 * thrown away)
 */

/// @todo pov could be *generated* as inverse, rather than calculating the inverse later
void Camera::start(
	Platform&                                                     factory,
	const Gallery&                                                gallery,
	Inspector&                                                    world,
	std::unordered_map< std::size_t, std::shared_ptr< Surface > > models
)
{
	const auto t0 = std::chrono::steady_clock::now();

	Keyboard* kbd        = factory.get_keyboard();
	View*     canvas     = factory.switch_window();
	bool      fullscreen = false;

	Affine pov(1);

	canvas->setTextures(&gallery);

	Key::State keys = 0;

	unsigned long long steptime = 0;

	// wait for system
	while ( factory.ready() )
	{
		// Event handling ======================================================
		// Update physics based on previous key state

		const Key::State oldkeystate = keys;
		keys = kbd->getKeyState();

		if ( key_pressed(keys, key_quit) )
		{
			break;
		}

		/// @bug don't think this is exactly what I want. Esp. considering scaling
		pov *= move(keys);

		// check if the FS key was released
		if ( key_pressed(oldkeystate, key_fs) && !key_pressed(keys, key_fs) )
		{
			if ( fullscreen )
			{
				canvas     = factory.switch_window();
				fullscreen = false;
			}
			else
			{
				canvas     = factory.switch_fullscreen();
				fullscreen = true;
			}

			canvas->setTextures(&gallery);
		}

		// draw ================================================================
		const auto t = std::chrono::steady_clock::now() - t0;

		auto curr = world.view(std::chrono::nanoseconds(steptime) );

		/// @bug Go back to real time
		steptime += 1000000000 / 60;
		canvas->beginScene(pov.inverse() );

		// Load lighting
		for (auto& it : curr.lights)
		{
			canvas->addLight(it);
		}

		// Draw polygons
		for (auto& it : curr.bodies)
		{
			const auto jt = models.find(it.second);

			/// @todo If no model, maybe we should draw with a solid colour. Invisible objects
			/// should be explicitly setup with a special invisible Surface
			if ( jt != models.end() )
			{
				canvas->draw(it.first.getGeometry(), it.first.getFrame(), *( jt->second ) );
			}
		}

		canvas->endScene();
	}

	delete canvas;
}

Affine Camera::move(Key::State keys) const
{
	const int dpitch = key_movement(keys, key_look_up, key_look_down);
	const int dyaw   = key_movement(keys, key_look_left, key_look_right);
	const int droll  = key_movement(keys, key_look_cw, key_look_ccw);
	const int dx     = key_movement(keys, key_right, key_left);
	const int dy     = key_movement(keys, key_up, key_down);
	const int dz     = key_movement(keys, key_backward, key_forward);

	Affine b(1);

	rotate(b, static_cast< float >( dpitch ) * delta_pitch, static_cast< float >( dyaw ) * delta_yaw,
		static_cast< float >( droll ) * delta_roll);

	b.translate(step * static_cast< float >( dx ), step * static_cast< float >( dy ), step * static_cast< float >( dz ) );

	return b;
}
