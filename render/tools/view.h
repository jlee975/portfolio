#ifndef VIEW_H
#define VIEW_H

/// @todo "math/geometry/fwd.h"
#include "math/geometry/affine.h"
#include "math/geometry/geometry.h"
class Surface;
struct Light;
class Gallery;

class View
{
public:
	virtual ~View() = default;

	virtual void setTextures(const Gallery*)                                                = 0;
	virtual void addLight(const Light&)                                                     = 0;
	virtual void draw(const geo::Geometry&, const geo::affine< 3, float >&, const Surface&) = 0;
	virtual void draw(const geo::Geometry&)                                                 = 0;
	virtual void draw(const geo::Geometry&, const Surface&)                                 = 0;
	virtual void beginScene(const geo::affine< 3, float >&)                                 = 0;
	virtual void endScene()                                                                 = 0;
};

#endif // VIEW_H
