/**
 * @todo The texture loader uses Image and Layer classes -- which are
 *       completely unnecessary for this application
 * @todo I'd rather like to be able to refer to textures via a path like
 *    "/wood/oak" or "metal/aluminum"
 * @todo Should be able to load a bunch of textures from a zip
 * @todo Don't load until needed by operator[]
 * @todo Clean up the Singleton implementation
 * @todo shouldn't be responsible for delete-ing bitmap
 */
#include "glxfactory.h"

#include <cstring>
#include <iostream>
#include <stdexcept>

#include <X11/extensions/xf86vmode.h>
#include <X11/keysym.h>
#include <X11/keysymdef.h>

#include <GL/gl.h>
#include <GL/glext.h>
#include <GL/glxext.h>

#include "glwindow.h"

namespace
{
class RenderCommon
	: public GLWindow
{
public:
	RenderCommon(
		Display*   d,
		GLXContext c
	)
		: display_(d), context_(c), window_(0)
	{
	}
protected:
	Display*   display_;
	GLXContext context_;
	Window     window_;

	void update() override
	{
		glXSwapBuffers(display_, window_);
	}

	void change_window(GLXContext c)
	{
		glXMakeCurrent(display_, window_, c);

		XWindowAttributes xwa;

		XGetWindowAttributes(display_, window_, &xwa);

		GLWindow::init(xwa.width, xwa.height, xwa.depth);
	}

};

class RenderWindow
	: public RenderCommon
{
public:
	RenderWindow(
		Display*     d,
		GLXContext   c,
		XVisualInfo* vi,
		Colormap     colourmap,
		unsigned     winw,
		unsigned     winh,
		std::string  s
	)
		: RenderCommon(d, c), title_(std::move(s) )
	{
		XSetWindowAttributes mattr;

		mattr.colormap     = colourmap;
		mattr.border_pixel = 0;
		mattr.event_mask   = KeyPressMask | KeyReleaseMask | ButtonPressMask | StructureNotifyMask;

		window_ = XCreateWindow(display_, RootWindow(display_, vi->screen), 0, 0, winw, winh, 0, vi->depth,
			InputOutput, vi->visual, CWBorderPixel | CWColormap | CWEventMask, &mattr);

		// only set window title and handle wm_delete_events if in windowed mode
		Atom wmDelete = XInternAtom(display_, "WM_DELETE_WINDOW", True);

		XSetWMProtocols(display_, window_, &wmDelete, 1);

		XSetStandardProperties(display_, window_, title_.c_str(), title_.c_str(), None, nullptr, 0, nullptr);
		XMapRaised(display_, window_);

		// Move the GL context to the new menu
		change_window(context_);
	}

	~RenderWindow() override
	{
		glXMakeCurrent(display_, None, nullptr);

		XDestroyWindow(display_, window_);
	}
private:
	std::string title_;
};

class RenderFullscreen
	: public RenderCommon
{
public:
	RenderFullscreen(
		Display*     d,
		GLXContext   c,
		XVisualInfo* vi,
		Colormap     colourmap,
		unsigned     fsw,
		unsigned     fsh
	)
		: RenderCommon(d, c), modes(nullptr), nmodes(0), mdeskMode()
	{
		const int screen = DefaultScreen(display_);

		// save desktop-resolution before switching modes
		XF86VidModeGetAllModeLines(display_, screen, &nmodes, &modes);
		mdeskMode = *modes[0];

		// look for mode with requested resolution
		int bestMode = 0;

		for (int i = 0; i < nmodes; i++)
		{
			if ( ( modes[i]->hdisplay == fsw ) && ( modes[i]->vdisplay == fsh ) )
			{
				bestMode = i;
				break;
			}
		}

		XF86VidModeSwitchToMode(display_, screen, modes[bestMode]);
		XF86VidModeSetViewPort(display_, screen, 0, 0);

		const std::pair< int, int > dims_(modes[bestMode]->hdisplay, modes[bestMode]->vdisplay);

		XSetWindowAttributes mattr;

		mattr.colormap          = colourmap;
		mattr.border_pixel      = 0;
		mattr.override_redirect = True;
		mattr.event_mask        = KeyPressMask | KeyReleaseMask | ButtonPressMask | StructureNotifyMask;

		window_ = XCreateWindow(display_, RootWindow(display_, vi->screen), 0, 0, dims_.first, dims_.second, 0,
			vi->depth, InputOutput, vi->visual,
			CWBorderPixel | CWColormap | CWEventMask | CWOverrideRedirect, &mattr);

		XWarpPointer(display_, None, window_, 0, 0, 0, 0, 0, 0);
		XMapRaised(display_, window_);
		XGrabKeyboard(display_, window_, True, GrabModeAsync, GrabModeAsync, CurrentTime);
		XGrabPointer(display_, window_, True, ButtonPressMask, GrabModeAsync, GrabModeAsync, window_, None,
			CurrentTime);

		// Move the GL context to the new menu
		change_window(context_);
	}

	~RenderFullscreen() override
	{
		glXMakeCurrent(display_, None, nullptr);

		XDestroyWindow(display_, window_);

		// restore original desktop resolution
		const int screen = DefaultScreen(display_);

		XF86VidModeSwitchToMode(display_, screen, &mdeskMode);
		XF86VidModeSetViewPort(display_, screen, 0, 0);

		XFree(modes);
	}
private:
	XF86VidModeModeInfo** modes;
	int                   nmodes;
	XF86VidModeModeInfo   mdeskMode;
};

/* attributes for a single buffered visual in RGBA format with at least
 * 4 bits per color and a 16 bit depth buffer */
int attrListSgl[] = { GLX_RGBA, GLX_RED_SIZE, 4, GLX_GREEN_SIZE, 4, GLX_BLUE_SIZE, 4, GLX_DEPTH_SIZE, 16, None };

/* attributes for a double buffered visual in RGBA format with at least
 * 4 bits per color and a 16 bit depth buffer */
int attrListDbl[] = { GLX_DOUBLEBUFFER, GLX_RGBA, GLX_RED_SIZE, 4, GLX_GREEN_SIZE, 4, GLX_BLUE_SIZE, 4,
	                  GLX_DEPTH_SIZE, 16, None };

class XKeyboard
	: public Keyboard
{
public:
	XKeyboard() = default;

	Key::State getKeyState() const override
	{
		return keystate;
	}

	void press(XKeyEvent* e)
	{
		keystate |= UINTMAX_C(1) << convert_key(e);
	}

	void release(XKeyEvent* e)
	{
		keystate &= ~( UINTMAX_C(1) << convert_key(e) );
	}
private:
	static Key::Key convert_key(XKeyEvent* ev)
	{
		switch ( XLookupKeysym(ev, 0) )
		{
		case XK_Escape:
			return Key::Esc;

		case XK_A:
		case XK_a:
			return Key::A;

		case XK_D:
		case XK_d:
			return Key::D;

		case XK_E:
		case XK_e:
			return Key::E;

		case XK_F:
		case XK_f:
			return Key::F;

		case XK_Q:
		case XK_q:
			return Key::Q;

		case XK_R:
		case XK_r:
			return Key::R;

		case XK_S:
		case XK_s:
			return Key::S;

		case XK_W:
		case XK_w:
			return Key::W;

		case XK_Left:
			return Key::Left;

		case XK_Right:
			return Key::Right;

		case XK_Up:
			return Key::Up;

		case XK_Down:
			return Key::Down;

		case XK_F1:
			return Key::F1;

		case XK_F2:
			return Key::F2;

		case XK_F3:
			return Key::F3;

		case XK_F4:
			return Key::F4;

		case XK_F5:
			return Key::F5;

		case XK_F6:
			return Key::F6;

		case XK_F7:
			return Key::F7;

		case XK_F8:
			return Key::F8;

		case XK_F9:
			return Key::F9;

		case XK_KP_0:
			return Key::DIGIT0;

		case XK_KP_1:
			return Key::DIGIT1;

		case XK_KP_2:
			return Key::DIGIT2;

		case XK_KP_3:
			return Key::DIGIT3;

		case XK_KP_4:
			return Key::DIGIT4;

		case XK_KP_5:
			return Key::DIGIT5;

		case XK_KP_6:
			return Key::DIGIT6;

		case XK_KP_7:
			return Key::DIGIT7;

		case XK_KP_8:
			return Key::DIGIT8;

		case XK_KP_9:
			return Key::DIGIT9;

		default:
			break;
		} // switch

		return Key::Invalid;
	}

	Key::State keystate{ 0 };
};
}

GLXFactory::GLXFactory()
	: keyboard(new XKeyboard)
{
	// get a connection
	display_ = XOpenDisplay(nullptr);

	if ( display_ == nullptr )
	{
		throw std::runtime_error("No connection to X display");
	}

	const int screen = DefaultScreen(display_);

	vi = glXChooseVisual(display_, screen, attrListDbl);

	if ( vi == nullptr )
	{
		vi = glXChooseVisual(display_, screen, attrListSgl);

		if ( vi == nullptr )
		{
			throw std::runtime_error("Cannot choose visual");
		}
	}

	// create a GLX context
	context_ = glXCreateContext(display_, vi, nullptr, GL_TRUE);

	if ( context_ == nullptr )
	{
		throw std::runtime_error("Cannot acquire GLX context");
	}

	// create a color map
	colourmap = XCreateColormap(display_, RootWindow(display_, vi->screen), vi->visual, AllocNone);
}

GLXFactory::~GLXFactory()
{
	delete keyboard;
	XFreeColormap(display_, colourmap);
	glXDestroyContext(display_, context_);
	XCloseDisplay(display_);
}

View* GLXFactory::switch_window()
{
	/// @bug When leaving fullscreen, multi-screens were not restored properly. All
	/// displays were unified
	delete current;
	current = new RenderWindow(display_, context_, vi, colourmap, winw, winh, "OpenGL Test");
	return current;
}

View* GLXFactory::switch_fullscreen()
{
	delete current;
	current = new RenderFullscreen(display_, context_, vi, colourmap, winw, winh);
	return current;
}

Keyboard* GLXFactory::get_keyboard()
{
	return keyboard;
}

bool GLXFactory::ready()
{
	bool quit = false;

	while ( XPending(display_) > 0 )
	{
		XEvent event;
		XNextEvent(display_, &event);

		switch ( event.type )
		{
		case ConfigureNotify:

			if ( current != nullptr )
			{
				current->resize(event.xconfigure.width, event.xconfigure.height);
			}

			break;
		case ClientMessage:

			/// @todo Should actually be checking the xclient.data member for WM_DELETE_WINDOW
			if ( strcmp(XGetAtomName(event.xclient.display, event.xclient.message_type), "WM_PROTOCOLS")
			     == 0
			)
			{
				quit = true;
			}

			break;
		case KeyRelease:

			if ( XEventsQueued(display_, QueuedAfterReading) != 0 )
			{
				XEvent nev;
				XPeekEvent(display_, &nev);

				// check if this is an autorepeat sequence
				if ( ( nev.type == KeyPress ) && ( nev.xkey.time == event.xkey.time )
				     && ( nev.xkey.keycode == event.xkey.keycode )
                                                                   )
				{
					// delete event
					XNextEvent(display_, &nev);
					break;
				}
			}

			// key has been released, clear the corresponding bit
			static_cast< XKeyboard* >( keyboard )->release(&event.xkey);
			break;

		case KeyPress:
			// key is down, set the corresponding bit
			static_cast< XKeyboard* >( keyboard )->press(&event.xkey);
			break;

		default:
			break;
		} // switch

	}

	return !quit;
}

Platform& getPlatform()
{
	static GLXFactory f;

	return f;
}
