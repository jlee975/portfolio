/* Rewrite ---------------------------------------------------------------------
   UI Thread
   Rendering (because it has to be done in the main thread)
   Synchronization, monotonic clock, adjusted for pauses
     Predict next frame time
     Tell physic engine to move to that time
     Physics will forward positions to camera
     Camera will forward triangles to here
     Rendering happens
   Forward keypresses etc to camera and physics threads
   Preemptively load data for part of a level, based on position
   rolocks on data
   ID -> Textures
      -> Materials
   Resources Thread
   Manage geometry, textures, materials
     Return placeholders for data that isn't loaded yet
   rwlocks on data
   Physics Thread
   Runs up to 1s ahead determining next collision events
   Moving, collision detection, etc.
   Object creation
   rolocks on data
   ID -> last position
      -> current position
      -> True geometry
   Camera Threads
   Occlusion culling, etc.
   Preps a scene for the render thread
   rolocks on data
   ID -> prev position
      -> LOD geometry
 */

/**
 * @todo Throughout, define an intermediate<Real> type, which is a larger width
 * floating point type to use in intermediate calculations, for precision. Ex.,
 * float -> double where it is beneficial. Alternatively, most functions could have
 * a second Real template parameter which may be used for intermediate calculations
 * @todo Arrange program as a lib that we are using. After all, we want to
 *   develop an API that can be used in several different ways
 * @todo Exceptions in exceptions/ directory
 * @todo Move geometric primitives into a directory
 * @todo Collision detection and response
 * @todo Memory management all around. As little duplication as possible
 * @todo Physics: velocity, acceleration, center of mass. Objects should not
 *       have a single "current" affine transformation, but an origin, velocity,
 *       and rotational velocity. The "current" transform should be calculated
 *       from the last time the vel/acc were changed.
 * @todo HUD
 * @todo Redo image handling library
 * @todo Object picking (ray intersection with viewed object)
 * @todo Textures are reflected the opposite of what they should be
 * @todo Multiple views w/ tab switching
 * @todo Parametric surfaces
 * @todo Environment mapping
 * @todo Lighting
 * @todo Normals
 * @todo Reflection
 * @todo Material properties
 * @todo Skeletons
 * @todo vertex morphing
 * @todo Fast primitive types including SIMD (even if we need to pad); packed point-s;
 *       approximating floating point with 32-bit fixed point; rotations as 1/256th of 2pi;
 * @todo Use tagged dispatch to call faster variations of functions. Ex., if we know a vector
 *  is unit, or a matrix is orthogonal, we can avoid some cost and mark it explicitly
 * @todo Use builtin_sqrt, etc. or other "fast" versions of functions when we know the input/output
 * will be well defined.
 * @todo normalize may be replaced with scaling in some cases, which could be faster
 */
#include <condition_variable>
#include <mutex>
#include <thread>
#include <unordered_map>

#include "chill/gallery.h"
#include "physics/world.h"
#include "policy.h"
#include "predicates.h"
#include "tools/camera.h"

#include "worldinspector.h"

// From glxfactory.cpp
/// @attention The user is responsible for implementing this function
Platform& getPlatform();

std::tuple< World, std::unordered_map< std::size_t, std::shared_ptr< Surface > > > make_world(Gallery& textures)
{
	World world;

	#if 1
	/// @todo Not super happy about the initialization, or using affines instead of planes
	const Affine::initializer boundaries[] = {
		{{{ 1, 0, 0 }, { 0, 1, 0 }, { 0, 0, 1 }}, 1, { 60, 0, 0 }},
		{{{ -1, 0, 0 }, { 0, -1, 0 }, { 0, 0, 1 }}, 1, { -60, 0, 0 }},
		{{{ 0, 1, 0 }, { -1, 0, 0 }, { 0, 0, 1 }}, 1, { 0, -60, 0 }},
		{{{ 0, -1, 0 }, { 1, 0, 0 }, { 0, 0, 1 }}, 1, { 0, -60, 0 }},
		{{{ 0, 0, 1 }, { 0, 1, 0 }, { -1, 0, 0 }}, 1, { 0, 0, 60 }},
		{{{ 0, 0, -1 }, { 0, 1, 0 }, { 1, 0, 0 }}, 1, { 0, 0, -60 }}, };

	std::unordered_map< std::size_t, std::shared_ptr< Surface > > models;

	{
		auto g = std::make_shared< geo::Geometry >(geo::Plane);
		auto m = std::make_shared< Surface >();

		for (const auto& a : boundaries)
		{
			Body b(0, g);
			b.setFrame(a);
			b.setMass(INFINITY);
			world.addBody(std::move(b) );
		}
	}

	// Create some basic models to be in the scene
	/// @todo Use IDs like textures so that we don't have whole models being copied around. Or
	/// a shared_ptr (perhaps only for debug builds)
	auto g1      = std::make_shared< geo::Geometry >(geo::Sphere);
	auto earth   = std::make_shared< Surface >(g1->getSphericalCoords(), textures.load("earth3.png") );
	auto g2      = std::make_shared< geo::Geometry >(geo::Icosahedron);
	auto jupiter = std::make_shared< Surface >(g2->getSphericalCoords(), textures.load("jupiter.png") );

	// Create a bunch of objects and add them to the scene
	for (int i = 0; i < 2; ++i)
	{
		for (int j = 0; j < 2; ++j)
		{
			Body   b(0, g2);
			Affine f(1);
			f.translate(-10.0f * i, 10.0f * j, -10.0f);
			b.setVelocity(i, j, i + j);
			b.setFrame(f);

			const std::size_t k = world.addBody(std::move(b) );
			models.emplace(k, earth);
		}
	}

	/*
	   {
	    Body   b(0, jupiter);
	    Affine f(1);
	    f.translate(geo::point< 3 >{ 5.f, -5.f, -18.f });
	    b.setFrame(f);
	    world.addBody(b);
	   }
	 */
	#else
	{
		auto earth = std::make_shared< const Surface >(std::make_shared< Geometry >(Sphere),
			textures.load("earth3.png"), Surface::Spherical);

		{
			Affine frame(1);
			frame.translate(geo::point< 3, float >{ 0, 0, -20 });
			Body b(0, earth);
			b.setFrame(frame);
			b.setVelocity(0, 0, 0);
			world.addBody(std::move(b) );
		}
	}
	#endif // if 1

	// Add lights
	world.addLight(Light{{ 0.f, 0.f, -10.0f, 1.f }});

	return { std::move(world), std::move(models) };
}

/**
 * @brief Creates the objects in the world and runs the physics loop
 * @param camera An optional view of the objects in the scene
 * @param textures Access to the library of textures
 *
 * @todo Thread-safe access to "textures"
 * @todo If null camera, don't load textures
 */
int main()
{
	// Load textures so they can be used in game
	Gallery textures = { "/home/jonathan/source/textures/" };

	auto [world, models] = make_world(textures);

	Camera camera;

	WorldInspector inspector(world);

	// start the physics thread and notify the draw thread of updates
	std::thread physics(&WorldInspector::start, std::ref(inspector) );

	// start user interface and drawing
	camera.start(getPlatform(), textures, inspector, std::move(models) );

	// user has exited -- cleanup resources, wind-down threads
	physics.join();

	return EXIT_SUCCESS;
}
