#ifndef GLFACTORY_H
#define GLFACTORY_H

#include <X11/Xlib.h>
#include <X11/Xutil.h>

#define GL_GLEXT_PROTOTYPES
#include <GL/glx.h>

#include "tools/keyboard.h"
#include "tools/platform.h"
#include "tools/view.h"

class GLWindow;

/** @brief An abstract factory for creating a render window and texture manager
 *
 * @todo Provide a method to get renderer by number of dimensions (2d or 3d)
 */
class GLXFactory
	: public Platform
{
public:
	GLXFactory();
	~GLXFactory() override;

	View* switch_window() override;
	View* switch_fullscreen() override;
	bool ready() override;
	Keyboard* get_keyboard() override;
private:
	Display*     display_{ nullptr };
	GLXContext   context_{ nullptr };
	XVisualInfo* vi{ nullptr };
	Colormap     colourmap{};
	GLWindow*    current{ nullptr };

	bool fullscreen_{ false };
	bool mapped_{ false };

	// windowed mode position and dimensions
	int winw{ 640 };
	int winh{ 480 };

	Keyboard* keyboard;

	GLXFactory(const GLXFactory&)            = delete;
	GLXFactory& operator=(const GLXFactory&) = delete;
};

#endif // GLFACTORY_H
