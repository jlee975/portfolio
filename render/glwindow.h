#ifndef GLWINDOW_HPP
#define GLWINDOW_HPP

#include <string>
#include <unordered_map>
#include <vector>

#define GL_GLEXT_PROTOTYPES
#include <GL/gl.h>

#include "chill/gallery.h"
#include "math/geometry/affine.h"
#include "math/geometry/geometry.h"
#include "tools/view.h"

class GLWindow
	: public View
{
public:
	GLWindow();

	~GLWindow() override;

	void resize(unsigned int, unsigned int);

	void setTextures(const Gallery*) override;
	void addLight(const Light&) override;
	void beginScene(const geo::affine< 3, float >&) override;
	void draw(const geo::Geometry&, const geo::affine< 3, float >&, const Surface&) override;
	void draw(const geo::Geometry&, const Surface&) override;
	void draw(const geo::Geometry&) override;
	void endScene() override;
protected:
	void load_vertex_shader(const std::string&);
	bool init(unsigned, unsigned, unsigned);
	virtual void update() = 0;
private:
	unsigned int width_{ 0 };
	unsigned int height_{ 0 };
	unsigned int depth_{};

	GLuint                                   glsl_program{ 0 };
	geo::affine< 3, float >                  camera;
	unsigned long                            ilight{ 0 };
	const Gallery*                           gallery{ nullptr };
	std::unordered_map< texture_id, GLuint > textures;

	void adjustPerspective(float, float, float, float);
	void destroy_vertex_shader();
	GLuint lookup_texture(texture_id);

	GLWindow(const GLWindow&)            = delete;
	GLWindow& operator=(const GLWindow&) = delete;
};

#endif // ifndef GLWINDOW_HPP
