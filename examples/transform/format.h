#ifndef FORMAT_H
#define FORMAT_H

#include <iosfwd>

enum class format
{
	invalid,
	base64,
	gzip,
	crc,
	json
};

std::ostream& operator<<(std::ostream&, format);

#endif // FORMAT_H
