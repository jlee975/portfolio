#ifndef OPERATION_H
#define OPERATION_H

#include <iosfwd>

enum class operation
{
	none,
	to,
	from,
	verify,
	prettify
};

std::ostream& operator<<(std::ostream&, operation);

#endif // OPERATION_H
