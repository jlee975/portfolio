#include "operation.h"

#include <iostream>

std::ostream& operator<<(
	std::ostream& os,
	operation     op
)
{
	switch ( op )
	{
	case operation::none:
		return os << "none";

	case operation::to:
		return os << "to";

	case operation::from:
		return os << "from";

	case operation::verify:
		return os << "verify";

	case operation::prettify:
		return os << "prettify";
	}

	return os;
}
