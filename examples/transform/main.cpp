#include <cstdlib>
#include <fstream>
#include <iostream> // TESTSUITE
#include <map>
#include <memory>
#include <string>
#include <tuple>
#include <vector>

/// @todo Output to files instead of cout
/// @todo base64: variations like which characters to use for 62 and 63; line length
/// @todo Common interface for ex., base64 and deflate
/// @todo --from auto which would use the algorithm appropriate for the file extension

#include "base64/base64.h"
#include "crc/crc.h"
#include "gzip/gzip.h"
#include "memorymap/mmap.h"
#include "parse/ebnf.h"
#include "parse/parse.h"
#include "json/json.h"

#include "format.h"
#include "operation.h"

// Usage: app --to base64 file.txt

bool transform_file_verify(
	const std::string& filename,
	const std::byte*   filedata,
	std::size_t        filesize,
	format             fmt
)
{
	if ( fmt == format::json )
	{
		if ( jl::json::validate(filedata, filesize) )
		{
			std::cout << filename << " is JSON" << std::endl;
		}
		else
		{
			std::cout << filename << " is not JSON" << std::endl;
		}

		return true;
	}

	return false;
}

bool transform_file_prettify(
	const std::string&,
	const std::byte* filedata,
	std::size_t      filesize,
	format           fmt
)
{
	if ( fmt == format::json )
	{
		return jl::json::prettify(filedata, filesize);
	}

	return false;
}

bool transform_file_from(
	const std::string&,
	const std::byte* filedata,
	std::size_t      filesize,
	format           fmt
)
{
	if ( fmt == format::gzip )
	{
		GZip mygzip;

		const auto e = mygzip.extract(filedata, filesize);

		if ( e.ae == archive_error::none )
		{
			const auto& v = e.dataz;

			std::cout.write(reinterpret_cast< const char* >( v.data() ), v.size() );
			return true;
		}

		return false;
	}

	return false;
}

bool transform_file_to(
	const std::string& filename,
	const std::byte*   filedata,
	std::size_t        filesize,
	format             fmt
)
{
	if ( fmt == format::base64 )
	{
		// Do the base64 transform
		const std::size_t n  = 4 * ( ( filesize + 2 ) / 3 );
		const auto        s  = std::make_unique< char[] >(n);
		const std::size_t ns = Base64Encode(filedata, filesize, s.get(), n);

		// Print the base64 string, 70 columns wide
		std::cout << "=BEGIN: " << filename << std::endl;

		const std::size_t linelen = 70;

		for (std::size_t kk = 0; kk < ns; kk += linelen)
		{
			for (std::size_t jj = 0; jj < linelen; jj++)
			{
				if ( jj + kk < ns )
				{
					putchar(s[jj + kk]);
				}
			}
		}

		std::cout << std::endl;
		return true;
	}

	if ( fmt == format::crc )
	{
		const auto c = chest::crc32(filedata, filesize, chest::crc::ogg, chest::crc::flags::none);
		std::cout << c << '\t' << filename << std::endl;
		return true;
	}

	return false;
}

bool transform_file(
	const std::string& filename,
	operation          op,
	format             fmt
)
{
	memory_map m(filename);

	if ( m.is_open() )
	{
		switch ( op )
		{
		case operation::to:
			return transform_file_to(filename, m.data(), m.size(), fmt);

		case operation::from:
			return transform_file_from(filename, m.data(), m.size(), fmt);

		case operation::verify:
			return transform_file_verify(filename, m.data(), m.size(), fmt);

		case operation::prettify:
			return transform_file_prettify(filename, m.data(), m.size(), fmt);

		default:
			return false;
		}
	}
	else
	{
		std::ifstream f(filename);

		if ( !f.is_open() )
		{
			std::cerr << "ERROR (open) [" << filename << "]" << std::endl;
			return false;
		}

		if ( !f.seekg(0, std::ios_base::end) )
		{
			std::cerr << "ERROR (seek) [" << filename << "]" << std::endl;
			return false;
		}

		const auto filesize = f.tellg();

		if ( filesize == std::ifstream::pos_type(-1) )
		{
			std::cerr << "ERROR (tell) [" << filename << "]" << std::endl;
			return false;
		}

		if ( !f.seekg(0) )
		{
			std::cerr << "ERROR (seek) [" << filename << "]" << std::endl;
			return false;
		}

		// Allocate memory for the binary and encoded data
		const auto filedata = std::make_unique< std::byte[] >(filesize);

		// Read the file into memory
		if ( !f.read(reinterpret_cast< char* >( filedata.get() ), filesize) )
		{
			// Clean up the memory we used
			std::cerr << "ERROR (read) [" << filename << "]" << std::endl;
			return false;
		}

		if ( f.gcount() != filesize )
		{
			std::cerr << "Error (read) [" << filename << "]" << std::endl;
			return false;
		}

		switch ( op )
		{
		case operation::to:
			return transform_file_to(filename, filedata.get(), filesize, fmt);

		case operation::from:
			return transform_file_from(filename, filedata.get(), filesize, fmt);

		default:
			return false;
		}
	}
}

int main(
	int   argc,
	char* argv[]
)
{
	const std::map< std::string, operation > ops
	    = {{ "-t", operation::to }, { "--to", operation::to },
		{ "-f", operation::from }, { "--from", operation::from },
		{ "-p", operation::prettify }, { "--prettify", operation::prettify },
		{ "-v", operation::verify }, { "--verify", operation::verify }};

	const std::map< operation, std::map< std::string, format > > zz
	    = {{ operation::to, {{ "base64", format::base64 }, { "crc", format::crc }}},
		{ operation::from, {{ "gzip", format::gzip }}},
		{ operation::prettify, {{ "json", format::json }}},
		{ operation::verify, {{ "json", format::json }}}};

	std::vector< std::tuple< std::string, operation, format > > files;

	{
		operation op  = operation::none;
		format    fmt = format::invalid;

		for (int i = 1; i < argc;)
		{
			auto it = ops.find(argv[i]);

			if ( it != ops.end() )
			{
				if ( i + 1 < argc )
				{
					op  = it->second;
					fmt = format::invalid;

					auto jt = zz.find(op);

					if ( jt != zz.end() )
					{
						auto kt = jt->second.find(argv[i + 1]);

						if ( kt != jt->second.end() )
						{
							fmt = kt->second;
						}
					}

					i += 2;
				}
				else
				{
					std::cerr << "Missing argument to " << argv[i] << std::endl;
					return EXIT_FAILURE;
				}
			}
			else
			{
				// A file
				if ( op != operation::none )
				{
					files.emplace_back(argv[i], op, fmt);
					++i;
				}
				else
				{
					std::cerr << "Missing conversion for '" << argv[i] << "'" << std::endl;
					return EXIT_FAILURE;
				}
			}
		}
	}

	for (const auto& [filename, op, fmt] : files)
	{
		if ( !transform_file(filename, op, fmt) )
		{
			std::cerr << "Conversion of '" << filename << "' " << op << " '" << fmt << "' failed"
			          << std::endl;
		}
	}

	return EXIT_SUCCESS;
}
