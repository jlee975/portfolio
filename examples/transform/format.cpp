#include "format.h"

#include <iostream>

std::ostream& operator<<(
	std::ostream& os,
	format        fmt
)
{
	switch ( fmt )
	{
	case format::invalid:
		return os << "invalid";

	case format::base64:
		return os << "base64";

	case format::gzip:
		return os << "gzip";

	case format::crc:
		return os << "crc";

	case format::json:
		return os << "json";
	}

	return os;
}
