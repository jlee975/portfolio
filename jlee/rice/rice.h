#ifndef RICE_RICE_H
#define RICE_RICE_H

#include "bitstream/bebitstream.h"

IBEBitstream::bits::word_type rice0(IBEBitstream&, IBEBitstream::bits::length_type);

#endif
