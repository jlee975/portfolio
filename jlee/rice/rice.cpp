#include "rice.h"

#include <stdexcept>

IBEBitstream::bits::word_type rice0(
	IBEBitstream&                   bs,
	IBEBitstream::bits::length_type n
)
{
	unsigned long t = 0;

	auto w = bs.peek();

	while ( w.value == 0 )
	{
		if ( w.length == 0 )
		{
			throw std::runtime_error("Not enough bits in stream");
		}

		t += w.length;
		bs.discard(w.length);
		w = bs.peek();
	}

	const auto s = w.nlz();

	bs.discard(s + 1);

	return ( ( s + t ) << n ) + bs.read(n);
}
