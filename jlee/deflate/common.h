#ifndef COMMON_H
#define COMMON_H

#include <array>

class HuffmanTable;

namespace deflate
{
enum BlockType
{
	no_compression = 0,
	fixed_huffman = 1,
	dynamic_huffman = 2,
	reserved = 3
};

extern const HuffmanTable fixed_llbook;
extern const HuffmanTable fixed_distbook;

const std::array< unsigned, 19 > clenord = { 16, 17, 18, 0, 8, 7, 9, 6, 10, 5, 11, 4, 12, 3, 13, 2, 14, 1, 15 };

const unsigned eob     = 256; // end of block marker
const unsigned maxCode = 285; // maximum literal/length code
}

#endif // COMMON_H
