#ifndef DEFLATE_HPP
#define DEFLATE_HPP

#include "bitstream/lebitstream.h"
#include <utility>
#include <vector>

namespace deflate
{
class Compressor
{
public:
	enum class error
	{
		none,
		dest_size,  // Not enough room in the destination buffer
		block_type, // Reserved/unknown block type found
		nlen,       // LEN and NLEN differ in non-compressed block
		code,       // A disallowed literal/length code occurred
		distance,   // The distance specified is before the BOS
		dist_range, // A disallowed distance code occurred
		bad_arg     // Bad argument, ex., a null pointer with non zero amount of data
	};
	error Compress(const std::byte*, std::size_t, std::byte*, std::size_t*);
private:
	using bits = OLEBitstream::bits;

	struct lz77
	{
		std::size_t dist;
		std::size_t length;
	};

	/// @todo Use struct instead of pair
	void WriteBlock(std::size_t, std::size_t, const std::vector< lz77 >&);

	bits HuffmanFixedLL(unsigned long);
	bits HuffmanFixedDist(unsigned long);
	OLEBitstream os;
};
}
#endif // #ifndef DEFLATE_HPP
