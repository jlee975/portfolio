#include "decompressor.h"

#include <stdexcept>

#include "bitstream/lebitstream.h"
#include "common.h"
#include "huffman/huffmantable.h"

namespace deflate
{
Decompressor::Decompressor() = default;

void Decompressor::reset()
{
	state = read_next_block;
	data_.clear();
}

Decompressor::error Decompressor::exec(ILEBitstream& bs)
{
	while ( state != processed_final_block )
	{
		try
		{
			if ( state == read_next_block )
			{
				// first bit indicates final block
				const auto x = bs.read(3);
				final_block_bit = x & 1;
				block_type      = x >> 1;
				lzd.state       = get_pre;
				dhd.state       = begin;
				ncd.state       = skip;
				state           = do_block_type;
			}

			if ( state == do_block_type )
			{
				switch ( block_type )
				{
				case dynamic_huffman:
					do_dynamic_huffman(bs);
					break;
				case fixed_huffman:
					lz77(bs, fixed_llbook, fixed_distbook);
					break;
				case no_compression:
					do_no_compression(bs);
					break;
				default:
					return error::block;
				}
			}

			state = ( final_block_bit != 0 ) ? processed_final_block : read_next_block;
		}
		catch ( unexpected_eos_type )
		{
			return error::eof;
		}
		catch ( error e )
		{
			return e;
		}
	}

	return error::none;
}

void Decompressor::do_no_compression(ILEBitstream& bs)
{
	if ( ncd.state == skip )
	{
		bs.move_to_next_byte_boundary(); // Align stream to next byte
		ncd.state = get_len;
	}

	if ( ncd.state == get_len )
	{
		const auto x = bs.read(32);
		ncd.copy_len = x & UINT16_C(0xffff); // # of bytes to copy

		// Verify against one's complement stored in stream
		if ( ( ncd.copy_len ^ ( x >> 16 ) ) != UINT16_C(0xFFFF) )
		{
			throw error::parity;
		}

		ncd.state = copy_raw;
	}

	if ( ncd.state == copy_raw )
	{
		/// @todo Copy bytes directly instead of going through read
		for (; ncd.copy_len != 0; --ncd.copy_len) // Store data
		{
			const auto t = bs.read(8);
			data_.push_back(static_cast< std::byte >( t ) );
		}
	}
}

void Decompressor::do_dynamic_huffman(ILEBitstream& bs)
{
	// Read # of codes stored for each table
	if ( dhd.state == begin )
	{
		const auto x = bs.read(14);
		dhd.nlit     = 257 + ( x & 0x1f );
		dhd.ndist    = 1 + ( ( x >> 5 ) & 0x1f );
		dhd.cd.nclen = 4 + ( x >> 10 );
		dhd.state    = get_clenbook;
	}

	if ( dhd.state == get_clenbook )
	{
		auto x = bs.read(3 * dhd.cd.nclen);

		// Read the lengths of the 'table table'
		for (std::size_t i = 0; i < 19; ++i)
		{
			dhd.cd.clen[clenord[i]] = ( x & 7 );
			x                     >>= 3;
		}

		dhd.pd.state    = read_ccode;
		dhd.pd.clenbook = { dhd.cd.clen, 19 };
		dhd.pd.cp       = 0;
		dhd.pd.ci       = 0;
		dhd.state       = get_plens;
	}

	if ( dhd.state == get_plens )
	{
		for (; dhd.pd.ci < dhd.nlit + dhd.ndist;)
		{
			if ( dhd.pd.state == read_ccode )
			{
				dhd.pd.ccode = ReadCodeword(bs, dhd.pd.clenbook);
				dhd.pd.state = read_r;
			}

			if ( dhd.pd.state == read_r )
			{
				dhd.pd.r = 1; // repeat count

				if ( dhd.pd.ccode < 17 ) // literal, or repeat a code
				{
					if ( dhd.pd.ccode == 16 )
					{
						dhd.pd.r = 3 + bs.read(2);
					}
					else
					{
						dhd.pd.cp = dhd.pd.ccode;
					}
				}
				else // write zero lengths
				{
					dhd.pd.cp = 0;
					dhd.pd.r  = ( dhd.pd.ccode == 17 ? 3 + bs.read(3) : 11 + bs.read(7) );
				}

				for (; dhd.pd.r > 0 && dhd.pd.ci < 320; dhd.pd.r--)
				{
					dhd.pd.plens[dhd.pd.ci++] = dhd.pd.cp;
				}

				dhd.pd.state = read_ccode;
			}
		}

		dhd.state = create_dyn_tables;
	}

	if ( dhd.state == create_dyn_tables )
	{
		dhd.dyn_llbook   = HuffmanTable(dhd.pd.plens, dhd.nlit);
		dhd.dyn_distbook = HuffmanTable(dhd.pd.plens + dhd.nlit, dhd.ndist);
		lzd.state        = get_pre;
		dhd.state        = do_lz77;
	}

	if ( dhd.state == do_lz77 )
	{
		// Create tables from code lengths that we've read
		lz77(bs, dhd.dyn_llbook, dhd.dyn_distbook);
	}
}

void Decompressor::lz77(
	ILEBitstream&       bs,
	const HuffmanTable& llbook,
	const HuffmanTable& distbook
)
{
	while ( true )
	{
		if ( lzd.state == get_pre )
		{
			lzd.pre   = ReadCodeword(bs, llbook); // code for literal or (dist,len) pair
			lzd.state = get_runlen;
		}

		if ( lzd.pre == eob )
		{
			break;
		}

		if ( lzd.pre < eob /* && 0 <= x */ ) // literal to be stored
		{
			data_.push_back(static_cast< std::byte >( lzd.pre ) );
			lzd.state = get_pre;
		}
		else if ( lzd.pre <= maxCode ) // (length, distance) pair to be copied
		{
			if ( lzd.state == get_runlen )
			{
				if ( lzd.pre < 265 ) // no extra bits to read
				{
					// length of 3-10
					lzd.runlen = lzd.pre - 254;
				}
				else if ( lzd.pre < maxCode ) // len is partly encoded in extra bits
				{
					const auto y = lzd.pre - 257;
					lzd.runlen = bs.read( ( y >> 2 ) - 1) + 3 + ( ( 4 + ( y & 3 ) ) << ( ( y >> 2 ) - 1 ) );
				}
				else
				{
					lzd.runlen = 258;
				}

				lzd.state = get_rundist;
			}

			if ( lzd.state == get_rundist )
			{
				lzd.rundist = ReadCodeword(bs, distbook);
				lzd.state   = get_rundist_extended;
			}

			if ( lzd.state == get_rundist_extended )
			{
				if ( lzd.rundist < 4 )
				{
					++lzd.rundist;
				}
				else if ( lzd.rundist <= 29 )
				{
					lzd.rundist = 1 + bs.read( ( lzd.rundist - 2 ) >> 1)
					              + ( ( 2 + ( lzd.rundist & 1 ) ) << ( ( lzd.rundist - 2 ) >> 1 ) );
				}
				else
				{
					throw error::invalid_distance;
				}

				lzd.state = copy_bytes;
			}

			if ( lzd.state == copy_bytes )
			{
				// Copy earlier byte sequence
				if ( lzd.rundist <= data_.size() )
				{
					unsigned long si = data_.size() - lzd.rundist;

					for (; lzd.runlen > 0; lzd.runlen--)
					{
						data_.push_back(data_[si++]);
					}
				}
				else
				{
					throw error::invalid_distance;
				}

				lzd.state = get_pre;
			}
		}
		else
		{
			throw error::invalid_literal;
		}
	} // END: LZ77 loop

}

Decompressor::bits::word_type Decompressor::ReadCodeword(
	ILEBitstream&       bs,
	const HuffmanTable& lptree
)
{
	const bits t = bs.peek();

	const auto [x, cb] = lptree.extract(t);

	if ( cb == 0 )
	{
		bs.read(bits::maxlength);
		throw std::logic_error("Problem with Huffman Table");
	}

	bs.discard(cb);
	return x;
}

const std::vector< std::byte >& Decompressor::data() const&
{
	return data_;
}

std::vector< std::byte >&& Decompressor::data() &&
{
	return std::move(data_);
}

}
