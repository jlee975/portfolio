#ifndef DECOMPRESSOR_H
#define DECOMPRESSOR_H

#include "bitstream/lebitstream.h"
#include "huffman/huffmantable.h"
#include <vector>

namespace deflate
{
/**
 * @brief The Decompressor class
 * @todo Scan. i.e., find the size of the deflate data without extracting it
 */
class Decompressor
{
public:
	enum class error
	{
		none,             ///< No error occurred
		block,            ///< Bad block type
		parity,           ///< Bad length value encoded in uncompressed data block
		eof,              ///< Unexpected EOF
		invalid_distance, ///< Invalid distance in compressed data
		invalid_literal   ///< Invalid literal or length code in compressed data
	};

	Decompressor();

	void reset();
	error exec(ILEBitstream&);
	const std::vector< std::byte >& data() const&;
	std::vector< std::byte >&& data() &&;
private:
	using bits = ILEBitstream::bits;

	enum state_type
	{
		read_next_block,
		do_block_type,
		processed_final_block
	};

	enum no_compression_state_type
	{
		skip,
		get_len,
		copy_raw
	};

	enum dynamic_huffman_state_type
	{
		begin,
		get_clenbook,
		get_plens,
		create_dyn_tables,
		do_lz77
	};

	enum plens_state_type
	{
		read_ccode,
		read_r
	};

	enum lz77_state_type
	{
		get_pre,
		get_runlen,
		get_rundist,
		get_rundist_extended,
		copy_bytes
	};

	struct no_compression_data
	{
		no_compression_state_type state;
		bits::word_type copy_len;
	};

	struct dynamic_huffman_data
	{
		struct clen_data
		{
			bits::word_type nclen;
			bits::length_type clen[19];
		};

		struct plens_data
		{
			plens_state_type state;
			HuffmanTable clenbook;
			bits::length_type cp;
			std::size_t ci;
			bits::word_type r;
			bits::word_type ccode;
			bits::length_type plens[320];
		};

		dynamic_huffman_state_type state;
		bits::word_type nlit;
		bits::word_type ndist;
		clen_data cd;
		plens_data pd;
		HuffmanTable dyn_llbook;
		HuffmanTable dyn_distbook;
	};

	struct lz77_data
	{
		lz77_state_type state;
		bits::word_type pre;
		unsigned long runlen;
		unsigned long rundist;
	};

	void lz77(ILEBitstream&, const HuffmanTable&, const HuffmanTable&);
	bits::word_type ReadCodeword(ILEBitstream&, const HuffmanTable&);
	void do_dynamic_huffman(ILEBitstream&);
	void do_no_compression(ILEBitstream&);

	/// @todo Could reduce the number of variables as some of them are mutually exclusive
	/// @todo Free tables after they are no longer used
	/// @todo introduce some structs and use variant to clean things up
	state_type state{ read_next_block };

	/// @todo Smarter allocation/container tuned for the behaviour of Deflate
	/// Esp. knowing we only go back 64k, and we want to avoid copying on reallocs
	/// Also, want to be able to reserve/resize without zeroing. Don't want to be
	/// relying on the extra logic push_back has. Just resize (without initializing)
	/// and copy.
	std::vector< std::byte > data_;

	bits::word_type final_block_bit;
	bits::word_type block_type;

	no_compression_data  ncd;
	dynamic_huffman_data dhd;
	lz77_data            lzd;
};
}

#endif // DECOMPRESSOR_H
