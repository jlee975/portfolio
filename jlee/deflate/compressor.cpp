/* Deflate =====================================================================

   General purpose compression algorithm.

   FIXME --------------------------------------------------------------------------

   - Watch for illegal values in distance table (values 30 and 31 are illegal).

   TODO ---------------------------------------------------------------------------

   - Inflate should be resumable, especially since we don't know the size of the
   output before hand.
   - Could probably speed up the straight copy (not-compressed case by adding
   another method to the Huffman class. Reading 8-bits at a time is not
   efficient.
   - Delete Huffman tables after a block is completed because we won't be using
   them again
   TODO replace hash table with unordered_map from tr1/c++0x

   References ---------------------------------------------------------------------

   http://www.ietf.org/rfc/rfc1951.txt


   ==============================================================================*/

#include "compressor.h"

#include <algorithm>
#include <iostream>
#include <limits>
#include <stdexcept>

#include "common.h"

const std::size_t DEFLATE_HASH_SIZE = 256;
// Size of the compression hash. Should be a power of two.

const std::size_t DEFLATE_MAX_PAIRS = 2000;
// The maximum number of dist/len pairs or literal entries to keep. When this
// many are found, a block is written out

const std::size_t minMatchLength = 3;     // minumum length for a match
const std::size_t maxMatchLength = 258;   // maximum length for a match
const std::size_t maxDistance    = 32768; // furthest back to find match

namespace
{
// Adapted from Thomas Wang's routine. Can't find the original, but
// credited here: https://burtleburtle.net/bob/hash/integer.html
std::uint32_t hashfn(const std::byte* p)
{
	std::uint32_t h = ( std::to_integer< std::uint32_t >(p[0]) ) + ( ( std::to_integer< std::uint32_t >(p[1]) ) << 8 )
	                  + ( ( std::to_integer< std::uint32_t >(p[2]) ) << 16 );

	h += ~( h << 15 );
	h ^= ( h >> 10 );
	h += ( h << 3 );
	h ^= ( h >> 6 );
	h += ~( h << 11 );
	h ^= ( h >> 16 );
	return h;
}

}

namespace deflate
{

// FIXME: Check bounds very strictly
// TODO: Change return value
Compressor::error Compressor::Compress(
	const std::byte* pIn,
	std::size_t      cbIn,
	std::byte*       pOut,
	std::size_t*     cbOut
)
{
	if ( cbOut == nullptr || ( pIn == nullptr && cbIn != 0 ) || ( pOut == nullptr && *cbOut != 0 ) )
	{
		return error::bad_arg;
	}

	std::vector< std::size_t > hashhead(DEFLATE_HASH_SIZE, std::size_t(-1) );
	std::vector< std::size_t > hashnext(maxDistance, std::size_t(-1) );

	// dist, ll
	std::vector< lz77 > lz77info;

	lz77info.reserve(DEFLATE_MAX_PAIRS);

	os.open(pOut, *cbOut);

	for (std::size_t si = 0; si < cbIn;)
	{
		// the best match found (offset and length);
		lz77 best = { 0, minMatchLength - 1 };

		if ( si + minMatchLength <= cbIn )
		{
			// Hash function of first three bytes
			const std::size_t h = hashfn(pIn + si) % DEFLATE_HASH_SIZE;

			for (std::size_t di = hashhead[h];
			     best.length < cbIn - si && di < si && si - di <= maxDistance;
			)
			{
				// A quick check so we don't bother checking matches that wouldn't be as good as what we've
				// already found
				if ( ( pIn + si )[best.length] == ( pIn + di )[best.length] )
				{
					std::size_t k = 0;

					// how many characters match?
					while ( k < cbIn - si && ( pIn + si )[k] == ( pIn + di )[k] )
					{
						++k;
					}

					// Was this a better match?
					if ( k > best.length )
					{
						best.dist = si - di;

						if ( k >= maxMatchLength )
						{
							// We can't do any better, so time to quit
							best.length = maxMatchLength;
							break;
						}

						best.length = k;
					}
				}

				// Check next possible match
				di = hashnext[di % maxDistance];
			}

			// Update hash table for this string
			hashnext[si % maxDistance] = hashhead[h];
			hashhead[h]                = si;
		}

		if ( best.length >= minMatchLength )
		{
			lz77info.push_back(best);
			si += best.length;
		}
		else // write out a literal
		{
			lz77info.push_back({ 0, std::to_integer< std::size_t >(pIn[si]) });
			si++;
		}

		if ( !( lz77info.size() < DEFLATE_MAX_PAIRS && si < cbIn ) )
		{
			WriteBlock(si, cbIn, lz77info);

			lz77info.clear();
		}
	}

	*cbOut = os.size();
	return error::none;
}

void Compressor::WriteBlock(
	std::size_t                si,
	std::size_t                cbIn,
	const std::vector< lz77 >& lz77info
)
{
	// Is this a final block?
	os.write({ ( si >= cbIn ? 1U : 0U ), 1 });

	/// @todo Dynamic huffman
	// Write out the type of block and huffman table
	os.write({ fixed_huffman, 2 });

	for (auto [dist, len_] : lz77info)
	{
		if ( dist != 0 )
		{
			std::size_t len = len_ - 3;

			// Write the length
			if ( len >= 8 )
			{
				if ( len != 255 )
				{
					unsigned nextrabits = 0;

					while ( ( 8UL << nextrabits ) <= len )
					{
						nextrabits++;
					}

					const unsigned long code = 257 + nextrabits * 4 + ( len >> nextrabits );
					os.write(HuffmanFixedLL(code) );
					os.write({ len, nextrabits });
				}
				else
				{
					os.write(HuffmanFixedLL(285) );
				}
			}
			else
			{
				os.write(HuffmanFixedLL(len + 257) );
			}

			// Write the distance
			if ( dist-- > 4 )
			{
				unsigned nextrabits = 0;

				while ( ( 4UL << nextrabits ) <= dist )
				{
					nextrabits++;
				}

				const unsigned long code = 2 * nextrabits + 2 + ( ( dist >> nextrabits ) & 1 );
				os.write(HuffmanFixedDist(code) );

				os.write({ dist, nextrabits });
			}
			else
			{
				os.write(HuffmanFixedDist(dist) );
			}
		}
		else
		{
			// write a literal
			os.write(HuffmanFixedLL(len_) );
		}
	}

	os.write(HuffmanFixedLL(eob) );
}

// FIXME: Move to chomp class and generalize/optimize
Compressor::bits Compressor::HuffmanFixedLL(unsigned long sym)
{
	unsigned long rev = sym;
	unsigned      nb  = 5;

	if ( sym < 256 )
	{
		if ( sym < 144 )
		{
			rev += 48;
			nb   = 8;
		}
		else
		{
			rev += 256;
			nb   = 9;
		}
	}
	else
	{
		if ( sym < 280 )
		{
			rev -= 256;
			nb   = 7;
		}
		else
		{
			rev -= 88;
			nb   = 8;
		}
	}

	return reverse_bits(bits{ rev, nb });
}

Compressor::bits Compressor::HuffmanFixedDist(unsigned long sym)
{
	return reverse_bits(bits{ sym, 5 });
}

}
