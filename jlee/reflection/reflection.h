#pragma once

#include <iostream>
#include <algorithm>
#include <string_view>
#include <type_traits>

namespace jl
{
namespace reflection
{
template< std::size_t N >
struct StringLiteral
{
	constexpr StringLiteral(const char ( & str )[N])
	{
		std::copy_n(str, N, value);
	}

	operator std::string_view() const
	{
		return { value + 0, value + ( N - 1 ) };
	}

	char value[N];
};

/// @todo S is a separate thing, conceptually, from reflection. It's really a serialization thing.
template< auto P, StringLiteral S >
requires std::is_member_pointer_v< decltype( P ) >
struct member;

template< typename...Xs >
struct pack;

namespace details
{
template< typename T, typename V, typename X >
struct visit_one
{
	static void apply(
		T& t,
		V& v
	)
	{
		if constexpr ( std::is_const_v< T >)
		{
			v(static_cast< const X& >( t ) );
		}
		else
		{
			v(static_cast< X& >( t ) );
		}
	}

};

template< typename T, typename V, auto... Xs >
struct visit_one< T, V, member< Xs... > >
{
	static void apply(
		T& t,
		V& v
	)
	{
		v(t, Xs...);
	}

};

template< typename T, typename V, typename Pack >
struct visit_each;

template< typename T, typename V, typename...Xs >
struct visit_each< T, V, pack< Xs... > >
{
	static void apply(
		T& t,
		V& visitor
	)
	{
		( visit_one< T, V, Xs >::apply(t, visitor), ... );
	}

};

}

template< typename L, typename T, typename V >
void visit(
	T& t,
	V& visitor
)
{
	details::visit_each< T, V, L >::apply(t, visitor);
}

}
}
