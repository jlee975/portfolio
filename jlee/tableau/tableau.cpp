#include "tableau.h"

#include <utility>

Tableau::Tableau(
	const std::vector< int >& r,
	std::vector< int >        c
)
	: rows(r), cols(std::move(c) ), nrows(rows.size() ), ncols(cols.size() ),
	sum(std::accumulate(r.begin(), r.end(), 0) )
{
	if ( sum != std::accumulate(cols.begin(), cols.end(), 0) )
	{
		throw std::runtime_error("Same size");
	}
}

int Tableau::at(
	std::size_t r,
	std::size_t c
) const
{
	return est[r * ncols + c];
}

void Tableau::solve()
{
	est.resize(nrows * ncols);

	for (std::size_t r = 0; r < nrows; ++r)
	{
		for (std::size_t c = 0; c < ncols; ++c)
		{
			est[r * ncols + c] = std::floor(rows[r] * cols[c] / double(sum) );
		}
	}

	while ( update_avail() )
	{
		typedef std::pair< std::size_t, std::size_t > pair;

		// there is some row and some col not filled. Consequently, we can
		// add one at their intersection
		std::vector< pair > good;

		/// @todo Accept functor to determine legal moves
		for (const unsigned long avail_col : avail_cols)
		{
			for (const unsigned long avail_row : avail_rows)
			{
				good.emplace_back(avail_row, avail_col);
			}
		}

		const pair        p = good[rand() % good.size()];
		const std::size_t r = p.first;
		const std::size_t c = p.second;

		est[r * ncols + c] += 1;
	}
}

bool Tableau::update_avail()
{
	avail_rows.clear();
	avail_cols.clear();

	for (std::size_t r = 0; r < nrows; ++r)
	{
		int total = 0;

		for (std::size_t c = 0; c < ncols; ++c)
		{
			total += est[r * ncols + c];
		}

		if ( total < rows[r] )
		{
			avail_rows.push_back(r);
		}
	}

	for (std::size_t c = 0; c < ncols; ++c)
	{
		int total = 0;

		for (std::size_t r = 0; r < nrows; ++r)
		{
			total += est[r * ncols + c];
		}

		if ( total < cols[c] )
		{
			avail_cols.push_back(c);
		}
	}

	return !avail_cols.empty();
}
