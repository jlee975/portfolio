#ifndef TABLEAU_H
#define TABLEAU_H

#include <cmath>
#include <cstdlib>
#include <iostream>
#include <numeric>
#include <stdexcept>
#include <vector>

class Tableau
{
public:
	Tableau(const std::vector< int >& r, std::vector< int > c);

	/// @todo Recursive solution with backtracking. Only requires O(N*M) space
	void solve();
	int at(std::size_t, std::size_t) const;
private:
	bool update_avail();

	const std::vector< int > rows;
	const std::vector< int > cols;
	const std::size_t        nrows;
	const std::size_t        ncols;
	const int                sum;

	std::vector< int >         est;
	std::vector< std::size_t > avail_rows;
	std::vector< std::size_t > avail_cols;
};

#endif // TABLEAU_H
