#ifndef HASH_H
#define HASH_H

#include "bits/hex.h"
#include <array>
#include <functional>

namespace jlee
{
template< unsigned B >
class hash
{
public:
	hash()
		: content()
	{
	}

	static hash from_array(const unsigned char* x)
	{
		hash h;

		for (std::size_t i = 0; i < B / 8; ++i)
		{
			h.content[i] = x[i];
		}

		return h;
	}

	std::string to_string() const
	{
		std::string s;

		for (std::size_t i = 0; i < B / 8; ++i)
		{
			s += to_hex( ( content[i] & 0xf0 ) >> 4);
			s += to_hex(content[i] & 0xf);
		}

		return s;
	}

	std::string to_string(std::size_t n) const
	{
		std::string s = to_string();

		if ( n < s.length() )
		{
			s.resize(n);
		}

		return s;
	}

	void to_string(char* s) const
	{
		for (std::size_t i = 0; i < B / 8; ++i)
		{
			s[2 * i]     = to_hex( ( content[i] & 0xf0 ) >> 4);
			s[2 * i + 1] = to_hex(content[i] & 0xf);
		}

		s[2 * ( B / 8 )] = '\0';
	}

	static hash from_string(const std::string& s)
	{
		hash h;

		for (std::size_t i = 0; i < B / 8; ++i)
		{
			h.content[i] = ( from_hex(s[2 * i]) << 4 ) + from_hex(s[2 * i + 1]);
		}

		return h;
	}

	friend bool operator<(
		const hash& l,
		const hash& r
	)
	{
		return l.content < r.content;
	}

	friend bool operator==(
		const hash& l,
		const hash& r
	)
	{
		return l.content == r.content;
	}

	std::size_t get_std_hash() const
	{
		return *reinterpret_cast< const std::size_t* >( content.data() );
	}
private:
	std::array< unsigned char, B / 8 > content;
};
}

namespace std
{
template< unsigned B >
struct hash< jlee::hash< B > >
{
	std::size_t operator()(const jlee::hash< B >& h) const
	{
		return h.get_std_hash();
	}

};
}

#endif // HASH_H
