/*=========================================================================*//**
*  \file        flac.cpp
*//*==========================================================================*/
#include <climits>
#include <stdexcept>

#include "audio/packet.h"
#include "audio/samples.h"
#include "bitstream/bebitstream.h"
#include "chirp/metadata.h"
#include "endian/endian.h"
#include "flac.h"
#include "rice/rice.h"

namespace
{
long ReadBitsS(IBEBitstream&, unsigned int);
}

/*=========================================================================*//**
*  \class       FlacFrame
*
*  This class encapsulates the native flac framing.
*
*  \todo Moving past the sync is naive and slow
*//*==========================================================================*/
FlacFrame::FlacFrame(std::istream& input)
	: is(input), iStream(0), fixed_blocksize(0), current_pos{0}
{
}

/***************************************************************************/ /**
 *  \todo 38 is size of 4-byte id and 34-byte mandatory STREAMINFO block
 *  \todo The findsync loop is really bad for FLAC because we don't get anywhere
 *     near the next frame
 *  \todo Clean up the char/unsigned char problems
 *******************************************************************************/
void FlacFrame::prepare()
{
	char buf[38];

	if ( is.good() )
	{
		is.seekg(0);
		is.read(buf, 38);

		if ( is.gcount() != 38 )
		{
			return;
		}

		fixed_blocksize = beread< 16 >(reinterpret_cast< const std::byte* >( buf ) + 8);
	}

	while ( is.good() )
	{
		is.read(buf, 16);

		if ( is.gcount() != 16 )
		{
			if ( is.eof() )
			{
				is.clear();
			}

			break;
		}

		if ( is.good() && !Flac::IsProbablyPacket(reinterpret_cast< unsigned char* >( buf ), 16) )
		{
			for (int i = 0; i < 15; ++i)
			{
				buf[i] = buf[i + 1];
			}

			is.get(buf[15]);
		}

		if ( !is.good() )
		{
			if ( is.eof() )
			{
				is.clear();
			}

			break;
		}

		seektable[getSampleNo(reinterpret_cast< unsigned char* >( buf ) )] = is.tellg() - std::streamoff(16);
	}
}

/***************************************************************************/ /**
 *  \fn          sample_off FlacFrame::getSampleNo(const unsigned char*)
 *
 *  \bug accessing values without bound checking
 *  \bug multiplying (y:x) by minimum_blocksize is done incorrectly
 *******************************************************************************/
audio::sample_off FlacFrame::getSampleNo(const unsigned char* p)
{
	// Get frame number or sample number
	unsigned long x = p[4];

	unsigned long y = 0;

	std::size_t i = 1; // 7 bits total

	if ( x >= 0xC0 )
	{
		x &= 63, i = 2; // 12 bits total
	}

	if ( x >= 0xE0 )
	{
		x &= 31, i = 3; // 17 bits total
	}

	if ( x >= 0xF0 )
	{
		x &= 15, i = 4; // 22 bits total
	}

	if ( x >= 0xF8 )
	{
		x &= 7, i = 5; // 27 bits total
	}

	if ( x >= 0xFC )
	{
		x &= 3, i = 6; // 32 bits total
	}

	if ( x >= 0xFE )
	{
		x &= 1, i = 7; // 37 bits total
	}

	for (std::size_t j = 1; j < i; ++j)
	{
		y  = ( y << 6 ) + ( x >> 26 );
		x  = ( x << 6 ) + ( static_cast< unsigned long >( p[4 + j] ) & 0x63UL );
		x &= 0xFFFFFFFFUL;
	}

	if ( ( p[1] % 2 ) == 0 ) // (y, x) is frame number
	{
		y *= fixed_blocksize;
		x *= fixed_blocksize; // Taken from Streaminfo
	}

	return { ( static_cast< std::uint_fast64_t >( y ) << 32 ) + x };
}

/***************************************************************************/ /**
 *  \fn          std::vector<unsigned char> FlacFrame::ReadPacket()
 *******************************************************************************/
Packet FlacFrame::ReadPacket()
{
	Packet packet(16);
	char   c;

	is.seekg(iStream);
	is.get(c);

	if ( !is.good() )
	{
		return packet;
	}

	packet[0] = static_cast< unsigned char >( c );

	std::size_t n;

	switch ( packet[0] )
	{
	case 0xFF: // Audio packet
		is.read(reinterpret_cast< char* >( &packet[1] ), 15);

		if ( ( packet[1] & 0xFC ) != 0xF8 )
		{
			throw std::runtime_error("ERROR: AUDIO_SYNC");
		}

		current_pos = getSampleNo(&packet[0]);

		n = 0;

		do
		{
			is.get(c);

			if ( !is.good() )
			{
				break;
			}

			packet.push_back(static_cast< unsigned char >( c ) );
			++n;
		}
		while ( !Flac::IsProbablyPacket(&packet[n], 16) );

		packet.resize(n);
		iStream += n;
		break;
	case 0x66: // Stream header
		n = 4;
		is.read(reinterpret_cast< char* >( &packet[1] ), 3);
		packet.resize(4);
		iStream += 4;
		break;
	default: // some kind of meta data
		is.read(reinterpret_cast< char* >( &packet[1] ), 3);
		n = 4 + ( beread< 32 >(reinterpret_cast< const std::byte* >( packet.data() ) ) % 0x1000000UL );
		packet.resize(n);
		is.read(reinterpret_cast< char* >( &packet[4] ), n - 4);
		iStream += n;
		break;
	} // switch

	return packet;
}

/***************************************************************************/ /**
 *  \fn          bool FlacFrame::eos()
 *******************************************************************************/
bool FlacFrame::eos() const
{
	return iStream >= is.seekg(0, std::ios_base::end).tellg();
}

// */

/***************************************************************************/ /**
 *  \fn          void FlacFrame::seek(unsigned long)
 *******************************************************************************/
void FlacFrame::seek(audio::sample_off i)
{
	auto it = seektable.lower_bound(audio::sample_off(i) );

	if ( it != seektable.end() )
	{
		iStream = it->second;
	}
	else
	{
		iStream = 0;
	}
}

/***************************************************************************/ /**
 *******************************************************************************/
audio::sample_off FlacFrame::position() const
{
	return current_pos;
}

/***************************************************************************/ /**
 *******************************************************************************/
audio::sample_off FlacFrame::length() const
{
	if ( !seektable.empty() )
	{
		return seektable.rbegin()->first;
	}

	return { 0 };
}

/*=========================================================================*//**
*  \class       Flac
*
*  \bug Missing/uninitialized STREAMINFO. Is this a problem?
*  \bug Subset compliance
*  \bug Do we have to worry about gain from subframe to subframe
*       int gain = bits_per_sample - bps;
*       if (gain != 0) std::cout << "Gain = " << gain << std::endl;
*  \bug When checking blocksize, compare against max_samples
*  \bug Wasted bits per sample
*  \bug Handle higher resolution bitwidths for output
*  \bug Does bits_per_sample change for left/side, right/side, mid/side when constant or verbatim
*  \bug is it an error if max samples < min samples?? frame max < frame min??
*  \bug can the residue escape code be 0?
*
*//*==========================================================================*/

/***************************************************************************/ /**
 *  \fn          Flac::Flac
 *******************************************************************************/
Flac::Flac()
	: sample_rate_{0}
{
}

/***************************************************************************/ /**
 *******************************************************************************/
Flac::~Flac()
{
	delete[] pbuf;
}

/***************************************************************************/ /**
 *  \fn          std::vector<short> Flac::DumpAudio
 *
 *  \todo extend to n channels instead of 2
 *******************************************************************************/
Flac::samples_type Flac::DumpAudio()
{
	samples_type data(2);

	for (unsigned long i = 0; i < gensamples; i++)
	{
		for (unsigned j = 0; j < 2; j++)
		{
			long t = buf[j][i];
			auto u = static_cast< std::int_least16_t >( t );

			if ( t < -32767 )
			{
				u = -32767;
			}
			else if ( t > 32767 )
			{
				u = 32767;
			}

			data.push_back(u);
		}
	}

	return data;
}

/***************************************************************************/ /**
 *  \fn          void Flac::DecodePacket(const std::vector<unsigned char>&)
 *
 *  \todo Handle Packet better, rather than simply indexing into it
 *******************************************************************************/
void Flac::DecodePacket(const Packet& p)
{
	if ( p.empty() )
	{
		return;
	}

	switch ( p[0] )
	{
	case 0x7F: // FLAC-to-Ogg header
		ProcessF2OHeader(p);
		break;
	case 0xFF: // This is an audio packet
		DecodeAudio(p);
		break;
	default:                   // This should be a meta data packet
		ProcessMetaData(p, 0); // \bug Native header
	}
}

/***************************************************************************/ /**
 *  \fn          void Flac::seek()
 *******************************************************************************/
void Flac::seek()
{
}

/***************************************************************************/ /**
 *  \fn          Error Flac::ProcessF20Header(const std::vector<unsigned char>&)
 *******************************************************************************/
Flac::Error Flac::ProcessF2OHeader(const Packet& pData)
{
	std::size_t nData = pData.size();

	if ( nData < 13 )
	{
		return ERROR_EOS;
	}

	// Check for FLAC-to-Ogg Mapping
	if ( !( ( pData[0] == 0x7F ) && ( pData[1] == 0x46 ) && ( pData[2] == 0x4C ) && ( pData[3] == 0x41 )
	        && ( pData[4] == 0x43 ) )
	)
	{
		return ERROR_OGG;
	}

	// Check that we support the version of F2O in use
	if ( !( ( pData[5] == 0x01 ) && ( pData[6] == 0x00 ) ) )
	{
		return ERROR_STREAM_VERSION;
	}

	// The number of headers, which we never actually use
	// num_headers = (static_cast<unsigned>(pData[7]) << 8)
	// + static_cast<unsigned>(pData[8]);

	if ( !( ( pData[9] == 0x66 ) && ( pData[10] == 0x4C ) && ( pData[11] == 0x61 ) && ( pData[12] == 0x43 ) ) )
	{
		return ERROR_SIGNATURE;
	}

	return ProcessMetaData(pData, 13);
}

/***************************************************************************/ /**
 *  \fn          Error Flac::ProcessMetaData(const std::vector<unsigned char>&, std::size_t)
 *
 *  \bug Cuesheet handling
 *******************************************************************************/
Flac::Error Flac::ProcessMetaData(
	const Packet& pData,
	std::size_t   k
)
{
	unsigned long nData = pData.size();

	if ( k + 4 >= nData )
	{
		return ERROR_EOS;
	}

	// last_meta = ((pData[0] & 0x80) != 0);
	unsigned block_type = 0x7FU & pData[k];

	/*
	 * unsigned long meta_size;
	 * meta_size = (static_cast<unsigned long>(pData[k + 1]) << 16)
	 + (static_cast<unsigned long>(pData[k + 2]) <<  8)
	 + (static_cast<unsigned long>(pData[k + 3]));
	 + // */

	Flac::Error err = OK;

	switch ( block_type )
	{
	case StreamInfo:
		err = ProcessStreamInfo(pData, k + 4);
		break;
	case Comment:
		ProcessComment(pData, k + 4);
		break;
	case Invalid:
		err = ERROR_META_BLOCKTYPE;
		break;
	default:
		break;
	}

	return err;
}

/***************************************************************************/ /**
 *  \fn          Error Flac::ProcessStreamInfo(const std::vector<unsigned charL>&, std::size_t)
 *
 *  \todo This code is a little hacky the way pData is accessed...
 *******************************************************************************/
Flac::Error Flac::ProcessStreamInfo(
	const Packet& pData,
	std::size_t   k
)
{
	unsigned long nData = pData.size();

	if ( k + 14 >= nData )
	{
		return ERROR_EOS;
	}

	unsigned min_samples; // Don't need to save this to the class...

	min_samples = ( static_cast< unsigned >( pData[k] ) << 8 ) + static_cast< unsigned >( pData[k + 1] );
	max_samples = ( static_cast< unsigned >( pData[k + 2] ) << 8 ) + static_cast< unsigned >( pData[k + 3] );

	if ( ( min_samples < 16 ) || ( max_samples < 16 ) )
	{
		return ERROR_SAMPLES;
	}

	// min_frame_size = (static_cast<unsigned long>(pData[4]) << 16)
	// + (static_cast<unsigned long>(pData[5]) << 8)
	// + static_cast<unsigned long>(pData[6]);
	// max_frame_size = (static_cast<unsigned long>(pData[7]) << 16)
	// + (static_cast<unsigned long>(pData[8]) << 8)
	// + static_cast<unsigned long>(pData[9]);

	sample_rate_ = audio::sample_rate_type{ ( static_cast< unsigned long >( pData[k + 10] ) << 12 )
		                                    + ( static_cast< unsigned long >( pData[k + 11] ) << 4 )
		                                    + ( ( static_cast< unsigned long >( pData[k + 12] ) >> 4 ) & 0xF ) };

	if ( sample_rate_ == audio::sample_rate_type{ 0 } )
	{
		return ERROR_SAMPLE_RATE;
	}

	channels        = ( ( static_cast< unsigned >( pData[k + 12] ) >> 1 ) & 0x7 ) + 1;
	bits_per_sample = ( ( static_cast< unsigned >( pData[k + 12] ) & 1 ) << 4 )
	                  + ( ( static_cast< unsigned >( pData[k + 13] ) >> 4 ) & 0xF ) + 1;

	if ( bits_per_sample < 4 )
	{
		return ERROR_BITS_PER_SAMPLE;
	}

	pbuf = new long [channels * max_samples];

	for (unsigned i = 0; i < channels; i++)
	{
		buf[i] = pbuf + ( i * max_samples );
	}

	// \bug total samples 36 bits
	// \bug MD5 is 128 bits
	return OK;
}

/***************************************************************************/ /**
 *  \fn          void Flac::ProcessComment(const std::vector<unsigned char>&, std::size_t)
 *  \todo Move this code to MetaData
 *******************************************************************************/
void Flac::ProcessComment(
	const Packet& p,
	std::size_t   k
)
{
	meta.clear();

	std::size_t ip = k;

	// Copy the vendor std::string over
	unsigned long nVendor = leread< 32 >(p.data() + ip, p.size() - ip);

	ip += 4;

	if ( ( nVendor != 0 ) && ( ( ip + nVendor ) < p.size() ) )
	{
		meta[std::string("VENDOR")].assign(&p[ip], ( &p[ip] ) + nVendor);
		ip += nVendor;
	}

	// Save each of the user comments
	unsigned long nComments = leread< 32 >(p.data() + ip, p.size() - ip);

	ip += 4;

	for (unsigned long i = 0; i < nComments; ++i)
	{
		unsigned long n = leread< 32 >(p.data() + ip, p.size() - ip);
		ip += 4;
		std::string s;

		if ( ip + n < p.size() )
		{
			s.assign(&p[ip], ( &p[ip] ) + n);
		}

		ip += n;

		std::size_t j = s.find('=');

		if ( j != std::string::npos )
		{
			std::string t = s.substr(j + 1);
			s.erase(j);

			if ( meta.count(s) != 0 )
			{
				( meta[s] += '\n' ).append(t);
			}
			else
			{
				meta[s] = t;
			}
		}
	}

	havemeta = true;
}

/***************************************************************************/ /**
 *  \fn          Error Flac::DecodeAudio(const std::vector<unsigned char>&)
 *
 *  \bug respond to the errors from LPCSubframe and FixedSubframe
 *******************************************************************************/
Flac::Error Flac::DecodeAudio(const Packet& pa)
{
	IBEBitstream bs;

	bs.open(reinterpret_cast< const std::byte* >( pa.data() ), pa.size() );

	// Verify this is an audio packet
	if ( bs.read(14) != FlacaudioSync )
	{
		return ERROR_AUDIO_SYNC;
	}

	if ( bs.read(1) != 0 )
	{
		return ERROR_RESERVED;
	}

	bs.read(1);                     // bool fixed_blocksize = (read(1) == 0);
	unsigned long iBS = bs.read(4); // used below for blocksize
	unsigned long iSR = bs.read(4); // used below for sample rate

	// Channel assignment
	unsigned       ch     = 2;
	StereoEncoding stereo = Independent;
	unsigned long  t1     = bs.read(4);

	if ( t1 < 8 )
	{
		ch = static_cast< unsigned >( t1 ) + 1;
	}
	else if ( t1 == 8 )
	{
		stereo = LeftSide;
	}
	else if ( t1 == 9 )
	{
		stereo = RightSide;
	}
	else if ( t1 == 10 )
	{
		stereo = MidSide;
	}
	else
	{
		return ERROR_CHANNEL_ASSIGNMENT;
	}

	// \bug Should verify that ch many channels have buffers

	// Bits per sample
	unsigned bps = 0;

	unsigned      rbps = 0; // bits per sample for this frame
	unsigned long t2   = bs.read(3);

	if ( t2 == 0 )
	{
		bps = bits_per_sample;
	}
	else if ( t2 == 1 )
	{
		bps = 8;
	}
	else if ( t2 == 2 )
	{
		bps = 12;
	}
	else if ( t2 == 4 )
	{
		bps = 16;
	}
	else if ( t2 == 5 )
	{
		bps = 20;
	}
	else if ( t2 == 6 )
	{
		bps = 24;
	}
	else
	{
		return ERROR_BITS_PER_SAMPLE;
	}

	if ( ( bps < 4 ) || ( bps > 32 ) )
	{
		return ERROR_BITS_PER_SAMPLE;
	}

	rbps = bps;

	// Reserved bit
	if ( bs.read(1) != 0 )
	{
		return ERROR_RESERVED;
	}

	// Sample position
	unsigned long t3 = bs.read(8);

	if ( t3 >= 0xC0 )
	{
		bs.read(8);
	}

	if ( t3 >= 0xE0 )
	{
		bs.read(8);
	}

	if ( t3 >= 0xF0 )
	{
		bs.read(8);
	}

	if ( t3 >= 0xF8 )
	{
		bs.read(8);
	}

	if ( t3 >= 0xFC )
	{
		bs.read(8);
	}

	if ( t3 >= 0xFE )
	{
		bs.read(8); // \bug UTF-8 "encoded" value depends on fixed_blocksize flag
	}

	// Blocksize
	unsigned long blocksize = 0;

	if ( iBS == 1 )
	{
		blocksize = 192;
	}
	else if ( ( 2 <= iBS ) && ( iBS < 6 ) )
	{
		blocksize = 144U << iBS;
	}
	else if ( iBS == 6 )
	{
		blocksize = bs.read(8) + 1;
	}
	else if ( iBS == 7 )
	{
		blocksize = bs.read(16) + 1;
	}
	else if ( ( 8 <= iBS ) && ( iBS < 16 ) )
	{
		blocksize = 1U << iBS;
	}
	else
	{
		return ERROR_BLOCKSIZE;
	}

	if ( ( blocksize < 16 ) || ( 65535 < blocksize ) )
	{
		return ERROR_BLOCKSIZE;
	}

	// Sample rate
	audio::sample_rate_type sr{ 0 };

	if ( iSR == 0 )
	{
		sr = sample_rate_;
	}
	else if ( ( 0 < iSR ) && ( iSR < 12 ) )
	{
		sr = sample_rate_table[iSR];
	}
	else if ( iSR == 12 )
	{
		sr = audio::sample_rate_type{ bs.read(8) * 1000 };
	}
	else if ( iSR == 13 )
	{
		sr = audio::sample_rate_type{ bs.read(16) };
	}
	else if ( iSR == 14 )
	{
		sr = audio::sample_rate_type{ 10 * bs.read(16) };
	}
	else
	{
		return ERROR_SAMPLE_RATE;
	}

	if ( sr == audio::sample_rate_type{ 0 } )
	{
		return ERROR_SAMPLE_RATE; // \todo: Non-fatal error??
	}

	// CRC
	bs.read(8); // \bug Do something besides discard this

	for (unsigned i = 0; i < ch; i++)
	{
		// \bug Does this affect residue only, or const and verbatim
		// samples, too?
		/// \todo switch (stereo)
		if ( stereo == RightSide )
		{
			bps = ( i == 0 ? rbps + 1 : rbps );
		}

		if ( ( stereo == LeftSide ) || ( stereo == MidSide ) )
		{
			bps = ( i == 1 ? rbps + 1 : rbps );
		}

		if ( bs.read(1) != 0 )
		{
			return ERROR_PADDING;
		}

		// Decode subframe type
		auto sftype = static_cast< unsigned int >( bs.read(6) );

		// Decode wasted bits per sample
		if ( bs.read(1) != 0 )
		{
			return ERROR_UNSUPPORTED;
		}

		if ( sftype == 0 ) // Constant
		{
			long c = ReadBitsS(bs, bps);

			for (unsigned long j = 0; j < blocksize; j++)
			{
				buf[i][j] = c;
			}
		}
		else if ( sftype == 1 ) // Verbatim
		{
			for (unsigned long j = 0; j < blocksize; j++)
			{
				buf[i][j] = ReadBitsS(bs, bps);
			}
		}
		else if ( ( 32 <= sftype ) && ( sftype < 64 ) ) // Linear Predictor
		{
			LPCSubframe(bs, buf[i], sftype - 31, bps, blocksize);
		}
		else if ( ( 8 <= sftype ) && ( sftype < 13 ) ) // Fixed predictor
		{
			FixedSubframe(bs, buf[i], sftype - 8, bps, blocksize);
		}
		else
		{
			return ERROR_SUBFRAME_TYPE;
		}
	}

	gensamples = blocksize;

	// Channel decoupling
	/// \todo switch (stereo)
	if ( stereo == MidSide )
	{
		for (unsigned long i = 0; i < blocksize; i++)
		{
			long m = buf[0][i];

			long s = buf[1][i];
			m = ( ( m + m ) | ( s & 1 ) );

			buf[0][i] = ( m + s ) / 2;
			buf[1][i] = ( m - s ) / 2;
		}
	}
	else if ( stereo == RightSide )
	{
		for (unsigned long i = 0; i < blocksize; i++)
		{
			buf[0][i] += buf[1][i];
		}
	}
	else if ( stereo == LeftSide )
	{
		for (unsigned long i = 0; i < blocksize; i++)
		{
			buf[1][i] = buf[0][i] - buf[1][i];
		}
	}

	return OK;
}

/***************************************************************************/ /**
 *******************************************************************************/
Flac::Error Flac::LPCSubframe(
	IBEBitstream& bs,
	long*         curve,
	unsigned      order,
	unsigned      bps,
	unsigned long blocksize
)
{
	int qlpc_coeff[32];

	// Bounds checking
	if ( curve == nullptr )
	{
		return ERROR_POINTER;
	}

	if ( ( blocksize < 16 ) || ( blocksize > 65536 ) )
	{
		return ERROR_BLOCKSIZE;
	}

	if ( ( bps < 4 ) || ( bps > 32 ) )
	{
		return ERROR_BITS_PER_SAMPLE;
	}

	if ( order > 32 )
	{
		return ERROR_QLPC_ORDER;
	}

	Flac::Error  err;
	unsigned int qlpc_prec;
	int          qlpc_shift;

	// Read warm up samples
	for (unsigned j = 0; j < order; j++)
	{
		curve[j] = ReadBitsS(bs, bps);
	}

	if ( ( qlpc_prec = static_cast< unsigned int >( bs.read(4) + 1 ) ) == 16 )
	{
		return ERROR_QLPC_PRECISION;
	}

	qlpc_shift = static_cast< int >( bs.read(5) );
	/// \bug Treat negative shifts as error

	for (unsigned j = 0; j < order; j++)
	{
		qlpc_coeff[j] = static_cast< int >( ReadBitsS(bs, qlpc_prec) );
	}

	if ( ( err = ResidueDecode(bs, curve, blocksize, order) ) != OK )
	{
		return err;
	}

	// LINEAR PREDICTOR
	for (unsigned long j = order; j < blocksize; j++)
	{
		long y = 0;

		for (unsigned k = 0; k < order; ++k)
		{
			y += qlpc_coeff[k] * curve[j - k - 1];
		}

		if ( qlpc_shift >= 0 ) // \bug signed shift
		{
			if ( y >= 0 )
			{
				curve[j] += ( y >> qlpc_shift );
			}
			else
			{
				curve[j] -= ( ( ( 1 << qlpc_shift ) - 1 - y ) >> qlpc_shift );
			}
		}
		else
		{
			curve[j] += ( y << ( -qlpc_shift ) );
		}
	}

	return OK;
}

/***************************************************************************/ /**
 *******************************************************************************/
Flac::Error Flac::FixedSubframe(
	IBEBitstream& bs,
	long*         curve,
	unsigned      order,
	unsigned      bps,
	unsigned long blocksize
)
{
	// Bounds checking
	if ( curve == nullptr )
	{
		return ERROR_POINTER;
	}

	if ( ( blocksize < 16 ) || ( blocksize > 65536 ) )
	{
		return ERROR_BLOCKSIZE;
	}

	if ( ( bps < 4 ) || ( bps > 32 ) )
	{
		return ERROR_BITS_PER_SAMPLE;
	}

	if ( order > 4 )
	{
		return ERROR_FIXED_ORDER;
	}

	Flac::Error err;

	// Warm up samples
	for (unsigned j = 0; j < order; j++)
	{
		curve[j] = ReadBitsS(bs, bps);
	}

	// Residue Decode
	if ( ( err = ResidueDecode(bs, curve, blocksize, order) ) != OK )
	{
		return err;
	}

	// Linear Predictor
	for (unsigned long k = order; k < blocksize; ++k)
	{
		long x = 0;

		switch ( 4 - order )
		{
		case 0:
			x -= curve[k - 4] - 3 * curve[k - 3] + 3 * curve[k - 2] - curve[k - 1];
		case 1:
			x += curve[k - 3] - 2 * curve[k - 2] + curve[k - 1];
		case 2:
			x -= curve[k - 2] - curve[k - 1];
		case 3:
			curve[k] += x + curve[k - 1];
		}
	}

	return OK;
}

/***************************************************************************/ /**
 *  Decode the subframe to the channel buffer
 *******************************************************************************/
Flac::Error Flac::ResidueDecode(
	IBEBitstream& bs,
	long*         curve,
	unsigned long blocksize,
	unsigned      order
)
{
	if ( curve == nullptr )
	{
		return ERROR_POINTER;
	}

	if ( ( blocksize < 16 ) || ( blocksize > 65536 ) )
	{
		return ERROR_BLOCKSIZE;
	}

	if ( order > 32 )
	{
		return ERROR_ORDER;
	}

	// Residue Decode
	const unsigned long restype = bs.read(2);

	if ( restype > 1 )
	{
		return ERROR_RESIDUE;
	}

	const auto    part_order = static_cast< unsigned >( bs.read(4) );
	unsigned long begin      = order;

	unsigned long end = ( blocksize >> part_order );

	for (unsigned k = 0; ( k < ( 1U << part_order ) ) && ( end <= blocksize ); ++k)
	{
		auto len = static_cast< unsigned >( restype == 0 ? bs.read(4) : bs.read(5) );

		if ( len != ( ( restype == 0 ) ? 15 : 31 ) ) // rice coded
		{                                            /// \todo This is the ONLY place ReadRice is called
			for (unsigned long p = begin; p < end; p++)
			{
				unsigned long u;
				long          u2;

				u  = rice0(bs, len);
				u2 = static_cast< long >( u >> 1 );

				if ( ( u & 1 ) != 0 )
				{
					curve[p] = -1L - u2;
				}
				else
				{
					curve[p] = u2;
				}
			}
		}
		else // fixed width
		{
			len = static_cast< unsigned >( bs.read(5) );

			for (unsigned long p = begin; p < end; p++)
			{
				curve[p] = static_cast< long >( bs.read(len) ); /// \todo check cast
			}
		}

		begin = end;
		end  += ( blocksize >> part_order );
	}

	return OK;
}

/***************************************************************************/ /**
 *******************************************************************************/
audio::sample_rate_type Flac::sample_rate() const
{
	return sample_rate_;
}

/***************************************************************************/ /**
 *******************************************************************************/
bool Flac::getMeta(MetaData& m) const
{
	if ( havemeta )
	{
		std::map< std::string, std::string >::const_iterator it;
		it = meta.find(std::string("ARTIST") );

		if ( it != meta.end() )
		{
			m.assign(MetaData::Artist, it->second);
		}

		it = meta.find(std::string("ALBUM") );

		if ( it != meta.end() )
		{
			m.assign(MetaData::Album, it->second);
		}

		it = meta.find(std::string("TITLE") );

		if ( it != meta.end() )
		{
			m.assign(MetaData::Title, it->second);
		}
	}

	return havemeta;
}

/***************************************************************************/ /**
 *******************************************************************************/
bool Flac::IsProbablyPacket(
	const unsigned char* pPacket,
	std::size_t          nPacket
)
{
	if ( ( pPacket == nullptr ) || ( nPacket < 6 ) )
	{
		return false; // bound checking
	}

	unsigned long w = beread< 32 >(reinterpret_cast< const std::byte* >( pPacket ) ); // Flag word

	if ( ( w & mSync ) != FrameaudioSync )
	{
		return false;
	}

	// Might be a packet. Check for invalid fields
	if ( ( w & mReserved ) != 0 )
	{
		return false;
	}

	if ( ( w & mBlockSize ) == invalidBlockSize )
	{
		return false;
	}

	if ( ( w & mSampleRate ) == invalidSampleRate )
	{
		return false;
	}

	if ( ( w & mChannels ) >= reserveChannelsLo )
	{
		return false;
	}

	if ( ( w & mSampleSize ) == invalidSampleSize )
	{
		return false;
	}

	// CRC check
	std::size_t nbytes = 5; // at least this many bytes in header

	// Add bytes for "UTF-8" coded sample number
	auto x = static_cast< unsigned >( pPacket[4] );

	if ( x >= 0xFEU )
	{
		nbytes += 6;
	}
	else if ( x >= 0xFCU )
	{
		nbytes += 5;
	}
	else if ( x >= 0xF8U )
	{
		nbytes += 4;
	}
	else if ( x >= 0xF0U )
	{
		nbytes += 3;
	}
	else if ( x >= 0xE0U )
	{
		nbytes += 2;
	}
	else if ( x >= 0xC0U )
	{
		++nbytes;
	}

	switch ( w & mBlockSize ) // Add bytes if block size was stored in header
	{
	case stored8BlockSize:
		++nbytes;
		break;
	case stored16BlockSize:
		nbytes += 2;
	default:
		break;
	}

	switch ( w & mSampleRate ) // Add bytes if sample rate was in header
	{
	case stored8SampleRate:
		++nbytes;
		break;
	case stored10SampleRate:
	case stored16SampleRate:
		nbytes += 2;
	default:
		break;
	}

	if ( nPacket <= nbytes )
	{
		return false;
	}

	// Perform the actual CRC calculation
	unsigned y = 0;

	for (std::size_t i = 0; i < nbytes; i++)
	{
		y = crctable[( y ^ pPacket[i] ) % 256U];
	}

	return y == static_cast< unsigned >( pPacket[nbytes] );
}

namespace
{
/***************************************************************************/ /**
 *  Signed wrapper for ReadBits
 *  \todo Drop need for y. Calculate unsigned version in x and static_cast on return.
 *******************************************************************************/
long ReadBitsS(
	IBEBitstream& r,
	unsigned int  nbits
)
{
	unsigned long x = 0;

	if ( nbits != 0 )
	{
		const unsigned long s = 1UL << ( nbits - 1 );

		x = r.read(nbits);

		if ( x >= s ) // cast two's complement unsigned to a signed value
		{
			x = ( x - s ) - s;
		}
	}

	return static_cast< long >( x );
}

} // end anonymous namespace

/*
 * > Can the bits per sample change from frame to frame?
 *
 * no, not in the native container (i.e. fLaC header + metadata +
 * frames).  in a raw streaming situation where you are getting only
 * frames it might be advantageous to support it but I don't know of
 * anything that works like that.
 *
 * > Can constant or verbatim subframes have left/side,
 * > right/side, or mid/side channel assignment? If so, does the
 * > difference channel have an extra bit per sample?
 *
 * good question... yes it is possible and the difference channel
 * always has the extra bps.
 *
 * > Is it a fatal error if the minimum frame size is larger
 * > than the maximum frame size (in STREAMINFO)? What about the
 * > minimum number of samples being larger than the maximum
 * > number of samples?
 *
 * they're not fatal.  the STREAMINFO is to help inform the decoder
 * but if you detect invalid values it should be treated as if those
 * values are unknown.
 *
 * a decoder implementation with limited memory is allowed to fail
 * if it can't get accurate STREAMINFO that it needs though.  this is
 * an implementation detail though.
 *
 * > If the residue bit width is given by an escape code, can
 * > this escape code be zero? What is the effect of reading
 * > "zero" bits? (assuming that the stream is not
 * > advanced and 0 is the value read)
 *
 * another good one.  an escape code of 0 is invalid; should be
 * treated as reserved for use by a future encoder.
 *
 * > The MD5 is calculated on the "unencoded data",
 * > but this leaves a lot of room for interpretation. Does this
 * > mean the data produced by the decoder, or the data that was
 * > passed to the encoded when the file was made? If it means on
 * > produced data, are the channels interleaved? Stored as big
 * > or little endian? If it means the original data, how can we
 * > be sure of the format of that data?
 *
 * this is ill-specified, but it is the channel-interleaved signed
 * samples, samples truncated to the smalled number of bytes that
 * can represent the sample, then converted to bytes by shifting
 * LSBs 8 bits at a time into the MD5 accumulator.  see
 * src/libFLAC/md5.c:format_input_()
 *
 * > How do I handle negative QLPC shifts, officially? Treat
 * > them as positive left shifts? Or treat them as an error
 * > condition? Or treat them as a right shift with the shift
 * > index taken modulo 32 (as apparently happened with earlier
 * > versions of the reference code)?
 *
 * this one's going to haunt me forever... the reference encoder has
 * avoided negative shifts entirely since 1.1.4.  negative shifts
 * were meant to be positive shifts in the opposite direction but
 * they were implemented directly with << >> and in C negative shifts
 * are undefined.
 *
 * treat as an error for now to avoid bad behavior but I need to
 * come back to this to specify exactly.
 *
 * > If I read a signed value that is one bit, what is
 * > "1" equal to?
 *
 * -1
 *
 * > If a sample rate of zero is specified, is this a fatal
 * > error? Or should it be ignored?
 *
 * it is technically an invalid sample rate, even though the codec
 * does not care about the sample rate (it is merely stored and passed
 * on for playback).  I think the reference decoder does not fail on
 * it but it should.
 *
 * Josh
 */
