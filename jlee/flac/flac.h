#ifndef FLAC_HPP
#define FLAC_HPP

#include <iostream>
#include <map>
#include <string>
#include <vector>

#include "audio/codec.h"
#include "audio/encapsulation.h"
class IBEBitstream;

class FlacFrame
	: public audio::Encapsulation
{
	typedef std::map< audio::sample_off, std::streampos > SeekTable;

	std::istream&  is;
	std::streampos iStream;
	unsigned long  fixed_blocksize;

	SeekTable         seektable;
	audio::sample_off current_pos;

	void seek(audio::sample_off) override;
	audio::sample_off getSampleNo(const unsigned char*);
	void buildSeekTable();

	FlacFrame(const FlacFrame&);            // non-copyable
	FlacFrame& operator=(const FlacFrame&); // non-copyable
public:
	explicit FlacFrame(std::istream&);
	Packet ReadPacket() override;
	bool eos() const override;
	void prepare() override;
	audio::sample_off position() const override;
	audio::sample_off length() const override;
};

class Flac
	: public audio::Codec
{
	// Data members
	signed long int*                     pbuf{ nullptr };
	signed long int*                     buf[8];
	unsigned long                        gensamples{ 0 };
	audio::sample_rate_type              sample_rate_;
	unsigned                             channels{ 0 };
	unsigned                             bits_per_sample{ 0 };
	unsigned                             max_samples{ 0 };
	bool                                 havemeta{ false };
	std::map< std::string, std::string > meta;

	enum Error
	{
		OK,
		ERROR_BUFFER,
		ERROR_NO_DATA,
		ERROR_POINTER,
		ERROR_EOS,
		ERROR_UNSUPPORTED,
		ERROR_INIT,
		ERROR_RESERVED,
		ERROR_PADDING,
		ERROR_SIGNATURE,
		ERROR_OGG,
		ERROR_AUDIO_SYNC,
		ERROR_STREAM_VERSION,
		ERROR_META_BLOCKTYPE,
		ERROR_SAMPLE_RATE,
		ERROR_SAMPLES,
		ERROR_BLOCKSIZE,
		ERROR_CHANNEL_ASSIGNMENT,
		ERROR_BITS_PER_SAMPLE,
		ERROR_SUBFRAME_TYPE,
		ERROR_RESIDUE,
		ERROR_QLPC_SHIFT,
		ERROR_QLPC_PRECISION,
		ERROR_QLPC_ORDER,
		ERROR_FIXED_ORDER,
		ERROR_ORDER
	};

	enum masks
	{
		mSampleSize = 0x0EUL,
		mChannels = 0xF0UL,
		mSampleRate = 0xF00UL,
		mBlockSize = 0xF000UL,
		mReserved = 0x20001UL,
		mSync = 0xFFFC0000UL
	};

	enum BlockTypes
	{
		StreamInfo = 0,
		Padding = 1,
		Application = 2,
		SeekTable = 3,
		Comment = 4,
		CueSheet = 5,
		Picture = 6,
		Invalid = 127
	};

	enum StereoEncoding
	{
		Independent,
		LeftSide,
		RightSide,
		MidSide
	};

	Error ProcessF2OHeader(const Packet&);
	Error ProcessMetaData(const Packet&, std::size_t);
	Error ProcessStreamInfo(const Packet&, std::size_t);
	void  ProcessComment(const Packet&, std::size_t);
	Error DecodeAudio(const Packet&);
	Error LPCSubframe(IBEBitstream&, signed long int*, unsigned, unsigned, unsigned long);
	Error FixedSubframe(IBEBitstream&, signed long int*, unsigned, unsigned, unsigned long);
	Error ResidueDecode(IBEBitstream&, signed long int*, unsigned long, unsigned);

	Flac(const Flac&)            = delete; // non-copyable
	Flac& operator=(const Flac&) = delete; // non-copyable
public:
	Flac();
	~Flac() override;

	void DecodePacket(const Packet&) override;
	samples_type DumpAudio() override;
	void seek() override;
	bool getMeta(MetaData&) const override;
	audio::sample_rate_type sample_rate() const override;

	static bool IsProbablyPacket(const unsigned char*, std::size_t);
};

const audio::sample_rate_type sample_rate_table[12] = // Sample rate in 10s of Hz
{{ 0 }, { 88200 }, { 176400 }, { 192000 }, { 8000 }, { 16000 },
	{ 22050 }, { 24000 }, { 32000 }, { 44100 }, { 48000 }, { 96000 }};

const unsigned long FlacaudioSync  = UINT32_C(0x3FFE);
const unsigned long FrameaudioSync = UINT32_C(0xFFF80000);

const unsigned long stored8BlockSize   = UINT32_C(0x6000);
const unsigned long stored16BlockSize  = UINT32_C(0x7000);
const unsigned long invalidBlockSize   = 0UL;
const unsigned long stored8SampleRate  = UINT32_C(0x0C00);
const unsigned long stored16SampleRate = UINT32_C(0x0D00);
const unsigned long stored10SampleRate = UINT32_C(0x0E00);
const unsigned long invalidSampleRate  = UINT32_C(0x0F00);
const unsigned long reserveChannelsLo  = UINT32_C(0xB0);
const unsigned long invalidSampleSize  = UINT32_C(0x0E);

const unsigned char crctable[256]
    = { 0x00, 0x07, 0x0e, 0x09, 0x1c, 0x1b, 0x12, 0x15, 0x38, 0x3f, 0x36, 0x31, 0x24, 0x23, 0x2a, 0x2d, 0x70, 0x77,
	    0x7e, 0x79, 0x6c, 0x6b, 0x62, 0x65, 0x48, 0x4f, 0x46, 0x41, 0x54, 0x53, 0x5a, 0x5d, 0xe0, 0xe7, 0xee, 0xe9,
	    0xfc, 0xfb, 0xf2, 0xf5, 0xd8, 0xdf, 0xd6, 0xd1, 0xc4, 0xc3, 0xca, 0xcd, 0x90, 0x97, 0x9e, 0x99, 0x8c, 0x8b,
	    0x82, 0x85, 0xa8, 0xaf, 0xa6, 0xa1, 0xb4, 0xb3, 0xba, 0xbd, 0xc7, 0xc0, 0xc9, 0xce, 0xdb, 0xdc, 0xd5, 0xd2,
	    0xff, 0xf8, 0xf1, 0xf6, 0xe3, 0xe4, 0xed, 0xea, 0xb7, 0xb0, 0xb9, 0xbe, 0xab, 0xac, 0xa5, 0xa2, 0x8f, 0x88,
	    0x81, 0x86, 0x93, 0x94, 0x9d, 0x9a, 0x27, 0x20, 0x29, 0x2e, 0x3b, 0x3c, 0x35, 0x32, 0x1f, 0x18, 0x11, 0x16,
	    0x03, 0x04, 0x0d, 0x0a, 0x57, 0x50, 0x59, 0x5e, 0x4b, 0x4c, 0x45, 0x42, 0x6f, 0x68, 0x61, 0x66, 0x73, 0x74,
	    0x7d, 0x7a, 0x89, 0x8e, 0x87, 0x80, 0x95, 0x92, 0x9b, 0x9c, 0xb1, 0xb6, 0xbf, 0xb8, 0xad, 0xaa, 0xa3, 0xa4,
	    0xf9, 0xfe, 0xf7, 0xf0, 0xe5, 0xe2, 0xeb, 0xec, 0xc1, 0xc6, 0xcf, 0xc8, 0xdd, 0xda, 0xd3, 0xd4, 0x69, 0x6e,
	    0x67, 0x60, 0x75, 0x72, 0x7b, 0x7c, 0x51, 0x56, 0x5f, 0x58, 0x4d, 0x4a, 0x43, 0x44, 0x19, 0x1e, 0x17, 0x10,
	    0x05, 0x02, 0x0b, 0x0c, 0x21, 0x26, 0x2f, 0x28, 0x3d, 0x3a, 0x33, 0x34, 0x4e, 0x49, 0x40, 0x47, 0x52, 0x55,
	    0x5c, 0x5b, 0x76, 0x71, 0x78, 0x7f, 0x6a, 0x6d, 0x64, 0x63, 0x3e, 0x39, 0x30, 0x37, 0x22, 0x25, 0x2c, 0x2b,
	    0x06, 0x01, 0x08, 0x0f, 0x1a, 0x1d, 0x14, 0x13, 0xae, 0xa9, 0xa0, 0xa7, 0xb2, 0xb5, 0xbc, 0xbb, 0x96, 0x91,
	    0x98, 0x9f, 0x8a, 0x8d, 0x84, 0x83, 0xde, 0xd9, 0xd0, 0xd7, 0xc2, 0xc5, 0xcc, 0xcb, 0xe6, 0xe1, 0xe8, 0xef,
	    0xfa, 0xfd, 0xf4, 0xf3 };
#endif // ifndef FLAC_HPP
