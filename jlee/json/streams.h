#ifndef JL_JSON_STREAMS_H
#define JL_JSON_STREAMS_H

#include <filesystem>

#include "memorymap/mmap.h"

#include "serializer.h"
#include "deserializer.h"

namespace jl
{
namespace json
{
extern serializer out;
extern serializer err;

template< typename T >
std::string to_string(const T& x)
{
	std::stringstream ss;

	serializer t{ ss };

	t << x;

	return ss.str();
}

template< typename T >
bool from_string(
	const std::string_view& s,
	T&                      t
)
{
	Deserializer< const char*, T > v(t);

	const char*       p = s.data();
	const std::size_t n = s.size();

	return detail::validate_utf8(p, p + n, v);
}

template< typename T >
bool save(
	const std::filesystem::path& path,
	const T&                     t
)
{
	return jl::serialization::save< serializer, T >(path, t);
}

template< typename T >
bool load(
	const std::filesystem::path& path,
	T&                           t
)
{
	const memory_map m(path);

	if ( m.is_open() )
	{
		return from_string(std::string_view( reinterpret_cast< const char* >( m.data() ), m.size() ), t);
	}

	/// @todo Fallback to another method. Or just have memory_map handle that
	return false;
}

}
}

#endif // JL_JSON_STREAMS_H
