#ifndef JL_JSON_VISITOR_H
#define JL_JSON_VISITOR_H

#include <iostream>

namespace jl
{
namespace json
{
class VisitorBase
{
public:
	/// @todo Extend to a "parse result" type and have error codes as well as "found null"
	enum class event
	{
		null_literal,
		true_literal,
		false_literal,
		number,
		string,
		array_begin,
		array_end,
		array_separator,
		object_begin,
		object_end,
		object_separator,
		field_separator,
		field_name
	};

	virtual ~VisitorBase() = default;
protected:
	template< typename Iterator >
	void write_helper(
		std::ostream& os,
		Iterator      first,
		Iterator      last
	)
	{
		os << std::string(first, last);
	}

	void write_helper(
		std::ostream& os,
		const char*   first,
		const char*   last
	)
	{
		os.write(first, last - first);
	}

};

template< typename Iterator >
class Visitor
	: public VisitorBase
{
public:
	virtual void operator()(event)
	{
	}

	virtual void operator()(
		event,
		Iterator,
		Iterator
	)
	{
	}

};

template< typename Iterator >
class Printer
	: public Visitor< Iterator >
{
public:
	using event = typename Visitor< Iterator >::event;

	explicit Printer(std::ostream& os_)
		: os(os_)
	{
	}

	void operator()(event e) override
	{
		switch ( e )
		{
		case event::array_begin:
			os.put('[');
			break;
		case event::array_end:
			os.put(']');
			break;
		case event::array_separator:
			os.put(',');
			break;
		case event::object_begin:
			os.put('{');
			break;
		case event::object_end:
			os.put('}');
			break;
		case event::object_separator:
			os.put(',');
			break;
		case event::field_separator:
			os.put(':');
			break;
		default:
			break;
		} // switch

	}

	void operator()(
		event    e,
		Iterator first,
		Iterator last
	) override
	{
		switch ( e )
		{
		case event::null_literal:
		case event::true_literal:
		case event::false_literal:
		case event::number:
		case event::string:
		case event::field_name:
			this->write_helper(os, first, last);
			break;
		default:
			this->operator()(e);
			break;
		}
	}
private:
	std::ostream& os;
};
}
}

#endif // JL_JSON_VISITOR_H
