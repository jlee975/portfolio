#ifndef JL_JSON_JSON_H
#define JL_JSON_JSON_H

#include <cstddef>
#include <string_view>
#include <filesystem>

namespace jl
{
namespace json
{
bool validate(const std::byte*, std::size_t);
bool validate(const std::string_view&);
bool validate_file(const std::filesystem::path&);

/// @todo Accept an output object for prettify. Currently just writes to cout
/// @todo Take options for prettify (ex., tabs, spaces, etc.)
bool prettify(const std::byte*, std::size_t);
bool prettify(const std::string_view&);
bool prettify_file(const std::filesystem::path&);
}
}
#endif // JL_JSON_JSON_H
