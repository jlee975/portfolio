#ifndef JL_JSON_SAX_H
#define JL_JSON_SAX_H

#include <iostream>

namespace jl
{
namespace json
{
/// @todo Template on char traits
/// @todo Should have access to unescape strings, convert numbers to built-in types
class SAXBase
{
public:
	enum error_code
	{
		expected_null,            // got part of 'null', but not the whole thing
		expected_true,            // got part of a 'true', but not the whole thing
		expected_false,           // got part of a 'false', but not the whole thing
		expected_mantissa,        // number has a . in it, but no digit following it
		expected_exponent,        // got an 'e' or 'E', but no integer after
		invalid_unicode,          // found an invalid code point in a string
		bad_escape,               // got a '\' without a proper escape code
		bad_string,               // disallowed character in string (ex., control characters)
		unterminated_string,      // Did not find closing double quote for string
		expected_value,           // Expected to be able to parse some value, but that failed
		unterminated_array,       // Did not find closing bracket for array
		expected_array_separator, // Array element should be followed by ',' or ']'
		expected_field,           // Expected field name of object
		expected_object_separator // Object field should be followed by ',' or '}'
	};
	virtual ~SAXBase() = default;
protected:
	template< typename Iterator >
	bool write_helper(
		std::ostream& os,
		Iterator      first,
		Iterator      last
	)
	{
		/// @todo Performance
		os << std::string(first, last);
		return !os.bad();
	}

	bool write_helper(
		std::ostream& os,
		const char*   first,
		const char*   last
	)
	{
		os.write(first, last - first);
		return !os.bad();
	}

	bool write_helper(
		std::ostream&    os,
		const std::byte* first,
		const std::byte* last
	)
	{
		return write_helper( os, reinterpret_cast< const char* >( first ), reinterpret_cast< const char* >( last ) );
	}

	void error_event()
	{
	}

};

/// @todo Helper functions for converting text representations of numbers into ints
/// @todo Helper functions for unescaping strings
template< typename Iterator >
class SAX
	: public SAXBase
{
public:
	/// @todo take two iterators
	virtual void error(
		error_code,
		const Iterator&
	)
	{
		error_event();
	}

	virtual bool null_literal(
		const Iterator&,
		const Iterator&
	)
	{
		return true;
	}

	virtual bool true_literal(
		const Iterator&,
		const Iterator&
	)
	{
		return true;
	}

	virtual bool false_literal(
		const Iterator&,
		const Iterator&
	)
	{
		return true;
	}

	/// @todo Take additional data like iterators for sign, integer, decimal, exponent
	virtual bool number(
		const Iterator&,
		const Iterator&
	)
	{
		return true;
	}

	virtual bool string(
		const Iterator&,
		const Iterator&
	)
	{
		return true;
	}

	virtual bool array_begin(const Iterator&)
	{
		return true;
	}

	virtual bool array_end(
		const Iterator&,
		bool empty
	)
	{
		return true;
	}

	virtual bool array_separator(const Iterator&)
	{
		return true;
	}

	virtual bool object_begin(const Iterator&)
	{
		return true;
	}

	virtual bool object_end(
		const Iterator&,
		bool empty
	)
	{
		return true;
	}

	/// @todo Rename. This is when we encounter ':' after a field name
	virtual bool member_begin(const Iterator&)
	{
		return true;
	}

	/// @todo Rename. This is when we encounter ',' after a field object
	virtual bool member_end(const Iterator&)
	{
		return true;
	}

	virtual bool field_name(
		const Iterator&,
		const Iterator&
	)
	{
		return true;
	}

};

/// @todo Options to insert appropriate whitespace
template< typename Iterator >
class Printer
	: public SAX< Iterator >
{
public:
	using error_code = SAXBase::error_code;

	explicit Printer(std::ostream& os_)
		: os(os_)
	{
	}

	bool null_literal(
		const Iterator& first,
		const Iterator& last
	) override
	{
		return this->write_helper(os, first, last);
	}

	bool true_literal(
		const Iterator& first,
		const Iterator& last
	) override
	{
		return this->write_helper(os, first, last);
	}

	bool false_literal(
		const Iterator& first,
		const Iterator& last
	) override
	{
		return this->write_helper(os, first, last);
	}

	bool number(
		const Iterator& first,
		const Iterator& last
	) override
	{
		return this->write_helper(os, first, last);
	}

	bool string(
		const Iterator& first,
		const Iterator& last
	) override
	{
		return this->write_helper(os, first, last);
	}

	bool array_begin(const Iterator&) override
	{
		/// @todo LBRACKET
		os.put('[');
		return !os.bad();
	}

	bool array_end(
		const Iterator&,
		bool
	) override
	{
		os.put(']');
		return !os.bad();
	}

	bool array_separator(const Iterator&) override
	{
		os.put(',');
		return !os.bad();
	}

	bool object_begin(const Iterator&) override
	{
		os.put('{');
		return !os.bad();
	}

	bool object_end(
		const Iterator&,
		bool
	) override
	{
		os.put('}');
		return !os.bad();
	}

	bool member_begin(const Iterator&) override
	{
		os.put(':');
		return true;
	}

	bool member_end(const Iterator&) override
	{
		os.put(',');
		return true;
	}

	bool field_name(
		const Iterator& first,
		const Iterator& last
	) override
	{
		return this->write_helper(os, first, last);
	}

	void error(
		error_code      ec,
		const Iterator& it
	) override
	{
		SAX< Iterator >::error(ec, it);
	}
private:
	std::ostream& os;
};
}
}

#endif // JL_JSON_SAX_H
