/**
 * @todo Many functions may run faster if we know that first + 3 < last (for example). I.e., that our scan can assume there'll
 * be many bytes. For example, when scanning for a quote we can process a word at a time (a la Hacker's Delight)
 * @todo Lots of magic values could be calculated from code points
 * @bug Functions that parse input iterators do not call the visitor. Need some kind of wrapping iterator that will cache read
 * values, so that we can go back and use the data for the visitor.
 */
#include "json.h"

#include <fstream>
#include <iostream>

#include "memorymap/mmap.h"

#include "parse.h"

namespace jl
{
namespace json
{
bool validate(
	const std::byte* p_,
	std::size_t      n
)
{
	const auto p = reinterpret_cast< const char* >( p_ );

	return detail::validate_utf8(p, p + n);
}

bool validate(const std::string_view& s)
{
	const auto        p = s.data();
	const std::size_t n = s.length();

	return detail::validate_utf8(p, p + n);
}

bool validate_file(const std::filesystem::path& filepath)
{
	const memory_map m(filepath);

	if ( m.is_open() )
	{
		return jl::json::validate( m.data(), m.size() );
	}

	return false;
}

bool prettify(
	const std::byte* p_,
	std::size_t      n
)
{
	Printer< const char* > v(std::cout);

	const auto p = reinterpret_cast< const char* >( p_ );

	return detail::validate_utf8(p, p + n, v);
}

bool prettify(const std::string_view& s)
{
	Printer< const char* > v(std::cout);

	const auto        p = reinterpret_cast< const char* >( s.data() );
	const std::size_t n = s.length();

	return detail::validate_utf8(p, p + n, v);
}

bool prettify_file(const std::filesystem::path& filepath)
{
	const memory_map m(filepath);

	if ( m.is_open() )
	{
		return jl::json::prettify( m.data(), m.size() );
	}

	return false;
}

}
}
