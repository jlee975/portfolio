#include "parse.h"

namespace jl
{
namespace json
{
/// @todo This should really use the same logic as the parsing. Esp recognizing Unicode surrogates.
std::string unescape(
	const char* first,
	const char* last
)
{
	std::string s;

	const std::size_t n = last - first;

	s.reserve(n);

	std::size_t i = 0;

	while ( i < n )
	{
		std::size_t j = i;

		while ( j < n && first[j] != detail::REVERSE_SOLIDUS )
		{
			++j;
		}

		s.append(first + i, j - i);

		if ( j == n )
		{
			break;
		}

		if ( j + 1 < n )
		{
			switch ( first[j + 1] )
			{
			case detail::QUOTE:
				s.push_back(detail::QUOTE);
				break;
			case detail::REVERSE_SOLIDUS:
				s.push_back(detail::REVERSE_SOLIDUS);
				break;
			case detail::SOLIDUS:
				s.push_back(detail::SOLIDUS);
				break;
			case detail::MINORB:
				s.push_back(detail::BACKSPACE);
				break;
			case detail::MINORF:
				s.push_back(detail::FF);
				break;
			case detail::MINORN:
				s.push_back(detail::LF);
				break;
			case detail::MINORR:
				s.push_back(detail::CR);
				break;
			case detail::MINORT:
				s.push_back(detail::TAB);
				break;
			case detail::MINORU:
				throw std::logic_error("Not implemented");
			default:
				throw std::runtime_error("Malformed JSON string");
			} // switch

			i = j + 2;
		}
		else
		{
			throw std::runtime_error("Malformed JSON string");
		}
	}

	return s;
}

}
}
