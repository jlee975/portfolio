#pragma once

#include "serialization/out.h"

namespace jl
{
namespace json
{
// If an output fails (like, an input value is invalid), formatter must set the fail bit of os
class serializer
	: public serialization::ostream_base
{
public:
	using ostream_base::ostream_base;

	void writeb(bool x) override;

	void writei(long long x) override;

	void writeu(unsigned long long x) override;

	void writenull() override;

	void writef(long double x) override;

	void start_array() override;

	void array_separator() override;

	void finish_array() override;

	void start_object() override;

	void finish_object() override;

	void object_separator() override;

	void field_name(const char*, std::size_t) override;

	void write_string(const char*, std::size_t) override;
};
}
}
