#ifndef TOKENS_H
#define TOKENS_H

namespace jl
{
namespace json
{
namespace detail
{
/// @todo char8_t
// Constants for some of the Unicode characters that JSON uses
const char BACKSPACE = '\x8', TAB = '\x9', LF = '\xa', FF = '\xc', CR = '\xd', SPACE = '\x20', QUOTE = '\x22',
           PLUS = '\x2b', COMMA = '\x2c',
           MINUS = '\x2d', PERIOD = '\x2e', SOLIDUS = '\x2f', DIGIT0 = '\x30', DIGIT9 = '\x39',
           COLON = '\x3a', MAJORE = '\x45', LBRACKET = '\x5b', REVERSE_SOLIDUS = '\x5c', RBRACKET = '\x5d',
           MINORA = '\x61', MINORB = '\x62', MINORE = '\x65', MINORF = '\x66', MINORL = '\x6c', MINORN = '\x6e',
           MINORR = '\x72', MINORS = '\x73',
           MINORT = '\x74', MINORU = '\x75', LCURLY = '\x7b', RCURLY = '\x7d';
}
}
}

#endif // TOKENS_H
