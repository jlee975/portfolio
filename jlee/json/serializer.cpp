#include "serializer.h"

#include <iostream>
#include <cstring>

#include "tokens.h"

namespace
{
char to_hex(std::uint_least32_t x)
{
	if ( x <= 9 )
	{
		return static_cast< char >( jl::json::detail::DIGIT0 + x );
	}

	return static_cast< char >( jl::json::detail::MINORA + ( x - 10 ) );
}

}


namespace jl
{
namespace json
{
void serializer::field_name(
	const char* s,
	std::size_t n
)
{
	write_string(s, n);
	os << detail::COLON;
}

void serializer::writenull()
{
	static const char s[4] = { detail::MINORN, detail::MINORU, detail::MINORL, detail::MINORL };

	os.write(s, 4);
}

void serializer::write_string(
	const char* s,
	std::size_t n
)
{
	std::size_t i = 0;

	os << detail::QUOTE;

	while ( true )
	{
		// Write unescaped sequences
		std::size_t j = i;

		while ( j < n && detail::SPACE <= s[j] && s[j] <= 0x10ffff && s[j] != detail::QUOTE && s[j] != detail::REVERSE_SOLIDUS )
		{
			++j;
		}

		if ( j != i )
		{
			os.write(s + i, j - i);
		}

		// Check end of string condition
		if ( j == n )
		{
			break;
		}

		// Write byte that needs escaping
		if ( s[j] == detail::QUOTE || s[j] == detail::REVERSE_SOLIDUS )
		{
			os << detail::REVERSE_SOLIDUS << s[j];
		}
		else if ( s[j] == detail::BACKSPACE )
		{
			os << detail::REVERSE_SOLIDUS << detail::MINORB;
		}
		else if ( s[j] == detail::TAB )
		{
			os << detail::REVERSE_SOLIDUS << detail::MINORT;
		}
		else if ( s[j] == detail::LF )
		{
			os << detail::REVERSE_SOLIDUS << detail::MINORN;
		}
		else if ( s[j] == detail::FF )
		{
			os << detail::REVERSE_SOLIDUS << detail::MINORF;
		}
		else if ( s[j] == detail::CR )
		{
			os << detail::REVERSE_SOLIDUS << detail::MINORR;
		}
		else
		{
			const std::uint_least32_t x = static_cast< unsigned char >( s[j] );

			if ( x <= UINT32_C(0xffff) )
			{
				// Basic Multilingual Plane
				os << detail::REVERSE_SOLIDUS << detail::MINORU
				   << to_hex( ( x >> 12 ) & 0xf) << to_hex( ( x >> 8 ) & 0xf)
				   << to_hex( ( x >> 4 ) & 0xf) << to_hex( ( x >> 0 ) & 0xf);
			}
			else
			{
				const std::uint_least32_t rem = x - UINT32_C(0x10000);
				const std::uint_least32_t hi  = ( rem >> 10 ) + UINT32_C(0xd800);
				const std::uint_least32_t lo  = ( rem & UINT32_C(0x3ff) ) + UINT32_C(0xdc00);

				os << detail::REVERSE_SOLIDUS << detail::MINORU
				   << to_hex( ( hi >> 12 ) & 0xf) << to_hex( ( hi >> 8 ) & 0xf)
				   << to_hex( ( hi >> 4 ) & 0xf) << to_hex( ( hi >> 0 ) & 0xf)
				   << detail::REVERSE_SOLIDUS << detail::MINORU
				   << to_hex( ( lo >> 12 ) & 0xf) << to_hex( ( lo >> 8 ) & 0xf)
				   << to_hex( ( lo >> 4 ) & 0xf) << to_hex( ( lo >> 0 ) & 0xf);
			}
		}

		i = j + 1;
	}

	os << detail::QUOTE;
}

void serializer::writeb(bool x)
{
	if ( x )
	{
		static const char s[4] = { detail::MINORT, detail::MINORR, detail::MINORU, detail::MINORE };

		os.write(s, 4);
	}
	else
	{
		static const char s[5] = { detail::MINORF, detail::MINORA, detail::MINORL, detail::MINORS, detail::MINORE };

		os.write(s, 5);
	}
}

void serializer::writei(long long x)
{
	/// @todo Performance .Calling writeu is inefficient
	if ( x >= 0 )
	{
		writeu(static_cast< unsigned long long >( x ) );
	}
	else
	{
		os << detail::MINUS;
		writeu(-static_cast< unsigned long long >( x ) );
	}
}

void serializer::writeu(unsigned long long x)
{
	/// @todo Performance
	// Enough to fit 18446744073709551615
	constexpr std::size_t w = std::numeric_limits< unsigned long long >::digits10 + 1;
	char                  digits[w];

	std::size_t n = w;

	do
	{
		digits[n - 1] = detail::DIGIT0 + ( x % 10 );
		x            /= 10;
		--n;
	}
	while ( x != 0 );

	os.write(digits + n, w - n);
}

void serializer::writef(long double x)
{
	/// @bug Standardized output. Ex., 'e+07' vs 'E+7'
	os << x;
}

void serializer::start_array()
{
	os << detail::LBRACKET;
}

void serializer::array_separator()
{
	os << detail::COMMA;
}

void serializer::finish_array()
{
	os << detail::RBRACKET;
}

void serializer::start_object()
{
	os << detail::LCURLY;
}

void serializer::finish_object()
{
	os << detail::RCURLY;
}

void serializer::object_separator()
{
	os << detail::COMMA;
}

serializer out{ std::cout };
serializer err{ std::cerr };
}
}
