#pragma once

#include <charconv>
#include <memory>
#include <typeindex>
#include <map>

#include "serialization/category.h"

#include "parse.h"

namespace jl
{
namespace json
{
template< typename Iterator >
class DeserializerBase
	: public SAX< Iterator >
{
public:
	void error(
		SAXBase::error_code,
		const Iterator&
	) override
	{

	}

	bool null_literal(
		const Iterator&,
		const Iterator&
	) override
	{
		return this->reject();
	}

	bool true_literal(
		const Iterator&,
		const Iterator&
	) override
	{

		return this->reject();
	}

	bool false_literal(
		const Iterator&,
		const Iterator&
	) override
	{
		return this->reject();
	}

	bool number(
		const Iterator&,
		const Iterator&
	) override
	{
		return this->reject();
	}

	bool string(
		const Iterator&,
		const Iterator&
	) override
	{
		return this->reject();
	}

	bool array_begin(const Iterator&) override
	{
		return this->reject();
	}

	bool array_end(
		const Iterator&,
		bool
	) override
	{
		return this->reject();
	}

	bool array_separator(const Iterator&) override
	{
		return this->reject();
	}

	bool object_begin(const Iterator&) override
	{
		return this->reject();
	}

	bool object_end(
		const Iterator&,
		bool
	) override
	{
		return this->reject();
	}

	bool member_begin(const Iterator&) override
	{
		return this->reject();
	}

	bool member_end(const Iterator&) override
	{
		return this->reject();
	}

	bool field_name(
		const Iterator&,
		const Iterator&
	) override
	{
		return this->reject();
	}
protected:
	bool reject() const
	{
		return false;
	}

};

template< typename Iterator, typename T >
class Deserializer;

template< typename Iterator >
class Deserializer< Iterator, bool >
	: public DeserializerBase< Iterator >
{
public:
	explicit Deserializer(bool& b_)
		: b(b_)
	{
	}

	bool true_literal(
		const Iterator&,
		const Iterator&
	) override
	{
		b = true;
		return true;
	}

	bool false_literal(
		const Iterator&,
		const Iterator&
	) override
	{
		b = false;
		return true;
	}
private:
	bool& b;
};

template< typename Iterator, serialization::signed_category T >
class Deserializer< Iterator, T >
	: public DeserializerBase< Iterator >
{
public:
	explicit Deserializer(T& i_)
		: i(i_)
	{
	}

	bool number(
		const Iterator& first,
		const Iterator& last
	) override
	{
		const auto res = std::from_chars(first, last, i, 10);

		return res.ec == std::errc() && res.ptr == last;
	}
private:
	T& i;
};

template< typename Iterator, serialization::unsigned_category T >
class Deserializer< Iterator, T >
	: public DeserializerBase< Iterator >
{
public:
	explicit Deserializer(T& u_)
		: u(u_)
	{
	}

	bool number(
		const Iterator& first,
		const Iterator& last
	) override
	{
		const auto res = std::from_chars(first, last, u, 10);

		return res.ec == std::errc() && res.ptr == last;
	}
private:
	T& u;
};

template< typename Iterator, serialization::floating_category T >
class Deserializer< Iterator, T >
	: public DeserializerBase< Iterator >
{
public:
	explicit Deserializer(T& x_)
		: x(x_)
	{
	}

	bool number(
		const Iterator& first,
		const Iterator& last
	) override
	{
		/// @bug Set the format correctly
		const auto res = std::from_chars(first, last, x);

		return res.ec == std::errc() && res.ptr == last;
	}
private:
	T& x;
};

template< typename Iterator, serialization::string_category T >
class Deserializer< Iterator, T >
	: public DeserializerBase< Iterator >
{
public:
	explicit Deserializer(T& s_)
		: s(s_)
	{
	}

	bool string(
		const Iterator& first,
		const Iterator& last
	) override
	{
		/// @todo Unescape
		s = unescape(first + 1, last - 1);
		return true;
	}
private:
	T& s;
};

template< typename Iterator, serialization::call_function_category T >
class Deserializer< Iterator, T >
	: public DeserializerBase< Iterator >
{
public:
	explicit Deserializer(T& s_)
		: s(s_)
	{
	}

	bool string(
		const Iterator& first,
		const Iterator& last
	) override
	{
		/// @todo Unescape
		return s.deserialize( unescape(first + 1, last - 1) );
	}
private:
	T& s;
};

/// @todo Should support maps which have two issues: no push_back and E has a const member
/// Solution seems to be something like using the std::map constructor that takes iterators.
/// Write special iterators that walk the SAX tree
template< typename Iterator, serialization::sequence_category T >
class Deserializer< Iterator, T >
	: public DeserializerBase< Iterator >
{
	using E = typename serialization::sequence_element< T >::type;
public:
	explicit Deserializer(T& s_)
		: s(s_)
	{
	}

	bool null_literal(
		const Iterator& first,
		const Iterator& last
	) override
	{
		if ( proxy )
		{
			return proxy->null_literal(first, last);
		}

		return this->reject();
	}

	bool true_literal(
		const Iterator& first,
		const Iterator& last
	) override
	{
		if ( proxy )
		{
			return proxy->true_literal(first, last);
		}

		return this->reject();
	}

	bool false_literal(
		const Iterator& first,
		const Iterator& last
	) override
	{
		if ( proxy )
		{
			return proxy->false_literal(first, last);
		}

		return this->reject();
	}

	bool number(
		const Iterator& first,
		const Iterator& last
	) override
	{
		if ( proxy )
		{
			return proxy->number(first, last);
		}

		return this->reject();
	}

	bool string(
		const Iterator& first,
		const Iterator& last
	) override
	{
		if ( proxy )
		{
			return proxy->string(first, last);
		}

		return this->reject();
	}

	bool array_begin(const Iterator& it) override
	{
		if ( ++depth > 1 )
		{
			// proxy
			return proxy->array_begin(it);
		}

		s     = T();
		proxy = std::make_unique< Deserializer< Iterator, E > >(e);

		return true;
	}

	bool array_end(
		const Iterator& it,
		bool            empty
	) override
	{
		if ( --depth != 0 )
		{
			// proxy
			return proxy->array_end(it, empty);
		}

		if ( !empty )
		{
			s.insert( s.end(), std::move(e) );
		}

		proxy.reset();
		return true;
	}

	bool array_separator(const Iterator& it) override
	{
		if ( depth > 1 )
		{
			// proxy
			return proxy->array_separator(it);
		}

		// This is the array separator for this object
		s.insert( s.end(), std::move(e) );
		proxy = std::make_unique< Deserializer< Iterator, E > >(e);
		return true;
	}

	bool object_begin(const Iterator& it) override
	{
		if ( proxy )
		{
			return proxy->object_begin(it);
		}

		return this->reject();
	}

	bool object_end(
		const Iterator& it,
		bool            empty
	) override
	{
		if ( proxy )
		{
			return proxy->object_end(it, empty);
		}

		return this->reject();
	}

	bool member_begin(const Iterator& it) override
	{
		if ( proxy )
		{
			return proxy->member_begin(it);
		}

		return this->reject();
	}

	bool member_end(const Iterator& it) override
	{
		if ( proxy )
		{
			return proxy->member_end(it);
		}

		return this->reject();
	}

	bool field_name(
		const Iterator& first,
		const Iterator& last
	) override
	{
		if ( proxy )
		{
			return proxy->field_name(first, last);
		}

		return this->reject();
	}
private:
	T&                                              s;
	E                                               e;
	std::unique_ptr< DeserializerBase< Iterator > > proxy;
	unsigned long long                              depth = 0;
};

/// @todo Need to define what happens if member is not deserialized. Default value? Error?
template< typename Iterator, jl::serialization::reflectable_d T >
class Deserializer< Iterator, T >
	: public DeserializerBase< Iterator >
{
	class Visitor
	{
public:
		explicit Visitor(Deserializer* parent)
			: m_parent(parent)
		{
		}

		template< typename B >
		void operator()(B& b)
		{
			m_parent->bases.emplace(std::type_index(typeid( B ) ), std::make_unique< Deserializer< Iterator, B > >(b) );
		}

		template< typename P >
		void operator()(
			T&                      s2,
			P                       ptom,
			const std::string_view& label
		)
		{
			m_parent->members.emplace(label, std::make_unique< Deserializer< Iterator, std::remove_reference_t< decltype( s2.*ptom ) > > >(s2.*ptom) );
		}
private:

		Deserializer* m_parent;
	};
public:
	explicit Deserializer(T& s_)
		: s(s_)
	{
		if constexpr ( std::is_default_constructible_v< T >)
		{
			/// @todo What to do if not default constructible
			/// @todo How does this interact with base classes
			s = T();
		}

		/// @todo Calculating a map for every instance seems inefficient. We know this
		/// stuff at compile time
		Visitor f(this);

		// For each member, create a deserializer
		jl::reflection::visit< typename serialization::deserialize_type< T >::type >(s, f);
	}

	bool null_literal(
		const Iterator& first,
		const Iterator& last
	) override
	{
		if ( proxy )
		{
			return proxy->null_literal(first, last);
		}

		return this->reject();
	}

	bool true_literal(
		const Iterator& first,
		const Iterator& last
	) override
	{
		if ( proxy )
		{
			return proxy->true_literal(first, last);
		}

		return this->reject();
	}

	bool false_literal(
		const Iterator& first,
		const Iterator& last
	) override
	{
		if ( proxy )
		{
			return proxy->false_literal(first, last);
		}

		return this->reject();
	}

	bool number(
		const Iterator& first,
		const Iterator& last
	) override
	{
		if ( proxy )
		{
			return proxy->number(first, last);
		}

		return this->reject();
	}

	bool string(
		const Iterator& first,
		const Iterator& last
	) override
	{
		if ( proxy )
		{
			return proxy->string(first, last);
		}

		return this->reject();
	}

	bool array_begin(const Iterator& it) override
	{
		if ( proxy )
		{
			return proxy->array_begin(it);
		}

		return this->reject();
	}

	bool array_end(
		const Iterator& it,
		bool            empty
	) override
	{
		if ( proxy )
		{
			return proxy->array_end(it, empty);
		}

		return this->reject();
	}

	bool array_separator(const Iterator& it) override
	{
		if ( proxy )
		{
			return proxy->array_separator(it);
		}

		return this->reject();
	}

	bool object_begin(const Iterator& it) override
	{
		if ( ++depth > 1 )
		{
			// proxy
			return proxy->object_begin(it);
		}

		return true;
	}

	bool object_end(
		const Iterator& it,
		bool            empty
	) override
	{
		if ( --depth != 0 )
		{
			// proxy
			return proxy->object_end(it, empty);
		}

		/// @bug Any fields that weren't processed should be default initialized... or something
		proxy.reset();
		members.clear();
		return true;
	}

	bool member_begin(const Iterator& it) override
	{
		if ( depth > 1 )
		{
			return proxy->member_begin(it);
		}

		return true;
	}

	bool member_end(const Iterator& it) override
	{
		if ( depth > 1 )
		{
			return proxy->member_end(it);
		}

		proxy.reset();
		return true;
	}

	bool field_name(
		const Iterator& first,
		const Iterator& last
	) override
	{
		// create proxy
		if ( depth > 1 )
		{
			return proxy->field_name(first, last);
		}

		/// @todo Transparent lookup
		const auto label = unescape(first + 1, last - 1);

		auto it = members.find(label);

		if ( it == members.end() )
		{
			// not a member, or already deserialized
			return this->reject();
		}

		proxy = std::move(it->second);
		members.erase(it);
		return true;
	}
private:
	T& s;

	// Proxies for members
	std::map< std::string_view, std::unique_ptr< DeserializerBase< Iterator > > > members;

	std::map< std::type_index, std::unique_ptr< DeserializerBase< Iterator > > > bases;

	// We're in some inner object
	std::unique_ptr< DeserializerBase< Iterator > > proxy;
	unsigned long long                              depth = 0;
};

template< typename Iterator, serialization::pointer_category T >
class Deserializer< Iterator, T >
	: public DeserializerBase< Iterator >
{
public:
	explicit Deserializer(T& p_)
		: p(p_)
	{
	}

	bool null_literal(
		const Iterator& first,
		const Iterator& last
	) override
	{
		if ( proxy )
		{
			return proxy->null_literal(first, last);
		}

		p = nullptr;
		return true;
	}

	bool true_literal(
		const Iterator& first,
		const Iterator& last
	) override
	{
		return create_proxy().true_literal(first, last);
	}

	bool false_literal(
		const Iterator& first,
		const Iterator& last
	) override
	{
		return create_proxy().false_literal(first, last);
	}

	bool number(
		const Iterator& first,
		const Iterator& last
	) override
	{
		return create_proxy().number(first, last);
	}

	bool string(
		const Iterator& first,
		const Iterator& last
	) override
	{
		return create_proxy().string(first, last);
	}

	bool array_begin(const Iterator& it) override
	{
		return create_proxy().array_begin(it);
	}

	bool array_end(
		const Iterator& it,
		bool            empty
	) override
	{
		return create_proxy().array_end(it, empty);
	}

	bool array_separator(const Iterator& it) override
	{
		return create_proxy().array_separator(it);
	}

	bool object_begin(const Iterator& it) override
	{
		return create_proxy().object_begin(it);
	}

	bool object_end(
		const Iterator& it,
		bool            empty
	) override
	{
		return create_proxy().object_end(it, empty);
	}

	bool member_begin(const Iterator& it) override
	{
		return create_proxy().member_begin(it);
	}

	bool member_end(const Iterator& it) override
	{
		return create_proxy().member_end(it);
	}

	bool field_name(
		const Iterator& first,
		const Iterator& last
	) override
	{
		return create_proxy().field_name(first, last);
	}
private:
	DeserializerBase< Iterator >& create_proxy()
	{
		if ( !proxy )
		{
			p     = T(new typename T::element_type);
			proxy = std::make_unique< Deserializer< Iterator, typename T::element_type > >(*p);
		}

		return *proxy;
	}

	T&                                              p;
	std::unique_ptr< DeserializerBase< Iterator > > proxy;
};

template< typename Iterator, serialization::tuple_category T >
class Deserializer< Iterator, T >
	: public DeserializerBase< Iterator >
{
	template< std::size_t... Is >
	Deserializer(
		T& s_,
		std::index_sequence< Is... >
	)
		: s(s_), proxies{
		                 std::make_unique< Deserializer< Iterator, std::tuple_element_t< Is, T > > >( std::get< Is >(s) )...
		}
	{

	}
public:
	explicit Deserializer(T& s_)
		: Deserializer(
			s_,
			std::make_index_sequence< std::tuple_size_v< T > >( )
		)
	{
	}

	bool null_literal(
		const Iterator& first,
		const Iterator& last
	) override
	{
		if ( iproxy < std::tuple_size_v< T >)
		{
			return proxies[iproxy]->null_literal(first, last);
		}

		return this->reject();
	}

	bool true_literal(
		const Iterator& first,
		const Iterator& last
	) override
	{
		if ( iproxy < std::tuple_size_v< T >)
		{
			return proxies[iproxy]->true_literal(first, last);
		}

		return this->reject();
	}

	bool false_literal(
		const Iterator& first,
		const Iterator& last
	) override
	{
		if ( iproxy < std::tuple_size_v< T >)
		{
			return proxies[iproxy]->false_literal(first, last);
		}

		return this->reject();
	}

	bool number(
		const Iterator& first,
		const Iterator& last
	) override
	{
		if ( iproxy < std::tuple_size_v< T >)
		{
			return proxies[iproxy]->number(first, last);
		}

		return this->reject();
	}

	bool string(
		const Iterator& first,
		const Iterator& last
	) override
	{
		if ( iproxy < std::tuple_size_v< T >)
		{
			return proxies[iproxy]->string(first, last);
		}

		return this->reject();
	}

	bool array_begin(const Iterator& it) override
	{
		if ( ++depth > 1 )
		{
			// proxy
			return proxies[iproxy]->array_begin(it);
		}

		iproxy = 0;

		return true;
	}

	bool array_end(
		const Iterator& it,
		bool            empty
	) override
	{
		if ( --depth != 0 )
		{
			// proxy
			return proxies[iproxy]->array_end(it, empty);
		}

		if ( ++iproxy != std::tuple_size_v< T >)
		{
			return this->reject();
		}

		return true;
	}

	bool array_separator(const Iterator& it) override
	{
		if ( depth > 1 )
		{
			// proxy
			return proxies[iproxy]->array_separator(it);
		}

		// This is the array separator for this object
		++iproxy;
		return true;
	}

	bool object_begin(const Iterator& it) override
	{
		if ( iproxy < std::tuple_size_v< T >)
		{
			return proxies[iproxy]->object_begin(it);
		}

		return this->reject();
	}

	bool object_end(
		const Iterator& it,
		bool            empty
	) override
	{
		if ( iproxy < std::tuple_size_v< T >)
		{
			return proxies[iproxy]->object_end(it, empty);
		}

		return this->reject();
	}

	bool member_begin(const Iterator& it) override
	{
		if ( iproxy < std::tuple_size_v< T >)
		{
			return proxies[iproxy]->member_begin(it);
		}

		return this->reject();
	}

	bool member_end(const Iterator& it) override
	{
		if ( iproxy < std::tuple_size_v< T >)
		{
			return proxies[iproxy]->member_end(it);
		}

		return this->reject();
	}

	bool field_name(
		const Iterator& first,
		const Iterator& last
	) override
	{
		if ( iproxy < std::tuple_size_v< T >)
		{
			return proxies[iproxy]->field_name(first, last);
		}

		return this->reject();
	}
private:
	T&                                                                                    s;
	std::array< std::unique_ptr< DeserializerBase< Iterator > >, std::tuple_size_v< T > > proxies;
	std::size_t                                                                           iproxy = std::tuple_size_v< T >;
	unsigned long long                                                                    depth  = 0;
};

/// @todo We're constantly copying over the proxy value, but we should really only do it once
/// when we've successfully done it
template< typename Iterator, serialization::proxy_category T >
class Deserializer< Iterator, T >
	: public DeserializerBase< Iterator >
{
	using U = typename serialization::category< T >::proxy;
public:
	explicit Deserializer(T& p_)
		: p(p_), q(), proxy(q)
	{
	}

	bool null_literal(
		const Iterator& first,
		const Iterator& last
	) override
	{
		if ( proxy.null_literal(first, last) )
		{
			p = T(q);
			return true;
		}

		return this->reject();
	}

	bool true_literal(
		const Iterator& first,
		const Iterator& last
	) override
	{
		if ( proxy.true_literal(first, last) )
		{
			p = T(q);
			return true;
		}

		return this->reject();
	}

	bool false_literal(
		const Iterator& first,
		const Iterator& last
	) override
	{
		if ( proxy.false_literal(first, last) )
		{
			p = T(q);
			return true;
		}

		return this->reject();
	}

	bool number(
		const Iterator& first,
		const Iterator& last
	) override
	{
		if ( proxy.number(first, last) )
		{
			p = T(q);
			return true;
		}

		return this->reject();
	}

	bool string(
		const Iterator& first,
		const Iterator& last
	) override
	{
		if ( proxy.string(first, last) )
		{
			p = T(q);
			return true;
		}

		return this->reject();
	}

	bool array_begin(const Iterator& it) override
	{
		if ( proxy.array_begin(it) )
		{
			p = T(q);
			return true;
		}

		return this->reject();
	}

	bool array_end(
		const Iterator& it,
		bool            empty
	) override
	{
		if ( proxy.array_end(it, empty) )
		{
			p = T(q);
			return true;
		}

		return this->reject();
	}

	bool array_separator(const Iterator& it) override
	{
		if ( proxy.array_separator(it) )
		{
			p = T(q);
			return true;
		}

		return this->reject();
	}

	bool object_begin(const Iterator& it) override
	{
		if ( proxy.object_begin(it) )
		{
			p = T(q);
			return true;
		}

		return this->reject();
	}

	bool object_end(
		const Iterator& it,
		bool            empty
	) override
	{
		if ( proxy.object_end(it, empty) )
		{
			p = T(q);
			return true;
		}

		return this->reject();
	}

	bool member_begin(const Iterator& it) override
	{
		if ( proxy.member_begin(it) )
		{
			p = T(q);
			return true;
		}

		return this->reject();
	}

	bool member_end(const Iterator& it) override
	{
		if ( proxy.member_end(it) )
		{
			p = T(q);
			return true;
		}

		return this->reject();
	}

	bool field_name(
		const Iterator& first,
		const Iterator& last
	) override
	{
		if ( proxy.field_name(first, last) )
		{
			p = T(q);
			return true;
		}

		return this->reject();
	}
private:
	T& p;
	U  q;

	Deserializer< Iterator, U > proxy;
};
}
}
