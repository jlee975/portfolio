#ifndef JL_JSON_PARSE_H
#define JL_JSON_PARSE_H

#include <iterator>

#include "unicode/unicode.h"

#include "sax.h"
#include "tokens.h"

// Wrap all iterators as forward iterators that return unsigned char to ensure we can support such types
#undef FORWARD_ITERATOR_DEBUG

#ifdef FORWARD_ITERATOR_DEBUG
template< typename Iterator >
class ForwardIteratorWrapper
{
public:
	using iterator_category = std::forward_iterator_tag;
	using difference_type   = typename std::iterator_traits< Iterator >::difference_type;
	using value_type        = unsigned char;
	using pointer_type      = unsigned char*;
	using reference         = unsigned char&;

	ForwardIteratorWrapper()
	{
	}

	explicit ForwardIteratorWrapper(const Iterator& it_)
		: it(it_)
	{
	}

	friend bool operator==(
		const ForwardIteratorWrapper& l,
		const ForwardIteratorWrapper& r
	)
	{
		return l.it == r.it;
	}

	friend bool operator!=(
		const ForwardIteratorWrapper& l,
		const ForwardIteratorWrapper& r
	)
	{
		return l.it != r.it;
	}

	ForwardIteratorWrapper& operator++()
	{
		++it;
		return *this;
	}

	ForwardIteratorWrapper operator++(int)
	{
		return { it++ };
	}

	unsigned char operator*() const
	{
		return *it;
	}
private:
	Iterator it;
};
#endif // ifdef FORWARD_ITERATOR_DEBUG

namespace jl
{
namespace json
{
namespace detail
{
// Note this is not an optional. second will contain an iterator regardless of first, but possibly indicating an error position
template< typename I >
struct parse_result
{
	explicit operator bool() const
	{
		return first;
	}
	const I& operator*() const
	{
		return second;
	}

	bool first;
	I second;
};

/* The type returned by this helper will be unsigned and suitably for passing to test
 * functions that take uint_fast32_t
 */
/// @todo char8_t may be preferable for many types. char32_t, though, is only supposed
/// to have valid code points, so it may not be suitable for possibly bad data
template< typename I >
using uchar = std::make_unsigned_t< std::remove_cvref_t< decltype( *( std::declval< I >() ) ) > >;

/* Check if a unicode character is json whitespace. We do this by testing a bit
 * mask that encodes all whitespace characters
 */
constexpr bool is_whitespace(const std::uint_fast32_t c)
{
	// The lhs handles characters 0 and >=33. The rhs handles 1-32
	const auto s = c - UINT32_C(1);

	// This makes a bitmask corresponding to space, tab, cr, and lf
	constexpr auto m = ( UINT32_C(1) << ( 9 - 1 ) )
			   | ( UINT32_C(1) << ( 10 - 1 ) )
			   | ( UINT32_C(1) << ( 13 - 1 ) )
			   | ( UINT32_C(1) << ( 32 - 1 ) );

	// Alternative: (s < SPACE ? UINT32_C(1) : UINT32_C(0)) & ( m >> s );
	return s < SPACE && ( ( ( m >> s ) & UINT32_C(1) ) != 0 );
}

/// @todo We can probably omit checking first against last if we check the tail of the json
/// for non-whitespace characters. If a quick check early on shows a '}' or a '"', for example,
/// we can be sure any valid call to skip_whitespace will find that sentinel character before
/// hitting "last"
/// @todo Typically looking for { " : 0-9 t f n
/// @todo By far the most frequent case is whitespace will be only a couple bytes
/// @todo Review these utf8 functions and decide which could support, for example, utf-32. This function,
/// for example, should probably work just fine
/// @todo Perhaps put the utf8 parse strings in a namespace
template< typename Iterator >
Iterator skip_whitespace_utf8(
	Iterator first,
	const Iterator last
)
{
	while ( first != last && is_whitespace(static_cast< uchar< Iterator > >( *first ) ) )
	{
		++first;
	}

	return first;
}

/* Check if a unicode character is a json digit. Using unsigned arithmetic this
 * is a single comparison
 */
constexpr bool is_digit(std::uint_fast32_t c)
{
	return c - DIGIT0 <= DIGIT9 - DIGIT0;
}

template< typename Iterator >
Iterator skip_digits_utf8(
	Iterator first,
	const Iterator last
)
{
	while ( first != last && is_digit(static_cast< uchar< Iterator > >( *first ) ) )
	{
		++first;
	}

	return first;
}

/* Check if a unicode character forms a two character escape sequence in a json
 * string.
 */
constexpr bool is_escape(const std::uint_fast32_t c)
{
	const auto s = c - UINT32_C(86);

	return s < UINT32_C(32) ? ( ( ( UINT32_C(0xd1011040) >> s ) & 1 ) != 0 ) : ( c == QUOTE || c == SOLIDUS );
}

/* Check if a character is "plain" text, i.e., string data that isn't of
 * special interest to the parser, i.e. most data
 */
constexpr bool is_plain_text(const std::uint_fast32_t c)
{
	// return (c - UINT32_C(0x23) < UINT32_C(0x5c) - UINT32_C(0x23) ) || (c - UINT32_C(0x5d) < UINT32_C(0x80) - UINT32_C(0x5d)) || (c - UINT32_C(0x20) < UINT32_C(0x2));

	return unicode::is_basic_latin(c) && ( c != QUOTE ) && ( c != REVERSE_SOLIDUS );
}

#if 0
bool is_plain_text_mw(unsigned long long x)
{
	const unsigned long long m = 31;
	const unsigned long long n = 128;

#define haszero(x) (((x) - 0x0101010101010101ULL) & ~(x) & 0x8080808080808080ULL)
#define hasless(x,n) (((x)-(~0ULL/255)*(n))&~(x)&~0ULL/255*128)

	return (hasless(x,32) == 0)
	    && (hasless(0xffffffffffffffffull-x, 128) == 0)
	    && ((haszero((x) ^ (~0ULL/255 * (0x22ull)))) == 0)
	    && ((haszero((x) ^ (~0ULL/255 * (0x5cull)))) == 0);
}
#endif

template< typename Iterator >
Iterator skip_plain_text_utf8(
	Iterator first,
	const Iterator last
)
{
#if 0
	The following code aligns the pointer to first and then tries to do a multibyte search for non-plain
	text characters. It works, but it slower than a straight search.
	Probably because most strings are not going to be very long
	/// @todo Or unsigned char or std::byte
	if constexpr (std::is_same_v<Iterator, const char*>)
	{
		void* adj = const_cast< char*>(static_cast< const char*>(first));
		std::size_t size = last - first;

		if (std::align(alignof(unsigned long long), sizeof(unsigned long long), adj, size) != nullptr)
		{
			// Do the first part
			while ( first != adj )
			{
				if (!is_plain_text(static_cast< uchar< Iterator > >( *first ) ))
				{
					return first;
				}
				++first;
			}

			// Do the aligned part
			while ( static_cast< std::size_t >(last - first) >= sizeof(unsigned long long) && is_plain_text_mw(*reinterpret_cast<const unsigned long long*>(first)))
			{
				first += sizeof(unsigned long long);
			}

			const std::size_t nn = sizeof(unsigned long long);

			const auto* tail = first;

			// Do the tail part
			while ( first != last)
			{
				if (!is_plain_text(static_cast< uchar< Iterator > >( *first ) ))
				{
					return first;
				}
				++first;
			}

			return first;
		}
	}
#endif
	// Fallback
	while ( first != last && is_plain_text(static_cast< uchar< Iterator > >( *first ) ) )
	{
		++first;
	}

	return first;
}

/* Test that bottom 4 bytes of x are hexadecimal digits.This is done in
 * parallel, by determining where digits and a-f are (in the masks is_dec and
 * is_hex, respectively). From each byte, we then subtract '0' or 'a'-10 to
 * get their integral values. Finally, we pack the 32 bytes into 16 by dropping
 * every other 4 bits. 25 ops, 1 branch with excellent prediction. Saved about
 * 80 bytes in opcodes compared to the obvious implementation. Worth it.
 */
constexpr std::uint_fast32_t from_hex(const std::uint_fast32_t x)
{
	// Remove the MSB of each byte. Should all be zeros...
	const std::uint_fast32_t x1 = x & UINT32_C(0x7f7f7f7f);

	// ... but we'll track the ones that do have the MSB set and disqualify them later
	const std::uint_fast32_t y = UINT32_C(0x80808080) & ( ~x );

	// Set MSB if <= '9' AND >= '0'
	const std::uint_fast32_t is_dec = ( UINT32_C(0xb9b9b9b9) - x1 ) & ( x1 + UINT32_C(0x50505050) ) & y;

	// lower case x
	const std::uint_fast32_t x2 = x1 | UINT32_C(0x20202020);

	// Checks if between 'a' and 'f'
	const std::uint_fast32_t is_hex = ( UINT32_C(0xE6E6E6E6) - x2 ) & ( x2 + UINT32_C(0x1f1f1f1f) ) & y;

	// Check that all bytes are hexadecimal
	if ( ( is_dec | is_hex ) == UINT32_C(0x80808080) )
	{
		// Convert ascii bytes to integer equivalents, in parallel
		const std::uint_fast32_t m  = is_dec - ( is_dec >> 7 );
		const std::uint_fast32_t z1 = x2 - ( UINT32_C(0x57575757) ^ ( UINT32_C(0x67676767) & m ) );
		const std::uint_fast32_t z2 = z1 | ( z1 >> 4 );
		const std::uint_fast32_t z3 = ( z2 & 0xff ) | ( ( z2 >> 8 ) & 0xff00 );

		return z3;
	}

	return unicode::INVALID;
}

template< std::forward_iterator Iterator >
parse_result< Iterator > parse_null_utf8(
	const Iterator first,
	const Iterator last
)
{
	Iterator it = first;

	if ( it != last && *it == MINORU && ++it != last && *it == MINORL && ++it != last && *it == MINORL )
	{
		return { true, ++it };
	}

	return { false, it };
}

template< std::random_access_iterator Iterator >
parse_result< Iterator > parse_null_utf8(
	const Iterator first,
	const Iterator last
)
{
	Iterator it = first;

	if ( 2 < last - it && it[0] == MINORU && it[1] == MINORL && it[2] == MINORL )
	{
		return { true, it + 3 };
	}

	return { false, it };
}

template< std::forward_iterator Iterator >
parse_result< Iterator > parse_true_utf8(
	const Iterator first,
	const Iterator last
)
{
	Iterator it = first;

	if ( it != last && *it == MINORR && ++it != last && *it == MINORU && ++it != last && *it == MINORE )
	{
		return { true, ++it };
	}

	return { false, it };
}

template< std::random_access_iterator Iterator >
parse_result< Iterator > parse_true_utf8(
	const Iterator first,
	const Iterator last
)
{
	Iterator it = first;

	if ( 2 < last - it && it[0] == MINORR && it[1] == MINORU && it[2] == MINORE )
	{
		return { true, it + 3 };
	}

	return { false, it };
}

template< std::forward_iterator Iterator >
parse_result< Iterator > parse_false_utf8(
	const Iterator first,
	const Iterator last
)
{
	Iterator it = first;

	if ( it != last && *it == MINORA && ++it != last && *it == MINORL && ++it != last && *it == MINORS &&
	     ++it != last && *it == MINORE
	)
	{
		return { true, ++it };
	}

	return { false, it };
}

/// @todo parse_literal<'F','A','L','S','E'> that switches on forward_iterator or random_access_iterator
/// Reuse for "true" and "null"
/// @todo We have other big endian code, maybe use that to read the four bytes and do one compare. But
/// the compiler might be better than us at that
template< std::random_access_iterator Iterator >
parse_result< Iterator > parse_false_utf8(
	const Iterator first,
	const Iterator last
)
{
	Iterator it = first;

	if ( 3 < last - it && it[0] == MINORA && it[1] == MINORL && it[2] == MINORS && it[3] == MINORE )
	{
		return { true, it + 4 };
	}

	return { false, it };
}

template< typename Iterator >
parse_result< Iterator > parse_mantissa_utf8(
	const Iterator         start,
	const Iterator         first,
	const Iterator         last,
	SAX< Iterator >& sax
)
{
	Iterator it = first;

	if ( it != last && *it == PERIOD )
	{
		if ( ++it == last || !is_digit(*it) )
		{
			sax.error(SAXBase::expected_mantissa, it);
			return { false, it };
		}

		it = skip_digits_utf8(it, last);
	}

	if ( it != last && ( *it == MINORE || *it == MAJORE ) )
	{
		// parse exponent
		if ( ++it != last )
		{
			if ( ( *it == PLUS || *it == MINUS ) && ++it == last )
			{
				sax.error(SAXBase::expected_exponent, it);
				return { false, it };
			}

			if ( !is_digit(*it) )
			{
				sax.error(SAXBase::expected_exponent, it);
				return { false, it };
			}

			it = skip_digits_utf8(++it, last);
		}
		else
		{
			sax.error(SAXBase::expected_exponent, it);
			return { false, it };
		}
	}

	if ( !sax.number(start, it) )
	{
		return { false, start };
	}

	return { true, it };
}

/* Read four hex digits and returns the integer value
 */
template< std::input_iterator Iterator >
std::pair< std::uint_fast32_t, Iterator > uescape_utf8(
	const Iterator first,
	const Iterator last
)
{
	using U = uchar< Iterator >;
	Iterator it = first;

	if ( it != last )
	{
		const std::uint_fast32_t x1 = static_cast< U >( *it );

		if ( ++it != last )
		{
			const std::uint_fast32_t x2 = static_cast< U >( *it );

			if ( ++it != last )
			{
				const std::uint_fast32_t x3 = static_cast< U >( *it );

				if ( ++it != last )
				{
					const std::uint_fast32_t x4 = static_cast< U >( *it );
					++it;

					const std::uint_fast32_t x = ( x1 << 24 ) | ( x2 << 16 ) | ( x3 << 8 ) | x4;

					return { from_hex(x), it };
				}
			}
		}
	}

	return { unicode::INVALID, last };
}

template< std::random_access_iterator Iterator >
std::pair< std::uint_fast32_t, Iterator > uescape_utf8(
	const Iterator it,
	const Iterator last
)
{
	using U = uchar< Iterator >;

	if ( 3 < last - it )
	{
		/// @todo be32
		const std::uint_fast32_t x1 = static_cast< U >( it[0] );
		const std::uint_fast32_t x2 = static_cast< U >( it[1] );
		const std::uint_fast32_t x3 = static_cast< U >( it[2] );
		const std::uint_fast32_t x4 = static_cast< U >( it[3] );

		const std::uint_fast32_t x = ( x1 << 24 ) | ( x2 << 16 ) | ( x3 << 8 ) | x4;

		/// @todo On LE architectures, read the word in LE order and do the byteswap once we've
		/// confirmed the value is indeed hex
		return { from_hex(x), it + 4 };
	}

	return { unicode::INVALID, last };
}

/* Parse a JSON string. The return value is one-past-the-last character in the
 * string. In the event of an error, first is returned (i.e., nothing parsed)
 * Upon entering this function first[-1] was a QUOTE.
 */
/// @todo The SAX user will probably want to do something with this string, like unescaping it.
/// We'll have done a lot of the work here, it would be nice to not have to redo things
template< typename Iterator >
parse_result< Iterator > parse_string_utf8(
	const Iterator         first,
	const Iterator         last,
	SAX< Iterator >& sax
)
{
	using U = uchar< Iterator >;

	// Scan for non-normal characters
	for (Iterator it = skip_plain_text_utf8(first, last); it != last;
	     it = skip_plain_text_utf8(it, last)
	)
	{
		if ( *it == QUOTE )
		{
			++it;

			return { true, it };
		}

		if ( *it == REVERSE_SOLIDUS )
		{
			if ( ++it != last && is_escape( static_cast< U >( *it ) ) )
			{
				const bool isu = ( *it == MINORU );
				++it;

				if ( isu )
				{
					// Expect 4 hexadecimal digits
					auto res = uescape_utf8(it, last);

					if ( res.first == unicode::INVALID )
					{
						// didn't get 4 hexadecimal digits
						sax.error(SAXBase::invalid_unicode, it);
						return { false, it };
					}

					it = res.second;

					if ( unicode::is_first_surrogate(res.first) )
					{
						// UTF-16 surrogate pair - requires a second encoding

						if ( it != last && *it == REVERSE_SOLIDUS && ++it != last && *it == MINORU )
						{
							res = uescape_utf8(++it, last);

							const std::uint_fast32_t surr2 = res.first;

							if ( surr2 == unicode::INVALID || !unicode::is_second_surrogate(surr2) )
							{
								// Did not get second part of surrogate
								sax.error(SAXBase::invalid_unicode, it);
								return { false, it };
							}

							it = res.second;
						}
						else
						{
							// didn't get the second surrogate
							sax.error(SAXBase::invalid_unicode, it);
							return { false, it };
						}
					}
				}
			}
			else
			{
				sax.error(SAXBase::bad_escape, it);
				return { false, it };
			}
		}
		else if ( unicode::is_utf8_mb( static_cast< U >( *it ) ) )
		{
			// UTF-8 multibyte sequence
			// len is 1, 2, or 3
			const unsigned len = unicode::sequence_length(static_cast< U >( *it ) );

			std::uint_fast32_t code    = 0;
			std::uint_fast32_t topbits = 0; // bits 7 and 8 should be zero

			/// @todo Unroll the loop
			for (unsigned i = 0; i < len; ++i)
			{
				if ( ++it == last )
				{
					sax.error(SAXBase::invalid_unicode, it);
					return { false, it };
				}

				const std::uint_fast32_t x = static_cast< uchar< Iterator > >( *it ) ^ UINT32_C(0x80); // top two bits must be 10
				topbits |= x;
				code     = ( code << 6 ) | x;
			}

			code += ( len == 1 ? UINT32_C(0x80) : ( len == 2 ? UINT32_C(0x800) : UINT32_C(0x10000) ) );

			if ( ( topbits & UINT32_C(0xC0) ) || !unicode::is_valid_codepoint(code) )
			{
				sax.error(SAXBase::invalid_unicode, it);
				return { false, it };
			}

			++it;
		}
		else
		{
			// control code or illegal utf8 character
			sax.error(SAXBase::bad_string, it);
			return { false, it };
		}
	}

	sax.error(SAXBase::unterminated_string, last);
	return { false, last };
}

template< typename Iterator >
parse_result< Iterator > parse_any_utf8(Iterator, Iterator, SAX< Iterator >&);

template< typename Iterator >
parse_result< Iterator > parse_array_utf8(
	Iterator         it,
	const Iterator         last,
	SAX< Iterator >& sax
)
{
	if ( !sax.array_begin(it) )
	{
		return { false, it };
	}

	++it;
	it = skip_whitespace_utf8(it, last);

	if ( it != last && *it == RBRACKET )
	{
		if ( !sax.array_end(it, true) )
		{
			return { false, it };
		}

		return { true, ++it };
	}

	// expecting any
	while ( true )
	{
		/// @todo An array typically has the same type repeated, so
		/// we can expect the next type
		const auto res = parse_any_utf8(it, last, sax);

		if ( !res )
		{
			sax.error(SAXBase::expected_value, it);
			return { false, it };
		}

		it = skip_whitespace_utf8(*res, last);

		if ( it == last )
		{
			sax.error(SAXBase::unterminated_array, it);
			return { false, it };
		}

		// Are we done?
		if ( *it == RBRACKET )
		{
			if ( !sax.array_end(it, false) )
			{
				return { false, it };
			}

			return { true, ++it };
		}

		if ( *it != COMMA )
		{
			sax.error(SAXBase::expected_array_separator, it);
			return { false, it };
		}

		if ( !sax.array_separator(it) )
		{
			return { false, it };
		}

		++it;
	}
}

template< typename Iterator >
parse_result< Iterator > parse_object_utf8(
	Iterator         it,
	const Iterator         last,
	SAX< Iterator >& sax
)
{
	if ( !sax.object_begin(it) )
	{
		return { false, it };
	}

	++it;
	it = skip_whitespace_utf8(it, last);

	if ( it != last && *it == RCURLY )
	{
		// empty object
		if ( !sax.object_end(it, true) )
		{
			return { false, it };
		}

		return { true, ++it };
	}

	// Parse "string" : any
	while ( true )
	{
		// Look for a string
		if ( it == last || *it != QUOTE )
		{
			sax.error(SAXBase::expected_field, it);
			return { false, it };
		}

		// Parse the string
		const auto t = parse_string_utf8(std::next(it), last, sax);

		if ( !t )
		{
			sax.error(SAXBase::expected_field, it);
			return { false, it };
		}

		/// @todo Pass information on string composition (ex., if it has any escapes, pure ascii, etc.)
		if ( !sax.field_name(it, *t) )
		{
			return { false, it };
		}

		// Parse the colon
		Iterator jt = skip_whitespace_utf8(*t, last);

		if ( jt == last || *jt != COLON )
		{
			sax.error(SAXBase::expected_object_separator, jt);
			return { false, jt };
		}

		if ( !sax.member_begin(jt) )
		{
			return { false, jt };
		}

		// Parse a value
		const auto res = parse_any_utf8(++jt, last, sax);

		if ( !res )
		{
			sax.error(SAXBase::expected_value, jt);
			return { false, jt };
		}

		it = skip_whitespace_utf8(*res, last);

		if ( it == last )
		{
			sax.error(SAXBase::expected_object_separator, it);
			return { false, it };
		}

		// Are we done?
		if ( *it == RCURLY )
		{
			if ( !sax.object_end(it, false) )
			{
				return { false, it };
			}

			return { true, ++it };
		}

		if ( *it != COMMA )
		{
			sax.error(SAXBase::expected_object_separator, it);
			return { false, it };
		}

		if ( !sax.member_end(it) )
		{
			return { false, it };
		}

		++it;
		it = skip_whitespace_utf8(it, last);
	}
}

/* Parse a single JSON value (ex., number, string). The returned value is one-
 * past-the-last character of the value. In the event of an error, first is
 * returned (i.e., nothing was parsed)
 */
template< typename Iterator >
parse_result< Iterator > parse_any_utf8(
	const Iterator         first,
	const Iterator         last,
	SAX< Iterator >& sax
)
{
	using U = uchar< Iterator >;
	Iterator it = skip_whitespace_utf8(first, last);

	if ( it != last )
	{
		const Iterator           jt = it;
		const std::uint_fast32_t c  = static_cast< U >( *it );
		++it;

		if ( c <= DIGIT9 )
		{
			if ( c >= DIGIT0 )
			{
				if ( c != DIGIT0 )
				{
					it = skip_digits_utf8(it, last);
				}

				return parse_mantissa_utf8(jt, it, last, sax);
			}

			if ( c == QUOTE )
			{
				const auto res = parse_string_utf8(it, last, sax);

				if ( res )
				{
					/// @todo Pass information on string composition (ex., if it has any escapes, pure ascii, etc.)
					if ( !sax.string(jt, *res) )
					{
						return { false, jt };
					}
				}

				return res;
			}

			if ( c == MINUS )
			{
				if ( it != last && is_digit(*it) )
				{
					if ( *it == DIGIT0 )
					{
						++it;
					}
					else
					{
						it = skip_digits_utf8(it, last);
					}

					return parse_mantissa_utf8(jt, it, last, sax);
				}
			}
		}
		else
		{
			if ( c < MINORN )
			{
				if ( c == LBRACKET )
				{
					return parse_array_utf8(jt, last, sax);
				}

				if ( c == MINORF )
				{
					const auto res = parse_false_utf8(it, last);

					if ( res )
					{
						if ( !sax.false_literal(jt, *res) )
						{
							return { false, jt };
						}
					}
					else
					{
						sax.error(SAXBase::expected_false, jt);
					}

					return res;
				}
			}
			else
			{
				if ( c == LCURLY )
				{
					return parse_object_utf8(jt, last, sax);
				}

				if ( c == MINORN )
				{
					const auto res = parse_null_utf8(it, last);

					if ( res )
					{
						if ( !sax.null_literal(jt, *res) )
						{
							return { false, jt };
						}
					}
					else
					{
						sax.error(SAXBase::expected_null, jt);
					}

					return res;
				}

				if ( c == MINORT )
				{
					const auto res = parse_true_utf8(it, last);

					if ( res )
					{
						if ( !sax.true_literal(jt, *res) )
						{
							return { false, jt };
						}
					}
					else
					{
						sax.error(SAXBase::expected_true, jt);
					}

					return res;
				}
			}
		}
	}

	sax.error(SAXBase::expected_value, last);
	return { false, last };
}

#ifdef FORWARD_ITERATOR_DEBUG
template< typename Iterator >
bool validate_utf8(
	Iterator first_,
	Iterator last_,
	SAX< Iterator >&
)
{
	ForwardIteratorWrapper< Iterator > first(first_);
	ForwardIteratorWrapper< Iterator > last(last_);

	SAX< ForwardIteratorWrapper< Iterator > > sax;

	if ( const auto res = parse_any(first, last, sax) )
	{
		return skip_whitespace_utf8(*res, last) == last;
	}

	return false;
}

#else
/* Validate UTF-8 data. Iterator::value_type should be unsigned
 */
/// @todo *first should produce a char, signed char, unsigned char, or std::byte
template< std::forward_iterator Iterator >
bool validate_utf8(
	const Iterator         first,
	const Iterator         last,
	SAX< Iterator >& sax
)
{
	const auto res = parse_any_utf8(first, last, sax);

	if ( res.first )
	{
		return skip_whitespace_utf8(res.second, last) == last;
	}

	return false;
}

#endif // ifdef FORWARD_ITERATOR_DEBUG

/// @todo Support input iterators since we don't have to pass them to a SAX instance
template< typename Iterator >
bool validate_utf8(
	const Iterator first,
	const Iterator last
)
{

	SAX< Iterator > do_nothing;

	return validate_utf8(first, last, do_nothing);
}

}

std::string unescape(const char*, const char*);

}
}

#endif // JL_JSON_PARSE_H
