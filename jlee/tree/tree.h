#ifndef JL_TREE_TREE_H
#define JL_TREE_TREE_H

#include <cstdint>
#include <map>
#include <stdexcept>
#include <vector>

#include "util/index_type.h"

namespace details
{
template< typename T >
struct node
{
	using key_type = index_type< std::uintmax_t, node< T > >;

	node(key_type parent_, const T& value_)
		: parent(parent_), value(value_)
	{
	}

	node(key_type parent_, T && value_)
		: parent(parent_), value(std::move(value_) )
	{
	}

	template< typename...Ts >
	explicit node(key_type parent_, Ts && ... ts)
		: parent(parent_), value(std::forward< Ts >(ts)...)
	{
	}

	key_type parent;
	std::vector< key_type > children;
	T value;
};
}

/// @todo When nodes are deleted, just move them to another map for reuse
template< typename T >
class tree
{
	class key_compare
	{
public:
		bool operator()(
			const typename details::node< T >::key_type& l,
			const typename details::node< T >::key_type& r
		) const
		{
			return l.get() < r.get();
		}

	};
public:
	using node_type       = details::node< T >;
	using key_type        = typename node_type::key_type;
	using size_type       = std::size_t;
	using value_type      = T;
	using reference       = value_type&;
	using const_reference = const value_type&;

	using iterator       = typename std::map< key_type, node_type, key_compare >::iterator;
	using const_iterator = typename std::map< key_type, node_type, key_compare >::const_iterator;

	static constexpr key_type INVALID{ UINTMAX_C(-1) };

	tree() = default;

	tree(const tree&) = default;

	tree(tree&& o) noexcept
		: nodes(std::move(o.nodes) ), roots(std::move(o.roots) ), next(std::move(o.next) )
	{
		o.clear();
	}

	~tree() = default;

	tree& operator=(const tree&) = default;

	tree& operator=(tree&& o) noexcept
	{
		nodes = std::move(o.nodes);
		roots = std::move(o.roots);
		next  = std::move(o.next);
		o.clear();
		return *this;
	}

	const_iterator find(key_type k) const
	{
		return nodes.find(k);
	}

	iterator find(key_type k)
	{
		return nodes.find(k);
	}

	iterator insert(const value_type& x)
	{
		const key_type i = get_next_id();

		auto res = nodes.emplace_hint(nodes.end(), std::piecewise_construct, std::make_tuple(i),
			std::make_tuple(INVALID, x) );

		roots.push_back(i);
		return res;
	}

	iterator insert(value_type&& x)
	{
		const key_type i = get_next_id();

		auto res = nodes.emplace_hint(nodes.end(), std::piecewise_construct, std::make_tuple(i),
			std::make_tuple(INVALID, std::move(x) ) );

		roots.push_back(i);
		return res;
	}

	iterator insert(
		key_type          parent,
		const value_type& x
	)
	{
		if ( parent == INVALID )
		{
			return insert(x);
		}

		auto it = nodes.find(parent);

		if ( it != nodes.end() )
		{
			const key_type i = get_next_id();
			it->second.children.push_back(i);
			return nodes.emplace_hint(nodes.end(), std::piecewise_construct, std::make_tuple(i),
				std::make_tuple(parent, x) );
		}

		throw std::runtime_error("Parent node does not exist");
	}

	iterator insert(
		key_type     parent,
		value_type&& x
	)
	{
		if ( parent == INVALID )
		{
			return insert(std::move(x) );
		}

		auto it = nodes.find(parent);

		if ( it != nodes.end() )
		{
			const key_type i = get_next_id();
			it->second.children.push_back(i);
			return nodes.emplace_hint(nodes.end(), std::piecewise_construct, std::make_tuple(i),
				std::make_tuple(parent, std::move(x) ) );
		}

		throw std::runtime_error("Parent node does not exist");
	}

	template< typename...Ts >
	iterator emplace(
		iterator parent,
		Ts&&...  ts
	)
	{
		if ( parent == nodes.end() )
		{
			const key_type i = get_next_id();

			auto res = nodes.emplace_hint(nodes.end(), std::piecewise_construct, std::make_tuple(i),
				std::make_tuple(INVALID, std::forward< Ts >(ts)...) );
			roots.push_back(i);
			return res;
		}

		if ( parent != nodes.end() )
		{
			const key_type i = get_next_id();
			parent->second.children.push_back(i);
			return nodes.emplace_hint(nodes.end(), std::piecewise_construct, std::make_tuple(i),
				std::make_tuple(parent->first, std::forward< Ts >(ts)...) );
		}

		throw std::runtime_error("Parent node does not exist");
	}

	[[nodiscard]] key_type child(size_type i) const
	{
		return roots.at(i);
	}

	[[nodiscard]] key_type child(
		key_type  i,
		size_type j
	) const
	{
		if ( i == INVALID )
		{
			return child(j);
		}

		return nodes.at(i).children.at(j);
	}

	[[nodiscard]] iterator child(
		iterator  i,
		size_type j
	)
	{
		if ( i == end() )
		{
			return nodes.find(roots.at(j) );
		}

		return nodes.find(i->second.children.at(j) );
	}

	[[nodiscard]] const_iterator child(
		const_iterator i,
		size_type      j
	) const
	{
		if ( i == end() )
		{
			return nodes.find(roots.at(j) );
		}

		return nodes.find(i->second.children.at(j) );
	}

	[[nodiscard]] key_type parent(key_type i) const
	{
		if ( i == INVALID )
		{
			return INVALID;
		}

		return nodes.at(i).parent;
	}

	iterator end()
	{
		return nodes.end();
	}

	const_iterator end() const
	{
		return nodes.end();
	}

	[[nodiscard]] const_iterator parent(const_iterator i) const
	{
		if ( i == nodes.end() )
		{
			return i;
		}

		return nodes.find(i->second.parent);
	}

	[[nodiscard]] iterator parent(iterator i)
	{
		if ( i == nodes.end() )
		{
			return i;
		}

		return nodes.find(i->second.parent);
	}

	[[nodiscard]] size_type row(key_type id) const
	{
		if ( id == INVALID )
		{
			return 0;
		}

		const key_type p = nodes.at(id).parent;

		const auto& children = ( p == INVALID ? roots : nodes.at(p).children );

		for (std::size_t i = 0, n = children.size(); i < n; ++i)
		{
			if ( children[i] == id )
			{
				return i;
			}
		}

		throw std::logic_error("Node not found in parent list. Impossible.");
	}

	bool empty() const
	{
		return roots.empty();
	}

	[[nodiscard]] size_type child_count(key_type i = INVALID) const
	{
		if ( i == INVALID )
		{
			return roots.size();
		}

		auto it = nodes.find(i);

		return it != nodes.end() ? it->second.children.size() : 0;
	}

	[[nodiscard]] size_type child_count(iterator it) const
	{
		return it == end() ? roots.size() : it->second.children.size();
	}

	[[nodiscard]] size_type child_count(const_iterator it) const
	{
		return it == end() ? roots.size() : it->second.children.size();
	}

	const_reference at(key_type i) const
	{
		return nodes.at(i).value;
	}

	reference at(key_type i)
	{
		return nodes.at(i).value;
	}

	void swap(tree& o)
	{
		nodes.swap(o.nodes);
		roots.swap(o.roots);
		std::swap(next, o.next);
	}

	void clear()
	{
		nodes.clear();
		roots.clear();
		next = key_type();
	}

	void erase(key_type k)
	{
		erase(nodes.find(k) );
	}

	void erase(iterator it)
	{
		if ( it != nodes.end() )
		{
			// Remove from list of parents children
			const key_type p = it->second.parent;

			std::vector< key_type >& children = ( p == INVALID ? roots : nodes.at(p).children );

			for (std::size_t j = 0, n = children.size(); j < n; ++j)
			{
				if ( children[j] == it->first )
				{
					children.erase(children.begin() + j);
					break;
				}
			}

			erase_inner(it);
		}
	}
private:
	key_type get_next_id()
	{
		if ( next != INVALID )
		{
			const auto x = next;
			++next;
			return x;
		}

		// We've overflowed the number of elements added to this container
		// The only safe way to proceed is to manually search for an ID not in use
		/// @todo Might be a smarter way to do this, but the chance of this code executing is
		/// basically 0.
		key_type i = key_type();

		while ( nodes.count(i) != 0 )
		{
			++i;
		}

		if ( i == INVALID )
		{
			throw std::runtime_error("Container is full");
		}

		return i;
	}

	void erase_inner(iterator it)
	{
		/// @todo Pretty sure this will always hold true
		if ( it != nodes.end() )
		{
			for (const auto k : it->second.children)
			{
				erase_inner(nodes.find(k) );
			}

			nodes.erase(it);
		}
	}

	std::map< key_type, node_type, key_compare > nodes;

	std::vector< key_type > roots;
	key_type                next{ 0 };
};
#endif // JL_TREE_TREE_H
