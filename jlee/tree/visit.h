#ifndef JL_TREE_VISIT_H
#define JL_TREE_VISIT_H

#include "tree.h"

template< typename T >
class tree_visitor_interface
{
public:
	using size_type = typename tree< T >::size_type;

	virtual ~tree_visitor_interface() = default;

	virtual bool visit(const T&)
	{
		return false;
	}

	virtual void before_child(size_type)
	{
	}

	virtual void after_child(size_type)
	{
	}

	virtual void before_children(size_type)
	{
	}

	virtual void after_children(size_type)
	{
	}

};

namespace details
{
template< typename T >
bool preorder_traverse_children(
	tree_visitor_interface< T >& visitor,
	const tree< T >&             t,
	typename tree< T >::key_type k
)
{
	// Visit i itself, then its children
	if ( !visitor.visit(t.at(k) ) )
	{
		return false;
	}

	const typename tree< T >::size_type n = t.child_count(k);

	visitor.before_children(n);

	for (typename tree< T >::size_type i = 0; i < n; ++i)
	{
		visitor.before_child(i);

		if ( !preorder_traverse_children(visitor, t, t.child(k, i) ) )
		{
			return false;
		}

		visitor.after_child(i);
	}

	visitor.after_children(n);

	return true;
}

}

template< typename T >
bool preorder_traversal(
	tree_visitor_interface< T >& visitor,
	const tree< T >&             t
)
{
	// visit roots in order
	const typename tree< T >::size_type n = t.child_count();

	visitor.before_children(n);

	for (typename tree< T >::size_type i = 0; i < n; ++i)
	{
		visitor.before_child(i);

		if ( !details::preorder_traverse_children(visitor, t, t.child(i) ) )
		{
			return false;
		}

		visitor.after_child(i);
	}

	visitor.after_children(n);
	return true;
}

#endif // JL_TREE_VISIT_H
