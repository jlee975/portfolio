#include "adler32.h"

#include <algorithm>

/* We don't need to do the modulo-65521 very often. For i bytes of data, it's simple to see
 * that
 *
 *     a <= 65520+255*i
 *
 * It follows that
 *
 *     b <= 65520+i*65520+255*i*(i+1)/2
 *
 * For a 64-bit uint, b is guaranteed not to overflow for i <= 380368439. Thus, we only do
 * the modulo reduction after that many bytes.
 */
/** @todo If we use a block size of 2**28 == 268435456 instead of 380368439 we can use a shift to quickly
 * calculate the number of blocks (avoiding a division). Then we would have more straight-forward loops.
 * Moreover, we might be able to optimize the first loop if we know its a power of two (perhaps leveraging
 * SSE instructions)
 * @todo Even if b overflows, a will not overflow for a long time
 * @todo For small lengths, b will not even overflow 65521
 */
std::uint_least32_t adler32(
	const unsigned char* data,
	std::size_t          len
)
{
	#if 1
	constexpr std::uint_fast32_t blocksize = UINT32_C(380368439);
	std::uint_fast64_t           a         = 1;
	std::uint_fast64_t           b         = 0;

	for (std::size_t i = 0; i < len;)
	{
		const std::size_t j = blocksize < len - i ? i + blocksize : len;

		for (; i < j; ++i)
		{
			a += data[i];
			b += a;
		}

		a %= 65521;
		b %= 65521;
	}

	#else
	// naive implementation
	std::uint_fast64_t a = 1;
	std::uint_fast64_t b = 0;

	for (std::size_t i = 0; i < len; ++i)
	{
		a = ( a + data[i] ) % 65521;
		b = ( b + a ) % 65521;
	}

	#endif // if 1
	return ( b << 16 ) | a;
}
