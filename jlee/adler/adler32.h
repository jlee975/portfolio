#ifndef JLEE_ADLER32_H
#define JLEE_ADLER32_H

#include <cstddef>
#include <cstdint>
#include <iterator>

namespace details
{
/* Can the value be converted to unsigned char? Only whitelisting types, not
 * auto-detecting.
 */
template< typename T >
struct uchar_like
	: std::false_type
{
};

template<>
struct uchar_like< unsigned char >
	: std::true_type
{
};

template<>
struct uchar_like< signed char >
	: std::true_type
{
};

template<>
struct uchar_like< char >
	: std::true_type
{
};

template<>
struct uchar_like< std::byte >
	: std::true_type
{
};

template<>
struct uchar_like< char8_t >
	: std::true_type
{
};
}

// base case. All other calls basically forward to this
std::uint_least32_t adler32(const unsigned char*, std::size_t);

inline std::uint_least32_t adler32(
	const char* p,
	std::size_t n
)
{
	return adler32(reinterpret_cast< const unsigned char* >( p ), n);
}

inline std::uint_least32_t adler32(
	const signed char* p,
	std::size_t        n
)
{
	return adler32(reinterpret_cast< const unsigned char* >( p ), n);
}

inline std::uint_least32_t adler32(
	const std::byte* p,
	std::size_t      n
)
{
	return adler32(reinterpret_cast< const unsigned char* >( p ), n);
}

inline std::uint_least32_t adler32(
	const char8_t* p,
	std::size_t    n
)
{
	return adler32(reinterpret_cast< const unsigned char* >( p ), n);
}

template< typename Iterator >
std::uint_least32_t adler32(
	Iterator first,
	Iterator last
)
{
	if constexpr ( std::contiguous_iterator< Iterator >)
	{
		return adler32(first, last - first);
	}
	else
	{
		static_assert(details::uchar_like< std::remove_cvref_t< decltype( *first ) > >::value,
			"Adler-32 can only be calculated on byte-like data");

		// naive implementation
		std::uint_fast64_t a = 1;
		std::uint_fast64_t b = 0;

		while ( first != last )
		{
			a = ( a + static_cast< unsigned char >( *first ) ) % 65521;
			b = ( b + a ) % 65521;
			++first;
		}

		return ( b << 16 ) | a;
	}
}

template< typename Container >
std::uint_least32_t adler32(const Container& c)
{
	using std::cbegin;
	using std::end;

	return adler32(cbegin(c), end(c) );
}

#endif // ifndef JLEE_ADLER32_H
