#ifndef PLAYLIST_HPP
#define PLAYLIST_HPP

#include <cstddef>
#include <iostream>
#include <string>
#include <vector>

#include "chirp/metadata.h"

class Playlist
{
public:
	using size_type = std::size_t;

	enum Format
	{
		M3U,
		PLS,
		CAT
	};

	// Assignment and modifiers
	void swap(Playlist&);
	bool insert(const std::string&);
	bool insert(const std::string&, const MetaData&);
	void remove(const std::string&);

	void erase(size_type);
	void clear();

	std::string serialize(Format) const;
	size_type size() const;
	bool empty() const;

	std::string field(size_type, MetaData::Field) const;
	std::string filename(size_type) const;
	size_type find(const std::string&) const;
	void sort(MetaData::Field = MetaData::Invalid_, bool = true);

	MetaData meta(size_type) const;

	std::string missingMeta() const;
	size_type updateMeta(const std::string&, const MetaData&);

	std::string saveM3U() const;
	static Playlist loadM3U(std::istream&);

	std::string saveCAT() const;
	static Playlist loadCAT(std::istream&);

	friend class TrackSorter;
	/// \todo Not keen on this solution
private:
	struct track_t;
	std::vector< track_t > tracks;
};

struct Playlist::track_t
{
	MetaData meta;
	std::string filename;
	bool init{ false };

	track_t();
	explicit track_t(std::string);
	track_t(const track_t&);
	track_t& operator=(const track_t&);

	// std::string operator[](MetaData::Field) const;
	void swap(track_t&);
};

#endif // #ifndef PLAYLIST_HPP
