/*=========================================================================*//**
   \file        playlist.cpp
*//*==========================================================================*/
#include <algorithm> // For stable_sort, swap
#include <set>       // For iterator shuffle functionality
#include <stdexcept>
#include <utility>

#include "playlist.h"

/*=========================================================================*//**
   \class       TrackSorter
   \brief       Used for sorting track_ts based on a particular field
*//*==========================================================================*/
class TrackSorter
{
	MetaData::Field field;
	bool            asc;
public:
	TrackSorter(
		MetaData::Field f,
		bool            a
	)
		: field(f), asc(a)
	{
	}

	bool operator()(
		const Playlist::track_t& a,
		const Playlist::track_t& b
	) const
	{
		return asc ? a.meta[field] < b.meta[field] : b.meta[field] < a.meta[field];
	}

};

/*=========================================================================*//**
   \class       Playlist

   \todo Might want an iterator class to simplify operations outside of this class
   \todo Iterating over tracks should probably not use std::size_t

*//*==========================================================================*/

/***************************************************************************/ /**
 *******************************************************************************/
void Playlist::swap(Playlist& x)
{
	if ( &x != this )
	{
		tracks.swap(x.tracks);
	}
}

/***************************************************************************/ /**
 *******************************************************************************/
Playlist::size_type Playlist::size() const
{
	return tracks.size();
}

/***************************************************************************/ /**
 *******************************************************************************/
bool Playlist::empty() const
{
	return tracks.empty();
}

/***************************************************************************/ /**
 *******************************************************************************/
void Playlist::clear()
{
	tracks.clear();
}

/***************************************************************************/ /**
 *******************************************************************************/
std::string Playlist::filename(size_type i) const
{
	return tracks.at(i).filename;
}

/***************************************************************************/ /**
 *******************************************************************************/
MetaData Playlist::meta(size_type i) const
{
	return tracks[i].meta;
}

/***************************************************************************/ /**
 *******************************************************************************/
std::string Playlist::field(
	size_type       i,
	MetaData::Field f
) const
{
	return tracks.at(i).meta[f];
}

/***************************************************************************/ /**
 *******************************************************************************/
void Playlist::sort(
	MetaData::Field f,
	bool            asc
)
{
	std::stable_sort(tracks.begin(), tracks.end(), TrackSorter(f, asc) );
}

/***************************************************************************/ /**
   \brief       Add filename s to the list
   \todo        if (s is recognizable file format) { ... }
 *******************************************************************************/
bool Playlist::insert(const std::string& s)
{
	if ( !s.empty() && ( find(s) >= tracks.size() ) ) // not already in list
	{
		tracks.emplace_back(s);
		return true;
	}

	return false;
}

/***************************************************************************/ /**
 *******************************************************************************/
bool Playlist::insert(
	const std::string& s,
	const MetaData&    m
)
{
	if ( !s.empty() && ( find(s) >= tracks.size() ) )
	{
		tracks.emplace_back(s);
		tracks.back().meta = m;
		tracks.back().init = true;
		return true;
	}

	return false;
}

/***************************************************************************/ /**
   \todo rename to erase
 *******************************************************************************/
void Playlist::remove(const std::string& s)
{
	size_type i = find(s);

	if ( i < tracks.size() )
	{
		tracks.erase(tracks.begin() + i);
	}
}

/***************************************************************************/ /**
 *******************************************************************************/
void Playlist::erase(size_type i)
{
	if ( i < tracks.size() )
	{
		tracks.erase(tracks.begin() + i);
	}
}

/***************************************************************************/ /**
 *******************************************************************************/
Playlist::size_type Playlist::find(const std::string& s) const
{
	size_type i = 0;

	size_type n = tracks.size();

	while ( i < n && tracks[i].filename != s )
	{
		++i;
	}

	return i;
}

/***************************************************************************/ /**
   \todo Can probably move the m.initialized stuff into the MetaData::field() call
 *******************************************************************************/
std::string Playlist::missingMeta() const
{
	std::string s;

	auto it = tracks.begin();

	while ( it != tracks.end() && it->init )
	{
		++it;
	}

	if ( it != tracks.end() )
	{
		s = it->filename;
	}

	return s;
}

/***************************************************************************/ /**
 *******************************************************************************/
Playlist::size_type Playlist::updateMeta(
	const std::string& f,
	const MetaData&    m
)
{
	size_type i = find(f);

	if ( i < tracks.size() )
	{
		tracks[i].meta = m;
		tracks[i].init = true;
	}

	return i;
}

/***************************************************************************/ /**
 *******************************************************************************/
std::string Playlist::serialize(Format fileformat) const
{
	std::string s;

	switch ( fileformat )
	{
	case M3U:
		saveM3U().swap(s);
		break;
	case CAT:
		saveCAT().swap(s);
		break;
	default:
		break;
	}

	return s;
}

/***************************************************************************/ /**
 *******************************************************************************/
std::string Playlist::saveM3U() const
{
	std::string s = "#EXTM3U\n";

	for (const auto& track : tracks)
	{
		if ( track.init )
		{
			/// \todo Watch for empty fields
			s += "#EXTINF:";
			s += "123,"; /// \todo The actual duration
			s += track.meta[MetaData::Artist];
			s += '-';
			s += track.meta[MetaData::Title];
			s += '\n';
		}

		s += track.filename;
		s += '\n';
	}

	return s;
}

/***************************************************************************/ /**
   \todo Set seconds
   \todo Trim title and artist of whitespace
 *******************************************************************************/
Playlist Playlist::loadM3U(std::istream& is)
{
	Playlist pl;

	bool          extended = false; // This is/isn't an extended M3U file
	bool          extinfo  = false; // There is meta data to go with the filename
	unsigned long linenum  = 0;

	std::string s;
	MetaData    m;

	while ( std::getline(is, s) )
	{
		++linenum;

		if ( s.empty() )
		{
			continue;
		}

		if ( ( linenum == 1 ) && ( s == "#EXTM3U" ) )
		{
			extended = true;
		}
		else if ( extended && ( s.substr(0, 8) == "#EXTINF:" ) )
		{
			m.clear();
			std::size_t i = s.find(',', 8);

			if ( i != std::string::npos ) // field before comma is duration in seconds
			{
				std::size_t j = s.find('-', i);

				if ( j != std::string::npos ) // The artist info is present
				{
					m.assign(MetaData::Artist, s.substr(i + 1, j - ( i + 1 ) ) );
					m.assign(MetaData::Title, s.substr(j + 1) );
				}
				else
				{
					m.assign(MetaData::Title, s.substr(i + 1) );
				}

				extinfo = true;
			} /// else malformed

		}
		else
		{
			track_t t(s);

			if ( extinfo )
			{
				t.meta  = m;
				extinfo = false;
			}

			pl.tracks.push_back(t);
		}
	}

	return pl;
}

/***************************************************************************/ /**
   \brief Hacked in file format for saving meta data
   \todo Remove bad characters
 *******************************************************************************/
std::string Playlist::saveCAT() const
{
	std::string s;

	for (const auto& track : tracks)
	{
		const MetaData& m = track.meta;
		std::string     t;

		t = track.filename;

		if ( !t.empty() )
		{
			s.append(t, 0, t.find('\n') );
		}

		s += '\n';

		t = m[MetaData::Artist];

		if ( !t.empty() )
		{
			s.append(t, 0, t.find('\n') );
		}

		s += '\n';

		t = m[MetaData::Album];

		if ( !t.empty() )
		{
			s.append(t, 0, t.find('\n') );
		}

		s += '\n';

		t = m[MetaData::Title];

		if ( !t.empty() )
		{
			s.append(t, 0, t.find('\n') );
		}

		s += '\n';
	}

	return s;
}

/***************************************************************************/ /**
 *******************************************************************************/
Playlist Playlist::loadCAT(std::istream& is)
{
	Playlist pl;

	std::string artist;

	std::string album;

	std::string title;

	std::string filename;

	while ( is )
	{
		MetaData m;

		getline(is, filename);

		if ( is.eof() )
		{
			break;
		}

		getline(is, artist);
		getline(is, album);
		getline(is, title);

		m.assign(MetaData::Artist, artist);
		m.assign(MetaData::Album, album);
		m.assign(MetaData::Title, title);

		pl.insert(filename, m);
	}

	return pl;
}

/*=========================================================================*//**
   \class       track_t
   \brief       Wraps together MetaData, filename, and initialization information
*//*==========================================================================*/
Playlist::track_t::track_t() = default;

Playlist::track_t::track_t(std::string s)
	: filename(std::move(s) ), init(false)
{
}

Playlist::track_t::track_t(const track_t& t) = default;

Playlist::track_t& Playlist::track_t::operator=(const track_t& t)
{
	if ( &t != this )
	{
		track_t(t).swap(*this);
	}

	return *this;
}

void Playlist::track_t::swap(track_t& t)
{
	meta.swap(t.meta);
	filename.swap(t.filename);
	std::swap(init, t.init);
}
