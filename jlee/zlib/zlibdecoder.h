#ifndef ZLIBDECODER_H
#define ZLIBDECODER_H

#include "deflate/decompressor.h"

class ZLibDecoder
{
public:
	enum class error
	{
		none,
		unsupported_compression_method, // only deflate is supported
		bad_check_bits,                 // zlib header corrupted
		dictionary_not_supported,
		bad_adler,     // data corrupted
		deflate_error, // error while deflating data
		eof            // unexpected eof. Can call again with more data.
	};

	ZLibDecoder();

	void reset();
	error exec(ILEBitstream&);
	std::vector< std::byte > take();
	const std::vector< std::byte >& view() const;
private:
	enum cm_type
	{
		cm_deflate = 8,
		cm_reserved = 15
	};

	enum flag_type
	{
		fdict = 32
	};

	enum state_type
	{
		get_header,
		do_deflate,
		skip,
		do_adler,
		done
	};

	state_type               state{ get_header };
	deflate::Decompressor    def;
	std::vector< std::byte > data;
};

#endif // ZLIBDECODER_H
