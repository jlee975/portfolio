#include "zlibdecoder.h"

#include <stdexcept>

#include "adler/adler32.h"
#include "bits/bswap.h"

ZLibDecoder::ZLibDecoder() = default;

void ZLibDecoder::reset()
{
	state = get_header;
	def.reset();
	data.clear();
}

ZLibDecoder::error ZLibDecoder::exec(ILEBitstream& bs)
{
	try
	{
		if ( state == get_header )
		{
			const auto x   = bs.read(16);
			const auto cmf = x & 0xff;
			const auto flg = x >> 8;

			if ( ( cmf & 0xF ) != cm_deflate )
			{
				return error::unsupported_compression_method;
			}

			if ( ( cmf * 256 + flg ) % 31 != 0 )
			{
				return error::bad_check_bits;
			}

			if ( ( flg & fdict ) != 0 )
			{
				return error::dictionary_not_supported;
			}

			state = do_deflate;
		}

		if ( state == do_deflate )
		{
			const auto res = def.exec(bs);

			if ( res == deflate::Decompressor::error::eof )
			{
				return error::eof;
			}

			if ( res != deflate::Decompressor::error::none )
			{
				return error::deflate_error;
			}

			data  = std::move(def).data();
			state = skip;
		}

		if ( state == skip )
		{
			bs.move_to_next_byte_boundary();
			state = do_adler;
		}

		if ( state == do_adler )
		{
			const auto act = bswap32(bs.read(32) );
			const auto chk = adler32(data.data(), data.size() );

			if ( act != chk )
			{
				return error::bad_adler;
			}

			state = done;
		}
	}
	catch ( unexpected_eos_type )
	{
		return error::eof;
	}

	return error::none;
}

std::vector< std::byte > ZLibDecoder::take()
{
	return std::move(data);
}

const std::vector< std::byte >& ZLibDecoder::view() const
{
	return data;
}
