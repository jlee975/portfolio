#ifndef GRAMMAR_H
#define GRAMMAR_H

#include <map>
#include <string>

#include "graph/directed.h"

// when parsing a "character class", or alternate bunch of characters
// short hand for a bunch of rules that would simply be rule -> 'A'; rule -> 'B', etc.
struct alt
{
	explicit alt(std::string s)
		: t(std::move(s) )
	{
	}

	std::string t;
};

/** @todo DFA minimization
 * @todo Combine common prefixes
 * @todo Recognize necessarily greedy patterns. Ex., nonnegative + nonnegative. Consecutive digits must be
 * part of the same nonnegative, b/c no following pattern begins with a digit.
 * @todo Recognizer regular languages
 * @todo Special handling for parens-like situations of nested rules and matching begin/end tokens
 * @todo Combine consecurive reads into reading a string
 * @todo Mark nodes that are deterministic (i.e., all edges exit with mutually exclusive reads)
 * @todo Recognize operator precedence parsing. Ex., a*b+c will do lots of backtracking, but we could keep
 * subexpressions as we switch from * to +
 * @todo Recognize tokenizing languages. i.e., those where the character stream can be tokenized, and then
 * grammar rules are applied to those tokens. Ex., C/C++, json
 */
class NondeterministicFiniteAutomaton
{
public:
	enum type_type
	{
		ETA_EDGE, // edge can always be taken
		ALT_EDGE, // edge can be taken if next character is one of those given (i.e., a character class)
		SEQ_EDGE  // edge can be taken if sequences of specific characters are read (i.e., a string literal)
	};

	struct E
	{
		E()
			: type(ETA_EDGE)
		{
		}

		E(alt x)
			: type(ALT_EDGE), s(std::move(x.t) )
		{
		}

		E(std::string lit)
			: type(SEQ_EDGE), s(std::move(lit) )
		{
		}

		/// @todo Use a std::variant
		type_type type;
		std::string s;
	};

	/// @todo template
	using vertex_id = int;
	using edge_id   = std::size_t;

	struct edge
	{
		vertex_id from;
		vertex_id to;
		E data;
	};

	NondeterministicFiniteAutomaton(std::initializer_list< edge > l)
		: NondeterministicFiniteAutomaton(std::vector< edge >( l.begin(), l.end() ) )
	{
	}

	explicit NondeterministicFiniteAutomaton(const std::vector< edge >& e)
	{
		for (std::size_t i = 0, n = e.size(); i < n; ++i)
		{
			edges.emplace_back(e[i]);
			vertices[e[i].to];
			vertices[e[i].from].push_back(i);
		}
	}

	const std::vector< edge_id >& transitions(vertex_id i) const
	{
		return vertices.at(i);
	}

	const edge& transition(
		vertex_id   j,
		std::size_t i
	) const
	{
		return edges.at(transitions(j)[i]);
	}
private:
	std::map< vertex_id, std::vector< edge_id > > vertices;
	std::vector< edge >                           edges;
};

typedef NondeterministicFiniteAutomaton NFA;

#endif // GRAMMAR_H
