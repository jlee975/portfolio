/** Linear Prediction

   Based on the algorithm presented in:
    "Stable and Efficient Lattice Methods for Linear Prediction",
        John Makhoul, IEEE Transactions on Acoustics, Speech, and Signal
        Processing. Vol. ASSP-25, No. 5, Oct 1977, p 423-428

   Numerical Recipes for the pointer to the above document

   \todo Rewrite for C++-style
 */

#include <cmath>
#include <cstdlib>
#include <iostream>
using std::cout;
using std::endl;

// Computer the covariances
// set m = 0
// Computer C_m(n) and F_m(n) + B_m(n - 1)
// Computer K_{m+1}

// \todo This function can be inlined in lp because of the recursion
double phi(
	double*  s,
	unsigned N,
	unsigned k,
	unsigned i,
	unsigned m
)
{
	double sum;

	if ( 0 < m && k <= m && i <= m )
	{
		sum = phi(s, N, k, i, m - 1) - s[m - 1 - k] * s[m - 1 - i];
	}
	else
	{
		sum = 0.0;

		for (unsigned n = m; n < N; ++n)
		{
			sum += s[n - k] * s[n - i];
		}
	}

	// */
	return sum;
}

void lp(
	double*  signal,
	unsigned N,
	double*  coeff,
	unsigned p
)
{
	double* olda = new double [p];

	coeff[0] = 2.0 * phi(signal, N, 0, 1, 1) / ( phi(signal, N, 0, 0, 1) + phi(signal, N, 1, 1, 1) );

	for (unsigned m = 2; m <= p; ++m)
	{
		double C, F;

		C = phi(signal, N, 0, m, m);
		F = phi(signal, N, 0, 0, m) + phi(signal, N, m, m, m);

		for (unsigned k = 1; k < m; ++k)
		{
			double x = coeff[k - 1];

			C += -x* phi(signal, N, 0, m - k, m) + -x* phi(signal, N, k, m, m)
			     + x * x * phi(signal, N, k, m - k, m);
			F += -2. * x * phi(signal, N, 0, k, m) + x * x * phi(signal, N, k, k, m)
			     + -2. * x * phi(signal, N, m, m - k, m) + x * x * phi(signal, N, m - k, m - k, m);
		}

		for (unsigned k = 1; k + 1 < m; ++k)
		{
			for (unsigned i = k + 1; i < m; ++i)
			{
				double x = coeff[k - 1] * coeff[i - 1];
				C += x * ( phi(signal, N, k, m - i, m) + phi(signal, N, i, m - k, m) );
				F += 2.0 * x * ( phi(signal, N, k, i, m) + phi(signal, N, m - k, m - i, m) );
			}
		}

		for (unsigned j = 0; j + 1 < m; ++j)
		{
			olda[j] = coeff[j];
		}

		// Calculate a(m + 1)
		double K = 2.0 * C / F;
		coeff[m - 1] = K;

		for (unsigned j = 0; j + 1 < m; ++j)
		{
			coeff[j] -= K * olda[m - j - 2];
		}
	}

	delete[] olda;
}

#define SIZER 100
#define SIZEC 8

double v[SIZER];
double v2[SIZER];
double c[SIZEC];
double xmmmm;

int main()
{
	for (int i = 0; i < SIZEC; ++i)
	{
		c[i] = 0.0;
	}

	for (int i = 0; i < SIZEC; ++i)
	{
		v[i] = ( 1.0 * rand() / RAND_MAX ) - 0.5;
	}

	for (int i = SIZEC; i < SIZER; ++i)
	{
		v[i] = v[i - 1] * 0.23 - v[i - 2] * 0.46 + v[i - 3] * .004 + v[i - 4] * .052;
	}

	// + (static_cast<double>(rand()) / RAND_MAX) - 0.5;

	for (int i = 0; i < SIZER; ++i)
	{
		cout << v[i] << endl;
	}

	lp(v, SIZER, c, SIZEC);

	for (int i = 0; i < SIZEC; ++i)
	{
		cout << "c[" << i << "] = " << c[i] << endl;
	}

	double lse = 0.0;

	for (int i = SIZEC; i < SIZER; ++i)
	{
		double sum = 0.0;

		for (int j = 0; j < SIZEC; ++j)
		{
			sum += c[j] * v[i - j - 1];
		}

		v2[i] = sum;
		cout << "diff = " << fabs(v2[i] / v[i]) << endl;
		lse += ( v2[i] - v[i] ) * ( v2[i] - v[i] );
	}

	cout << "Error = " << lse << endl;

	return 0;
}
