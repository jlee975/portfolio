#ifndef OGG_HPP
#define OGG_HPP

#include <cstddef>
#include <istream>
#include <map>
#include <vector>

#include "audio/encapsulation.h"

// Ogg Structures
class Ogg
	: public audio::Encapsulation
{
public:
	explicit Ogg(std::istream&);
	Ogg(const Ogg&) = delete;

	Ogg& operator=(const Ogg&) = delete;

	Packet ReadPacket() override;
	void seek(audio::sample_off) override;
	bool eos() const override;
	void prepare() override;
	audio::sample_off position() const override;
	audio::sample_off length() const override;
private:
	struct Page
	{
		std::uint_least8_t version;
		std::uint_least8_t flags;
		audio::sample_off granule;
		std::uint_least32_t serial;
		std::uint_least32_t pageseq;
		std::uint_least32_t checksum;
		std::uint_least8_t pagecount;
	};

	struct LogicalStream
	{
		std::uint_least32_t serial;
		std::ios::pos_type offset;
		std::size_t skip_packets;
	};

	using SeekTable = std::map< audio::sample_off, std::ios::pos_type >;

	static bool unserialize(Page&, const char*);

	static const unsigned supported_version;

	/// @bug Logical streams ought to be grouped by serial number
	std::vector< LogicalStream > logical_stream;

	std::istream&              is;
	mutable bool               eosknown;
	mutable std::ios::pos_type eosvalue;

	SeekTable         seektable;
	audio::sample_off current_pos;
};

#endif // #ifndef OGG_HPP
