/** @brief Ogg stream functions
 *
 *  These functions are used for accessing data stored in an Ogg stream. The exact
 *  structure of these streams is documented by Xiph. However, it may be sufficient
 *  to say that a stream is divided into small packets and each packet (for our
 *  current purposes) contains compressed audio data.
 *
 *  Ogg streams are therefore read a packet at a time, and each packet is passed to
 *  an appropriate decompressor. The exact packet we're at is tracked in an
 *  OGGSTREAM structure which is filled in using ogg_init().
 *
 *  At the moment the functions are not wrapped in a class, though this is on my
 *  list of things to do. It will be especially natural once some seeking functions
 *  are in place. There is no real need for a class implementation at this point
 *  in development, though.
 *
 *  \bug
 *
 *  - calculate checksum for ogg packets
 *
 *  \todo
 *
 *  - Derive ogg from a Frame class that will provide seek, etc.
 *  - read initial pages of ogg stream and setup serial numbers, etc. for each
 *  logical stream
 *  - Ogg should read from serial number
 *  - store granule data, page number, etc.
 *  - ogg_read_packet: consider moving bytesread to the OGGSTREAM structure
 *  - Seek, and return position
 *  - we may just want to allocate an array and copy ogg data over so we can access
 *  it privately. Then again - that seems like a class thing to do.
 *
 *  References
 *
 *  "Ogg logical and physical bitstream overview", Xiph.org Foundation, 1994-2005,
 *  http://xiph.org/ogg/doc/oggstream.html
 *
 *  "Ogg logical bitstream framing", Xiph.org Foundation, 1994-2005,
 *  http://xiph.org/ogg/doc/oggstream.html
 *
 */
#include "ogg.h"

#include <stdexcept>

#include "audio/packet.h"
#include "endian/endian.h"

constexpr std::size_t pagesize               = 27;
const unsigned        Ogg::supported_version = 0; // Highest supported version

/**
 *  \todo Should probably check if stream open
 *  \todo Might move to Encapsulation and have that provide buffering
 */
Ogg::Ogg(std::istream& stream)
	: logical_stream(1, { 0, 0, 0 }), is(stream), eosknown(false), eosvalue(0), current_pos{0}
{
}

/**
 *  \todo Could call conditionally from GetEnd() or seek()
 *  \todo Could set eosknown, eosvalue
 *  \todo Any other recoverable istream errors?
 */
void Ogg::prepare()
{
	Page page;
	char buf[256];

	is.seekg(0);

	while ( is.good() )
	{
		// Read next Ogg page
		is.read(buf, pagesize);

		if ( is.gcount() != pagesize )
		{
			if ( is.eof() )
			{
				is.clear();
				is.seekg(0, std::ios::end);
				eosvalue = is.tellg();
				eosknown = true;
			}

			return;
		}

		// Lost sync? Reaquire
		while ( is.good() && !unserialize(page, buf) )
		{
			/// @todo Scan for sync bytes, instead of going byte-by-byte. Fill the buffer with data
			/// and do unserialize(page, buf + i), instead of copying bytes left.
			for (std::size_t i = 0; i + 1 < pagesize; ++i)
			{
				buf[i] = buf[i + 1];
			}

			is.get(buf[26]);
		}

		if ( !is.good() )
		{
			return;
		}

		// Record the page position within the file
		seektable[page.granule] = is.tellg() - std::streamoff(pagesize);

		// Advance si to next Ogg page
		if ( page.pagecount != 0 )
		{
			is.read(buf, page.pagecount);

			if ( !is.good() || ( is.gcount() != page.pagecount ) )
			{
				return;
			}

			std::istream::off_type si = 0;

			for (int i = 0; i < page.pagecount; ++i)
			{
				si += static_cast< unsigned char >( buf[i] );
			}

			is.seekg(si, std::ios_base::cur);
		}
	}
}

audio::sample_off Ogg::position() const
{
	return current_pos;
}

void Ogg::seek(audio::sample_off i)
{
	auto it = seektable.lower_bound(i);

	if ( it != seektable.end() )
	{
		logical_stream[0].offset = it->second;
	}
	else
	{
		logical_stream[0].offset = 0;
	}

	logical_stream[0].skip_packets = 0;
}

/***************************************************************************/ /**
 *  \bug Ensure that a max number of lacing values is agreed on.. no runaway
 *     packets...
 *  \todo Multiple streams
 *  \todo serial number
 *******************************************************************************/
bool Ogg::eos() const
{
	if ( !eosknown )
	{
		const std::ios::pos_type orig = is.tellg();
		eosvalue = is.seekg(0, std::ios_base::end).tellg();
		is.seekg(orig);
		eosknown = true;
	}

	return logical_stream[0].offset >= eosvalue;
}

/***************************************************************************/ /**
 *******************************************************************************/
audio::sample_off Ogg::length() const
{
	if ( !seektable.empty() )
	{
		return seektable.rbegin()->first;
	}

	return { 0 };
}

/***************************************************************************/ /**
   @todo Checksum
   @todo Check that the flags make sense
   @todo Check page sequence number
 *******************************************************************************/
bool Ogg::unserialize(
	Page&      page,
	const char p[pagesize]
)
{
	if ( leread< 32 >(p) == UINT32_C(0x5367674F) )
	{
		page.version = static_cast< unsigned char >( p[4] );
		page.flags   = static_cast< unsigned char >( p[5] );
		page.granule = audio::sample_off{ ( static_cast< std::uint_fast64_t >( leread< 32 >(p + 10) ) << 32 )
			                              + leread< 32 >(p + 6) };
		page.serial    = static_cast< std::uint_least32_t >( leread< 32 >(p + 14) );
		page.pageseq   = static_cast< std::uint_least32_t >( leread< 32 >(p + 18) );
		page.checksum  = static_cast< std::uint_least32_t >( leread< 32 >(p + 22) );
		page.pagecount = static_cast< unsigned char >( p[26] );
		return true;
	}

	return false;
}

/***************************************************************************/ /**
 *******************************************************************************/
Packet Ogg::ReadPacket()
{
	Page page;

	Packet data;
	char   buf[256];
	char   c;

	// Setup --------------------------------------------------------------
	bool firstpage = true;  // if we are on the first page of this packet
	bool finished  = false; // Flag for when we've got all the packet bytes

	do
	{
		// process next page
		is.seekg(logical_stream[0].offset);

		if ( !is.good() )
		{
			break;
		}

		is.read(buf, pagesize);

		if ( is.gcount() != pagesize )
		{
			break;
		}

		while ( is.good() && !unserialize(page, buf) )
		{
			for (int i = 0; i < 26; ++i)
			{
				buf[i] = buf[i + 1];
			}

			is.get(buf[26]);
		}

		if ( !is.good() )
		{
			break;
		}

		if ( page.version != supported_version ) // Unsupported version
		{
			throw std::runtime_error("UnsupportedVersion");
		}

		current_pos = page.granule;

		// Lacing Table
		int            ipage         = 0;
		std::streamoff bytes_to_skip = 0;

		/// \todo one read() vs get()

		if ( firstpage ) // Skip over packets that have been processed
		{
			bool        partial_first_packet = ( ( page.flags & 1U ) != 0 );
			std::size_t packets_to_skip      = logical_stream[0].skip_packets;

			if ( partial_first_packet )
			{
				++packets_to_skip;
			}

			// Add up the data bytes to skip
			while ( ipage != page.pagecount && packets_to_skip > 0 )
			{
				++ipage;
				is.get(c);
				int n = static_cast< unsigned char >( c );
				bytes_to_skip += n;

				if ( n != 255 )
				{
					--packets_to_skip;
				}
			}
		}

		// Calculate how many bytes we need to copy for this page
		std::size_t bytes_to_copy = 0;

		while ( ipage != page.pagecount && !finished )
		{
			is.get(c);
			int n = static_cast< unsigned char >( c );
			bytes_to_copy += n;

			finished = ( n < 255 ); // done if the byte is < 255
			++ipage;
		}

		// Data Copy ---------------------------------------------------
		is.seekg(bytes_to_skip + ( page.pagecount - ipage ), std::ios_base::cur);

		std::size_t k = data.size();
		data.resize(k + bytes_to_copy);
		is.read(reinterpret_cast< char* >( &data[k] ), bytes_to_copy);

		if ( ipage == page.pagecount )
		{
			logical_stream[0].skip_packets = 0;
			logical_stream[0].offset       = is.tellg();
		}
		else
		{
			logical_stream[0].skip_packets++;
		}

		if ( finished && !firstpage )
		{
			logical_stream[0].skip_packets = 0;
		}

		firstpage = false; // Any following page will be a continuation
	}
	while ( !finished );

	return data; // Everything went OK. Return the data
}
