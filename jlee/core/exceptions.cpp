#include "exceptions.h"

not_implemented::not_implemented(const std::string& s)
	: logic_error(s)
{
}
