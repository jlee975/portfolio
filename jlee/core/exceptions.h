#ifndef CORE_EXCEPTIONS_H
#define CORE_EXCEPTIONS_H

#include <stdexcept>

class not_implemented
	: public std::logic_error
{
public:
	explicit not_implemented(const std::string& s);
};

#endif // CORE_EXCEPTIONS_H
