#ifndef CONCURRENCY_QUEUE_H
#define CONCURRENCY_QUEUE_H

#include <mutex>
#include <queue>

template< typename T >
class concurrent_queue
{
public:
	concurrent_queue() = default;

	bool try_pop(T& x)
	{
		std::lock_guard< std::mutex > lk(queue_mutex);

		if ( !events.empty() )
		{
			x = std::move(events.front() );
			events.pop();
			return true;
		}

		return false;
	}

	void push(T x)
	{
		std::lock_guard< std::mutex > lk(queue_mutex);

		events.push(std::move(x) );
	}

	template< typename...Args >
	void emplace(Args&&... args)
	{
		std::lock_guard< std::mutex > lk(queue_mutex);

		events.emplace(std::forward< Args >(args)...);
	}

	void clear()
	{
		std::lock_guard< std::mutex > lk(queue_mutex);

		events = std::queue< T >();
	}
private:
	std::queue< T > events;

	std::mutex queue_mutex;
};

#endif // CONCURRENCY_QUEUE_H
