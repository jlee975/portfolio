#ifndef ITERATOR_WRAPPER_H
#define ITERATOR_WRAPPER_H

#include "util/strong_typedef.h"
#include <iterator>

template< typename Iterator,
          typename Tag = void,
          typename = typename std::iterator_traits< Iterator >::iterator_category >
class iterator_wrapper;

template< typename Iterator, typename Tag >
class iterator_wrapper< Iterator, Tag, std::forward_iterator_tag >
	: public strong_typedef< Iterator, op_arrow, op_dereference, op_equal_comparable, op_inc >
{
public:
	using strong_typedef< Iterator, op_arrow, op_dereference, op_equal_comparable, op_inc >::strong_typedef;
};

template< typename Iterator, typename Tag >
class iterator_wrapper< Iterator, Tag, std::random_access_iterator_tag >
	: public strong_typedef< Iterator, op_arrow, op_dereference, op_ordered, op_inc >
{
public:
	using strong_typedef< Iterator, op_arrow, op_dereference, op_ordered, op_inc >::strong_typedef;
};

namespace std
{
template< typename Iterator, typename Tag, typename Category >
struct iterator_traits< iterator_wrapper< Iterator, Tag, Category > >
{
	typedef typename iterator_wrapper< Iterator, Tag, Category >::difference_type difference_type;
	typedef typename iterator_wrapper< Iterator, Tag, Category >::value_type value_type;
	typedef typename iterator_wrapper< Iterator, Tag, Category >::pointer pointer;
	typedef typename iterator_wrapper< Iterator, Tag, Category >::reference reference;
	typedef typename iterator_wrapper< Iterator, Tag, Category >::iterator_category iterator_category;
};
}

#endif // ITERATOR_WRAPPER_H
