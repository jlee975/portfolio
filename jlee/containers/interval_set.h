#ifndef INTERVAL_SET_H
#define INTERVAL_SET_H

#include <algorithm>
#include <iostream>
#include <limits>
#include <utility>
#include <vector>

/// @todo Rename to min/max
template< typename T >
struct closed_interval
{
	T first;
	T second;
};

template< typename T >
bool operator==(
	const closed_interval< T >& l,
	const closed_interval< T >& r
)
{
	return l.first == r.first && l.second == r.second;
}

template< typename T >
bool operator!=(
	const closed_interval< T >& l,
	const closed_interval< T >& r
)
{
	return !( l == r );
}

/** @brief A set of (closed) intervals, for efficiently storing many consecutive elements
 *
 * The value_type must be orderable with operator<
 *
 * @todo A specialization for char32_t and possibly others where we can use unsigned arithmetic or
 * bit masks to achieve faster checks.
 */
template< typename T >
class interval_set
{
public:
	using value_type    = T;
	using interval_type = closed_interval< T >;

	// Add a single value (effectively, the interval [c,c]) to the collection
	void insert(value_type c)
	{
		insert(c, c);
	}

	// Add the interval [l,h] to the collection. If h < l, this function has no effect
	void insert(
		value_type l,
		value_type h
	)
	{
		if ( h < l )
		{
			return;
		}

		// Compare funciton
		auto it = std::lower_bound(intervals.begin(), intervals.end(), l, comp);

		if ( it == intervals.end() || h < it->first )
		{
			// [l,h] disjoint from any other interval
			intervals.insert(it, interval_type{ l, h });
		}
		else
		{
			// [l,h] and *it intersect
			if ( l < it->first )
			{
				// Adjust lower bound
				it->first = l;
			}

			if ( h > it->second )
			{
				// Adjust upper bound
				it->second = h;

				// Merge any following intervals that overlap with [?,h]
				auto jt = it;
				++jt;

				while ( jt != intervals.end() && h >= jt->first )
				{
					if ( jt->second > h )
					{
						it->second = jt->second;
					}

					++jt;
				}

				// erase it + 1 to jt
				++it;
				intervals.erase(it, jt);
			}
		}
	}

	/// @todo Smarter
	void insert(const interval_set& o)
	{
		for (std::size_t i = 0; i < o.interval_count(); ++i)
		{
			const auto [first, second] = o.interval(i);
			insert(first, second);
		}
	}

	// Check if c is in the collection
	std::size_t count(value_type l) const
	{
		const auto it = std::lower_bound(intervals.begin(), intervals.end(), l, comp);

		return it == intervals.end() || l < it->first ? 0 : 1;
	}

	std::size_t interval_count() const
	{
		return intervals.size();
	}

	const interval_type& interval(std::size_t i) const
	{
		return intervals.at(i);
	}
private:
	static bool comp(
		const interval_type& e,
		const value_type&    x
	)
	{
		return e.second < x;
	}

	// Contents of the set, in  Single values are stored as (x,x)
	std::vector< interval_type > intervals;
};

template< typename T >
bool operator==(
	const interval_set< T >& l,
	const interval_set< T >& r
)
{
	const std::size_t n = l.interval_count();

	if ( r.interval_count() == n )
	{
		for (std::size_t i = 0; i < n; ++i)
		{
			if ( l.interval(i) != r.interval(i) )
			{
				return false;
			}
		}

		return true;
	}

	return false;
}

template< typename T >
bool operator!=(
	const interval_set< T >& l,
	const interval_set< T >& r
)
{
	return !( l == r );
}

// Check if this and o have a common value
template< typename T >
bool intersect(
	const interval_set< T >& l,
	const interval_set< T >& r
)
{
	const std::size_t na = l.interval_count();
	const std::size_t nb = r.interval_count();

	std::size_t i = 0;
	std::size_t j = 0;

	while ( i < na && j < nb )
	{
		const auto& li = l.interval(i);
		const auto& rj = r.interval(j);

		if ( li.first > rj.second )
		{
			// [1, 5] [7, 8]
			++j;
		}
		else if ( li.second < rj.first )
		{
			++i;
		}
		else
		{
			return true;
		}
	}

	return false;
}

template< typename Iterator, typename T >
Iterator find_first_not_of(
	Iterator                 first,
	Iterator                 last,
	const interval_set< T >& c
)
{
	Iterator it = first;

	while ( it != last && c.count(*it) != 0 )
	{
		++it;
	}

	return it;
}

// Invert the collection. Everything that wasn't in it is now in it, and vice versa.
/// @todo Implement this with set subtraction
template< typename T, T Min = std::numeric_limits< T >::min(), T Max = std::numeric_limits< T >::max() >
interval_set< T > invert(const interval_set< T >& c)
{
	interval_set< T > x;

	const std::size_t m = c.interval_count();

	if ( m == 0 )
	{
		// Nothing in collection, therefore add everything
		x.insert(Min, Max);
	}
	else
	{
		if ( Min < c.interval(0).first )
		{
			x.insert(Min, c.interval(0).first - 1);
		}

		for (std::size_t j = 1; j < m; ++j)
		{
			x.insert(c.interval(j - 1).second + 1, c.interval(j).first - 1);
		}

		if ( c.interval(m - 1).second != Max )
		{
			x.insert(c.interval(m - 1).second + 1, Max);
		}
	}

	return x;
}

template< typename C >
std::ostream& operator<<(
	std::ostream&            os,
	const interval_set< C >& c
)
{
	const std::size_t n = c.interval_count();

	for (std::size_t i = 0; i < n; ++i)
	{
		const auto& ci = c.interval(i);

		if ( i != 0 )
		{
			os << ",";
		}

		if ( ci.first == ci.second )
		{
			os << ci.first;
		}
		else
		{
			os << ci.first << "-" << ci.second;
		}
	}

	return os;
}

#endif // INTERVAL_SET_H
