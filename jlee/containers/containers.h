#ifndef CONTAINERS_H
#define CONTAINERS_H

#include <iostream>
#include <iterator>
#include <vector>

template< typename T, typename A >
void append(
	std::vector< T, A >&       a,
	const std::vector< T, A >& b
)
{
	a.insert(a.end(), b.begin(), b.end() );
}

/// @todo Further template
template< typename T >
bool starts_with(
	const std::vector< T >& a,
	const std::vector< T >& b,
	std::size_t             j
)
{
	const std::size_t n = b.size() - j;

	if ( a.size() < n )
	{
		return false;
	}

	for (std::size_t i = 0; i < n; ++i)
	{
		if ( a[i] != b[j + i] )
		{
			return false;
		}
	}

	return true;
}

template< typename T >
bool contains(
	const std::vector< T >& v,
	const T&                x
)
{
	for (std::size_t i = 0, n = v.size(); i < n; ++i)
	{
		if ( v[i] == x )
		{
			return true;
		}
	}

	return false;
}

const std::size_t NOT_FOUND = std::size_t(-1);

template< typename RandomIterator1, typename RandomIterator2 >
RandomIterator1 find_subsequence(
	RandomIterator1 first1,
	RandomIterator1 last1,
	RandomIterator2 first2,
	RandomIterator2 last2
)
{
	if ( first1 < last1 )
	{
		const std::ptrdiff_t m = last2 - first2;

		for (RandomIterator1 it = first1; m - 1 < last1 - it; ++it)
		{
			std::ptrdiff_t k = 0;

			while ( k < m && it[k] == first2[k] )
			{
				++k;
			}

			if ( k == m )
			{
				return it;
			}
		}
	}

	return last1;
}

template< typename ForwardIterator >
void print(
	std::ostream&   os,
	ForwardIterator first,
	ForwardIterator last
)
{
	bool comma = false;

	os << "[";

	while ( first != last )
	{
		if ( comma )
		{
			os << ",";
		}

		os << *first++;
		comma = true;
	}

	os << "]";
}

template< typename Container >
void print(
	std::ostream&    os,
	const Container& c
)
{
	print(os, std::begin(c), std::end(c) );
}

#endif // CONTAINERS_H
