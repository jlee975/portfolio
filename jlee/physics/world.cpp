#include "world.h"

#include <algorithm>
#include <cstdint>

#include "math/geometry/intersection/test_geometry.h"

#ifdef DEBUG_PHYSICS
#define DEBUG_PHYSICS_WORLD
#endif

World::World() = default;

/** @todo Whatever code we write for changing the velocity, we may as well call
 *    that here, as if the velocity had been changed from 0 to v
 * @todo Take time as a parameter
 */
std::size_t World::addBody(Body b)
{
	const std::size_t i = curr.size();

	const time_type t = from_secs(b.getBeginTime() );

	curr.emplace_back(std::move(b), nextid);

	for (std::size_t j = 0; j < i; ++j)
	{
		events.insert(events.begin(), event{ retest, t, i, j });
	}

	/// @todo Manually insert at the right point
	std::sort(events.begin(), events.end(), event_less);
	unknowntime = events.empty() ? UINTMAX_C(-1) : events.front().time;
	return nextid++;
}

void World::addLight(Light l)
{
	lights.push_front(std::move(l) );
}

/** @brief Find and log collisions in the scene
 *
 * @todo Combine collision detection with building a BSP tree. Make nodes of
 *   the tree store separating planes. When inserting an object, compare against
 *   the planes first. The sorting essentially does a poor man's BSP.
 */

int World::event_compare(
	const event& l,
	const event& r
)
{
	if ( l.time < r.time )
	{
		return -1;
	}

	if ( l.time > r.time )
	{
		return 1;
	}

	// same time. Check type
	if ( l.type < r.type )
	{
		return -1;
	}

	if ( l.type > r.type )
	{
		return 1;
	}

	// Same type. Check objects
	if ( l.object1 < r.object1 )
	{
		return -1;
	}

	if ( l.object1 > r.object1 )
	{
		return 1;
	}

	// Same objects. Check second object if applicable
	if ( l.object2 < r.object2 )
	{
		return -1;
	}

	if ( l.object2 > r.object2 )
	{
		return 1;
	}

	// Equivalent
	return 0;
}

bool World::event_less(
	const event& l,
	const event& r
)
{
	return event_compare(l, r) < 0;
}

bool World::event_equal(
	const event& l,
	const event& r
)
{
	return event_compare(l, r) == 0;
}

/** This thread looks forward in time to generate the correct information for object placement, when requested
 *
 */
void World::step()
{
	// As far in the future we are willing to look (1s)
	const auto tend = seentime + 1000000000;

	// only look one second into the future
	/// @todo yield, or wait for seentime to be updated
	// update by TIME_STEP
	while ( !events.empty() && events.front().time < tend )
	{
		const auto e = events.front();
		events.erase(events.begin() );

		// apply this event
		switch ( e.type )
		{
		case retest:
			do_retest(e.object1, e.object2);
			break;
		case bounding_sphere_collision:
			do_bounding_sphere_collision(to_secs(e.time), e.object1, e.object2);
			break;
		} // switch

	}

	/// @todo This only needs to be called if events.front is changed. Though I
	/// guess it's cheap enough
	if ( !events.empty() )
	{
		unknowntime = events.front().time;
	}
	else
	{
		// in the unlikely event that there will never be another collision... current info is good forever
		/// @todo Use the appropriate type instead of int64_t
		unknowntime = std::numeric_limits< time_type >::max();
	}
}

/// @todo Extract as testing if lines get close
void World::do_retest(
	std::size_t i,
	std::size_t j
)
{
	const auto p = Body::do_retest_inner(curr[i].first, curr[j].first);

	if ( p.first )
	{
		// event is in the time line of these two objects
		events.insert(events.begin(),
			event{ bounding_sphere_collision, from_secs(p.second), std::min(i, j), std::max(i, j) });
		std::sort(events.begin(), events.end(), event_less);
		unknowntime = events.empty() ? UINTMAX_C(-1) : events.front().time;
	}
}

void World::do_bounding_sphere_collision(
	seconds_type t,
	std::size_t  i,
	std::size_t  j
)
{
	/// @todo structured binding
	const std::pair< float, geo::intersection_result< 3 > > coll = Body::collision(t, curr[i].first, curr[j].first);

	// case collision
	// case separated and moving away
	// case separated and moving together
	if ( coll.second.type == geo::intersects )
	{
		/// @bug te+1 should really be te, but that is creating the same events over and over
		// Time of collision
		const time_type te = from_secs(coll.first);
		auto [l, r]
		    = Body::apply_collision(coll.first, curr[i].first, curr[j].first, coll.second.intersection_m);

		const std::size_t k = curr.size();

		/// @bug curr[k] should not test against curr[i] (they are the same object).
		/// This is handled by testing the beginning of curr[k] against end time of
		/// curr[i]. Check that floating point error doesn't cause a problem here
		if ( l )
		{
			const std::size_t ii = curr.size();
			curr.emplace_back(std::move(*l), curr[i].second);
			curr[i].first.setEndTime(coll.first);

			// remove events involving colliding objects
			for (std::size_t e = 0; e < events.size();)
			{
				if ( events[e].time > te && ( events[e].object1 == i || events[e].object2 == i ) )
				{
					events.erase(events.begin() + e);
				}
				else
				{
					++e;
				}
			}

			for (std::size_t z = 0; z < k; ++z)
			{
				if ( z != i )
				{
					events.insert(events.begin(), event{ retest, te, ii, z });
				}
			}
		}

		if ( r )
		{
			const std::size_t jj = curr.size();
			curr.emplace_back(std::move(*r), curr[j].second);
			curr[j].first.setEndTime(coll.first);

			// remove events involving colliding objects
			for (std::size_t e = 0; e < events.size();)
			{
				if ( events[e].time > te && ( events[e].object1 == j || events[e].object2 == j ) )
				{
					events.erase(events.begin() + e);
				}
				else
				{
					++e;
				}
			}

			for (std::size_t z = 0; z < k; ++z)
			{
				if ( z != j )
				{
					events.insert(events.begin(), event{ retest, te, jj, z });
				}
			}
		}

		/// @todo Manually insert at the correct point, during the preceding loop
		std::sort(events.begin(), events.end(), event_less);
		unknowntime = events.empty() ? UINTMAX_C(-1) : events.front().time;
	}
}

bool World::ready(resolution_type t_) const
{
	const auto c = t_.count();

	return c >= 0 && static_cast< unsigned long long >( c ) >= unknowntime;
}

/// @bug When objects collide, they are in contact for a non-zero amount of time. Have
/// 1ns period where objects are motionless, then apply the new direction. This will help,
/// too with creating new events "at the same time"

Scene World::view(resolution_type t_)
{
	const time_type t(t_.count() );

	// create the correct positioning information for time t and knowntime
	Scene v;

	// Add all bodies and adjust time
	/// @bug Not deleting old objects
	for (std::size_t i = 0, n = curr.size(); i < n;)
	{
		const auto& b    = curr[i].first;
		const auto  tend = from_secs(b.getEndTime() );

		if ( tend <= t )
		{
			// the object has expired. Delete it.
			if ( i != n - 1 )
			{
				curr[i] = curr[n - 1];

				// Delete i events and update n-1 events
				std::size_t k = 0;

				for (std::size_t j = 0; j < events.size(); ++j)
				{
					if ( !( events[j].object1 == i || events[j].object2 == i ) )
					{
						if ( events[j].object1 == n - 1 )
						{
							events[j].object1 = i;
						}

						if ( events[j].object2 == n - 1 )
						{
							events[j].object2 = i;
						}

						events[k++] = events[j];
					}
				}

				events.resize(k);
			}

			curr.pop_back();
			--n;
		}
		else
		{
			const auto ti = from_secs(b.getBeginTime() );

			if ( ti <= t )
			{
				// object exists
				v.bodies.emplace_back(curr[i]);
				v.bodies.back().first.advance(to_secs(t - ti) );
			}

			++i;
		}
	}

	#ifdef DEBUG_PHYSICS_WORLD
	{
		for (std::size_t i = 0, n = v.bodies.size(); i < n; ++i)
		{
			for (std::size_t j = 0; j < i; ++j)
			{
				const auto res
				    = test_intersect(v.bodies[i].first.getGeometry(), v.bodies[i].first.getFrame(),
					v.bodies[j].first.getGeometry(), v.bodies[j].first.getFrame() );

				if ( res.type != geo::no_intersection )
				{
					throw std::logic_error("Collision was missed");
				}
			}
		}
	}
	#endif

	v.lights.assign(lights.begin(), lights.end() );

	seentime = t + 1;

	return v;
}
