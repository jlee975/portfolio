#ifndef SCENE_H
#define SCENE_H

#include <utility>
#include <vector>

#include "body.h"
#include "light.h"

// State of the physical world at a particular time
/// @todo Only need IDs of objects
struct Scene
{
	/// @todo A lot of Body information is not needed for drawing
	std::vector< std::pair< Body, std::size_t > > bodies;
	std::vector< Light > lights;
};

#endif // SCENE_H
