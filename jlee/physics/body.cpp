/// @todo Make a "Path" interface that will do things like vel/acc or springs
/// @todo Replace "turn" with angular velocity vectors
/// @todo Do something about the many updates of aabb. Return aabb to physics
/// loop and manage bounding box in octree or BSP.
/// @todo Use trajectories (origin, direction,time) to calculate current position
/// @todo Check conservation of momentum and kinetic energy
#include "body.h"

#include <utility>

#include "math/geometry/intersection/test_geometry.h"
#include "math/geometry/print.h"

constexpr float MOMENTUM_TOLERANCE       = 1e-6f;
constexpr float KINETIC_ENERGY_TOLERANCE = 1e-4f;

#ifdef DEBUG_PHYSICS
#define DEBUG_PHYSICS_BODY
#define DEBUG_PHYSICS_BODY_PRINT
#endif

/// @todo Configurable for column major or row major. Currently column major
// Body::Body()
// : id(0), mass(1.f), frame(), velocity(), model(0), aabb(),
// radius(), collisions()
// {
// turn[0] = turn[1] = turn[2] = 0;
// }
Body::Body(std::shared_ptr< geo::Geometry > geom_)
	: Body(0, std::move(geom_) )
{
}

Body::Body(
	double                           tbegin_,
	std::shared_ptr< geo::Geometry > geom_
)
	: tbegin(tbegin_), tend(INFINITY), mass(1.f), frame(1), velocity(zero_initialized), geom(std::move(geom_) ),
	aabb(uninitialized), radius()
{
	turn[0] = turn[1] = turn[2] = 0;
	update_aabb();
	update_radius();
}

void Body::update_aabb()
{
	aabb = frame(geom->bounding_box() );
}

/// @todo Should be called if model changed
void Body::update_radius()
{
	radius = frame(geom->physical_radius() );
}

geo::point< 3 > Body::extremum(const geo::ray< 3 >& v) const
{
	return frame(geom->extremum(geo::support< 3 >(frame.invert(v) ) ) );
}

bool Body::isFixed() const
{
	return std::isinf(mass);
}

bool Body::isInsubstantial() const
{
	return mass <= 0;
}

void Body::setMass(float m)
{
	mass = m;
}

void Body::setBeginTime(double t)
{
	tbegin = t;
}

double Body::getBeginTime() const
{
	return tbegin;
}

void Body::setEndTime(double t)
{
	tend = t;
}

double Body::getEndTime() const
{
	return tend;
}

Body Body::bodyAt(
	double               t,
	const geo::ray< 3 >& n,
	float                v
) const
{
	Body b = *this;

	b.advance(t - tbegin);
	b.velocity += topoint(n, v);
	b.tbegin    = t;
	return b;
}

Body Body::bodyAt(
	double               t,
	const geo::ray< 3 >& n
) const
{
	return bodyAt(t, n, -2 * velocity_component(n) );
}

float Body::velocity_component(const geo::ray< 3 >& n) const
{
	return la::inner(topoint(n, 1), velocity);
}

// resolve (elastic) collisions
/// @bug The returned bodies should be guaranteed to be non-colliding
std::pair< std::optional< Body >, std::optional< Body > > Body::apply_collision(
	double                             t,
	const Body&                        l,
	const Body&                        r,
	const geo::intersection_info< 3 >& ci
)
{
	/// @todo Rather than basing off mass, Body should have a PHYSICS_TYPE member (ex., immovable)
	if ( l.isInsubstantial() || r.isInsubstantial() )
	{
		// no effect
		return { std::nullopt, std::nullopt };
	}

	if ( l.isFixed() )
	{
		if ( r.isFixed() )
		{
			// nothing to do. Both fixed objects stay in place
			return { std::nullopt, std::nullopt };
		}

		return { std::nullopt, r.bodyAt(t, ci.n) };
	}

	if ( r.isFixed() )
	{
		return { l.bodyAt(t, ci.n), std::nullopt };
	}

	// component of velocity in direction of plane's normal (before collision)
	const float v1 = l.velocity_component(ci.n);
	const float v2 = r.velocity_component(ci.n);

	// velocity of this object after collision
	const float u1 = resolved_velocity(v1, l.mass, v2, r.mass);
	const float u2 = resolved_velocity(v2, r.mass, v1, l.mass);

	#ifdef DEBUG_PHYSICS_BODY
	Body l2 = l.bodyAt(t, ci.n, u1);
	Body r2 = r.bodyAt(t, ci.n, u2);

	{
		// Check the total momentum is conserved
		const auto momentum_before = l.mass * l.velocity + r.mass * r.velocity;
		const auto momentum_after  = l2.mass * l2.velocity + r2.mass * r2.velocity;

		if ( ( momentum_after - momentum_before ).length2() > MOMENTUM_TOLERANCE )
		{
			#ifdef DEBUG_PHYSICS_BODY_PRINT
			std::cerr << momentum_before << std::endl;
			std::cerr << momentum_after << std::endl;
			std::cerr << momentum_after - momentum_before << std::endl;
			#endif
			throw std::logic_error("Momentum not conserved after collision");
		}
	}

	{
		// Check the total kinetic energy is conserved
		const auto kinetic_energy_before
		    = 0.5f * l.mass * l.velocity.length2() + 0.5f * r.mass * r.velocity.length2();
		const auto kinetic_energy_after
		    = 0.5f * l2.mass * l2.velocity.length2() + 0.5f * r2.mass * r2.velocity.length2();

		if ( std::abs(kinetic_energy_after - kinetic_energy_before) > KINETIC_ENERGY_TOLERANCE )
		{
			#ifdef DEBUG_PHYSICS_BODY_PRINT
			std::cerr << kinetic_energy_before << std::endl;
			std::cerr << kinetic_energy_after << std::endl;
			std::cerr << kinetic_energy_after - kinetic_energy_before << std::endl;
			#endif
			throw std::logic_error("Kinetic energy not conserved after collision");
		}
	}

	return { std::move(l2), std::move(r2) };

	#else
	return { l.bodyAt(t, ci.n, u1), r.bodyAt(t, ci.n, u2) };

	#endif // ifdef DEBUG_PHYSICS_BODY
}

float Body::resolved_velocity(
	float lv,
	float lm,
	float rv,
	float rm
)
{
	return ( 2 * rm * rv + ( lm - rm ) * lv ) / ( lm + rm ) - lv;
}

void Body::advance(float dt)
{
	/// @todo Use similar logic to Camera keyboard handling
	frame.translate(velocity * dt);

	rotate(frame, dt * static_cast< float >( turn[0] ), dt * static_cast< float >( turn[1] ),
		dt * static_cast< float >( turn[2] ) );

	/*

	   if ( turn[0] != 0 )
	   {
	    qrep.rotatex(( theta * dt ) * static_cast< float >( turn[0] ));
	   }

	   if ( turn[1] != 0 )
	   {
	    qrep.rotatey(( theta * dt ) * static_cast< float >( turn[1] ));
	   }

	   if ( turn[2] != 0 )
	   {
	    qrep.rotatez(( theta * dt ) * static_cast< float >( turn[2] ));
	   }
	 */

	update_aabb();
}

const geo::Geometry& Body::getGeometry() const
{
	return *geom;
}

Affine& Body::getFrame()
{
	return frame;
}

const Affine& Body::getFrame() const
{
	return frame;
}

void Body::setFrame(const Affine& f)
{
	frame = f;
	update_aabb();
	update_radius();
}

Affine Body::frameAt(double t) const
{
	/// @todo Avoid copy if we can
	Affine f = frame;

	/// @todo Apply rotation, actually
	/// @todo f1 = o1.frame * change_since_t1. i.e., write the change as an affine transformation itself
	f.translate(velocity * float(t - tbegin) );
	return f;
}

std::pair< bool, float > Body::do_retest_inner(
	const Body& bi,
	const Body& bj
)
{
	#if 0
	{
		const Affine f1 = bi.getFrame();
		const Affine f2 = bj.getFrame();

		if ( ( f1.origin() == geo::point< 3 >{{ 0, 0, 60 }} && f2.origin() == geo::point< 3 >{{ -50, 50, -10 }}
		       && bi.getVelocity() ==
		       geo::point< 3 >{{ 0, 0, 0 }} && bj.getVelocity() == geo::point< 3 >{{ 5, 5, 10 }} )
		     || ( f2.origin() == geo::point< 3 >{{ 0, 0, 60 }} && f1.origin() == geo::point< 3 >{{ -50, 50, -10 }}
		          && bj.getVelocity() ==
		          geo::point< 3 >{{ 0, 0, 0 }} && bi.getVelocity() == geo::point< 3 >{{ 5, 5, 10 }} )
                                                                                                      )
		{
			std::cout << "HERE" << std::endl;
		}
	}
	#endif

	if ( bi.isFixed() && bj.isFixed() )
	{
		return { false, INFINITY };
	}

	const float ti = bi.getBeginTime();
	const float tj = bj.getBeginTime();

	if ( !( bi.getEndTime() <= tj || bj.getEndTime() <= ti ) )
	{
		// find time interval [t0,t1] where paths are close enough for objects to intersect

		/* The distance between two points moving in a straight line at constant velocity is
		 * a quadratic. We simply solve for when that distance is small enough to bring their
		 * bounding spheres in contact.
		 */
		/// @todo This will produce false positives for, for example, object-plane collisions where
		/// the radius of the plane is effectively infinite
		/// @todo Can use doubles here, probably
		const auto   vij = bi.getVelocity() - bj.getVelocity();
		const double a   = vij.length2();

		if ( a > 0 )
		{
			float t1 = INFINITY;
			float t2 = -INFINITY;

			// relative positions at time zero
			const auto pij = ( bi.getFrame().origin() - bi.getVelocity() * ti )
			                 - ( bj.getFrame().origin() - bj.getVelocity() * tj );

			if ( bi.getGeometry().get_hint() == geo::Plane || bj.getGeometry().get_hint() == geo::Plane )
			{
				// Planes have large (if not infinite) physical radius, so false positives are easy to come by
				if ( bi.getGeometry().get_hint() == geo::Plane )
				{
					/// @bug This prevents planes from colliding
					if ( bj.getGeometry().get_hint() != geo::Plane )
					{
						std::tie(t1, t2) = do_retest_plane(bi, bj, ti, tj);
					}
				}
				else
				{
					std::tie(t1, t2) = do_retest_plane(bj, bi, tj, ti);
				}
			}
			else
			{
				const double r = bi.getPhysicalRadius() + bj.getPhysicalRadius();
				const double c = la::inner(pij, pij) - r * r;
				const double b = 2 * la::inner(pij, vij);

				const double d2 = b * b - 4 * a * c;

				if ( d2 >= 0 )
				{
					// there's a chance they will collide
					const double d = std::sqrt(d2);

					t1 = ( -b - d ) / ( 2 * a );
					t2 = ( -b + d ) / ( 2 * a );
				}
			}

			// trim times to when the objects actually exist
			t1 = std::max< float >(t1, std::max(bi.getBeginTime(), bj.getBeginTime() ) );
			t2 = std::min< float >(t2, std::min(bi.getEndTime(), bj.getEndTime() ) );

			// check that we have a non-empty interval where the objects might actually collide
			/// @todo Instances of -1ull need to be replaced with max_time
			if ( t1 <= t2 && t1 < -1ull && t2 >= 0 )
			{
				return { true, t1 };
			}
		}
	}

	return { false, INFINITY };
}

std::pair< float, float > Body::do_retest_plane(
	const Body& bi,
	const Body& bj,
	float       ti,
	float       tj
)
{
	// difference between current positions. Points towards plane
	const auto pij
	    = ( bi.getFrame().origin() - bi.getVelocity() * ti ) - ( bj.getFrame().origin() - bj.getVelocity() * tj );

	const auto vij = bi.getVelocity() - bj.getVelocity();

	/// @bug I think the pij and vij values get flipped
	// normal of plane
	/// @todo want some better way of retreiving plane's normal. Or some better guarantee of
	/// plane's normal being (1,0,0)
	auto ni = bi.getFrame() ( geo::ray< 3 >({ 1, 0, 0 }) );
	auto n  = topoint(ni, 1);

	// distance of object to plane. Flip the normal so it points "away" from bj
	auto d = la::inner(pij, n);

	if ( d < 0 )
	{
		n *= -1;
		d *= -1;
	}

	// velocity component along plane normal
	auto sij = la::inner(vij, n);

	if ( sij < 0 )
	{
		// object is moving toward plane
		const auto r = bj.getFrame() ( bj.getPhysicalRadius() );

		return { ( d - r ) / -sij, ( d + r ) / -sij };
	}

	// object is moving away from plane (or they are at rest, relatively)
	return { INFINITY, -INFINITY };
}

/// @todo Exact collision detection. Right now, just doing a binary search
/// @bug Take into account rotation of objects
/// @todo If dt steps are small enough, use a convex hull of the object at times t and t + dt
/// and use 4d gjk. (for small dt, this object will nearly be convex anyway)
/// @todo Exhaustive face-by-face test for collisions
/// @todo Exact collision detection. Once collision detection determines closest points,
/// "follow" them through time. they'll be along a particular face until the object rotates
/// out of the way, at which point it will be on a new face. Do this until the objects
/// clear each other.
/// @bug If two bodies are moving away from each other, they are not colliding
std::pair< float, geo::intersection_result< 3 > > Body::collision(
	float       t_,
	const Body& o1,
	const Body& o2
)
{
	/// @todo Might be using this object repeatedly from Body::collision
	float t0 = t_;
	float t1 = t_;

	while ( true )
	{
		const Affine f1 = o1.frameAt(t1);
		const Affine f2 = o2.frameAt(t1);

		const geo::intersection_result< 3 > coll = test_intersect(*( o1.geom ), f1, *( o2.geom ), f2);

		if ( coll.type != geo::no_intersection )
		{
			return { t1, coll };
		}

/// @todo Should have some guarantee in place that coll.n points "toward" a
/// @todo The gap would have been calculated during the collision detection. Store it and use it here.
/// @todo Remove the guess work with p11, p12, etc.
// how big is the gap between these objects, parallel to this plane
		#ifdef DEBUG_PHYSICS_BODY
		{
			const auto& si = coll.separation_m;

			const auto n1  = f1.invert(si.n);
			const auto p1  = f1(o1.geom->extremum(geo::support< 3 >(n1) ) );
			const auto p11 = f1(o1.geom->extremum(geo::support< 3 >(-n1) ) );
			const auto d1  = si.d1;
			const auto d11 = la::inner(si.n, p11);

			if ( !( d1 >= d11 ) )
			{
				#ifdef DEBUG_PHYSICS_BODY_PRINT
				std::cerr << p11 << "; " << p1 << std::endl;
				std::cerr << d11 << "; " << d1 << std::endl;
				#endif
				throw std::logic_error("Normal of separating plane is in the wrong direction");
			}
		}
		#endif

		#ifdef DEBUG_PHYSICS_BODY
		{
			const auto& si = coll.separation_m;

			const auto n2  = f2.invert(si.n);
			const auto p2  = f2(o2.geom->extremum(geo::support< 3 >(-n2) ) );
			const auto p22 = f2(o2.geom->extremum(geo::support< 3 >(n2) ) );
			const auto d2  = si.d2;
			const auto d22 = la::inner(si.n, p22);

			if ( !( d2 <= d22 ) )
			{
				#ifdef DEBUG_PHYSICS_BODY_PRINT
				std::cerr << p22 << "; " << p2 << std::endl;
				std::cerr << d22 << "; " << d2 << std::endl;
				#endif
				throw std::logic_error("Normal of separating plane is in the wrong direction");
			}
		}
		#endif

		// gap width
		const auto d = coll.separation_m.d1 - coll.separation_m.d2;

		// how fast are they closing that gap? per second
		const auto v = la::inner(o1.velocity - o2.velocity, coll.separation_m.n);

		if ( ( v > 0 && d < 0 ) || ( v < 0 && d > 0 ) )
		{
			// objects are closing the gap. Move time forward as much as we can until we hit separating plane
			/// @bug if |d/v| is much smaller than t, we get an infinite loop
			t0  = t1;
			t1 += std::max< float >(-( d / v ), 1.f / 1024.f);
		}
		else
		{
			return { 0, geo::separation_info< 3 >{ zero_initialized, 0, 0 }};
		}
	}
}

void Body::setScale(float s)
{
	frame.rescale(s);
	update_aabb();
	update_radius();
}

void Body::setVelocity(
	float dx,
	float dy,
	float dz
)
{
	velocity = geo::point< 3 >{{ dx, dy, dz }};
}

const geo::point< 3 >& Body::getVelocity() const
{
	return velocity;
}

bool Body::less_by_x(
	const Body& a,
	const Body& b
)
{
	return a.aabb.min[0] < b.aabb.min[0];
}

bool Body::left_of(const Body& b) const
{
	return aabb.max[0] < b.aabb.min[0];
}

bool Body::test_yz(const Body& b) const
{
	return !( aabb.max[1] < b.aabb.min[1] || aabb.min[1] > b.aabb.max[1] || aabb.max[2] < b.aabb.min[2]
	          || aabb.min[2] > b.aabb.max[2] );
}

float Body::getPhysicalRadius() const
{
	return radius;
}
