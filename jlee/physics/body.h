#ifndef BODY_HPP
#define BODY_HPP

#include <memory>
#include <optional>

#include "math/geometry/affine.h"
#include "math/geometry/geometry.h"
#include "math/geometry/intersection/intersection_result.h"

typedef geo::affine< 3, float > Affine;

/** @brief Represents a physical object that has position, orientation, mass, velocity
 *
 */
class Body
{
public:
	// Body();
	explicit Body(std::shared_ptr< geo::Geometry >);

	Body(double, std::shared_ptr< geo::Geometry >);

	// Sort by minimum x of AABB
	static bool less_by_x(const Body&, const Body&);

	// return the affine transformation of rotation and translation
	Affine& getFrame();
	const Affine& getFrame() const;
	void setFrame(const Affine&);

	const geo::Geometry& getGeometry() const;
	void advance(float);
	static std::pair< std::optional< Body >, std::optional< Body > > apply_collision(double, const Body&, const Body&, const geo::intersection_info< 3 >&);
	float getPhysicalRadius() const;

	// change velocity
	void setVelocity(float, float, float);
	const geo::point< 3 >& getVelocity() const;

	// change properties
	void setScale(float);
	void setMass(float);
	void setBeginTime(double);
	double getBeginTime() const;
	void setEndTime(double);
	double getEndTime() const;

	// collision handling
	/// @todo rename
	/// @todo Use std::optional
	static std::pair< bool, float > do_retest_inner(const Body&, const Body&);
	static std::pair< float, geo::intersection_result< 3 > > collision(float, const Body&, const Body&);
	bool left_of(const Body&) const;
	bool test_yz(const Body&) const;

	geo::point< 3 > extremum(const geo::ray< 3 >&) const;

	bool isFixed() const;
	bool isInsubstantial() const;
private:
	static std::pair< float, float > do_retest_plane(const Body&, const Body&, float, float);
	float velocity_component(const geo::ray< 3 >&) const;
	static float resolved_velocity(float, float, float, float);
	Body bodyAt(double, const geo::ray< 3 >&, float) const;
	Body bodyAt(double, const geo::ray< 3 >&) const;
	Affine frameAt(double) const;
	void update_aabb();
	void update_radius();

	// time the object begins to exist
	double tbegin;

	// time object ceases to exist
	double tend;

	float mass;

	// position, orientation, scale
	Affine frame;

	// movement
	geo::point< 3 > velocity; // velocity
	int             turn[3];  // angular velocity

	// model
	std::shared_ptr< geo::Geometry > geom;

	geo::box< 3 > aabb;
	float         radius; // physical radius of model
};

#endif // ifndef BODY_HPP
