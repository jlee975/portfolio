#ifndef WORLD_HPP
#define WORLD_HPP

#include <chrono>
#include <forward_list>

#include "body.h"
#include "light.h"
#include "scene.h"

/** @brief Represents objects in the physical world
 *
 * Contains objects and lights and passes them to the drawing thread. Does
 * physics calculations, such as collision detection
 *
 * @todo In general can we use HPET or RDTSC of the cpu for times instead of steady_clock? Avoid
 * rounding that the steady_clock would do when converting to nanoseconds
 * @todo Periodically reset times relative to seentime. With 64-bits and nanosecond precision,
 * we can only run for 524 years. But if we sometimes subtract seentime-ish then we could run
 * forever.
 * @todo Allocator for events
 */
class World
{
public:
	using resolution_type = std::chrono::nanoseconds;

	World();
	void step();
	void addLight(Light);
	std::size_t addBody(Body);
	bool ready(resolution_type) const;

	/// @todo It's probably fine for camera to request state accurate to millisecond
	/// accuracy, while we internally track nano-seconds
	Scene view(resolution_type);
private:
	enum event_type
	{
		retest,                   ///< Test mentioned object against all live objects
		bounding_sphere_collision ///< Bounding spheres are going to collide
	};

	using seconds_type = float;
	using time_type    = std::uintmax_t;

	static seconds_type to_secs(time_type x)
	{
		return static_cast< seconds_type >( x / 1000000000.0 );
	}

	static time_type from_secs(seconds_type x)
	{
		if ( x > std::numeric_limits< time_type >::max() / 1000000000 )
		{
			return std::numeric_limits< time_type >::max();
		}

		return static_cast< time_type >( x * 1000000000 );
	}

	/// @invariant object1 < object2 if two objects are involved
	struct event
	{
		event_type type;
		time_type time;
		std::size_t object1;
		std::size_t object2;
	};

	void do_retest(std::size_t, std::size_t);
	void do_bounding_sphere_collision(seconds_type, std::size_t, std::size_t);

	/// @todo Make the usual operators
	static int event_compare(const event&, const event&);
	static bool event_less(const event&, const event&);
	static bool event_equal(const event&, const event&);

	// Live objects. (time last updated
	/// @todo Separately track stationary and fixed objects
	std::vector< std::pair< Body, std::size_t > > curr;

	// time when next test event occurs
	/// @todo Use a time_point from a custom clock representing game time
	time_type unknowntime{ 0 };

	// camera has seen up to, but not including, this time. Given in resolution_type
	time_type seentime{ 0 };

	// (time, object1, object2)
	/// @todo Change container
	std::vector< event > events;

	std::forward_list< Light > lights; ///< Lights in the scene

	std::size_t nextid{ 0 };
};

#endif // WORLD_HPP
