#ifndef STDTYPES_H
#define STDTYPES_H

#include <type_traits>
#include <memory>
#include <utility>

#include <string>
#include <string_view>
#include <chrono>
#include <optional>

#include <vector>
#include <forward_list>
#include <tuple>
#include <deque>
#include <set>
#include <map>
#include <list>
#include <array>
#include <unordered_map>
#include <unordered_set>
#include <variant>

#include "out.h"

namespace jl
{
namespace serialization
{
template< typename T, typename A >
struct category< std::vector< T, A > >
{
	using type = sequence_tag;
};

template< typename T, typename A >
struct category< std::list< T, A > >
{
	using type = sequence_tag;
};

template< typename T, typename A >
struct category< std::forward_list< T, A > >
{
	using type = sequence_tag;
};

template< typename T, typename A >
struct category< std::deque< T, A > >
{
	using type = sequence_tag;
};

template< typename K, typename T, typename C, typename A >
struct sequence_element< std::map< K, T, C, A > >
{
	using type = std::pair< K, T >;
};

template< typename K, typename T, typename C, typename A >
struct sequence_element< std::multimap< K, T, C, A > >
{
	using type = std::pair< K, T >;
};

template< typename K, typename V, typename H, typename E, typename A >
struct sequence_element< std::unordered_map< K, V, H, E, A > >
{
	using type = std::pair< K, V >;
};

template< typename K, typename V, typename H, typename E, typename A >
struct sequence_element< std::unordered_multimap< K, V, H, E, A > >
{
	using type = std::pair< K, V >;
};

template< typename K, typename T, typename C, typename A >
struct category< std::map< K, T, C, A > >
{
	using type = sequence_tag;
};

template< typename K, typename T, typename C, typename A >
struct category< std::multimap< K, T, C, A > >
{
	using type = sequence_tag;
};

template< typename K, typename C, typename A >
struct category< std::set< K, C, A > >
{
	using type = sequence_tag;
};

template< typename K, typename C, typename A >
struct category< std::multiset< K, C, A > >
{
	using type = sequence_tag;
};

template< typename K, typename V, typename H, typename E, typename A >
struct category< std::unordered_map< K, V, H, E, A > >
{
	using type = sequence_tag;
};

template< typename K, typename V, typename H, typename E, typename A >
struct category< std::unordered_multimap< K, V, H, E, A > >
{
	using type = sequence_tag;
};

template< typename K, typename H, typename E, typename A >
struct category< std::unordered_set< K, H, E, A > >
{
	using type = sequence_tag;
};

template< typename K, typename H, typename E, typename A >
struct category< std::unordered_multiset< K, H, E, A > >
{
	using type = sequence_tag;
};

template< typename C, typename T, typename A >
struct category< std::basic_string< C, T, A > >
{
	using type = string_tag;
};

template< typename C, typename T >
struct category< std::basic_string_view< C, T > >
{
	using type = string_tag;
};

template< typename T >
struct category< std::shared_ptr< T > >
{
	using type = pointer_tag;
};

template< typename T >
struct category< std::unique_ptr< T > >
{
	using type = pointer_tag;
};

template< typename T >
struct category< std::optional< T > >
{
	using type = pointer_tag;
};

namespace details
{
struct ymd_proxy
{
	std::chrono::year year;
	std::chrono::month month;
	std::chrono::day day;

	ymd_proxy()
	{
	}

	explicit ymd_proxy(const std::chrono::year_month_day& ymd)
		: year( ymd.year() ), month( ymd.month() ), day( ymd.day() )
	{

	}

	operator std::chrono::year_month_day() const
	{
		return { year, month, day };
	}
};

struct ym_proxy
{
	std::chrono::year year;
	std::chrono::month month;

	ym_proxy()
	{
	}

	explicit ym_proxy(const std::chrono::year_month& ym)
		: year( ym.year() ), month( ym.month() )
	{

	}

	operator std::chrono::year_month() const
	{
		return { year, month };
	}
};

struct md_proxy
{
	std::chrono::month month;
	std::chrono::day day;

	md_proxy()
	{
	}

	explicit md_proxy(const std::chrono::month_day& md)
		: month( md.month() ), day( md.day() )
	{

	}

	operator std::chrono::month_day() const
	{
		return { month, day };
	}
};
}

template<>
struct reflection_types< details::ymd_proxy >
{
	using type = pack< member< &details::ymd_proxy::year, "y" >,
	                   member< &details::ymd_proxy::month, "m" >,
	                   member< &details::ymd_proxy::day, "d" > >;
};

template<>
struct reflection_types< details::ym_proxy >
{
	using type = pack< member< &details::ym_proxy::year, "y" >,
	                   member< &details::ym_proxy::month, "m" > >;
};

template<>
struct reflection_types< details::md_proxy >
{
	using type = pack< member< &details::md_proxy::month, "m" >,
	                   member< &details::md_proxy::day, "d" > >;
};

template<>
struct category< std::chrono::year_month_day >
{
	using type  = proxy_tag;
	using proxy = details::ymd_proxy;
};

template<>
struct category< std::chrono::year_month >
{
	using type  = proxy_tag;
	using proxy = details::ym_proxy;
};

template<>
struct category< std::chrono::month_day >
{
	using type  = proxy_tag;
	using proxy = details::md_proxy;
};

template<>
struct category< std::chrono::year >
{
	using type  = proxy_tag;
	using proxy = int;
};

template<>
struct category< std::chrono::month >
{
	using type  = proxy_tag;
	using proxy = unsigned;
};

template<>
struct category< std::chrono::day >
{
	using type  = proxy_tag;
	using proxy = unsigned;
};

#if 0
template< typename...Ts >
struct category< std::variant< Ts... > >
{
	using type = call_function_tag;

	template< std::size_t I >
	static bool serialize_inner(
		jl::serialization::ostream_base& os,
		const std::variant< Ts... >&     v
	)
	{
		if constexpr ( I < sizeof...( Ts ) )
		{
			if ( const auto* p = std::get_if< I >(&v) )
			{
				os.start_object();
				os.field_name("index", 5);
				os << I;
				os.object_separator();
				os.field_name("value", 5);
				os << *p;
				os.finish_object();
				return true;
			}

			return serialize_inner< I + 1 >(os, v);
		}
		else
		{
			return false;
		}
	}

	static bool serialize(
		jl::serialization::ostream_base& os,
		const std::variant< Ts... >&     v
	)
	{
		return serialize_inner< 0 >(os, v);
	}

};
#endif // if 0
}
}

#endif // STDTYPES_H
