#ifndef JL_SERIALIZATION_OUT_H
#define JL_SERIALIZATION_OUT_H

#include <iosfwd>
#include <type_traits>
#include <iterator>
#include <fstream>
#include <filesystem>
#include <concepts>
#include <functional>

#include "category.h"

namespace jl
{
namespace serialization
{
class ostream_base
{
public:
	explicit ostream_base(std::ostream& os_)
		: os(os_)
	{
	}

	virtual ~ostream_base() = default;

	virtual void writeb(bool)                           = 0;
	virtual void writei(long long x)                    = 0;
	virtual void writeu(unsigned long long x)           = 0;
	virtual void writef(long double x)                  = 0;
	virtual void writenull()                            = 0;
	virtual void start_array()                          = 0;
	virtual void array_separator()                      = 0;
	virtual void finish_array()                         = 0;
	virtual void start_object()                         = 0;
	virtual void finish_object()                        = 0;
	virtual void object_separator()                     = 0;
	virtual void field_name(const char* s, std::size_t) = 0;

	virtual void write_string(const char* s, std::size_t) = 0;
protected:
	std::ostream& os;
};

template< typename T, std::size_t I >
void tuple_serializer(ostream_base& os, const T& x);


class DerpityDerp
{
public:
	explicit DerpityDerp(ostream_base& os)
		: m_os(os), m_first(true)
	{
	}

	template< typename B >
	void operator()(const B&)
	{
		throw std::logic_error("Serialize base");
	}

	template< typename T, typename P >
	void operator()(
		const T&                x2,
		P                       ptom,
		const std::string_view& label
	)
	{
		if ( !m_first )
		{
			m_os.object_separator();
		}

		m_os.field_name(label.data(), label.size() );

		if constexpr ( std::is_member_function_pointer_v< decltype( ptom ) >)
		{
			m_os << std::invoke(ptom, x2);
		}
		else
		{
			m_os << x2.*ptom;
		}

		m_first = false;
	}
private:
	ostream_base& m_os;
	bool          m_first;
};

/// @todo If has inspect, but category<T> not defined
template< typename T >
ostream_base& operator<<(
	ostream_base& os,
	const T&      x
)
{
	// First check if the type T has a serialization category set explicitly
	if constexpr ( bool_category< T >)
	{
		os.writeb(x);
	}
	else if constexpr ( signed_category< T >)
	{
		os.writei(x);
	}
	else if constexpr ( unsigned_category< T >)
	{
		os.writeu(x);
	}
	else if constexpr ( floating_category< T >)
	{
		os.writef(x);
	}
	else if constexpr ( string_category< T >)
	{
		os.write_string(x.data(), x.size() );
	}
	else if constexpr ( tuple_category< T >)
	{
		os.start_array();

		tuple_serializer< T, 0 >(os, x);

		os.finish_array();
	}
	else if constexpr ( sequence_category< T >)
	{
		// Note that tuple_category may kick in before sequence. Ex., for std::array
		os.start_array();

		auto it   = std::begin(x);
		auto last = std::end(x);

		bool comma = false;

		for (; it != last; ++it)
		{
			if ( comma )
			{
				os.array_separator();
			}

			os << *it;
			comma = true;
		}

		os.finish_array();
	}
	else if constexpr ( serialization::reflectable_s< T >)
	{
		os.start_object();

		DerpityDerp f(os);

		/// @todo The reflection is inverted. We should be mapping members to a generic type
		/// (in this case, labels)
		jl::reflection::visit< typename serialization::serialize_type< T >::type >(x, f);

		os.finish_object();
	}
	else if constexpr ( pointer_category< T >)
	{
		/// @todo May be able to detect automatically
		if ( x )
		{
			return os << ( *x );
		}

		os.writenull();
		return os;
	}
	else if constexpr ( call_function_category< T >)
	{
		/// @todo Rename. Especially because we may not be able to define custom_serializer in
		/// the same namespace as T (or in jl::serialize
		/// @todo Generic serializable type. Used to have marshall::object, etc.
		/// @todo Do away with c_str
		const auto s = x.serialize();
		os.write_string(s.data(), s.length() );
	}
	else if constexpr ( proxy_category< T >)
	{
		typename category< T >::proxy y(x);

		os << y;
	}
	else
	{
		static_assert(std::is_same_v< void, T >, "No serialization function for type");
	}

	return os;
}

/// @todo Avoid recursion
template< typename T, std::size_t I >
void tuple_serializer(
	ostream_base& os,
	const T&      x
)
{
	if constexpr ( I < std::tuple_size_v< T >)
	{
		if constexpr ( I != 0 )
		{
			os.array_separator();
		}

		os << std::get< I >(x);

		tuple_serializer< T, I + 1 >(os, x);
	}
}

template< typename F, typename T >
bool save(
	const std::filesystem::path& path,
	const T&                     x
)
{
	std::ofstream file(path);

	F os(file);

	os << x;
	return file.good();
}

}
}

#endif // JL_SERIALIZATION_OUT_H
