#ifndef CATEGORY_H
#define CATEGORY_H

#include <type_traits>
#include <tuple>

#include "reflection/reflection.h"

namespace jl
{
namespace serialization
{
struct bool_tag {};
struct signed_tag {};
struct unsigned_tag {};
struct floating_point_tag {};
struct sequence_tag {};      // iterate over object at run time
struct string_tag {};        // a sequence of characters, quoted
struct tuple_tag {};         // number of elements fixed at compile time, could be mixed types
struct pointer_tag {};       // serialize the stored object, or an empty object if nullptr
struct call_function_tag {}; // call the function indicated by category::serializer or category::deserializer
struct proxy_tag {};         // category struct should indicate a type to use instead for serialization/deserialization

template< typename T >
struct sequence_element
{
	using type = typename T::value_type;
};

namespace details
{
template< typename T >
concept Enum = std::is_enum_v< T >;

/// @todo Should also require get<I>(t) for all 0.. tuple_size
template< class T >
concept Tuple = requires(T t)
{
	typename std::tuple_size< T >::type;
};

template< typename T >
struct predefined_serialization {};

template< Enum T >
struct predefined_serialization< T >
{
	using type  = proxy_tag;
	using proxy = std::underlying_type_t< T >;
};

template< Tuple T >
struct predefined_serialization< T >
{
	using type = tuple_tag;
};
}

/// @todo Instead of a struct, can probably get away with a typedef
template< typename T >
struct category
	: public details::predefined_serialization< T >
{
};

template<>
struct category< bool >
{
	using type = bool_tag;
};

template<>
struct category< short >
{
	using type = signed_tag;
};

template<>
struct category< int >
{
	using type = signed_tag;
};

template<>
struct category< long >
{
	using type = signed_tag;
};

template<>
struct category< long long >
{
	using type = signed_tag;
};

template<>
struct category< unsigned short >
{
	using type = unsigned_tag;
};

template<>
struct category< unsigned int >
{
	using type = unsigned_tag;
};

template<>
struct category< unsigned long >
{
	using type = unsigned_tag;
};

template<>
struct category< unsigned long long >
{
	using type = unsigned_tag;
};

template<>
struct category< float >
{
	using type = floating_point_tag;
};

template<>
struct category< double >
{
	using type = floating_point_tag;
};

template<>
struct category< long double >
{
	using type = floating_point_tag;
};

template< typename T, std::size_t N >
struct category< T [N] >
{
	using type = sequence_tag;
};

template< typename T >
struct category< T* >
{
	using type = pointer_tag;
};

template< typename T, typename C, typename = void >
constexpr bool is_category = false;

template< typename T, typename C >
constexpr bool is_category< T, C, std::void_t< decltype( sizeof( typename category< T >::type ) ) > > = std::is_same_v< C, typename category< T >::type >;

template< typename T >
concept bool_category = is_category< T, bool_tag >;

template< typename T >
concept signed_category = is_category< T, signed_tag >;

template< typename T >
concept unsigned_category = is_category< T, unsigned_tag >;

template< typename T >
concept floating_category = is_category< T, floating_point_tag >;

template< typename T >
concept string_category = is_category< T, string_tag >;

template< typename T >
concept sequence_category = is_category< T, sequence_tag >;

template< typename T >
concept pointer_category = is_category< T, pointer_tag >;

template< typename T >
concept tuple_category = is_category< T, tuple_tag >;

template< typename T >
concept call_function_category = is_category< T, call_function_tag >;

template< typename T >
concept proxy_category = is_category< T, proxy_tag >;

template< typename T >
struct reflection_types {};

template< typename T >
concept is_reflectable_b = requires{ typename reflection_types< T >::type; };

template< typename T, typename = void >
concept is_reflectable_s = requires{ typename reflection_types< T >::serialize; };

template< typename T >
concept only_reflectable_s = is_reflectable_s< T >;

template< typename T >
concept reflectable_s = is_reflectable_s< T >|| is_reflectable_b< T >;

template< typename T, typename = void >
concept is_reflectable_d = requires{ typename reflection_types< T >::deserialize; };

template< typename T >
concept only_reflectable_d = is_reflectable_d< T >;

template< typename T >
concept reflectable_d = is_reflectable_d< T >|| is_reflectable_b< T >;

template< typename T >
struct serialize_type
{
	using type = typename reflection_types< T >::type;
};

template< only_reflectable_s T >
struct serialize_type< T >
{
	using type = typename reflection_types< T >::serialize;
};

template< typename T >
struct deserialize_type
{
	using type = typename reflection_types< T >::type;
};

template< only_reflectable_d T >
struct deserialize_type< T >
{
	using type = typename reflection_types< T >::deserialize;
};

using reflection::pack;
using reflection::member;
}
}

#endif // CATEGORY_H
