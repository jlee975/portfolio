/** @brief Basic operations to support big integer algorithms, usually with assembly language implementations
 *
 * @todo shld instruction for word sized operands, w/ arch optimizations
 * @todo Provide arch_adc() (add with carry) and/or arch_sbb (subtract with
 * borrow) and/or arch_mul_adc() since arch_mul almost always involves an
 * addition immediately after
 * @todo MSVC asm implementations of arch_mul and arch_div
 */

#ifndef PRIMITIVES_H
#define PRIMITIVES_H

#include <climits>
#include <limits>

template< typename T >
struct division_t
{
	T quotient;
	T remainder;
};

/** @brief Holds the result of wide multiplication
 */
template< typename T >
struct product_t
{
	/// The low N bits of the product
	T lo;

	/// The high N bits of the product
	T hi;
};

/** @brief Full width multiplication of two unsigned integers
 * @param a A factor
 * @param b A factor
 *
 * U is an unsigned integral type.
 *
 * This function is the heart of the multiplication routines, allowing us to
 * multiply two unsigned integers and get the full width result. While there is
 * fall back code to accomplish this in pure C++, it is intended to be an
 * assembly language wrapper. Full specializations are provided to accomplish
 * this, depending on architecture.
 *
 * @todo static_assert that digits is divisible by two
 */
template< typename U >
product_t< U > arch_mul(
	U a,
	U b
)
{
	// A shift value to split U into lo and hi bit sets
	static const unsigned c = std::numeric_limits< U >::digits / 2;

	// A mask for the lower bits
	static const U m = ( ~U(0) >> c );

	const U al = a & m;
	const U ah = a >> c;
	const U bl = b & m;
	const U bh = b >> c;

	const U x  = bl * ah;
	const U y  = al * bh;
	const U z  = al * bl;
	const U lo = a * b;

	const U carry = ( ( ( x & m ) + ( y & m ) + ( z >> c ) ) >> c );
	const U hi    = ( ah * bh ) + ( x >> c ) + ( y >> c ) + carry;

	product_t< U > w = { lo, hi };

	return w;
}

/* Portable way to check for 32 and 64-bit values. Need to be careful of shifts and integral constants.
 * In particular, we can't be sure that ull exists, or shifting by 32 (or 16) bits is defined
 */
#define IS32(X) ( ( X > 0xffffu ) && ( ( X >> 16 ) == 0xffffu ) && ( ( X & 0xffffu ) == 0xffffu ) )
#define IS64(X) ( ( X > 0xfffffffful ) && IS32(X >> 32) && IS32(X & 0xfffffffful) )

#if ( defined( __GNUG__ ) && defined( __x86_64__ ) )
// 64-bit multiplication available

#if ( defined( ULLONG_MAX ) && IS64(ULLONG_MAX) )
// 64-bit unsigned long long
template<>
inline product_t< unsigned long long > arch_mul< unsigned long long >(
	unsigned long long a,
	unsigned long long b
)
{
	product_t< unsigned long long > result;

	asm ( "mulq %3" : "=a" ( result.lo ), "=d" ( result.hi ) : "a" ( a ), "d" ( b ) );
	return result;
}

#endif

#if IS64(ULONG_MAX)
// 64-bit unsigned long
template<>
inline product_t< unsigned long > arch_mul< unsigned long >(
	unsigned long a,
	unsigned long b
)
{
	product_t< unsigned long > result;

	asm ( "mulq %3" : "=a" ( result.lo ), "=d" ( result.hi ) : "a" ( a ), "d" ( b ) );
	return result;
}

#elif IS32(ULONG_MAX)
// 32-bit unsigned long
template<>
inline product_t< unsigned long > arch_mul< unsigned long >(
	unsigned long a,
	unsigned long b
)
{
	product_t< unsigned long > result;

	asm ( "mull %3" : "=a" ( result.lo ), "=d" ( result.hi ) : "a" ( a ), "d" ( b ) );
	return result;
}

#endif // if IS64(ULONG_MAX)

#if IS64(UINT_MAX)
// 64-bit unsigned
template<>
inline product_t< unsigned > arch_mul< unsigned >(
	unsigned a,
	unsigned b
)
{
	product_t< unsigned > result;

	asm ( "mulq %3" : "=a" ( result.lo ), "=d" ( result.hi ) : "a" ( a ), "d" ( b ) );
	return result;
}

#elif IS32(UINT_MAX)
// 32-bit unsigned
template<>
inline product_t< unsigned > arch_mul< unsigned >(
	unsigned a,
	unsigned b
)
{
	product_t< unsigned > result;

	asm ( "mull %3" : "=a" ( result.lo ), "=d" ( result.hi ) : "a" ( a ), "d" ( b ) );
	return result;
}

#endif // if IS64(UINT_MAX)

#elif ( defined( __GNUG__ ) && defined( __i386__ ) )
// 32-bit multiplication available
#if IS32(ULONG_MAX)
// 32-bit unsigned long
template<>
inline product_t< unsigned long > arch_mul< unsigned long >(
	unsigned long a,
	unsigned long b
)
{
	product_t< unsigned long > result;

	asm ( "mull %3" : "=a" ( result.lo ), "=d" ( result.hi ) : "a" ( a ), "d" ( b ) );
	return result;
}

#endif
#if IS32(UINT_MAX)
// 32-bit unsigned
template<>
inline product_t< unsigned > arch_mul< unsigned >(
	unsigned a,
	unsigned b
)
{
	product_t< unsigned > result;

	asm ( "mull %3" : "=a" ( result.lo ), "=d" ( result.hi ) : "a" ( a ), "d" ( b ) );
	return result;
}

#endif
#endif // if ( defined( __GNUG__ ) && defined( __x86_64__ ))

/** @brief Divide a wide unsigned integer value by a single unsigned integer value
 * @param hi The high word of the dividend. Must be < d
 * @param lo The low word of the dividend.
 * @param d The divisor. Must not be zero.
 *
 * Divide a double wide integer by a regular integer. Forms the basis for other
 * div operations, much like arch_mul is the basis for multiplication operations.
 *
 * The input must satisfy hi < d, so that the result fits in a single unsigned
 * long. For performance reasons, this condition is _not_ checked. The only places
 * where this function is called in this source, the assumption holds.
 *
 * Also, d must be greater than 0. Again, the only places where this function is
 * called that assumption holds.
 *
 * While this code has a pure C++ fall back implementation, it is best to use (or
 * implement) assembly language appropriate for the processor. Specializations are
 * provided based on architecture.
 *
 * The fallback uses binary long division.
 *
 * @note The brackets and order of operations in the fall back code must be
 * done precisely as they are. There are problems with additions overflowing, etc.
 * @todo For fall back code -- couldn't we just initialize q and rl with 0 and lo?
 * @todo The fall back code has been smooshed together from two different
 * functions so it looks flat out awful right now.
 */
// bit-by-bit long division
template< typename U >
division_t< U > arch_div(
	U hi,
	U lo,
	U d
)
{
	division_t< U > result;
	U               x = hi % d;

	// Quotient
	U q(0);

	for (int m = std::numeric_limits< U >::digits - 1; m >= 0; --m)
	{
		U b = ( ( lo >> m ) & U(1) );

		if ( x + b >= d - x )
		{
			x  = ( x + b ) - ( d - x );
			q += U(1) << m;
		}
		else
		{
			x = x + x + b;
		}
	}

	result.quotient  = q;
	result.remainder = x;
	return result;
}

#if ( defined( __GNUG__ ) && defined( __x86_64__ ) )
// 64-bit division available

#if ( defined( ULLONG_MAX ) && IS64(ULLONG_MAX) )
template<>
inline division_t< unsigned long long > arch_div< unsigned long long >(
	unsigned long long hi,
	unsigned long long lo,
	unsigned long long d
)
{
	division_t< unsigned long long > result;

	asm ( "divq %4" : "=a" ( result.quotient ), "=d" ( result.remainder ) : "a" ( lo ), "d" ( hi ), "r" ( d ) );
	return result;
}

#endif

#if IS64(ULONG_MAX)
template<>
inline division_t< unsigned long > arch_div< unsigned long >(
	unsigned long hi,
	unsigned long lo,
	unsigned long d
)
{
	division_t< unsigned long > result;

	asm ( "divq %4" : "=a" ( result.quotient ), "=d" ( result.remainder ) : "a" ( lo ), "d" ( hi ), "r" ( d ) );
	return result;
}

#elif IS32(ULONG_MAX)
template<>
inline division_t< unsigned long > arch_div< unsigned long >(
	unsigned long hi,
	unsigned long lo,
	unsigned long d
)
{
	division_t< unsigned long > result;

	asm ( "divl %4" : "=a" ( result.quotient ), "=d" ( result.remainder ) : "a" ( lo ), "d" ( hi ), "r" ( d ) );
	return result;
}

#endif // if IS64(ULONG_MAX)

#if IS64(UINT_MAX)
template<>
inline division_t< unsigned > arch_div< unsigned >(
	unsigned hi,
	unsigned lo,
	unsigned d
)
{
	division_t< unsigned > result;

	asm ( "divq %4" : "=a" ( result.quotient ), "=d" ( result.remainder ) : "a" ( lo ), "d" ( hi ), "r" ( d ) );
	return result;
}

#elif IS32(UINT_MAX)
template<>
inline division_t< unsigned > arch_div< unsigned >(
	unsigned hi,
	unsigned lo,
	unsigned d
)
{
	division_t< unsigned > result;

	asm ( "divl %4" : "=a" ( result.quotient ), "=d" ( result.remainder ) : "a" ( lo ), "d" ( hi ), "r" ( d ) );
	return result;
}

#endif // if IS64(UINT_MAX)

#elif ( defined( __GNUG__ ) && defined( __i386__ ) )
// 32-bit multiplication available
#if IS32(ULONG_MAX)
template<>
division_t< unsigned long > arch_div< unsigned long >(
	unsigned long hi,
	unsigned long lo,
	unsigned long d
)
{
	division_t< unsigned long > result;

	asm ( "divl %4" : "=a" ( result.quotient ), "=d" ( result.remainder ) : "a" ( lo ), "d" ( hi ), "r" ( d ) );
	return result;
}

#endif
#if IS32(UINT_MAX)
// 32-bit unsigned
template<>
division_t< unsigned > arch_div< unsigned >(
	unsigned hi,
	unsigned lo,
	unsigned d
)
{
	division_t< unsigned > result;

	asm ( "divl %4" : "=a" ( result.quotient ), "=d" ( result.remainder ) : "a" ( lo ), "d" ( hi ), "r" ( d ) );
	return result;
}

#endif
#else
#endif // if ( defined( __GNUG__ ) && defined( __x86_64__ ))

#undef IS64
#undef IS32

#endif // PRIMITIVES_H
