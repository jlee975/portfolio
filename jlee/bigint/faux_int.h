#ifndef FAUX_INT_H
#define FAUX_INT_H

#include <limits>

template< typename T >
struct faux_int
{
	using value_type   = T;
	using safe_bool_fn = void(faux_int< T >::*) () const;

	void safe_bool() const
	{
	}

	faux_int() = default;

	explicit faux_int(value_type x)
		: value(x)
	{
	}

	operator safe_bool_fn() const
	{
		return value ? &faux_int::safe_bool : 0;
	}

	friend faux_int operator+(
		const faux_int& a,
		const faux_int& b
	)
	{
		return faux_int(a.value + b.value);
	}

	friend faux_int operator-(
		const faux_int& a,
		const faux_int& b
	)
	{
		return faux_int(a.value - b.value);
	}

	friend faux_int operator*(
		const faux_int& a,
		const faux_int& b
	)
	{
		return faux_int(a.value * b.value);
	}

	friend faux_int operator/(
		const faux_int& a,
		const faux_int& b
	)
	{
		return faux_int(a.value / b.value);
	}

	friend faux_int operator%(
		const faux_int& a,
		const faux_int& b
	)
	{
		return faux_int(a.value % b.value);
	}

	friend bool operator==(
		const faux_int& a,
		const faux_int& b
	)
	{
		return a.value == b.value;
	}

	friend bool operator!=(
		const faux_int& a,
		const faux_int& b
	)
	{
		return a.value != b.value;
	}

	friend bool operator<(
		const faux_int& a,
		const faux_int& b
	)
	{
		return a.value < b.value;
	}

	friend bool operator>(
		const faux_int& a,
		const faux_int& b
	)
	{
		return a.value > b.value;
	}

	friend bool operator>=(
		const faux_int& a,
		const faux_int& b
	)
	{
		return a.value >= b.value;
	}

	friend bool operator<=(
		const faux_int& a,
		const faux_int& b
	)
	{
		return a.value <= b.value;
	}

	faux_int& operator++()
	{
		++value;
		return *this;
	}

	faux_int& operator--()
	{
		--value;
		return *this;
	}

	faux_int operator--(int)
	{
		return faux_int(value++);
	}

	faux_int& operator-=(const faux_int& x)
	{
		value -= x.value;
		return *this;
	}

	faux_int& operator&=(const faux_int& x)
	{
		value &= x.value;
		return *this;
	}

	faux_int& operator^=(const faux_int& x)
	{
		value ^= x.value;
		return *this;
	}

	faux_int& operator|=(const faux_int& x)
	{
		value |= x.value;
		return *this;
	}

	faux_int& operator+=(const faux_int& x)
	{
		value += x.value;
		return *this;
	}

	friend faux_int operator|(
		const faux_int& a,
		const faux_int& b
	)
	{
		return faux_int(a.value | b.value);
	}

	friend faux_int operator&(
		const faux_int& a,
		const faux_int& b
	)
	{
		return faux_int(a.value & b.value);
	}

	friend faux_int operator^(
		const faux_int& a,
		const faux_int& b
	)
	{
		return faux_int(a.value ^ b.value);
	}

	faux_int& operator>>=(int c)
	{
		value >>= c;
		return *this;
	}

	faux_int& operator<<=(int c)
	{
		value <<= c;
		return *this;
	}

	faux_int operator<<(int c) const
	{
		return faux_int(value << c);
	}

	faux_int operator<<(unsigned c) const
	{
		return faux_int(value << c);
	}

	faux_int operator<<(unsigned long c) const
	{
		return faux_int(value << c);
	}

	faux_int operator>>(unsigned c) const
	{
		return faux_int(value >> c);
	}

	faux_int operator-() const
	{
		return faux_int(-value);
	}

	faux_int operator~() const
	{
		return faux_int(~value);
	}

	value_type value;
};

namespace std
{
template< typename T >
struct numeric_limits< faux_int< T > >
	: std::numeric_limits< T >
{
};
}

#endif // FAUX_INT_H
