/** @file zinteger.h
 * @author Jonathan Lee <jonathan.lee.975@gmail.com>
 * @brief A class and functions for multiple precision integers.
 *
 * This source provides a class, ZInteger, that represents multiple precision
 * integers. It can be used much like the built-in integer types. Most, if not
 * all, operations can mix types using global overloads.
 *
 * The class is in the namespace jl::bignum.
 *
 * Some extensions to the C++ standard library are provided, such as output to an
 * ostream, and a specialization of std::swap.
 *
 * There is also a specialization of the C++11 random number generator class,
 * uniform_int_distribution. It is largely for test purposes, and for some number
 * theory algorithms that rely on random numbers (ex., probalistic primality
 * testing). It is not particularly fast or statistically robust.
 *
 * My goal to provide a rigorous C++ library that is fast and easy to use. To the
 * best of my knowledge, all algorithms are in the public domain. Bugs, feature
 * requests, optimizations, questions, suggestions, C++ concerns etc. can be sent
 * to Jonathan Lee <jonathan.lee.975@gmail.com>. Please include "zinteger" in the
 * subject so it doesn't get lost among the randomness.
 *
 * --------------------------------------------------------------------------------
 *
 *            Copyright Jonathan Lee 2009 - 2020.
 *   Distributed under the Boost Software License, Version 1.0.
 *      (See accompanying file LICENSE_1_0.txt or copy at
 *            http://www.boost.org/LICENSE_1_0.txt)
 * --------------------------------------------------------------------------------
 *
 * @section com  Compiling
 *
 * Using the class simply involves including the zinteger.h file in your source code.
 * You'll probably want a typedef or using declaration for the class.
 *
 * A test suite is available as zinteger_test.cpp. It is probably included in
 * with this source. See that file for compilation instructions.
 *
 * Some macros will enable C++11 features if they are defined. The header should
 * define them under appropriate conditions (esp. when __cplusplus indicates
 * the new version of C++ is supported). These macros will be named USE_XXX
 * where XXX is some feature that will be reasonably self documenting. Ex.,
 * USE_RVALUE_REFERENCES.
 *
 * Passes the following g++ flags without error or warning:
 *   -pedantic -ansi -Wall -Wextra -Wshadow -Wpointer-arith -Wcast-qual
 *   -Wcast-align -Wwrite-strings -Wconversion -Wold-style-cast -Winit-self
 *   -Wfloat-equal -Wsign-conversion -Wredundant-decls -Weffc++
 *
 * Passes cppcheck -s -a without warning.
 *
 * @section ref References
 *
 * -# "The Art of Computer Programming: Vol. 2",
 *      Donald Knuth
 * -# "Prime Numbers: A Computational Perspective",
 *      R. Crandall and C. Pomerance
 * -# "Comparison of Three Modular Reduction Functions",
 *      A. Bosselaers, R. Govaerts, and J. Vandewalle
 * -# "Toom-Cook 3-way", http://bodrato.it/toom-cook/
 *      M. Bodrato
 *
 * --------------------------------------------------------------------------------
 *
 */
#ifndef ZINTEGER_HPP
#define ZINTEGER_HPP

/// Enable any C++11 features. In particular, Rvalue references.
#if ( __cplusplus >= 201103L )
#define USE_RVALUE_REFERENCES
#define USE_LONG_LONG
#define USE_EXPLICIT_BOOL
#endif

#include <cstddef>
#include <iosfwd>
#include <string>
#include <utility>

#include "basic_nonnegative.h"
#include "bits/uabs.h"

namespace jl
{
namespace bignum
{

/** @brief Class for handling large integer arithmetic.
 *
 * ZInteger is a class for multi-precision integer arithmetic. It is designed to
 * respect the usual notation of the built-in types, so that code will be legible
 * and compact. For example, the usual operators +, -, *, /, and % can be used as
 * expected. Comparison, logical, and bitwise perators are also provided. For
 * example,
 *
 * @code
 * ZInteger a;                    // Default constructor sets a equal to 0
 * ZInteger b = 23719;            // A ZInteger can be set to a specific value
 * ZInteger c(b);                 // Or a ZInteger can be initialized to a copy
 * a = b + c;                     // Assignment and arithmetic work as usual
 * b *= 45;
 *
 * a = (a / 3 - (c << 100) % b);  // More complicated expressions are possible
 *
 * cout << a << endl;
 * @endcode
 *
 * Internally, the class stores integers in a sign/magnitude format, with the magnitude
 * being a basic_nonnegative.
 *
 * Note for security users: The class is designed to only allocate memory in the
 * member function reserve(). Memory is only deleted in reserve() and the
 * destructor. Thus, if one wanted to make the class use secure memory, these would
 * be the places to cut in.
 *
 * @section dev Developers
 *
 * The following conditions must always hold, and some optimizations depend on
 * them:
 *
 * @invariant (mag.nnum != 0) implies (mag.pnum != 0)
 * @invariant (negative == true) implies (mag.nnum != 0)
 * @invariant (mag.nnum != 0) implies (mag.pnum[mag.nnum - 1] != 0)
 * @invariant (mag.nnum <= mag.mnum) always
 *
 * @todo Still needs a good algorithm for division. Current is O(n^2).
 * @todo Should div() satisfy remainder >= 0, so it will be a Division Algorithm?
 * @todo Schonage-Strassen multiplication, FFT, Furer's algorithm
 * @todo serialize(), unserialize()
 * @todo Decide on how casting should be performed
 * @todo Try to eliminate as many calls to reduce() as possible
 * @todo The multiplication algorithms use a lot of copying and memory allocations.
 * Surely the KaratsubaLimit and ToomCookLimit values could be brought down.
 * @todo Small string optimization. Drop all the overloads for built-in types
 * 'cause they'll be cheap to construct
 */
class ZInteger
{
	static const unsigned ToomCookLimit = 1024; ///< Min. size before Toom-Cook used

	#ifndef USE_EXPLICIT_BOOL
	// For the safe bool idiom (pre-C++11)
	using safe_bool_type = void ( ZInteger::* ) () const;
	#endif
public:
	/** @name Constructors
	 *
	 * Implicit constructors for integral types are provided. No loss of
	 * precision occurs.
	 *
	 * @todo Move constructor
	 * @todo short int, unsigned short, bool, etc.
	 */
	///@{
	/** Default constructor. (Effectively) sets the ZInteger to zero.
	 *
	 * @note Guaranteed not to allocate any memory
	 */
	ZInteger()
		: mag(), negative(false)
	{
	}

	/// Copy constructor
	ZInteger(const ZInteger& x)
		: mag(x.mag), negative(x.negative)
	{
	}

	/// Construct from unsigned long
	ZInteger(unsigned long x)
		: mag(x), negative(false)
	{
	}

	/// Construct from long
	ZInteger(long x)
		: mag(uabs(x) ), negative(x < 0)
	{
	}

	/// Construct from unsigned int
	ZInteger(unsigned x)
		: mag(x), negative(false)
	{
	}

	/// Construct from int
	ZInteger(int x)
		: mag(uabs(x) ), negative(x < 0)
	{
	}

	///@}

	/// Destructor frees resources
	~ZInteger()
	{
	}

	/** Static constructor from string representation
	 *
	 * Simply calls the private constructor with the same argument list.
	 *
	 * @todo Might want to handle base prefixes (e.g., "0x") here, adjusting the
	 *  ptr and passing the correct base.
	 */
	static ZInteger from_string(
		const std::string& s,
		unsigned           base = 10
	)
	{
		return ZInteger(s, base);
	}

	/// Copy assignment
	ZInteger& operator=(const ZInteger& x)
	{
		mag      = x.mag;
		negative = x.negative;

		return *this;
	}

	ZInteger& operator=(ZInteger&& x)
	{
		mag.swap(x.mag);
		std::swap(x.negative, negative);
		return *this;
	}

	/// Assign from unsigned long
	ZInteger& operator=(unsigned long a)
	{
		mag      = a;
		negative = false;
		return *this;
	}

	/// Assign from long
	ZInteger& operator=(unsigned a)
	{
		mag      = a;
		negative = false;
		return *this;
	}

	/// Assign from unsigned int
	ZInteger& operator=(long a)
	{
		mag      = uabs(a);
		negative = ( a < 0 );
		return *this;
	}

	/// Assign from int
	ZInteger& operator=(int a)
	{
		mag      = uabs(a);
		negative = ( a < 0 );
		return *this;
	}

	/** Swap value with other
	 *
	 * Basically a pointer swap. Very cheap
	 */
	void swap(ZInteger& x)
	{
		if ( &x != this )
		{
			mag.swap(x.mag);
			std::swap(x.negative, negative);
		}
	}

	/** Get the size, in bits, of a ZInteger.
	 *
	 * @returns Number of bits in representation.
	 *
	 * @exception out_of_range Thrown if there are more bits in the ZInteger
	 * than unsigned long can represent.
	 *
	 * This function returns the number of bits used to represent the integer
	 * stored by x. Equivalently, it returns one plus the index of the highest
	 * set bit. The bit width of zero is zero.
	 */
	unsigned long size() const
	{
		return mag.bit_size();
	}

// casting
	#ifdef USE_EXPLICIT_BOOL
	explicit operator bool() const
	{
		return !mag.is_zero();
	}
	#else
	operator safe_bool_type() const
	{
		return mag.is_zero() ? nullptr : &ZInteger::safe_bool_func;
	}
	#endif

	// Conversion
	unsigned long to_ulong() const
	{
		const unsigned long u = static_cast< unsigned long >( mag.loword() );

		return negative ? -u : u;
	}

	unsigned to_uint() const
	{
		const unsigned u = static_cast< unsigned >( mag.loword() );

		return negative ? -u : u;
	}

	std::string to_string(unsigned long base = 10) const
	{
		std::string s = mag.to_string(base);

		if ( negative )
		{
			s.insert(s.begin(), '-');
		}

		return s;
	}

	/** @name Comparison
	 *
	 * All of these functions return -1, 0, or 1 if *this is less than, equal to,
	 * or greater than the argument.
	 */
	///@{
	int compare(const ZInteger& v) const
	{
		if ( negative != v.negative )
		{
			return negative ? -1 : 1;
		}

		const int res = mag.compare(v.mag);

		return negative ? -res : res;
	}

	int compare(unsigned long x) const
	{
		return negative ? -1 : mag.compare(x);
	}

	int compare(unsigned x) const
	{
		return negative ? -1 : mag.compare(x);
	}

	int compare(long x) const
	{
		// differing signs
		if ( negative && x >= 0 )
		{
			return -1;
		}

		if ( !negative && x < 0 )
		{
			return 1;
		}

		if ( negative )
		{
			return -( mag.compare(uabs(x) ) );
		}
		else
		{
			return mag.compare(static_cast< unsigned long >( x ) );
		}
	}

	int compare(int x) const
	{
		return compare(static_cast< long >( x ) );
	}

	///@}

	/***************************************************************************/ /**
	   @brief		Shorthand for comparison against 0

	   @return		True if the represented value is zero, false otherwise.

	   This is really a convenience function for checking if a ZInteger is zero. It
	   does the opposite of the bool cast operator. Supposing a, and b are ZIntegers,
	   then "if(a && !b) {}" would work as they do for the built in integral types.
	   Of course, if this function were not defined, that code fragment would work just
	   as well since the boolean cast is defined. But this is slightly simpler.
	 *******************************************************************************/
	bool operator!() const
	{
		return mag.is_zero();
	}

	/** @name Arithmetic operators
	 *
	 * These all do the obvious thing
	 */
	///@{
	/***************************************************************************/ /**
	   @brief		Unary plus.
	   @return		Copy of the ZInteger.

	   This function doesn't really do a lot, except exhibit the behavior that's
	   expected.
	 *******************************************************************************/
	ZInteger operator+() const
	{
		ZInteger a(*this);

		return a;
	}

	/***************************************************************************/ /**
	   @brief		Unary negation.
	   @return		A copy of the negative of this ZInteger.
	 *******************************************************************************/
	ZInteger operator-() const
	{
		ZInteger a(*this);

		if ( !a.mag.is_zero() )
		{
			a.negative = !negative;
		}

		return a;
	}

	/***************************************************************************/ /**
	   @brief       Prefix increment.
	   @return      Reference to the ZInteger.
	 *******************************************************************************/
	ZInteger& operator++() // prefix operator
	{
		if ( negative )
		{
			// Can't be zero, so just dec
			--mag;
		}
		else
		{
			++mag;
		}

		return *this;
	}

	/***************************************************************************/ /**
	   @brief       Postfix increment
	   @return      Copy of the value before increment.
	 *******************************************************************************/
	const ZInteger operator++(int) // Postfix operator
	{
		ZInteger before(*this);

		operator++();

		return before;
	}

	/***************************************************************************/ /**
	   @brief       Prefix decrement
	   @return      Reference to this
	 *******************************************************************************/
	ZInteger& operator--()
	{
		if ( negative )
		{
			++mag;
		}
		else if ( mag.is_zero() )
		{
			mag      = 1ul;
			negative = true;
		}
		else
		{
			--mag;
		}

		return *this;
	}

	/***************************************************************************/ /**
	   @brief       Postfix decrement
	   @return      Copy of the value before decrement.
	 *******************************************************************************/
	const ZInteger operator--(int)
	{
		ZInteger before(*this);

		operator--();

		return before;
	}

	/***************************************************************************/ /**
	   @brief       Adds x to *this.
	   @return      Reference to this.

	   If *this and x have the same sign, their magnitudes are added by the function
	   abs_add(). If they are of different sign, then their absolute difference is
	   computed via abs_diff(), and the sign is adjusted if necessary.
	 *******************************************************************************/
	ZInteger& operator+=(const ZInteger& x)
	{
		if ( negative != x.negative ) // Opposite sign
		{
			abs_diff(x);
		}
		else
		{
			mag += x.mag; // Same sign
		}

		return *this;
	}

	ZInteger& operator+=(unsigned long a)
	{
		return operator+=(ZInteger(a) );
	}

	ZInteger& operator+=(unsigned a)
	{
		return operator+=(static_cast< unsigned long >( a ) );
	}

	ZInteger& operator+=(long a)
	{
		return a >= 0 ? operator+=(static_cast< unsigned long >( a ) ) :
		       operator-=(-static_cast< unsigned long >( a ) );
	}

	ZInteger& operator+=(int a)
	{
		return operator+=(static_cast< long >( a ) );
	}

	/***************************************************************************/ /**
	   @brief       Subtracts x from *this
	   @return      Reference to this.

	   Works similar to operator+=(). If the signs differ, magnitudes are added via
	   abs_add(). If the signs are the same, the absolute difference is calculated
	   via abs_diff(), and the magnitude is corrected if necessary.
	 *******************************************************************************/
	ZInteger& operator-=(const ZInteger& x)
	{
		if ( negative == x.negative ) // Same sign
		{
			abs_diff(x);
		}
		else
		{
			mag += x.mag; // Opposite sign
		}

		return *this;
	}

	ZInteger& operator-=(unsigned long a)
	{
		return operator-=(ZInteger(a) );
	}

	ZInteger& operator-=(unsigned a)
	{
		return operator-=(static_cast< unsigned long >( a ) );
	}

	ZInteger& operator-=(long a)
	{
		return a >= 0 ? operator-=(static_cast< unsigned long >( a ) ) :
		       operator+=(-static_cast< unsigned long >( a ) );
	}

	ZInteger& operator-=(int a)
	{
		return operator-=(static_cast< long >( a ) );
	}

	ZInteger& operator*=(const ZInteger&);
	ZInteger& operator*=(unsigned long);
	ZInteger& operator*=(unsigned);
	ZInteger& operator*=(long);
	ZInteger& operator*=(int);

	ZInteger& operator/=(const ZInteger&);
	ZInteger& operator/=(unsigned long);
	ZInteger& operator/=(unsigned);
	ZInteger& operator/=(long);
	ZInteger& operator/=(int);

	ZInteger& operator%=(const ZInteger&);
	ZInteger& operator%=(unsigned long);
	ZInteger& operator%=(unsigned);
	ZInteger& operator%=(long);
	ZInteger& operator%=(int);
	///@}

	/** @name Bitwise operators
	 */
	///@{
	ZInteger& operator>>=(unsigned long);
	ZInteger& operator>>=(unsigned);
	ZInteger& operator>>=(long);
	ZInteger& operator>>=(int);

	ZInteger& operator<<=(unsigned long);
	ZInteger& operator<<=(unsigned);
	ZInteger& operator<<=(long);
	ZInteger& operator<<=(int);

	ZInteger& operator&=(const ZInteger&);
	ZInteger& operator&=(unsigned long);
	ZInteger& operator&=(unsigned);

	ZInteger& operator^=(const ZInteger&);
	ZInteger& operator^=(unsigned long);
	ZInteger& operator^=(unsigned);

	ZInteger& operator|=(const ZInteger&);
	ZInteger& operator|=(unsigned long);
	ZInteger& operator|=(unsigned);
	///@}

	#ifndef NDEBUG /* We shouldn't need this for production code */
	friend class ZIntegerTester;
// i.e., ::bignum::ZIntegerTester
	#endif
private:
	using size_type = std::size_t;

	/** Construct from ASCIIZ string of digits
	 *
	 * @exception invalid_argument Occurs if the indicated base is not supported
	 * @exception overflow_error Occurs if the string is very long and cannot be
	 *            represented by a ZInteger.
	 * @exception runtime_error Thrown if string is not properly terminated, or
	 *            otherwise malformed.
	 *
	 * This constructor creates a ZInteger from positional notation. Supported
	 * bases are from 1 to 36, inclusive, and follow hexadecimal. That is,
	 * characters '0' through '9' stand for the values zero to nine,
	 * respectively, and 'a' through 'z' (case insensitive) represent 10
	 * through 35.
	 *
	 * @note This constructor is private. Use from_string() static method.
	 * @note Does not support "Base64", "Base32", or similar schemes. Nor is
	 *       this constructor meant to. Does not support "0x103"-like notation.
	 */
	ZInteger(
		const std::string& s,
		unsigned           base
	)
		: mag(), negative(false)
	{
		const std::size_t n = s.size();

		if ( n > 0 && s[0] == '-' )
		{
			mag.from_string(s.data() + 1, n - 1, base);
			negative = !mag.is_zero();
		}
		else
		{
			mag.from_string(s.data(), n, base);
		}
	}

	#ifndef USE_EXPLICIT_BOOL
	void safe_bool_func() const
	{
	} // dummy for Safe Bool idiom

	#endif

	// Memory management
	/** Reserve storage for holding a big number
	 *
	 * @param n Minimum number of words to allocate.
	 *
	 * @exception std::bad_alloc If memory is low, new[] might throw
	 *
	 * This function ensures that there are at least n words of data in the
	 * internal buffer for this bignum. If there are already n words, nothing
	 * is done. Otherwise, a new buffer is allocated and the existing data is
	 * copied to the new buffer. The old buffer is freed.
	 *
	 * In short, after this function, pnum[0] through pnum[n - 1] are guaranteed
	 * to exist.
	 *
	 * @note This is the only place where memory is allocated for ZInteger. This
	 * has been done on purpose so that another memory allocator could be
	 * slipped in. Either for security concerns, or for performance.
	 * @todo Accept bits instead of number of words, since we may not know (at
	 * the [NZ]Integer level how large a word is.
	 */

	/** Shrinks representation by discarding leading zeros
	 *
	 * This is a private function simply meant for doing the oft-repeated task
	 * of shrinking the representation of an integer. An integer value is
	 * represented by the ZInteger class in an array of nnum unsigned longs. If
	 * the most significant ulong is zero, we can discard it and still represent
	 * the same integer. Keeping the representation small aids in reducing the
	 * complexity of most operations.
	 *
	 * The version accepting an argument acts as a write accessor for nnum. This
	 * is preferred way of setting nnum internally, since it immediately adjusts
	 * nnum when higher words of pnum are zero. It is equivalent to
	 *
	 * nnum = n;
	 * reduce();
	 */

	// Shared routines for arithmetic
	/***************************************************************************/ /**
	   @brief       Calculates |*this| - |x|, and adjusts sign.

	   If the objects are of different size, a straight subtraction of the smaller
	   from the larger (in magnitude) is stored in mag.pnum. If they are the same size,
	   the larger value is determined by scanning for the highest non-equal word. Then
	   the smaller is subtracted from the larger.

	   The sign is preserved unless |x| > |*this|, in which case the sign is flipped.
	   Of course, if the difference is zero, the sign is set to false.

	   @todo simplify
	   @todo rename. This basically just calculates subtraction
	 *******************************************************************************/
	void abs_diff(const ZInteger& x)
	{
		const int comp = mag.diff(x.mag);

		if ( comp == -1 )
		{
			// flip the sign
			negative = !negative;
		}
		else if ( comp == 0 )
		{
			negative = false;
		}
	}

	// Variations of multiplication
	ZInteger mul(const ZInteger& x) const
	{
		ZInteger w;

		w.mag = mag * x.mag;

		if ( !w.mag.is_zero() )
		{
			w.negative = ( negative != x.negative );
		}

		return w;
	}

	/** Constructs a ZInteger from a section of another
	 *
	 * @param off The offset into pnum where copying should begin
	 * @param n The (max) number of ulongs to copy from pnum
	 *
	 * This function copies a number of ulongs from *this and creates a new
	 * ZInteger from that data. Used by the multiplication routines, in
	 * particular.
	 *
	 * The one argument version simply copies from off to nnum - 1.
	 *
	 * @note The result is unsigned.
	 */

	// Data members
	basic_nonnegative< unsigned long > mag;
	bool                               negative{ false }; ///< If true, indicates value is less than zero
};

/*=========================================================================*//**
   @name Global functions

   The following functions are provided so that ZInteger can work with the built-in
   types with ease. They simply call the appropriate ZInteger member function,
   possibly with type conversion. As much as possible is done to avoid constructing
   ZInteger temporaries.

   \todo Guard these with enable_if
*//*==========================================================================*/
//@{

inline bool operator==(
	const ZInteger& a,
	const ZInteger& b
)
{
	return a.compare(b) == 0;
}

inline bool operator!=(
	const ZInteger& a,
	const ZInteger& b
)
{
	return a.compare(b) != 0;
}

inline bool operator<(
	const ZInteger& a,
	const ZInteger& b
)
{
	return a.compare(b) < 0;
}

inline bool operator<=(
	const ZInteger& a,
	const ZInteger& b
)
{
	return a.compare(b) <= 0;
}

inline bool operator>(
	const ZInteger& a,
	const ZInteger& b
)
{
	return a.compare(b) > 0;
}

inline bool operator>=(
	const ZInteger& a,
	const ZInteger& b
)
{
	return a.compare(b) >= 0;
}

inline ZInteger operator+(
	const ZInteger& a,
	const ZInteger& b
)
{
	ZInteger x(a);

	x += b;
	return x;
}

inline ZInteger operator-(
	const ZInteger& a,
	const ZInteger& b
)
{
	ZInteger x(a);

	x -= b;
	return x;
}

inline ZInteger operator*(
	const ZInteger& a,
	const ZInteger& b
)
{
	ZInteger x(a);

	x *= b;
	return x;
}

inline ZInteger operator%(
	const ZInteger& a,
	const ZInteger& b
)
{
	ZInteger x(a);

	x %= b;
	return x;
}

inline ZInteger operator/(
	const ZInteger& a,
	const ZInteger& b
)
{
	ZInteger x(a);

	x /= b;
	return x;
}

inline ZInteger operator&(
	const ZInteger& a,
	const ZInteger& b
)
{
	ZInteger x(a);

	x &= b;
	return x;
}

inline ZInteger operator^(
	const ZInteger& a,
	const ZInteger& b
)
{
	ZInteger x(a);

	x ^= b;
	return x;
}

inline ZInteger operator|(
	const ZInteger& a,
	const ZInteger& b
)
{
	ZInteger x(a);

	x |= b;
	return x;
}

template< typename T >
bool operator==(
	const ZInteger& n,
	T               m
)
{
	return n.compare(m) == 0;
}

template< typename T >
bool operator!=(
	const ZInteger& n,
	T               m
)
{
	return n.compare(m) != 0;
}

template< typename T >
bool operator<(
	const ZInteger& n,
	T               m
)
{
	return n.compare(m) < 0;
}

template< typename T >
bool operator<=(
	const ZInteger& n,
	T               m
)
{
	return n.compare(m) <= 0;
}

template< typename T >
bool operator>(
	const ZInteger& n,
	T               m
)
{
	return n.compare(m) > 0;
}

template< typename T >
bool operator>=(
	const ZInteger& n,
	T               m
)
{
	return n.compare(m) >= 0;
}

template< typename T >
bool operator==(
	T               m,
	const ZInteger& n
)
{
	return n.compare(m) == 0;
}

template< typename T >
bool operator!=(
	T               m,
	const ZInteger& n
)
{
	return n.compare(m) != 0;
}

template< typename T >
bool operator<(
	T               m,
	const ZInteger& n
)
{
	return n.compare(m) > 0;
}

template< typename T >
bool operator<=(
	T               m,
	const ZInteger& n
)
{
	return n.compare(m) >= 0;
}

template< typename T >
bool operator>(
	T               m,
	const ZInteger& n
)
{
	return n.compare(m) < 0;
}

template< typename T >
bool operator>=(
	T               m,
	const ZInteger& n
)
{
	return n.compare(m) <= 0;
}

// operator+   Addition
template< typename T >
ZInteger operator+(
	const ZInteger& lhs,
	T               rhs
)
{
	ZInteger x(lhs);

	x += rhs;
	return x;
}

template< typename T >
ZInteger operator+(
	T               a,
	const ZInteger& b
)
{
	ZInteger x(b);

	x += a;
	return x;
}

// operator-   Substraction
template< typename T >
ZInteger operator-(
	const ZInteger& lhs,
	T               rhs
)
{
	ZInteger x(lhs);

	x -= rhs;
	return x;
}

template< typename T >
ZInteger operator-(
	T               a,
	const ZInteger& b
)
{
	ZInteger y = -b;

	y += a;
	return y;
}

// operator*   Multiplication
template< typename T >
ZInteger operator*(
	const ZInteger& lhs,
	T               rhs
)
{
	ZInteger x(lhs);

	x *= rhs;
	return x;
}

template< typename T >
ZInteger operator*(
	T               a,
	const ZInteger& b
)
{
	return b * a;
}

// operator/   Division
template< typename T >
ZInteger operator/(
	const ZInteger& lhs,
	T               rhs
)
{
	ZInteger x(lhs);

	x /= rhs;
	return x;
}

template< typename T >
ZInteger operator/(
	T               a,
	const ZInteger& b
)
{
	ZInteger y = a;

	y /= b;
	return y;
}

// operator%   Modulo reduction
template< typename T >
ZInteger operator%(
	const ZInteger& lhs,
	T               rhs
)
{
	ZInteger x(lhs);

	x %= rhs;
	return x;
}

// operator>>   bitwise right shift
template< typename T >
ZInteger operator>>(
	const ZInteger& lhs,
	T               rhs
)
{
	ZInteger x(lhs);

	x >>= rhs;
	return x;
}

// operator<<   bitwise left shift
template< typename T >
ZInteger operator<<(
	const ZInteger& lhs,
	T               rhs
)
{
	ZInteger x(lhs);

	x <<= rhs;
	return x;
}

// operator&   bitwise AND
inline ZInteger operator&(
	const ZInteger& lhs,
	unsigned long   rhs
)
{
	ZInteger x(rhs);

	x &= lhs;
	return x;
}

inline ZInteger operator&(
	const ZInteger& lhs,
	unsigned        rhs
)
{
	ZInteger x(rhs);

	x &= lhs;
	return x;
}

inline ZInteger operator&(
	const ZInteger& lhs,
	long            rhs
)
{
	ZInteger x(rhs >= 0 ? static_cast< unsigned long >( rhs ) : -static_cast< unsigned long >( rhs ) );

	x &= lhs;
	return x;
}

inline ZInteger operator&(
	const ZInteger& lhs,
	int             rhs
)
{
	ZInteger x(rhs >= 0 ? static_cast< unsigned long >( rhs ) : -static_cast< unsigned long >( rhs ) );

	x &= lhs;
	return x;
}

template< typename T >
ZInteger operator&(
	T               a,
	const ZInteger& b
)
{
	return b & a;
}

// operator^   bitwise XOR
inline ZInteger operator^(
	const ZInteger& lhs,
	unsigned long   rhs
)
{
	ZInteger x(lhs);

	x ^= rhs;
	return x;
}

inline ZInteger operator^(
	const ZInteger& lhs,
	unsigned        rhs
)
{
	ZInteger x(lhs);

	x ^= static_cast< unsigned long >( rhs );
	return x;
}

inline ZInteger operator^(
	const ZInteger& lhs,
	long            rhs
)
{
	ZInteger x(lhs);

	x ^= ( rhs >= 0 ? static_cast< unsigned long >( rhs ) : -static_cast< unsigned long >( rhs ) );
	return x;
}

inline ZInteger operator^(
	const ZInteger& lhs,
	int             rhs
)
{
	ZInteger x(lhs);

	x ^= ( rhs >= 0 ? static_cast< unsigned long >( rhs ) : -static_cast< unsigned long >( rhs ) );
	return x;
}

template< typename T >
ZInteger operator^(
	T               a,
	const ZInteger& b
)
{
	return b ^ a;
}

// operator|   bitwise OR
inline ZInteger operator|(
	const ZInteger& lhs,
	unsigned long   rhs
)
{
	ZInteger x(lhs);

	x |= rhs;
	return x;
}

inline ZInteger operator|(
	const ZInteger& lhs,
	unsigned        rhs
)
{
	ZInteger x(lhs);

	x |= static_cast< unsigned long >( rhs );
	return x;
}

inline ZInteger operator|(
	const ZInteger& lhs,
	long            rhs
)
{
	ZInteger x(lhs);

	x |= ( rhs >= 0 ? static_cast< unsigned long >( rhs ) : -static_cast< unsigned long >( rhs ) );
	return x;
}

inline ZInteger operator|(
	const ZInteger& lhs,
	int             rhs
)
{
	ZInteger x(lhs);

	x |= ( rhs >= 0 ? static_cast< unsigned long >( rhs ) : -static_cast< unsigned long >( rhs ) );
	return x;
}

template< typename T >
ZInteger operator|(
	T               a,
	const ZInteger& b
)
{
	return b | a;
}

//@}

std::ostream& operator<<(std::ostream&, const jl::bignum::ZInteger&);

} // end bignum namespace
}

/*=========================================================================*//**
   @name Extensions to STL

   The following functions are provided for common tasks.
*//*==========================================================================*/
//@{

namespace std
{
/***************************************************************************/ /**
   \fn          void swap(bignum::ZInteger&, bignum::ZInteger&)
   \brief       Swap integer values

   This is a template specialization for std::swap. It merely calls the swap
   member of ZInteger.
 *******************************************************************************/

template<>
inline void swap< jl::bignum::ZInteger >(
	jl::bignum::ZInteger& x,
	jl::bignum::ZInteger& y
)
{
	x.swap(y);
}

} // end namespace std

//@}

#if ( ( __cplusplus > 199711L ) || defined( __GXX_EXPERIMENTAL_CXX0X__ ) )
namespace cpp0x = ::std;
namespace std
{
#else
namespace cpp0x
{
#endif

// Forward declaration of template from <random>
template< class T >
class uniform_int_distribution;

template<>
class uniform_int_distribution< jl::bignum::ZInteger >
{
public:
	using result_type = jl::bignum::ZInteger;
	typedef std::pair< jl::bignum::ZInteger, jl::bignum::ZInteger > param_type;

	uniform_int_distribution(
		const jl::bignum::ZInteger& a_,
		const jl::bignum::ZInteger& b_
	)
		: p(a_, b_)
	{
	}

	explicit uniform_int_distribution(param_type q)
		: p(std::move(q) )
	{
	}

	result_type a() const
	{
		return p.first;
	}

	result_type b() const
	{
		return p.second;
	}

	/// \todo Efficiency
	/// \todo Fix biased output
	template< class URNG >
	result_type operator()(URNG& g)
	{
		result_type k = 1;
		result_type x = 0;
		result_type n = p.second - p.first;

		typename URNG::result_type mindigit = g.min();
		typename URNG::result_type maxdigit = g.max() - mindigit;

		/// \note base == maxdigit + 1
		while ( k <= n ) // digit by digit until in range
		{
			k += k * maxdigit; // k *= base
			x += x * maxdigit; // x *= base
			x += g() - mindigit;
		}

		x %= n;
		x += p.first;
		return x;
	}

	void reset()
	{
	}

	param_type param() const
	{
		return p;
	}

	void param(const param_type& q)
	{
		p = q;
	}

	result_type min() const
	{
		return p.first;
	}

	result_type max() const
	{
		return p.second;
	}
private:
	param_type p;
};
} // end namespace cpp0x/std
#endif // ifndef ZINTEGER_HPP
