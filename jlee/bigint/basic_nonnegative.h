#ifndef MULTIWORD_H
#define MULTIWORD_H

// Copyright Jonathan Lee 2015.
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <algorithm>
#include <cstdint>
#include <limits>
#include <stdexcept>
#include <string>

#include "primitives.h"
#include "scratchpad.h"

const unsigned KaratsubaLimit = 4; ///< Min. size before Karatsuba used

// convert an ascii character to the value it represents
inline unsigned actou(char c)
{
	unsigned x = -( 1U );

	if ( 48 <= c && c <= 57 ) // ASCII '0' to '9'
	{
		x = static_cast< unsigned >( c - 48 );
	}
	else if ( 97 <= c && c <= 122 ) // ASCII 'a' to 'z'
	{
		x = static_cast< unsigned >( ( c - 97 ) + 10 );
	}
	else if ( 65 <= c && c <= 90 ) // ASCII 'A' to 'Z'
	{
		x = static_cast< unsigned >( ( c - 65 ) + 10 );
	}

	return x;
}

template< typename T, typename U >
constexpr bool is_safely_convertible_v = std::is_convertible_v< T, U >&& ( sizeof( T ) <= sizeof( U ) );

/** A class for unsigned, bit int arithmetic
 *
 * This class manages the storage for big numbers and provides basic arithmetic. It is used to back
 * basic_integer.
 *
 * @param W Underlying type used for arithmetic. Typically std::uintmax_t.
 *
 * @invariant nnum <= mnum
 * @invariant pnum != 0 iff mnum != 0
 * @invariant nnum != 0 implies pnum[nnum-1] != 0
 *
 * @todo Small string optimization. Switch on pnum == 0
 * @todo mnum should be moved to pnum[-1]. Encapsulate.
 */
template< typename W >
class basic_nonnegative
{
public:
	/** Type used for storage and base arithmetic operations
	 *
	 * Would usually be the largest unsigned integral type, but could be a
	 * custom type (ex., a multiword array with the necessary operations
	 * defined)
	 */
	using word_type = W;

	/// Type for sizes
	using size_type = std::size_t;

	/** @name Constructors
	 */
	/// {
	/// Construct a zero value
	basic_nonnegative()
		: pnum(nullptr), nnum(0), mnum(0)
	{
	}

	/// Copy constructor
	basic_nonnegative(const basic_nonnegative& x)
		: basic_nonnegative()
	{
		if ( x.nnum != 0 )
		{
			reserve(x.nnum);
			std::copy(x.pnum, x.pnum + x.nnum, pnum);
			nnum = x.nnum;
		}
	}

	/// Move constructor
	basic_nonnegative(basic_nonnegative&& x)
		: pnum(x.pnum), nnum(x.nnum), mnum(x.mnum)
	{
		x.pnum = nullptr;
		x.nnum = 0;
		x.mnum = 0;
	}

	/** Value constructor
	 *
	 * @param x A value for initialization
	 *
	 * Sets the value of this to x
	 */
	explicit basic_nonnegative(word_type x)
		: basic_nonnegative()
	{
		if ( x )
		{
			reserve(1);
			nnum    = 1;
			pnum[0] = x;
		}
	}

	/** Marshalling constructor
	 *
	 * @param p Pointer to an array
	 * @param n Size of the array
	 *
	 * Initialize the bignum with data from an array. Data is stored in
	 * "little endian" order. That is, the least significant words are at
	 * lower array indices
	 */
	/* The value of n in the code appears to be the number of bits. This doesn't match the description above
	   basic_nonnegative(const word_type* p, size_type n)
	   {
	    if ( p != nullptr && n > 0 )
	    {
	        word_type m = ~( ~word_type(0) << ( n % WIDTH ) );
	        size_type k = n / WIDTH;

	        if ( m )
	        {
	 ++k;
	        }

	        reserve(k);

	        std::copy(p, p + k, pnum);

	        if ( m )
	        {
	            pnum[k - 1] &= m; // mask extraneous bits
	        }

	        nnum = k;
	        reduce();
	    }
	    else
	    {
	        pnum = nullptr;
	        nnum = 0;
	        mnum = 0;
	    }
	   }
	 */
	///@}

	/** Destructor
	 *
	 * Releases resources
	 */
	~basic_nonnegative()
	{
		delete[] pnum;
	}

	/** @name Assignment
	 */
	/// {
	/// Copy assignment
	basic_nonnegative& operator=(const basic_nonnegative& x)
	{
		if ( x.nnum <= mnum )
		{
			// Enough room to copy straight to our buffer
			std::copy(x.pnum, x.pnum + x.nnum, pnum);
			nnum = x.nnum;
		}
		else
		{
			// Need to allocate. May as well use copy and swap
			basic_nonnegative t(x);
			swap(t);
		}

		return *this;
	}

	/// Move constructor
	basic_nonnegative& operator=(basic_nonnegative&& x)
	{
		delete[] pnum;

		pnum = x.pnum;
		nnum = x.nnum;
		mnum = x.mnum;

		x.pnum = nullptr;
		x.nnum = 0;
		x.mnum = 0;

		return *this;
	}

	template< typename T >
	std::enable_if_t< is_safely_convertible_v< T, W >, basic_nonnegative& > operator=(T x)
	{
		if constexpr ( std::is_signed_v< T >)
		{
			if ( x < 0 )
			{
				throw std::domain_error("Negative values cannot be assigned to basic_nonnegative");
			}
		}

		assign(x);

		return *this;
	}

	/** Swap the contents of two multiwords
	 *
	 * @param other A basic_nonnegative to swap with
	 */
	void swap(basic_nonnegative& other)
	{
		std::swap(pnum, other.pnum);
		std::swap(nnum, other.nnum);
		std::swap(mnum, other.mnum);
	}

	///@}

	/** @name Comparison
	 */
	///@{
	/** Compare this to another basic_nonnegative
	 *
	 * @param other Another basic_nonnegative
	 *
	 * @retval 1 This is greater than other
	 * @retval 0 This is equal to other
	 * @retval -1 This less than other
	 */
	int compare(const basic_nonnegative& v) const
	{
		return compare_inner(pnum, nnum, v.pnum, v.nnum);
	}

	/** Compare this to a word
	 *
	 * @param value Value to compare
	 *
	 * @retval 1 This is greater than value
	 * @retval 0 This is equal to value
	 * @retval -1 This less than value
	 */
	int compare(word_type x) const
	{
		int c = 1;

		if ( nnum <= 1 )
		{
			if ( nnum == 1 )
			{
				if ( pnum[0] <= x )
				{
					c = ( pnum[0] == x ? 0 : -1 );
				}
			}
			else
			{
				c = ( x ? -1 : 0 );
			}
		}

		return c;
	}

	/** Check if the stored value is equal to zero
	 */
	bool is_zero() const
	{
		return nnum == 0;
	}

	///@}

	/** Number of bits to represent the current value
	 *
	 * Returns smallest N such that 2^N > this.
	 */
	std::uintmax_t bit_size() const
	{
		return nnum > 0 ? ( nnum * WIDTH - nlz() ) : 0;
	}

	/** Get the least significant word of the basic_nonnegative
	 *
	 * Useful for casting
	 */
	word_type loword() const
	{
		return nnum != 0 ? pnum[0] : 0;
	}

	/** @name Arithmetic
	 */
	///@{
	/** Increase the magnitude by 1
	 */
	/// @todo add(word_type); inc() -> add(1)
	basic_nonnegative& operator++()
	{
		size_type i = 0;

		while ( i < nnum && ( ++pnum[i] == word_type(0) ) )
		{
			++i;
		}

		if ( i == nnum ) // Was there carry after the last digit
		{
			reserve(nnum + 1);
			pnum[nnum++] = word_type(1);
		}

		return *this;
	}

	basic_nonnegative operator++(int)
	{
		basic_nonnegative t(*this);

		++( *this );

		return t;
	}

	/** Decrement the magnitude by 1 (if non-zero)
	 *
	 * @returns true if this > 0 and decrementing was successful
	 */
	/// @todo sub(word_type); dec() -> sub(1)
	basic_nonnegative& operator--()
	{
		if ( nnum == 0 )
		{
			throw std::domain_error("Decrement not defined for 0");
		}

		size_type i = 0;

		while ( i < nnum && !( pnum[i]-- ) )
		{
			++i;
		}

		if ( !pnum[nnum - 1] )
		{
			--nnum;
		}

		return *this;
	}

	basic_nonnegative operator--(int)
	{
		basic_nonnegative t(*this);

		--( *this );

		return t;
	}

	/** Add the value of other to this
	 *
	 * @param other Another basic_nonnegative
	 */
	basic_nonnegative& operator+=(const basic_nonnegative& x)
	{
		size_type m = std::max(x.nnum, nnum) + 1; // Reserve room for the sum

		if ( m > mnum )
		{
			reserve(m);
		}

		nnum = addto(pnum, nnum, x.pnum, x.nnum);
		return *this;
	}

	basic_nonnegative operator+(const basic_nonnegative& x) const
	{
		basic_nonnegative t(*this);

		t += x;

		return t;
	}

	/** Calculate the (absolute) difference betwen this and other
	 *
	 * @param other Another basic_nonnegative
	 * @returns -1 if this was smaller, 0 if equal, 1 if this was larger
	 *
	 * Mathematically, calculates |*this - x|.
	 */
	int diff(const basic_nonnegative& x)
	{
		const int c = compare(x);

		if ( c != 0 )
		{
			if ( c == 1 )
			{
				nnum = sub(pnum, nnum, x.pnum, x.nnum);
			}
			else
			{
				rsub(x);
			}
		}
		else
		{
			nnum = 0;
		}

		return c;
	}

	basic_nonnegative& operator*=(const basic_nonnegative& x)
	{
		basic_nonnegative t = *this * x;

		swap(t);

		return *this;
	}

	basic_nonnegative operator*(const basic_nonnegative& x) const
	{
		basic_nonnegative w;

		if ( nnum > 0 && x.nnum > 0 )
		{
			w.reserve(nnum + x.nnum);
			w.nnum = mul(pnum, nnum, x.pnum, x.nnum, w.pnum);
		}

		return w;
	}

	basic_nonnegative& operator/=(const basic_nonnegative& x)
	{
		div_inner(x, true);
		return *this;
	}

	basic_nonnegative operator/(const basic_nonnegative& x) const
	{
		basic_nonnegative t(*this);

		t /= x;
		return t;
	}

	basic_nonnegative& operator%=(const basic_nonnegative& x)
	{
		div_inner(x, false);
		return *this;
	}

	basic_nonnegative operator%(const basic_nonnegative& x) const
	{
		basic_nonnegative t(*this);

		t %= x;

		return t;
	}

	basic_nonnegative operator%(word_type value) const
	{
		return basic_nonnegative(modulo_word(value) );
	}

	basic_nonnegative& operator%=(word_type value)
	{
		assign(modulo_word(value) );
		return *this;
	}

	///@}

	/** @name Bit shifts
	 */
	///@{
	/** Logical shift right
	 *
	 * @param n The number of bits to shift
	 * @returns false if result is zero. true otherwise.
	 */
	basic_nonnegative& operator>>=(std::uintmax_t count)
	{
		const size_type a = count / WIDTH;
		const unsigned  b = count % WIDTH;

		if ( a < nnum )
		{
			if ( b != 0 ) // shift and move
			{
				word_type y = pnum[a];

				for (size_type i = 0; i < nnum - a - 1; ++i)
				{
					word_type x = y >> b;
					y       = pnum[i + a + 1];
					pnum[i] = x | ( y << ( WIDTH - b ) );
				}

				if ( y >>= b )
				{
					// high word has a bit
					pnum[nnum - a - 1] = y;
					nnum               = nnum - a;
					return *this;
				}
				else
				{
					// high word is zero, but next word must have a bit set
					nnum = nnum - a - 1;
					return *this;
				}
			}
			else // plain move
			{
				for (size_type i = 0; i < nnum - a; ++i)
				{
					pnum[i] = pnum[i + a];
				}

				nnum = nnum - a;
				return *this;
			}
		}
		else
		{
			// trivially zero
			nnum = 0;
			return *this;
		}
	}

	basic_nonnegative operator<<(std::uintmax_t count) const
	{
		basic_nonnegative t(*this);

		t <<= count;
		return t;
	}

	/** Logical shift left
	 *
	 * @param n The number of bits to shift
	 */
	basic_nonnegative& operator<<=(unsigned long count)
	{
		const size_type     a = count / WIDTH;
		const unsigned long b = count % WIDTH;

		if ( a >= ( -( 1UL ) ) - nnum )
		{
			throw std::overflow_error("Overflow occurred in operator<<=(unsigned long)");
		}

		reserve(nnum + ( 1 + a ) );

		if ( b != 0 )
		{
			word_type y(0);

			for (size_type i = nnum; i > 0; --i)
			{
				word_type x = y << b;
				y               = pnum[i - 1];
				( pnum + a )[i] = x | ( y >> ( WIDTH - b ) );
			}

			( pnum + a )[0] = y << b;

			if ( !pnum[nnum + a] )
			{
				nnum = nnum + a;
			}
			else
			{
				nnum = nnum + a + 1;
			}
		}
		else
		{
			// straight move
			for (size_type i = nnum; i > 0; --i)
			{
				( pnum + a )[i - 1] = pnum[i - 1];
			}

			nnum += a;
		}

		// fill in the bottom words
		for (size_type i = 0; i < a; ++i)
		{
			pnum[i] = word_type(0);
		}

		return *this;
	}

	basic_nonnegative operator>>(std::uintmax_t count) const
	{
		basic_nonnegative t(*this);

		t >>= count;
		return t;
	}

	///@}

	/** @name Bitwise operators
	 *
	 * These functions perform the named bitwise operation, and assign back
	 * to this. Where the operands have a different size (in bits), the smaller
	 * is effectively extended with zero bits.
	 *
	 * Note that for all of this operators, the result of 0 & 0 = 0 | 0 = 0 ^ 0 = 0
	 * Therefore, any bits "higher" than both operands would also be zero.
	 *
	 * Overloads are provided for a single word.
	 */
	///@{
	/** Logical conjunction
	 *
	 * @param other Value
	 */
	basic_nonnegative& operator&=(const basic_nonnegative& x)
	{
		const size_type m = std::min(x.nnum, nnum);

		size_type n = 0;

		for (size_type i = 0; i < m; ++i)
		{
			const word_type t = pnum[i] & x.pnum[i];

			if ( t )
			{
				n = i + 1;
			}

			pnum[i] = t;
		}

		nnum = n;
		return *this;
	}

	basic_nonnegative& operator&=(word_type value)
	{
		if ( nnum > 0 )
		{
			if ( pnum[0] &= value )
			{
				nnum = 1;
			}
			else
			{
				nnum = 0;
			}
		}

		return *this;
	}

	basic_nonnegative operator&(const basic_nonnegative& x) const
	{
		basic_nonnegative t(*this);

		t &= x;

		return t;
	}

	/** Logical disjunction
	 */
	/// @todo If an allocation is triggered, put result in temporary and swap in
	basic_nonnegative& operator|=(const basic_nonnegative& x)
	{
		if ( x.nnum != 0 )
		{
			const size_type m = std::min(nnum, x.nnum);

			if ( m != x.nnum )
			{
				reserve(x.nnum);
			}

			size_type i = 0;

			for (; i < m; ++i)
			{
				word_type t = pnum[i] | x.pnum[i];
				pnum[i] = t;
			}

			for (; i < x.nnum; ++i)
			{
				pnum[i] = x.pnum[i];
			}

			nnum = std::max(nnum, x.nnum);
		}

		return *this;
	}

	basic_nonnegative& operator|=(word_type a)
	{
		if ( nnum != 0 )
		{
			pnum[0] |= a;
		}
		else
		{
			assign(a);
		}

		return *this;
	}

	basic_nonnegative operator|(const basic_nonnegative& x) const
	{
		basic_nonnegative t(*this);

		t |= x;

		return t;
	}

	/** Exclusive Or
	 */
	basic_nonnegative& operator^=(const basic_nonnegative& x)
	{
		if ( x.nnum != 0 )
		{
			size_type m = std::min(nnum, x.nnum);

			if ( m != x.nnum )
			{
				reserve(x.nnum);
			}

			size_type i = 0;

			for (; i < m; ++i)
			{
				word_type t = pnum[i] ^ x.pnum[i];
				pnum[i] = t;
			}

			for (; i < x.nnum; ++i)
			{
				pnum[i] = x.pnum[i];
			}

			nnum = std::max(nnum, x.nnum);
			reduce();
		}

		return *this;
	}

	basic_nonnegative& operator^=(word_type a)
	{
		if ( nnum != 0 )
		{
			pnum[0] ^= a;
			reduce();
		}
		else
		{
			assign(a);
		}

		return *this;
	}

	basic_nonnegative operator^(const basic_nonnegative& x) const
	{
		basic_nonnegative t(*this);

		t ^= x;

		return t;
	}

	///@}

	/** @name Conversion
	 */
	///@{
	/** Positional notation for the number
	 *
	 * @param base The base to use for representing the number (2-36)
	 */
	std::string to_string(word_type base_) const
	{
		if ( base_ < 2 || base_ >= 36 )
		{
			throw std::invalid_argument("Invalid base");
		}

		static const char q[] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

		if ( nnum == 0 )
		{
			return "0";
		}

		// Save divisions by pulling out several digits at once
		word_type base   = base_;
		size_type digits = 1;

		/// @todo Could precalculate this for 2..36
		while ( std::numeric_limits< word_type >::max() / base >= base_ )
		{
			++digits;
			base *= base_;
		}

		std::string s;

		s.reserve(digits * nnum + 1);

		scratchpad< word_type > scratch(nnum);
		word_type*              x = scratch.get();

		// Print each digit-group
		{
			word_type hi = 0;

			for (size_type i = nnum; i > 0; --i)
			{
				const division_t< word_type > r = arch_div(hi, pnum[i - 1], base);
				x[i - 1] = r.quotient;
				hi       = r.remainder;
			}

			for (size_type i = 0; i < digits; ++i)
			{
				s  += q[hi % 10];
				hi /= 10;
			}
		}

		for (size_type j = 0; j < nnum; ++j)
		{
			word_type hi = x[nnum - 1 - j];

			for (int i = nnum - 1 - j; i > 0; --i)
			{
				const division_t< word_type > r = arch_div(hi, x[i - 1], base);
				x[i - 1] = r.quotient;
				hi       = r.remainder;
			}

			for (size_type i = 0; i < digits; ++i)
			{
				s  += q[hi % 10];
				hi /= 10;
			}
		}

		s.resize(s.find_last_not_of('0') + 1);

		// Fix the order of th digits
		std::reverse(s.begin(), s.end() );

		return s;
	}

	/** Initialize this using a string
	 *
	 * @param s Pointer to string
	 * @param n Number of characters in string
	 * @param base Base that s is written in (2-36)
	 *
	 * Characters should be 0-9, A-Z (case insensitive)
	 *
	 * @todo Accept byte-to-unsigned conversion function for larger bases
	 * @todo Accept unary
	 */
	void from_string(
		const char* s,
		size_type   n,
		unsigned    base
	)
	{
		if ( base <= 1 || base > 36 )
		{
			throw std::invalid_argument("Base not supported");
		}

		// empty string
		if ( !s || n == 0 )
		{
			throw std::invalid_argument("Malformed string");
		}

		// Allocate enough space (at most, 6 bits per digit)
		/// @todo log2(base**n), or figure out how many digits in a word_type
		size_type j = ( 6 * n + ( WIDTH - 1 ) ) / WIDTH;

		reserve(j);
		std::fill_n(pnum, j, word_type(0) );

		// Repeatedly multiply by base and add the next digit
		/// @todo Read several digits at once
		for (size_type i = 0; i < n; ++i)
		{
			word_type x(actou(s[i]) );

			if ( x >= word_type(base) )
			{
				throw std::runtime_error("Malformed string");
			}

			/// @todo Use mul(word_type)
			word_type c(0); // carry

			// array = array * base + {next digit};
			for (size_type k = 0; k < j; ++k)
			{
				product_t< word_type > r = arch_mul< word_type >(pnum[k], word_type(base) );
				pnum[k] = r.lo + x + c;
				c       = word_type(c ? ( x >= ( -( word_type(1) ) ) - r.lo ) : ( x > ( -( word_type(1) ) ) - r.lo ) );
				x       = r.hi;
			}
		}

		nnum = j;
		reduce();
	}

	///@}
private:
	/** Value assignment
	 *
	 * @param value Value to assign to this
	 */
	void assign(word_type value)
	{
		if ( value != 0 )
		{
			if ( !pnum )
			{
				reserve(1);
			}

			pnum[0] = value;
			nnum    = 1;
		}
		else
		{
			nnum = 0;
		}
	}

	word_type modulo_word(word_type value) const
	{
		if ( !value )
		{
			throw std::domain_error("Division by zero");
		}

		word_type hi = 0;

		for (size_type i = nnum; i > 0; --i)
		{
			hi = arch_div(hi, pnum[i - 1], value).quotient;
		}

		return hi;
	}

	/** Ensure that pnum points to at least n words of storage
	 *
	 * If mnum >= n, nothing is allocated or copied
	 */
	void reserve(size_type n)
	{
		if ( mnum < n )
		{
			const size_type m = std::max( ( ( n >> 3 ) + 2U ) << 3, n);

			word_type* t = new word_type[m];

			std::copy(pnum, pnum + nnum, t);
			std::swap(pnum, t);
			mnum = m;
			delete[] t;
		}
	}

	/** Ensure the top word is non-zero
	 *
	 * Adjusts nnum so that either nnum == 0, or pnum[nnum - 1] != 0
	 */
	void reduce()
	{
		size_type i = nnum;

		while ( i > 0 && !pnum[i - 1] )
		{
			--i;
		}

		nnum = i;
	}

	static int compare_inner(
		const word_type* u,
		size_type        nu,
		const word_type* v,
		size_type        nv
	)
	{
		for (size_type i = nu; i > nv; --i)
		{
			if ( u[i - 1] )
			{
				return 1;
			}
		}

		for (size_type i = nv; i > nu; --i)
		{
			if ( v[i - 1] )
			{
				return -1;
			}
		}

		for (size_type i = std::min(nu, nv); i > 0; --i)
		{
			const word_type a = u[i - 1], b = v[i - 1];

			if ( a != b )
			{
				return a < b ? -1 : 1;
			}
		}

		return 0;
	}

	// when you know this is greater than other
	static size_type addto(
		word_type*       u,
		size_type        nu,
		const word_type* v,
		size_type        nv
	)
	{
		word_type c(0); // carry
		size_type j = 0;

		for (size_type jmin = std::min(nu, nv); j < jmin; ++j)
		{
			word_type t = u[j], y = v[j];
			u[j] += y + c;
			c     = word_type(c ? ( t >= ~y ) : ( t > ~y ) );
		}

		if ( c != word_type(0) )
		{
			if ( j != nv ) // move and propagate carry
			{
				do
				{
					u[j] = v[j] + c;
					c   &= word_type(u[j] == word_type(0) );
				}
				while ( ++j < nv );

				nu = nv;
			}
			else // just propagate carry
			{
				while ( j < nu && ( ++u[j] == word_type(0) ) )
				{
					++j;
				}

				c = word_type(j == nu);
			}

			if ( c ) // Carry is still set - extend by one word
			{
				u[nu++] = word_type(1);
			}
		}
		else if ( j < nv ) // move and propagate carry
		{
			do
			{
				u[j] = v[j];
				++j;
			}
			while ( j < nv );

			nu = nv;
		}

		return nu;
	}

	static size_type add(
		const word_type* u,
		size_type        nu,
		const word_type* v,
		size_type        nv,
		word_type*       w
	)
	{
		word_type       c    = 0;
		const size_type jmin = std::min(nu, nv);

		for (size_type j = 0; j < jmin; ++j)
		{
			word_type t = u[j], y = v[j];
			w[j] = t + y + c;
			c    = word_type(c != 0 ? ( t >= ~y ) : ( t > ~y ) );
		}

		if ( c )
		{
			size_type nw = jmin;

			size_type j = jmin;

			if ( j < nv )
			{
				do
				{
					w[j] = v[j] + c;
					c   &= word_type(w[j] == word_type(0) );
				}
				while ( ++j < nv );

				nw = nv;
			}
			else if ( j != nu )
			{
				do
				{
					w[j] = u[j] + c;
					c   &= word_type(w[j] == word_type(0) );
				}
				while ( ++j < nu );

				nw = nu;
			}

			if ( c ) // Carry is still set - extend by one word
			{
				w[nw++] = word_type(1);
			}

			while ( w[nw - 1] == 0 )
			{
				--nw;
			}

			return nw;
		}
		else
		{
			size_type nw = jmin;
			size_type j  = jmin;

			if ( j < nv ) // straight move
			{
				do
				{
					w[j] = v[j];
					++j;
				}
				while ( j < nv );

				nw = nv;
			}
			else if ( j < nu )
			{
				do
				{
					w[j] = u[j];
					++j;
				}
				while ( j < nu );

				nw = nu;
			}

			while ( nw > 0 && w[nw - 1] == 0 )
			{
				--nw;
			}

			return nw;
		}
	}

	static size_type sub(
		word_type*       u,
		size_type        nu,
		const word_type* v,
		size_type        nv
	)
	{
		word_type c(0);
		size_type j = 0;

		for (; j < nv; ++j)
		{
			word_type t = u[j] - v[j] - c;
			c    = word_type(c ? ( v[j] >= u[j] ) : ( v[j] > u[j] ) );
			u[j] = t;
		}

		if ( c ) // propagate the borrow
		{
			while ( j < nu && --u[j] == -( word_type(1) ) )
			{
				++j;
			}
		}

		if ( u[nu - 1] == 0 )
		{
			--nu;
		}

		return nu;
	}

	/// @todo Toom Cook 3 and Toom Cook 4 2
	/// @todo Get scratchpad here and use it all the way down
	static size_type mul(
		const word_type* u,
		size_type        nu,
		const word_type* v,
		size_type        nv,
		word_type*       w
	)
	{
		const size_type n = std::min(nu, nv);

		if ( n >= KaratsubaLimit )
		{
			return mul_karatsuba(u, nu, v, nv, w);
		}
		else if ( n > 0 )
		{
			return mul_schoolbook(u, nu, v, nv, w);
		}

		return 0;
	}

	static void mul_word(
		const word_type* u,
		size_type        nu,
		word_type        v,
		word_type*       w
	)
	{
		word_type hi(0);

		for (size_type i = 0; i < nu; ++i)
		{
			const product_t< word_type > t = arch_mul(u[i], v);

			const word_type x = hi + t.lo;

			w[i] = x;

			hi = ( x < hi ? t.hi + 1 : t.hi );
		}

		w[nu] = hi;
	}

	/***************************************************************************/ /**
	   @brief       Calculates w = u * v.
	   @exception   overflow_error Occurs if result cannot be stored

	   The most basic multiplication algorithm for multiprecision integers. Implements
	   Knuth's Algorithm M (with a slight modification). As this is an O(N^2)
	   algorithm, it is quickly outperformed by Karatsuba multiplication. The advantage
	   of this algorithm, however, is that it does not require any additional memory.

	   @todo Faster school book maybe by doing every other multiplication so the
	      additions don't overlap so often. i.e., do all the odd and even
	      multiplies and sum the two results
	   @todo Favour multiplication by a single unsigned long. i.e., if nv == 1
	 *******************************************************************************/
	static size_type mul_schoolbook(
		const word_type* u,
		size_type        nu,
		const word_type* v,
		size_type        nv,
		word_type*       w
	)
	{
		if ( nu == 0 || nv == 0 )
		{
			return 0;
		}

		if ( nv > nu )
		{
			// prefer nu > nv in the code below
			std::swap(u, v);
			std::swap(nu, nv);
		}

		mul_word(u, nu, v[0], w);

		for (size_type j = 1; j < nv; ++j)
		{
			word_type       k(0);
			const word_type vj = v[j];

			for (size_type i = 0; i < nu; ++i)
			{
				product_t< word_type > t = arch_mul(u[i], vj);
				t.lo += w[i + j];

				if ( t.lo < w[i + j] )
				{
					++t.hi;
				}

				t.lo += k;

				if ( t.lo < k )
				{
					++t.hi;
				}

				w[i + j] = t.lo;
				k        = t.hi;
			}

			w[j + nu] = k;
		}

		size_type i = nu + nv;

		while ( i > 0 && w[i - 1] == 0 )
		{
			--i;
		}

		return i;
	}

	static size_type mul_karatsuba(
		const word_type* u,
		size_type        nu,
		const word_type* v,
		size_type        nv,
		word_type*       w
	)
	{
		if ( nu > 0 && nv > 0 )
		{
			size_type n = std::max(nu, nv);
			n = n - ( n / 2 );

			size_type nu0 = std::min(n, nu);

			while ( nu0 > 0 && u[nu0 - 1] == 0 )
			{
				--nu0;
			}

			size_type nv0 = std::min(n, nv);

			while ( nv0 > 0 && v[nv0 - 1] == 0 )
			{
				--nv0;
			}

			const size_type nz0 = mul(u, nu0, v, nv0, w);

			for (size_type i = nz0; i < n + n; ++i)
			{
				w[i] = 0;
			}

			size_type nz2 = 0;

			if ( nu > n && nv > n )
			{
				nz2 = mul(u + n, nu - n, v + n, nv - n, w + ( n + n ) );
			}

			scratchpad< word_type > t(4 * n + 4);
			word_type*              a  = t.get();
			word_type*              b  = a + ( n + 1 );
			word_type*              c  = a + ( n + 1 + n + 1 );
			const size_type         na = add(u, nu0, u + n, ( nu > n ? nu - n : 0 ), a);
			const size_type         nb = add(v, nv0, v + n, ( nv > n ? nv - n : 0 ), b);

			size_type nz1 = mul(a, na, b, nb, c);
			nz1 = sub(c, nz1, w + n + n, nz2);
			nz1 = sub(c, nz1, w, nz0);

			size_type nres = addto(w + n, n + nz2, c, nz1) + n;

			return nres;
		}

		return 0;
	}

	// when you know this is less than other
	void rsub(const basic_nonnegative& x)
	{
		/// @todo If we trigger an allocation, sub and copy
		reserve(x.nnum);
		word_type c(0);

		for (size_type j = 0; j < nnum; ++j)
		{
			word_type t = x.pnum[j] - pnum[j] - c;
			c       = word_type(c ? ( pnum[j] >= x.pnum[j] ) : ( pnum[j] > x.pnum[j] ) );
			pnum[j] = t;
		}

		for (size_type j = nnum; j < x.nnum; ++j)
		{
			pnum[j] = x.pnum[j] - c;
			c       = word_type(c > x.pnum[j] ? 1 : 0);
		}

		nnum = ( pnum[x.nnum - 1] == 0 ? x.nnum - 1 : x.nnum );
	}

	word_type div_eq(word_type x)
	{
		word_type hi = 0;

		for (size_type i = nnum; i > 0; --i)
		{
			const division_t< word_type > res = arch_div(hi, pnum[i - 1], x);
			pnum[i - 1] = res.quotient;
			hi          = res.remainder;
		}

		return hi;
	}

	void div_inner(
		const basic_nonnegative& x,
		bool                     q
	)
	{
		if ( x.nnum == 0 )
		{
			throw std::domain_error("Division by zero");
		}

		// Normalization factor
		const unsigned count = x.nlz();

		word_type d = x.pnum[x.nnum - 1] << count;

		if ( count != 0 && x.nnum >= 2 )
		{
			d |= x.pnum[x.nnum - 2] >> ( WIDTH - count );
		}

		basic_nonnegative quo;
		basic_nonnegative rem(*this);

		if ( q && rem.nnum >= x.nnum )
		{
			quo.reserve(rem.nnum - ( x.nnum - 1 ) );
			quo.nnum = rem.nnum - ( x.nnum - 1 );
		}

		for (size_type i = rem.nnum; i >= x.nnum; --i)
		{
			word_type a = ( i < rem.nnum ? rem.pnum[i] : word_type(0) );
			word_type b = rem.pnum[i - 1];

			if ( count != 0 )
			{
				a   = ( a << count ) | ( b >> ( WIDTH - count ) );
				b <<= count;

				if ( i >= 2 )
				{
					b |= ( rem.pnum[i - 2] >> ( WIDTH - count ) );
				}
			}

			// Estimate quotient digit
			word_type c = arch_div< word_type >(a, b, d).quotient;

			const size_type offset = i - x.nnum;

			if ( c > word_type(2) ) // Subtract (estimate * divisor)
			{
				c -= word_type(2);

				for (size_type j = offset; j < i; ++j)
				{
					product_t< word_type > p = arch_mul(c, x.pnum[j - offset]);

					if ( rem.pnum[j] < p.lo )
					{
						++p.hi;
					}

					rem.pnum[j] -= p.lo;

					if ( p.hi != word_type(0) )
					{
						if ( p.hi > rem.pnum[j + 1] )
						{
							rem.pnum[j + 1] -= p.hi;
							size_type k = j + 2;

							while ( rem.pnum[k]-- == word_type(0) )
							{
								++k;
							}
						}
						else
						{
							rem.pnum[j + 1] -= p.hi;
						}
					}
				}
			}
			else
			{
				c = word_type(0);
			}

			while ( compare_inner(rem.pnum + offset, rem.nnum - offset, x.pnum, x.nnum) >= 0 )
			{
				rem.nnum = sub(rem.pnum + offset, rem.nnum - offset, x.pnum, x.nnum) + offset;
				++c;
			}

			if ( q )
			{
				quo.pnum[i - x.nnum] = c;
			}
		}

		if ( !q )
		{
			if ( x.nnum < rem.nnum )
			{
				rem.nnum = x.nnum;
			}
		}
		else
		{
			rem.swap(quo); // return quotient instead of remainder
		}

		rem.reduce();
		swap(rem);
	}

	/** Number of leading zeros on the high word
	 *
	 * Used by the division algorithm. Returns zero if this is zero
	 */
	unsigned nlz() const
	{
		unsigned count = 0;

		if ( nnum != 0 )
		{
			const word_type top_bit = word_type(1ul) << ( WIDTH - 1 );

			// while high bit is not set, increment count
			for (word_type d = pnum[nnum - 1]; ( d & top_bit ) == 0; d <<= 1)
			{
				++count;
			}
		}

		return count;
	}

	/*
	   basic_nonnegative basic_nonnegative::mulToomCook(
	    const basic_nonnegative& v
	    ) const
	   {
	    ZInteger w;

	    if (mag.nnum > 0 && v.mag.nnum > 0) {
	        size_type n = std::max(mag.nnum, v.mag.nnum);
	        n = (n / 3) + (n % 3 != 0 ? 1 : 0);

	        ZInteger u0 = slice(0, n);
	        ZInteger u1 = slice(n, n);
	        ZInteger u2 = slice(n + n);
	        ZInteger v0 = v.slice(0, n);
	        ZInteger v1 = v.slice(n, n);
	        ZInteger v2 = v.slice(n + n);

	        ZInteger w3 = u0 + u2;
	        ZInteger w0 = w3 - u1;
	        w3 += u1;
	        ZInteger w2 = v0 + v2;
	        ZInteger w4 = w2 - v1;
	        w2 += v1;
	        ZInteger w1 = w3 * w2;
	        w2 = w0 * w4;
	        w0 += u2;
	        w0 <<= 1UL;
	        w0 -= u0;
	        w4 += v2;
	        w4 <<= 1UL;
	        w4 -= v0;
	        w3 = w0 * w4;
	        w0 = u0 * v0;
	        w4 = u2 * v2;
	        w3 -= w1;
	        w3 /= 3UL; /// @todo Exact division by 3 can be done faster
	        w1 -= w2;
	        w1 >>= 1UL;
	        w2 -= w0;
	        w3 = w2 - w3;
	        w3 >>= 1UL;
	        w3 += w4 + w4;
	        w2 += w1;
	        w2 -= w4;
	        w1 -= w3;

	        // W = w4 * b^4 + w3 * b^3 + w2 * b^2 + w1 * b + w0
	        w  = w4 << (n * (4 * LONG_BIT));
	        w += w3 << (n * (3 * LONG_BIT));
	        w += w2 << (n * (2 * LONG_BIT));
	        w += w1 << (n * LONG_BIT);
	        w += w0;
	    }

	    return w;
	   }
	 */

	/*
	   ZInteger ZInteger::mulToomCook42(
	    const ZInteger& v
	    ) const
	   {
	    ZInteger w;

	    if (mag.nnum > 0 && v.mag.nnum > 0) {
	        size_type n = std::max(mag.nnum, v.mag.nnum);
	        n = (n / 4) + (n % 4 != 0 ? 1 : 0);

	        ZInteger u0, u1, u2, u3, v0, v1;

	        if (mag.nnum > v.mag.nnum) {
	            slice(0, n).swap(u0);
	            slice(n, n).swap(u1);
	            slice(n + n, n).swap(u2);
	            slice(n + n + n).swap(u3);
	            v.slice(0, n).swap(v0);
	            v.slice(n).swap(v1);
	        } else {
	            slice(0, n).swap(v0);
	            slice(n).swap(v1);
	            v.slice(0, n).swap(u0);
	            v.slice(n, n).swap(u1);
	            v.slice(n + n, n).swap(u2);
	            v.slice(n + n + n).swap(u3);
	        }

	        ZInteger w3 = u0 + u2;
	        ZInteger w2 = v0 + v1;
	        ZInteger w1 = u1 + u3;
	        ZInteger w4 = v0 - v1;
	        ZInteger w0 = w3 - w1;

	        w3 += w1;
	        w1 = w3 * w2;
	        w2 = w0 * w4;
	        w0 = u0 - ((u1 - ((u2 - (u3 << 1)) << 1)) << 1);
	        w4 -= v1;
	        w3 = w0 * w4;
	        w0 = u0 * v0;
	        w4 = u3 * v1;
	        w3 -= w1;
	        w3 /= 3UL;
	        w1 -= w2;
	        w1 >>= 1UL;
	        w2 -= w0;
	        w3 = ((w2 - w3) >> 1) + (w4 << 1);
	        w2 += w1 - w4;
	        w1 -= w3;

	        w  = w4 << (n * (4 * LONG_BIT));
	        w += w3 << (n * (3 * LONG_BIT));
	        w += w2 << (n * (2 * LONG_BIT));
	        w += w1 << (n * LONG_BIT);
	        w += w0;
	    }

	    return w;
	   }
	 */

	static const int WIDTH = std::numeric_limits< word_type >::digits;
	word_type*       pnum{ nullptr }; ///< Pointer to the integer representation
	size_type        nnum{ 0 };       ///< Current size of the pnum array
	size_type        mnum{ 0 };       ///< Maximum size of the pnum array
};

template< typename T, typename U >
bool operator==(
	const basic_nonnegative< T >& l,
	U&&                           r
)
{
	return l.compare(std::forward< U >(r) ) == 0;
}

template< typename T, typename U >
bool operator!=(
	const basic_nonnegative< T >& l,
	U&&                           r
)
{
	return l.compare(std::forward< U >(r) ) != 0;
}

template< typename T, typename U >
bool operator<(
	const basic_nonnegative< T >& l,
	U&&                           r
)
{
	return l.compare(std::forward< U >(r) ) < 0;
}

template< typename T, typename U >
bool operator<=(
	const basic_nonnegative< T >& l,
	U&&                           r
)
{
	return l.compare(std::forward< U >(r) ) <= 0;
}

template< typename T, typename U >
bool operator>(
	const basic_nonnegative< T >& l,
	U&&                           r
)
{
	return l.compare(std::forward< U >(r) ) > 0;
}

template< typename T, typename U >
bool operator>=(
	const basic_nonnegative< T >& l,
	U&&                           r
)
{
	return l.compare(std::forward< U >(r) ) >= 0;
}

template< typename T >
struct is_basic_nonnegative
	: std::false_type
{
};

template< typename T >
struct is_basic_nonnegative< basic_nonnegative< T > >
	: std::true_type
{
};

template< typename T >
constexpr bool is_basic_nonnegative_v = is_basic_nonnegative< T >::value_type;

template< typename T, typename U >
std::enable_if_t< !is_basic_nonnegative_v< U >, bool > operator==(
	U&&                           l,
	const basic_nonnegative< T >& r
)
{
	return r == l;
}

template< typename T, typename U >
std::enable_if_t< !is_basic_nonnegative_v< U >, bool > operator!=(
	U&&                           l,
	const basic_nonnegative< T >& r
)
{
	return r != l;
}

template< typename T, typename U >
std::enable_if_t< !is_basic_nonnegative_v< U >, bool > operator<(
	U&&                           l,
	const basic_nonnegative< T >& r
)
{
	return r > l;
}

template< typename T, typename U >
std::enable_if_t< !is_basic_nonnegative_v< U >, bool > operator<=(
	U&&                           l,
	const basic_nonnegative< T >& r
)
{
	return r >= l;
}

template< typename T, typename U >
std::enable_if_t< !is_basic_nonnegative_v< U >, bool > operator>(
	U&&                           l,
	const basic_nonnegative< T >& r
)
{
	return r < l;
}

template< typename T, typename U >
std::enable_if_t< !is_basic_nonnegative_v< U >, bool > operator>=(
	U&&                           l,
	const basic_nonnegative< T >& r
)
{
	return r <= l;
}

#endif // MULTIWORD_H
