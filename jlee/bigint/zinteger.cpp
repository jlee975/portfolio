#include "zinteger.h" // Class declaration

#include <algorithm>
#include <climits> // for LONG_BIT (hopefully)
#include <cstdlib>
#include <iostream>
#include <limits>
#include <stdexcept> // For various throws
#include <utility>   // For pair

namespace jl
{
namespace bignum
{

/***************************************************************************/ /**
   @name Arithmetic operators
 *******************************************************************************/
//@{

ZInteger& ZInteger::operator*=(const ZInteger& x)
{
	const bool is_signed = !( mag.is_zero() ) && !( x.mag.is_zero() ) && ( x.negative != negative );

	ZInteger y = mul(x);

	swap(y);

	negative = is_signed;

	return *this;
}

ZInteger& ZInteger::operator*=(unsigned long a)
{
	return operator*=(ZInteger(a) );
}

ZInteger& ZInteger::operator*=(unsigned a)
{
	return operator*=(static_cast< unsigned long >( a ) );
}

ZInteger& ZInteger::operator*=(long a)
{
	if ( a < 0 )
	{
		if ( !mag.is_zero() )
		{
			operator*=(-static_cast< unsigned long >( a ) );
			negative = !negative;
		}
	}
	else
	{
		operator*=(static_cast< unsigned long >( a ) );
	}

	return *this;
}

ZInteger& ZInteger::operator*=(int a)
{
	if ( a < 0 )
	{
		if ( !mag.is_zero() )
		{
			operator*=(-static_cast< unsigned long >( a ) );
			negative = !negative;
		}
	}
	else
	{
		operator*=(static_cast< unsigned long >( a ) );
	}

	return *this;
}

ZInteger& ZInteger::operator/=(const ZInteger& x)
{
	mag /= x.mag;

	if ( !mag.is_zero() )
	{
		negative = ( negative != x.negative );
	}

	return *this;
}

ZInteger& ZInteger::operator/=(unsigned long a)
{
	return operator/=(ZInteger(a) );
}

ZInteger& ZInteger::operator/=(unsigned a)
{
	return operator/=(static_cast< unsigned long >( a ) );
}

ZInteger& ZInteger::operator/=(long a)
{
	return operator/=(ZInteger(a) );
}

ZInteger& ZInteger::operator/=(int a)
{
	return operator/=(ZInteger(a) );
}

ZInteger& ZInteger::operator%=(const ZInteger& x)
{
	mag %= x.mag;

	if ( negative && mag.is_zero() )
	{
		negative = false;
	}

	return *this;
}

/// @bug What is modulo if negative
ZInteger& ZInteger::operator%=(unsigned long a)
{
	mag %= a;

	if ( mag.is_zero() )
	{
		negative = false;
	}

	return *this;
}

ZInteger& ZInteger::operator%=(unsigned a)
{
	return operator%=(static_cast< unsigned long >( a ) );
}

ZInteger& ZInteger::operator%=(long a)
{
	return operator%=(ZInteger(a) );
}

ZInteger& ZInteger::operator%=(int a)
{
	return operator%=(ZInteger(a) );
}

/***************************************************************************/ /**
   @brief       Function for calculating quotient or remainder with divisor x.
   @param       x  Divisor.
   @param       q  If true, calculates quotient. If false, calculates remainder.
   @exception   domain_error Thrown if x is zero.

   Calculates quotient or remainder of *this divided by x. If q and r are the
   resulting quotient and remainder, respectively, then *this = q * x + r. The
   remainder has the same sign as the dividend.

   @todo        mul/sub loop has a lot of "if"s
   @todo        "if (mag.pnum[j] &lt; p[0]) ++ux;" <=> ux += (mag.pnum[j] &lt; p[0]);
   @todo        The shld inside the main loop is probably expensive
   @todo        Knuth has some optimizations about this. Plus the whole thing can
             be replaced with a "calculate reciprocal and multiply" algorithm
             which is supposedly faster.

 *******************************************************************************/
/*
   ZInteger ZInteger::div(const ZInteger& x, bool q) const
   {
    multiword y = mag.div(x.mag, q);

    ZInteger z;
    if (q && !y.is_zero())
    {
        z.negative = (negative != x.negative);
    }
    // 7/3  =  2 * 3 + 1
    // 7/-3 = -2 * -3 + 1
    // -7/3 = -2 * 3 - 1
    // -7/-3 = 2 * -3 - 1
    z.mag.swap(y);
    return z;

   }
 */
//@}

/***************************************************************************/ /**
   @name Bitshift operators
 *******************************************************************************/
//@{
/***************************************************************************/ /**
   @brief       Shift the ZInteger right by count bits.
   @return      Reference to this.

   This function shifts the binary representation toward the least significant bit
   by count bits. Effectively dividing *this by 2^count, with rounding toward zero.

   Since ZInteger uses a sign/magnitude representation, a negative ZInteger shifted
   right only shifts the magnitude. There is no ambiguity because of sign extension
   as there is with built-in types.

   If the result of the shift is zero, the sign is corrected so the ZInteger is
   non-negative.

   @todo        Document the optimization for multiples of LONG_BIT. But provide a
             public function to get the value of LONG_BIT.
 *******************************************************************************/
ZInteger& ZInteger::operator>>=(unsigned long count)
{
	mag >>= count;

	if ( mag.is_zero() )
	{
		negative = false;
	}

	return *this;
}

ZInteger& ZInteger::operator>>=(unsigned a)
{
	return operator>>=(static_cast< unsigned long >( a ) );
}

ZInteger& ZInteger::operator>>=(long a)
{
	return a >= 0 ? operator>>=(static_cast< unsigned long >( a ) ) : operator<<=(-static_cast< unsigned long >( a ) );
}

ZInteger& ZInteger::operator>>=(int a)
{
	return a >= 0 ? operator>>=(static_cast< unsigned long >( a ) ) : operator<<=(-static_cast< unsigned long >( a ) );
}

/***************************************************************************/ /**
   @brief       Shift *this left by count bits.
   @exception   overflow_error Thrown if the left shift cannot be represented.
   @return      Reference to this.

   This operator shifts the ZInteger left by count bits, filling the right bits
   with 0. Equivalently, this function multiplyies *this by 2^count.

   @sa          ZInteger::operator>>=
 *******************************************************************************/
ZInteger& ZInteger::operator<<=(unsigned long count)
{
	mag <<= count;
	return *this;
}

ZInteger& ZInteger::operator<<=(unsigned a)
{
	return operator<<=(static_cast< unsigned long >( a ) );
}

ZInteger& ZInteger::operator<<=(long a)
{
	return a >= 0 ? operator<<=(static_cast< unsigned long >( a ) ) : operator>>=(-static_cast< unsigned long >( a ) );
}

ZInteger& ZInteger::operator<<=(int a)
{
	return a >= 0 ? operator<<=(static_cast< unsigned long >( a ) ) : operator>>=(-static_cast< unsigned long >( a ) );
}

//@}

/***************************************************************************/ /**
   @name Bitwise operators

   @note All of the bitwise operators return non-negative values. Otherwise there
      is the chance of getting negative 0, which is disallowed.
 *******************************************************************************/
//@{
/***************************************************************************/ /**
   @brief       Bitwise AND.
   @return      Reference to this.

   This function masks the bits of *this against x.
 *******************************************************************************/
ZInteger& ZInteger::operator&=(const ZInteger& x)
{
	mag     &= x.mag;
	negative = false;
	return *this;
}

// Mixed type overloads
ZInteger& ZInteger::operator&=(unsigned long a)
{
	mag     &= a;
	negative = false;
	return *this;
}

ZInteger& ZInteger::operator&=(unsigned a)
{
	mag     &= a;
	negative = false;
	return *this;
}

/***************************************************************************/ /**
   @brief       Bitwise OR.
   @return      Reference to this.

   Mixes in the bits of x, as given by the OR function. If x is "longer" (i.e.,
   has higher bits set than *this), then *this is extended.
 *******************************************************************************/
ZInteger& ZInteger::operator|=(const ZInteger& x)
{
	mag     |= x.mag;
	negative = false;
	return *this;
}

// Mixed type overloads
ZInteger& ZInteger::operator|=(unsigned long a)
{
	mag     |= a;
	negative = false;
	return *this;
}

ZInteger& ZInteger::operator|=(unsigned a)
{
	mag     |= a;
	negative = false;
	return *this;
}

/***************************************************************************/ /**
   @brief       Bitwise XOR.
   @return      Reference to this.

   This function mixes bits into *this from x, based on the XOR rule.
 *******************************************************************************/
ZInteger& ZInteger::operator^=(const ZInteger& x)
{
	mag     ^= x.mag;
	negative = false;
	return *this;
}

// Mixed type overloads
ZInteger& ZInteger::operator^=(unsigned long a)
{
	mag     ^= a;
	negative = false;
	return *this;
}

ZInteger& ZInteger::operator^=(unsigned a)
{
	mag     ^= a;
	negative = false;
	return *this;
}

//@}

} // end namespace bignum
}

namespace
{
/*=========================================================================*//**
   @name Helper functions
*//*==========================================================================*/
//@{

/***************************************************************************/ /**
   @brief       Convert character to integer

   Takes an ASCII character and converts it to the number it represents. Ex., '8'
   is converted to 8, and 'd' is converted to 13.

   Valid inputs are '0' through '9', 'a' through 'z', and 'A' through 'Z' (in
   ASCII).
 *******************************************************************************/
//@}

/***************************************************************************/ /**
   @name Machine level arithmetic functions
 *******************************************************************************/
//@{

//@}

} // end anonymous namespace

namespace jl
{
namespace bignum
{
/*=========================================================================*//**
   @name Extensions to STL
*//*==========================================================================*/
//@{
/***************************************************************************/ /**
   @brief       Print ZInteger to ostream.
   @return      The ostream.

   This function prints the value of the ZInteger in decimal.
 *******************************************************************************/
std::ostream& operator<<(
	std::ostream&               os,
	const jl::bignum::ZInteger& x
)
{
	unsigned long base = 10;

	switch ( os.flags() & os.basefield )
	{
	case std::ios_base::hex:
		base = 16;
		break;
	case std::ios_base::oct:
		base = 8;
		break;
	default:
		break;
	}

	return os << x.to_string(base);
}

}
}
