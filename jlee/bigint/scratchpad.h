#ifndef SCRATCHPAD_H
#define SCRATCHPAD_H

// Copyright Jonathan Lee 2015.
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

/** @todo Multiple buffers so we can call get many times
 */
template< typename T, std::size_t N = 128 >
class scratchpad
{
public:
	scratchpad()
		: p(0)
	{
	}

	explicit scratchpad(std::size_t n)
		: p(nullptr)
	{
		reserve(n);
	}

	~scratchpad()
	{
		delete[] p;
	}

	T* reserve(std::size_t n)
	{
		if ( n <= N )
		{
			return buf;
		}

		if ( p )
		{
			if ( n <= m )
			{
				return p;
			}

			delete[] p;
		}

		p = new T[n];
		return p;
	}

	T* get()
	{
		return p ? p : buf;
	}
private:
	scratchpad(const scratchpad&)            = delete;
	scratchpad& operator=(const scratchpad&) = delete;

	T           buf[N];
	T*          p;
	std::size_t m;
};

#endif // SCRATCHPAD_H
