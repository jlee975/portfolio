#ifndef EXPECTED_H
#define EXPECTED_H

#include <variant>

template< typename E >
class unexpected
{
public:
	unexpected() = delete;

	explicit unexpected(const E& v)
		: value_(v)
	{
	}

	explicit unexpected(E&& v)
		: value_(std::move(v) )
	{
	}

	const E& value() const
	{
		return value_;
	}
private:
	E value_;
};

// A simple expected class until we get std::expected. Wraps std::variant
template< typename T, typename E >
class expected
{
public:
	expected(const unexpected< E >& x)
		: var(x.value() )
	{
	}

	expected(unexpected< E >&& x)
		: var(x.value() )
	{
	}

	expected(const T& x)
		: var(x)
	{
	}

	expected(T&& x)
		: var(std::move(x) )
	{
	}

	explicit operator bool() const
	{
		return var.index() == 0;
	}

	const T& operator*() const
	{
		return std::get< 0 >(var);
	}
private:
	std::variant< T, E > var;
};

#endif // EXPECTED_H
