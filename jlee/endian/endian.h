#ifndef ENDIAN_H
#define ENDIAN_H

#include <climits>
#include <cstddef>
#include <cstdint>
#include <utility>

namespace detail
{
template< unsigned N >
struct ufast;

template<>
struct ufast< 64 >
{
	using type = std::uint_fast64_t;
};

template<>
struct ufast< 32 >
{
	using type = std::uint_fast32_t;
};

template<>
struct ufast< 16 >
{
	using type = std::uint_fast16_t;
};

template<>
struct ufast< 8 >
{
	using type = std::uint_fast8_t;
};

template< std::size_t, std::size_t... >
struct last;

template< std::size_t I0 >
struct last< I0 >
{
	static constexpr std::size_t value = I0;
};

template< std::size_t I0, std::size_t I1, std::size_t... I >
struct last< I0, I1, I... >
{
	static constexpr std::size_t value = last< I1, I... >::value;
};

/* Helper function which compiles as a single read of type U
 *
 * The version of littleEndian with only one argument should be optimized as a single, multi-byte read from
 * memory. GCC and clang (at least) compile this helper function as such. Writing the function in a loop, or
 * using addition instead of or-ing the bits defeats this optimization.
 */
template< std::size_t... I >
auto lehelper(const unsigned char* p, std::index_sequence< I... >)
{
	using U = typename detail::ufast< CHAR_BIT * sizeof...(I) >::type;

	return ((static_cast< U >(p[I]) << (CHAR_BIT * I)) | ...);
}

template< std::size_t... I >
auto behelper(const unsigned char* p, std::index_sequence< I... >)
{
	using U = typename detail::ufast< CHAR_BIT * sizeof...(I) >::type;

	return ((static_cast< U >(p[I]) << (CHAR_BIT * (last< I... >::value - I))) | ...);
}

}

/*
   template< typename U >
   U leread(const std::byte* p)
   {
    return detail::lehelper< U >(p, std::make_index_sequence< sizeof( U ) >{});
   }

   template< typename U >
   U leread(const char* p)
   {
    return leread< U >(reinterpret_cast< const std::byte* >( p ) );
   }

   template< typename U >
   U leread(const unsigned char* p)
   {
    return leread< U >(reinterpret_cast< const std::byte* >( p ) );
   }

   template< typename U >
   U beread(const std::byte* p)
   {
    return detail::behelper< U >(p, std::make_index_sequence< sizeof( U ) >{});
   }

   template< typename U >
   U beread(const char* p)
   {
    return beread< U >(reinterpret_cast< const std::byte* >( p ) );
   }

   template< typename U >
   U beread(const unsigned char* p)
   {
    return beread< U >(reinterpret_cast< const std::byte* >( p ) );
   }
 */

template< unsigned N, typename B >
auto leread(const B* p)
{
	static_assert(N % CHAR_BIT == 0);

	return detail::lehelper(reinterpret_cast< const unsigned char* >(p),
				std::make_index_sequence< N / CHAR_BIT >{});
}

template< unsigned N, typename B >
auto beread(const B* p)
{
	static_assert(N % CHAR_BIT == 0);

	return detail::behelper(reinterpret_cast< const unsigned char* >(p),
				std::make_index_sequence< N / CHAR_BIT >{});
}

template< unsigned N, typename B >
void lewrite(B* p, typename detail::ufast< N >::type x)
{
	// As of gcc 8.2, need to write to unsigned char pointer for optimization. Clang doesn't work regardless.
	auto* q = reinterpret_cast< unsigned char* >(p);

	for ( std::size_t i = 0; i < N / 8; ++i )
	{
		q[i] = static_cast< unsigned char >(x >> (8 * i));
	}
}

template< unsigned N, typename B >
auto leread(const B* p_, std::size_t n)
{
	const auto p = reinterpret_cast< const std::byte* >(p_);

	static_assert(N % CHAR_BIT == 0);

	using return_type = typename detail::ufast< N >::type;

	if ( n >= N / CHAR_BIT )
	{
		return leread< N >(p);
	}

	return_type x = 0;

	for ( std::size_t i = 0; i < n; ++i )
	{
		x |= std::to_integer< return_type >(p[i]) << (CHAR_BIT * i);
	}

	return x;
}

template< unsigned N, typename B >
auto beread(const B* p_, std::size_t n)
{
	static_assert(N % CHAR_BIT == 0);

	const auto p = reinterpret_cast< const std::byte* >(p_);

	using return_type = typename detail::ufast< N >::type;

	if ( n >= N / CHAR_BIT )
	{
		return beread< N >(p);
	}

	return_type x = 0;

	for ( std::size_t i = 0; i < n; ++i )
	{
		x |= std::to_integer< return_type >(p[i]) << (CHAR_BIT * (N / CHAR_BIT - 1 - i));
	}

	return x;
}

template< unsigned N, typename B >
void bewrite(B* p, typename detail::ufast< N >::type x)
{
	// As of gcc 8.2, need to write to unsigned char pointer for optimization. Clang doesn't work regardless.
	auto* q = reinterpret_cast< unsigned char* >(p);

	for ( std::size_t i = 0; i < N / 8; ++i )
	{
		q[i] = static_cast< unsigned char >(x >> (8 * (3 - i)));
	}
}

#endif // ENDIAN_H
