// ISAAC constants -------------------------------------------------------------
#ifndef ISAAC_HPP
#define ISAAC_HPP

/*
 #if (defined(LONGBITS) && (LONGBITS == 64))
    // 64-bit
 #define ISAAC_B 3
 #define ISAAC_S 21
 #define ISAAC_T 5
 #define ISAAC_U 12
 #define ISAAC_V 33
 #define ISAAC_M ~0UL
 #define ISAACMIX(i) \
    { \
    unsigned long * p = state + (i & 0xFF);\
    p[0]-=p[4]; p[5]^=p[7]>>9;  p[7]+=p[0]; \
    p[1]-=p[5]; p[6]^=p[0]<<9;  p[0]+=p[1]; \
    p[2]-=p[6]; p[7]^=p[1]>>23; p[1]+=p[2]; \
    p[3]-=p[7]; p[0]^=p[2]<<15; p[2]+=p[3]; \
    p[4]-=p[0]; p[1]^=p[3]>>14; p[3]+=p[4]; \
    p[5]-=p[1]; p[2]^=p[4]<<20; p[4]+=p[5]; \
    p[6]-=p[2]; p[3]^=p[5]>>17; p[5]+=p[6]; \
    p[7]-=p[3]; p[4]^=p[6]<<14; p[6]+=p[7]; \
    }
 #define ISAACKK { \
        0x647c4677a2884b7cUL,0xb9f8b322c73ac862UL, \
        0x8c0ea5053d4712a0UL,0xb29b2e824a595524UL,\
        0x82f053db8355e0ceUL,0x48fe4a0fa5a09315UL,\
        0xae985bf2cbfc89edUL,0x98f5704f6c44c0abUL}
 #else
 #endif
   // */

// 32-bit
#define ISAAC_B 2
#define ISAAC_S 13
#define ISAAC_T 6
#define ISAAC_U 2
#define ISAAC_V 16
#define ISAAC_M 0

class ISAAC
{
	unsigned long state[256];
	unsigned long aa;
	unsigned long bb;
	unsigned long cc;
	unsigned long t;

	static const unsigned long kk[8];

	void mix(int);
public:
	typedef unsigned long result_type;

	explicit ISAAC(const unsigned long*);
	result_type operator()();
};

#endif // ifndef ISAAC_HPP
