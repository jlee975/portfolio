class CMWC
{
	unsigned long Q[4096];
	unsigned long c;
	unsigned long i;
public:
	CMWC()
		: Q(), c(362436), i(4095)
	{
	}

	unsigned long operator()()
	{
		unsigned long long t;

		unsigned long long a = 1878LL;
		unsigned long long b = 4294967295LL;

		unsigned long r = b - 1;

		i = ( i + 1 ) % 4096;
		t = a * Q[i] + c;
		c = ( t >> 32 );
		t = ( t & b ) + c;

		if ( t > r )
		{
			++c;
			t = t - b;
		}

		Q[i] = r - t;
		return Q[i];
	}

};
