/*=========================================================================*//**

   ISAAC is a cryptographic pseudo-random number generator designed by Bob Jenkins.

   To generate a random unsigned long, simply call ISAAC(0).

   This PRNG is self-initializing. It can be called immediately, and the first time
   it is called it will take care of any initialization that needs to be done.

   A seed may be provided instead of calling the function with a zero value. Note
   that it accepts an array of unsigned longs, not a single unsigned long as a
   parameter. If a seed is provided, then no random number is generated. The
   function must be called again.

   Finally, note that there is both a 32-bit and 64-bit set of parameters for this
   function. The appropriate set will be chosen by the header file for the host
   architecture.

   \todo

   Variable sized seed array.

   The seed expansion here and in MT can probably be the same. Choose the "better"
   of the two.

   References

   "ISAAC, a fast cryptographic random number generator" by Bob Jenkins.
   http://www.burtleburtle.net/bob/rand/isaacafa.html

*//*==========================================================================*/

#include "isaac.h"

/***************************************************************************/ /**
 *******************************************************************************/
ISAAC::ISAAC(const unsigned long* seed)
	: state(), aa(0), bb(0), cc(0), t(0)
{
	if ( seed )
	{
		for (int i = 0; i < 256; i++)
		{
			state[i] = seed[i];
		}
	}

	for (int j = 0; j < 8; j++)
	{
		state[j] += kk[j];
	}

	mix(0);

	for (int i = 1; i < 512; i += 8)
	{
		for (int j = 0; j < 8; j++)
		{
			state[( i + j ) & 0xFF] += state[( i + j + 248 ) & 0xFF];
		}

		mix(i);
	}
}

/***************************************************************************/ /**
 *******************************************************************************/
const unsigned long ISAAC::kk[8] = { 0x1367df5aUL, 0x95d90059UL, 0xc3163e4bUL, 0x0f421ad8UL,
	                                 0xd92a4a78UL, 0xa51a3c49UL, 0xc4efea1bUL, 0x30609119UL };

/***************************************************************************/ /**
 *******************************************************************************/
ISAAC::result_type ISAAC::operator()()
{
	if ( t == 0 )
	{
		bb += ++cc;
	}

	switch ( t % 4 )
	{
	case 0:
		aa ^= ( aa << ISAAC_S ) ^ ISAAC_M;
		break;
	case 1:
		aa ^= ( aa >> ISAAC_T );
		break;
	case 2:
		aa ^= ( aa << ISAAC_U );
		break;
	case 3:
		aa ^= ( aa >> ISAAC_V );
		break;
	}

	aa += state[( t + 128 ) & 0xFF];

	unsigned long x = state[t];

	state[t] = state[( x >> ISAAC_B ) & 0xFF] + aa + bb;
	bb       = state[( state[t] >> ( ISAAC_B + 8 ) ) & 0xFF] + x;
	t        = ( t + 1 ) & 0xFF;
	return bb;
}

/***************************************************************************/ /**
 *******************************************************************************/
void ISAAC::mix(int i)
{
	unsigned long* p = state + i;

	p[0] ^= p[1] << 11;
	p[3] += p[0];
	p[1] += p[2];
	p[1] ^= p[2] >> 2;
	p[4] += p[1];
	p[2] += p[3];
	p[2] ^= p[3] << 8;
	p[5] += p[2];
	p[3] += p[4];
	p[3] ^= p[4] >> 16;
	p[6] += p[3];
	p[4] += p[5];
	p[4] ^= p[5] << 10;
	p[7] += p[4];
	p[5] += p[6];
	p[5] ^= p[6] >> 4;
	p[0] += p[5];
	p[6] += p[7];
	p[6] ^= p[7] << 8;
	p[1] += p[6];
	p[7] += p[0];
	p[7] ^= p[0] >> 9;
	p[2] += p[7];
	p[0] += p[1];
}
