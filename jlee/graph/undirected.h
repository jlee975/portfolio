/*=========================================================================*//**
   \class       undirected_graph
   \brief       Container class for type T
   \author      Jonathan Lee

   This template class provides a container for its template type, where relations
   can be expressed through undirected edges. Each element of the graph container
   has an id, returned by the function insert(). The element may be accessed via
   the label through the square brackets operator. For example,

   \code
    undirected_graph<Employee> employees;
    Employee bob;

    // initialize bob, somehow

    undirected_graph<Employee>::id_type bob_id = employees.insert(bob);
    employees[bob_id].vacation(...)
   \endcode

   Additionally, vertices can be connected by an edge using the connect() function:

   \code
    employees.connect(alice_id, bob_id);
    employees.connect(alice_id, carl_id);
   \endcode

   This would add two edges to the graph \em from Alice \em to Bob and Carl. This
   edge could symbolize heirarchy, substitutes, or something else. It's really for
   the user to decide.

   -- Plane Graphs ----------------------------------------------------------------

   Graphs that are embedded in the plane should be implemented through a template
   type that defines member functions T::x() and T::y().

   \todo The whole plane graph thing is a bit ad-hoc. Two possible solutions:
      subclass from undirected_graph to make a PlaneGraph class which handles (x, y)
      associations with vertex labels. OR, provide a PlaneNode class that the
      template parameter T can derive from.
   \todo Should replace for_each_xxx with breadth-first and depth-first
      searches. In general, those will be more useful.
   \todo Use weighted edges? Possibly make the Edge type a template parameter
   \todo Directed graphs
   \todo Arithmetic operators
*//*==========================================================================*/
#ifndef UNDIRECTED_H
#define UNDIRECTED_H

#include <map>       // For undirected_graph<T>::v
#include <set>       // For undirected_graph<T>::edges
#include <stdexcept> // For std::out_of_range
#include <utility>   // For std::pair

template< typename T >
class undirected_graph
{
public:
	typedef T node_type;

	struct label_type
	{
		label_type& operator++()
		{
			++value;
			return *this;
		}

		friend bool operator==(
			const label_type& l,
			const label_type& r
		)
		{
			return l.value == r.value;
		}

		friend bool operator<(
			const label_type& l,
			const label_type& r
		)
		{
			return l.value < r.value;
		}

		unsigned long value;
	};

	struct edge_type
	{
		friend bool operator<(
			const edge_type& l,
			const edge_type& r
		)
		{
			return l.first < r.first || ( l.first == r.first && l.second < r.second );
		}

		label_type first;
		label_type second;
	};

	typedef typename std::set< edge_type >::iterator edge_iterator;
	typedef typename std::set< edge_type >::const_iterator const_edge_iterator;
	typedef typename std::map< label_type, T >::size_type size_type;
	typedef typename std::map< label_type, T >::iterator vertex_iterator;
	typedef typename std::map< label_type, T >::const_iterator const_vertex_iterator;

	/***************************************************************************/ /**
	   \fn          undirected_graph()
	   \brief       Default constructor creates a null graph.
	 *******************************************************************************/
	undirected_graph() = default;

	/***************************************************************************/ /**
	   \fn          undirected_graph(const undirected_graph&)
	   \brief       Copy constructor makes a straight copy of the source graph
	 *******************************************************************************/
	undirected_graph(const undirected_graph& g)
		: index(g.index), v(g.v), edges(g.edges)
	{
	}

	undirected_graph(undirected_graph&& g)
		: index(std::move(g.index) ), v(std::move(g.v) ), edges(std::move(g.edges) )
	{
		g.index = label_type();
		g.v     = {};
		g.edges = {};
	}

	/***************************************************************************/ /**
	   \fn          ~undirected_graph()
	   \brief       Destructor
	 *******************************************************************************/
	~undirected_graph() = default;

	/***************************************************************************/ /**
	   \fn          undirected_graph& operator=(const undirected_graph&)
	   \brief       Copy assignment
	 *******************************************************************************/
	undirected_graph& operator=(const undirected_graph& g)
	{
		if ( &g != this )
		{
			undirected_graph(g).swap(*this);
		}

		return *this;
	}

	undirected_graph& operator=(undirected_graph&& g)
	{
		index = std::move(g.index);
		v     = std::move(g.v);
		edges = std::move(g.edges);
		g.clear();
	}

	/***************************************************************************/ /**
	   \fn          void swap(undirected_graph&)
	   \brief       Swap graph contents
	 *******************************************************************************/
	void swap(undirected_graph& g)
	{
		std::swap(index, g.index);
		v.swap(g.v);
		edges.swap(g.edges);
	}

	/***************************************************************************/ /**
	   \fn          void clear()
	   \brief       Resets class to null graph
	 *******************************************************************************/
	void clear()
	{
		index = label_type();
		v     = {};
		edges = {};
	}

	/***************************************************************************/ /**
	   \fn          bool isNull()
	   \brief       Tests if graph is the null graph
	   \returns     true if there are no vertices (and no edges), false otherwise
	 *******************************************************************************/
	bool isNull() const
	{
		return v.empty() && edges.empty();
	}

	/***************************************************************************/ /**
	   \fn          bool isEmpty()
	   \brief       Tests if graph is an empty graph, i.e., has no edges
	   \returns     true if there are no edges, false otherwise

	   \note An empty graph and the null graph are not the same thing (though the
	   null graph is trivially a kind of empty graph).
	 *******************************************************************************/
	bool isEmpty() const
	{
		return edges.empty();
	}

	/***************************************************************************/ /**
	   \fn          size_type order()
	   \brief       Get the graph's order
	   \returns     The number of vertices in the graph
	 *******************************************************************************/
	size_type order() const
	{
		return v.size();
	}

	/***************************************************************************/ /**
	   \fn          size_type size()
	   \brief       Get the graph's size
	   \returns     The number of edges in the graph
	 *******************************************************************************/
	size_type size() const
	{
		return edges.size();
	}

	/***************************************************************************/ /**
	   \fn          T& operator[]
	   \brief       Accessor for vertices
	   \returns     Reference to a vertex, with label i
	 *******************************************************************************/
	T& operator[](label_type i)
	{
		vertex_iterator j = v.find(i);

		if ( j == v.end() )
		{
			throw std::out_of_range("Range error from operator[]");
		}

		return j->second;
	}

	const T& operator[](label_type i) const
	{
		const_vertex_iterator j = v.find(i);

		if ( j == v.end() )
		{
			throw std::out_of_range("Range error from operator[]");
		}

		return j->second;
	}

	/***************************************************************************/ /**
	   \fn          label_type insert(const T&)
	   \brief       Copies n into the graph and assigns it a node label
	   \return      Label of the node
	 *******************************************************************************/
	label_type insert(const T& n)
	{
		v[++index] = n;
		return index;
	}

	/***************************************************************************/ /**
	   \fn          void erase(label_type)
	   \brief       Erase a vertex

	   This function erases a vertex and all of its incident edges.

	   \todo Can speed this up by erasing out edges and in edges in pairs
	 *******************************************************************************/
	void erase(label_type i)
	{
		edge_iterator it = edges.begin();

		while ( it != edges.end() )
		{
			if ( it->first == i || it->second == i )
			{
				edges.erase(it++);
			}
			else
			{
				++it;
			}
		}

		v.erase(i);
	}

	/***************************************************************************/ /**
	   \fn          void erase(edge_iterator)
	   \brief       Erase an edge, by iterator

	   \todo Consider renaming
	   \note Invalidates iterator
	 *******************************************************************************/
	void erase(edge_iterator it)
	{
		edges.erase(it);
	}

	/***************************************************************************/ /**
	   \fn          void connect(label_type, label_type)
	   \brief       Adds both directed edges to the graph

	   \todo Add a variation for directed graphs
	 *******************************************************************************/
	void connect(
		label_type i,
		label_type j
	)
	{
		if ( v.find(i) == v.end() || v.find(j) == v.end() )
		{
			throw std::out_of_range("Cannot add edge to a vertex that does not exist.");
		}

		edges.insert(edge_type(i, j) );
		edges.insert(edge_type(j, i) );
	}

	/***************************************************************************/ /**
	   \fn          bool adjacent(label_type i, label_type j)
	   \brief       Tests if two vertices are joined by an edge
	   \returns     true if there is an edge connected nodes i and j
	 *******************************************************************************/
	bool adjacent(
		label_type i,
		label_type j
	) const
	{
		return edges.count(edge_type(i, j) ) > 0 || edges.count(edge_type(j, i) ) > 0;
	}

	/***************************************************************************/ /**
	   \fn          std::set<label_type> neighbourhood(label_type ind)
	   \brief       Gets the vertices which are connected to ind
	   \returns     A list of nodes that are adjacent to ind

	   \bug Technically this should return the induced subgraph including all edges
	      between the adjacent nodes
	 *******************************************************************************/
	std::set< label_type > neighbourhood(label_type ind)
	{
		std::set< label_type > n;

		edge_iterator kt = edges.lower_bound(edge_type(ind, label_type() ) );

		while ( kt != edges.end() && kt->first == ind )
		{
			n.insert(kt->second);
			++kt;
		}

		return n;
	}

	/***************************************************************************/ /**
	   \fn          unsigned long degree(label_type idx)
	   \brief       Counts the number of edges incident with vertex idx
	 *******************************************************************************/
	size_type degree(label_type ind)
	{
		size_type     d  = 0;
		edge_iterator kt = edges.lower_bound(edge_type(ind, label_type() ) );

		while ( kt != edges.end() && kt->first == ind )
		{
			++d;
			++kt;
		}

		return d;
	}

	/***************************************************************************/ /**
	   \name        Vertex iterators

	   For iterating over the vertex set in no particular order.
	 *******************************************************************************/
	//@{
	vertex_iterator vbegin()
	{
		return v.begin();
	}

	vertex_iterator vend()
	{
		return v.end();
	}

	const_vertex_iterator vbegin() const
	{
		return v.begin();
	}

	const_vertex_iterator vend() const
	{
		return v.end();
	}

	//@}

	/***************************************************************************/ /**
	   \fn          void for_each_vertex(...)
	   \brief       Applies a function to every vertex of the graph
	 *******************************************************************************/
	template< typename F >
	void for_each_vertex(F&& f)
	{
		for (vertex_iterator it = v.begin(), jt = v.end(); it != jt; ++it)
		{
			f(it->second);
		}
	}

	/***************************************************************************/ /**
	   \name        Edge iterators

	   For iterating over the edge set in no particular order.
	 *******************************************************************************/
	//@{
	edge_iterator edge_begin() const
	{
		return edges.begin();
	}

	edge_iterator edge_end() const
	{
		return edges.end();
	}

	//@}

	/***************************************************************************/ /**
	   \fn          void for_each_edge(...)
	   \brief       Applies a function to every edge of the graph
	 *******************************************************************************/
	template< typename F >
	void for_each_edge(F&& f)
	{
		for (edge_iterator it = edges.begin(), jt = edges.end(); it != jt; ++it)
		{
			f(v[it->first], v[it->second]);
		}
	}
private:
	label_type                index; ///< Source for unique identifiers
	std::map< label_type, T > v;     ///< The vertices, indexed by an u.long
	std::set< edge_type >     edges; ///< Edges in this graph
};

#endif // ifndef GRAPH_HPP

// Old ideas about serializing the undirected_graph<> class. Documented in case I ever
// want to refer back to it, but largely unimportant.
/* serialize -------------------------------------------------------------------

   The graph is serialized in the unsigned char vector.

   The format is:
   serialize :: = (0xFE node (neighbour-id)*)* 0xFF
   node ::= float float float
   neighbour-id ::= "utf-8 encoded integer"
   float ::= "ieee 754-1985 single precision value, in little endian order"

   The escape codes 0xFE and 0xFF cannot appear in UTF-8 (or the slight variation
   that is in use here). The serialization is thus well-defined.
   ------------------------------------------------------------------------------*/
/*
   undirected_graph& undirected_graph::operator>>(std::vector<unsigned char>& out) {
    out.clear();

    for (unsigned long i = 0; i < n_nodes; ++i) {
        out.push_back(0xFE); // Indicates the beginning of a node

        nodes[i].serialize(out); // add the node details

        // Node is followed by neighbour IDs, in UTF-8 style
        for (unsigned long j = i + 1; j < n_nodes; ++j) {
            if (!adjacent(i,j)) continue;

            bool first = true;
            unsigned int m1 = 1;
            unsigned int m2 = 0xFC;
            for (int k = 30; k >= 0; k -= 6) {
                if (!first || ((j >> k) != 0)) {
                    unsigned char c;
                    c = ((j >> k) & m1) | m2;
                    out.push_back(c);
                    m1 = 0x3F;
                    m2 = 0x80;
                    first = false;
   } else {
                    m1 = m1 + m1 + 1;
                    m2 = (m2 + m2) & 0xFF;
   }
   }
   }
   }

    out.push_back(0xFF); // Indicates end of serialization
    return *this;
   }
 */

/* Adjacency list

   (a, b, c, d, e)   // first is adjacent to each of the others
   (b, e)
   (c, e)

   a: {b, c, d, e}
   b: {e}
   c: {e}



   abcde(0xFF)be(0xFF)ce(0xFF)(0xFF)abcdefghijk
   ^^^^^
   - Construct adjacency matrix
   - Record each row by indicating the node correspoding to the row, and then all neighbours
   - list of nodes and their adjacent nodes, by int or UTF-8 code
   - empty list indicates end of lists and beginning of nodes
   - node info packed into floats (x,y,z)

   all nodes

   (int for number of nodes)
   (list of all nodes and properties

 */
