#ifndef DIRECTED_H
#define DIRECTED_H

#include <algorithm>
#include <unordered_set>
#include <vector>

template< typename V, typename E = void >
class DirectedGraph;

template< typename V >
class DirectedGraph< V, void >
{
	/// @todo A base class could be shared between graphs, reducing
	/// code duplication
	struct Node
	{
		void add_parent(Node* parent)
		{
			if ( parent )
			{
				// Is parent already a parent?
				for (const Node* parenti : parents)
				{
					if ( parenti == parent )
					{
						return;
					}
				}

				parents.push_back(parent);
			}
		}

		void add_child(Node* child)
		{
			if ( child )
			{
				for (const Node* childi : children)
				{
					if ( childi == child )
					{
						return;
					}
				}

				children.push_back(child);
			}
		}

		void remove_parent(const Node* parent)
		{
			for (std::size_t i = 0, n = parents.size(); i < n; ++i)
			{
				if ( parents[i] == parent )
				{
					parents[i] = parents[n - 1];
					parents.pop_back();
					break;
				}
			}
		}

		void remove_child(const Node* child)
		{
			for (auto it = children.begin(), last = children.end(); it != last; ++it)
			{
				if ( *it == child )
				{
					children.erase(it);
					break;
				}
			}
		}

		std::vector< Node* > parents;
		std::vector< Node* > children;
		V value;
	};
public:
	using vertex_type = V;
	using size_type   = typename std::vector< Node* >::size_type;

	class const_vertex_iterator
	{
		friend DirectedGraph< V >;
public:
		const_vertex_iterator()
			: node(nullptr)
		{
		}

		size_type child_count() const
		{
			return node->children.size();
		}

		const_vertex_iterator child(size_type i) const
		{
			return const_vertex_iterator{ node->children.at(i) };
		}

		const V& operator*() const
		{
			return node->value;
		}

		const V* operator->() const
		{
			return &( node->value );
		}

		friend bool operator==(
			const const_vertex_iterator& l,
			const const_vertex_iterator& r
		)
		{
			return l.node == r.node;
		}

		friend bool operator!=(
			const const_vertex_iterator& l,
			const const_vertex_iterator& r
		)
		{
			return l.node != r.node;
		}
private:
		explicit const_vertex_iterator(Node* node_)
			: node(node_)
		{
		}

		Node* node;
	};

	class vertex_iterator
	{
		friend DirectedGraph< V >;
public:
		vertex_iterator()
			: node(nullptr)
		{
		}

		size_type child_count() const
		{
			return node->children.size();
		}

		vertex_iterator child(size_type i) const
		{
			return vertex_iterator{ node->children.at(i) };
		}

		V& operator*()
		{
			return node->value;
		}

		const V& operator*() const
		{
			return node->value;
		}

		operator const_vertex_iterator() const
		{
			return const_vertex_iterator(node);
		}

		V* operator->()
		{
			return &( node->value );
		}

		const V* operator->() const
		{
			return &( node->value );
		}

		void disconnect_child(size_type i) const
		{
			if ( node && i < node->children.size() )
			{
				node->children[i]->remove_parent(node);
				node->children.erase(node->children.begin() + i);
			}
		}

		friend bool operator==(
			const vertex_iterator& l,
			const vertex_iterator& r
		)
		{
			return l.node == r.node;
		}

		friend bool operator!=(
			const vertex_iterator& l,
			const vertex_iterator& r
		)
		{
			return l.node != r.node;
		}
private:
		explicit vertex_iterator(Node* node_)
			: node(node_)
		{
		}

		Node* node;
	};

	DirectedGraph() = default;

	~DirectedGraph()
	{
		clear();
	}

	template< typename...Args >
	vertex_iterator emplace(Args&&... args)
	{
		Node* t = new Node{{}, {}, { std::forward< Args >(args)... }};

		vertices.insert(t);

		return vertex_iterator{ t };
	}

	template< typename...Args >
	vertex_iterator emplace_child(
		vertex_iterator parent,
		Args&&...       args
	)
	{
		auto it = emplace(std::forward< Args >(args)...);

		connect(parent, it);
		return it;
	}

	vertex_iterator end()
	{
		return {};
	}

	const_vertex_iterator end() const
	{
		return {};
	}

	const_vertex_iterator cend() const
	{
		return {};
	}

	void clear()
	{
		for (Node* v : vertices)
		{
			delete v;
		}

		vertices.clear();
	}

	void erase(const const_vertex_iterator& it)
	{
		if ( const auto jt = vertices.find(it.node);jt != vertices.end() )
		{
			// Remove from children's parents list
			for (Node* child : it.node->children)
			{
				child->remove_parent(it.node);
			}

			// Unlist from all parents
			for (Node* parent : it.node->parents)
			{
				parent->remove_child(it.node);
			}

			// Delete node
			delete it.node;

			// Bookkeeping
			vertices.erase(jt);
		}
	}

	void connect(
		vertex_iterator parent,
		vertex_iterator child
	)
	{
		if ( parent.node != nullptr && child.node != nullptr )
		{
			parent.node->add_child(child.node);
			child.node->add_parent(parent.node);
		}
	}

	void disconnect(
		vertex_iterator parent,
		vertex_iterator child
	)
	{
		if ( parent.node != nullptr && child.node != nullptr )
		{
			child.node->remove_parent(parent.node);
			parent.node->remove_child(child.node);
		}
	}
private:
	std::unordered_set< Node* > vertices;
};

#endif // DIRECTED_H
