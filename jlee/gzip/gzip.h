#ifndef GZIP_HPP
#define GZIP_HPP

#include <cstddef>
#include <vector>

#include "archive/iarchive.h"

/**
 * @brief The GZip class
 * @todo Treat as an archive. GZIP can have multiple files simply by concatenating .gz files.
 */
class GZip
	: public IArchive
{
public:
	enum error : derived_error
	{
		bad_flag,    ///< A reserved bit (as of specification version 4.3) was not zero
		signature,   ///< The signature bytes were not found
		compression, ///< The compression method is unknown. Only "Deflate" is defined by the spec
		deflate,     ///< There was a problem with the deflate data
		crc,         ///< Data did not match stored checksum.
		filesize     ///< Stored filesize did not match data extracted.
	};

	// The fourth byte contains flags abut what other values may appear before data
	enum class flag : unsigned char
	{
		none = 0,
		text = 0x01,
		hcrc = 0x02,
		extra = 0x04,
		name = 0x08,
		comment = 0x10,
		reserved = 0xE0
	};

	// The tenth byte indicates the original operating system
	enum class os : unsigned char
	{
		fat = 0,
		amiga = 1,
		vms = 2,
		unix = 3,
		vm = 4,
		atari = 5,
		hpfs = 6,
		max = 7,
		zsystem = 8,
		cpm = 9,
		tops20 = 10,
		ntfs = 11,
		qdos = 12,
		acorn = 13,
		unknown = 255
	};

	enum class compression_method : unsigned char
	{
		deflate = 8
	};

	/** @brief Get the decompressed data
	 *
	 * @note Data is still available if Extract returns crc or filesize. Deflate still succeeded.
	 */
	Result extract(const std::byte*, std::size_t) const override;
	Result archive(const std::byte*, std::size_t) const override;
};

constexpr GZip::flag operator&(
	GZip::flag l,
	GZip::flag r
)
{
	return static_cast< GZip::flag >( static_cast< unsigned char >( l ) & static_cast< unsigned char >( r ) );
}

#endif // #ifndef GZIP_h
