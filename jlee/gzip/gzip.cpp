/* GZIP ========================================================================

   References ---------------------------------------------------------------------

   http://www.ietf.org/rfc/rfc1952.txt
   http://www.gzip.org/

   ==============================================================================*/
#include "gzip.h"

#include <ctime>
#include <stdexcept>

#include "bitstream/lebitstream.h"
#include "crc/crc.h"
#include "deflate/deflate.h"
#include "endian/endian.h"

namespace
{
// Awkward, but portable way to get POSIX epoch in a time_t
std::time_t make_epoch()
{
	std::tm e = {};

	e.tm_sec   = 0;
	e.tm_min   = 0;
	e.tm_hour  = 0;
	e.tm_mday  = 1;
	e.tm_mon   = 0;
	e.tm_year  = 70;
	e.tm_isdst = 0;
	return std::mktime(&e);
}

}

// Gzip archives begin with a two byte signature 0x1f then 0x8b
constexpr std::byte GZIP_ID1{ 0x1f };
constexpr std::byte GZIP_ID2{ 0x8b };

// The third byte of a gzip archive indicates the compression method. Only one
// is defined.

GZip::Result GZip::extract(
	const std::byte* const pData,
	std::size_t            cbData
) const
{
	if ( pData == nullptr )
	{
		throw std::invalid_argument("Arguments cannot be null");
	}

	// Find the gzip header
	std::size_t iSig = 0;

	while ( ( iSig + 9 ) < cbData && !( pData[iSig] == GZIP_ID1 && pData[iSig + 1] == GZIP_ID2 ) )
	{
		++iSig;
	}

	if ( ( iSig + 9 ) >= cbData )
	{
		return { archive_error::derived, signature, {}};
	}

	// Read header members
	const auto cm    = static_cast< compression_method >( pData[iSig + 2] );
	const auto flags = static_cast< flag >( pData[iSig + 3] );

	// Check validity of header
	if ( cm != compression_method::deflate )
	{
		return { archive_error::derived, compression, {}};
	}

	// Read flags
	if ( (flags& flag::reserved) != flag::none )
	{
		return { archive_error::derived, bad_flag, {}};
	}

	/// @todo MTIME, XFL and OS fields

	// Find the compressed data, skipping over the meta data
	std::size_t ideflated = iSig + 10;

	if ( (flags& flag::extra) != flag::none )
	{
		// If extra data is present, it consists of a 2-byte length field and then that many bytes
		if ( ideflated + 1 >= cbData )
		{
			return { archive_error::nodata, {}, {}};
		}

		const auto extralen = leread< 16 >(pData + ideflated);

		if ( ideflated + 2 + extralen > cbData )
		{
			return { archive_error::nodata, {}, {}};
		}

		ideflated += 2 + extralen;
	}

	if ( (flags& flag::name) != flag::none )
	{
		// If a file name is present, it is a zero terminated string
		while ( ideflated < cbData && pData[ideflated] != std::byte(0) )
		{
			++ideflated;
		}

		if ( ideflated >= cbData )
		{
			return { archive_error::nodata, {}, {}};
		}

		// Skip past the last byte
		++ideflated;
	}

	if ( (flags& flag::comment) != flag::none )
	{
		// If a comment is present, it is a zero terminated string
		while ( ideflated < cbData && pData[ideflated] != std::byte(0) )
		{
			++ideflated;
		}

		if ( ideflated >= cbData )
		{
			return { archive_error::nodata, {}, {}};
		}

		++ideflated;
	}

	if ( (flags& flag::hcrc) != flag::none )
	{
		ideflated += 2;
	}

	if ( ideflated > cbData )
	{
		return { archive_error::nodata, {}, {}};
	}

	ILEBitstream bs;

	bs.open(pData + ideflated, cbData - ideflated);

	deflate::Decompressor d;

	const auto res = d.exec(bs);

	if ( res == deflate::Decompressor::error::none )
	{
		auto data = std::move(d).data();

		bs.move_to_next_byte_boundary();

		const auto stored_crc = bs.read(32);
		const auto actual_crc = chest::crc32(data.data(), data.size(), chest::crc::png, chest::crc::flags::png);

		if ( actual_crc != stored_crc )
		{
			return { archive_error::derived, crc, {}};
		}

		const auto stored_filesize = bs.read(32);
		const auto actual_filesize = data.size();

		if ( ( actual_filesize & UINT32_C(0xffffffff) ) != stored_filesize )
		{
			return { archive_error::derived, filesize, {}};
		}

		return { archive_error::none, {}, std::move(data) };
	}

	return { archive_error::derived, deflate, {}};
}

GZip::Result GZip::archive(
	const std::byte* p,
	std::size_t      n
) const
{
	deflate::Compressor c;

	/// @todo A more accurate estimate of overhead. Worst case 5 additionaly bytes per block, but block size
	/// can be configured differently
	std::size_t outsize = n + ( ( n + 16383 ) / 16384 ) * 5;

	/// @todo Don't want to have to pay for the initialization.
	std::vector< std::byte > data(outsize + 18, std::byte{ 0 });

	/// @todo MTIME doesn't reflect the original file time (if the data came from a file)
	data[0] = GZIP_ID1;
	data[1] = GZIP_ID2;
	data[2] = std::byte(compression_method::deflate);
	data[3] = std::byte(flag::none); // flags

	std::uint_least32_t mtime = 0;

	{
		const std::time_t epoch           = make_epoch();
		const std::time_t now             = std::time(nullptr);
		const double      sec_since_epoch = std::difftime(now, epoch);

		// It's not specified if mtime is signed or unsigned. Assuming signed, and if the result
		// is out of range, we store 0, which *is* allowed by the spec
		if ( sec_since_epoch >= 0 && sec_since_epoch <= 2147483647 )
		{
			mtime = static_cast< std::uint_least32_t >( sec_since_epoch );
		}
	}

	lewrite< 32 >(data.data() + 4, mtime); // 4 bytes POSIX timestamp
	data[8] = std::byte{ 0 };              // compression flags
	data[9] = std::byte(os::unknown);      // operating system

	/// @todo Extra fields like filename and such

	if ( c.Compress(p, n, data.data() + 10, &outsize) != deflate::Compressor::error::none )
	{
		return { archive_error::derived, deflate, {}};
	}

	// original data crc
	const auto x = chest::crc32(p, n, chest::crc::png, chest::crc::flags::png);

	lewrite< 32 >(data.data() + ( outsize + 10 ), x);

	// file size
	lewrite< 32 >(data.data() + ( outsize + 14 ), n & UINT32_C(0xffffffff) );
	return { archive_error::none, {}, std::move(data) };
}
