#ifndef DERIVATION_H
#define DERIVATION_H

#include <iostream>
#include <map>

#include "tree/tree.h"
#include "unicode/ustring.h"

/// @todo Need the guarantee that tree is ordered by offset and children do not extend past parents
template< typename NonTerminal >
class derivation
{
public:
	struct parse_match
	{
		NonTerminal rule;
		std::size_t offset;
		std::size_t end;
	};

	using key_type        = typename tree< parse_match >::key_type;
	using size_type       = typename tree< parse_match >::size_type;
	using const_reference = typename tree< parse_match >::const_reference;
	using value_type      = typename tree< parse_match >::value_type;

	static constexpr key_type INVALID = tree< parse_match >::INVALID;

	void clear()
	{
		content.clear();
	}

	key_type parent(key_type k) const
	{
		auto it = content.parent(content.find(k) );

		if ( it != content.end() )
		{
			return it->first;
		}

		return INVALID;
	}

	size_type child_count(key_type k = INVALID) const
	{
		return content.child_count(k);
	}

	key_type child(
		key_type  k,
		size_type i
	) const
	{
		return content.child(k, i);
	}

	key_type child(size_type i) const
	{
		return content.child(i);
	}

	parse_match& at(key_type k)
	{
		return content.at(k);
	}

	size_type index(key_type k) const
	{
		return content.row(k);
	}

	const parse_match& at(key_type k) const
	{
		return content.at(k);
	}

	bool empty() const
	{
		return content.empty();
	}

	void erase(key_type k)
	{
		content.erase(k);
	}

	key_type insert(
		key_type    k,
		NonTerminal rule,
		std::size_t offset,
		std::size_t end
	)
	{
		return content.insert(k, parse_match{ rule, offset, end })->first;
	}
private:
	tree< parse_match > content;
};

namespace details
{
template< typename NonTerminal >
auto default_to_string(NonTerminal i)
{
	using std::to_string;

	return to_string(i);
}

template< typename NonTerminal, typename Sequence, typename F >
void print_helper(
	const derivation< NonTerminal >&             d,
	typename derivation< NonTerminal >::key_type k,
	const Sequence&                              s,
	std::size_t                                  depth,
	const F&                                     to_string
)
{
	const auto n = d.child_count(k);

	if ( k != derivation< NonTerminal >::INVALID )
	{
		const auto& dk = d.at(k);
		std::cout << std::string(depth, ' ') << char(depth % 10 + '0') << ":" << to_string(dk.rule) << ": ";

		for (std::size_t i = dk.offset; i < dk.end; ++i)
		{
			if ( i != dk.offset )
			{
				std::cout << ' ';
			}

			std::cout << s[i];
		}

		std::cout << std::endl;
	}

	for (typename derivation< NonTerminal >::size_type i = 0; i < n; ++i)
	{
		print_helper(d, d.child(k, i), s, depth + 1, to_string);
	}
}

}

template< typename NonTerminal, typename Sequence, typename F >
void print(
	const derivation< NonTerminal >& d,
	const Sequence&                  s,
	const F&                         f
)
{
	if constexpr ( std::is_invocable_v< F, NonTerminal >)
	{
		details::print_helper(d, derivation< NonTerminal >::INVALID, s, 0, f);
	}
	else
	{
		// assume a map
		auto m = [&f](NonTerminal x){
					 if ( auto it = f.find(x);it != f.end() )
					 {
						 return it->second;
					 }

					 using std::to_string;

					 return to_string(x);
				 };

		details::print_helper(d, derivation< NonTerminal >::INVALID, s, 0, m);
	}
}

template< typename NonTerminal, typename Sequence >
void print(
	const derivation< NonTerminal >& d,
	const Sequence&                  s
)
{
	print(d, s, [](NonTerminal x){ return to_string(x); });
}

#endif // DERIVATION_H
