#include "history.h"

#include <cassert>

namespace details
{

std::tuple< History::rematch_result, History::offset_type > History::start_match(
	sequence_id seq,
	offset_type curr,
	offset_type max
)
{
	// Ensure we aren't pursuing a circular match (ex., because of kleene star or plus)
	/// @todo We can know before hand if seq can even be circular
	/// @todo Can probably do something smarter with this. Either improving the Kleene match, or detecting sooner
	/// @todo The call to der.parent here is actually comparatively expensive. The underlying tree
	/// object should probably be replaced with something better anyway
	for (std::size_t i = 0, n = parents.size(); i < n; ++i)
	{
		const auto it = parents[n - 1 - i];

		const auto& [seq_, curr_, max_, ignore] = *it;

		if ( curr != curr_ || max != max_ )
		{
			break;
		}

		if ( seq == seq_ )
		{
			return { rematch_result::circular, 0 };
		}
	}

	// Check if we already have a match at this position, for this sequence, with the given amount of text
	/// @todo Can probably save a lot of lookups knowing that curr and max are bound from previous searches
	if ( auto kt = known_matches.upper_bound(parse_match{ seq, curr, max, std::size_t(-1) });
	     kt != known_matches.begin()
	)
	{
		--kt;

		// Is this a match at the correct place, and applicable to the text we can see in [curr, max)?
		if ( ( *kt )->rule == seq && ( *kt )->offset == curr && max <= ( *kt )->max )
		{
			// Add the node back into the current match
			der.connect(parents.back(), *kt);
			return { rematch_result::known_match, ( *kt )->end };
		}
	}

	// Check if we have previously failed to make a match here
	/// @todo There is more to failing than simply not having enough bytes. Some prefixes will simply never
	/// match (ex., class_head will never match a string starting with "namespace" )
	if ( auto_fail(seq, curr, max) )
	{
		// a failure with at least as many bytes available
		return { rematch_result::known_failure, 0 };
	}

	if ( parents.empty() )
	{
		root = der.emplace(seq, curr, max, max);
		parents.push_back(root);
	}
	else
	{
		parents.push_back(der.emplace_child(parents.back(), seq, curr, max, max) );
	}

	return { rematch_result::unknown, 0 };
}

void History::accept_match(offset_type end)
{
	const auto [seq, offset_, max, ignore] = *( parents.back() );

	// Update derivation
	parents.back()->end = end;

	// Record the successful match and the derivation that goes with it
	{
		auto it = known_matches.lower_bound(parse_match{ seq, offset_, end, 0 });

		if ( it != known_matches.end() && ( *it )->rule == seq && ( *it )->offset == offset_ && ( *it )->end == end )
		{
			/// @todo Store multiple derivations?
			/// @todo Not sure that this if is necessary. It's the only reason we could be here
			// Already have a match at this length, but in a longer context, so we should update it
			assert( ( *it )->max < ignore);
			der.erase(*it);
			it = known_matches.erase(it);
		}

		assert(parents.back()->rule == seq && parents.back()->offset == offset_ && parents.back()->end == end
			&& parents.back()->max == max);
		known_matches.insert(it, parents.back() );
	}

	parents.pop_back();
}

void History::reject_match()
{
	const auto [seq, curr, max, ignore] = *( parents.back() );

	if ( const offset_type prev = known_failures[curr][seq];prev < ignore )
	{
		known_failures[curr][seq] = ignore;
	}

	// Children will be unparented and can be reused as successful matches
	/// @todo Recycle node
	der.erase(parents.back() );
	parents.pop_back();
}

void History::clear()
{
	known_matches.clear();
	known_failures.clear();
	der.clear();
	parents.clear();
	root = der.end();
}

void History::note_empty_match(
	sequence_id seq,
	offset_type curr
)
{
	const auto [r, o] = start_match(seq, curr, curr);

	if ( r == rematch_result::unknown )
	{
		accept_match(curr);
	}
}

History::offset_type History::accept_alt()
{
	const auto        it = parents.back();
	const std::size_t n  = it.child_count();

	if ( n > 1 )
	{
		// Erase the one that is shorter
		auto first  = it.child(0);
		auto second = it.child(1);

		/// @todo Use common logic with the reject_match so that successful submatches
		/// are saved for rematching
		if ( first->end < second->end )
		{
			// Newer is better
			it.disconnect_child(0);
			return second->end;
		}

		it.disconnect_child(1);
		return first->end;
	}

	return it.child(0)->end;
}

History::parse_match History::get_rematch_info()
{
	return *( parents.back() );
}

History::parse_match History::forget()
{
	const auto m = *( parents.back() );

	known_matches.erase(parents.back() );
	der.erase(parents.back() );
	parents.pop_back();

	return m;
}

// No longer sure of the successful match
void History::unaccept()
{
	known_matches.erase(parents.back() );

	reject_match();
}

bool History::not_empty() const
{
	return !parents.empty();
}

bool History::auto_fail(
	sequence_id seq,
	offset_type curr,
	offset_type max
) const
{
	if ( auto it = known_failures.find(curr);it != known_failures.end() )
	{
		if ( auto jt = it->second.find(seq);jt != it->second.end() )
		{
			return max <= jt->second;
		}
	}

	return false;
}

bool History::find_preliminary_match(const std::map< offset_type, offset_type >& open_to_close)
{
	parents.clear();

	return find_preliminary_match_inner(open_to_close.begin(), open_to_close.end(), root);
}

bool History::find_preliminary_match_inner(
	std::map< offset_type, offset_type >::const_iterator       first,
	const std::map< offset_type, offset_type >::const_iterator last,
	DirectedGraph< parse_match >::vertex_iterator              it
)
{
	if ( first == last )
	{
		return false;
	}

	const std::size_t nchildren = it.child_count();

	if ( it->offset == first->first && nchildren == 0 )
	{
		// This will be a nested rule that hasn't been matched fully, yet
		parents.push_back(it);

		return true;
	}

	#if 1

	for (std::size_t i = 0; i < nchildren; ++i)
	{
		const auto jt = it.child(i);

		/// @todo lower_bound, upper_bound
		while ( first != last && first->first < jt->offset )
		{
			++first;
		}

		auto mid = first;

		while ( mid != last && mid->first < jt->end )
		{
			++mid;
		}

		if ( find_preliminary_match_inner(first, mid, jt) )
		{
			parents.insert(parents.begin(), it);

			return true;
		}

		first = mid;
	}

	return false;

	#else

	const tree< parse_match >::size_type n = der.child_count(k);

	for (tree< parse_match >::size_type i = 0; i < n; ++i)
	{
		const auto ki = find_preliminary_match_inner(g_, first, last, der.child(k, i) );

		if ( ki != tree< parse_match >::INVALID )
		{
			return ki;
		}
	}

	return tree< parse_match >::INVALID;

	#endif // if 1
}

}
