#ifndef EBNF_H
#define EBNF_H

#include <map>

#include "grammar/context_free_grammar.h"

std::pair< jl::grammar::context_free_grammar< char32_t, int >, std::map< ustring, int > > ebnf_to_cfg(const std::vector< ustring >&);

template< typename Container >
std::pair< jl::grammar::context_free_grammar< char32_t, int >, std::map< ustring, int > > ebnf_to_cfg(const Container& c)
{
	using ::std::begin;
	using ::std::end;

	const std::vector< ustring > v(begin(c), end(c) );

	return ebnf_to_cfg(v);
}

#endif // EBNF_H
