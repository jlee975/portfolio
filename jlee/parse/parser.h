#ifndef PARSER_H
#define PARSER_H

#include "history.h"
#include <cassert>

namespace details
{
/// @todo Infer Terminal from Iterator
/// @todo Specialization for contiguous iterator, char, memcmp
template< typename Iterator, typename Terminal >
bool match_literal(
	const Iterator&                    s,
	const std::span< const Terminal >& lit,
	std::size_t                        j,
	std::size_t                        k
)
{
	const std::size_t n = lit.size();

	if ( k - j >= n )
	{
		std::size_t i = 0;

		while ( i < n && s[j + i] == lit[i] )
		{
			++i;
		}

		if ( i == n )
		{
			return true;
		}
	}

	return false;
}

/// @todo Infer Terminal from Iterator
/// @todo Infer offset_type from Iterator
template< typename Iterator, typename Terminal >
auto find_open_and_closed(
	const typename jl::grammar::details::tibuilder< Terminal >::nested_pair_list& lr,
	Iterator                                                                      first_,
	Iterator                                                                      last_
)
{
	using offset_type = std::size_t;
	const offset_type                    max = last_ - first_;
	const Iterator                       s   = first_;
	std::map< offset_type, offset_type > open_to_close;
	std::map< offset_type, offset_type > close_to_open;

	// for nested literals, the left and right offsets of the matched symbols
	std::vector< offset_type > open;

	// for each position, is there a beginning or ending terminal here
	/// @bug If a r terminal is also a l terminal, throw. Such a throw should happen in context_free_grammar
	for (offset_type k = 0; k < max;)
	{
		std::size_t step = 1;

		for (const auto& [l, r] : lr)
		{
			if ( details::match_literal< Iterator, Terminal >(s, l, k, max) )
			{
				// left terminal
				open.push_back(k);
				open_to_close.emplace(k, offset_type(-1) );
				step = l.size();
				break;
			}
			else if ( details::match_literal< Iterator, Terminal >(s, r, k, max) )
			{
				// right terminal
				if ( open.empty() )
				{
					throw std::runtime_error("Not nested");
				}

				const offset_type k0 = open.back();
				open.pop_back();

				if ( !details::match_literal< Iterator, Terminal >(s, l, k0, k) )
				{
					throw std::runtime_error("Mismatched nested");
				}

				open_to_close[k0] = k;
				step              = r.size();
				break;
			}
		}

		k += step;
	}

	for (const auto& [o, c] : open_to_close)
	{
		close_to_open.emplace(c, o);
	}

	if ( !open.empty() )
	{
		throw std::runtime_error("Unmatched nested");
	}

	return std::make_pair(std::move(open_to_close), std::move(close_to_open) );
}

/** @brief Parsing class
 *
 * This is a class for parsing a string froma context free grammar. It works, and it is fast enough for my
 * purposes. It is basically, however, a brute force parse. It tries every possibility the grammar allows. It
 * has only one real optimization, which is to record partial matches (and reuse them). This is good enough,
 * though, to parse a 182kb xml virtually instantly.
 *
 * @invariant curr <= max
 *
 * There are a number of optimizations I would like to make though:
 * @todo Guided rematching. Ex., first parse C++ as balanced braces (a smaller grammar), then improve
 * the parse around these fixed parts. Another example, "enum class" can only appear in one rule. Keywords
 * like "do", "for", "if" are very strong indicators. The key idea is to match simplified (possibly
 * unambiguous) grammars to provide a starting point for matching the full grammar
 * @todo GLL parse-treegeneration by Elizabeth Scott, Adrian Johnstone
 * @todo Iterators that abstract char32_t. Template.
 * @todo Alt character class, character class should be combined into one
 * @todo Concat string lit, string lit should be combined
 * @todo ranges should be checked with unsigned arithmetic (ex., c - '0' <= '9' - '0')
 * @todo alt eta,x should skip the eta
 * @todo Implement GLR, Earley, or some @em actual algorithm
 * @todo Recognize regular sub-grammars and jump to a NFA to parse that portion
 * @todo Arrange the context free grammar to favour prefixes. i.e., A rule A := B C D should be decomposed
 * as A := E D; E := BC, so that a partial match of A to E can be used for some other rule. When we rematch,
 * we are often rematching prefixes
 * @todo If, at any point, we know we won't be backtracking, we can "forget" the partial match information
 * 'cause we won't be reusing it
 * @todo During rematching, we often want to "shrink" an already matched rule. Currently this is done by
 * imposing an artificial limit on the input string length, then calling the normal match routine. A
 * specialized "shrink" routine would probably be faster
 * @todo We should pursue @em all alternates for a match, since we sometimes can have two rules match the
 * same substring. The C++ grammar has examples of this (ex., the most vexing parse)
 * @todo Recognize tokenizing subgrammars. Ex., C++ can be tokenized (into string literals, keywords, operators,
 * etc.) and then those tokens treated as terminals and a whole second grammar stacked on top
 * @todo Replace kleene star with kleene plus. A* -> (eta|A+). In general, no rule should be an eta production
 * unless it is explicitly an eta production. This will mean that each match will match at least one character,
 * which in turn will ensure our depth cannot get out of hand
 * @todo Watch for runaway strings. Esp (Char* 'end'). Char* can potentially run to the end of the file, then
 * fail and we'd backtrack character by character, always matching the Char* portion, but taking a very long
 * time to find the right point where it preceeds 'end'. This is potentially a problem for any two concated
 * rules where the end of one can be the beginning of the other
 * @todo Recognize nested parens and similar type rules, Ex., where '(' must have a corresponding ')'
 * @todo Recognize quoted, escaped string sequences
 * @todo Jump from one failed partial match to another match. Ex., parsing (Expression '*') might fail, but
 * (Expression '+') might be successful. Want to quickly switch matching rather than backtrack
 * @todo Try to arrange alts so they are mutually exclusive, esp by prefix.
 * @todo Fail for some submatches. Ex., "identifier" will never match as "identifie" because there is no
 * situation where "identifie" and "r" would be adjacent (i.e., it is tokenized). So this should be marked
 * as "unshrinkable"
 * @todo Kleene star/plus can probably be implemented with ALT and some code in match_alt to recognize the
 * special situation
 * @todo In rematch, allow previous matches to be "promoted" Ex., if we match OPTIONAL and that could be used
 * directly as either an ALTTERM, CATTERM, or whatever, then accept the match. This can be precalculated
 * easily
 * @todo Shunting yard or similar if we recognize "expressions"
 * @todo Improve rematch by leveraging the fact that we should pretty much always know where we are wrt
 * history. i.e., usually, we'll be at the end. If we have to back track, then adjust the search space to
 * reflect that. Otherwise, save some of the map lookup by stashing the uninteresting part of the history
 * @todo GLR, CYK, GLL, Earley sub parsers
 * @todo Shared Packed Parse Forest instead of derivation
 * @todo A recognize function that doesn't bother with construction of the derivation or SPPF
 * @todo Some matches cannot be shrunk because they couldn't possibly be followed by a rule that would slide
 * left to accomodate. Ex., C++ tokens like strings, numbers, identifiers. Matching an identifier @em must
 * be greedy. There's no way to shorten the match and have whatever follows it conform to the grammar.
 * @todo if, for, while, should be near balanced parens. Note, it is possible for for(){ } to appear in
 *   attribute_argument_clause
 * @todo There are limited number of parses once we see ex., "class", "namespace". We can prematch these, then let
 *   the rematch code pick up on these
 * @todo Where we match certain things, we can automatically register some alternatives as fails
 */
template< typename Terminal, typename NonTerminal, typename Iterator >
class iterator_parser
{
public:
	using cfg         = jl::grammar::context_free_grammar< Terminal, NonTerminal >;
	using sequence_id = typename cfg::sequence_id;
	using offset_type = std::size_t;

	iterator_parser(
		const cfg& g,
		Iterator   first_,
		Iterator   last_
	)
		: iterator_parser(
			g,
			first_,
			last_,
			find_open_and_closed< Iterator, Terminal >( g.get_all_nested(), first_, last_ )
		)
	{
	}

	iterator_parser(const iterator_parser&)            = delete;
	iterator_parser(iterator_parser&&)                 = delete;
	iterator_parser& operator=(const iterator_parser&) = delete;
	iterator_parser& operator=(iterator_parser&&)      = delete;

	derivation< NonTerminal > parse(
		NonTerminal                start,
		const std::vector< bool >& keep
	)
	{
		history.clear();
		curr = 0;
		max  = last - s;

		if ( parse_inner(g_.getseq(start) ) )
		{
			return history.make_derivation(g_, last - s, start);
		}

		return {};
	}
private:
	bool parse_inner(sequence_id seq)
	{
		if ( match_seq(seq) && curr == max )
		{
			history.debug_check();

			/// @todo This could be parallelized because guaranteed to be disjoint
			while ( history.find_preliminary_match(open_to_close) )
			{
				if ( !handle_unmatched_nested() )
				{
					return {};

					/// @todo Once parsing up succeeds, we should retry the preliminary match
				}
			}

			// std::cout << "Parsing succeeded... on to making the derivation" << std::endl;

			return true;
		}

		return false;
	}

	bool handle_unmatched_nested()
	{
		{
			const auto pm = history.get_rematch_info();

			// Try to match the nested rule
			const auto& open  = g_.open_literal(pm.rule);
			const auto& close = g_.close_literal(pm.rule);

			set_range(pm.offset + open.size(), pm.end - close.size() );

			if ( match_seq(g_.get_nested(pm.rule) ) && curr + close.size() == pm.end )
			{
				return true;
			}
		}

		// Matching the inside of the nested sequence failed. Reject the nested sequence itself
		// and move up to parent.
		history.unaccept();

		while ( history.not_empty() )
		{
			/// @todo This deletes history.parents.back(), but it just gets re-added below. Need
			/// to short circuit this operation
			// Erase the known success from before so we can re-attempt
			const auto pm = history.forget();

			set_range(pm.offset, pm.end);

			/// @todo Can we use parse_inner and recursively re-match preliminary matches (perhaps
			/// using curr and max to narrow down the search)
			if ( match_seq(pm.rule) && curr == max )
			{
				return true;
			}
		}

		// Total failure
		return false;
	}

	iterator_parser(
		const cfg&                                                                              g,
		Iterator                                                                                first_,
		Iterator                                                                                last_,
		std::pair< std::map< offset_type, offset_type >, std::map< offset_type, offset_type > > otoc
	)
		: g_(g), s(first_), last(last_), open_to_close(std::move(otoc.first) ),
		close_to_open(std::move(otoc.second) ), max(last_ - first_), curr(0)
	{
	}

	/* Nonterminals simply forward to the match pattern in the grammar. They only need
	 * to be distinguished so we can build the parse tree afterwards. Ex., The production
	 * 'a' 'b' may match for non terminals 'X' or 'Y', or simply appear on its own. So
	 * while the parser is really only interested in matching 'a' 'b', the construction of
	 * the parse tree for the user must know if any particular matching is for one of
	 * the non-terminals
	 *
	 * Consequently, this function "intercepts" non-terminal match sequences, notes that
	 * we are trying to match it, but ultimately strips it out and tries to match the
	 * underlying pattern. This promotes rematching.
	 */
	/// @todo If we ever get back to matching the start symbol, we can probably commit to what we've matched (and
	/// release any tracking details). Probably complicated by the fact that the actual start symbol will be a
	/// NONTERMINAL match
	/// @todo If the minimum match length is 1, should just store the character class and test that against one symbol
	/// @todo Where we match certain things, we can automatically register some alternatives as fails
	bool match_seq(sequence_id seq)
	{
		const offset_type minlen = g_.get_min_match_length(seq);

		/// @todo If something can begin with X, we should note which paths allow such a match. Esp. if seq is an
		/// ALT. Actually, return the sequence that would result after consuming the token
		if ( max - curr < minlen || ( minlen != 0 && !g_.can_begin_with(seq, s[curr]) ) )
		{
			return false;
		}

		const offset_type original_curr = curr;
		const offset_type original_max  = max;

		while ( true )
		{
			if ( max == curr )
			{
				// We have an empty string, and can match it (by the previous logic, there's enough text)
				history.note_empty_match(seq, curr);

				max = original_max;
				return true;
			}

			// Begin matching, and use short circuit logic for repeated matches
			const auto [r, o] = history.start_match(seq, curr, max);

			switch ( r )
			{
			case History::rematch_result::unknown:

				if ( match_seq_inner(seq) )
				{
					history.accept_match(curr);
					max = original_max;
					return true;
				}

				history.reject_match();
				set_range(original_curr, original_max);
				return false;

			case History::rematch_result::known_match:
				set_range(o, original_max);
				return true;

			case History::rematch_result::known_failure:
				set_range(original_curr, original_max);
				return false;

			case History::rematch_result::circular:

				// We already tried matching this
				// Maybe we can match on a substring
				/// @todo instead of shrinking by 1, shrink by minimum required for tail
				if ( max - curr <= minlen )
				{
					// No chance if the string is not long enough
					set_range(original_curr, original_max);
					return false;
				}

				set_max(max - 1);
				break;

			default:
				throw std::logic_error("Bad enum");
			} // switch

		}
	}

	bool match_seq_inner(sequence_id seq)
	{
		// Have we reached the end of the sequence
		switch ( g_.getkind(seq) )
		{
		case jl::grammar::details::LITERAL:
			return match_literal(seq);

		case jl::grammar::details::CHARACTER_CLASS:
			return match_character_class(seq);

		case jl::grammar::details::ALT:
			return match_alt(seq);

		case jl::grammar::details::CAT:
			return match_ab(seq);

		case jl::grammar::details::EXCEPT:
			return match_except(seq);

		case jl::grammar::details::NESTED:
			return match_nested(seq);

		case jl::grammar::details::NONTERMINAL:
			return match_seq(g_.assoc(seq) );

		default:
			throw std::logic_error("Missing case");
		} // switch

	}

	bool match_literal(sequence_id seq)
	{
		const auto& lit = g_.getliteral(seq);

		if ( details::match_literal< Iterator, Terminal >(s, lit, curr, max) )
		{
			set_curr(curr + lit.size() );
			return true;
		}

		return false;
	}

	bool match_character_class(sequence_id seq)
	{
		if ( curr < max && g_.getcharacterclass(seq).count(s[curr]) != 0 )
		{
			set_curr(curr + 1);
			return true;
		}

		return false;
	}

	/// @todo If AB have mutually exclusive beginning character sets, do a quick test to determine which way to go
	/// @todo If AB have common prefix, use that information
	/// @todo Detect kleene-start/plus esp for character classes or literals that repeat
	/// @todo IF we can match minimum lengths, we can pass in the length of the current best match
	/// @todo If only one potentail match (ex., because of can_begin_with) treat as a simple pass through
	bool match_alt(sequence_id seq)
	{
		const auto&       v      = g_.getalts(seq);
		const offset_type offset = curr;

		std::optional< offset_type > longest;

		for (const sequence_id i : v)
		{
			set_curr(offset);

			if ( match_seq(i) )
			{
				longest = history.accept_alt();

				if ( curr == max )
				{
					break;
				}
			}
		}

		if ( longest )
		{
			set_curr(*longest);
			return true;
		}

		// Nothing matched
		set_curr(offset);
		return false;
	}

	bool match_ab(sequence_id seq)
	{
		const auto& v = g_.getcat(seq);

		const std::size_t n = v.size();

		/// @todo Precalculate this vector as the remaining tail min lengths
		/// @todo Probably don't need this vector. Can probably sum, and then
		/// subtract min_string_length(v[i]) as we go deeper
		std::vector< std::size_t > minlens;

		minlens.reserve(n);

		for (std::size_t i = 0; i < n; ++i)
		{
			minlens.push_back(g_.get_min_match_length(v[i]) );
		}

		return match_ab(v, 0, n, minlens);
	}

	/// @todo Removed the "if shiftable" logic
	/// @todo Reuse the earlier match information for a
	/// @bug Technically, AB @em might be longer for a shorter A followed by a longer B
	/// @todo Rules with nested literals should be precomputed
	/// @todo Try to avoid recursion with a partitioning algorithm
	// We have matched v[j] for j in [0, i). Try to match the rest
	bool match_ab(
		const std::span< const sequence_id >& v,
		std::size_t                           i,
		std::size_t                           n,
		const std::vector< std::size_t >&     minlens
	)
	{
		// Match the end of the sequence, trivially
		if ( i == n )
		{
			return true;
		}

		std::size_t min_length_for_tail = 0;

		for (std::size_t j = i + 1; j < n; ++j)
		{
			min_length_for_tail += minlens[j];
		}

		if ( min_length_for_tail <= max - curr )
		{
			const offset_type offset = curr;
			const offset_type len    = max;

			/// @todo This is surprisingly costly
			set_max(len - min_length_for_tail);

			if ( match_seq(v[i]) )
			{
				do
				{
					max = len;

					if ( match_ab(v, i + 1, n, minlens) )
					{
						// Matched A and B, no problem. Let's go.
						return true;
					}
				}
				while ( shrink(v[i], offset) );
			}

			max = len;
			set_curr(offset);
		}

		return false;
	}

	// Match the opening literal, the inner sequence, and the closing literal
	bool match_nested(sequence_id seq)
	{
		const offset_type offset = curr;
		const offset_type len    = max;

		const auto& open  = g_.open_literal(seq);
		const auto& close = g_.close_literal(seq);

		// Check if open is found at current position
		if ( details::match_literal< Iterator, Terminal >(s, open, curr, last - s) )
		{
			// Check if closing literal is in range
			/// @todo If it's not in range a bunch of stuff above is gonna fail
			if ( auto it = open_to_close.find(curr);it != open_to_close.end() )
			{
				const offset_type right = it->second;

				if ( right < max && close.size() <= max - right )
				{
					set_curr(right + close.size() );
					return true;
				}
			}
		}

		return false;
	}

	#if 0
	/// @todo Generate on demand, rather than precalculate
	offset_type find_matching_literal(
		offset_type               i,
		const character_sequence& l,
		const character_sequence& r
	) const
	{
		if ( const offset_type n = last - s;match_literal(s, l, i, n) )
		{
			// Have we already found the matching literal for this position?
			if ( auto it = open_to_close.find(i);it != open_to_close.end() )
			{
				// Just make sure that it's the correct literal at that position
				return it->second;
			}

			#if 0
			// for nested literals, the left and right offsets of the matched symbols
			std::vector< offset_type > open;

			// for each position, is there a beginning or ending terminal here
			for (offset_type k = i + l.size(); k < n;)
			{
				std::size_t step = 1;

				if ( match_literal(l, k, n) )
				{
					// left terminal
					open.push_back(k);
					step = l.size();
				}
				else if ( match_literal(r, k, n) )
				{
					// right terminal
					if ( open.empty() )
					{
						// We're done
						open_to_close.emplace(i, k);
						close_to_open.emplace(k, i);
						return k;
					}

					open_to_close.emplace(open.back(), k);
					close_to_open.emplace( k, open.back() );
					open.pop_back();
					step = r.size();
				}

				k += step;
			}

			#endif // if 0
		}

		return offset_type(-1);
	}

	#endif // if 0
	// matched seq from offset to aend, but could not match the following sequence
	// shrink the range and try again
	/// @todo Actually try shrinking the match intelligently
	/// @todo Try shrinking everything matched so far
	/// @todo Shrink cat should try to shrink the last term first
	/// @todo Shrink alt we should already know the alt lengths largest to smallest
	/// @todo Shrinking a cat except the last term would require shifting the next term to the left
	bool shrink(
		sequence_id seq,
		offset_type offset
	)
	{
		if ( curr == offset )
		{
			// we cannot shrink a any more
			return {};
		}

		// Try again, shrinking A
		const auto max_ = curr - 1;

		set_curr(offset);

		set_max(max_);

		return match_seq(seq);
	}

	/*
	   std::optional< length_type > match_kleene(sequence_id seq)
	   {
	    const auto a = g_.getkleene(seq);

	    if ( g_.getkind(a) == cfg::CHARACTER_CLASS )
	    {
	        // Special case -- scan characters
	        const character_class& cc = g_.getcharacterclass(a);

	        auto first = s.begin() + curr;
	        auto last  = s.begin() + max;
	        auto it    = find_first_not_of(first, last, cc);

	        return it - first;
	    }

	    const offset_type offset = curr;

	    offset_type start = offset;

	    while ( match_seq(a) )
	    {
	        // Match of length zero is gonna cause an infinite loop
	        const offset_type end = curr;

	        if ( end == start )
	        {
	            break;
	        }

	        start = end;
	    }

	    return start - offset;
	   }
	 */

	/// @todo Once b matches, shrink and rematch using what we already know. Only
	/// test a again once we have failure
	/// @todo Shrink a and b simultaneously
	bool match_except(sequence_id seq)
	{
		const auto [a, b] = g_.getexception(seq);
		const offset_type offset = curr;
		const offset_type len    = max;

		while ( match_seq(a) )
		{
			// At least one match for A. Check if the same string matches B
			const offset_type aend = curr;

			set_curr(offset);
			set_max(aend);

			if ( !match_seq(b) || curr != aend )
			{
				// B did not match. Accept, fixup d and return
				set_range(aend, len);
				return true;
			}

			// B matched, so we need to shrink len
			if ( aend == offset )
			{
				// Can't actually shrink
				break;
			}

			set_curr(offset);
			set_max(aend - 1);
		}

		// Failure
		set_range(offset, len);

		return false;
	}

	/* If there are nested pairs, then we should adjust max_ based on:
	 *   - max <= Any closing terminal whose opening terminal is before curr
	 *      This simply means checking the most recent opening terminal
	 *   - max <= Any opening terminal whose closing terminal is after max_
	 */
	void set_max(const offset_type max_)
	{
		/* If setting the max cuts off the end of a nested pair, then we may as well set
		 * the max to the opening pair
		 */
		offset_type min = max_;

		/* Not sure why this doesn't work. If a nested pair is open at curr, the match can not include
		 * the closing terminal
		 *
		   for (auto it = open_to_close.begin(); it != open_to_close.end() && it->first < curr; ++it)
		   {
		    // Nested pair is still open at this rule
		    if (it->first < curr && it->second >= curr)
		    {
		        min = std::min(min, it->second);
		    }
		   }
		   //*/

		/// @todo Precalculate
		/// @todo As min changes, does this affect what should be considered? i.e., the original search
		/// for max_ might need to be adjusted if min < max_
		for (auto it = close_to_open.lower_bound(max_); it != close_to_open.end(); ++it)
		{
			if ( it->second >= curr && it->second < min )
			{
				min = it->second;
			}
		}

		max = min;
	}

	// Does not adjust check max
	void set_range(
		offset_type curr_,
		offset_type max_
	)
	{
		max = max_;
		set_curr(curr_);
	}

	void set_curr(offset_type curr_)
	{
		assert(curr_ <= max);
		curr = curr_;
	}

	/*
	   void print_inner(const ustring&, std::size_t, std::size_t) const;
	 */

	const cfg& g_;

	const Iterator s;
	const Iterator last;

	/// @todo Make const flatmap
	/// (offset of opening literal, offset of closing literal)
	const std::map< offset_type, offset_type > open_to_close;

	/// @todo Could combine with open_to_close. Would know if it's a closing or opening position based on first ?= second
	const std::map< offset_type, offset_type > close_to_open;

	History history;

	// Max length of the string being matched
	offset_type max;

	// Where matching is currently
	offset_type curr;
};

template< typename Sequence >
auto begin_helper(const Sequence& s)
{
	using std::cbegin;

	return cbegin(s);
}

template< typename Sequence >
auto end_helper(const Sequence& s)
{
	using std::cend;

	return cend(s);
}

template< typename Terminal, typename NonTerminal, typename Sequence >
class parser
	: public iterator_parser< Terminal, NonTerminal, decltype( begin_helper(std::declval< Sequence >() ) ) >
{
public:
	parser(
		const jl::grammar::context_free_grammar< Terminal, NonTerminal >& g,
		const Sequence&                                                   s_
	)
		: iterator_parser< Terminal, NonTerminal, decltype( begin_helper(std::declval< Sequence >( ) ) ) >(
			g, begin_helper(s_), end_helper(s_) )
	{
	}

};

/// @todo All these specializations boil down to "has contiguous iterators". Can probably use std::span
template< typename Terminal, typename NonTerminal, typename T, typename A >
class parser< Terminal, NonTerminal, std::vector< T, A > >
	: public iterator_parser< Terminal, NonTerminal, const T* >
{
public:
	parser(
		const jl::grammar::context_free_grammar< Terminal, NonTerminal >& g,
		const std::vector< T, A >&                                        v
	)
		: iterator_parser< Terminal, NonTerminal, const T* >(g, v.data(), v.data() + v.size() )
	{
	}

};

template< typename Terminal, typename NonTerminal, typename T >
class parser< Terminal, NonTerminal, std::span< T > >
	: public iterator_parser< Terminal, NonTerminal, const T* >
{
public:
	parser(
		const jl::grammar::context_free_grammar< Terminal, NonTerminal >& g,
		const std::span< T >&                                             v
	)
		: iterator_parser< Terminal, NonTerminal, const T* >(g, v.data(), v.data() + v.size() )
	{
	}

};

template< typename Terminal, typename NonTerminal, typename C, typename T, typename A >
class parser< Terminal, NonTerminal, std::basic_string< C, T, A > >
	: public iterator_parser< Terminal, NonTerminal, const C* >
{
public:
	parser(
		const jl::grammar::context_free_grammar< Terminal, NonTerminal >& g,
		const std::basic_string< C, T, A >&                               s_
	)
		: iterator_parser< Terminal, NonTerminal, const C* >(g, s_.data(), s_.data() + s_.size() )
	{
	}

};

template< typename Terminal, typename NonTerminal, typename C, typename T >
class parser< Terminal, NonTerminal, std::basic_string_view< C, T > >
	: public iterator_parser< Terminal, NonTerminal, const C* >
{
public:
	parser(
		const jl::grammar::context_free_grammar< Terminal, NonTerminal >& g,
		const std::basic_string_view< C, T >&                             s_
	)
		: iterator_parser< Terminal, NonTerminal, const C* >(g, s_.data(), s_.data() + s_.size() )
	{
	}

};

template< typename Terminal, typename NonTerminal, typename T, std::size_t N >
class parser< Terminal, NonTerminal, std::array< T, N > >
	: public iterator_parser< Terminal, NonTerminal, const T* >
{
public:
	parser(
		const jl::grammar::context_free_grammar< Terminal, NonTerminal >& g,
		const std::array< T, N >&                                         a
	)
		: iterator_parser< Terminal, NonTerminal, const T* >(g, a.data(), a.data() + N)
	{
	}

};

template< typename Terminal, typename NonTerminal, typename T, std::size_t N >
class parser< Terminal, NonTerminal, T(&)[N] >
	: public iterator_parser< Terminal, NonTerminal, const T* >
{
public:
	parser(
		const jl::grammar::context_free_grammar< Terminal, NonTerminal >& g,
		const T ( & a )[N]
	)
		: iterator_parser< Terminal, NonTerminal, const T* >(g, a + 0, a + N)
	{
	}

};

/// @todo C is a char, signed char, unsigned char, wchar_t, char1_t, char32_t, char8_t
template< typename Terminal, typename NonTerminal, typename C >
class parser< Terminal, NonTerminal, const C* >
	: public iterator_parser< Terminal, NonTerminal, const C* >
{
public:
	parser(
		const jl::grammar::context_free_grammar< Terminal, NonTerminal >& g,
		const C*                                                          s_
	)
		: iterator_parser< Terminal, NonTerminal, const C* >(g, s_ + 0, s_ + std::char_traits< C >::length(s_) )
	{
	}

};
}

#endif // PARSER_H
