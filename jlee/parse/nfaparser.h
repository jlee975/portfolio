#ifndef PARSER_H
#define PARSER_H

#include "automata/nfa.h"

class nfaparser
{
public:
	using vertex_id = NondeterministicFiniteAutomaton::vertex_id;

	explicit nfaparser(NondeterministicFiniteAutomaton g_)
		: g(std::move(g_) )
	{
	}

	template< typename String >
	bool validate(
		const String& s,
		vertex_id     j_
	) const
	{
		using std::begin;
		using std::end;
		return validate(begin(s), end(s), j_);
	}

	template< typename ForwardIterator >
	bool validate(
		ForwardIterator first,
		ForwardIterator last,
		vertex_id       j_
	) const
	{
		// beginning at node i, read characters, push and pop until EOF and terminal node
		std::vector< std::tuple< ForwardIterator, std::size_t, std::size_t > > history;

		std::vector< std::size_t > matches;

		// Initialize at start of string, initial rule, plan to use first transition
		ForwardIterator it = first;
		std::size_t     j  = j_;
		std::size_t     k  = 0;

		while ( true )
		{
			// we're at state j. Try to transition
			while ( k < g.transitions(j).size() )
			{
				auto        jt         = it;
				bool        success    = false;
				const auto& transition = g.transition(j, k);

				switch ( transition.data.type )
				{
				case NondeterministicFiniteAutomaton::ETA_EDGE:
					// Can always take an eta-production
					success = true;
					break;
				/*
				   case NondeterministicFiniteAutomaton::PUSH_NODE:
				   matches.push_back(g[j].transitions[k].x);
				   success = true;
				   break;
				   case NondeterministicFiniteAutomaton::POP_NODE:
				   if (!matches.empty() && matches.back() == g[j].transitions[k].dest)
				   {
				    matches.pop_back();
				    success = true;
				   }
				   break;
				 */
				case NondeterministicFiniteAutomaton::ALT_EDGE:

					if ( jt != last && transition.data.s.find(*jt) != std::string::npos )
					{
						++jt;
						success = true;
					}

					break;
				case NondeterministicFiniteAutomaton::SEQ_EDGE:
				{
					const std::size_t n = transition.data.s.length();
					std::size_t       i = 0;

					while ( i < n && jt != last && transition.data.s[i] == *jt )
					{
						++i;
						++jt;
					}

					if ( i == n )
					{
						success = true;
					}

					break;
				}
				} // switch

				if ( success )
				{
					// Move to the next state. Record where we've been
					history.emplace_back(it, j, k);
					it = jt;
					j  = transition.to;
					k  = 0;
				}
				else
				{
					// Couldn't make transition. Try the next one
					++k;
				}
			}

			if ( g.transitions(j).empty() )
			{
				if ( it == last && matches.empty() )
				{
					return true;
				}
			}

			// Nope. Backtrack.
			if ( history.empty() )
			{
				// No place to go back to. We've failed to recognize the string
				return false;
			}

			std::tie(it, j, k) = history.back();

			switch ( g.transition(j, k).data.type )
			{
			case NondeterministicFiniteAutomaton::ETA_EDGE:
				break;
			/*
			   case NondeterministicFiniteAutomaton::PUSH_NODE:
			   matches.pop_back();
			   break;
			   case NondeterministicFiniteAutomaton::POP_NODE:
			   matches.push_back(g[j].transitions[k].dest);
			   break;
			 */
			case NondeterministicFiniteAutomaton::ALT_EDGE:
				break;
			case NondeterministicFiniteAutomaton::SEQ_EDGE:
				break;
			}

			++k;
			history.pop_back();
		}
	}
private:
	NondeterministicFiniteAutomaton g;
};

#endif // PARSER_H
