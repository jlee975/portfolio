#ifndef PARSE_H
#define PARSE_H

#include "parser.h"

template< typename Sequence, typename Terminal, typename NonTerminal >
derivation< NonTerminal > parse(
	const Sequence&                                                   s,
	NonTerminal                                                       start,
	const jl::grammar::context_free_grammar< Terminal, NonTerminal >& g,
	const std::vector< bool >&                                        keep
)
{
	details::parser< Terminal, NonTerminal, Sequence > d(g, s);

	return d.parse(start, keep);
}

/// @todo const char arrays end up including the terminating null character
template< typename Sequence, typename Terminal, typename NonTerminal >
derivation< NonTerminal > parse(
	const Sequence&                                                   s,
	NonTerminal                                                       start,
	const jl::grammar::context_free_grammar< Terminal, NonTerminal >& g
)
{
	const std::vector< bool > keep(g.get_subs_size(), true);

	return parse(s, start, g, keep);
}

#endif // PARSE_H
