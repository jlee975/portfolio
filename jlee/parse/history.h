#ifndef HISTORY_H
#define HISTORY_H

#include <map>
#include <set>

#include "graph/directed.h"

#include "derivation.h"
#include "grammar/context_free_grammar.h"

namespace details
{
/** @brief Tracks results of match attempts
 *
 *  Notes where successful and failed matches occur, so we don't duplicate the work
 *
 *  @todo This whole thing needs an overhaul to avoid duplicating information. A smart graph structure
 *  with reference counting.
 *
 *  @todo We should note if a previously found match is the longest it possibly could be for that rule
 *  We can know this recursively if all components are at their longest, for example (also, doesn't end
 *  with S*, S+, that kind of thing)
 *  @todo If we are convinced of something (i.e., there are no other possible parses) we should drop the
 *  information we don't need
 *  @todo Instead of recording match success at a particular position, any similar substring
 *  @todo Remove parents stuff that is totally useless now
 *  @todo Template on iterator type and/or size_type/difference_type
 */
class History
{
public:
	using sequence_id = ::jl::grammar::details::sequence_id;
	using offset_type = std::size_t;

	/** @brief Where a rule matches.
	 *
	 * @todo Prefer a "view" to offset/end pair.
	 */
	struct parse_match
	{
		sequence_id rule;
		offset_type offset;
		offset_type end;
		offset_type max;
	};

	/** @brief Result of beginning a match.
	 *
	 *  @todo A match that could be expanded (matched in a shorter context, and maybe could
	 *  be extended in the longer substring).
	 */
	enum class rematch_result
	{
		/** No information about matching at the given location.
		 */
		unknown,
		/** Already have a match (and it should be used).
		 */
		known_match,
		/** Does not match at this position, at the provided length.
		 */
		known_failure,
		/** Currently already in a match for this very thing.
		 *
		 *  Means that rule recursively calls itself (ex., a Kleene star).
		 */
		circular
	};

	/** @brief Note the beginning of a match attempt, and get previous information if there is any.
	 *
	 *  If we already have a match for seq at the current position, within the max limit, it is
	 *  automatically accepted (i.e., when the return type is `known_match`).
	 *
	 *  If we know the match is known to fail, it is automatically rejected.
	 */
	std::tuple< rematch_result, offset_type > start_match(sequence_id seq, offset_type curr, offset_type max);

	/** @brief Trivially match rule at the given position
	 *
	 *  Used when the parsing context is 0 bytes, but the rule is allowed to match the empty string
	 *
	 *  @todo Require the derivation for the rule that produces the empty string
	 */
	void note_empty_match(sequence_id seq, offset_type curr);

	/** @brief Note that the attempted match has been successful
	 */
	void accept_match(offset_type end);

	/** @brief Note that the attempted match has failed
	 *
	 *  Internally, the match is marked as failing up to this length
	 */
	void reject_match();

	/** @brief Reset internal data for a new parse
	 */
	void clear();

	offset_type accept_alt();

	void debug_check() const
	{
		if ( !parents.empty() )
		{
			throw std::logic_error("Should be empty");
		}
	}

	/// @todo Smarter/faster. Iterate over open_to_close
	/// @todo Avoid grammar
	bool find_preliminary_match(const std::map< offset_type, offset_type >& open_to_close);

	/// @todo Rename. Works for all keys
	parse_match get_rematch_info();

	// Had marked a successful match, but it isn't actually successful
	parse_match forget();
	void unaccept();
	bool not_empty() const;

	/// @todo Shouldn't need max and start here
	/// @todo The whole result should be returned in derivation, and derivation should simply
	/// be an interface that makes querying NonTerminals easy
	template< typename Terminal, typename NonTerminal >
	derivation< NonTerminal > make_derivation(
		const jl::grammar::context_free_grammar< Terminal, NonTerminal >& g,
		offset_type                                                       max,
		NonTerminal                                                       start
	) const
	{
		derivation< NonTerminal > d;

		const auto p = d.insert(derivation< NonTerminal >::INVALID, start, 0, max);

		make_derivation_inner(g, d, p, root);

		return d;
	}
private:
	/// @todo first should be the "most important" thing to match. We may be spending time trying to
	/// reduce mid for no real purpose
	// Preliminary if marked as successful match, but it's child isn't
	bool find_preliminary_match_inner(std::map< offset_type, offset_type >::const_iterator first, const std::map< offset_type, offset_type >::const_iterator last, DirectedGraph< parse_match >::vertex_iterator it);

	struct successful_match_result
	{
		/* The longest length (at this offset) where the rule has been matched. Any
		 * longer than this, and we may want to expand the search
		 */
		offset_type max;
	};

	template< typename Terminal, typename NonTerminal >
	void make_derivation_inner(
		const jl::grammar::context_free_grammar< Terminal, NonTerminal >& g,
		derivation< NonTerminal >&                                        d,
		typename derivation< NonTerminal >::key_type                      parent,
		DirectedGraph< parse_match >::const_vertex_iterator               it
	) const
	{
		// go through all children of der/s. If sequence, create a similar
		const typename DirectedGraph< parse_match >::size_type n = it.child_count();

		for (typename tree< parse_match >::size_type i = 0; i < n; ++i)
		{
			const auto jt = it.child(i);

			auto newp = parent;

			const auto& node = *jt;

			if ( const auto nt = g.to_nonterminal(node.rule) )
			{
				// create a node in the derivation for this
				newp = d.insert(parent, *nt, node.offset, node.end);
			}

			make_derivation_inner(g, d, newp, jt);
		}
	}

	bool auto_fail(sequence_id seq, offset_type curr, offset_type max) const;

	struct KnownMatchSorter
	{
		using is_transparent = parse_match;

		bool operator()(
			const parse_match& l,
			const parse_match& r
		) const
		{
			if ( l.offset < r.offset )
			{
				return true;
			}

			if ( r.offset < l.offset )
			{
				return false;
			}

			if ( l.rule < r.rule )
			{
				return true;
			}

			if ( r.rule < l.rule )
			{
				return false;
			}

			if ( l.end < r.end )
			{
				return true;
			}

			if ( r.end < l.end )
			{
				return false;
			}

			return l.max < r.max;
		}

		bool operator()(
			const DirectedGraph< parse_match >::vertex_iterator& l,
			const DirectedGraph< parse_match >::vertex_iterator& r
		) const
		{
			return this->operator()(* l, * r);
		}

		bool operator()(
			const DirectedGraph< parse_match >::vertex_iterator& l,
			const parse_match&                                   r
		) const
		{
			return this->operator()(* l, r);
		}

		bool operator()(
			const parse_match&                                   l,
			const DirectedGraph< parse_match >::vertex_iterator& r
		) const
		{
			return this->operator()(l, * r);
		}

	};

	/* For a given position, for each sequence, previous matching results
	 */
	std::set< DirectedGraph< parse_match >::vertex_iterator, KnownMatchSorter > known_matches;

	// anything <= this offset will be a failure
	std::map< offset_type, std::unordered_map< sequence_id, offset_type > > known_failures;

	// The (currently) successful derivation
	DirectedGraph< parse_match > der;

	std::vector< DirectedGraph< parse_match >::vertex_iterator > parents;

	DirectedGraph< parse_match >::vertex_iterator root;
};

}

#endif // HISTORY_H
