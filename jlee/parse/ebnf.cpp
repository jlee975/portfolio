#include "ebnf.h"

#include <string>

#include "derivation.h"
#include "grammar/context_free_grammar.h"
#include "parse.h"

#include "ebnf_private.h"

namespace details
{
namespace
{
std::string to_string(ebnfnt i)
{
	switch ( i )
	{
	case RULE:
		return "RULE";

	case SYMBOL:
		return "SYMBOL";

	case S0:
		return "S0";

	case S1:
		return "S1";

	case ATOMIC:
		return "ATOMIC";

	case EXPRESSION:
		return "EXPRESSION";

	case ESCAPECHAR:
		return "ESCAPECHAR";

	case ESCAPESEQ:
		return "ESCAPESEQ";

	case LITERAL:
		return "LITERAL";

	case HEX:
		return "HEX";

	case CHARACTER_CLASS:
		return "CHARACTERCLASS";

	case RANGE:
		return "RANGE";

	case RANGECHAR:
		return "RANGECHAR";

	case PAREN:
		return "PAREN";

	case OPTIONAL:
		return "OPTIONAL";

	case STAR:
		return "STAR";

	case PLUS:
		return "PLUS";

	case SUB:
		return "SUB";

	case SUBTERM:
		return "SUBTERM";

	case ALT:
		return "ALT";

	case ALTTERM:
		return "ALTTERM";

	case ALTTAIL:
		return "ALTTAIL";

	case CAT:
		return "CAT";

	case CATTERM:
		return "CATTERM";

	case CATTAIL:
		return "CATTAIL";

	case S:
		return "S";

	case ANY:
		return "ANY";

	case CATTERMNEEDSSPACE:
		return "CATTERMNEEDSSPACE";

	case CATTAILNEEDSSPACE:
		return "CATTAILNEEDSSPACE";
	} // switch

	return {};
}

}

const jl::grammar::context_free_grammar< char32_t, ebnfnt >& ebnfgrammar()
{
	/// @todo SYMBOL should allow pretty much any letters from unicode. XML tagname might be a good choice
	/// @todo STAR, PLUS, OPTIONAL, ATOMIC -> SUBTERM
	/// @todo ALTTERM -> CATTAIL
	/// @todo CATTAIL -> { S1, CATTERM }; CAT -> { CATTERM, CATTAIL+ }
	/// @todo Might allow a '-' as the first character of a character class
	/// @todo Use actual unicode code points for most characters. Or decide if this should be portable to other character sets
	static const jl::grammar::context_free_grammar< char32_t, ebnfnt > g = {
		{ ANY, {{ ustring(U"[#x0-#x10ffff]") }}},
		{ ESCAPECHAR, {{ ustring(U"[\\#\\\\\\-\\]\\^]") }}},
		{ SYMBOL, {{ ustring(U"[A-Za-z_]"), { ustring(U"[A-Za-z_0-9]"), '*' }}}},
		{ S, {{ ustring(U"[#x9#xA#xD#x20]") }}},
		{ S0, {{{ S, '*' }}}},
		{ S1, {{ S, S0 }}},

		{ HEX, {{ ustring(U"'#x'"), { ustring(U"[0-9A-Fa-f]"), '+' }}}},

		{ ESCAPESEQ, {{ ustring(U"'\\\\'"), ESCAPECHAR }}},

		{ LITERAL,
		  {{ ustring(U"'\"'"), { ustring(U"[^\"]"), '*' }, ustring(U"'\"'") },
			  { ustring(U"'\\''"), { ustring(U"[^']"), '*' }, ustring(U"'\\''") }}},

		{ RULE, {{ S0, SYMBOL, S0, ustring(U"'::='"), S0, EXPRESSION, S0 }}},
		{ RANGECHAR,
		  {{ ustring(U"[#x0-#x22#x24-#x2c#x2e-#x5b#x5f-#x10ffff]") },
			  { HEX },
			  { ESCAPESEQ }}}, // anything but #, -, \, ], or ^
		{ RANGE, {{ RANGECHAR }, { RANGECHAR, ustring(U"'-'"), RANGECHAR }}},
		{ CHARACTER_CLASS, {{ ustring(U"'['"), { ustring(U"'^'"), '?' }, { RANGE, '+' }, ustring(U"']'") }}},
		{ ATOMIC, {{ LITERAL }, { HEX }, { CHARACTER_CLASS }, { SYMBOL }, { PAREN }}},
		{ OPTIONAL, {{ ATOMIC, S0, ustring(U"'?'") }}},
		{ STAR, {{ ATOMIC, S0, ustring(U"'*'") }}},
		{ PLUS, {{ ATOMIC, S0, ustring(U"'+'") }}},
		{ SUBTERM, {{ STAR }, { PLUS }, { OPTIONAL }, { ATOMIC }}},
		{ SUB, {{ SUBTERM, S0, ustring(U"'-'"), S0, SUBTERM }}},

		{ CATTERM, {{ SUB }, { STAR }, { PLUS }, { OPTIONAL }, { ATOMIC }}},
		{ CATTAIL, {{ CAT }, { CATTERM }}},

		// A cat term will need a space if it *doesn't* end in *,+,?,],),', or "
		{ CATTERMNEEDSSPACE, {{{ ANY, '*' }, ustring(U"[^*+?\\])'\"]") }}},

		// A cat tail will need a space if it *doesn't* begin with #,[,(,', or "
		{ CATTAILNEEDSSPACE, {{ ustring(U"[^\\#[('\"]"), { ANY, '*' }}}},

		{ CAT,
		  {{ CATTERM, S1, CATTAIL },
			  {{ CATTERM, 0, CATTERMNEEDSSPACE, '-' }, CATTAIL },
			  { CATTERM, { CATTAIL, 0, CATTAILNEEDSSPACE, '-' }}}},

		{ ALTTERM, {{ CAT }, { SUB }, { STAR }, { PLUS }, { OPTIONAL }, { ATOMIC }}},
		{ ALTTAIL, {{ S0, ustring(U"'|'"), S0, ALTTERM }}},
		{ ALT, {{ ALTTERM, { ALTTAIL, '+' }}}},

		{ EXPRESSION, {{ ALT }, { CAT }, { SUB }, { STAR }, { PLUS }, { OPTIONAL }, { ATOMIC }}},
		{ PAREN, {{ ustring(U"'('"), S0, EXPRESSION, S0, ustring(U"')'") }}}, };

	return g;
}

/// @todo Move to a more generic namespace
struct bad_parse_exception
{
};

/** @todo This is a fairly thin wrapper around derivation. Wondering of some of the member
 *  functions can just be free functions taking derivation arguments
 */
class Rule
{
public:
	Rule(ustring s_)
		: s(std::move(s_) ), d(parse(s, details::RULE, ebnfgrammar() ) )
	{
		if ( d.empty() )
		{
			throw bad_parse_exception{};
		}
	}

	derivation< ebnfnt >::size_type child_count(derivation< ebnfnt >::key_type k) const
	{
		return d.child_count(k);
	}

	derivation< ebnfnt >::key_type child(
		derivation< ebnfnt >::key_type  k,
		derivation< ebnfnt >::size_type i
	) const
	{
		return d.child(k, i);
	}

	derivation< ebnfnt >::const_reference at(derivation< ebnfnt >::key_type k) const
	{
		return d.at(k);
	}

	/// @todo Return string_view
	ustring text(derivation< ebnfnt >::key_type k) const
	{
		const auto& x = d.at(k);

		return s.substr(x.offset, x.end - x.offset);
	}

	std::pair< ustring, derivation< ebnfnt >::key_type > basic() const
	{
		const auto [symbol, exp] = find_two(d.child(0), SYMBOL, EXPRESSION);

		return { text(symbol), exp };
	}

	std::pair< derivation< ebnfnt >::key_type, derivation< ebnfnt >::key_type > find_two(
		derivation< ebnfnt >::key_type k,
		ebnfnt                         r1,
		ebnfnt                         r2
	) const
	{
		derivation< ebnfnt >::key_type j1 = derivation< ebnfnt >::INVALID;
		derivation< ebnfnt >::key_type j2 = derivation< ebnfnt >::INVALID;

		for (std::size_t i = 0, n = d.child_count(k); i < n; ++i)
		{
			const auto j = d.child(k, i);

			const auto r = d.at(j).rule;

			if ( r == r1 && j1 == derivation< ebnfnt >::INVALID )
			{
				j1 = j;
			}
			else if ( r == r2 && j2 == derivation< ebnfnt >::INVALID )
			{
				j2 = j;
				break;
			}
		}

		if ( j1 == derivation< ebnfnt >::INVALID || j2 == derivation< ebnfnt >::INVALID )
		{
			throw std::logic_error("Bad parse");
		}

		return { j1, j2 };
	}

	derivation< ebnfnt >::key_type find_one(
		derivation< ebnfnt >::key_type k,
		ebnfnt                         r
	) const
	{
		// ATOMIC
		for (std::size_t i = 0, n = d.child_count(k); i < n; ++i)
		{
			const auto j = d.child(k, i);

			if ( d.at(j).rule == r )
			{
				return j;
			}
		}

		throw std::logic_error("Bad parse");
	}
private:
	ustring              s;
	derivation< ebnfnt > d;
};

class make_context_free_grammar
{
public:
	make_context_free_grammar() = default;

	using cfg             = jl::grammar::context_free_grammar< char32_t, int >;
	using production_rule = jl::grammar::production_rule< char32_t, int >;
	using production_set  = jl::grammar::production_set< char32_t, int >;

	/** @todo Creating a lot of nonterminals, because context free grammar has a limit to the depth that
	 * it will accept for initializers. This is blocking some optimizations, because we must keep
	 * nonterminals
	 */
	std::pair< cfg, std::map< ustring, int > > operator()(const std::vector< Rule >& ds)
	{
		// id to expressions
		std::map< int, std::vector< std::pair< std::size_t, derivation< ebnfnt >::key_type > > > exps;

		for (std::size_t i = 0; i < ds.size(); ++i)
		{
			const auto& r = ds[i];
			auto [t, exp] = r.basic();

			auto jt = ids.find(t);

			if ( jt == ids.end() )
			{
				ids.emplace(std::move(t), nextid);
				exps[nextid].emplace_back(i, exp);
				getnextid();
			}
			else
			{
				exps[jt->second].emplace_back(i, exp);
			}
		}

		production_set productions;

		// revisit all and parse the rhs of the rule
		for (const auto& [i, v] : exps)
		{
			production_rule rule = { i, {}};

			for (const auto& [di, k] : v)
			{
				rule.substitutions.push_back({ add_expression(ds[di], k, productions) });
			}

			productions.push_back(rule);
		}

		// find top level alt terms
		return { cfg{ productions }, ids };
	}
private:
	std::pair< int, int > add_subexpressions(
		const Rule&                    d,
		derivation< ebnfnt >::key_type k,
		production_set&                productions,
		ebnfnt                         r1,
		ebnfnt                         r2
	)
	{
		const auto [j1, j2] = d.find_two(k, r1, r2);

		const auto first  = add_expression(d, j1, productions);
		const auto second = add_expression(d, j2, productions);

		return std::make_pair(first, second);
	}

	int add_subexpression(
		const Rule&                    d,
		derivation< ebnfnt >::key_type k,
		production_set&                productions,
		const ebnfnt                   r1
	)
	{
		return add_expression(d, d.find_one(k, r1), productions);
	}

	int add_atomic(
		const Rule&                    d,
		derivation< ebnfnt >::key_type k,
		production_set&                productions,
		const char                     c
	)
	{
		const auto first = add_subexpression(d, k, productions, ATOMIC);

		productions.push_back({ nextid, {{{ first, c }}}});
		return getnextid();
	}

	int add_expression(
		const Rule&                    d,
		derivation< ebnfnt >::key_type k,
		production_set&                productions
	)
	{
		const auto& sub = d.at(k);

		// should have one child -- alt, cat, sub, star, plus, optional, atomic
		switch ( sub.rule )
		{
		case ALT:
			// get ALTTERM and ALTTAIL
			/// @todo Handle ALTTERMS that might be ALTs in parens
		{
			production_rule rule = { nextid, {}};

			for (std::size_t i = 0, n = d.child_count(k); i < n; ++i)
			{
				const auto j = d.child(k, i);

				const auto r = d.at(j).rule;

				if ( r == ALTTERM || r == ALTTAIL )
				{
					rule.substitutions.push_back({{ add_expression(d, j, productions) }});
				}
			}

			productions.push_back(std::move(rule) );
			return getnextid();
		}
		case ALTTAIL:
			return add_subexpression(d, k, productions, ALTTERM);

		case ALTTERM:
		case CATTERM:
		case CATTAIL:
		case SUBTERM:
		case EXPRESSION:
		case ATOMIC:
			// Just forward to child
			return add_expression(d, d.child(k, 0), productions);

		case CAT:
			// get CATTERM and CATTAIL
		{
			const auto [first, second] = add_subexpressions(d, k, productions, CATTERM, CATTAIL);

			productions.push_back({ nextid, {{ first, second }}});
			return getnextid();
		}
		case SUB:
			// SUBTERM + SUBTERM
		{
			const auto [first, second] = add_subexpressions(d, k, productions, SUBTERM, SUBTERM);

			productions.push_back({ nextid, {{{ first, 0, second, '-' }}}});
			return getnextid();
		}
		case STAR:
			return add_atomic(d, k, productions, '*');

		case PLUS:
			return add_atomic(d, k, productions, '+');

		case OPTIONAL:
			return add_atomic(d, k, productions, '?');

		case LITERAL:
		case HEX:
		case CHARACTER_CLASS:
			productions.push_back(production_rule{ nextid, {{ d.text(k) }}});
			return getnextid();

		case SYMBOL:
		{
			auto it = ids.find(d.text(k) );

			if ( it == ids.end() )
			{
				throw std::runtime_error("Unrecognized rule");
			}

			return it->second;
		}
		case PAREN:
			// Forward to expression
			return add_subexpression(d, k, productions, EXPRESSION);
		} // switch

		throw std::logic_error("Not implemented");
	}

	int getnextid()
	{
		return nextid++;
	}

	// (rulename) -> id
	std::map< ustring, int > ids;

	int nextid{ 0 };
};

}

/// @todo Already have the ustring in v, why copy it to ds?
std::pair< jl::grammar::context_free_grammar< char32_t, int >, std::map< ustring, int > > ebnf_to_cfg(const std::vector< ustring >& v)
{
	// derivations
	std::vector< details::Rule > ds;

	for (std::size_t i = 0, n = v.size(); i != n; ++i)
	{
		try
		{
			ds.emplace_back(v[i]);
		}
		catch ( details::bad_parse_exception& )
		{
			return {{}, {}};
		}
	}

	details::make_context_free_grammar maker;

	return maker(ds);
}
