#ifndef EBNF_PRIVATE_H
#define EBNF_PRIVATE_H

namespace details
{
enum ebnfnt
{
	RULE,              // 0
	SYMBOL,            // 1
	S0,                // 2
	S1,                // 3
	ATOMIC,            // 4
	EXPRESSION,        // 5
	ESCAPECHAR,        // 6
	ESCAPESEQ,         // 7
	LITERAL,           // 8
	HEX,               // 9
	CHARACTER_CLASS,   // 10
	RANGE,             // 11
	RANGECHAR,         // 12
	PAREN,             // 13
	OPTIONAL,          // 14
	STAR,              // 15
	PLUS,              // 16
	SUB,               // 17
	SUBTERM,           // 18
	ALT,               // 19
	ALTTERM,           // 20
	ALTTAIL,           // 21
	CAT,               // 22
	CATTERM,           // 23
	CATTAIL,           // 24
	S,                 // 25
	ANY,               // 26
	CATTERMNEEDSSPACE, // 27
	CATTAILNEEDSSPACE  // 28
};
}
#endif // EBNF_PRIVATE_H
