#include "mmap.h"

#include <algorithm>

#if ( !defined( _WIN32 ) && ( defined( __unix__ ) || defined( __unix ) || ( defined( __APPLE__ ) && defined( __MACH__ ) ) ) )
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#endif

memory_map::memory_map() = default;

/**
 * @todo Windows support. And wrap *nix in #if's
 * @todo Redesign to access through operator[]
 */
memory_map::memory_map(const char* filename)
	: memory_map()
{
	open(filename);
}

memory_map::memory_map(const std::string& s)
	: memory_map(s.c_str() )
{
}

memory_map::memory_map(const std::filesystem::path& s)
	: memory_map(s.c_str() )
{
}

memory_map::memory_map(memory_map&& o) noexcept
	: ptr(o.ptr), fd(o.fd), nbytes(o.nbytes)
{
	o.ptr    = nullptr;
	o.fd     = -1;
	o.nbytes = 0;
}

memory_map::~memory_map()
{
	close();
}

memory_map& memory_map::operator=(memory_map&& o) noexcept
{
	swap(o);
	o.close();
	return *this;
}

void memory_map::swap(memory_map& o)
{
	std::swap(ptr, o.ptr);
	std::swap(fd, o.fd);
	std::swap(nbytes, o.nbytes);
}

bool memory_map::open(const std::string& filename)
{
	return this->open(filename.c_str() );
}

bool memory_map::open(const std::filesystem::path& filename)
{
	return this->open(filename.c_str() );
}

bool memory_map::open(const char* filename)
{
	if ( is_open() )
	{
		return false;
	}

	int filed = ::open(filename, O_RDONLY);

	if ( filed != -1 )
	{
		struct stat buf;

		if ( ::fstat(filed, &buf) == 0 )
		{
			const std::size_t n = buf.st_size;

			void* p = ::mmap(nullptr, n, PROT_READ, MAP_PRIVATE, filed, 0);

			if ( p != MAP_FAILED )
			{
				ptr    = static_cast< std::byte* >( p );
				nbytes = n;
				fd     = filed;

				posix_madvise(p, n, POSIX_MADV_SEQUENTIAL);

				return true;
			}
		}

		::close(filed);
	}

	return false;
}

void memory_map::close()
{
	if ( ptr != nullptr )
	{
		::munmap(ptr, nbytes);
		ptr = nullptr;
	}

	if ( fd != -1 )
	{
		::close(fd);
		fd = -1;
	}

	nbytes = 0;
}

bool memory_map::is_open() const
{
	return ptr != nullptr;
}

const std::byte* memory_map::data() const
{
	return ptr;
}

const std::byte& memory_map::operator[](size_type i) const
{
	return ptr[i];
}

memory_map::size_type memory_map::size() const
{
	return nbytes;
}
