#ifndef JL_MEMORYMAP_MMAP_HPP
#define JL_MEMORYMAP_MMAP_HPP

#include <cstddef>
#include <filesystem>
#include <string>

/** @brief Map a file's contents to memory
 *
 * This class provides a read-only view of a file, as if it were read into memory.
 *
 * @todo const_memory_map that will let us load files into memory even if not backed by mmap. Since
 * it is const, the user should have no expectation of altering the file on disk
 */
class memory_map
{
public:
	using size_type = std::size_t;

	/** @brief An empty memory map
	 *
	 * size() will return 0. data() will return nullptr.
	 */
	memory_map();

	/** @brief Map the given file
	 * @param filename Path to file
	 */
	explicit memory_map(const char* filename);

	/** @brief Map the given file
	 * @param filename Path to file
	 */
	explicit memory_map(const std::string& filename);

	explicit memory_map(const std::filesystem::path&);

	memory_map(const memory_map&) = delete;

	/** @brief Move constructor
	 */
	memory_map(memory_map&&) noexcept;

	/** @brief Clean up allocated resources
	 */
	~memory_map();

	memory_map& operator=(const memory_map&) = delete;

	/** @brief Move assignment
	 */
	memory_map& operator=(memory_map&&) noexcept;

	/** @brief Swap
	 */
	void swap(memory_map&);

	/** @brief Map the given file
	 * @returns true if the file was successfully mapped
	 * @returns false if the file was not mapped, or a file is already mapped
	 */
	bool open(const char*);

	/** @brief Overload of open(const char*)
	 */
	bool open(const std::string&);

	bool open(const std::filesystem::path&);

	/** @brief Release the resources and end the mapping
	 *
	 * Invalidates any pointer previously returned by data()
	 */
	void close();

	/** @brief Number of bytes available in mapping
	 */
	size_type size() const;

	/** @brief Check if a file is mapped
	 */
	bool is_open() const;

	/** @brief Direct access to the file, as mapped into memory
	 */
	const std::byte* data() const;

	/** @brief Array-like access to file data
	 *
	 * This is an unchecked access. Trying to access past the end of the mapped area will be undefined behavior.
	 */
	const std::byte& operator[](size_type) const;
private:
	/// Pointer to mapped file data, or nullptr
	std::byte* ptr{ nullptr };

	/// File handle for file being mapped, or -1
	int fd{ -1 };

	/// Size of the mapping, or 0
	size_type nbytes{ 0 };
};
#endif // ifndef MMAP_HPP
