#ifndef ZIP_HPP
#define ZIP_HPP

#include <fstream>
#include <string>
#include <vector>

class LocalFileHeader; // forward declaration

class Zip
{
public:
	using size_type = std::vector< LocalFileHeader >::size_type;

	enum Error
	{
		OK,
		ErrorPointer,
		ErrorNotFound,
		ErrorOpen,
		ErrorCreate,
		ErrorRead,
		ErrorWrite,
		ErrorSeek,
		ErrorNotOpen,
		ErrorFeature,
		ErrorLocalSignature
	};

	enum CompressionMethod
	{
		Store = 0,
		Shrink = 1,
		Reduce1 = 2,
		Reduce2 = 3,
		Reduce3 = 4,
		Reduce4 = 5,
		Implode = 6,
		Tokenize = 7,
		Deflate = 8,
		Deflate64 = 9,
		DCLI = 10,
		BZip2 = 12,
		LZMA = 14,
		Terse = 18,
		LZ77 = 19,
		WavPack = 97,
		PPMD = 98,
		Unknown = 1000
	};

	Zip();
	explicit Zip(const char*);
	~Zip();
	Error open(const char*);
	Error close();

	std::vector< std::byte > extract(const char*);
	std::vector< std::byte > extract(size_type);

	Error addFile(const char*);
	Error addData(const char*, const std::byte*, unsigned long, time_t);

	LocalFileHeader operator[](size_type) const;

	size_type find(const char*) const;
	size_type size() const;
private:
	std::fstream                   file;
	std::vector< LocalFileHeader > records;
	std::streampos                 iCD;

	static const char emptyzip[22];
};

class LocalFileHeader
{
public:
	LocalFileHeader();
	LocalFileHeader(const std::byte*, unsigned long);
	LocalFileHeader(const LocalFileHeader&);
	~LocalFileHeader();

	LocalFileHeader& operator=(const LocalFileHeader&);

	static bool isLocal(const std::byte*);
	static std::streamsize size(const std::byte*);

	void local_off(std::streampos);
	void filename(std::string);
	void extraLen(unsigned);
	void compressedSize(unsigned long);
	void uncompressedSize(unsigned long);
	void compressionMethod(Zip::CompressionMethod);
	void crc(unsigned long);

	std::streampos data_off() const;
	std::streampos local_off() const;
	std::string filename() const;
	unsigned extraLen() const;
	unsigned long compressedSize() const;
	unsigned long uncompressedSize() const;
	Zip::CompressionMethod compressionMethod() const;
	unsigned long crc() const;
private:
	std::streampos         local_off_;
	std::string            filename_;
	unsigned               extraLen_{ 0 };
	unsigned long          csize{ 0 }; // compressed size
	unsigned long          osize{ 0 }; // original, uncompressed size
	Zip::CompressionMethod method{ Zip::Unknown };
	unsigned long          crc_{ 0 };
	unsigned               flags{ 0 };
};

#endif /* #ifndef ZIP_HPP */
