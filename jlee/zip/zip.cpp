// TODO: Shrink, implode, bz2, lzma
// http://www.pkware.com/documents/casestudies/APPNOTE.TXT
// FIXME: Extensive error checking, esp. on i/o
#include "zip.h"

#include <cstring>
#include <ctime>
#include <stdexcept>
#include <sys/stat.h>

#include <iostream>
#include <utility>

#include "bitstream/lebitstream.h"
#include "crc/crc.h"
#include "deflate/deflate.h"
#include "endian/endian.h"

static unsigned long UnixToMSDOSTime(time_t);

#define LOCAL_HEADER_SIZE             30
#define FILE_HEADER_SIZE              46

#define ZIP_SIG_LOCAL_FILE            UINT32_C(0x04034b50)
#define ZIP_SIG_FILE                  UINT32_C(0x02014b50)
#define ZIP_SIG_END_OF_CD             UINT32_C(0x06054b50)

#define ZIP_FLAG_ENCRYPTED            1
#define ZIP_FLAG_IMPLODING_8K         2
#define ZIP_FLAG_IMPLODING_SF3        4
#define ZIP_FLAG_DEFLATE_METHOD       6
#define ZIP_DEFLATE_METHOD_NORMAL     0
#define ZIP_DEFLATE_METHOD_MAX        2
#define ZIP_DEFLATE_METHOD_FAST       4
#define ZIP_DEFLATE_METHOD_SUPER_FAST 6
#define ZIP_FLAG_LZMA_EOS             2
#define ZIP_FLAG_DATA_DESCRIPTOR      8
#define ZIP_FLAG_PATCHED              DATA 32
#define ZIP_FLAG_STRONG_ENCRYPTION    64
#define ZIP_FLAG_UTF8                 2048
#define ZIP_FLAG_LOCAL_HEADER_MASKED  8192

LocalFileHeader::LocalFileHeader()
	: local_off_(0)
{
}

LocalFileHeader::LocalFileHeader(
	const std::byte* h,
	unsigned long
)
	: local_off_(0), extraLen_(0), csize(0), osize(0), method(Zip::Unknown), crc_(0), flags(0)
{
	if ( h == nullptr )
	{
		throw std::runtime_error("Unexpected null pointer passed to "
			"constructor");
	}

	if ( leread< 32 >(h) == ZIP_SIG_LOCAL_FILE )
	{
		flags = leread< 16 >(h + 6);

		switch ( leread< 16 >(h + 8) )
		{
		case Zip::Store:
			method = Zip::Store;
			break;
		case Zip::Shrink:
			method = Zip::Shrink;
			break;
		case Zip::Reduce1:
			method = Zip::Reduce1;
			break;
		case Zip::Reduce2:
			method = Zip::Reduce2;
			break;
		case Zip::Reduce3:
			method = Zip::Reduce3;
			break;
		case Zip::Reduce4:
			method = Zip::Reduce4;
			break;
		case Zip::Implode:
			method = Zip::Implode;
			break;
		case Zip::Tokenize:
			method = Zip::Tokenize;
			break;
		case Zip::Deflate:
			method = Zip::Deflate;
			break;
		case Zip::Deflate64:
			method = Zip::Deflate64;
			break;
		case Zip::DCLI:
			method = Zip::DCLI;
			break;
		case Zip::BZip2:
			method = Zip::BZip2;
			break;
		case Zip::LZMA:
			method = Zip::LZMA;
			break;
		case Zip::Terse:
			method = Zip::Terse;
			break;
		case Zip::LZ77:
			method = Zip::LZ77;
			break;
		case Zip::WavPack:
			method = Zip::WavPack;
			break;
		case Zip::PPMD:
			method = Zip::PPMD;
			break;
		default:
			method = Zip::Unknown;
		} // switch

		crc_  = leread< 32 >(h + 16);
		csize = leread< 32 >(h + 18);
		osize = leread< 32 >(h + 22);
		filename_.assign(reinterpret_cast< const char* >( h + LOCAL_HEADER_SIZE ), leread< 16 >(h + 26) );
		extraLen_ = leread< 16 >(h + 28);
	} // TODO? else throw

}

LocalFileHeader::LocalFileHeader(const LocalFileHeader& f)
{
	operator=(f);
}

LocalFileHeader::~LocalFileHeader() = default;

LocalFileHeader& LocalFileHeader::operator=(const LocalFileHeader& f) = default;

bool LocalFileHeader::isLocal(const std::byte* h)
{
	return h != nullptr ? ( leread< 32 >(h) == ZIP_SIG_LOCAL_FILE ) : false;
}

std::streamsize LocalFileHeader::size(const std::byte* h)
{
	return h != nullptr ? ( leread< 16 >(h + 26) + leread< 16 >(h + 28) + LOCAL_HEADER_SIZE ) : 0;
}

// Offset of the compressed data
std::streampos LocalFileHeader::data_off() const
{
	return local_off_ + std::streamoff(LOCAL_HEADER_SIZE + filename_.size() + extraLen_);
}

void LocalFileHeader::local_off(std::streampos x)
{
	local_off_ = x;
}

void LocalFileHeader::filename(std::string s)
{
	filename_ = std::move(s);
}

void LocalFileHeader::extraLen(unsigned n)
{
	extraLen_ = n;
}

void LocalFileHeader::compressedSize(unsigned long n)
{
	csize = n;
}

void LocalFileHeader::uncompressedSize(unsigned long n)
{
	osize = n;
}

void LocalFileHeader::compressionMethod(Zip::CompressionMethod m)
{
	method = m;
}

void LocalFileHeader::crc(unsigned long c)
{
	crc_ = c;
}

std::streampos LocalFileHeader::local_off() const
{
	return local_off_;
}

std::string LocalFileHeader::filename() const
{
	return filename_;
}

unsigned LocalFileHeader::extraLen() const
{
	return extraLen_;
}

unsigned long LocalFileHeader::compressedSize() const
{
	return csize;
}

unsigned long LocalFileHeader::uncompressedSize() const
{
	return osize;
}

Zip::CompressionMethod LocalFileHeader::compressionMethod() const
{
	return method;
}

unsigned long LocalFileHeader::crc() const
{
	return crc_;
}

// -----------------------------------------------------------------------------

const char Zip::emptyzip[22] = { 0x50, 0x4b, 0x05, 0x06, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

Zip::Zip()
	: iCD(0)
{
}

Zip::Zip(const char* filename)
	: iCD(0)
{
	open(filename);
}

Zip::~Zip()
{
	close();
}

// FIXME: If not a recognizable zip file, delete everything so later operations
// won't succeed (or try to succeed);
// TODO: !filename => make a temp file
Zip::Error Zip::open(const char* filename)
{
	if ( filename == nullptr )
	{
		return ErrorPointer;
	}

	close();

	// Try opening the named file
	file.open(filename, std::ios_base::in | std::ios_base::out | std::ios_base::binary);

	if ( !file.fail() ) // File exists and was opened successfully
	{
		std::byte      lfh[LOCAL_HEADER_SIZE]; // local file header
		std::streampos pos = 0;

		do // Get a list of all the files
		{
			std::vector< std::byte > buf;

			file.read(reinterpret_cast< char* >( lfh ), LOCAL_HEADER_SIZE);

			if ( file.gcount() != LOCAL_HEADER_SIZE )
			{
				return ErrorRead;
			}

			file.seekg(pos);

			if ( !LocalFileHeader::isLocal(lfh) )
			{
				break;
			}

			std::streamsize record_size = LocalFileHeader::size(lfh);
			buf.reserve(record_size);

			file.read(reinterpret_cast< char* >( buf.data() ), record_size);

			LocalFileHeader p(buf.data(), record_size);
			p.local_off(pos);
			records.push_back(p);

			pos += record_size + p.compressedSize();
			file.seekg(pos);
		}
		while ( true );

		file.seekg(0);
		iCD = pos;
	}
	else // Named file could not be opened -- create an empty zip file
	{
		file.open(filename,
			std::ios_base::in | std::ios_base::out | std::ios_base::binary | std::ios_base::trunc);

		if ( !file.fail() )
		{
			file.write(emptyzip, sizeof( emptyzip ) );

			if ( !file.bad() )
			{
				file.seekp(0);

				if ( !file.good() )
				{
					return ErrorSeek;
				}
			}
			else
			{
				return ErrorWrite;
			}
		}
		else
		{
			return ErrorCreate;
		}
	}

	return OK;
}

Zip::Error Zip::close()
{
	if ( file.is_open() )
	{
		file.close();
	}

	records.clear();
	iCD = 0;

	return OK;
}

// Extract data pointed to by LocalFileHeader
// TODO: Write to file and preserve attributes, etc.
std::vector< std::byte > Zip::extract(size_type i)
{
	if ( !file.is_open() )
	{
		throw std::runtime_error("Bleep");
	}

	const LocalFileHeader& q    = records.at(i);
	unsigned long          cbIn = q.compressedSize();
	// unsigned long cbOut = q.uncompressedSize();
	std::vector< std::byte > pIn(cbIn);
	std::vector< std::byte > pOut;

	file.seekg(q.data_off() );
	file.read(reinterpret_cast< char* >( &pIn[0] ), cbIn);
	file.seekg(0);

	if ( q.compressionMethod() == Zip::Deflate )
	{
		ILEBitstream bs;
		bs.open(pIn.data(), cbIn);
		deflate::Decompressor d;
		d.exec(bs);
		pOut = std::move(d).data();
	}
	else
	{
		throw std::runtime_error("Unsupported compression method.");
	}

	return pOut;
}

// Extract by filename
std::vector< std::byte > Zip::extract(const char* filename)
{
	std::vector< std::byte > data;

	size_type i = find(filename);

	if ( i < records.size() )
	{
		data = extract(i);
	}
	else
	{
		throw std::runtime_error("File not found for extraction.");
	}

	return data;
}

// Find the record with the same filename
Zip::size_type Zip::find(const char* filename) const
{
	size_type n = records.size();

	if ( filename != nullptr )
	{
		size_type i = 0;

		while ( i < n && records[i].filename().compare(filename) != 0 )
		{
			++i;
		}

		n = i;
	}

	return n;
}

Zip::size_type Zip::size() const
{
	return records.size();
}

LocalFileHeader Zip::operator[](size_type i) const
{
	return records[i];
}

// FIXME: If we add a file we ought to update "records"
Zip::Error Zip::addData(
	const char*      filename,
	const std::byte* pIn,
	unsigned long    insize,
	time_t           modtime
)
{
	if ( ( filename == nullptr ) || ( pIn == nullptr ) )
	{
		return ErrorPointer;
	}

	if ( !file.is_open() )
	{
		return ErrorNotOpen;
	}

	file.seekg(0, std::ios_base::end);
	std::streampos zipsize = file.tellg();

	file.seekg(iCD);

	unsigned long nfilename = strlen(filename);
	unsigned long filecrc   = chest::crc32(pIn, insize, chest::crc::zip, chest::crc::flags::zip);

	auto nbytes = std::streamsize(zipsize - iCD);

	nbytes += insize + 20         // for the deflated data
	          + LOCAL_HEADER_SIZE // size of the local file header
	          + FILE_HEADER_SIZE  // for the file header in the CD
	          + 2 * nfilename;    // for the file name field in the LFH and CD

	// Compress the file ---------------------------------------------------
	// FIXME: Check return value of Compress
	auto*               pOut = new std::byte[nbytes];
	deflate::Compressor d;
	auto                csize = static_cast< unsigned long >( nbytes - ( LOCAL_HEADER_SIZE + nfilename ) );

	d.Compress(pIn, insize, pOut + ( LOCAL_HEADER_SIZE + nfilename ), &csize);

	// Read the central directory information
	std::streamsize j = LOCAL_HEADER_SIZE + nfilename + csize;

	file.read(reinterpret_cast< char* >( pOut + j ), std::streamsize(zipsize - iCD) );

	while ( leread< 32 >(pOut + j) == ZIP_SIG_FILE )
	{
		j += FILE_HEADER_SIZE + leread< 16 >(pOut + j + 28) + leread< 16 >(pOut + j + 30)
		     + leread< 16 >(pOut + j + 32);
	}

	// Make room for a new central directory entry
	// TODO: Cut back on the total number of bytes moved since we're just
	// moving EVERYTHING
	for (std::streamsize k = nbytes - ( FILE_HEADER_SIZE + nfilename + 1 ); k >= j; k--)
	{
		pOut[k + FILE_HEADER_SIZE + nfilename] = pOut[k];
	}

	// Fill in the file header ---------------------------------------------
	unsigned long modtime_m = UnixToMSDOSTime(modtime);

	// TODO: get external attributes from st_mode of stat
	lewrite< 32 >(pOut + j, ZIP_SIG_FILE);                              // file header signature
	lewrite< 16 >(pOut + j + 4, 0x0314);                                // Version made by TODO: set by os
	lewrite< 16 >(pOut + j + 6, 20);                                    // Version needed to extract
	lewrite< 16 >(pOut + j + 8, 0);                                     // General purpose bit flag
	lewrite< 16 >(pOut + j + 10, Deflate);                              // Compression method
	lewrite< 32 >(pOut + j + 12, modtime_m);                            // Last modified time
	lewrite< 32 >(pOut + j + 16, filecrc);                              // CRC-32
	lewrite< 32 >(pOut + j + 20, csize);                                // Compressed size
	lewrite< 32 >(pOut + j + 24, insize);                               // Uncompressed size
	lewrite< 16 >(pOut + j + 28, nfilename);                            // File name field length
	lewrite< 16 >(pOut + j + 30, 0);                                    // Extra field length
	lewrite< 16 >(pOut + j + 32, 0);                                    // File comment length
	lewrite< 16 >(pOut + j + 34, 0);                                    // Disk number start
	lewrite< 16 >(pOut + j + 36, 0);                                    // Internal attributes
	lewrite< 32 >(pOut + j + 38, 0x81ed0000);                           // External attributes
	lewrite< 32 >(pOut + j + 42, static_cast< unsigned long >( iCD ) ); // Relative offset of file

	// Copy the relevant information to the local file header
	lewrite< 32 >(pOut, ZIP_SIG_LOCAL_FILE); // local header signature

	for (unsigned long k = 4; k < LOCAL_HEADER_SIZE; k++)
	{
		pOut[k] = pOut[j + 2 + k];
	}

	// Save the file name to both headers
	/// \todo don't like the cast here
	for (unsigned long k = 0; k < nfilename; k++)
	{
		pOut[j + FILE_HEADER_SIZE + k] = pOut[LOCAL_HEADER_SIZE + k] = std::byte(filename[k]);
	}

	// Update the central directory ----------------------------------------
	j += FILE_HEADER_SIZE + nfilename;

	if ( leread< 32 >(pOut + j) != ZIP_SIG_END_OF_CD )
	{
		return ErrorFeature;
	}

	// FIXME: Watch for overflow of 16 bit values
	unsigned      nentries;
	unsigned long cdsize;

	unsigned long cdoff;

	nentries = 1 + leread< 16 >(pOut + j + 8);
	lewrite< 16 >(pOut + j + 8, nentries); // no. of entries in the CD on this disk
	nentries = 1 + leread< 16 >(pOut + j + 10);
	lewrite< 16 >(pOut + j + 10, nentries); // no. of entries in the CD
	cdsize = FILE_HEADER_SIZE + nfilename + leread< 32 >(pOut + j + 12);
	lewrite< 32 >(pOut + j + 12, cdsize); // Size of the central directory
	cdoff = 30 + nfilename + csize + leread< 32 >(pOut + j + 16);
	lewrite< 32 >(pOut + j + 16, cdoff); // offset of the central directory

	file.seekp(iCD);
	file.write(reinterpret_cast< char* >( pOut ), nbytes - insize + csize);
	file.seekg(0);
	file.seekp(0);
	iCD += 30 + nfilename + csize;
	delete[] pOut;
	return OK;
}

// FIXME: Don't return without delete[]'ing
// TODO: If compressed size is greater than regular size, just store it
Zip::Error Zip::addFile(const char* filename)
{
	unsigned long insize;
	FILE*         fadd;
	struct stat   fileinfo;

	std::byte* pIn;

	if ( stat(filename, &fileinfo) != 0 )
	{
		return ErrorNotFound;
	}

	insize = static_cast< unsigned long >( fileinfo.st_size );

	if ( ( fadd = fopen(filename, "rb") ) == nullptr )
	{
		return ErrorOpen;
	}

	pIn = new std::byte[insize];

	if ( fread(pIn, 1, insize, fadd) < insize )
	{
		return ErrorRead;
	}

	addData(filename, pIn, insize, fileinfo.st_mtime);

	delete[] pIn;
	return OK;
}

/*
   Zip myzip;
   unsigned char justsomestuff[] = "ALKJDFJCLIEL";

   int main(int argc, char * argv[]) {
    myzip.New("bleh.zip");

    for (int i = 1; i < argc; i++)
        myzip.AddFile(argv[i]);
    myzip.AddData("Anythang.txt", justsomestuff, 12, 0);
    myzip.Close();
    return 0;
   }
   // */

// TODO: if year is less than 1980...
unsigned long UnixToMSDOSTime(time_t t // seconds since Unix epoch. Often cast from a time_t
)
{
	tm* u = localtime(&t);

	unsigned long msdostime = 0;

	if ( u->tm_year >= 80 ) // Earlier years are not representable
	{
		msdostime   = static_cast< unsigned long >( u->tm_year - 80 );
		msdostime <<= 4;
		msdostime  |= static_cast< unsigned long >( 1 + u->tm_mon );
		msdostime <<= 5;
		msdostime  |= static_cast< unsigned long >( u->tm_mday );
		msdostime <<= 5;
		msdostime  |= static_cast< unsigned long >( u->tm_hour );
		msdostime <<= 6;
		msdostime  |= static_cast< unsigned long >( u->tm_min );
		msdostime <<= 5;
		msdostime  |= static_cast< unsigned long >( ( u->tm_sec >> 1 ) & 31 );
	}

	return msdostime;
}
