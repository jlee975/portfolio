#ifndef IARCHIVE_H
#define IARCHIVE_H

#include <cstddef>
#include <vector>

enum class archive_error
{
	none,   ///< Operation was successful
	nodata, ///< Input is incomplete. Expected data, but didn't get it.
	derived ///< Error specific to archive format
};

/// @todo Support a set of functions for listing contents
class IArchive
{
public:
	/// The type that derived classes should use for custom errors
	using derived_error = unsigned long;

	/// @todo If there's an error, probably want to store additional information. Like where the error occurred
	/// in the stream, relevant CRC values, etc.
	/// @todo Should probably be a std::variant, or wrapped version thereof
	struct Result
	{
		archive_error ae;
		derived_error de;
		std::vector< std::byte > dataz;
	};

	virtual ~IArchive() = default;

	virtual Result extract(const std::byte*, std::size_t) const = 0;
	virtual Result archive(const std::byte*, std::size_t) const = 0;
};

#endif // IARCHIVE_H
