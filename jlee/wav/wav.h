#ifndef WAV_HPP
#define WAV_HPP

#include <iostream>

#include "audio/codec.h"
#include "audio/encapsulation.h"
#include "chirp/metadata.h"

class WavFrame
	: public audio::Encapsulation
{
	std::istream& is;

	/// @todo Probably should be audio::sample_off
	unsigned long isample;
	bool          in_data;
	unsigned      channels;
	unsigned      bits_per_sample;
	unsigned long nsamples;
public:
	explicit WavFrame(std::istream&);

	Packet ReadPacket() override;
	void prepare() override;

	audio::sample_off position() const override;
	audio::sample_off length() const override;
	void seek(audio::sample_off) override;
	bool eos() const override;
};

class Wav
	: public audio::Codec
{
	audio::sample_rate_type         sample_rate_;
	unsigned                        channels{ 0 };
	unsigned                        bits_per_sample{ 0 };
	std::vector< signed short int > signal;
public:
	Wav();
	void DecodePacket(const Packet&) override;
	samples_type DumpAudio() override;
	void seek() override;
	bool getMeta(MetaData&) const override;
	audio::sample_rate_type sample_rate() const override;
};

#endif // WAV_HPP
