/*=========================================================================*//**
*  \file        wav.cpp
*
*  \todo Incorporate BWF extensions for metadata, etc.
*//*==========================================================================*/

#include <climits>

#include "audio/packet.h"
#include "audio/samples.h"
#include "endian/endian.h"

#if ( CHAR_BIT != 8 )
#error "Sorry, this code only supports 8-bit chars"
#endif

#include "wav.h"

/***************************************************************************/ /**
 *  \todo istream error checking
 *
 *  Really RIFF
 *******************************************************************************/
WavFrame::WavFrame(std::istream& stream)
	: is(stream), isample(0), in_data(false), channels(0), bits_per_sample(0), nsamples(0)
{
	char buf[44];

	is.seekg(0);
	is.read(buf, 44);

	channels        = leread< 16 >(buf + 22);
	bits_per_sample = leread< 16 >(buf + 34);
	nsamples        = leread< 32 >(buf + 40) / ( channels * bits_per_sample / 8 );
}

/***************************************************************************/ /**
 *  \note When returning audio data, a fake RIFF 'data' chunk is returned, holding
 *     the data. About 1024 samples are returned.
 *  \todo Should verify this is a WAV in RIFF
 *  \todo Possibly some trouble with bits_per_sample and CHAR_BIT
 *******************************************************************************/
Packet WavFrame::ReadPacket()
{
	Packet data;

	if ( in_data ) // return 'data' subchunk
	{
		unsigned bytes_per_sample = channels * bits_per_sample / CHAR_BIT;

		data.resize(8 + 1024 * bytes_per_sample);
		data[0] = 'd', data[1] = 'a', data[2] = 't', data[3] = 'a';

		is.seekg(44 + isample * bytes_per_sample);
		is.read(reinterpret_cast< char* >( &data[8] ), 1024 * bytes_per_sample);
		std::streamsize n = is.gcount();

		if ( n >= 0 )
		{
			auto m = static_cast< unsigned long >( n );
			data[4] = static_cast< unsigned char >( m );
			data[5] = static_cast< unsigned char >( m >> 8 );
			data[6] = static_cast< unsigned char >( m >> 16 );
			data[7] = static_cast< unsigned char >( m >> 24 );

			if ( m != 1024UL * bytes_per_sample )
			{
				data.resize(8 + n);
			}

			isample += m / bytes_per_sample;
		} /// \todo else error

	}
	else // return 'fmt ' subchunk
	{
		is.seekg(12);
		data.resize(24);
		is.read(reinterpret_cast< char* >( &data[0] ), 24);
		in_data = true;
	}

	return data;
}

/***************************************************************************/ /**
 *******************************************************************************/
void WavFrame::prepare()
{
}

/***************************************************************************/ /**
 *******************************************************************************/
audio::sample_off WavFrame::position() const
{
	return audio::sample_off{ isample };
}

audio::sample_off WavFrame::length() const
{
	return audio::sample_off{ nsamples };
}

void WavFrame::seek(audio::sample_off i)
{
	isample = i.value;
}

bool WavFrame::eos() const
{
	return nsamples <= isample;
}

/*=========================================================================*//**
*  \class       WAV
*//*==========================================================================*/
Wav::Wav()
	: sample_rate_{0}
{
}

/***************************************************************************/ /**
 *  \todo Error reporting
 *  \bug Assuming 16-bit wavs
 *******************************************************************************/
void Wav::DecodePacket(const Packet& p)
{
	if ( p.size() < 8 )
	{
		// do nothing
	}
	else if ( ( p[0] == 'd' ) && ( p[1] == 'a' ) && ( p[2] == 't' ) && ( p[3] == 'a' ) )
	{
		/// \todo verify size
		for (std::size_t i = 8, n = p.size(); i < n - 1; i += 2)
		{
			std::uint_least16_t t = p[i + 1];
			t <<= 8;
			t  += p[i];

			std::int_least16_t u;
			#if ( USHRT_MAX == 65535 )
			u = static_cast< std::int_least16_t >( t );
			#else

			if ( t >= 32768 )
			{
				u = -static_cast< short >( 65535U - t );
				--u;
			}
			else
			{
				u = static_cast< short >( t );
			}

			#endif
			signal.push_back(u);
		}
	}
	else if ( ( p[0] == 'f' ) && ( p[1] == 'm' ) && ( p[2] == 't' ) && ( p[3] == ' ' ) )
	{
		/// \todo verify size
		unsigned format = leread< 16 >(&p[8]);

		if ( format != 1U )
		{
			return;
		}

		channels        = leread< 16 >(&p[10]);
		sample_rate_    = audio::sample_rate_type{ leread< 32 >(&p[12]) };
		bits_per_sample = leread< 16 >(&p[22]);
	}
}

/***************************************************************************/ /**
 *******************************************************************************/
Wav::samples_type Wav::DumpAudio()
{
	samples_type data(2); /// \todo channels should be set appropriately

	data.set(signal);
	signal.clear();
	return data;
}

/***************************************************************************/ /**
 *******************************************************************************/
void Wav::seek()
{
}

/***************************************************************************/ /**
 *  \todo MetaData system needs indication for "doesn't have meta so don't ask"
 *******************************************************************************/
bool Wav::getMeta(MetaData&) const
{
	return true;
}

/***************************************************************************/ /**
 *******************************************************************************/
audio::sample_rate_type Wav::sample_rate() const
{
	return sample_rate_;
}
