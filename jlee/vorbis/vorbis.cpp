/*=========================================================================*//**
*  \class       Vorbis
*  \brief       Decoder class for Vorbis audio
*
*  The Vorbis class is used to wrap up all the functions for decoding a raw Vorbis
*  audio stream. The API is relatively simple: feed DecodePacket() the packets from
*  the container stream (ex., Ogg) and it will output audio samples.
*
*  Performance-wise this class is reasonably fast. It is approaching the Xiph
*  library. It can decompress a 6m song in about 3.6s on my AMD Turion 64 X2 Mobile
*  TL-50 (using -O2). Agressive optimization can reduce this to about 3.3s. This
*  compares to 2.8s for oggdec, the decoder from Vorbis tools. There are some areas
*  to improve upon (esp., the Huffman trees and IMDCT calls), but i feel confidant
*  that i can get this code to comparable times.
*
*  This speed does not come at a cost of flexibility. It is an explicit purpose of
*  mine to move as much code to generic libraries as possible. Thus the Huffman
*  class is separate from the Vorbis code, and so are the signal processing
*  functions. The various modules should be perfectly suitable for inclusion in
*  other projects.
*
*  References
*
*  "Vorbis I Specification", Xiph.org Foundation, 1994-2007,
*  http://xiph.org/vorbis/doc/Vorbis_I_spec.pdf
*
*  "Ogg Vorbis I Audio Decoder", Sean Barrett, http://nothings.org/stb_vorbis
*
*  \bug
*
*  - Residue decoding does not properly handle do_not_decode
*  - Residue type 2 might have a begin value that does not equal zero. Is the
*  standing code correct?
*  - Beware of unicode characters in the comments. When they are output to the
*  screen they could mess up the terminal settings
*  - Catch the new exceptions. Some are unimportant, but some are fatal
*  - There is a strange error condition in 8.6.1 that reads "If
*  [classifications]^[classbook].dimensions does not equal [classbook].entries
*  the bitstream should be regarded as undecodable". This seems to be referring
*  to the codebook with number classbook.
*  - Floor0 treats the 'last' variable incorrectly - it is reset to zero at the
*  beginning of the loop and never altered.
*  - Floor0 check that map really requires 8193 elements (i.e., that the last
*  element is actually used)
*  - Check the spec: in floors an unfinished packet is meant to be marked as
*  unused?
*  - Copy the output to the destination buffer only if AudioDecode returns without
*  error
*  - Calls to ReadVQ and ReadHuffman should be checked for error conditions (or
*  should throw an exception)
*
*  \todo
*  - Integer only implementation
*  - Ensure nothing is allocated for playback until last moment. This class may
*  often be instantiated to get the comment block, and that's it.
*  - The various pVQ arrays could be combined. Since we're adjusting the indices
*  in the huffman tables _anyways_ we might as well pool the memory.
*  - The size of "classifications" is basically bounded by "nPart", which doesn't
*  seem to get much larger than 64. So instead of allocating 2048 entries for
*  that array, we could probably get away with much less.
*  - If a header packet is passed in, call Reset().
*  - Huffman probably uses more memory than necessary considering we store the
*  _entire_ tree in addition to the accelerator table.
*  - Can we get away with floats everywhere? somewhere?
*  - Windows are symmetric - no real reason to store both halves. Could store
*  windows in opposite order since whenever they are BOTH accessed, they are
*  accessed opposite one another.
*  - Feasably we could want differnt windows -- should implement a window function
*  in Audio and reorder the resultant to favour mixing.
*  - No real need to store inverse_db_table explicitly when it could be generated
*  at the time of class construction.
*  - Output the comments fields
*  - Support a seek flag, or some way of clearing the prev values when playing from
*  somewhere other than the beginning
*  - Floor0: what is the constant 0.11512925?
*  - Split audio transforms and properties into the Chirp class (eg., std::vector
*  Quantization, num of channels, samples, windowing, etc.)
*  - Move reasonable members to Audio class for reuse
*  - Reduce copying of data
*  - Fix the cache thrash in AudioDecode wrt overlap_add. Perhaps by windowing
*  right after the IMDCT
*  - Reuse the vorbis structure for different songs. When a packet header is
*  encountered while other ones have already been found, delete[] everything
*  and start fresh.
*  - Move do_not_decode and related variables to decode and avoid the double
*  negative
*  - Floor0 copies the VQ with a bound of 256 elements. Can this be bound to
*  nOrder, instead?
*  - Floor0 1st bark is constant so why recalculate it in the loop?
*  - Could probably reduce the size of inverse_db_table by declaring it as float.
*  Or by using an arithmetic-chain calculation and using two tables of 16
*  elements, or one table of 16 if the repeated squaring is acceptable.
*  - Does ilog really need to be a function?? Macro instead? arch.h?
*  - Can Residue.books_used be implemented as an invalid value instead of a
*  separate array of flags?
*  - Return bitrate for ALSA to match on playback
*  - Recognize certain comment fields and print them with "friendly" labels
*  instead of "FIELDNAME="
*//*==========================================================================*/
#include <algorithm> // for swap, min, sort
#include <climits>
#include <cmath>
#include <stdexcept>

#include "audio/packet.h"
#include "audio/samples.h"
#include "bits/ilog.h"
#include "chirp/metadata.h"
#include "endian/endian.h"
#include "signal/imdct.h"

#include "vorbis.h" // Class methods, etc.

#ifndef M_PI
#define M_PI   3.141592653589793238462643
#endif
#ifndef M_PI_4
#define M_PI_4 0.78539816339744830962
#endif

#if CHAR_BIT != 8
#error "This code implicitly uses the fact that chars are 8 bits"
#endif

namespace
{
bool valid_signature(const Packet&);

unsigned long le32(const Packet&, std::size_t);
std::vector< double > generatewindow(unsigned);
double minmax(double, double, double);
}

namespace Header
{
enum Header
{
	ID = 1,
	Comment = 3,
	Setup = 5
};
}

/***************************************************************************/ /**
 *  \fn          Vorbis::Vorbis()
 *  \brief       Default constructor.
 *
 *  Inside the class definition are three boolean flags indicating if the
 *  corresponding setup packets have been processed. They are referred to in
 *  ProcessCommentHeader(), etc. to ensure that later packets can be handled. At
 *  this point, obviously, none of the setup packets have been encountered so the
 *  flags are set to false.
 *
 *  Also, prevn which indicates the previous window size, is set to zero because
 *  there is no audio previous frame. This is referred to during the overlap-add
 *  code at the end of AudioDecode() and causes the decoder to not create any
 *  output.
 *******************************************************************************/
Vorbis::Vorbis()
	: sample_rate_{0}
{
}

/***************************************************************************/ /**
 *  \fn          Vorbis::~Vorbis()
 *  \brief       Destructor. Cleans up memory, buffers that have been allocated.
 *
 *  Destructor. Clean up after ourselves by deleting all memory that was allocated
 *  during runtime. The std::string values pointed to by pComments, and pVendor are
 *  allocated in ProcessCommentHeader(). The VQ information is allocated during
 *  ProcessSetupHeader().
 *******************************************************************************/
Vorbis::~Vorbis()
{
	delete[] buffers;
	delete[] pbuffers;

	if ( classifications != nullptr )
	{
		delete[] classifications[0];
	}

	delete[] classifications;
}

/***************************************************************************/ /**
 *  \fn          Vorbis::seek()
 *  \brief       Resets internal pointers when a seek operation is performed.
 *
 *  Since the audio packets are overlapped, we do not want a seek to cause old data
 *  to be overlapped with the new data. This resets the class to indicate that we
 *  do not have any previous data, and so to drop the first half of the new
 *  overlapped data.
 *******************************************************************************/
void Vorbis::seek()
{
	icenter = 0;
	prevn   = 0;
}

/***************************************************************************/ /**
 *  \fn          Vorbis::DumpAudio(void*, unsigned long)
 *  \brief       Store as much of the available audio to the provided output buffer
 *
 *  \todo Return a std::vector or channel containing the info.
 *******************************************************************************/
Vorbis::samples_type Vorbis::DumpAudio()
{
	samples_type data(2);

	for (unsigned long m = 0; m < output_samples; ++m)
	{
		double a = minmax(volume* final [0][m], -32767.0, 32767.0);
		double b = minmax(volume* final [1][m], -32767.0, 32767.0);

		data.push_back(static_cast< std::int_least16_t >( a ) );
		data.push_back(static_cast< std::int_least16_t >( b ) );
	}

	return data;
}

/***************************************************************************/ /**
 *  \fn          Vorbis::DecodePacket(const unsigned char*, unsigned long)
 *  \brief       Dispatch the raw packet to the correct handler
 *
 *  Dispatches packet information to correct function. This function handles the
 *  bulk of class interaction.
 *
 *  In Vorbis, compressed audio data is divided into packets containing the
 *  information to decode 64-8192 samples of data. There are also three other
 *  packets that contain setup tables and meta-information. This function accepts a
 *  packet and then dispatches the data to the correct subroutine. The basic decode
 *  loop then is to read a packet from the containing bitstream (usually Ogg) and
 *  then pass the data to this function.
 *
 *  The function also accepts an output buffer, pPCM, to output the decompressed
 *  audio to (should there be any). The size of the buffer must be provided by nPCM.
 *  The number of samples written to this buffer is returned in nPCM. For the first
 *  4 packets this will be zero as the three setup packets produce no output, and
 *  the first audio packet is only used to prime the decoder.
 *
 *  \bug The nPacket == 1 should be caught in AudioDecode as an end of stream
 *      Ogg sometimes adds these 1 byte frames for padding (apparently). Do not
 *      attempt decode. OR OpenBitstream should eventually throw if we use
 *      nPacket part of the initalization
 *  \todo Accept different output formats
 *  \bug Not checking return values of headers
 *******************************************************************************/
void Vorbis::DecodePacket(const Packet& p)
{
	if ( p.size() <= 1 )
	{
		return;
	}

	unsigned header_byte = p[0];

	switch ( header_byte )
	{
	case Header::ID:
		ProcessIDHeader(p);
		break;
	case Header::Comment:
		ProcessCommentHeader(p);
		break;
	case Header::Setup:
		ProcessSetupHeader(p);
		break;
	default: // Probably audio

		if ( ( header_byte & 1U ) != 0 )
		{
			throw std::runtime_error("Invalid header in packet.");
		}

		AudioDecode(p);
	}
}

/***************************************************************************/ /**
 *  The first packet in a Vorbis bitstream is the identification header. This
 *  function parses the header for stream parameters such as:
 *
 *  - number of channels of audio
 *  - sample rate
 *  - bitrate
 *
 *  Upon successful completion the function sets the id_header flag to true, thereby
 *  allowing the later functions to continue.
 *******************************************************************************/
void Vorbis::ProcessIDHeader(const Packet& p)
{
	if ( !valid_signature(p) || ( p.size() < 30 ) )
	{
		throw std::runtime_error("Header packet does not have signature.");
	}

	// Read parameters -----------------------------------------------------
	unsigned long version;
	unsigned      bs;

	unsigned framing;

	version      = le32(p, 7);                             // read version
	channels     = p[11];                                  // read # of channels
	sample_rate_ = audio::sample_rate_type{ le32(p, 12) }; // read sample rate
	bitrate_max  = le32(p, 16);                            // read maximum bitrate
	bitrate_nom  = le32(p, 20);                            // read nominal bitrate
	bitrate_min  = le32(p, 24);                            // read minimum bitrate
	bs           = p[28];
	blocksize0   = 1U << ( bs & 0xFU );          // read small blocksize
	blocksize1   = 1U << ( ( bs >> 4 ) & 0xFU ); // read large blocksize
	framing      = p[29] & 1U;                   // read framing bit

	// Check error conditions ----------------------------------------------
	if ( version != 0 )
	{
		throw std::runtime_error("Unsupported version.");
	}

	if ( channels == 0 )
	{
		throw std::runtime_error("Invalid channels field.");
	}

	if ( sample_rate_ == audio::sample_rate_type{ 0 } )
	{
		throw std::runtime_error("Incorrect sample_rate field.");
	}

	if ( ( 8192 < blocksize1 ) || ( blocksize1 < blocksize0 ) || ( blocksize0 < 64 ) )
	{
		throw std::runtime_error("Illegal blocksizes.");
	}

	if ( framing == 0 )
	{
		throw std::runtime_error("Framing bit.");
	}

	// Set header flags to indicate our progress ---------------------------
	id_header      = true;
	comment_header = false;
	setup_header   = false;
}

/***************************************************************************/ /**
 *  \fn          void Vorbis::ProcessCommentHeader(const std::vector<unsigned char>&)
 *
 *  After the ID header is the comment header. It contains information such as the
 *  artist, title, etc. Although all the information in this packet is optional, the
 *  packet itself is not. It 'must' be present, although the spec indicates that its
 *  absence is a non-fatal error.
 *
 *  Each comment, field, or std::string is allocated memory via new, and copied into the
 *  class. Other than that, no real processing is done. The organization of metadata
 *  will ultimately be handled by the Playlist class, so this is just in
 *  preparation.
 *
 *  Upon successful completion, this function sets the comment_header flag to true
 *  so that decoding may continue.
 *
 *  \todo? Instead of using meta data, use a Vorbis comment class and keep a copy of
 *  the packet? Then access the fields as we need them.
 *
 *  // Recognized fields from the spec and their user friendly equivalent
 *  const char * fields[][2] = { {"TITLE","Title"},
 *       {"VERSION", "Version or Remix"}, {"ALBUM", "Album"},
 *       {"TRACKNUMBER", "Track No."}, {"TRACKTOTAL", "Total Tracks"},
 *       {"ARTIST", "Artist"}, {"PERFORMER", "Performer"},
 *       {"COPYRIGHT", "Copyright"}, {"LICENSE", "License"},
 *       {"ORGANIZATION", "Organization"}, {"DESCRIPTION", "Description"},
 *       {"GENRE", "Genre"}, {"DATE", "Date Recorded"},
 *       {"LOCATION", "Location Recorded"}, {"CONTACT", "Contact Information"},
 *       {"ISRC", "ISRC"}};
 *  \todo: Do something about the silly casting.
 *
 *  \todo No bounds checking
 *******************************************************************************/
void Vorbis::ProcessCommentHeader(const Packet& p)
{
	meta.clear();
	comment_header = true;
	setup_header   = false;

	if ( !id_header || !valid_signature(p) || ( p.size() < 11 ) )
	{
		return;
	}

	std::vector< unsigned char >::size_type i = 7; // index into data
	// Save the vendor std::string ----------------------------------------------
	unsigned long nVendor = le32(p, i);

	i      += 4;
	nVendor = std::min< unsigned long >(nVendor, p.size() - i);
	meta["VENDOR"].assign(&p[i], &p[i + nVendor]);
	i += nVendor;

	if ( 4 > p.size() - i )
	{
		return;
	}

	// Save each of the user comments --------------------------------------
	unsigned long nComments = le32(p, i);

	i += 4;

	for (unsigned long k = 0; ( k < nComments ) && ( 4 <= p.size() - i ); ++k)
	{
		std::pair< std::string, std::string > s;
		unsigned long                         n = le32(p, i);
		i += 4;

		if ( n > p.size() - i )
		{
			n = p.size() - i;
		}

		s.first.assign(&p[i], &p[i + n]);
		i += n;
		std::size_t j = s.first.find('=');

		if ( ( j != std::string::npos ) && ( j < s.first.size() - 1 ) )
		{
			s.second.assign(s.first, j + 1, std::string::npos);
			s.first.erase(j);
			auto it = meta.find(s.first);

			if ( it != meta.end() )
			{
				( it->second += '\n' ).append(s.second);
			}
			else
			{
				meta.insert(s);
			}
		}
	}
}

/***************************************************************************/ /**
 *  \fn          Error Vorbis::ProcessSetupHeader(const std::vector<unsigned char>&)
 *
 *  Vorbis stores <emphasis>all</emphasis> of the codec information in this header.
 *  So, the bulk of the codec setup is done in this routine. All of the information
 *  for Huffman codebooks, floors, etc. is contained here. After verifying the
 *  signature setup procedes as follows:
 *
 *  1. Huffman codebook decode and setup
 *  2. Time Domain Transform setup
 *  3. Floor decode and setup
 *  4. Residue decode and setup
 *  5. Mapping decode and setup
 *  6. Mode decode and setup
 *
 *  This function has had some things added to it that the specification instructs
 *  to do on a frame-by-frame basis. Some of these things are constant, though, and
 *  only need to be calculated once. This includes sorting the floor x-values,
 *  finding the low and high neighbours (for type 1 floors), and generating the
 *  IMDCT window functions.
 *
 *  Optimization -------------------------------------------------------------------
 *
 *  This function is rather large, but i don't see any reason to break it up into
 *  smaller functions. None of the "subfunctions" are called except in this context
 *  and there should be no need going forward to significantly alter or optimize
 *  this code. The total time to run this function on my AMD64 1600MHz is 0.1s
 *  (pretty much constant from stream to stream, given the nature of the function).
 *  In other words, there's little potential for gain here. Though it might benefit
 *  from being 'tidied up'.
 *******************************************************************************/
Vorbis::Error Vorbis::ProcessSetupHeader(const Packet& p)
{
	// Ensure that we've seen the earlier headers
	if ( !id_header )
	{
		return ERROR_ID_HEADER;
	}

	if ( !comment_header )
	{
		return ERROR_COMMENT_HEADER;
	}

	if ( !valid_signature(p) )
	{
		return ERROR_SIGNATURE;
	}

	ILEBitstream bs;

	bs.open(reinterpret_cast< const std::byte* >( p.data() ), p.size() ); // Prepare class for bit reading
	bs.discard(8);                                                        // Skip the header byte
	bs.discard(32);                                                       // skip the 48-bit sig
	bs.discard(16);
	codebooks.clear();
	floors.clear();
	residues.clear();
	mappings.clear();
	modes.clear();
	// Setup the Huffman codebooks -----------------------------------------
	unsigned long ncodebooks = bs.read(8) + 1; // the number of codebooks

	codebooks.resize(ncodebooks);

	for (unsigned long i = 0; i < ncodebooks; ++i)
	{
		codebooks[i].unserialize(bs);
	}

	// Time Domain Transform Setup -----------------------------------------
	// This information can be discarded since it is defined to be zero
	unsigned long ntdt = bs.read(6) + 1;

	for (unsigned long i = 0; i < ntdt; ++i)
	{
		if ( bs.read(16) != 0 )
		{
			return ERROR_TDT;
		}
	}

	// Floor Setup ---------------------------------------------------------
	unsigned long nfloors = bs.read(6) + 1;

	floors.resize(nfloors);

	for (unsigned long i = 0; i < nfloors; ++i)
	{
		floors[i].unserialize(bs);
	}

	// Residue Setup ------------------------------------------------------
	unsigned long nresidues = bs.read(6) + 1;

	residues.resize(nresidues);

	for (unsigned long i = 0; i < nresidues; ++i)
	{
		residues[i].unserialize(bs);
	}

	// Mappings Setup -----------------------------------------------------
	unsigned long nmappings = bs.read(6) + 1;

	mappings.resize(nmappings);

	for (unsigned i = 0; i < nmappings; ++i)
	{
		mappings[i].unserialize(bs, channels);
	}

	// Mode Setup ---------------------------------------------------------
	unsigned long nmodes = bs.read(6) + 1;

	modes.resize(nmodes);

	for (unsigned long i = 0; i < nmodes; ++i)
	{
		modes[i].unserialize(bs);
	}

	// Read a framing bit --------------------------------------------------
	if ( bs.read(1) == 0 )
	{
		return ERROR_FRAMING;
	}

	createBuffers();
	// Create the windows
	window0      = generatewindow(blocksize0);
	window1      = generatewindow(blocksize1);
	setup_header = true;
	return OK;
}

/***************************************************************************/ /**
 *******************************************************************************/
audio::sample_rate_type Vorbis::sample_rate() const
{
	return sample_rate_;
}

/***************************************************************************/ /**
 *******************************************************************************/
// Create buffers --------------------------------------------------------------
void Vorbis::createBuffers()
{
	delete[] buffers;
	buffers = nullptr;
	buffers = new double [( blocksize1 / 2 ) * channels * 5];
	delete[] pbuffers;
	pbuffers    = nullptr;
	pbuffers    = new double*[3 * channels];
	resvec      = pbuffers;
	floor_curve = resvec + channels;
	final       = floor_curve + channels;

	for (unsigned i = 0; i < channels; ++i)
	{
		resvec[i]      = buffers + i * ( blocksize1 / 2 );
		floor_curve[i] = buffers + ( channels * ( blocksize1 / 2 ) + i * blocksize1 );
		final [i]      = buffers + ( channels * ( blocksize1 / 2 ) * 3 + i * blocksize1 );
	}

	if ( classifications != nullptr )
	{
		delete[] classifications[0];
		delete[] classifications;
		classifications = nullptr;
	}

	classifications    = new unsigned long*[channels];
	classifications[0] = new unsigned long [channels * blocksize1];

	for (unsigned i = 1; i < channels; ++i)
	{
		classifications[i] = classifications[0] + ( i * blocksize1 );
	}
}

/***************************************************************************/ /**
 *  \fn          Error Vorbis::AudioDecode(const std::vector<unsigned char>&)
 *
 *  Most of the work of decoding Vorbis audio is done in this function. The mode
 *  index is read first, which associates the packet to the correct setup
 *  information. After that, the floor is decoded for each channel; then the residue
 *  information; inverse coupling is performed; the IMDCT is applied; and finally
 *  the overlap-add (TDAC) is done. See the related functions for better
 *  descriptions.
 *
 *  At the current stage of development, much of this code is planned to be moved to
 *  the Signal class, from which Vorbis is derived. In particular, the generation
 *  and application of the window function, and the overlap-add can be abstracted.
 *******************************************************************************/
Vorbis::Error Vorbis::AudioDecode(const Packet& p)
{
	ILEBitstream bs;

	bs.open(reinterpret_cast< const std::byte* >( p.data() ), p.size() ); // Prepare class for bit reading

	bs.discard(1);
	// Initialization -----------------------------------------------------
	auto imode = static_cast< unsigned >( bs.read(ilog2p1(modes.size() - 1) ) );

	if ( imode >= modes.size() )
	{
		return ERROR_MODE_NUMBER;
	}

	if ( modes[imode].mapping >= mappings.size() )
	{
		return ERROR_MAPPING_NUMBER;
	}

	const Mapping& pMapping = mappings[modes[imode].mapping];

	unsigned bsize;

	if ( modes[imode].blockflag )
	{
		bsize = blocksize1;
		bs.read(2); // Read and discard the window shape flags
	}
	else
	{
		bsize = blocksize0;
	}

	// Window generation --------------------------------------------------
	// The window generation has been moved to ProcessSetupHeader.
	// Clear the no_residue array
	bool no_residue[256];

	// Floor curve decode -------------------------------------------------
	// Each channel stores its floor curve in order.
	for (unsigned i = 0; i < channels; ++i)
	{
		std::size_t n = pMapping.submap_floor(i);

		if ( floors.at(n).type > 1 )
		{
			return ERROR_FLOOR_TYPE;
		}

		Error err;

		if ( floors[n].type == 1 )
		{
			err = Floor1Decode(floors[n], floor_curve[i], bsize / 2, bs);
		}
		else
		{
			err = Floor0Decode(floors[n], floor_curve[i], bsize / 2, bs);
		}

		no_residue[i] = ( err == ERROR_FLOOR_UNUSED );

		if ( ( err != OK ) && ( err != ERROR_FLOOR_UNUSED ) )
		{
			return err;
		}
	}

	// Nonzero std::vector Propagate -------------------------------------------
	// If either  of the  magnitude or  angle std::vectors are  marked  as not
	// having a residue, then both need to be marked as such.
	for (unsigned i = 0; i < pMapping.coupling_steps; ++i)
	{
		unsigned imag = pMapping.mag[i];

		unsigned iang = pMapping.ang[i];

		if ( ( imag >= channels ) || ( iang >= channels ) )
		{
			throw std::runtime_error("");
		}

		if ( !no_residue[imag] || !no_residue[iang] )
		{
			no_residue[imag] = false, no_residue[iang] = false;
		}
	}

	/// \todo there's a small chance that the index into resvec is duplicated
	/// What does the spec say?
	/// \todo Also, isn't this constant?

	// Residue decode -----------------------------------------------------
	for (unsigned i = 0; i < pMapping.submaps; ++i)
	{
		for (unsigned j = 0; j < channels; ++j)
		{
			if ( pMapping.mux[j] == i )
			{
				do_not_decode[pMapping.residue_to_resvec[j]] = no_residue[j];
			}
		}

		ResidueDecode(pMapping.submap_residue[i], bsize / 2, pMapping.submapch[i], bs);
	}

	// \todo: From here on is pure calculation, not "Vorbis" stuff.
	// Implement in audio class

	// Inverse Coupling ---------------------------------------------------
	for (unsigned i = pMapping.coupling_steps; i > 0; --i)
	{
		double* pmag = resvec[pMapping.residue_to_resvec[pMapping.mag[i - 1] & 255]];
		double* pang = resvec[pMapping.residue_to_resvec[pMapping.ang[i - 1] & 255]];

		for (unsigned k = 0; k + k < bsize; ++k)
		{
			double M = pmag[k];
			double A = pang[k];

			if ( A <= 0.0 )
			{
				pmag[k] = ( M > 0.0 ? M + A : M - A );
				pang[k] = M;
			}
			else
			{
				pang[k] = ( M > 0.0 ? M - A : M + A );
			}
		}
	}

	// Dot Product and IMDCT ----------------------------------------------
	for (unsigned i = 0; i < channels; ++i)
	{
		double* f = floor_curve[i];
		double* r = resvec[pMapping.residue_to_resvec[i]];

		for (unsigned j = 0; j < bsize / 2; ++j)
		{
			f[j] *= r[j];
		}
	}

	for (unsigned i = 0; i < channels; ++i)
	{
		IMDCT(floor_curve[i], bsize);
	}

	// Overlap add and apply window ---------------------------------------
	// The window is  applied during  the overlap add  phase. The hope is
	// that some  work is  saved where  the window  goes  to 0.0  or 1.0;
	// those values are not copied. This leaves us with four cases
	TDAC(bsize);
	// Output channel order -----------------------------------------------
	// This is  actually handled  by  the  wrapper function  (PacketDecode)
	// because the format of the output  can be many different things. This
	// function just saves the floating point output.
	return OK;
}

/***************************************************************************/ /**
 *******************************************************************************/
void Vorbis::TDAC(unsigned bsize)
{
	if ( prevn == bsize ) // blocks are the same size
	{
		double* w = ( bsize == blocksize0 ? &window0[0] : &window1[0] );

		for (unsigned j = 0; j < channels; ++j)
		{
			double* f   = floor_curve[j];
			double* fin = final [j] + icenter;

			for (unsigned i = 0; i + i < bsize; ++i)
			{
				f[i] = w[2 * i] * f[i] + w[2 * i + 1] * fin[i];
			}
		}

		icenter = bsize / 2;
	}
	else if ( prevn == blocksize0 ) // short followed by long
	{
		unsigned k = ( blocksize1 - blocksize0 ) >> 2;

		for (unsigned j = 0; j < channels; ++j)
		{
			double* f   = floor_curve[j];
			double* fin = final [j] + icenter;

			for (unsigned i = 0; i + i < prevn; ++i)
			{
				f[i] = window0[2 * i + 1] * fin[i] + window0[2 * i] * f[i + k];
			}

			// \todo: Here's another move that could move
			for (unsigned i = prevn / 2; 4 * i < prevn + bsize; ++i)
			{
				f[i] = f[i + k];
			}
		}

		icenter = blocksize1 / 2;
	}
	else if ( prevn == blocksize1 ) // long followed by short
	{
		unsigned k = ( blocksize1 - blocksize0 ) >> 2;

		for (unsigned j = 0; j < channels; ++j)
		{
			double*  f   = floor_curve[j];
			double*  fin = final [j] + icenter;
			unsigned i   = 0;

			// Move our new samples down by k \todo: get rid of the move
			for (i = blocksize0; i > 0; --i)
			{
				f[i - 1 + k] = f[i - 1];
			}

			i = 0;

			for (; ( 4 * i + bsize ) < prevn; ++i)
			{
				f[i] = fin[i];
			}

			for (; ( 4 * i ) < ( prevn + bsize ); ++i)
			{
				f[i] = window0[2 * ( i - k ) + 1] * fin[i] + window0[2 * ( i - k )] * f[i];
			}
		}

		icenter = ( blocksize1 + blocksize0 ) / 4;
	}
	else
	{
		icenter = ( bsize / 2 ); // first frame, or after seek
	}

	std::swap(final, floor_curve);
	output_samples = ( prevn != 0 ? ( prevn + bsize ) / 4 : 0 );
	prevn          = bsize;
	// ---------------------------------------------------------------------

	/*
	 * On output, prepared samples will be in final and floor_curve will still
	 * hold the last samples of the curve just generated.
	 * // */

	/*
	 *      final[i].erase(0, prepared samples);
	 *      if (prevn >= bsize) {
	 *              if (prevn > bsize) final[i].resize((3 * prevn + bsize) / 4);
	 *              if (bsize == blocksize0) {
	 *                      windowlast(final[i], floor_curve[i], window0);
	 *              } else windowlast(final[i], floor_curve[i], window1);
	 *              final[i].append(floor_curve[i], bsize/2, bsize/2);
	 *      } else {
	 *              floor_curve[i].erase(0, (blocksize1 - blocksize0) / 4);
	 *              windowlast(final[i], floor_curve[i], window0);
	 *              final[i].append(blocksize0/2, whateverremains);
	 *      }
	 *      // */
}

/***************************************************************************/ /**
 *  \fn          Error Vorbis::Floor0Decode(const Floor&, double*, std::size_t, ILEBitstream&)
 *
 *  Vorbis audio reconstruction begins with the generation of a floor curve that
 *  (very) roughly approximates the final signal. There are two ways of storing the
 *  floor shape in Vorbis I, type 0 and type 1 floors. There are therefore two
 *  functions for decoding the floor shape.
 *
 *  Floors of type 0 are nearly impossible to come by. In fact, i can't even find
 *  one to do the testing of this code... The spec says that this floor type is not
 *  deprecated, but not used either. So this is included for completeness and that's
 *  all. On the other hand, if someone out there has a file with floor 0 and wants
 *  to share, i'd be happy to hear from you.
 *******************************************************************************/
Vorbis::Error Vorbis::Floor0Decode(
	const Floor&  pFloor,                                             // the floor to use
	double*       pCurve,
	std::size_t   nCurve,
	ILEBitstream& bs
)
{
	std::vector< double > fmap(nCurve + 1);
	std::vector< double > C(256);
	unsigned long         amplitude = bs.read(pFloor.amplitude_bits);

	if ( amplitude == 0 )
	{
		std::fill_n(pCurve, nCurve, 0.0);
		return ERROR_FLOOR_UNUSED;
	}

	unsigned long nC    = 0; // number of coefficients in C std::vector
	auto          ibook = static_cast< unsigned char >( bs.read(pFloor.ilog_nbooks) );

	if ( ibook > codebooks.size() )
	{
		return ERROR_FLOOR_BOOK;
	}

	do
	{
		double                last     = 0.0;
		const Codebook&       pCB      = codebooks[pFloor.books[ibook]];
		unsigned              nF       = pCB.nVQ;
		const double*         lpHuffVQ = &pCB.VQ[0] + ( pCB.nVQ * pCB.vqbook->read(bs) );
		std::vector< double > F2(nF + 1);

		for (unsigned k = 0; k < nF; ++k)
		{
			F2[k] = lpHuffVQ[k];
		}

		for (unsigned k = 0; k < nF; ++k)
		{
			F2[k] += last;
		}

		last = F2[nF]; /// \bug This just seems to be a random value

		// Append the std::vector to the current coefficients std::vector
		for (unsigned k = 0; ( k < nF ) && ( nC + k < 256 ); ++k)
		{
			C[nC + k] = F2[k];
		}

		nC += nF;
	}
	while ( nC < pFloor.order );

	if ( nC > pFloor.order ) // assert size condition
	{
		nC = pFloor.order;
	}

	// Calculate the floor curve
	for (std::size_t i = 0; i < nCurve; ++i)
	{
		double x    = 0.5 * pFloor.rate;
		double bark = 13.1 * atan(.00074 * x) + 2.24 * atan(.0001 * x * ( .000185 * x + 1.0 ) );
		x      *= static_cast< double >( i );
		x      /= static_cast< double >( nCurve );
		bark    = ( 13.1 * atan(.00074 * x) + 2.24 * atan(.0001 * x * ( .000185 * x + 1.0 ) ) ) / bark;
		bark    = floor(bark * pFloor.bark_map_size);
		fmap[i] = ( ( pFloor.bark_map_size - 1 ) < bark ) ? ( pFloor.bark_map_size - 1 ) : bark;
	}

	fmap[nCurve] = -1.0; // \todo: O rly?
	std::size_t i = 0;

	do
	{
		double omega = M_PI * fmap[i] / pFloor.bark_map_size;
		double p;

		double q; // as in the spec

		// \todo: Wanna use unsigned.. is order >= 3??
		if ( ( pFloor.order & 1 ) != 0 ) // order is odd
		{
			p = sin(omega);

			for (int j = 0; j <= ( ( pFloor.order - 3 ) / 2 ); ++j)
			{
				p *= 2.0 * ( cos(C[( 2 * j + 1 ) & 255]) - cos(omega) );
			}

			p *= p;
			q  = .5;

			for (int j = 0; j <= ( ( pFloor.order - 1 ) / 2 ); ++j)
			{
				q *= 2.0 * ( cos(C[( 2 * j ) & 255]) - cos(omega) );
			}

			q *= q;
		}
		else // order is even
		{
			p = 1.0;

			for (int j = 0; j <= ( ( pFloor.order - 2 ) / 2 ); ++j)
			{
				p *= 2.0 * ( cos(C[( 2 * j + 1 ) & 255]) - cos(omega) );
			}

			p *= p;
			p *= 0.5 * ( 1.0 - cos(omega) );
			q  = 1.0;

			for (int j = 0; j <= ( ( pFloor.order - 2 ) / 2 ); ++j)
			{
				q *= 2.0 * ( cos(C[( 2 * j ) & 255]) - cos(omega) );
			}

			q *= q;
			q *= 0.5 * ( 1.0 + cos(omega) );
		}

		double linear_floor_value = static_cast< double >( amplitude ) * pFloor.amplitude_offset
		                            / ( sqrt(p + q) * ( ( 1 << pFloor.amplitude_offset ) - 1.0 ) )
		                            - pFloor.amplitude_offset;
		linear_floor_value = exp(0.11512925 * linear_floor_value);
		double iteration_condition;

		do
		{
			iteration_condition = fmap[i];
			pCurve[i++]         = linear_floor_value;
			/// \todo Make epsilon rigorous
		}
		while ( std::fabs(fmap[i] - iteration_condition) < 0.001 && ( i < nCurve ) );
	}
	while ( i < nCurve );

	return OK;
}

/***************************************************************************/ /**
 *  Vorbis audio reconstruction begins with the generation of a floor curve that
 *  (very) roughly approximates the final signal. There are two ways of storing the
 *  floor shape in Vorbis I, type 0 and type 1 floors. There are therefore two
 *  functions for decoding the floor shape.
 *
 *  Type 1 floors are piecewise linear functions, the points of which are encoded in
 *  an order of increasing resolution. That is, the curve is first given as two
 *  points -- a straight line. This is then divided into two connected lines that
 *  better approximate the signal, then four, eight, etc. A more detailed
 *  description is given in the code below as the curve is reconstructed.
 *
 *  As input, the function expects a floor index (which will match a floor setup
 *  read by ProcessSetupHeader()), the number of samples the floor produces, and the
 *  number of channels of output.
 *******************************************************************************/
Vorbis::Error Vorbis::Floor1Decode(
	const Floor&  pFloor,                         // the floor to use
	double*       pc,                             // the output curve
	std::size_t   nCurve,
	ILEBitstream& bs
)
{
	if ( bs.read(1) == 0 ) // Skip this floor if _not_ non-zero
	{
		return ERROR_FLOOR_UNUSED;
	}

	unsigned Y[256];
	bool     step2_flag[256];
	// range is one of {256,128,86,64}
	unsigned range      = ( 255U + pFloor.multiplier ) / pFloor.multiplier;
	unsigned ilog_range = 8U - ( pFloor.multiplier >> 1 );

	// Read Floor Curve Values --------------------------------------------
	// The floor curve is a piecewise linear function which is determined
	// by a set of  points (x_0, y_0), (x_1, y_1), etc. The  x values are
	// the same from  one frame to the  next and are read  from the setup
	// header. We read  the y values  here. Note that the  values are not
	// stored in order of increasing x_i. They are interleaved.
	Y[0] = static_cast< unsigned >( bs.read(ilog_range) ); // first Y value of floor-curve
	Y[1] = static_cast< unsigned >( bs.read(ilog_range) ); // last Y value of floor curve
	unsigned long offset = 2;

	for (unsigned i = 0; i < ( pFloor.partitions & 31U ); ++i)
	{
		unsigned      p_class = 15 & pFloor.partition_class_list[i];
		unsigned      cdim    = pFloor.class_dimensions[p_class];
		unsigned      cbits   = pFloor.class_subclasses[p_class];
		unsigned      csub    = 7 & ( ( 1 << cbits ) - 1 );
		unsigned long cval
		    = ( cbits == 0 ) ? 0 : codebooks[pFloor.class_masterbooks[p_class]].scalarbook->read(bs);

		for (unsigned j = 0; j < cdim; ++j)
		{
			unsigned book = pFloor.subclass_books[p_class][cval & csub];
			cval >>= cbits;

			if ( book < codebooks.size() ) // \todo: This recently changed, from (book >= 0)
			{
				Y[( j + offset ) & 255] = static_cast< unsigned >( codebooks[book].scalarbook->read(bs) );
			}
			else
			{
				Y[( j + offset ) & 255] = 0;
			}
		}

		offset += cdim;
	}

	// \todo: What follows is just calculation -- not vorbis specific stuff.
	// consider generalizing and splitting off. Perhaps reorder points
	// into a strictly increasing order, then have a general function
	// to interpolate. Another function can do the inverse db (?)
	// Apply Differentials ------------------------------------------------
	// The floor curve  is calculated in passes,  making refinements that
	// eventually yield the actual y  values. As read above, the y values
	// are differences  from the most recent refinement.  Here we rebuild
	// the actual y values stored in  this frame. (Note: the spec says to
	// calculate  low and  high  neighbour  offsets here,  but  they  are
	// constant  so they are  stored and calculated  during Setup  Header
	// decode.)
	unsigned int final_Y[256];

	final_Y[pFloor.X_R[0]] = Y[0]; // First and last values are
	final_Y[pFloor.X_R[1]] = Y[1]; // explicit so just copy them.
	// Mark we've calculated these
	step2_flag[pFloor.X_R[0]] = true;
	step2_flag[pFloor.X_R[1]] = true;

	for (unsigned i = 2; i < pFloor.nX; ++i)
	{
		unsigned      si        = pFloor.X_R[i]; // translate i with rank(X)
		unsigned long iLo       = pFloor.lo[i];  // low_neighbour_offset
		unsigned long iHi       = pFloor.hi[i];  // high_neighbour_offset
		unsigned      predicted = 0;
		{
			// render_point inlined: Interpolates the y value for this x
			// from the closest neighbours
			unsigned X;

			unsigned x0;

			unsigned y0;

			unsigned x1;

			unsigned y1;
			X         = pFloor.X[si];
			x0        = pFloor.X[iLo];
			y0        = final_Y[iLo];
			x1        = pFloor.X[iHi];
			y1        = final_Y[iHi];
			x1       -= x0;
			X        -= x0;
			predicted = ( y1 < y0 ) ? y0 - ( ( ( y0 - y1 ) * X ) / x1 ) : y0 + ( ( ( y1 - y0 ) * X ) / x1 );
		} // end render_point

		// Calculate actual y value from the predicted and differential
		if ( Y[i] != 0 )
		{
			step2_flag[iLo] = true;
			step2_flag[iHi] = true;
			step2_flag[si]  = true;
			unsigned val  = Y[i];
			unsigned room = predicted * 2;

			if ( range < room )
			{
				room = range + range - room;
			}

			if ( val >= room )
			{
				if ( range > predicted + predicted )
				{
					predicted = val;
				}
				else
				{
					predicted = range - val - 1;
				}
			}
			else // val < room
			{
				if ( ( val & 1 ) != 0 )
				{
					predicted -= ( val + 1 ) / 2;
				}
				else
				{
					predicted += ( val / 2 );
				}
			}
		}
		else
		{
			step2_flag[si] = false; // Y[i] == 0 \todo: Is it necessary to set to "false"?
		}

		final_Y[si] = predicted;
	}

	curveSynthesis(pFloor, final_Y, step2_flag, pc, nCurve);
	return OK;
}

/// \todo Could move y to double, thus freeing some general purpose registers
/// and saving the access to inverse_db_table
/// \todo Or, we could generate the floor curve as unsigneds, then fixup the
/// double conversion later. Maybe when we mul with resvec?
void Vorbis::curveSynthesis(
	const Floor&   pFloor,
	const unsigned final_Y[256],
	const bool     step2_flag[256],
	double*        pc,
	std::size_t    nCurve
)
{
	// Curve Synthesis ----------------------------------------------------
	// We can finally calculate the curve. The output is made by stepping
	// from the lowest  x to the highest, and interpolating  the y values
	// from what has been calculated above. The final step is to take the
	// integer values and convert them to decibels.
	unsigned hx = 0;

	unsigned hy = final_Y[0] * pFloor.multiplier;

	// \todo: rigorous bounds checking for data types

	for (unsigned i = 1; i < pFloor.nX; ++i)
	{
		if ( !step2_flag[i] )
		{
			continue;
		}

		unsigned lx = hx;
		unsigned ly = hy;
		hx = std::min(pFloor.X[i], static_cast< unsigned >( nCurve ) );
		hy = final_Y[i] * pFloor.multiplier;
		// \todo: Why is y >= 0 always?? when base can be negative?
		// \todo: Can we remove the inverse_db_table by calculating the
		// the value directly (iteratively)
		// render_line(lx, ly, hx, hy, prefloor);
		const unsigned adx = hx - lx;
		double*        px  = pc + lx;
		*px = inverse_db_table[ly % 256];
		++px;
		unsigned y   = ly;
		unsigned err = 0;

		if ( ly > hy )
		{
			unsigned base = ( ly - hy ) / adx;
			ly = ly - hy - ( base * adx ); // (ly - hy) % adx

			for (unsigned x = 1; x < adx; ++x)
			{
				err += ly;

				if ( err >= adx )
				{
					err -= adx;
					--y;
				}

				y  -= base;
				*px = inverse_db_table[y & 0xff];
				++px;
			}
		}
		else
		{
			unsigned base = ( hy - ly ) / adx;
			ly = hy - ly - ( base * adx ); // (hy - ly) % adx

			for (unsigned x = 1; x < adx; ++x)
			{
				err += ly;

				if ( err >= adx )
				{
					err -= adx;
					++y;
				}

				y  += base;
				*px = inverse_db_table[y & 0xff];
				++px;
			}
		}
	}

	if ( hx < nCurve )
	{
		double a = inverse_db_table[hy % 256];

		do
		{
			pc[hx] = a;
		}
		while ( ++hx < nCurve ); // render_line(hx,hy,samples,hy,prefloor)

	}
}

/***************************************************************************/ /**
 *  The floor functions approximate the shape of the original audio only roughly.
 *  The difference between the actual signal and the floor is the residue (i.e.,
 *  what's 'left over'). This function rebuilds the residue std::vectors so that they can
 *  be combined with the floor in AudioDecode().
 *
 *  There are three ways to store the residue information. Unlike the floor
 *  functions, i have decided <emphasis>not</emphasis> to split this into three
 *  separate functions. The reason being that type 1 and 2 residues are nearly
 *  identical. And i think with some clever coding, the three cases can be handled
 *  with a single block of code.
 *
 *  This function is a bit of a mess. The reason being that I want to move abstract
 *  the VQ handling and move it into the Signal class. That way the next audio
 *  decoder I'm including, Speex, can inherit the functionality. In short,this
 *  function is not completely done.
 *******************************************************************************/
Vorbis::Error Vorbis::ResidueDecode(
	unsigned      iResidue, // the residue number to use
	unsigned long nsamples, // number of samples = current block size / 2
	unsigned      ch,       // the number of channels
	ILEBitstream& bs
)
{
	if ( iResidue >= residues.size() )
	{
		return ERROR_RESIDUE_NUMBER;
	}

	const Residue& pResidue = residues[iResidue];

	// Clear the residue arrays
	for (unsigned i = 0; i < ( ch & 255 ); ++i)
	{
		double* r = resvec[i];

		for (unsigned j = 0; j < ( nsamples & 4095 ); ++j)
		{
			r[j] = 0.0;
		}
	}

	// Check for do_not_decode condition for type 2 residues
	if ( pResidue.type == 2 )
	{
		bool res2dnd = true;

		for (unsigned j = 0; j < ch; ++j)
		{
			if ( !do_not_decode[j & 255] )
			{
				res2dnd = false;
			}
		}

		if ( res2dnd )
		{
			return OK;
		}
	}

	unsigned long actual_size;

	unsigned long residue_begin;

	unsigned long residue_end;

	unsigned long nPart;
	unsigned      classwords_per_codeword;

	actual_size = ( pResidue.type == 2 ? nsamples * ch : nsamples );
	// \bug Pretty sure one of the following is wrong
	residue_begin           = std::min(pResidue.begin, actual_size);
	residue_end             = std::min(pResidue.end, actual_size);
	classwords_per_codeword = codebooks[pResidue.classbook & 255].nVQ;
	nPart                   = ( residue_end - residue_begin ) / pResidue.partition_size;

	if ( nPart == 0 ) // no residue to read
	{
		return OK;
	}

	for (unsigned long iPart = 0; iPart < nPart;)
	{
		unsigned i = 0;

		do
		{
			if ( do_not_decode[i] )
			{
				continue;
			}

			unsigned long cw;
			cw = codebooks[pResidue.classbook].scalarbook->read(bs);
			unsigned long* cl = classifications[i] + iPart;

			for (unsigned k = classwords_per_codeword; k >= 1;)
			{
				unsigned long cw2 = cw;
				cw     /= pResidue.classifications;
				cl[--k] = cw2 - cw * pResidue.classifications;
				// \todo: This is the only place where classifications is assigned to and
				// this is the only function it is referred to
			}
		}
		while ( ++i < ( ch & 255 ) && pResidue.type != 2 );

		for (i = 0; ( i < classwords_per_codeword ) && ( iPart < nPart ); ++i)
		{
			if ( pResidue.type == 2 )
			{
				Res2(residue_begin, iPart, pResidue, 0, ch, bs);
			}
			else if ( pResidue.type == 0 )
			{
				Res0(residue_begin, iPart, pResidue, 0, ch, bs);
			}
			else if ( pResidue.type == 1 )
			{
				Res1(residue_begin, iPart, pResidue, 0, ch, bs);
			}

			++iPart;
		}
	}

	for (unsigned pass = 1; pass < 8; ++pass)
	{
		for (unsigned long iPart = 0; iPart < nPart;)
		{
			for (unsigned i = 0; ( i < classwords_per_codeword ) && ( iPart < nPart ); ++i)
			{
				if ( pResidue.type == 2 )
				{
					Res2(residue_begin, iPart, pResidue, pass, ch, bs);
				}
				else if ( pResidue.type == 0 )
				{
					Res0(residue_begin, iPart, pResidue, pass, ch, bs);
				}
				else if ( pResidue.type == 1 )
				{
					Res1(residue_begin, iPart, pResidue, pass, ch, bs);
				}

				++iPart;
			}
		}
	}

	return OK;
}

/***************************************************************************/ /**
 *******************************************************************************/
void Vorbis::Res0(
	unsigned long  residue_begin,
	unsigned long  iPart,
	const Residue& pResidue,
	unsigned       pass,
	unsigned       ch,
	ILEBitstream&  bs
)
{
	for (unsigned j = 0; j < ch; ++j)
	{
		if ( do_not_decode[j & 255] )
		{
			continue;
		}

		unsigned long vqclass = classifications[j & 255][iPart & 8191];

		if ( !pResidue.books_used[vqclass & 63][pass & 7] )
		{
			continue;
		}

		unsigned char vqbook = pResidue.books[vqclass & 63][pass & 7];
		Codebook&     pCB    = codebooks[vqbook];
		unsigned long offset = residue_begin + iPart * pResidue.partition_size;
		unsigned long step;
		step = pResidue.partition_size / pCB.nVQ;

		for (unsigned k = 0; k < step; ++k)
		{
			double* lpHuffVQ = &pCB.VQ[0] + ( pCB.nVQ * pCB.vqbook->read(bs) );

			for (unsigned u = 0; u < pCB.nVQ; ++u)
			{
				resvec[j & 255][( offset + k + u * step ) & 4095] += lpHuffVQ[u & 8191];
			}
		}
	}
}

/***************************************************************************/ /**
 *  \todo Narrow down iPart bound
 *******************************************************************************/
void Vorbis::Res1(
	unsigned long  residue_begin,
	unsigned long  iPart,
	const Residue& r,
	unsigned       pass,
	unsigned       ch,
	ILEBitstream&  bs
)
{
	if ( ( ch > 255 ) || ( pass > 8 ) || ( iPart > 8191 ) )
	{
		return;
	}

	for (unsigned j = 0; j < ch; ++j)
	{
		if ( do_not_decode[j] )
		{
			continue;
		}

		unsigned long vqclass = classifications[j][iPart];

		if ( !r.books_used[vqclass & 63][pass] )
		{
			continue;
		}

		unsigned char vqbook = r.books[vqclass & 63][pass];
		Codebook&     cb     = codebooks[vqbook];
		unsigned long offset = residue_begin + iPart * r.partition_size;
		unsigned long b      = 0;

		do
		{
			unsigned long t        = cb.vqbook->read(bs);
			double*       lpHuffVQ = &cb.VQ[0] + ( cb.nVQ * t );

			for (unsigned k = 0; k < cb.nVQ; ++k)
			{
				resvec[j][( offset + b ) & 4095] += lpHuffVQ[k & 8191];
				++b;
			}
		}
		while ( b < r.partition_size );
	}
}

/***************************************************************************/ /**
 *
 *  \todo This is the 2nd most demanding function besides IMDCT. Because nvq tends
 *     to be small (2, 4, or 8) the inner loops have a high overhead. There is
 *     probably a lot of cache-thrash, too, since we are switching from channel
 *     to channel
 *  \todo Could deinterlace *after* getting everything from the VQs.
 *******************************************************************************/
void Vorbis::Res2(
	unsigned long  residue_begin,
	unsigned long  iPart,
	const Residue& pResidue,
	unsigned       pass,
	unsigned       ch,
	ILEBitstream&  bs
)
{
	// \bug if ipart > samples. Actually since classifications might only be npart bytes long..
	unsigned long vqclass = classifications[0][iPart & 8191];

	if ( !pResidue.books_used[vqclass & 63][pass & 7] )
	{
		return;
	}

	unsigned char vqbook = pResidue.books[vqclass & 63][pass & 7];
	Codebook&     pCB    = codebooks[vqbook];

	if ( pCB.VQ.empty() )
	{
		return;
	}

	const unsigned nvq = pCB.nVQ;
	// \bug The division of offset by ch might not be compliant with the
	// spec. Does ch always divide the other amount exactly? If not,
	// there is an amount lost
	unsigned long offset;

	unsigned long c = 0;

	offset = ( residue_begin + iPart * pResidue.partition_size ) / ch;

	for (unsigned long b = 0; b < pResidue.partition_size; b += nvq)
	{
		double* lpHuffVQ = &pCB.VQ[0] + ( nvq * pCB.vqbook->read(bs) );

		// * Absolutely works
		for (unsigned k = 0; k < nvq; ++k)
		{
			resvec[c][offset] += lpHuffVQ[k];

			if ( ++c >= ch )
			{
				c = 0, ++offset;
			}
		}

		// */
		// Equivalent variation of the above.. except not sure if c
		// and offset should evently divide nvq so that this optimization
		// is exactly equivalent.

		/*
		 * for (unsigned long c = 0; c < ch; ++c) {
		 *      unsigned long offset = oldoff;
		 *      double* r = &(resvec[c][0]);
		 *      for (unsigned k = c; k < nvq; k += ch) {
		 *              r[offset++] += lpHuffVQ[k];
		 *      }
		 * }
		 * oldoff += (nvq / ch);
		 * // */
	}
}

bool Vorbis::getMeta(MetaData& m) const
{
	if ( comment_header )
	{
		std::map< std::string, std::string >::const_iterator it;
		it = meta.find("ARTIST");

		if ( it != meta.end() )
		{
			m.assign(MetaData::Artist, it->second);
		}

		it = meta.find("ALBUM");

		if ( it != meta.end() )
		{
			m.assign(MetaData::Album, it->second);
		}

		it = meta.find("TITLE");

		if ( it != meta.end() )
		{
			m.assign(MetaData::Title, it->second);
		}
	}

	return comment_header;
}

/***************************************************************************/ /**
 *******************************************************************************/
Vorbis::Codebook::Codebook()

= default;

Vorbis::Codebook::Codebook(const Codebook& cb)
	: VQ(cb.VQ), nVQ(cb.nVQ), vqbook(nullptr), scalarbook(nullptr)
{
	if ( cb.vqbook != nullptr )
	{
		vqbook = new Huffman< unsigned long >(*cb.vqbook);
	}

	if ( cb.scalarbook != nullptr )
	{
		scalarbook = new Huffman< unsigned long >(*cb.scalarbook);
	}
}

/***************************************************************************/ /**
 *******************************************************************************/
Vorbis::Codebook::~Codebook()
{
	delete vqbook;
	delete scalarbook;
}

/// \todo copy-and-swap
Vorbis::Codebook& Vorbis::Codebook::operator=(const Codebook& cb)
{
	if ( std::addressof(cb) != this )
	{
		VQ  = cb.VQ;
		nVQ = cb.nVQ;
		delete vqbook;
		delete scalarbook;
		vqbook     = new Huffman< unsigned long >(*cb.vqbook);
		scalarbook = new Huffman< unsigned long >(*cb.scalarbook);
	}

	return *this;
}

/***************************************************************************/ /**
 *******************************************************************************/
double Vorbis::Codebook::float32_unpack(unsigned long a)
{
	double mantissa;

	double exponent;

	mantissa = static_cast< double >( a & UINT32_C(0x1FFFFF) );
	exponent = static_cast< double >( ( a >> 21 ) & UINT32_C(0x3FF) );

	if ( ( a & UINT32_C(0x80000000) ) != 0 )
	{
		mantissa = -mantissa;
	}

	mantissa *= pow(2.0, exponent - 788.0);
	return mantissa;
}

/***************************************************************************/ /**
 *******************************************************************************/
void Vorbis::Codebook::unserialize(ILEBitstream& bs)
{
	if ( bs.read(24) != UINT32_C(0x564342) ) // Verify codebook signature
	{
		throw std::runtime_error("Codebook signature was expected");
	}

	// Read the dimensions and entries parameters
	auto dimensions = static_cast< unsigned >( bs.read(16) );

	nVQ = dimensions;
	unsigned long entries = bs.read(24);
	// Create a codebook of the appropriate size
	// \todo pLengths could be an array of chars...
	// \todo why allocate the whole thing for sparse tables? Maybe
	// there is no choice..
	auto* pLengths = new unsigned char [entries];
	auto* pSymbols = new unsigned long [entries];
	// Tables may be encoded length ordered or not...
	bool ordered = ( bs.read(1) != 0 ); // [ordered] in the spec
	// Decode codeword lengths
	unsigned long actual_entries = 0;

	if ( !ordered ) // lengths must be read one by one
	{
		bool sparse = ( bs.read(1) != 0 ); // flag

		if ( !sparse ) // every entry has a length for it
		{
			for (unsigned long k = 0; k < entries; ++k)
			{
				pLengths[k] = static_cast< unsigned char >( bs.read(5) + 1 );
				pSymbols[k] = k;
			}

			actual_entries = entries;
		}
		else // sparse tree (some entries are unused)
		{
			for (unsigned long k = 0; k < entries; ++k)
			{
				if ( bs.read(1) != 0 ) // used
				{
					pLengths[actual_entries] = static_cast< unsigned char >( bs.read(5) + 1 );
					pSymbols[actual_entries] = k;
					++actual_entries;
				} // else skip the entry

			}
		}
	}
	else // the codeword lengths are ordered and grouped
	{
		auto          codelen = static_cast< unsigned char >( bs.read(5) + 1 );
		unsigned long j       = 0;

		while ( j < entries )
		{
			auto num = static_cast< unsigned >( bs.read(ilog2p1(entries - j) ) );

			for (unsigned k = 0; k < num; ++k)
			{
				pLengths[j + k] = codelen;
				pSymbols[j + k] = j + k;
			}

			j += num;
			++codelen; // next codelength
		}

		actual_entries = j;

		if ( j > entries ) // too many entries
		{
			delete[] pLengths; /// \todo The usual
			delete[] pSymbols;
			throw std::runtime_error("Too many huffman entries");
		}
	} // End: Decode Codelength words

	scalarbook = new Huffman< unsigned long >(pLengths, actual_entries, HuffmanTree::Minimal, pSymbols);
	// Decode std::vector Lookup Table
	auto vqtype = static_cast< unsigned >( bs.read(4) ); // lookup_type in the spec

	if ( vqtype > 2 )
	{
		delete[] pLengths; /// \todo The usual
		delete[] pSymbols;
		throw std::runtime_error("Invalid VQ type");
	}

	VQ.clear();

	if ( vqtype != 0 )
	{
		VQ.resize(actual_entries * dimensions);
		double min_value  = float32_unpack(bs.read(32) );
		double delta      = float32_unpack(bs.read(32) );
		auto   value_bits = static_cast< unsigned >( bs.read(4) + 1 );
		bool   sequence_p = ( bs.read(1) != 0 );
		// lookup1_values() was replaced by pow and a cast...
		unsigned long lookup_values
		    = ( vqtype == 1 )
		        ? static_cast< unsigned long >( pow(static_cast< double >( entries ), 1.0 / dimensions) )
		        : entries * dimensions;
		std::vector< unsigned long > multiplicands(lookup_values);

		for (unsigned long k = 0; k < lookup_values; ++k)
		{
			multiplicands[k] = bs.read(value_bits);
		}

		if ( vqtype == 1 )
		{
			// Create a std::vector for each codebook entry
			for (unsigned long j = 0; j < actual_entries; ++j)
			{
				// if (pCodebook.pHuffmanC[j] == 0) continue; // unused
				unsigned long q    = 1;
				double        last = 0.0;

				// integer_divisor in the spec
				for (unsigned k = 0; k < dimensions; ++k)
				{
					VQ[j * dimensions + k]
					    = last + min_value
					      + static_cast< double >(
							  static_cast< long double >(
								  multiplicands[( pSymbols[j] / q ) % lookup_values] )
							  * static_cast< long double >( delta ) );

					if ( sequence_p )
					{
						last = VQ[j * dimensions + k];
					}

					q *= lookup_values;
				}
			}
		}
		else // lookup_type == 2
		{
			for (unsigned long j = 0; j < actual_entries; ++j)
			{
				// if (pCodebook.pHuffmanC[j] == 0)
				// continue; // unused
				double        last   = 0.0;
				unsigned long offset = pSymbols[j] * dimensions;

				for (unsigned k = 0; k < dimensions; ++k)
				{
					VQ[j * dimensions + k]
					    = last + min_value
					      + static_cast< double >(
							  static_cast< long double >( multiplicands[offset] )
							  * static_cast< long double >( delta ) );

					if ( sequence_p )
					{
						last = VQ[j * dimensions + k];
					}

					++offset;
				}
			}
		}

		/// \todo Replace with a simple HuffmanBase
		vqbook = new Huffman< unsigned long >(pLengths, actual_entries, HuffmanTree::Minimal, nullptr);
	}
	else
	{
		vqbook = nullptr; // \bug should be an illegal value
	}

	delete[] pLengths;
	delete[] pSymbols;
}

/// \todo Default constructor sets type to invalid value. Make sure this change is reflected by code
Vorbis::Floor::Floor() = default;

Vorbis::Floor::Floor(const Floor& f)
	: type(f.type), ilog_nbooks(f.ilog_nbooks), rate(f.rate), bark_map_size(f.bark_map_size), order(f.order),
	amplitude_bits(f.amplitude_bits), amplitude_offset(f.amplitude_offset), nbooks(f.nbooks),
	multiplier(f.multiplier), partitions(f.partitions), nX(f.nX)
{
	std::copy(f.books, f.books + 16, books);

	for (std::size_t i = 0; i < 16; ++i)
	{
		for (std::size_t j = 0; j < 8; ++j)
		{
			subclass_books[i][j] = f.subclass_books[i][j];
		}
	}

	std::copy(f.X, f.X + 256, X);
	std::copy(f.X_R, f.X_R + 256, X_R);
	std::copy(f.lo, f.lo + 256, lo);
	std::copy(f.hi, f.hi + 256, hi);
	std::copy(f.partition_class_list, f.partition_class_list + 32, partition_class_list);
	std::copy(f.class_dimensions, f.class_dimensions + 16, class_dimensions);
	std::copy(f.class_subclasses, f.class_subclasses + 16, class_subclasses);
	std::copy(f.class_masterbooks, f.class_masterbooks + 16, class_masterbooks);
}

Vorbis::Floor& Vorbis::Floor::operator=(const Floor& f)
{
	type             = f.type;
	ilog_nbooks      = f.ilog_nbooks;
	rate             = f.rate;
	bark_map_size    = f.bark_map_size;
	order            = f.order;
	amplitude_bits   = f.amplitude_bits;
	amplitude_offset = f.amplitude_offset;
	nbooks           = f.nbooks;
	multiplier       = f.multiplier;
	partitions       = f.partitions;
	nX               = f.nX;
	std::copy(f.books, f.books + 16, books);

	for (std::size_t i = 0; i < 16; ++i)
	{
		for (std::size_t j = 0; j < 8; ++j)
		{
			subclass_books[i][j] = f.subclass_books[i][j];
		}
	}

	std::copy(f.X, f.X + 256, X);
	std::copy(f.X_R, f.X_R + 256, X_R);
	std::copy(f.lo, f.lo + 256, lo);
	std::copy(f.hi, f.hi + 256, hi);
	std::copy(f.partition_class_list, f.partition_class_list + 32, partition_class_list);
	std::copy(f.class_dimensions, f.class_dimensions + 16, class_dimensions);
	std::copy(f.class_subclasses, f.class_subclasses + 16, class_subclasses);
	std::copy(f.class_masterbooks, f.class_masterbooks + 16, class_masterbooks);
	return *this;
}

/***************************************************************************/ /**
 *******************************************************************************/
void Vorbis::Floor::unserialize(ILEBitstream& bs)
{
	// Read the floor type
	type = static_cast< unsigned >( bs.read(16) );

	if ( type > 1 )
	{
		throw std::runtime_error("FLOOR_TYPE");
	}

	if ( type == 0 )
	{
		order            = static_cast< unsigned char >( bs.read(8) );
		rate             = static_cast< unsigned >( bs.read(16) );
		bark_map_size    = static_cast< unsigned >( bs.read(16) );
		amplitude_bits   = static_cast< unsigned char >( bs.read(6) );
		amplitude_offset = static_cast< unsigned char >( bs.read(8) );
		nbooks           = static_cast< unsigned char >( bs.read(4) + 1 );
		ilog_nbooks      = ilog2p1(nbooks);

		for (unsigned j = 0; j < nbooks; ++j)
		{
			books[j] = static_cast< unsigned char >( bs.read(8) );

			/* \bug write this back in
			 * if (pf0.books[j & 15] > ncodebooks)
			 *      throw std::runtime_error("Mismatch of code book and floor");
			 * // */
		}
	}
	else
	{
		unsigned char maximum_class = 0;
		unsigned      X2[256];
		partitions = static_cast< unsigned char >( bs.read(5) );

		for (unsigned j = 0; j < partitions; ++j)
		{
			partition_class_list[j] = static_cast< unsigned char >( bs.read(4) );

			if ( maximum_class < partition_class_list[j] )
			{
				maximum_class = partition_class_list[j];
			}
		}

		for (unsigned j = 0; j <= maximum_class; ++j)
		{
			class_dimensions[j] = static_cast< unsigned char >( bs.read(3) + 1 );
			class_subclasses[j] = static_cast< unsigned char >( bs.read(2) );

			if ( class_subclasses[j] != 0 )
			{
				class_masterbooks[j] = static_cast< unsigned char >( bs.read(8) );
			}

			/*\bug add back in
			 * if (class_masterbooks[j] > ncodebooks)
			 *      return ERROR_FLOOR_BOOK;
			 * // */

			for (unsigned m = 0; m < ( 1U << class_subclasses[j] ); ++m)
			{
				// \todo: Work around the signed casts
				auto b = static_cast< unsigned >( bs.read(8) );

				/*\bug work back in. Note zero becomes -1 below
				 * if (b > ncodebooks)
				 *      return ERROR_FLOOR_BOOK;
				 * // */
				subclass_books[j][m & 7] = b - 1;
			}
		}

		multiplier = static_cast< unsigned >( bs.read(2) + 1 );
		auto rangebits = static_cast< unsigned char >( bs.read(4) );
		X2[0] = 0;
		X2[1] = ( 1U << rangebits );
		nX    = 2;
		unsigned long current_class_number;

		for (unsigned j = 0; j < partitions; ++j)
		{
			current_class_number = partition_class_list[j & 31];

			for (unsigned k = 0; k < class_dimensions[current_class_number & 15]; ++k)
			{
				X2[nX & 255] = static_cast< unsigned >( bs.read(rangebits) );
				++nX;
			}

			nX &= 0xFF;
		}

		// Precalc low neighbour and high neighbour offsets
		for (unsigned char k = 0; k < nX; ++k)
		{
			unsigned char iLo = 0;

			unsigned char iHi = 0;

			for (unsigned char j = 1; j < k; ++j)
			{
				if ( X2[j] < X2[k] )
				{
					if ( X2[j] > X2[iLo] )
					{
						iLo = j;
					}
				}
				else
				{
					if ( ( X2[j] < X2[iHi] ) || ( X2[iHi] < X2[k] ) )
					{
						iHi = j;
					}
				}
			}

			lo[k] = iLo & 0xFF;
			hi[k] = iHi & 0xFF;
		}

		// Pre-sort the X values of the floor in another array
		for (unsigned j = 0; j < 256; ++j)
		{
			X[j] = X2[j];
		}

		std::sort(X, X + nX);

		// Create a rank table of X (given the sorted table)
		for (unsigned j = 0; j < nX; ++j)
		{
			unsigned k = 0;

			for (; k < nX; ++k)
			{
				if ( X[k] == X2[j] )
				{
					break;
				}
			}

			X_R[j] = static_cast< unsigned char >( k );
		}

		// Fixup lo[] and hi[] to reflect the sorted array
		for (unsigned j = 0; j < nX; ++j)
		{
			lo[j] = X_R[lo[j]] & 0xFF;
			hi[j] = X_R[hi[j]] & 0xFF;
		}
	}
}

/***************************************************************************/ /**
 *******************************************************************************/
Vorbis::Residue::Residue() = default;

Vorbis::Residue::Residue(const Residue& r)
	: begin(r.begin), end(r.end), partition_size(r.partition_size), type(r.type),
	classifications(r.classifications), classbook(r.classbook)
{
	for (std::size_t i = 0; i < 64; ++i)
	{
		for (std::size_t j = 0; j < 8; ++j)
		{
			books[i][j]      = r.books[i][j];
			books_used[i][j] = r.books_used[i][j];
		}
	}
}

Vorbis::Residue& Vorbis::Residue::operator=(const Residue& r)
{
	begin           = r.begin;
	end             = r.end;
	partition_size  = r.partition_size;
	type            = r.type;
	classifications = r.classifications;
	classbook       = r.classbook;

	for (std::size_t i = 0; i < 64; ++i)
	{
		for (std::size_t j = 0; j < 8; ++j)
		{
			books[i][j]      = r.books[i][j];
			books_used[i][j] = r.books_used[i][j];
		}
	}

	return *this;
}

void Vorbis::Residue::unserialize(ILEBitstream& bs)
{
	type = static_cast< unsigned >( bs.read(16) );

	if ( type > 2 )
	{
		throw std::runtime_error("Invalid residue type");
	}

	begin           = bs.read(24);
	end             = bs.read(24);
	partition_size  = bs.read(24) + 1;
	classifications = static_cast< unsigned char >( bs.read(6) + 1 );
	classbook       = static_cast< unsigned char >( bs.read(8) );

	for (unsigned j = 0; j < classifications; ++j)
	{
		unsigned long cascade = bs.read(3) & 7;

		if ( bs.read(1) != 0L )
		{
			cascade += ( ( bs.read(5) << 3 ) & 0xF8 );
		}

		for (unsigned m = 0; m < 8; ++m)
		{
			books_used[j & 63][m] = ( ( cascade & ( 1U << m ) ) != 0 );
		}
	}

	for (unsigned j = 0; j < classifications; ++j)
	{
		for (unsigned m = 0; m < 8; ++m)
		{
			if ( books_used[j & 63][m] )
			{
				books[j & 63][m] = static_cast< unsigned char >( bs.read(8) );
			}

			/* \bug Work back in
			 * if (pResidue.books[j & 63][m] > ncodebooks)
			 *      return ERROR_RESIDUE_BOOK;
			 * // */
		}
	}
}

/***************************************************************************/ /**
 *******************************************************************************/
void Vorbis::Mode::unserialize(ILEBitstream& bs)
{
	blockflag = ( bs.read(1) != 0 );

	if ( bs.read(16) != 0 ) // decode and discard window type
	{
		throw std::runtime_error("Invalid window type");
	}

	if ( bs.read(16) != 0 ) // decode and discard transform type
	{
		throw std::runtime_error("Invalid transformation type");
	}

	mapping = static_cast< unsigned char >( bs.read(8) );
}

/***************************************************************************/ /**
 *******************************************************************************/
Vorbis::Mapping::Mapping() = default;

Vorbis::Mapping::Mapping(const Mapping& m)
	: mag(m.mag), ang(m.ang), mux(m.mux), coupling_steps(m.coupling_steps), submaps(m.submaps), ch(m.ch)
{
	std::copy(m.submap_floor_, m.submap_floor_ + 16, submap_floor_);
	std::copy(m.submap_residue, m.submap_residue + 16, submap_residue);
}

/***************************************************************************/ /**
 *******************************************************************************/
Vorbis::Mapping::~Mapping() = default;

Vorbis::Mapping& Vorbis::Mapping::operator=(const Mapping& m)
{
	coupling_steps = m.coupling_steps;
	submaps        = m.submaps;
	ch             = m.ch;
	mag            = m.mag;
	ang            = m.ang;
	mux            = m.mux;
	std::copy(m.submap_floor_, m.submap_floor_ + 16, submap_floor_);
	std::copy(m.submap_residue, m.submap_residue + 16, submap_residue);
	return *this;
}

/***************************************************************************/ /**
 *******************************************************************************/
void Vorbis::Mapping::unserialize(
	ILEBitstream& bs,
	unsigned      nch
)
{
	ch = nch;

	if ( bs.read(16) != 0 ) // read mapping type
	{
		throw std::runtime_error("ERROR_MAPPING_TYPE");
	}

	if ( bs.read(1) != 0 )
	{
		submaps = static_cast< unsigned char >( bs.read(4) + 1 );
	}
	else
	{
		submaps = 1;
	}

	if ( bs.read(1) != 0 ) // square polar channel mapping
	{
		coupling_steps = static_cast< unsigned >( bs.read(8) + 1 );
		mag.resize(coupling_steps);
		ang.resize(coupling_steps);

		for (unsigned j = 0; j < coupling_steps; ++j)
		{
			mag[j] = static_cast< unsigned char >( bs.read(ilog2p1(ch - 1) ) );

			if ( mag[j] >= ch )
			{
				throw std::runtime_error("ERROR_MAPPING_MAGNITUDE");
			}

			ang[j] = static_cast< unsigned char >( bs.read(ilog2p1(ch - 1) ) );

			if ( ang[j] >= ch )
			{
				throw std::runtime_error("ERROR_MAPPING_ANGLE");
			}

			if ( ang[j] == mag[j] )
			{
				throw std::runtime_error("ERROR_MAPPING_COUPLING");
			}
		}
	}
	else
	{
		coupling_steps = 0;
		mag.clear();
		ang.clear();
	}

	if ( bs.read(2) != 0 )
	{
		throw std::runtime_error("ERROR_MAPPING_RESERVED");
	}

	mux.resize(ch, 0);

	if ( submaps > 1 ) // read multiplex settings
	{
		for (unsigned j = 0; j < ch; ++j)
		{
			mux[j] = static_cast< unsigned char >( bs.read(4) & 0xF );

			if ( mux[j] >= submaps )
			{
				throw std::runtime_error("ERROR_MAPPING_MULTIPLEX");
			}
		}
	}

	for (unsigned i = 0; i < submaps; ++i)
	{
		unsigned chx = 0;

		for (unsigned j = 0; j < nch; ++j)
		{
			if ( mux[j] == i )
			{
				residue_to_resvec[j] = chx++;
			}
		}

		submapch[i] = chx;
	}

	for (unsigned j = 0; j < submaps; ++j)
	{
		bs.read(8); // discard reserved 8-bit field
		submap_floor_[j & 15]  = static_cast< unsigned char >( bs.read(8) );
		submap_residue[j & 15] = static_cast< unsigned char >( bs.read(8) );
	}
}

unsigned char Vorbis::Mapping::submap_floor(unsigned i) const
{
	return submap_floor_[mux[i]];
}

namespace
{
bool valid_signature(const Packet& p)
{
	return p.size() >= 7 && le32(p, 1) == UINT32_C(0x62726F76) && leread< 16 >(p.data() + 5) == UINT32_C(0x7369);
}

unsigned long le32(
	const Packet& p,
	std::size_t   i
)
{
	std::size_t n = p.size();

	if ( i < n )
	{
		unsigned long x = p[i];
		#if ( ( 3 * CHAR_BIT ) < 32 )

		if ( 3 < n - i )
		{
			x += ( static_cast< unsigned long >( p[i + 1] ) << CHAR_BIT )
			     + ( static_cast< unsigned long >( p[i + 2] ) << ( 2 * CHAR_BIT ) )
			     + ( static_cast< unsigned long >( p[i + 3] ) << ( 3 * CHAR_BIT ) );
		}
		else
		#endif
		#if ( ( 2 * CHAR_BIT ) < 32 )

		if ( 2 < n - i )
		{
			x += ( static_cast< unsigned long >( p[i + 1] ) << CHAR_BIT )
			     + ( static_cast< unsigned long >( p[i + 2] ) << ( 2 * CHAR_BIT ) );
		}
		else
		#endif
		#if ( CHAR_BIT < 32 )

		if ( 1 < n - i )
		{
			x += ( static_cast< unsigned long >( p[i + 1] ) << CHAR_BIT );
		}

		#endif
		#if ( ULONG_MAX == 4294967295 )
		return x;

		#else
		return x & 0xFFFFFFFFUL;

		#endif
	}

	return 0;
}

std::vector< double > generatewindow(unsigned n)
{
	std::vector< double > w(n, 0.0);

	for (unsigned i = 0; i < n; i += 2)
	{
		double x = sin(M_PI_4 * ( 1.0 - cos(M_PI * ( i + 1 ) / n) ) );
		w[i]         = x;
		w[n - i - 1] = x;
	}

	return w;
}

double minmax(
	double x,
	double lo,
	double hi
)
{
	return std::max(std::min(x, hi), lo);
}

} // end anonymous namespace
