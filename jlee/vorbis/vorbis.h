#ifndef VORBIS_HPP
#define VORBIS_HPP

#include <map>
#include <string>

#include "audio/codec.h" // IMDCT, packet.h
#include "bitstream/bebitstream.h"
#include "huffman/huffman.h" // bitstream.hpp

// Vorbis Structures
// TODO: Once some changes are made to Huffman (i.e., not storing symbols
// explicitly) we can probably do away with the separate books. Also, all
// vqdata should be allocated at once and stored in one array.

// Vorbis Class Definition
class Vorbis : public audio::Codec
{
	enum Error
	{
		OK = 0,			// There was no error.
		ERROR_POINTER,		// A null pointer was passed
		ERROR_NO_DATA,		// No input was provided
		ERROR_MEMORY,		// The decoder was not able to allocate memory.
		ERROR_HEADER_TYPE,	// Identified as header, but isn't
		ERROR_SIGNATURE,	// The "vorbis" string was not found
		ERROR_VERSION,		// Unsupported version number.
		ERROR_CHANNELS,		// The number of channels is listed as zero
		ERROR_SAMPLE_RATE,	// The sample rate was not defined
		ERROR_BLOCKSIZE,	// A blocksize was out of range
		ERROR_FRAMING,		// An expected framing bit was not set
		ERROR_ID_HEADER,	// ID header was not processed before decode
		ERROR_COMMENT_HEADER,	// Comment header was not processed before decode
		ERROR_CODEBOOK_SIG,	// Sync pattern of 0x564342 was not found
		ERROR_CODEBOOK_ENTRIES, // A Huffman tree is overspecified.
		ERROR_LOOKUP_TYPE,	// Incorrect VQ lookup tables was specified
		ERROR_TDT,		// Invalid Time Domain Transform specified
		ERROR_WINDOW_TYPE,	// An unknown window type was specified
		ERROR_TRANSFORM_TYPE,	// A transform type other than MDCT was given
		ERROR_FLOOR_TYPE,	// Incorrect floor type in the setup header
		ERROR_FLOOR_BOOK,	// A floor has an invalid codebook number
		ERROR_RESIDUE_TYPE,	// Incorrect residue type in setup header
		ERROR_RESIDUE_BOOK,	// A residue has an invalid codebook number
		ERROR_MAPPING_TYPE,	// A non-zero mapping type was defined
		ERROR_MAPPING_MAGNITUDE, // The channel specified as the magnitude vector for square polar channel mapping is not available
		ERROR_MAPPING_ANGLE, // The channel specified as the angle vector for square polar channel mapping is not available
		ERROR_MAPPING_COUPLING, // The same channel was selected for magnitude and angle in square polar channel mapping
		ERROR_MAPPING_RESERVED,	 // A reserved field was not zero
		ERROR_MAPPING_MULTIPLEX, // The multiplex selected an incorrect submap
		ERROR_SUBMAP_FLOOR,	 // A submap selected an incorrect floor
		ERROR_SUBMAP_RESIDUE,	 // A submap selected an incorrect residue
		ERROR_FLOOR_UNUSED,	 // The floor being decoded is marked as unused (non-fatal error)
		ERROR_RESIDUE_NUMBER,	 // An invalid residue number was passed to ResidueDecode
		ERROR_FLOOR_NUMBER,	 // An invalid floor number was passed to Floor0 or Floor1 Decode
		ERROR_MODE_NUMBER,	 // An invalid mdoe number was encountered
		ERROR_MAPPING_NUMBER,	 // An invalid mapping number was encountered
		ERROR_SUBMAP_NUMBER	 // An invalid submap number was encountered
	};

	class Floor;
	class Codebook;
	class Residue;
	class Mapping;
	class Mode;

	Vorbis(const Vorbis&) = delete;		   // non-copyable
	Vorbis& operator=(const Vorbis&) = delete; // non-copyable

	void ProcessIDHeader(const Packet&);
	void ProcessCommentHeader(const Packet&);
	Error ProcessSetupHeader(const Packet&);
	Error AudioDecode(const Packet&);

	Error Floor0Decode(const Floor&, double*, std::size_t, ILEBitstream&);
	Error Floor1Decode(const Floor&, double*, std::size_t, ILEBitstream&);

	void curveSynthesis(const Floor&, const unsigned[256], const bool[256], double*, std::size_t);
	Error ResidueDecode(unsigned, unsigned long, unsigned, ILEBitstream&);
	void Res0(unsigned long, unsigned long, const Residue&, unsigned, unsigned, ILEBitstream&);
	void Res1(unsigned long, unsigned long, const Residue&, unsigned, unsigned, ILEBitstream&);
	void Res2(unsigned long, unsigned long, const Residue&, unsigned, unsigned, ILEBitstream&);
	void TDAC(unsigned);
	void createBuffers();

	std::map< std::string, std::string > meta;

	bool do_not_decode[256]{}; // use 8 unsigned longs on stack instead

	std::vector< Codebook > codebooks;
	std::vector< Floor > floors;
	std::vector< Residue > residues;
	std::vector< Mode > modes;
	std::vector< Mapping > mappings;

	// These are straight arrays, and that should be fine
	double* buffers{ nullptr };
	double** pbuffers{ nullptr };

	double** resvec{ nullptr };	 // Residue workspace
	double** floor_curve{ nullptr }; // TODO: remove. Calculate addresses on the fly inside DecodeAudio
	double** final{ nullptr };
	std::vector< double > window0;
	std::vector< double > window1;

	unsigned long** classifications{ nullptr }; // Residue workspace

	unsigned long icenter{ 0 }; // TODO: This has questionable worth

	unsigned long bitrate_max{ 0 };
	unsigned long bitrate_nom{ 0 };
	unsigned long bitrate_min{ 0 };

	double volume{ 32767.0 };

	audio::sample_rate_type sample_rate_; // TODO: Move to audio parent class
	unsigned long output_samples{ 0 };
	unsigned int output_ch{ 2 };
	unsigned int channels{ 0 };

	unsigned int blocksize0{ 0 };
	unsigned int blocksize1{ 0 };
	unsigned int prevn{ 0 }; // TODO: Get rid of this or change to flag

	bool id_header{ false }; // TODO combine into one value
	bool comment_header{ false };
	bool setup_header{ false };

public:
	Vorbis();
	~Vorbis() override;

	void seek(void) override;
	void DecodePacket(const Packet&) override;
	samples_type DumpAudio() override;
	bool getMeta(MetaData&) const override;
	audio::sample_rate_type sample_rate() const override;
};

class Vorbis::Floor
{
public:
	unsigned int type{ 2 };

	// Floor 0 fields
	unsigned char books[16];       // book_list in the spec
	unsigned int ilog_nbooks{ 0 }; // store for ilog(nbooks) -- constant
	unsigned int rate{ 0 };
	unsigned int bark_map_size{ 0 };
	unsigned char order{ 0 };
	unsigned char amplitude_bits{ 0 };
	unsigned char amplitude_offset{ 0 };
	unsigned char nbooks{ 0 }; // was number_of_books

	// Floor 1 fields
	unsigned int subclass_books[16][8];
	unsigned int X[256];	// a sorted version of X
	unsigned char X_R[256]; // rank table for X
	unsigned char lo[256];	// stores low_neighbour_offset
	unsigned char hi[256];	// stores high_neighbour_offset
	unsigned char partition_class_list[32];
	unsigned char class_dimensions[16];
	unsigned char class_subclasses[16];
	unsigned char class_masterbooks[16];
	unsigned int multiplier{ 0 };
	unsigned char partitions{ 0 };
	unsigned char nX{ 0 }; // called [values] in the spec

	Floor();
	Floor(const Floor&);
	Floor& operator=(const Floor&);

	void unserialize(ILEBitstream&);
};

class Vorbis::Codebook // 24 bytes
{
	double float32_unpack(unsigned long);

public:
	std::vector< double > VQ;
	unsigned int nVQ{ 0 }; // "dimensions" in spec
	Huffman< unsigned long >* vqbook{ nullptr };
	Huffman< unsigned long >* scalarbook{ nullptr };

	Codebook();
	~Codebook();

	Codebook(const Codebook&);
	Codebook& operator=(const Codebook&);

	void unserialize(ILEBitstream&);
};

class Vorbis::Residue // 1064 bytes
{
public:
	unsigned char books[64][8];
	bool books_used[64][8];
	unsigned long begin{ 0 };
	unsigned long end{ 0 };
	unsigned long partition_size{ 0 };
	unsigned int type{ 0 };
	unsigned char classifications{ 0 };
	unsigned char classbook{ 0 };

	Residue();
	Residue(const Residue&);

	Residue& operator=(const Residue&);

	void unserialize(ILEBitstream&);
};

class Vorbis::Mapping // 808 bytes
{
	unsigned char submap_floor_[16];

public:
	std::vector< unsigned char > mag;
	std::vector< unsigned char > ang;
	std::vector< unsigned char > mux;
	unsigned char submap_residue[16];
	unsigned int coupling_steps{ 0 };
	unsigned char submaps{ 0 };
	unsigned ch{ 0 };
	unsigned residue_to_resvec[256]; // TODO: Can reduce the size a lot
	unsigned submapch[16];

	Mapping();
	Mapping(const Mapping&);
	~Mapping();

	Mapping& operator=(const Mapping&);

	void unserialize(ILEBitstream&, unsigned);
	unsigned char submap_floor(unsigned) const;
};

class Vorbis::Mode // 12 bytes
{
public:
	// unsigned int  window_type;     don't be a piggy
	// unsigned int  transform_type;  don't be a piggy
	unsigned char mapping;
	bool blockflag;

	void unserialize(ILEBitstream&);
};

const double inverse_db_table[256] = { 1.0649863e-07, 1.1341951e-07, 1.2079015e-07, 1.2863978e-07,
				       1.3699951e-07, 1.4590251e-07, 1.5538408e-07, 1.6548181e-07,
				       1.7623575e-07, 1.8768855e-07, 1.9988561e-07, 2.1287530e-07,
				       2.2670913e-07, 2.4144197e-07, 2.5713223e-07, 2.7384213e-07,
				       2.9163793e-07, 3.1059021e-07, 3.3077411e-07, 3.5226968e-07,
				       3.7516214e-07, 3.9954229e-07, 4.2550680e-07, 4.5315863e-07,
				       4.8260743e-07, 5.1396998e-07, 5.4737065e-07, 5.8294187e-07,
				       6.2082472e-07, 6.6116941e-07, 7.0413592e-07, 7.4989464e-07,
				       7.9862701e-07, 8.5052630e-07, 9.0579828e-07, 9.6466216e-07,
				       1.0273513e-06, 1.0941144e-06, 1.1652161e-06, 1.2409384e-06,
				       1.3215816e-06, 1.4074654e-06, 1.4989305e-06, 1.5963394e-06,
				       1.7000785e-06, 1.8105592e-06, 1.9282195e-06, 2.0535261e-06,
				       2.1869758e-06, 2.3290978e-06, 2.4804557e-06, 2.6416497e-06,
				       2.8133190e-06, 2.9961443e-06, 3.1908506e-06, 3.3982101e-06,
				       3.6190449e-06, 3.8542308e-06, 4.1047004e-06, 4.3714470e-06,
				       4.6555282e-06, 4.9580707e-06, 5.2802740e-06, 5.6234160e-06,
				       5.9888572e-06, 6.3780469e-06, 6.7925283e-06, 7.2339451e-06,
				       7.7040476e-06, 8.2047000e-06, 8.7378876e-06, 9.3057248e-06,
				       9.9104632e-06, 1.0554501e-05, 1.1240392e-05, 1.1970856e-05,
				       1.2748789e-05, 1.3577278e-05, 1.4459606e-05, 1.5399272e-05,
				       1.6400004e-05, 1.7465768e-05, 1.8600792e-05, 1.9809576e-05,
				       2.1096914e-05, 2.2467911e-05, 2.3928002e-05, 2.5482978e-05,
				       2.7139006e-05, 2.8902651e-05, 3.0780908e-05, 3.2781225e-05,
				       3.4911534e-05, 3.7180282e-05, 3.9596466e-05, 4.2169667e-05,
				       4.4910090e-05, 4.7828601e-05, 5.0936773e-05, 5.4246931e-05,
				       5.7772202e-05, 6.1526565e-05, 6.5524908e-05, 6.9783085e-05,
				       7.4317983e-05, 7.9147585e-05, 8.4291040e-05, 8.9768747e-05,
				       9.5602426e-05, 0.00010181521, 0.00010843174, 0.00011547824,
				       0.00012298267, 0.00013097477, 0.00013948625, 0.00014855085,
				       0.00015820453, 0.00016848555, 0.00017943469, 0.00019109536,
				       0.00020351382, 0.00021673929, 0.00023082423, 0.00024582449,
				       0.00026179955, 0.00027881276, 0.00029693158, 0.00031622787,
				       0.00033677814, 0.00035866388, 0.00038197188, 0.00040679456,
				       0.00043323036, 0.00046138411, 0.00049136745, 0.00052329927,
				       0.00055730621, 0.00059352311, 0.00063209358, 0.00067317058,
				       0.00071691700, 0.00076350630, 0.00081312324, 0.00086596457,
				       0.00092223983, 0.00098217216, 0.0010459992,  0.0011139742,
				       0.0011863665,  0.0012634633,  0.0013455702,  0.0014330129,
				       0.0015261382,  0.0016253153,  0.0017309374,  0.0018434235,
				       0.0019632195,  0.0020908006,  0.0022266726,  0.0023713743,
				       0.0025254795,  0.0026895994,  0.0028643847,  0.0030505286,
				       0.0032487691,  0.0034598925,  0.0036847358,  0.0039241906,
				       0.0041792066,  0.0044507950,  0.0047400328,  0.0050480668,
				       0.0053761186,  0.0057254891,  0.0060975636,  0.0064938176,
				       0.0069158225,  0.0073652516,  0.0078438871,  0.0083536271,
				       0.0088964928,  0.009474637,   0.010090352,   0.010746080,
				       0.011444421,   0.012188144,   0.012980198,   0.013823725,
				       0.014722068,   0.015678791,   0.016697687,   0.017782797,
				       0.018938423,   0.020169149,   0.021479854,   0.022875735,
				       0.024362330,   0.025945531,   0.027631618,   0.029427276,
				       0.031339626,   0.033376252,   0.035545228,   0.037855157,
				       0.040315199,   0.042935108,   0.045725273,   0.048696758,
				       0.051861348,   0.055231591,   0.058820850,   0.062643361,
				       0.066714279,   0.071049749,   0.075666962,   0.080584227,
				       0.085821044,   0.091398179,   0.097337747,   0.10366330,
				       0.11039993,    0.11757434,    0.12521498,    0.13335215,
				       0.14201813,    0.15124727,    0.16107617,    0.17154380,
				       0.18269168,    0.19456402,    0.20720788,    0.22067342,
				       0.23501402,    0.25028656,    0.26655159,    0.28387361,
				       0.30232132,    0.32196786,    0.34289114,    0.36517414,
				       0.38890521,    0.41417847,    0.44109412,    0.46975890,
				       0.50028648,    0.53279791,    0.56742212,    0.60429640,
				       0.64356699,    0.68538959,    0.72993007,    0.77736504,
				       0.82788260,    0.88168307,    0.9389798,	    1. };

#endif // #ifndef VORBIS_HPP
