#include "crc.h"

#include <array>
#include <stdexcept>

namespace
{
constexpr chest::crc::poly32 precomputed_crc_element(
	chest::crc::poly32 p,
	chest::crc::poly32 i
)
{
	chest::crc::poly32 y = i;

	for (int j = 0; j < 8; j++)
	{
		y = ( y & 1 ) != 0u ? ( ( y >> 1 ) ^ p ) : ( y >> 1 );
	}

	return y & UINT32_C(0xFFFFFFFF);
}

template< std::size_t... I >
constexpr std::array< chest::crc::poly32, 256 > create_precomputed_crc_table_helper(
	chest::crc::poly32 p,
	std::index_sequence< I... >
)
{
	return {{ precomputed_crc_element(p, I)... }};
}

/* Consider creating the acceleration table using gray codes. Instead of counting
 * from 0 to 255, go by binary reflected gray codes. Since only one bit changes,
 * the "next" table value will be a simple xor away from the last table value.
 */
constexpr std::array< chest::crc::poly32, 256 > create_precomputed_crc_table(chest::crc::poly32 p)
{
	return create_precomputed_crc_table_helper(p, std::make_index_sequence< 256 >{});
}

constexpr chest::crc::poly32 precomputed_reverse_crc_element(
	chest::crc::poly32 p,
	chest::crc::poly32 i
)
{
	chest::crc::poly32 y = i << 24;

	for (int j = 0; j < 8; j++)
	{
		y = ( y & UINT32_C(0x80000000) ) != 0u ? ( ( y << 1 ) ^ p ) : ( y << 1 );
	}

	return y & UINT32_C(0xFFFFFFFF);
}

template< std::size_t... I >
constexpr std::array< chest::crc::poly32, 256 > create_precomputed_reverse_crc_table_helper(
	chest::crc::poly32 p,
	std::index_sequence< I... >
)
{
	return {{ precomputed_reverse_crc_element(p, I)... }};
}

constexpr std::array< chest::crc::poly32, 256 > create_precomputed_reverse_crc_table(chest::crc::poly32 p)
{
	return create_precomputed_reverse_crc_table_helper(p, std::make_index_sequence< 256 >() );
}

constexpr std::uint_least32_t crc32_inner(
	const std::byte*   data,
	std::size_t        len,
	chest::crc::poly32 p,
	chest::crc::flags  f
)
{
	if ( data == nullptr )
	{
		throw std::invalid_argument("Argument cannot be null");
	}

	std::uint_least32_t x
	    = (f& chest::crc::flags::precondition) != chest::crc::flags::none ? UINT32_C(0xFFFFFFFF) : 0;

	if ( (f& chest::crc::flags::reverse) != chest::crc::flags::none )
	{
		const std::array< chest::crc::poly32, 256 > crctable = create_precomputed_crc_table(p);

		for (std::size_t i = 0; i < len; i++)
		{
			x = ( crctable[( x ^ std::to_integer< std::uint_least32_t >(data[i]) ) & 0xff] ^ ( x >> 8 ) )
			    & UINT32_C(0xFFFFFFFF);
		}
	}
	else
	{
		const std::array< chest::crc::poly32, 256 > crctable = create_precomputed_reverse_crc_table(p);

		for (std::size_t i = 0; i < len; i++)
		{
			x = ( crctable[( ( x >> 24 ) ^ std::to_integer< std::uint_least32_t >(data[i]) ) & 0xFF] ^ ( x << 8 ) )
			    & UINT32_C(0xFFFFFFFF);
		}
	}

	return (f& chest::crc::flags::postcondition) != chest::crc::flags::none ? ( x ^ UINT32_C(0xFFFFFFFF) ) : x;
}

}

namespace chest
{
std::uint_least32_t crc32(
	const std::byte* data,
	std::size_t      len,
	crc::poly32      p,
	crc::flags       f
)
{
	return ::crc32_inner(data, len, p, f);
}

}
