/** @brief Cyclic Redundancy Checks
 * @todo Adler32
 * @todo 64-bit CRCs?
 * @todo 8-bit CRCs?
 * @todo Iterator and stream variations
 * @todo Class that supports resumable computations, for example
 * @todo Bit-precise variation
 * @todo constexpr the table generation and one-shot function
 * @todo provide tables for ieee 802.3, at least
 * @todo Wikipedia has a whole list of CRCs
 */
#ifndef CRC_HPP
#define CRC_HPP

#include <cstddef>
#include <cstdint>
#include <type_traits>

namespace chest
{
namespace crc
{
/** @brief Options for calculating a CRC
 */
enum class flags
{
	none = 0,
	precondition = 1,                             ///< The CRC calculation begins with 0xffffffff instead of 0
	postcondition = 2,                            ///< The final CRC is xor'd with 0xffffffff
	reverse = 4,                                  ///< Bits are consumed in "reverse" order
	zip = precondition | postcondition | reverse, ///< Options used by ZIP
	png = precondition | postcondition | reverse, ///< Options used by PNG
	ogg = none                                    ///< Options used by OGG
};

using poly32 = std::uint_least32_t;

constexpr flags operator|(
	flags l,
	flags r
)
{
	using T = std::underlying_type_t< flags >;

	return static_cast< flags >( static_cast< T >( l ) | static_cast< T >( r ) );
}

constexpr flags operator&(
	flags l,
	flags r
)
{
	using T = std::underlying_type_t< flags >;

	return static_cast< flags >( static_cast< T >( l ) & static_cast< T >( r ) );
}

constexpr poly32 ieee_802_3 = UINT32_C(0x04c11db7);
constexpr poly32 ogg        = UINT32_C(0x04c11db7);
constexpr poly32 png        = UINT32_C(0xedb88320);
constexpr poly32 zip        = UINT32_C(0xedb88320);
}

/** @brief Calculate the Cyclic Redundancy Check of the given data
 * @param data Pointer to input data to calculate crc for
 * @param len Size of the input data
 * @param p Polynomial to use for the CRC
 * @param f Flags
 *
 * The polynomial used is specified by the third parameter. There are a few common ones defined:
 *
 * IEEE 802.3 specifies a 32-bit polynomial 0x04c11db7. This can be specified using poly::ieee_802_3. Since
 * this value is also used by Ogg, the value poly::ogg may also be specified.
 *
 * PNG uses another polynomial, 0xedb88320. Specify this polynomial with poly::png.
 *
 * Finally, CRCs are often called with pre- or post-whitening of the input, or in a bit reversed order.
 * Specify the whitening by or'ing flags::precondition or flags::postcondition into the flags.
 * For bit reversal, use flags::reverse as well.
 *
 * For example, the PNG specification uses both pre- and post-whitening, and interprets the bits in reverse
 * order from the default here. Ogg, however, uses no whitening and expects the bits in the default order.
 *
 */
std::uint_least32_t crc32(const std::byte* data, std::size_t len, crc::poly32 p, crc::flags f);
}

#endif // ifndef CRC_HPP
