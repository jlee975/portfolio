/* =============================================================================

   TODO: Display palette images, esp. for cellular automata and similar things
   which could save us an unnecessary copy.

   TODO: Code is basically a mess and should take advantage of the RGBA etc classes
      instead of trying to do the math ourselves
   ==============================================================================*/
#ifndef NDEBUG
#include <iostream>
#endif

#include <algorithm>
#include <climits>
#include <fstream>
#include <stdexcept>

#include "endian/endian.h"

#include "image.hpp"

/** ************************************************************************/ /**
 *******************************************************************************/
Image::Image() = default;

Image::Image(
	long             x,
	long             y,
	pixel< RGBA16 >* pixel_data
)
	: w_(x), h_(y)
{
	layersx.emplace_back(new Layer(x, y, pixel_data) );
}

// TODO: Constructor with dimensions and color data

void Image::SetCanvasDimensions(
	long ww,
	long hh
)
{
	if ( ww < 0 || hh < 0 )
	{
		throw std::invalid_argument("Image dimensions cannot be negative");
	}

	w_ = ww;
	h_ = hh;
	// TODO: extend background??
}

unsigned Image::AddLayer(
	long                 x,
	long                 y,
	cloning_ptr< Layer > lx
)
{
	const std::size_t i = layersx.size();

	layersx.emplace_back(std::move(lx) );
	layersx.back()->move(x, y);

	long ww = w_;
	long hh = h_;

	for (const auto& p : layersx)
	{
		if ( p->x() + p->width() > ww )
		{
			ww = p->x() + p->width();
		}

		if ( p->y() + p->height() > hh )
		{
			hh = p->y() + p->height();
		}
	}

	w_ = ww;
	h_ = hh;

	return i;
}

unsigned Image::AddLayer(
	long x,
	long y,
	long ww,
	long hh
)
{
	return AddLayer(x, y, cloning_ptr< Layer >(new Layer(ww, hh) ) );
}

unsigned Image::DuplicateLayer(unsigned l)
{
	const std::size_t i = layersx.size();
	auto              t = layersx.at(l);

	layersx.emplace_back(std::move(t) );
	return i;
}

void Image::NegateLayer(unsigned l)
{
	if ( l < layersx.size() )
	{
		layersx.at(l)->invert();
	}
}

void Image::SetPixel(
	unsigned               l,
	long                   x,
	long                   y,
	const pixel< RGBA16 >& p
)
{
	if ( l < layersx.size() )
	{
		layersx.at(l)->setPixel(x, y, p);
	}
}

// TODO: clean this up by writing RGBA addition, subtraction, and cast to UL
unsigned long Image::GetLayerPixel(
	unsigned l,
	long     x,
	long     y
) const
{
	double r = 0.0;

	double g = 0.0;

	double b = 0.0;

	if ( ( ( ( x / 8 ) + ( y / 8 ) ) & 1 ) == 0 ) // Fall-back checkerboard pattern
	{
		r = g = b = 0.4;
	}
	else
	{
		r = g = b = 0.6;
	}

	double a = ( static_cast< double >( IMAGE_ALPHA_TRANSPARENT - layersx.at(l)->alpha() )
	             / static_cast< double >( IMAGE_ALPHA_TRANSPARENT - IMAGE_ALPHA_OPAQUE ) );

	if ( a < 0.0 )
	{
		a = 0.0;
	}

	if ( a > 1.0 )
	{
		a = 1.0;
	}

	if ( l < layersx.size() )
	{
		pixel< RGBA16 > p = layersx.at(l)->getPixel(x, y);

		switch ( layersx.at(l)->mode() )
		{
		case Layer::Normal:
		default: // mix using layer alpha
			// TODO: Channel range conversion should be a macro
			// or something. Actually, class operator on Pixel class
			b += a * ( ( static_cast< double >( p[2] ) ) / 65535.0 - b );
			g += a * ( ( static_cast< double >( p[1] ) ) / 65535.0 - g );
			r += a * ( ( static_cast< double >( p[0] ) ) / 65535.0 - r );
		}
	}

	r *= 255.0;

	if ( r < 0.0 )
	{
		r = 0.0;
	}

	if ( r > 255.0 )
	{
		r = 255.0;
	}

	g *= 255.0;

	if ( g < 0.0 )
	{
		g = 0.0;
	}

	if ( g > 255.0 )
	{
		g = 255.0;
	}

	b *= 255.0;

	if ( b < 0.0 )
	{
		b = 0.0;
	}

	if ( b > 255.0 )
	{
		b = 255.0;
	}

	return ( ( static_cast< unsigned long >( r ) ) << 16 & UINT32_C(0xFF0000) )
	       + ( ( static_cast< unsigned long >( g ) ) << 8 & UINT32_C(0xFF00) )
	       + ( ( static_cast< unsigned long >( b ) ) & UINT32_C(0xFF) );
}

pixel< RGBA16 > Image::GetLayerPixel16(
	unsigned l,
	long     x,
	long     y
) const
{
	pixel< RGBA16 > val(0, 0, 0, 65535);

	if ( l < layersx.size() )
	{
		val = layersx.at(l)->getPixel(x, y);
	}

	return val;
}

unsigned long Image::GetPixel(
	// returns 0xRRGGBB
	long x,
	long y
) const
{
	double r = 0.0;

	double g = 0.0;

	double b = 0.0;

	if ( ( ( ( x / 8 ) + ( y / 8 ) ) & 1 ) == 0 ) // Fall-back checkerboard pattern
	{
		r = g = b = 0.4;
	}
	else
	{
		r = g = b = 0.6;
	}

	for (const auto& i : layersx)
	{
		double a = ( static_cast< double >( IMAGE_ALPHA_TRANSPARENT - i->alpha() )
		             / static_cast< double >( IMAGE_ALPHA_TRANSPARENT - IMAGE_ALPHA_OPAQUE ) );

		if ( a < 0.0 )
		{
			a = 0.0;
		}

		if ( a > 1.0 )
		{
			a = 1.0;
		}

		{
			pixel< RGBA16 > p = i->getPixel(x, y);
			a *= ( 1.0 - ( static_cast< double >( p[3] ) / 65535.0 ) );

			switch ( i->mode() )
			{
			case Layer::Normal:
			default: // mix using layer alpha
				// TODO: Channel range conversion should be a macro
				// or something
				b = a * ( ( static_cast< double >( p[2] ) ) / 65535.0 ) + ( 1.0 - a ) * b;
				g = a * ( ( static_cast< double >( p[1] ) ) / 65535.0 ) + ( 1.0 - a ) * g;
				r = a * ( ( static_cast< double >( p[0] ) ) / 65535.0 ) + ( 1.0 - a ) * r;
			}
		}
	}

	r *= 255.0;

	if ( r < 0.0 )
	{
		r = 0.0;
	}

	if ( r > 255.0 )
	{
		r = 255.0;
	}

	g *= 255.0;

	if ( g < 0.0 )
	{
		g = 0.0;
	}

	if ( g > 255.0 )
	{
		g = 255.0;
	}

	b *= 255.0;

	if ( b < 0.0 )
	{
		b = 0.0;
	}

	if ( b > 255.0 )
	{
		b = 255.0;
	}

	return ( ( ( static_cast< unsigned long >( r ) ) << 16 ) & UINT32_C(0xFF0000) )
	       + ( ( ( static_cast< unsigned long >( g ) ) << 8 ) & UINT32_C(0xFF00) )
	       + ( ( static_cast< unsigned long >( b ) ) & UINT32_C(0xFF) );
}

void Image::SetLayerAlpha(
	unsigned l,
	double   a
)
{
	double a2 = a;

	if ( a2 < 0.0 )
	{
		a2 = 0.0;
	}

	if ( a2 > 1.0 )
	{
		a2 = 1.0;
	}

	a2 = a2 * ( IMAGE_ALPHA_TRANSPARENT - IMAGE_ALPHA_OPAQUE ) + IMAGE_ALPHA_OPAQUE;

	if ( l < layersx.size() )
	{
		layersx.at(l)->alpha(static_cast< unsigned short >( a2 ) );
	}
}

void Image::SetLayerMode(
	unsigned    l,
	Layer::Mode m
)
{
	if ( l < layersx.size() )
	{
		layersx.at(l)->mode(m);
	}
}

void Image::fill(
	unsigned               l,
	const pixel< RGBA16 >& color
)
{
	if ( l < layersx.size() )
	{
		layersx.at(l)->fill(color);
	}
}

void Image::Threshold(
	unsigned l,
	double   lo,
	double   hi
)
{
	if ( l < layersx.size() )
	{
		layersx.at(l)->threshold(lo, hi);
	}
}

/* An example blur function
   float blur[25] = {
    0.3536, 0.4472, 0.5, 0.4472, 0.3536,
    0.4472, 0.7071, 1.0, 0.7071, 0.4472,
    0.5,    1.0,    1.0, 1.0,    0.5,
    0.4472, 0.7071, 1.0, 0.7071, 0.4472,
    0.3536, 0.4472, 0.5, 0.4472, 0.3536
   };
   // */

void Image::Filter(
	unsigned l,
	double*  f,
	int      fx,
	int      fy
)
{
	if ( l < layersx.size() )
	{
		layersx.at(l)->filter(f, fx, fy);
	}
}

void Image::Transform(
	unsigned l,
	pixel< RGBA16 >( *f ) (const pixel< RGBA16 >&) // apply f to every pixel in l
)
{
	if ( l < layersx.size() )
	{
		layersx.at(l)->transform(f);
	}
}

long Image::GetLayerWidth(unsigned l) const
{
	return l < layersx.size() ? layersx.at(l)->width() : 0;
}

long Image::GetLayerHeight(unsigned l) const
{
	return l < layersx.size() ? layersx.at(l)->height() : 0;
}

unsigned long Image::GetLayerAverage(
	unsigned l,
	long     x,
	long     y,
	long     d
) const
{
	return l < layersx.size() ? layersx.at(l)->average(x, y, d) : 0;
}

long Image::height() const
{
	return h_;
}

long Image::width() const
{
	return w_;
}

// TODO: separate into bitmap class.
void Image::SaveBMP(const char* filename)
{
	// TODO: Split this following bit off into it's own BMP class...
	unsigned char bmpfileheader[14] = { 66, 77, 0, 0, 0, 0, 0, 0, 0, 0, 54, 0, 0, 0 };
	unsigned char dh[40]; // dibheader

	lewrite< 32 >(dh, 40);                                                // size of dibheader
	lewrite< 32 >(dh + 4, static_cast< unsigned long >( w_ ) );           // width
	lewrite< 32 >(dh + 8, static_cast< unsigned long >( h_ ) );           // height
	lewrite< 16 >(dh + 12, 1);                                            // number of colour planes
	lewrite< 16 >(dh + 14, 32);                                           // colour depth
	lewrite< 32 >(dh + 16, 0);                                            // compression method
	lewrite< 32 >(dh + 20, static_cast< unsigned long >( 4 * w_ * h_ ) ); // size of the BMP data
	lewrite< 32 >(dh + 24, 0);                                            // horizontal resolution
	lewrite< 32 >(dh + 28, 0);                                            // vertical resolution
	lewrite< 32 >(dh + 32, 0);                                            // # colours in the palette
	lewrite< 32 >(dh + 36, 0);                                            // # important colours in palette

	auto* bmpdata = new unsigned char [w_ * h_ * 4];

	for (long i = 0; i < h_; ++i)
	{
		for (long j = 0; j < w_; ++j)
		{
			unsigned long c = GetPixel(j, i);
			bmpdata[4 * ( i * w_ + j )]     = static_cast< unsigned char >( ( c ) & 0xFF );       // b
			bmpdata[4 * ( i * w_ + j ) + 1] = static_cast< unsigned char >( ( c >> 8 ) & 0xFF );  // g
			bmpdata[4 * ( i * w_ + j ) + 2] = static_cast< unsigned char >( ( c >> 16 ) & 0xFF ); // r
			bmpdata[4 * ( i * w_ + j ) + 3] = 0;                                                  // TODO: alpha
		}
	}

	std::ofstream out(filename, std::ios_base::out | std::ios_base::binary | std::ios_base::trunc);

	if ( out.is_open() ) // FIXME: good ole' reinterpret_cast
	{
		out.write(reinterpret_cast< char* >( bmpfileheader ), 14);
		out.write(reinterpret_cast< char* >( dh ), 40);
		/// \todo the following static_cast is questionable
		out.write(reinterpret_cast< char* >( bmpdata ), static_cast< long >( 4 * w_ * h_ ) );
		out.close();
	}

	delete[] bmpdata;
}
