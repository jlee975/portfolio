#ifndef PIXELS_H
#define PIXELS_H

#include "endian/endian.h"
#include "pixel.h"
#include <functional>
#include <stdexcept>
#include <vector>

/** @brief Raw storage of pixel data
 *
 * This is a big array of size height * width * number of channels, of the raw channel type. This type can
 * store any supported pixel type. It exists so, for example, a PNG class doesn't have to be templated on the
 * pixel format.
 */
class pixel_storage
{
public:
	pixel_storage() = default;

	pixel_storage(const pixel_storage&) = delete;

	pixel_storage(pixel_storage&& o) noexcept
		: data(o.data), height_(o.height_), width_(o.width_), config(o.config)
	{
		o.data    = nullptr;
		o.height_ = 0;
		o.width_  = 0;
		o.config  = RGB8;
	}

	pixel_storage(
		void*                p_,
		std::size_t          w,
		std::size_t          h,
		colour_configuration X
	)
		: data(p_), height_(h), width_(w), config(X)
	{
	}

	~pixel_storage()
	{
		if ( data != nullptr )
		{
			switch ( config )
			{
			case RGB8:
				delete_helper< RGB8 >();
				break;
			case RGB16:
				delete_helper< RGB16 >();
				break;
			case RGBA8:
				delete_helper< RGBA8 >();
				break;
			case RGBA16:
				delete_helper< RGBA16 >();
				break;
			}
		}
	}

	pixel_storage& operator=(const pixel_storage&) = delete;

	pixel_storage& operator=(pixel_storage&& o) noexcept
	{
		std::swap(data, o.data);
		std::swap(height_, o.height_);
		std::swap(width_, o.width_);
		std::swap(config, o.config);
		return *this;
	}

	std::size_t width() const
	{
		return width_;
	}

	std::size_t height() const
	{
		return height_;
	}

	const void* get() const
	{
		return data;
	}

	pixel_storage convert(colour_configuration to) const
	{
		switch ( to )
		{
		case RGB8:
			return convert< RGB8 >();

		case RGB16:
			return convert< RGB16 >();

		case RGBA8:
			return convert< RGBA8 >();

		case RGBA16:
			return convert< RGBA16 >();
		}

		throw std::logic_error("Unknown colour configuration");
	}

	template< colour_configuration To >
	pixel_storage convert() const
	{
		switch ( config )
		{
		case RGB8:
			return convert_loop< RGB8, To >();

		case RGB16:
			return convert_loop< RGB16, To >();

		case RGBA8:
			return convert_loop< RGBA8, To >();

		case RGBA16:
			return convert_loop< RGBA16, To >();
		}

		throw std::logic_error("Unknown colour configuration");
	}
private:
	using convertfn = pixel_storage(const pixel_storage&, colour_configuration);
	using deletefn  = void ( void* );

	template< colour_configuration X >
	void delete_helper()
	{
		using T = typename colour_configuration_traits< X >::raw;
		delete[] static_cast< T* >( data );
	}

	template< colour_configuration From, colour_configuration To >
	static void convert_pixel(
		const typename colour_configuration_traits< From >::raw* from,
		typename colour_configuration_traits< To >::raw*         to
	)
	{
		if ( From != To )
		{
			throw std::logic_error("Not implemented");
		}

		for (unsigned i = 0; i < colour_configuration_traits< From >::num_channels; ++i)
		{
			to[i] = from[i];
		}
	}

	template< colour_configuration From, colour_configuration To >
	pixel_storage convert_loop() const
	{
		using T = typename colour_configuration_traits< To >::raw;
		using U = typename colour_configuration_traits< From >::raw;

		const unsigned n = colour_configuration_traits< To >::num_channels;
		const unsigned m = colour_configuration_traits< From >::num_channels;

		T*       t = new T[width_ * height_ * n];
		const U* u = static_cast< const U* >( data );

		for (std::size_t i = 0; i < height_; ++i)
		{
			for (std::size_t j = 0; j < width_; ++j)
			{
				convert_pixel< From, To >(u + ( i * width_ + j ) * m, t + ( i * width_ + j ) * n);
			}
		}

		return pixel_storage(t, width_, height_, To);
	}

	void*                data{ nullptr };
	std::size_t          height_{ 0 };
	std::size_t          width_{ 0 };
	colour_configuration config{};
};

template< colour_configuration X >
class const_pixel_reference
{
	static constexpr colour_model C = colour_configuration_traits< X >::model;
	static constexpr unsigned     N = colour_configuration_traits< X >::num_channels;
	using T = typename colour_configuration_traits< X >::raw;
public:
	explicit const_pixel_reference(const T* p_)
		: p(p_)
	{
	}

	const T& operator[](std::size_t i) const
	{
		return p[i];
	}

	template< typename U >
	explicit operator pixel_< C, U, N >( ) const
	{
		return pixel< X >(p[0], p[1], p[2], p[3]);
	}
private:
	const T* p;
};

template< colour_configuration X >
class pixel_reference
{
	static constexpr colour_model C = colour_configuration_traits< X >::model;
	static constexpr unsigned     N = colour_configuration_traits< X >::num_channels;
	using T = typename colour_configuration_traits< X >::raw;
public:
	explicit pixel_reference(T* p_)
		: p(p_)
	{
	}

	template< colour_configuration Y >
	pixel_reference& operator=(const pixel_reference< Y >& o)
	{
		/// @todo The intermediate is unfortunate
		pixel< X > t(o);

		p[0] = t[0];
		p[1] = t[1];
		p[2] = t[2];
		p[3] = t[3];
		return *this;
	}

	pixel_reference& operator=(const pixel< X >& t)
	{
		p[0] = t[0];
		p[1] = t[1];
		p[2] = t[2];
		p[3] = t[3];
		return *this;
	}

	template< typename U >
	explicit operator pixel_< C, U, N >( ) const
	{
		return pixel< X >(p[0], p[1], p[2], p[3]);
	}

	T& operator[](std::size_t i)
	{
		return p[i];
	}

	const T& operator[](std::size_t i) const
	{
		return p[i];
	}

	pixel_reference& operator+=(const pixel_reference& o)
	{
		for (unsigned i = 0; i < N; ++i)
		{
			p[i] += o.p[i];
		}

		return *this;
	}

	pixel_reference& operator+=(const const_pixel_reference< X >& o)
	{
		for (unsigned i = 0; i < N; ++i)
		{
			p[i] += o[i];
		}

		return *this;
	}

	pixel_reference& operator+=(const pixel< X >& x)
	{
		return this->operator+=(const_pixel_reference< X >(&x[0]) );
	}

	void invert()
	{
		for (unsigned i = 0; i < N; ++i)
		{
			p[i] = std::numeric_limits< T >::max() - p[i];
		}
	}

	template< typename U >
	friend pixel< X > operator/(
		const pixel_reference& l,
		U                      r
	)
	{
		return pixel< X >(l) / r;
	}
private:
	T* p;
};

/** @brief Configuration-templated pixel storage
 *
 * @todo Take height and width
 * @todo Return pixelref instead of pixel&
 * @todo Convert to pixels_storage and back (void pointer, size info, colour model)
 * @todo Element-wise math
 */
template< colour_configuration X >
class pixels
{
	static constexpr colour_model C = colour_configuration_traits< X >::model;
	static constexpr unsigned     N = colour_configuration_traits< X >::num_channels;
	using T = typename colour_configuration_traits< X >::raw;
public:
	using reference       = pixel_reference< X >;
	using const_reference = const_pixel_reference< X >;

	class iterator
	{
public:
		explicit iterator(T* p_)
			: p(p_)
		{
		}

		friend bool operator!=(
			const iterator& l,
			const iterator& r
		)
		{
			return l.p != r.p;
		}

		iterator& operator++()
		{
			p += N;
			return *this;
		}

		reference operator*()
		{
			return reference(p);
		}
private:
		T* p;
	};

	pixels()
		: data(nullptr)
	{
	}

	pixels(const pixels& o)
		: data(o.size ? new T[o.size] : nullptr), size(o.size), width_(o.width_)
	{
		for (std::size_t i = 0; i < size; ++i)
		{
			data[i] = o.data[i];
		}
	}

	pixels(pixels&& o)
		: data(o.data), size(o.size), width_(o.width_)
	{
		o.data   = nullptr;
		o.size   = 0;
		o.width_ = 0;
	}

	pixels(
		std::size_t w,
		std::size_t h
	)
		: data(new T[w * h * N]), size(w * h * N), width_(w)
	{
	}

	pixels(
		std::size_t       w,
		std::size_t       h,
		const pixel< X >& p
	)
		: pixels(
			w,
			h
		)
	{
		for (std::size_t j = 0; j < w * h; ++j)
		{
			for (unsigned i = 0; i < N; ++i)
			{
				data[j * N + i] = p[i];
			}
		}
	}

	~pixels()
	{
		delete[] data;
	}

	pixels& operator=(const pixels& o)
	{
		pixels t(o);

		swap(t);
		return *this;
	}

	pixels& operator=(pixels&& o) noexcept
	{
		data     = o.data;
		size     = o.size;
		width_   = o.width_;
		o.data   = nullptr;
		o.size   = 0;
		o.width_ = 0;
		return *this;
	}

	/// @todo Almost certainly won't need this anymore
	void assign(
		std::size_t       w,
		std::size_t       h,
		const pixel< X >* p,
		const pixel< X >* q
	)
	{
		if ( q - p < 0 || static_cast< std::size_t >( q - p ) != w * h )
		{
			throw std::runtime_error("Parameters do not agree");
		}

		if ( size < w * h * N )
		{
			delete[] data;

			data = new T[w * h * N];
			size = w * h * N;
		}

		for (std::size_t j = 0; j < w * h; ++j)
		{
			for (unsigned i = 0; i < N; ++i)
			{
				data[j * N + i] = p[j][i];
			}
		}

		width_ = w;
	}

	void swap(pixels& o)
	{
		std::swap(data, o.data);
		std::swap(size, o.size);
		std::swap(width_, o.width_);
	}

	iterator begin()
	{
		return iterator(data);
	}

	iterator end()
	{
		return iterator(data + size);
	}

	std::size_t width() const
	{
		return width_;
	}

	std::size_t height() const
	{
		return width_ != 0 ? ( size / width_ ) / N : 0;
	}

	reference operator[](std::size_t i)
	{
		return reference(data + N * i);
	}

	const_reference operator[](std::size_t i) const
	{
		return const_reference(data + N * i);
	}

	pixel_storage storage()
	{
		pixel_storage t(data, width_, height(), X);

		data   = nullptr;
		size   = 0;
		width_ = 0;

		return t;
	}
private:
	/// Raw pixel data, as in pixel_storage
	T* data;

	/// Total size of data, in bytes
	std::size_t size{ 0 };

	/// Number of pixels per line
	std::size_t width_{ 0 };
};

#endif // PIXELS_H
