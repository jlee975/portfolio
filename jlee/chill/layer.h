#ifndef LAYER_H
#define LAYER_H

#include <cstdint>
#include <memory>
#include <vector>

#include "util/clone.h"

#include "colour.h"
#include "pixels.h"

class ILayer
	: public ICloneable
{
public:
	virtual pixel< RGBA16 > getPixel(std::intmax_t, std::intmax_t) const = 0;
};

// Alpha ranges
#define IMAGE_ALPHA_TRANSPARENT 65535
#define IMAGE_ALPHA_OPAQUE      0

// TODO: Combine Layer with RGBDATA tacked to the end
// TODO: class-ify
// TODO: privatize
// TODO: manage own memory for rgbadata
class Layer
	: public ILayer
{
public:
	enum Mode
	{
		Normal
	};

	Layer();
	Layer(const Layer&)                                  = default;
	Layer(std::intmax_t, std::intmax_t, pixel< RGBA16 >* = nullptr);
	Layer& operator=(const Layer&)                       = default;

	Layer* clone() const override;
	void swap(Layer&);

	void move(std::intmax_t, std::intmax_t);
	void invert();
	void setPixel(std::intmax_t, std::intmax_t, const pixel< RGBA16 >&);
	pixel< RGBA16 > getPixel(std::intmax_t, std::intmax_t) const override;
	void fill(const pixel< RGBA16 >&);
	void filter(const double*, std::uintmax_t, std::uintmax_t);
	void threshold(double, double);
	void transform(pixel< RGBA16 >( * ) (const pixel< RGBA16 >&) );

	std::intmax_t x() const;
	std::intmax_t y() const;
	std::intmax_t width() const;
	std::intmax_t height() const;
	Mode mode() const;
	void mode(Mode);
	unsigned short alpha() const;
	void alpha(unsigned short);

	unsigned long average(std::intmax_t, std::intmax_t, std::intmax_t) const;
	std::shared_ptr< unsigned char[] > convert(colour_configuration) const;
private:
	pixels< RGBA16 > data;
	std::intmax_t    x_;
	std::intmax_t    y_;
	Mode             mode_;
	unsigned short   alpha_;
};

#endif // LAYER_H
