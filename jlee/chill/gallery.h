#ifndef TEXTURELIBRARY_HPP
#define TEXTURELIBRARY_HPP

#include <filesystem>
#include <memory>
#include <string>
#include <vector>

#include "util/index_type.h"

#include "pixels.h"

struct texture_id_tag
{
};

using texture_id = index_type< std::size_t, texture_id_tag >;
const texture_id INVALID_TEXTURE(-1);

/** @todo Cache loaded files. Unload them as needed
 * @todo Don't return Raw, but pointer to internal cache
 * @todo Move to chill module
 */
class Gallery
{
public:
	Gallery(std::initializer_list< std::filesystem::path >);

	/** @brief Get the raw data of the image identified by tid
	 * @param tid Identifies texture
	 * @param format Formats supported by caller
	 *
	 * @todo This should really return a shared pointer, and prompt reading the image on the first access
	 */
	pixel_storage getBitmap(texture_id tid, colour_configuration format) const;
	texture_id load(const std::string&);
private:
	// Textures that we can automatically generate
	enum gen_t
	{
		File ///< Load from file
	};

	struct entry
	{
		std::string name;
		std::size_t path;
	};

	std::vector< std::filesystem::path > paths;
	std::vector< entry >                 textures;
};

#endif // TEXTURELIBRARY_HPP
