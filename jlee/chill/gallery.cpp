/**
 * @todo This object needs to be thread safe
 */
#include "gallery.h"

#include <stdexcept>

#include "image.hpp"
#include "png/png.hpp"

Gallery::Gallery(std::initializer_list< std::filesystem::path > l)
	: paths(l)
{
}

texture_id Gallery::load(const std::string& s)
{
	// do we already have the image
	for (std::size_t i = 0, n = textures.size(); i < n; ++i)
	{
		if ( textures[i].name == s )
		{
			return texture_id(i);
		}
	}

	// not found, search paths
	for (std::size_t j = 0, n = paths.size(); j < n; ++j)
	{
		if ( std::filesystem::exists(paths[j] / s) )
		{
			const std::size_t i = textures.size();

			textures.push_back(entry{ s, j });

			return texture_id(i);
		}
	}

	return INVALID_TEXTURE;
}

pixel_storage Gallery::getBitmap(
	texture_id           i_,
	colour_configuration X
) const
{
	const std::size_t i = i_.get();

	if ( i < textures.size() )
	{
		/// @todo Cache
		const auto p = paths[textures[i].path] / textures[i].name;
		return PNG::open(p).convert(X);
	}

	return {};
}
