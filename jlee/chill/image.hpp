#ifndef IMAGE_HPP
#define IMAGE_HPP

#include <cstddef>

#include "layer.h"

class Image
{
public:
	Image();
	Image(const Image&)                = default;
	Image(long, long, pixel< RGBA16 >* = nullptr);
	virtual ~Image()                   = default;

	Image& operator=(const Image&) = default;

	void SaveBMP(const char*);

	// Layer functions
	void SetCanvasDimensions(long, long);
	unsigned AddLayer(long, long, cloning_ptr< Layer >);
	unsigned AddLayer(long, long, long, long);
	unsigned DuplicateLayer(unsigned);
	void Filter(unsigned, double*, int, int);
	void SetLayerAlpha(unsigned, double);
	void SetLayerMode(unsigned, Layer::Mode);
	void Threshold(unsigned, double, double);
	void NegateLayer(unsigned);
	void Transform(unsigned, pixel< RGBA16 >( * ) (const pixel< RGBA16 >&) );

	void SetPixel(unsigned, long, long, const pixel< RGBA16 >&);
	unsigned long GetPixel(long, long) const;
	unsigned long GetLayerPixel(unsigned, long, long) const;
	pixel< RGBA16 > GetLayerPixel16(unsigned, long, long) const;
	long GetLayerWidth(unsigned) const;
	long GetLayerHeight(unsigned) const;
	void fill(unsigned, const pixel< RGBA16 >&);
	long width() const;
	long height() const;
	unsigned long GetLayerAverage(unsigned, long, long, long) const;
public:
	long                                w_{ 0 };
	long                                h_{ 0 };
	std::vector< cloning_ptr< Layer > > layersx;
};

#endif // IMAGE_HPP
