/** ************************************************************************/ /**

   A template class for rgba representations
 *******************************************************************************/

#ifndef PIXEL_HPP
#define PIXEL_HPP

#include <limits>

#include "colour.h"

/** @brief Contains a pixel's values
 *
 * One would usually use pixel< C > instead of using this type directly
 *
 * @note This is not a marshalling type. There is no prescribed way to map channel values to memory. Ex.,
 * endianness or channel order
 * @note Alpha == 0 means opaque
 * @todo rgba, yuva, cmyka, etc. should wrap non-alpha versions
 */
template< colour_model, typename, unsigned >
class pixel_;

/** @brief Specialization for RGBA
 */
template< typename T >
class pixel_< RGB, T, 4 >
{
public:
	pixel_() = default;

	pixel_(const pixel_&) = default;

	pixel_(
		T r,
		T g,
		T b
	)
		: v{r, g, b, 0}
	{
	}

	pixel_(
		T r,
		T g,
		T b,
		T a
	)
		: v{r, g, b, a}
	{
	}

	/** Scales bit depth
	 *
	 * 0xYZ -> 0xYZYZ
	 *
	 * @bug Convert 0xff -> 0xff00 or 0xff80 instead of 0xffff. Cons: doesn't produce a pure white,
	 * or (potentially) pure black. Pros: Each colour level maps to a similarly sized block (ex., 256 "levels").
	 */
	template< typename U >
	explicit pixel_(const pixel_< RGB, U, 4 >& p)
	{
		constexpr T tmax = std::numeric_limits< T >::max();
		constexpr U umax = std::numeric_limits< U >::max();

		if constexpr ( tmax >= umax )
		{
			v[0] = ( tmax / umax ) * p[0];
			v[1] = ( tmax / umax ) * p[1];
			v[2] = ( tmax / umax ) * p[2];
			v[3] = ( tmax / umax ) * p[3];
		}
		else
		{
			/// @todo Round properly so conversion is reversible
			v[0] = p[0] / ( umax / tmax );
			v[1] = p[1] / ( umax / tmax );
			v[2] = p[2] / ( umax / tmax );
			v[3] = p[3] / ( umax / tmax );
		}
	}

	pixel_& operator=(const pixel_& x) = default;

	pixel_& set(
		T r,
		T g,
		T b
	)
	{
		v[0] = r, v[1] = g, v[2] = b, v[3] = 0;

		return *this;
	}

	pixel_& set(
		T r,
		T g,
		T b,
		T a
	)
	{
		v[0] = r, v[1] = g, v[2] = b, v[3] = a;
		return *this;
	}

	const T& operator[](std::size_t i) const
	{
		return v[i];
	}

	/// \note Alpha channel is the same as left hand operand
	pixel_ operator+(const pixel_& x) const
	{
		return { v[0] + x.v[0], v[1] + x.v[1], v[2] + x.v[2], v[3] };
	}

	/// \note Alpha channel does not change
	pixel_& operator+=(const pixel_& x)
	{
		v[0] += x.v[0], v[1] += x.v[1], v[2] += x.v[2];
		return *this;
	}

	pixel_& add(
		T r,
		T g,
		T b
	)
	{
		v[0] += r, v[1] += g, v[2] += b;
		return *this;
	}

	pixel_& invert()
	{
		v[0] = std::numeric_limits< T >::max() - v[0], v[1] = std::numeric_limits< T >::max() - v[1],
		v[2] = std::numeric_limits< T >::max() - v[2];
		return *this;
	}

	pixel_ operator/(T x) const
	{
		pixel_ y(v[0] / x, v[1] / x, v[2] / x, v[3]);

		return y;
	}
private:
	T v[4];
};

template< typename T >
class pixel_< RGB, T, 3 >
{
public:
	pixel_() = default;

	pixel_(const pixel_&) = default;

	pixel_(
		T r,
		T g,
		T b
	)
		: v{r, g, b}
	{
	}

	/** Scales bit depth
	 *
	 * 0xYZ -> 0xYZYZ
	 *
	 * @bug Convert 0xff -> 0xff00 or 0xff80 instead of 0xffff. Cons: doesn't produce a pure white,
	 * or (potentially) pure black. Pros: Each colour level maps to a similarly sized block (ex., 256 "levels").
	 */
	template< typename U >
	explicit pixel_(const pixel_< RGB, U, 3 >& p)
	{
		constexpr T tmax = std::numeric_limits< T >::max();
		constexpr U umax = std::numeric_limits< U >::max();

		if constexpr ( tmax >= umax )
		{
			v[0] = ( tmax / umax ) * p[0];
			v[1] = ( tmax / umax ) * p[1];
			v[2] = ( tmax / umax ) * p[2];
		}
		else
		{
			/// @todo Round properly so conversion is reversible
			v[0] = p[0] / ( umax / tmax );
			v[1] = p[1] / ( umax / tmax );
			v[2] = p[2] / ( umax / tmax );
		}
	}

	pixel_& operator=(const pixel_& x) = default;

	pixel_& set(
		T r,
		T g,
		T b
	)
	{
		v[0] = r, v[1] = g, v[2] = b, v[3] = 0;

		return *this;
	}

	const T& operator[](std::size_t i) const
	{
		return v[i];
	}

	/// \note Alpha channel is the same as left hand operand
	pixel_ operator+(const pixel_& x) const
	{
		return { v[0] + x.v[0], v[1] + x.v[1], v[2] + x.v[2] };
	}

	/// \note Alpha channel does not change
	pixel_& operator+=(const pixel_& x)
	{
		v[0] += x.v[0], v[1] += x.v[1], v[2] += x.v[2];
		return *this;
	}

	pixel_& add(
		T r,
		T g,
		T b
	)
	{
		v[0] += r, v[1] += g, v[2] += b;
		return *this;
	}

	pixel_& invert()
	{
		v[0] = std::numeric_limits< T >::max() - v[0], v[1] = std::numeric_limits< T >::max() - v[1],
		v[2] = std::numeric_limits< T >::max() - v[2];
		return *this;
	}

	pixel_ operator/(T x) const
	{
		pixel_ y(v[0] / x, v[1] / x, v[2] / x);

		return y;
	}
private:
	T v[3];
};

/// @todo Should have pixelref and pixels classes instead of individual pixel class. pixelref
/// will just be a pointer within a raw memory block
template< colour_configuration X >
using pixel = pixel_< colour_configuration_traits< X >::model,
                      typename colour_configuration_traits< X >::raw,
                      colour_configuration_traits< X >::num_channels >;

#endif // ifndef PIXEL_HPP
