#ifndef COLOUR_H
#define COLOUR_H

#include <cstdint>

/** @brief Enum for colour spaces
 * @todo greyscale, CMYK, YUV, HSV, index
 */
enum colour_model
{
	RGB ///< 3 Channels for red, green, and blue
};

/** @brief Convenient names for various configurations
 * @todo float, double
 */
enum colour_configuration
{
	RGB8, ///< 8 bits for each of 3 channels
	RGB16,
	RGBA8,
	RGBA16
};

/** @brief Traits for a colour configuration
 *
 * model - The colour model
 * alpha - Whether or not there is an alpha channel
 * bitdepth - bits per channel
 * num_channels - Number of channels from the model, plus the number of alpha channels
 * raw - Type used to store a single channel
 *
 * @todo colour_configuration traits: integer or floating point, saturating math
 * @todo raw Can be calculated
 * @todo num_channels could be calculated from number of channels in model + alpha value
 */
template< colour_configuration >
struct colour_configuration_traits;

template<>
struct colour_configuration_traits< RGB8 >
{
	static constexpr colour_model model    = RGB;
	static constexpr bool alpha            = false;
	static constexpr unsigned bitdepth     = 8;
	static constexpr unsigned num_channels = 3;
	using raw = std::uint8_t;
};

template<>
struct colour_configuration_traits< RGB16 >
{
	static constexpr colour_model model    = RGB;
	static constexpr bool alpha            = false;
	static constexpr unsigned bitdepth     = 16;
	static constexpr unsigned num_channels = 3;
	using raw = std::uint16_t;
};

template<>
struct colour_configuration_traits< RGBA8 >
{
	static constexpr colour_model model    = RGB;
	static constexpr bool alpha            = true;
	static constexpr unsigned bitdepth     = 8;
	static constexpr unsigned num_channels = 4;
	using raw = std::uint8_t;
};

template<>
struct colour_configuration_traits< RGBA16 >
{
	static constexpr colour_model model    = RGB;
	static constexpr bool alpha            = true;
	static constexpr unsigned bitdepth     = 16;
	static constexpr unsigned num_channels = 4;
	using raw = std::uint16_t;
};

#endif // COLOUR_H
