#include "layer.h"

#include <algorithm>
#include <stdexcept>

Layer::Layer()
	: Layer(0, 0, nullptr)
{
}

Layer::Layer(
	std::intmax_t    w,
	std::intmax_t    h,
	pixel< RGBA16 >* p
)
	: x_(0), y_(0), mode_(Normal), alpha_(IMAGE_ALPHA_OPAQUE)
{
	if ( w < 0 || h < 0 )
	{
		throw std::invalid_argument("Dimensions cannot be negative");
	}

	if ( p != nullptr )
	{
		data.assign(w, h, p + 0, p + w * h);
	}
	else
	{
		data = pixels< RGBA16 >(w, h, pixel< RGBA16 >(0, 0, 0) );
	}
}

Layer* Layer::clone() const
{
	return new Layer(*this);
}

void Layer::swap(Layer& l)
{
	data.swap(l.data);
	std::swap(x_, l.x_);
	std::swap(y_, l.y_);
	std::swap(mode_, l.mode_);
	std::swap(alpha_, l.alpha_);
}

std::intmax_t Layer::x() const
{
	return x_;
}

std::intmax_t Layer::y() const
{
	return y_;
}

std::intmax_t Layer::width() const
{
	return data.width();
}

std::intmax_t Layer::height() const
{
	return data.height();
}

Layer::Mode Layer::mode() const
{
	return mode_;
}

void Layer::mode(Mode m)
{
	mode_ = m;
}

unsigned short Layer::alpha() const
{
	return alpha_;
}

void Layer::alpha(unsigned short a)
{
	alpha_ = a;
}

void Layer::move(
	std::intmax_t x0,
	std::intmax_t y0
)
{
	x_ = x0, y_ = y0;
}

void Layer::invert()
{
	for (auto p : data)
	{
		p.invert();
	}
}

void Layer::setPixel(
	std::intmax_t          x0,
	std::intmax_t          y0,
	const pixel< RGBA16 >& c
)
{
	if ( x_ <= x0 && y_ <= y0 )
	{
		const auto        x = static_cast< std::uintmax_t >( x0 - x_ );
		const auto        y = static_cast< std::uintmax_t >( y0 - y_ );
		const std::size_t w = data.width();

		if ( x < w && y < data.height() )
		{
			auto p = data[y * w + x];

			/// @todo Poor initialization
			p[0] = c[0];
			p[1] = c[1];
			p[2] = c[2];
			p[3] = c[3];
		}
	}
}

pixel< RGBA16 > Layer::getPixel(
	std::intmax_t x0,
	std::intmax_t y0
) const
{
	if ( x_ <= x0 && y_ <= y0 )
	{
		const auto        x = static_cast< std::uintmax_t >( x0 - x_ );
		const auto        y = static_cast< std::uintmax_t >( y0 - y_ );
		const std::size_t w = data.width();

		if ( x < w && y < data.height() )
		{
			return pixel< RGBA16 >{ data[y * w + x] };
		}
	}

	return pixel< RGBA16 >(0, 0, 0, 65535);
}

void Layer::fill(const pixel< RGBA16 >& c)
{
	const std::size_t w = data.width();
	const std::size_t h = data.height();

	for (std::size_t i = 0; i < h; i++)
	{
		for (std::size_t j = 0; j < w; j++)
		{
			auto p = data[i * w + j];

			/// @todo poor initialization
			p[0] = c[0];
			p[1] = c[1];
			p[2] = c[2];
			p[3] = c[3];
		}
	}
}

// TODO: How to include alpha??
void Layer::filter(
	const double*  f,
	std::uintmax_t fx,
	std::uintmax_t fy
)
{
	if ( f == nullptr || fx <= 0 || fy <= 0 )
	{
		return;
	}

	const auto w = data.width();
	const auto h = data.height();

	pixels< RGBA16 > temp(w, h);

	for (std::uintmax_t i = 0; i < h; i++)
	{
		for (std::uintmax_t j = 0; j < w; j++)
		{
			double sumr = 0.0;

			double sumg = 0.0;

			double               sumb = 0.0;
			double               norm = 0.0;
			const std::uintmax_t il   = i < ( fy / 2 ) ? 0 : i - ( fy / 2 );
			const std::uintmax_t ih   = std::min< std::uintmax_t >( ( i + fy - ( fy / 2 ) ), h);
			const std::uintmax_t jl   = j < ( fx / 2 ) ? 0 : j - ( fx / 2 );
			const std::uintmax_t jh   = std::min< std::uintmax_t >( ( j + fx - ( fx / 2 ) ), w);

			for (std::uintmax_t ii = il; ii < ih; ii++)
			{
				for (std::uintmax_t jj = jl; jj < jh; jj++)
				{
					sumr += f[( ( ii + ( fy / 2 ) ) - i ) * fx + ( ( jj + ( fx / 2 ) ) - j )]
					        * static_cast< double >( data[ii * w + jj][0] );
					sumg += f[( ( ii + ( fy / 2 ) ) - i ) * fx + ( ( jj + ( fx / 2 ) ) - j )]
					        * static_cast< double >( data[ii * w + jj][1] );
					sumb += f[( ( ii + ( fy / 2 ) ) - i ) * fx + ( ( jj + ( fx / 2 ) ) - j )]
					        * static_cast< double >( data[ii * w + jj][2] );
					norm += f[( ( ii + ( fy / 2 ) ) - i ) * fx + ( ( jj + ( fx / 2 ) ) - j )];
				}
			}

			sumr /= norm;
			sumg /= norm;
			sumb /= norm;

			/// @todo Not the best way to initialize
			temp[i * w + j][0] = sumr;
			temp[i * w + j][1] = sumg;
			temp[i * w + j][2] = sumb;
			temp[i * w + j][3] = 0;
		}
	}

	data = std::move(temp);
}

void Layer::threshold(
	double lo,
	double hi
)
{
	const auto w = data.width();
	const auto h = data.height();

	for (std::size_t i = 0; i < h; i++)
	{
		for (std::size_t j = 0; j < w; j++)
		{
			// TODO: Separate function to get value from RGB structure
			auto         p = data[i * w + j];
			const double t = ( p[0] + p[1] + p[2] ) / ( 3.0 * 65535.0 );

			if ( ( ( t - lo ) * ( t - hi ) ) < 0.0 )
			{
				/// @todo poor initialization
				p[0] = 65535;
				p[1] = 65535;
				p[2] = 65535;
				p[3] = 0;
			}
			else
			{
				/// @todo poor initialization
				p[0] = 0;
				p[1] = 0;
				p[2] = 0;
				p[3] = 0;
			}
		}
	}
}

void Layer::transform(pixel< RGBA16 >( *f ) (const pixel< RGBA16 >&) )
{
	const auto w = data.width();
	const auto h = data.height();

	for (std::size_t i = 0; i < h; ++i)
	{
		for (std::size_t j = 0; j < w; ++j)
		{
			auto x = f(pixel< RGBA16 >(data[i * w + j]) );
			auto p = data[i * w + j];

			/// @todo Poor initialization
			p[0] = x[0];
			p[1] = x[1];
			p[2] = x[2];
			p[3] = x[3];
		}
	}
}

unsigned long Layer::average(
	std::intmax_t xx,
	std::intmax_t yy,
	std::intmax_t d
) const
{
	if ( d <= 0 )
	{
		return 0;
	}

	const auto w = data.width();
	const auto h = data.height();

	unsigned long r = 0;
	unsigned long g = 0;
	unsigned long b = 0;

	const std::intmax_t j0 = ( xx < d ? 0 : xx - d );
	const std::intmax_t jmax
	    = ( ( xx < -d || static_cast< std::uintmax_t >( xx + d ) < w ) ? xx + d
	                                  : static_cast< std::intmax_t >( w ) - 1 );

	const std::intmax_t i0 = ( yy < d ? 0 : yy - d );
	const std::intmax_t imax
	    = ( ( yy < -d || static_cast< std::uintmax_t >( yy + d ) < h ) ? yy + d
	                                  : static_cast< std::intmax_t >( h ) - 1 );

	for (std::intmax_t i = i0; i <= imax; ++i)
	{
		for (std::intmax_t j = j0; j <= jmax; ++j)
		{
			r += data[i * w + j][0];
			g += data[i * w + j][1];
			b += data[i * w + j][2];
		}
	}

	const auto c = static_cast< unsigned long >( ( imax - i0 + 1 ) * ( jmax - j0 + 1 ) );

	r /= c, g /= c, b /= c;

	return ( ( r << 8 ) & UINT32_C(0xFF0000) ) + ( g & UINT32_C(0xFF00) ) + ( ( b >> 8 ) & UINT32_C(0xFF) );
}

std::shared_ptr< unsigned char[] > Layer::convert(colour_configuration f) const
{
	if ( f != RGB8 )
	{
		throw std::logic_error("Not implemented");
	}

	const auto w = data.width();
	const auto h = data.height();

	auto* p = new unsigned char [w * h * 3];

	for (std::size_t i = 0; i < h; ++i)
	{
		for (std::size_t j = 0; j < w; ++j)
		{
			p[( w * i + j ) * 3 + 0] = static_cast< unsigned char >( data[w * i + j][0] >> 8 );
			p[( w * i + j ) * 3 + 1] = static_cast< unsigned char >( data[w * i + j][1] >> 8 );
			p[( w * i + j ) * 3 + 2] = static_cast< unsigned char >( data[w * i + j][2] >> 8 );
		}
	}

	return std::shared_ptr< unsigned char[] >{ p };
}
