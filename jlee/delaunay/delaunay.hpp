/*=========================================================================*//**
   \file        delaunay.hpp
   \brief       Exposes delaunay() function for a Delaunay Triangulation

   This header file provides functions for triangulating a given plane graph. To
   get the maximally planar graph of g, simply call

   Graph<T> h = delaunay(g);

   where g is of type Graph<T>.

   \note The type T must derive from PlaneNode in graph.hpp

   \todo Stability: delordis() could improve on testing (alpha + gamma > pi)
   \todo Stability: Delaunay<T>::operator() needs robust cross product
*//*==========================================================================*/

#ifndef DELAUNAY_HPP
#define DELAUNAY_HPP

#include <algorithm>
#include <cfloat>
#include <cmath>
#include <vector>

#ifndef NDEBUG
#include <iostream>
#endif

#include "../graph/undirected.h" // For undirected_graph<>

/*=========================================================================*//**
   \class       Delaunay
   \brief       Wraps the various triangulation functions together

   This class provides the static member function delordis(), and also acts as a
   predicate for delaunay().

   T should have setters and getters for x and y (i.e., double T::x(),
   void T::x(double))

*//*==========================================================================*/
template< class T >
class Delaunay
{
public:
	/*=========================================================================*//**
	   \class       face3
	   \brief       Little more than an ordered triple to hold vertices from a graph.

	   \invariant Points are assumed to be in clockwise order. Several optimizations in
	   this source rely on this fact. This class does _not_ check this on
	   construction.

	*//*==========================================================================*/
	class face3
	{
		typedef typename undirected_graph< T >::label_type label_type;
		typedef typename undirected_graph< T >::edge_type edge_type;
public:
		// constructor
		face3(
			label_type x,
			label_type y,
			label_type z
		)
			: a(x), b(y), c(z)
		{
		}

		// copy constructor
		face3(const face3& t)
			: a(t.a), b(t.b), c(t.c)
		{
		}

		// copy assignment
		face3& operator=(const face3& t)
		{
			a = t.a;
			b = t.b;
			c = t.c;
			return *this;
		}

		// accessor
		label_type operator[](unsigned i) const
		{
			return i ? ( i == 1 ? b : c ) : a;
		}

		// Tests if the face determines the same edge as xy
		bool hasEdge(const edge_type& e) const
		{
			return ( a == e.first || b == e.first || c == e.first )
			       && ( a == e.second || b == e.second || c == e.second );
		}
private:
		label_type a, b, c;
	};

	/** ************************************************************************/ /**
	   \fn          Delaunay
	   \brief       Construct the predicate functor

	   When operator() is called, the point (x, y) will be checked to see if it is in
	   the given face. The graph g will be used as reference.
	 *******************************************************************************/
	Delaunay(
		const undirected_graph< T >& h,
		float                        xx,
		float                        yy
	)
		: g(h), x(xx), y(yy)
	{
	}

	/** ************************************************************************/ /**
	   \fn          bool Delaunay<T>::operator()(const face3&)
	   \brief       Checks if (x,y) is in the given triangle
	   \param       t Lists 3 vertices of g which determine a triangle in the plane
	   \returns     true if (x,y) is in the face, false otherwise

	   A point is determined to be in the triangle if for each edge, it is on the same
	   side of the edge as the remaining point of the triangle. For example, suppose
	   ABC is a triangle. The edge AB determines two half planes, one of which contains
	   C. If the point (x,y) is in the same half plane as C, then they are considered
	   "on the same side".

	   Algebraically, if L is the line containing A and B, then L(A) = 0 and L(B) = 0.
	   Since face3 is assumed to be in clockwise order, it can be shown that L(C) > 0
	   always. Thus we only need to check that L(x,y) >= 0. The check is repeated
	   for the edge BC and CA.

	   The calculations below check these conditions.

	   \note If a point is on one of the edges of the face, it is considered inside.
	   Thus it will be in two faces (including the face sharing the edge), instead
	   of none. The function delaunay() relies on the assumption that _at least one
	   face_ contains the point (x, y).

	   \todo This is cross product, or determinant calculation. Are there numerically
	   stable methods that apply?
	 *******************************************************************************/
	bool operator()(const face3& t)
	{
		double ax = g[t[0]].x(), ay = g[t[0]].y();
		double bx = g[t[1]].x(), by = g[t[1]].y();
		double cx = g[t[2]].x(), cy = g[t[2]].y();

		// Is d on the sames side of (ab) as c?
		if ( y * ( bx - ax ) + x * ( ay - by ) < ( ay * bx - ax * by ) )
		{
			return false;
		}

		// Is d on the same side of (bc) as a?
		if ( y * ( cx - bx ) + x * ( by - cy ) < ( by * cx - bx * cy ) )
		{
			return false;
		}

		// Is d on the same side of (ca) as b?
		if ( y * ( ax - cx ) + x * ( cy - ay ) < ( cy * ax - ay * cx ) )
		{
			return false;
		}

		return true;
	}

	/** ************************************************************************/ /**
	   \fn          bool Delaunay<T>::delordis(undirected_graph<T>& g, face3& t, face3& u)
	   \brief       Ensures that triangles t and u form a Delaunay pair, if connected
	   \return      True if a "flip" occurs, false otherwise.

	   Suppose two triangles share an edge. Let the triangles be ABD and BCD. The
	   triangles meet the Delaunay condition if angle A plus angle C is less than or
	   equal to 180 degrees. If the triangles do _not_ meet the condition, then the
	   triangles ACD, and ABC meet the condition instead.

	   This function checks if t and u meet the Delaunay condition and rewrites them
	   if they do not. Note that if t and u do not share an edge, nothing is done.

	   \note It is important that this function keep the triangles in a clockwise
	      order. Other functions depend on this invariant.
	 *******************************************************************************/
	static bool delordis(
		undirected_graph< T >& g,
		face3&                 t,
		face3&                 u
	)
	{
		typedef typename undirected_graph< T >::label_type label_type;

		label_type v[4]   = {}; // Holds the four unique points in (t union u)
		int        common = 0;  // number of points in common

		// First determine if t and u have exactly two common points
		for (unsigned i = 0; i < 3; ++i)
		{
			for (unsigned j = 0; j < 3; ++j)
			{
				if ( t[i] != u[j] )
				{
					continue; // no match
				}

				if ( ++common == 1 )
				{
					v[0] = t[( i + 2 ) % 3];
					v[1] = t[i];
				}
				else
				{
					v[2] = u[( j + 2 ) % 3];
					v[3] = u[j];
				}
			}
		}

		// Check if t and u are disconnected, or identical
		if ( common != 2 || v[0] == v[2] )
		{
			return false;
		}

		// Calculate the angles not involving shared points using arctan fomula
		float ax, ay, bx, by, alpha, gamma, pa, pb;

		ax  = g[v[1]].x() - g[v[2]].x();
		ay  = g[v[1]].y() - g[v[2]].y();
		bx  = g[v[3]].x() - g[v[2]].x();
		by  = g[v[3]].y() - g[v[2]].y();
		pa  = hypot(ax, ay), pb = hypot(bx, by);
		ax /= pa, ay /= pa, bx /= pb, by /= pb;

		// alpha is half the angle between the vectors
		alpha = atan2(hypot(ax - bx, ay - by), hypot(ax + bx, ay + by) );

		ax  = g[v[1]].x() - g[v[0]].x();
		ay  = g[v[1]].y() - g[v[0]].y();
		bx  = g[v[3]].x() - g[v[0]].x();
		by  = g[v[3]].y() - g[v[0]].y();
		pa  = hypot(ax, ay), pb = hypot(bx, by);
		ax /= pa, ay /= pa, bx /= pb, by /= pb;

		// gamma is half the angle between the vectors
		gamma = atan2(hypot(ax - bx, ay - by), hypot(ax + bx, ay + by) );

		// Check the Delaunay condition and fix up the triangles if necessary
		if ( cos(alpha + gamma) < 0 ) // i.e., if half the angle is > pi/2
		{
			t = face3(v[0], v[1], v[2]);
			u = face3(v[2], v[3], v[0]);
			return true;
		}

		return false;
	}
private:
	const undirected_graph< T >& g; // undirected_graph object
	float                        x; // (x, y) is the point to test
	float                        y;
};

/** ************************************************************************/ /**
   \fn          undirected_graph<T> delaunay(const undirected_graph<T>&)
   \brief       Constructs a maximally planar graph with the same vertices as h

   This function uses the Delaunay algorithm to triangulate a given graph. That is,
   the resulting graph has the same vertices as the input, but the edges are chosen
   so that the plane is divided into triangles.

   The algorithm proceeds by first creating a triangle that contains all the
   vertices of the graph h. This triangle graph, g, is used as the basis for the
   result. One by one, vertices from h are added to g and the triangulation
   condition is checked: if the added point is within a triangle formed by three
   other vertices, then that is broken into three smaller triangles.

   Furthermore, the Delaunay condition is checked to ensure that the result has
   somewhat uniform edge lengths.

   Finally, the three vertices we constructed at the beginning are removed, leaving
   only the vertices that were in h.

   \note This function assumes undirected_graph is undirected.

   \todo Colinear points cause a problem. Suppose a point is on an edge. The face
      containing that edge is split into 3 triangles instead of 2. One of these
      triangles is effectively empty.
   \todo Technically not *maximally* planar because we leave the boundary alone.
      Shows up a little jagged when plotted.
   \todo Number of edges and triangles is predictable, so space could be reserved
      in the containers. A maximal planar graph (also, triangular) has exactly
      3v-6 edges and 2v-4 faces.
   \todo Need a more efficient way of finding tris containing edges. With some
      book-keeping, we can drill down the triangles to significantly reduce the
      search set. This will require _keeping_ triangles that have been replaced,
      and having them point to their replacements.

 *******************************************************************************/
template< class T >
undirected_graph< T > delaunay(const undirected_graph< T >& h)
{
	typedef typename undirected_graph< T >::edge_type edge_type;
	typedef typename undirected_graph< T >::const_vertex_iterator cviterator;
	typedef typename undirected_graph< T >::label_type label_type;
	typedef typename Delaunay< T >::face3 triangle;

	undirected_graph< T > g;

	if ( h.order() == 0 )
	{
		return g;
	}

	// Find the bounding rectangle of the graph
	float minx = FLT_MAX;
	float miny = FLT_MAX;
	float maxx = -FLT_MAX;
	float maxy = -FLT_MAX;

	/// \todo for_each_vertex()
	for (cviterator it = h.vbegin(); it != h.vend(); ++it)
	{
		float x = it->second.x(), y = it->second.y();

		if ( x < minx )
		{
			minx = x;
		}

		if ( x > maxx )
		{
			maxx = x;
		}

		if ( y < miny )
		{
			miny = y;
		}

		if ( y > maxy )
		{
			maxy = y;
		}
	}

	minx -= 10.0f, miny -= 10.0f, maxx += 10.0f, maxy += 10.0f;

	// Create a bounding face that encloses all vertices
	T          dummy;
	label_type va1, va2, va3;

	dummy.x(1.5f * minx - 0.5f * maxx - 1.0f);
	dummy.y(miny - 1.0f);
	va1 = g.insert(dummy);

	dummy.x(1.5f * maxx - 0.5f * minx + 1.0f);
	dummy.y(miny - 1.0f);
	va2 = g.insert(dummy);

	dummy.x(.5f * maxx + 0.5f * minx);
	dummy.y(maxy + ( maxy - miny ) + 1.0f);
	va3 = g.insert(dummy);

	// Construct our list of 3-faces
	std::vector< triangle > faces;

	faces.push_back(triangle(va1, va2, va3) );
	// Edges affected by adding a vertex
	std::vector< edge_type > affedg;

	// Add points to triangulation one by one
	for (cviterator it = h.vbegin(); it != h.vend(); ++it)
	{
		Delaunay< T > p(g, it->second.x(), it->second.y() );

		// Find the triangle that the vertex is "in"
		typename std::vector< triangle >::iterator jt = std::find_if(faces.begin(), faces.end(), p);

		if ( jt == faces.end() ) // should never happen (by design)
		{
			throw std::runtime_error("Triangulation has failed.");
		}

		// Insert the node, split the faces
		label_type v = g.insert(it->second);
		triangle   t(*jt);
		*jt = triangle(t[0], t[1], v);
		faces.push_back(triangle(t[1], t[2], v) );
		faces.push_back(triangle(t[2], t[0], v) );

		affedg.push_back(edge_type(t[0], t[1]) );
		affedg.push_back(edge_type(t[1], t[2]) );
		affedg.push_back(edge_type(t[2], t[0]) );

		// Check the Delaunay condition
		while ( !affedg.empty() )
		{
			edge_type& e = affedg.back();

			// Find two triangles that share the affected edge
			unsigned      tris = 0;
			unsigned long j = 0, k = 0;

			for (unsigned long i = 0; tris < 2 && i < faces.size(); ++i)
			{
				if ( faces[i].hasEdge(e) )
				{
					++tris;
					j = k;
					k = i;
				}
			}

			// Flip the triangles if the Delaunay condition doesn't
			// hold anymore.
			if ( tris == 2 && Delaunay< T >::delordis(g, faces[j], faces[k]) )
			{
				// Flip occurred. Four edges affected
				e = edge_type(faces[j][0], faces[j][1]);
				affedg.push_back(edge_type(faces[j][1], faces[j][2]) );
				affedg.push_back(edge_type(faces[k][0], faces[k][1]) );
				affedg.push_back(edge_type(faces[k][1], faces[k][2]) );
			}
			else
			{
				affedg.pop_back();
			}
		}
	}

	// Build edge list of triangulation
	for (unsigned long i = 0, n = faces.size(); i < n; ++i)
	{
		g.connect(faces[i][0], faces[i][1]);
		g.connect(faces[i][1], faces[i][2]);
		g.connect(faces[i][2], faces[i][0]);
	}

	// Clean up "fake" vertices
	g.erase(va1);
	g.erase(va2);
	g.erase(va3);
	return g;
}

#endif // ifndef DELAUNAY_HPP
