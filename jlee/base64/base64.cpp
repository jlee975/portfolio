#include "base64.h"

#include <cstdint>

namespace
{
constexpr unsigned decodechar(char c)
{
	if ( c <= 57 )
	{
		if ( c >= 48 )
		{
			// '0'..'9'
			return 52 + ( c - 48 );
		}

		if ( c == 43 )
		{
			// '+'
			return 62;
		}

		if ( c == 47 )
		{
			// '/'
			return 63;
		}
	}
	else
	{
		if ( 65 <= c && c <= 90 )
		{
			// 'A'..'Z'
			return c - 65;
		}

		if ( 97 <= c && c <= 122 )
		{
			// 'a'..'z'
			return c - 71;
		}
	}

	return 64;
}

}

/// @todo Some way to indicate failure, ex., if nOut is not large enough.
std::size_t Base64Decode(
	const char* s,
	std::size_t ns,
	std::byte*  pOut,
	std::size_t nOut
)
{
	if ( ( s == nullptr ) || ( pOut == nullptr ) )
	{
		return 0;
	}

	static const unsigned b = 64;

	std::size_t   j = 0;
	unsigned long x = 0;

	unsigned long r = 1;

	for (std::size_t i = 0; i < ns && j < nOut && s[i] != '='; ++i)
	{
		const unsigned k = decodechar(s[i]);

		if ( k < b )
		{
			x = ( x * b ) + k; // accumulate values in x

			if ( ( r *= b ) >= 256 ) // do we have enough for a byte?
			{
				r        /= 256;
				pOut[j++] = std::byte(x / r);
				x        %= r;
			}
		}
	}

	return j;
}

/// @todo Some way to indicate failure, ex., if cbOut is not large enough.
std::size_t Base64Encode(
	const std::byte* pIn,  // binary input
	std::size_t      cbIn, // size of binary input in bytes
	char*            pOut, // output string
	std::size_t      cbOut // bytes available in output string
)
{
	/// @todo The calculation of needed bytes could overflow
	if ( ( pIn == nullptr ) || ( pOut == nullptr ) || cbOut < ( 4 * ( ( cbIn + 2 ) / 3 ) ) )
	{
		return 0;
	}

	/// @bug Should actually use ascii codes
	static const char alphabet[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	                               "abcdefghijklmnopqrstuvwxyz0123456789+/";

	std::size_t i = 0;

	std::size_t j = 0;

	// Each triple of bytes can be expanded to 4 bytes
	while ( i + 2 < cbIn )
	{
		const std::uint_least32_t x = ( ( std::to_integer< std::uint_least32_t >(pIn[i]) ) << 16 )
		                              + ( ( std::to_integer< std::uint_least32_t >(pIn[i + 1]) ) << 8 )
		                              + ( std::to_integer< std::uint_least32_t >(pIn[i + 2]) );

		pOut[j]     = alphabet[( x >> 18 ) & 0x3F];
		pOut[j + 1] = alphabet[( x >> 12 ) & 0x3F];
		pOut[j + 2] = alphabet[( x >> 6 ) & 0x3F];
		pOut[j + 3] = alphabet[x & 0x3F];

		i += 3;
		j += 4;
	}

	// A partial block at the end of the input has to be padded
	switch ( cbIn - i )
	{
	case 2: // There are two characters left
	{
		const std::uint_least32_t x = ( ( std::to_integer< std::uint_least32_t >(pIn[i]) ) << 16 )
		                              + ( ( std::to_integer< std::uint_least32_t >(pIn[i + 1]) ) << 8 );
		pOut[j]     = alphabet[( x >> 18 ) & 0x3F];
		pOut[j + 1] = alphabet[( x >> 12 ) & 0x3F];
		pOut[j + 2] = alphabet[( x >> 6 ) & 0x3F];
		pOut[j + 3] = '=';
		j          += 4;
	}
	break;
	case 1:
	{
		const std::uint_least32_t x = ( ( std::to_integer< std::uint_least32_t >(pIn[i]) ) << 16 );
		pOut[j]     = alphabet[( x >> 18 ) & 0x3F];
		pOut[j + 1] = alphabet[( x >> 12 ) & 0x3F];
		pOut[j + 2] = '=';
		pOut[j + 3] = '=';
		j          += 4;
	}
	break;
	} // switch

	// */
	return j;
}
