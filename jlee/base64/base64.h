#ifndef BASE64_H
#define BASE64_H

#include <cstddef>

std::size_t Base64Decode(const char*, std::size_t, std::byte*, std::size_t);
std::size_t Base64Encode(const std::byte*, std::size_t, char*, std::size_t);

#endif // BASE64_H
