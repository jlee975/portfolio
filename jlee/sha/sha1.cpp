/*=========================================================================*//**
   \file        sha1.cpp

   \note I have tried to be careful to use "message" to mean the complete set of
      data over which a hash is to be calculated. The terms "block", "data", and
      "block(s) of data" refer to splitting a message into several pieces for
      processing small amounts at a time.
*//*==========================================================================*/
#include "sha1.h"

#ifndef NDEBUG
#include <iostream>
#endif

#include <algorithm>
#include <climits>

#include "bits/rotate.h"
#include "endian/endian.h"

#if ( CHAR_BIT != 8 )
#error "Only 8 bit chars supported."
#endif

/*=========================================================================*//**
   \class       SHA1
   \brief       Calculates the SHA-1 hash

   \todo option to supply bit length or byte length

   \note The round loop has been unrolled so that the rotation of the (abcde)
      values can be eliminated. Five rotations brings everything back to the
      initial state, so each loop has been unrolled five times.

   References

   "FIPS PUB 180-3: Secure Hash Standard (SHS)", National Institute of Standards
   and Technology,
   http://csrc.nist.gov/publications/fips/fips180-3/fips180-3_final.pdf

*//*==========================================================================*/
SHA1::SHA1() = default;

/***************************************************************************/ /**
   \fn          SHA1& SHA1::operator()(const unsigned char*, std::size_t, bool)
   \brief       Calculates SHA1 digest of a message
   \param       data Pointer to data for hash calculation
   \param       n Size of data
   \param       final True if this is the last block of the message, false if not.
   \returns     Reference to this (so calls can be chained)

   This provides the primary interface for calculating hashes of messages. The
   value of the hash is returned by SHA1::value(). Optionally, the hash can be
   calculated a block of data at a time. The basic usage pattern is:

    SHA1 s;
    s(data, n);
    Digest m = s.value();

   for a single block of contiguous data, i.e., a complete message, or

    SHA1 s;                          // construct a SHA1 object
    for(i = 1..m-1)                  // First m-1 blocks
        s(data_i, len_i, false);
    s(data_m, len_m, true);          // Final block
    Digest m = s.value();            // Get the value of the hash

   for a message broken into m blocks.

   The parameter "final" should be set true to indicate that the last block of
   the message has been sent to the SHA1 object. This is the default value.
   If the data passed to operator() is only a portion of the message, the "final"
   parameter should be set to false.

   The static function hash() is provided for one time use on a complete message.
   See the description of that function for details.

   \note If the size of the message is unknown, the user should then call
      operator() repeatedly with final == false until no more data is available.
      An 'empty' call with no data and final == true will finish the
      calculation.
 *******************************************************************************/
SHA1& SHA1::operator()(
	const unsigned char* data,
	std::size_t          n,
	bool final
)
{
	std::size_t k = 0;

	if ( restart )
	{
		reset();
		restart = false;
	}
	else if ( std::size_t i = tlen_lo % 64 )
	{
		// There's buffered data from last call
		for (; k < n && k + i < 64; ++k)
		{
			buff[i + k] = data[k];
		}

		if ( i + k == 64 ) // We got enough for a block
		{
			process_block(buff);
		}
	}

	// Process full blocks
	for (; n - k >= 64; k += 64)
	{
		process_block(data + k);
	}

	// Buffer remaining data
	for (std::size_t i = k; i < n; ++i)
	{
		buff[i - k] = data[i];
	}

	if ( n != 0 )
	{
		update_len(n);
	}

	if ( final )
	{
		last_block();
	}

	return *this;
}

/***************************************************************************/ /**
   \fn          Digest SHA1::hash(const unsigned char*, std::size_t)
   \brief       Static function for convenience
   \param       message Pointer to message for hash calculation
   \param       n Size of message

   This function calculates the hash for a complete message. It is equivalent to

    SHA1 s;
    s(message, n, true);
    return s.value();
 *******************************************************************************/
Digest SHA1::hash(
	const unsigned char* message,
	std::size_t          n
)
{
	return SHA1() (message, n, true).value();
}

/***************************************************************************/ /**
   \fn          void SHA1::process_block(const unsigned char*)
   \brief       Applies the round functions to a block of data
 *******************************************************************************/
void SHA1::process_block(const unsigned char* m)
{
	const unsigned long K00 = UINT32_C(0x5A827999);
	const unsigned long K20 = UINT32_C(0x6ED9EBA1);
	const unsigned long K40 = UINT32_C(0x8F1BBCDC);
	const unsigned long K60 = UINT32_C(0xCA62C1D6);

	unsigned long w[80];

	for (std::size_t i = 0; i < 16; i++)
	{
		w[i] = beread< 32 >(m + 4 * i);
	}

	for (std::size_t i = 16; i < 80; i++)
	{
		unsigned long t = w[i - 3] ^ w[i - 8] ^ w[i - 14] ^ w[i - 16];
		w[i] = ( ( t >> 31 ) + ( t << 1 ) ) & 0xFFFFFFFFUL;
	}

	unsigned long a = h[0];

	unsigned long b = h[1];

	unsigned long c = h[2];

	unsigned long d = h[3];

	unsigned long e = h[4];

	for (unsigned i = 0; i < 20; i += 5)
	{
		e += ( d ^ ( ( c ^ d ) & b ) ) + rol32(a, 5) + w[i + 0] + K00;
		b  = rol32(b, 30);
		d += ( c ^ ( ( b ^ c ) & a ) ) + rol32(e, 5) + w[i + 1] + K00;
		a  = rol32(a, 30);
		c += ( b ^ ( ( a ^ b ) & e ) ) + rol32(d, 5) + w[i + 2] + K00;
		e  = rol32(e, 30);
		b += ( a ^ ( ( e ^ a ) & d ) ) + rol32(c, 5) + w[i + 3] + K00;
		d  = rol32(d, 30);
		a += ( e ^ ( ( d ^ e ) & c ) ) + rol32(b, 5) + w[i + 4] + K00;
		c  = rol32(c, 30);
	}

	for (unsigned i = 20; i < 40; i += 5)
	{
		e += ( b ^ c ^ d ) + rol32(a, 5) + w[i + 0] + K20;
		b  = rol32(b, 30);
		d += ( a ^ b ^ c ) + rol32(e, 5) + w[i + 1] + K20;
		a  = rol32(a, 30);
		c += ( e ^ a ^ b ) + rol32(d, 5) + w[i + 2] + K20;
		e  = rol32(e, 30);
		b += ( d ^ e ^ a ) + rol32(c, 5) + w[i + 3] + K20;
		d  = rol32(d, 30);
		a += ( c ^ d ^ e ) + rol32(b, 5) + w[i + 4] + K20;
		c  = rol32(c, 30);
	}

	for (unsigned i = 40; i < 60; i += 5)
	{
		e += ( ( b & c ) | ( ( b | c ) & d ) ) + rol32(a, 5) + w[i + 0] + K40;
		b  = rol32(b, 30);
		d += ( ( a & b ) | ( ( a | b ) & c ) ) + rol32(e, 5) + w[i + 1] + K40;
		a  = rol32(a, 30);
		c += ( ( e & a ) | ( ( e | a ) & b ) ) + rol32(d, 5) + w[i + 2] + K40;
		e  = rol32(e, 30);
		b += ( ( d & e ) | ( ( d | e ) & a ) ) + rol32(c, 5) + w[i + 3] + K40;
		d  = rol32(d, 30);
		a += ( ( c & d ) | ( ( c | d ) & e ) ) + rol32(b, 5) + w[i + 4] + K40;
		c  = rol32(c, 30);
	}

	for (unsigned i = 60; i < 80; i += 5)
	{
		e += ( b ^ c ^ d ) + rol32(a, 5) + w[i + 0] + K60;
		b  = rol32(b, 30);
		d += ( a ^ b ^ c ) + rol32(e, 5) + w[i + 1] + K60;
		a  = rol32(a, 30);
		c += ( e ^ a ^ b ) + rol32(d, 5) + w[i + 2] + K60;
		e  = rol32(e, 30);
		b += ( d ^ e ^ a ) + rol32(c, 5) + w[i + 3] + K60;
		d  = rol32(d, 30);
		a += ( c ^ d ^ e ) + rol32(b, 5) + w[i + 4] + K60;
		c  = rol32(c, 30);
	}

	h[0] += a, h[1] += b, h[2] += c, h[3] += d, h[4] += e;
}

/***************************************************************************/ /**
   \fn          void SHA1::last_block()
   \brief       Called when all data has been processed, completing the hash

   After all data has been received, and before the hash is returned, a final
   stage of processing must be done. This includes padding the data to be
   congruent to 448 bits in length, and appending the 64-bit length of the
   message. This final stage is done within this function.

   \todo Maybe expose this since operator()(0,0,true) is equivalent
 *******************************************************************************/
void SHA1::last_block()
{
	buff[tlen_lo % 64] = 0x80;

	for (std::size_t i = ( tlen_lo % 64 ) + 1; i < 64; ++i)
	{
		buff[i] = 0;
	}

	// Extra block if storing the length would cross a block boundary
	if ( ( tlen_lo % 64 ) >= 56 )
	{
		process_block(buff);
		std::fill_n(buff, 56, 0);
	}

	// Store length in final block and process
	bewrite< 32 >(reinterpret_cast< std::byte* >( buff + 56 ), ( tlen_hi << 3 ) + ( ( tlen_lo >> 29 ) & 0xFFFFFFFFUL ) );
	bewrite< 32 >(reinterpret_cast< std::byte* >( buff + 60 ), tlen_lo << 3);
	process_block(buff);
	restart = true;
}

/***************************************************************************/ /**
   \fn          void SHA1::reset()
   \brief       Resets the intermediate hash info, and total length

   The SHA1 class keeps the intermediate calculated hash values from one call
   of operator() to the next. If a new set of data is to be processed, it is
   necessary to reset the hash values to their initial conditions. The total
   length of data that has been processed is also set to zero.
 *******************************************************************************/
void SHA1::reset()
{
	h[0] = UINT32_C(0x67452301), h[1] = UINT32_C(0xEFCDAB89), h[2] = UINT32_C(0x98BADCFE),
	h[3] = UINT32_C(0x10325476), h[4] = UINT32_C(0xC3D2E1F0);

	tlen_lo = 0;
	tlen_hi = 0;
}

/***************************************************************************/ /**
   \fn          void SHA1::update_len(std::size_t)
   \brief       Adjusts the total length of processed data
   \param       n Size of current block

   Internally, the SHA1 class keeps track of the total amount of data it has
   processed across calls to operator(). If the hash is calculated over several
   blocks, the (64-bit) size must be accumulated. This is done by calling this
   function with the size of the block being processed.
 *******************************************************************************/
void SHA1::update_len(std::size_t n)
{
	unsigned long x;

	x = ( tlen_lo + static_cast< unsigned long >( n ) ) & 0xFFFFFFFFUL;

	if ( x < tlen_lo ) // handle carry
	{
		++tlen_hi;
	}

	tlen_lo  = x;
	tlen_hi += static_cast< unsigned long >( ( n >> 16 ) >> 16 );
}

/***************************************************************************/ /**
   \fn          SHA1& SHA1::operator>>(Digest&)
 *******************************************************************************/
SHA1& SHA1::operator>>(Digest& md)
{
	md = value();
	return *this;
}

/***************************************************************************/ /**
   \fn          Digest SHA1::value()
   \brief       Return the value of the hash in a Digest structure

   \note The result of this function is undefined if operator() has not been called
      with "final" set to true.
 *******************************************************************************/
Digest SHA1::value() const
{
	Digest md(160);

	md[0] = static_cast< unsigned char >( h[0] >> 24 );
	md[1] = static_cast< unsigned char >( h[0] >> 16 );
	md[2] = static_cast< unsigned char >( h[0] >> 8 );
	md[3] = static_cast< unsigned char >( h[0] );

	md[4] = static_cast< unsigned char >( h[1] >> 24 );
	md[5] = static_cast< unsigned char >( h[1] >> 16 );
	md[6] = static_cast< unsigned char >( h[1] >> 8 );
	md[7] = static_cast< unsigned char >( h[1] );

	md[8]  = static_cast< unsigned char >( h[2] >> 24 );
	md[9]  = static_cast< unsigned char >( h[2] >> 16 );
	md[10] = static_cast< unsigned char >( h[2] >> 8 );
	md[11] = static_cast< unsigned char >( h[2] );

	md[12] = static_cast< unsigned char >( h[3] >> 24 );
	md[13] = static_cast< unsigned char >( h[3] >> 16 );
	md[14] = static_cast< unsigned char >( h[3] >> 8 );
	md[15] = static_cast< unsigned char >( h[3] );

	md[16] = static_cast< unsigned char >( h[4] >> 24 );
	md[17] = static_cast< unsigned char >( h[4] >> 16 );
	md[18] = static_cast< unsigned char >( h[4] >> 8 );
	md[19] = static_cast< unsigned char >( h[4] );

	return md;
}

/*=========================================================================*//**
   \bug Digest::bits is not well defined
     In practice, Digest::bits will always be a multiple of 8 (which, in turn,
     is likely equal to CHAR_BIT). However, if it is not, this source does not
     define how to pack bits. It is up to the hash initializing the Digest to
     set the data. This makes operator==() impossible to define, in particular.
     Possible solution: just run with big endian since bit string, when printed,
     should have all bits adjacent.
*//*==========================================================================*/
/// \todo Resolve bit/byte access
Digest::Digest(std::size_t b)
	: p(nullptr), n(0), bits(b)
{
	n = ( bits + ( CHAR_BIT - 1 ) ) / CHAR_BIT;
	p = new unsigned char [n];
	std::fill_n(p, n, 0);
}

Digest::Digest(const Digest& d)
	: p(nullptr), n(d.n), bits(d.bits)
{
	p = new unsigned char [n];

	for (std::size_t i = 0; i < n; ++i)
	{
		p[i] = d.p[i];
	}
}

Digest& Digest::operator=(const Digest& d)
{
	if ( &d != this )
	{
		Digest(d).swap(*this);
	}

	return *this;
}

bool Digest::operator==(const Digest& d) const
{
	if ( bits != d.bits )
	{
		return false;
	}

	bool e = true;

	for (std::size_t i = 0; i < n; ++i)
	{
		if ( p[i] != d.p[i] )
		{
			e = false;
		}
	}

	return e;
}

void Digest::swap(Digest& d)
{
	std::swap(p, d.p);
	std::swap(n, d.n);
	std::swap(bits, d.bits);
}

Digest::~Digest()
{
	delete[] p;
}

unsigned char& Digest::operator[](std::size_t i)
{
	return p[i];
}

const unsigned char& Digest::operator[](std::size_t i) const
{
	return p[i];
}

std::size_t Digest::size() const
{
	return bits;
}

std::string Digest::toString() const
{
	static const char digits[] = "0123456789abcdef";
	std::string       s;

	for (std::size_t i = 0; i < n; i++)
	{
		s.push_back(digits[( p[i] & 0xF0U ) >> 4]);
		s.push_back(digits[p[i] & 0x0FU]);
	}

	return s;
}

std::ostream& operator<<(
	std::ostream& o,
	const Digest& d
)
{
	return o << d.toString();
}

#ifdef TESTSUITE

#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iostream>
#include <iterator>
#include <vector>

const char* testmessages[][2] = { // Test strings for SHA-1
	{ "", "da39a3ee5e6b4b0d3255bfef95601890afd80709" },
	{ "abc", "a9993e364706816aba3e25717850c26c9cd0d89d" },
	{ "abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq", "84983e441c3bd26ebaae4aa1f95129e5e54670f1" },
	{ "The quick brown fox jumps over the lazy dog", "2fd4e1c67a2d28fced849ee1bb76e7391b93eb12" }};

bool alltests = true;

void test();

int main(
	int   argc,
	char* argv[]
)
{
	int i = 1;

	if ( argc == 1 )
	{
		test();
	}

	while ( i < argc )
	{
		ifstream f(argv[i], ios_base::in | ios_base::binary);
		Digest   d = SHA1::hash(istreambuf_iterator< char >(f), istreambuf_iterator< char >() );
		cout << d << ' ' << argv[i] << endl;
		++i;
	}

	return 0;
}

void test()
{
	cout << "========================================================\n"
	     << "Testing SHA-1 against known values\n"
	     << "--------------------------------------------------------\n";

	// A few small strings -------------------------------------------------
	for (unsigned j = 0; j < sizeof( testmessages ) / sizeof( testmessages[0] ); j++)
	{
		Digest md = SHA1::hash(reinterpret_cast< const unsigned char* >( testmessages[j][0] ),
			strlen(testmessages[j][0]) );

		cout << "Message:\t'" << testmessages[j][0] << "':\nResult:\t'" << md.toString() << '\'' << endl;

		if ( md.toString() == testmessages[j][1] )
		{
			cout << "PASSED" << endl;
		}
		else
		{
			cerr << "FAILED: " << testmessages[j][1] << endl;
			alltests = false;
		}

		cout << endl;
	}

	// A million 'a's ------------------------------------------------------
	SHA1           sha1;
	vector< char > million_a(1000000, '\x61');
	Digest         d2 = SHA1::hash(million_a.begin(), million_a.end() );

	cout << "Message:\tA million 'a's\nResult: \t" << d2 << endl;

	if ( d2.toString() == "34aa973cd4c4daa4f61eeb2bdbad27316534016f" )
	{
		cout << "PASSED" << endl;
	}
	else
	{
		cerr << "FAILED" << endl;
		alltests = false;
	}

	cout << "--------------------------------------------------------\n";

	if ( alltests )
	{
		cout << "All tests passed\n" << endl;
	}
	else
	{
		cerr << "ERROR!!: Not all tests passed\n" << endl;
	}
}

/*
   // \todo: Program returns hash of empty string when files are directories
    if (argc > 1) { // Calculate the SHA1 hash of the named files
        char * filecontents = NULL;
        if ((filecontents = (char * ) malloc (1024)) == NULL) cerr << "No memory available" << endl; else {

            for (int i = 1; i < argc; i++) { // loop over the file names
                FILE * inputfile = NULL;
                long int filesize = 0;

                // Open the file
                if ((inputfile = fopen(argv[i],"rb")) == NULL) cerr << "Cound not open file " << argv[i] << endl; else {
                // Seek to the end of the file to get filesize
                if (fseek(inputfile,0,SEEK_END) != 0) cerr << "Could not seek within file " << argv[i] << endl; else {
                // Get the file size
                if ((filesize = ftell(inputfile)) == -1L) cerr << "Could not determine size of file " << argv[i] << endl; else {
                // Allocate memory to hold the contents of the file
                if ((filecontents = (char * ) realloc (filecontents, filesize)) == NULL) cerr << "Out of memory" << endl; else {

                    // Read the file into memory
                    rewind(inputfile);
                    std::size_t bytesread = fread(filecontents,1,filesize,inputfile);
                    SHA1(md,filecontents,bytesread);
                    cout << std::hex << std::setfill('0') << std::setw(2);
                    for (int j = 0; j < 20; j++) cout << md[j];
                    cout << '\t' << argv[i] << endl;
                    cout << std::dec << std::setw(0);
                }}}
                fclose(inputfile);
                }
            }
            free (filecontents);
        }
    } else { // Print out some test results to confirm working
    cout << "You did not provide any filenames. Running Tests instead...\n" << endl;
 */
#endif // TESTSUITE
// ]]></programlisting></article>
