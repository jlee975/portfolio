#ifndef SHA1_HPP
#define SHA1_HPP

#include <string>

class Digest
{
	unsigned char* p;
	std::size_t    n;
	std::size_t    bits;
public:
	explicit Digest(std::size_t);
	Digest(const Digest&);
	~Digest();

	Digest& operator=(const Digest&);

	bool operator==(const Digest&) const;

	unsigned char& operator[](std::size_t);
	const unsigned char& operator[](std::size_t) const;
	std::string toString() const;
	std::size_t size() const;
	void swap(Digest&);
};

class SHA1
{
	unsigned long h[5]{};      // Intermediate hash values
	unsigned char buff[128]{}; // Partial block storage across calls

	unsigned long tlen_lo{ 0 }; // Total number of bytes processed
	unsigned long tlen_hi{ 0 };

	bool restart{ true };

	void reset();
	void process_block(const unsigned char*);
	void update_len(std::size_t);
	void last_block();
public:
	SHA1();

	SHA1& operator()(const unsigned char*, std::size_t, bool = true);
	SHA1& operator>>(Digest&);
	Digest value() const;
	static Digest hash(const unsigned char*, std::size_t);

	template< class InputIterator >
	static Digest hash(InputIterator, InputIterator);
};

/// \note the following requires an iterator that dereferences to unsigned
/// char, or 2s-complement signed char
template< class InputIterator >
Digest SHA1::hash(
	InputIterator begin,
	InputIterator end
)
{
	SHA1          s;
	unsigned char t[256];

	while ( begin != end )
	{
		std::size_t i = 0;

		while ( i < sizeof( t ) && begin != end )
		{
			t[i++] = static_cast< unsigned char >( *begin++ );
		}

		s(t, i, false);
	}

	s(nullptr, 0, true);
	return s.value();
}

#endif // #ifdef SHA1_HPP
