#ifndef PBL_CRYPT_HASH_SHA256_H
#define PBL_CRYPT_HASH_SHA256_H

#include <array>
#include <cstddef>
#include <cstdint>
#include <cstdio>
#include <iosfwd>
#include <string>

// A structure that contains information about a hash
/// @todo replace unsigned long with std::uint_least32_t
class sha256_result
{
public:
	constexpr sha256_result()
		: H()
	{
	}

	constexpr sha256_result(
		std::uint_least32_t h1,
		std::uint_least32_t h2,
		std::uint_least32_t h3,
		std::uint_least32_t h4,
		std::uint_least32_t h5,
		std::uint_least32_t h6,
		std::uint_least32_t h7,
		std::uint_least32_t h8
	)
		: H{h1, h2, h3, h4, h5, h6, h7, h8}
	{
	}

	explicit sha256_result(const std::string& s);

	std::uint_least32_t& operator[](std::size_t i);

	const std::uint_least32_t& operator[](std::size_t i) const;

	std::string text() const;
private:
	std::array< std::uint_least32_t, 8 > H;
};

bool operator==(const sha256_result&, const sha256_result&);
bool operator!=(const sha256_result&, const sha256_result&);
bool operator==(const sha256_result&, const std::string&);
bool operator!=(const sha256_result&, const std::string&);
bool operator==(const std::string&, const sha256_result&);
bool operator!=(const std::string&, const sha256_result&);
std::ostream& operator<<(std::ostream&, const sha256_result&);

sha256_result sha256_raw(const void* s_, std::size_t n);
sha256_result sha256_file(std::FILE* f);
sha256_result sha256_file(const char* filename);

class SHA256
{
public:
	SHA256();

	void process(const char*, std::size_t);
	sha256_result finish();
private:
	// A structure that contains a block of input to process
	using block_t = std::array< std::uint_least32_t, 16 >;

	// A structure to hold an SHA-256 schedule
	using schedule_t = std::array< std::uint_least32_t, 64 >;

	struct sha256_inter
	{
		sha256_result H;
		std::size_t size;
	};

	static block_t create_block(const unsigned char data[64]);
	static schedule_t create_schedule(const block_t&);
	static void process_block(sha256_inter*, const block_t& block);

	sha256_inter                     h;
	std::array< unsigned char, 128 > buf;
	std::size_t                      idx{ 0 };
};

#endif // ifndef PBL_CRYPT_HASH_SHA256_H
