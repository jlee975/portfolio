/// @todo Expose unit tests
#include <cassert>
#include <cstddef>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <stdexcept>

#include "sha256.h"

// Data used by SHA-256
constexpr std::uint_least32_t K256[64] = { UINT32_C(0x428a2f98), UINT32_C(0x71374491), UINT32_C(0xb5c0fbcf), UINT32_C(0xe9b5dba5), UINT32_C(0x3956c25b),
	                                       UINT32_C(0x59f111f1), UINT32_C(0x923f82a4), UINT32_C(0xab1c5ed5), UINT32_C(0xd807aa98), UINT32_C(0x12835b01),
	                                       UINT32_C(0x243185be), UINT32_C(0x550c7dc3), UINT32_C(0x72be5d74), UINT32_C(0x80deb1fe), UINT32_C(0x9bdc06a7),
	                                       UINT32_C(0xc19bf174), UINT32_C(0xe49b69c1), UINT32_C(0xefbe4786), UINT32_C(0x0fc19dc6), UINT32_C(0x240ca1cc),
	                                       UINT32_C(0x2de92c6f), UINT32_C(0x4a7484aa), UINT32_C(0x5cb0a9dc), UINT32_C(0x76f988da), UINT32_C(0x983e5152),
	                                       UINT32_C(0xa831c66d), UINT32_C(0xb00327c8), UINT32_C(0xbf597fc7), UINT32_C(0xc6e00bf3), UINT32_C(0xd5a79147),
	                                       UINT32_C(0x06ca6351), UINT32_C(0x14292967), UINT32_C(0x27b70a85), UINT32_C(0x2e1b2138), UINT32_C(0x4d2c6dfc),
	                                       UINT32_C(0x53380d13), UINT32_C(0x650a7354), UINT32_C(0x766a0abb), UINT32_C(0x81c2c92e), UINT32_C(0x92722c85),
	                                       UINT32_C(0xa2bfe8a1), UINT32_C(0xa81a664b), UINT32_C(0xc24b8b70), UINT32_C(0xc76c51a3), UINT32_C(0xd192e819),
	                                       UINT32_C(0xd6990624), UINT32_C(0xf40e3585), UINT32_C(0x106aa070), UINT32_C(0x19a4c116), UINT32_C(0x1e376c08),
	                                       UINT32_C(0x2748774c), UINT32_C(0x34b0bcb5), UINT32_C(0x391c0cb3), UINT32_C(0x4ed8aa4a), UINT32_C(0x5b9cca4f),
	                                       UINT32_C(0x682e6ff3), UINT32_C(0x748f82ee), UINT32_C(0x78a5636f), UINT32_C(0x84c87814), UINT32_C(0x8cc70208),
	                                       UINT32_C(0x90befffa), UINT32_C(0xa4506ceb), UINT32_C(0xbef9a3f7), UINT32_C(0xc67178f2) };

// Miscellaneous little functions used by SHA-256
constexpr std::uint_least32_t Ch(
	std::uint_least32_t x,
	std::uint_least32_t y,
	std::uint_least32_t z
)
{
	return ( ( x & y ) ^ ( ~x & z ) ) & UINT32_C(0xffffffff);
}

constexpr std::uint_least32_t Maj(
	std::uint_least32_t x,
	std::uint_least32_t y,
	std::uint_least32_t z
)
{
	return ( x & y ) ^ ( x & z ) ^ ( y & z );
}

constexpr std::uint_least32_t rotr(
	std::uint_least32_t x,
	unsigned            n
)
{
	return ( ( x >> n ) | ( x << ( 32 - n ) ) ) & UINT32_C(0xFFFFFFFF);
}

constexpr std::uint_least32_t shr(
	std::uint_least32_t x,
	unsigned            n
)
{
	return x >> n;
}

constexpr std::uint_least32_t SIGMA_256_0(std::uint_least32_t x)
{
	return rotr(x, 2) ^ rotr(x, 13) ^ rotr(x, 22);
}

constexpr std::uint_least32_t SIGMA_256_1(std::uint_least32_t x)
{
	return rotr(x, 6) ^ rotr(x, 11) ^ rotr(x, 25);
}

constexpr std::uint_least32_t sigma_256_0(std::uint_least32_t x)
{
	return rotr(x, 7) ^ rotr(x, 18) ^ shr(x, 3);
}

constexpr std::uint_least32_t sigma_256_1(std::uint_least32_t x)
{
	return rotr(x, 17) ^ rotr(x, 19) ^ shr(x, 10);
}

// A "constructor" for sha256_t
constexpr sha256_result initial_hash()
{
	return { UINT32_C(0x6a09e667), UINT32_C(0xbb67ae85), UINT32_C(0x3c6ef372), UINT32_C(0xa54ff53a),
	         UINT32_C(0x510e527f), UINT32_C(0x9b05688c), UINT32_C(0x1f83d9ab), UINT32_C(0x5be0cd19) };
}

// Transforms a block of raw data into big-endian words
SHA256::block_t SHA256::create_block(const unsigned char data[64])
{
	block_t b;

	for (std::size_t i = 0; i < 16; ++i)
	{
		std::uint_least32_t x = 0;

		for (std::size_t j = 0; j < 4; ++j)
		{
			x |= ( ( static_cast< std::uint_least32_t >( data[4 * i + j] ) ) << ( 8 * ( 3 - j ) ) );
		}

		b[i] = x;
	}

	return b;
}

// Initializes a schedule_t structure from a block of data
SHA256::schedule_t SHA256::create_schedule(const block_t& block)
{
	schedule_t schedule;

	for (std::size_t i = 0; i < 16; ++i)
	{
		schedule[i] = block[i];
	}

	for (std::size_t i = 16; i < 64; ++i)
	{
		std::uint_least32_t x = sigma_256_1(schedule[i - 2]) + schedule[i - 7] + sigma_256_0(schedule[i - 15])
		                        + schedule[i - 16];
		schedule[i] = x & UINT32_C(0xFFFFFFFF);
	}

	return schedule;
}

// Processes an individual block of data, updating the hash object
void SHA256::process_block(
	sha256_inter*  hash,
	const block_t& block
)
{
	const schedule_t schedule = create_schedule(block);

	std::uint_least32_t a = hash->H[0];
	std::uint_least32_t b = hash->H[1];
	std::uint_least32_t c = hash->H[2];
	std::uint_least32_t d = hash->H[3];
	std::uint_least32_t e = hash->H[4];
	std::uint_least32_t f = hash->H[5];
	std::uint_least32_t g = hash->H[6];
	std::uint_least32_t h = hash->H[7];

	for (std::size_t t = 0; t < 64; ++t)
	{
		const std::uint_least32_t T1
		    = ( h + SIGMA_256_1(e) + Ch(e, f, g) + K256[t] + schedule[t] ) & UINT32_C(0xFFFFFFFF);
		const std::uint_least32_t T2 = ( SIGMA_256_0(a) + Maj(a, b, c) ) & UINT32_C(0xFFFFFFFF);
		h = g;
		g = f;
		f = e;
		e = ( d + T1 ) & UINT32_C(0xFFFFFFFF);
		d = c;
		c = b;
		b = a;
		a = ( T1 + T2 ) & UINT32_C(0xFFFFFFFF);
	}

	hash->H[0] = ( a + hash->H[0] ) & UINT32_C(0xFFFFFFFF);
	hash->H[1] = ( b + hash->H[1] ) & UINT32_C(0xFFFFFFFF);
	hash->H[2] = ( c + hash->H[2] ) & UINT32_C(0xFFFFFFFF);
	hash->H[3] = ( d + hash->H[3] ) & UINT32_C(0xFFFFFFFF);
	hash->H[4] = ( e + hash->H[4] ) & UINT32_C(0xFFFFFFFF);
	hash->H[5] = ( f + hash->H[5] ) & UINT32_C(0xFFFFFFFF);
	hash->H[6] = ( g + hash->H[6] ) & UINT32_C(0xFFFFFFFF);
	hash->H[7] = ( h + hash->H[7] ) & UINT32_C(0xFFFFFFFF);
}

// Applies padding to an incomplete block of input
static int pad_message(
	unsigned char* data,
	std::size_t    n,
	std::size_t    bytesize
)
{
	std::uint_least32_t bitsize_lo = ( bytesize << 3 ) & UINT32_C(0xffffffff);
	std::uint_least32_t bitsize_hi = ( bytesize >> 29 ) & UINT32_C(0xffffffff);

	assert(data != nullptr && n < 64);
	data[n++] = static_cast< unsigned char >( '\x80' );

	while ( n % 64 != 56 )
	{
		data[n++] = '\0';
	}

	for (std::size_t j = 8; j > 4; --j)
	{
		data[n + ( j - 1 )] = static_cast< unsigned char >( bitsize_lo );
		bitsize_lo        >>= 8;
	}

	for (std::size_t j = 4; j > 0; --j)
	{
		data[n + ( j - 1 )] = static_cast< unsigned char >( bitsize_hi );
		bitsize_hi        >>= 8;
	}

	return n == 120 ? 1 : 0;
}

// Passes a block of data to lower level functions. Handles padding
// Calculate the sha256sum of a file
sha256_result sha256_file(const char* filename)
{
	if ( std::FILE* f = std::fopen(filename, "rb") )
	{
		const sha256_result& h = sha256_file(f);
		std::fclose(f);
		return h;
	}

	return initial_hash();
}

sha256_result sha256_file(std::FILE* f)
{
	if ( f != nullptr )
	{
		char buf[128];

		std::rewind(f);

		SHA256 s;

		while ( feof(f) == 0 )
		{
			std::size_t nread = std::fread(buf, 1, 64, f);

			s.process(buf, nread);
		}

		return s.finish();
	}

	return initial_hash();
}

// calculate the sha256sum of an array
sha256_result sha256_raw(
	const void* s_,
	std::size_t n
)
{
	SHA256 t;

	t.process(static_cast< const char* >( s_ ), n);
	return t.finish();
}

SHA256::SHA256()
	: h{initial_hash(), 0}
{
}

/// @todo Whole blocks could be processed in place
void SHA256::process(
	const char* s_,
	std::size_t n
)
{
	if ( s_ == nullptr && n != 0 )
	{
		throw std::invalid_argument("Null pointer");
	}

	auto s = reinterpret_cast< const unsigned char* >( s_ );

	for (std::size_t i = 0; i < n;)
	{
		// Copy data to local buffer
		const std::size_t nread = ( n - i >= 64 - idx ? 64 - idx : n - i );
		memcpy(buf.data() + idx, s + i, nread);
		i += nread;

		const std::size_t xnread = idx + nread;

		if ( xnread < 64 )
		{
			// Partial (last) block
			h.size += xnread;
			idx     = xnread;
		}
		else
		{
			// Full block
			h.size += xnread;
			block_t b = create_block(buf.data() );
			process_block(&h, b);
			idx = 0;
		}
	}
}

sha256_result SHA256::finish()
{
	// Process the last block
	const int extra_block = pad_message(buf.data(), idx, h.size);
	block_t   b           = create_block(buf.data() );

	process_block(&h, b);

	if ( extra_block != 0 )
	{
		b = create_block(buf.data() + 64);
		process_block(&h, b);
	}

	return h.H;
}

sha256_result::sha256_result(const std::string& s)
	: H()
{
	if ( s.length() != 64 )
	{
		throw std::invalid_argument("Not an SHA256 hash");
	}

	for (std::size_t i = 0; i < 8; ++i)
	{
		for (std::size_t j = 0; j < 8; ++j)
		{
			std::uint_least32_t z = 0;

			const char c = s[8 * i + j];

			if ( '0' <= c && c <= '9' )
			{
				z = ( c - '0' );
			}
			else if ( 'A' <= c && c <= 'F' )
			{
				z = ( c - 'A' ) + 10;
			}
			else if ( 'a' <= c && c <= 'f' )
			{
				z = ( c - 'a' ) + 10;
			}
			else
			{
				throw std::invalid_argument("Not an SHA256 hash");
			}

			H[i] |= z << ( 4 * ( 7 - j ) );
		}
	}
}

const std::uint_least32_t& sha256_result::operator[](std::size_t i) const
{
	return H[i];
}

std::uint_least32_t& sha256_result::operator[](std::size_t i)
{
	return H[i];
}

std::string sha256_result::text() const
{
	std::string s;

	s.reserve(64);

	// Convert to text
	for (std::size_t i = 0; i < 8; ++i)
	{
		for (std::size_t j = 0; j < 8; ++j)
		{
			s.push_back( ( "0123456789abcdef" )[( H[i] >> ( 4 * ( 7 - j ) ) ) & 0xful]);
		}
	}

	return s;
}

bool operator==(
	const sha256_result& l,
	const sha256_result& r
)
{
	for (std::size_t i = 0; i < 8; ++i)
	{
		if ( l[i] != r[i] )
		{
			return false;
		}
	}

	return true;
}

bool operator!=(
	const sha256_result& l,
	const sha256_result& r
)
{
	return !( l == r );
}

bool operator==(
	const sha256_result& l,
	const std::string&   r
)
{
	return l == sha256_result(r);
}

bool operator!=(
	const std::string&   r,
	const sha256_result& l
)
{
	return l != sha256_result(r);
}

bool operator==(
	const std::string&   r,
	const sha256_result& l
)
{
	return l == sha256_result(r);
}

bool operator!=(
	const sha256_result& l,
	const std::string&   r
)
{
	return l != sha256_result(r);
}

std::ostream& operator<<(
	std::ostream&        os,
	const sha256_result& h
)
{
	return os << h.text();
}
