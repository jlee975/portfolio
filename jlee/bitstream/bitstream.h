#ifndef BITSTREAM_H
#define BITSTREAM_H

#include <climits>
#include <cstdint>

/**
 * @todo length_type might be better off signed, especially so we can catch "negative" lengths due to shifting
 *
 * @note Unfortunately, L cannot be uint_fast8_t as this is unsigned char (on GCC, at least). This pretty much
 * ruins all arithmetic with length_type since the result will be int, a different type altogether. And,
 * perhaps more importantly, a type of different signedness.
 */
template< typename U = std::uintmax_t, typename L = std::uint_fast16_t >
struct basic_bits
{
	using word_type   = U;
	using length_type = L;

	static constexpr length_type maxlength = sizeof( word_type ) * CHAR_BIT;

	word_type value;
	length_type length;

	// shifting more then length bits is undefined
	constexpr basic_bits& operator>>=(length_type n)
	{
		value >>= n;
		length -= n;
		return *this;
	}

	// shifting more than length bits is undefined
	constexpr basic_bits operator>>(length_type n) const
	{
		return { value >> n, length - n };
	}

	/// @todo Could be faster with a binary search
	/// @todo Move to bits module
	length_type nlz() const
	{
		length_type s = 0;

		if ( length != 0 )
		{
			for (word_type m = word_type{ 1 } << ( length - 1 ); ( value & m ) == 0; m >>= 1)
			{
				++s;
			}
		}

		return s;
	}

};

/// @todo Can probably reverse the whole word and then shift.
/// @todo Move to bits module
template< typename U, typename L >
constexpr basic_bits< U, L > reverse_bits(const basic_bits< U, L >& x)
{
	typename basic_bits< U, L >::word_type y = 0;

	for (typename basic_bits< U, L >::length_type i = 0; i < x.length; ++i)
	{
		y |= ( ( x.value >> i ) & 1 ) << ( x.length - 1 - i );
	}

	return { y, x.length };
}

using bits_ = basic_bits<>;

enum unexpected_eos_type
{
	unexpected_eos
};

#endif // BITSTREAM_H
