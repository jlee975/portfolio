#ifndef LEBITSTREAM_H
#define LEBITSTREAM_H

#include <cstddef>

#include "bitstream.h"

/** @brief Wrap data so it can be read as a (little endian) bitstream
 *
 * This class buffers the underlying data as machine sized words in the correct endiannes (the members word1
 * and word2).
 *
 * A bitstream is "little endian" if the bits are packed in the least significant bit first. This way, "words"
 * that cross byte boundaries are ordered correctly when the bytes are placed in little endian order.
 *
 * Ex., suppose a bitstream encodes three words over two bytes: A (6 bits), B (5 bits), and C (5 bits). A is
 * followed by B, which is followed by C in the stream. There are two ways to place these words in two
 * consecutive bytes:
 *
 *    Little Endian:                 Bit Endian:
 *         BBAAAAAA CCCCCBBB               AAAAAABB BBBCCCCC
 *         76543210 76543210               76543210 76543210
 *          byte 1   byte 2                 byte 1   byte 2
 *
 * @todo Should be moveable, and maybe even copyable
 * @todo Align pBitstream pointer on long boundary for better performance
 * @todo read(1) happens enough that it may be beneficial to write a separate routine
 * @todo Balance open with a CloseBitstream
 * @todo Wrap other things besides in-memory arrays
 * @todo In move_to_next_by_boundary, throw if there's not enough data. Right now, we can always read
 * up to the next byte, because we don't have bit-resolution for the end of the stream.
 */
class ILEBitstream
{
public:
	using bits = bits_;

	ILEBitstream();
	ILEBitstream(const ILEBitstream&) = delete;
	ILEBitstream(ILEBitstream&&)      = delete;
	~ILEBitstream();
	ILEBitstream& operator=(const ILEBitstream&) = delete;
	ILEBitstream& operator=(ILEBitstream&&)      = delete;

	/** @brief Begin reading from given data
	 * @param p Pointer to data
	 * @param n Number of bytes in data
	 */
	void open(const std::byte* p, std::size_t n);

	/** @brief Append data to the bitstream. Requires previous data to be mostly completed
	 *
	 * Data can be appended if the end of the previous data has been cached.
	 *
	 * This function facilitates handling of multiple blocks of bitstream data. Once one block
	 * has been read until an eos condition is thrown, the user can append a new block and
	 * resume reading
	 */
	void append(const std::byte*, std::size_t);

	/** @brief Reset to invalid state. Not reading from any data
	 */
	void close();

	/** @brief Read number of bits indicated
	 * @param n Number of bits to read
	 *
	 * @todo Needs a way to indicate failure
	 *
	 * If there's not enough bits to read, throws a "bits" object, equivalent to the result of peek
	 */
	bits::word_type read(bits::length_type n);

	/** @brief Get as many bits as possible, without advancing the read pointer
	 *
	 * This should always return a full bits object (i.e., with the largest possible width), unless we are
	 * close to end of stream
	 */
	bits peek() const;

	/** @brief Discards the remaining bits in the stream's current byte, and move ahead.
	 *
	 * Some functions require this when fields are byte-aligned.
	 */
	void move_to_next_byte_boundary();

	void discard(bits::length_type);
private:
	/// First word of available data, in correct byte order
	bits word1;

	/// Second word of available data, in correct byte order. Moved to word1 when word1 is consumed.
	bits word2;

	/// Pointer to original data
	const std::byte* pBitstream{ nullptr };

	/// Size of original data
	std::size_t nBitstream{ 0 };

	/// Position of next word to read from pBitstream. iBitstream <= nBitstream, always
	std::size_t iBitstream{ 0 };
};

class OLEBitstream
{
public:
	using bits = bits_;

	OLEBitstream();

	OLEBitstream(const OLEBitstream&) = delete;
	OLEBitstream(OLEBitstream&&)      = delete;
	~OLEBitstream();
	OLEBitstream& operator=(const OLEBitstream&) = delete;
	OLEBitstream& operator=(OLEBitstream&&)      = delete;
	void open(std::byte*, std::size_t);
	void close();
	void write(const bits&);
	std::size_t size() const;
private:
	std::byte*  pBitstream{ nullptr };
	std::size_t nBitstream{ 0 };

	// Current write position
	std::size_t       iBitstream{ 0 };
	bits::length_type wpos_bit{ 0 };
};
#endif // LEBITSTREAM_H
