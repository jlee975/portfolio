#include "bebitstream.h"

#include <algorithm>
#include <climits>
#include <limits>
#include <stdexcept>

#include "endian/endian.h"

namespace
{
/** @brief Helper function to load raw data into appropriate byte order
 * @param p Pointer to data
 * @param n Number of bytes at p
 *
 * This version of the function checks the number of bytes available, in case we are near the end of the
 * data.
 */
IBEBitstream::bits read_word(
	const std::byte* p,
	std::size_t      n
)
{
	constexpr std::size_t word_size = sizeof( IBEBitstream::bits::word_type );
	const auto            x         = beread< IBEBitstream::bits::maxlength >(p, n);

	const std::size_t m = std::min(n, word_size);

	return { x >> ( ( word_size - m ) * CHAR_BIT ), m* CHAR_BIT };
}

}

IBEBitstream::IBEBitstream()
	: word1{0, 0}, word2{0, 0}
{
}

IBEBitstream::~IBEBitstream()
{
	close();
}

void IBEBitstream::close()
{
	pBitstream   = nullptr;
	nBitstream   = 0;
	iBitstream   = 0;
	word1.length = 0;
	word2.length = 0;
}

void IBEBitstream::open(
	const std::byte* pData,
	std::size_t      n
)
{
	close();

	if ( pData != nullptr )
	{
		// Point to the memory location and set our index within the block to 0
		pBitstream = pData;
		nBitstream = n;

		// Read the first cache words and store them
		word1 = read_word(pBitstream, n);

		constexpr std::size_t word_size = sizeof( IBEBitstream::bits::word_type );

		if ( n > word_size )
		{
			word2 = read_word(pBitstream + word_size, n - word_size);
		}
		else
		{
			word2 = { 0, 0 };
		}

		iBitstream = 0;
	}
}

void IBEBitstream::move_to_next_byte_boundary()
{
	if ( ( word1.length & 7 ) != 0 )
	{
		discard(word1.length & 7);
	}
}

IBEBitstream::bits IBEBitstream::peek() const
{
	if ( word1.length == bits::maxlength )
	{
		return word1;
	}

	const std::size_t m = std::min(bits::maxlength, word1.length + word2.length);

	return { ( word1.value << ( m - word1.length ) ) | ( word2.value >> ( word1.length + word2.length - m ) ), m };
}

/// @todo Optimize
void IBEBitstream::discard(bits::length_type n)
{
	/// @bug Unlike read, should not throw if n is very large
	read(n);
}

IBEBitstream::bits::word_type IBEBitstream::read(bits::length_type n)
{
	if ( n < word1.length )
	{
		if ( n == 0 )
		{
			return 0;
		}

		const bits::word_type w = word1.value >> ( word1.length - n );
		word1.value  &= ~( ( ~bits::word_type{ 0 } ) << ( word1.length - n ) );
		word1.length -= n;
		return w;
	}

	if ( n > bits::maxlength )
	{
		throw std::runtime_error("Too many bits");
	}

	if ( n - word1.length > word2.length )
	{
		throw std::runtime_error("Not enough bits in stream");
	}

	bits::word_type w;

	if ( n - word1.length != 0 )
	{
		const bits::length_type m = word2.length - ( n - word1.length );

		w = ( word1.value << ( n - word1.length ) ) | ( word2.value >> m );

		word1.value  = word2.value & ~( ( ~bits::word_type{ 0 } ) << m );
		word1.length = m;
	}
	else
	{
		// n == word1.length
		w = word1.value;

		word1 = word2;
	}

	constexpr std::size_t word_size = sizeof( bits::word_type );

	if ( word_size < nBitstream - iBitstream )
	{
		iBitstream += word_size;

		if ( nBitstream - iBitstream > word_size )
		{
			// we can get another word
			word2 = read_word(pBitstream + iBitstream + word_size, nBitstream - iBitstream - word_size);
		}
		else
		{
			word2 = { 0, 0 };
		}
	}
	else
	{
		iBitstream = nBitstream;
		word2      = { 0, 0 };
	}

	return w;
}
