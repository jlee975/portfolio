#include "lebitstream.h"

#include <algorithm>
#include <climits>
#include <limits>
#include <stdexcept>

#include "endian/endian.h"

namespace
{
/** @brief Helper function to load raw data into appropriate byte order
 * @param p Pointer to data
 * @param n Number of bytes at p
 *
 * This version of the function checks the number of bytes available, in case we are near the end of the
 * data.
 */
ILEBitstream::bits read_word(
	const std::byte* p,
	std::size_t      n
)
{
	constexpr std::size_t word_size = sizeof( ILEBitstream::bits::word_type );

	const std::size_t m = std::min(n, word_size);

	return { leread< CHAR_BIT * sizeof( ILEBitstream::bits::word_type ) >(p, n), m* CHAR_BIT };
}

}

ILEBitstream::ILEBitstream()
	: word1{0, 0}, word2{0, 0}
{
}

ILEBitstream::~ILEBitstream()
{
	close();
}

void ILEBitstream::close()
{
	pBitstream   = nullptr;
	nBitstream   = 0;
	iBitstream   = 0;
	word1.length = 0;
	word2.length = 0;
}

void ILEBitstream::open(
	const std::byte* pData,
	std::size_t      n
)
{
	/// @todo If open already, do nothing
	close();
	append(pData, n);
}

void ILEBitstream::append(
	const std::byte* pData,
	std::size_t      n
)
{
	// compact word1 and word2
	if ( word2.length != 0 && word1.length != bits::maxlength )
	{
		word1.value = word1.value | ( word2.value << word1.length );

		if ( word2.length > bits::maxlength - word1.length )
		{
			const auto m = bits::maxlength - word1.length;
			word1.length = bits::maxlength;
			word2      >>= m;
		}
		else
		{
			word1.length += word2.length;
			word2         = { 0, 0 };
		}
	}

	// read remainder of pBitstream into word1/word2
	if ( pBitstream != nullptr && iBitstream < nBitstream )
	{
		// word2 would have been full before compaction
		if ( nBitstream - iBitstream <= ( bits::maxlength - word2.length ) / CHAR_BIT )
		{
			const auto t = read_word(pBitstream + iBitstream, nBitstream - iBitstream);

			word2.value  |= t.value << word2.length;
			word2.length += t.length;
		}
		else
		{
			throw std::runtime_error("Still have remaining data");
		}

		pBitstream = nullptr;
		iBitstream = 0;
		nBitstream = 0;
	}

	// append data, optionally filling word1, word2
	if ( pData != nullptr )
	{
		// Point to the memory location and set our index within the block to 0
		pBitstream = pData;
		iBitstream = 0;
		nBitstream = n;

		constexpr std::size_t word_size = sizeof( ILEBitstream::bits::word_type );

		if ( word1.length == 0 )
		{
			// Fill both cache words
			word1 = read_word(pBitstream, n);

			if ( n > word_size )
			{
				word2      = read_word(pBitstream + word_size, n - word_size);
				iBitstream = ( n - word_size >= word_size ) ? 2 * word_size : n;
			}
			else
			{
				word2      = { 0, 0 };
				iBitstream = n;
			}
		}
		else if ( word2.length == 0 )
		{
			// Fill second cache word
			word2      = read_word(pBitstream, n);
			iBitstream = std::min(word_size, n);
		}
	}
}

void ILEBitstream::move_to_next_byte_boundary()
{
	if ( ( word1.length & 7 ) != 0 )
	{
		discard(word1.length & 7);
	}
}

ILEBitstream::bits ILEBitstream::peek() const
{
	if ( word1.length == bits::maxlength )
	{
		return word1;
	}

	return { word1.value | ( word2.value << word1.length ), std::min(bits::maxlength, word1.length + word2.length) };
}

void ILEBitstream::discard(bits::length_type n)
{
	if ( n < word1.length )
	{
		word1 >>= n;
		return;
	}

	constexpr std::size_t word_size = sizeof( bits::word_type );

	if ( n - word1.length > word2.length )
	{
		// Discard both cached words and effectively seek to a position
		n -= word1.length + word2.length;

		if ( n / CHAR_BIT >= nBitstream - iBitstream )
		{
			// Seeking past end of bitstream
			iBitstream = nBitstream;
			word1      = { 0, 0 };
			word2      = { 0, 0 };
			return;
		}

		// Seek and rebuild cachewords
		const auto nwords = n / bits::maxlength;
		const auto nrem   = n % bits::maxlength;

		iBitstream += nwords * word_size;
		word1       = read_word(pBitstream + iBitstream, nBitstream - iBitstream) >> nrem;

		if ( word_size < nBitstream - iBitstream )
		{
			// we can get another word
			word2      = read_word(pBitstream + iBitstream + word_size, nBitstream - iBitstream - word_size);
			iBitstream = std::min(iBitstream + word_size + word_size, nBitstream);
		}
		else
		{
			iBitstream = nBitstream;
			word2      = { 0, 0 };
		}

		return;
	}

	if ( n - word1.length != 0 )
	{
		word1 = word2 >> ( n - word1.length );
	}
	else
	{
		// n == word1.length
		word1 = word2;
	}

	if ( iBitstream < nBitstream )
	{
		// we can get another word
		word2      = read_word(pBitstream + iBitstream, nBitstream - iBitstream);
		iBitstream = std::min(iBitstream + word_size, nBitstream);
	}
	else
	{
		word2 = { 0, 0 };
	}
}

ILEBitstream::bits::word_type ILEBitstream::read(bits::length_type n)
{
	if ( n < word1.length )
	{
		const bits::word_type w = word1.value & ~( ~bits::word_type(0) << n );
		word1 >>= n;
		return w;
	}

	if ( n > bits::maxlength )
	{
		throw std::invalid_argument("Too many bits");
	}

	if ( n - word1.length > word2.length )
	{
		// Basically, peek but we know word1.length != bits::maxlength
		throw unexpected_eos;
	}

	bits::word_type w;

	if ( n - word1.length != 0 )
	{
		w     = ( n == bits::maxlength ) ? ~bits::word_type(0) : ~( ~bits::word_type(0) << n );
		w    &= word1.value | ( word2.value << word1.length );
		word1 = word2 >> ( n - word1.length );
	}
	else
	{
		// n == word1.length
		w = word1.value;

		word1 = word2;
	}

	constexpr std::size_t word_size = sizeof( bits::word_type );

	if ( iBitstream < nBitstream )
	{
		// we can get another word
		word2      = read_word(pBitstream + iBitstream, nBitstream - iBitstream);
		iBitstream = std::min(iBitstream + word_size, nBitstream);
	}
	else
	{
		word2 = { 0, 0 };
	}

	return w;
}

OLEBitstream::OLEBitstream() = default;

OLEBitstream::~OLEBitstream()
{
	close();
}

void OLEBitstream::open(
	std::byte*  pOut,
	std::size_t n
)
{
	pBitstream = pOut;
	nBitstream = n;
	iBitstream = 0;
	wpos_bit   = 0;
}

void OLEBitstream::close()
{
	pBitstream = nullptr;
	nBitstream = 0;
	iBitstream = 0;
	wpos_bit   = 0;
}

std::size_t OLEBitstream::size() const
{
	return iBitstream + ( wpos_bit != 0 ? 1 : 0 );
}

// FIXME: Move to chomp class and optimize
void OLEBitstream::write(const bits& w)
{
	const auto x = w.value;
	const auto n = w.length;

	std::uint_least32_t y = leread< 32 >(pBitstream + iBitstream);

	y = ( x << wpos_bit ) | ( y & ( ~( UINT32_C(-1) << wpos_bit ) ) );

	lewrite< 32 >(pBitstream + iBitstream, y);

	if ( n + wpos_bit > 32 )
	{
		y = ( x >> ( 32 - wpos_bit ) );
		lewrite< 32 >(pBitstream + ( iBitstream + 4 ), y);
	}

	const auto z = wpos_bit + n;

	wpos_bit    = z % 8;
	iBitstream += z / 8;
}
