#ifndef JL_UNICODE_BITS_H
#define JL_UNICODE_BITS_H

#include <cstdint>

namespace jl
{
namespace unicode
{
const std::uint_least32_t INVALID = 0x110000ul;

// Check that a "character" is, in fact, a legal unicode character
constexpr bool is_valid_codepoint(char32_t code)
{
	return ( code - UINT32_C(0xd800) >= UINT32_C(0x800) ) && ( code <= U'\U0010ffff' );
}

constexpr bool is_basic_latin(char32_t c)
{
	return c - UINT32_C(0x20) < UINT32_C(0x60);
}

constexpr bool is_utf8_mb(char32_t c)
{
	return c - UINT32_C(0xc0) < UINT32_C(0x38);
}

// The number of UTF8 continuation bytes after c. c must to be valid
/// @todo Return uint_least32_t. char32_t is supposed to be equivalent. Having difficulty finding
/// the rules for arithmetic promotion for this type
/// @todo jumpless
constexpr unsigned sequence_length(char32_t c)
{
	return c < UINT32_C(0xe0) ? 1u : ( ( c >> 4 ) - 12u );
}

// check if c is the first of a utf-16 surrogate pair
constexpr bool is_first_surrogate(char32_t c)
{
	return c - UINT32_C(0xd800) < UINT32_C(0x400);
}

// check if c is the second of a utf-16 surrogate pair
constexpr bool is_second_surrogate(char32_t c)
{
	return c - UINT32_C(0xdc00) < UINT32_C(0x400);
}

// get a code point from a utf-16 surrogate pair
constexpr char32_t decode_surrogate_pair(
	char32_t first,
	char32_t second
)
{
	return ( first << 10 ) + second - UINT32_C(0x35FDC00);
}

}
}

#endif // JL_UNICODE_BITS_H
