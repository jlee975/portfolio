#ifndef JL_JSON_UNICODE_H
#define JL_JSON_UNICODE_H

#include <cstdint>

namespace jl
{
namespace unicode
{
const std::uint_fast32_t INVALID = 0x110000ul;

// Check that a "character" is, in fact, a legal unicode character
constexpr bool is_valid_codepoint(std::uint_fast32_t code)
{
	return ( code < INVALID ) && ( code - UINT32_C(0xd800) >= UINT32_C(0x800) );
}

constexpr bool is_basic_latin(std::uint_fast32_t c)
{
	return c - UINT32_C(0x20) < UINT32_C(0x60);
}

constexpr bool is_basic_latin_space(std::uint_fast32_t c)
{
	// The lhs handles characters 0 and >=33. The rhs handles 1-32
	const auto s = c - UINT32_C(1);

	// This makes a bitmask corresponding to space, tab, cr, lf, ff, vt
	constexpr auto m = ( UINT32_C(1) << ( 9 - 1 ) )
	                   | ( UINT32_C(1) << ( 10 - 1 ) )
	                   | ( UINT32_C(1) << ( 11 - 1 ) )
	                   | ( UINT32_C(1) << ( 12 - 1 ) )
	                   | ( UINT32_C(1) << ( 13 - 1 ) )
	                   | ( UINT32_C(1) << ( 32 - 1 ) );

	return s < 32 && ( ( ( m >> s ) & UINT32_C(1) ) != 0 );
}

constexpr bool is_basic_latin_upper(std::uint_fast32_t c)
{
	return c - UINT32_C(65) < UINT32_C(26);
}

constexpr bool is_basic_latin_lower(std::uint_fast32_t c)
{
	return c - UINT32_C(97) < UINT32_C(26);
}

constexpr char to_basic_latin_upper(std::uint_fast32_t c)
{
	return static_cast< char >( is_basic_latin_lower(c) ? ( c - 32 ) : c );
}

constexpr bool is_basic_latin_alpha(std::uint_fast32_t c)
{
	return ( c - UINT32_C(65) < UINT32_C(26) ) || ( c - UINT32_C(97) < UINT32_C(26) );
}

constexpr bool is_basic_latin_digit(std::uint_fast32_t c)
{
	return c - UINT32_C(48) < UINT32_C(10);
}

constexpr bool is_basic_latin_nonzero_digit(std::uint_fast32_t c)
{
	return c - UINT32_C(49) < UINT32_C(9);
}

constexpr bool is_utf8_mb(std::uint_fast32_t c)
{
	return c - UINT32_C(0xc0) < UINT32_C(0x38);
}

// The number of UTF8 continuation bytes after c. c must to be valid
constexpr unsigned sequence_length(std::uint_fast32_t c)
{
	return c < UINT32_C(0xe0) ? 1u : ( ( c >> 4 ) - 12u );
}

// check if c is the first of a utf-16 surrogate pair
constexpr bool is_first_surrogate(std::uint_fast32_t c)
{
	return c - UINT32_C(0xd800) < UINT32_C(0x400);
}

// check if c is the second of a utf-16 surrogate pair
constexpr bool is_second_surrogate(std::uint_fast32_t c)
{
	return c - UINT32_C(0xdc00) < UINT32_C(0x400);
}

// get a code point from a utf-16 surrogate pair
constexpr char32_t decode_surrogate_pair(
	char32_t first,
	char32_t second
)
{
	return ( first << 10 ) + second - UINT32_C(0x35FDC00);
}

}
}

#endif // JL_JSON_UNICODE_H
