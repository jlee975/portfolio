#ifndef JL_UNICODE_USTRING_H
#define JL_UNICODE_USTRING_H

#include <iosfwd>
#include <vector>

class ustring
{
public:
	using char_type      = char32_t;
	using iterator       = std::vector< char_type >::iterator;
	using const_iterator = std::vector< char_type >::const_iterator;
	using size_type      = std::size_t;
	static constexpr size_type npos = size_type(-1);

	ustring() = default;

	ustring(size_type n, char_type c);

	ustring(const char*);
	ustring(const char32_t*);
	ustring(const char*, std::size_t);

	const char_type& operator[](size_type) const;
	char_type& operator[](size_type);

	bool empty() const;
	size_type length() const;

	size_type size() const
	{
		return length();
	}

	ustring substr(size_type i, size_type n = npos) const;
	ustring& assign(size_type, char_type);
	ustring& append(const ustring&);
	ustring& append(char_type);
	ustring& operator+=(const ustring&);

	static ustring from_utf8(std::istream& is);

	int compare(const ustring&) const;
	int compare(size_type i, size_type count1, const char_type* s, size_type count2) const;
	int compare(size_type i, size_type count1, const ustring&) const;
	void push_back(char_type);

	iterator begin();
	const_iterator begin() const;
	iterator end();
	const_iterator end() const;

	bool starts_with(char_type c) const;
	bool ends_with(char_type c) const;
	size_type find_last_of(char_type) const;
	size_type find(char_type) const;
private:
	std::vector< char_type > data;
};

bool operator==(const ustring&, const ustring&);
bool operator!=(const ustring&, const ustring&);
bool operator<(const ustring&, const ustring&);
bool operator<=(const ustring&, const ustring&);
bool operator>(const ustring&, const ustring&);
bool operator>=(const ustring&, const ustring&);

std::ostream& operator<<(std::ostream&, const ustring&);

ustring operator+(const ustring&, const ustring&);
ustring operator+(const ustring&, ustring::char_type);

#endif // JL_UNICODE_USTRING_H
