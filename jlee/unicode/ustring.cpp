#include "ustring.h"

#include <cstring>
#include <iostream>
#include <stdexcept>

ustring::ustring(
	size_type n,
	char_type c
)
	: data(n, c)
{
}

ustring::ustring(
	const char* s_,
	std::size_t n
)
{
	if ( s_ != nullptr )
	{
		for (std::size_t j = 0; j < n;)
		{
			const char c = s_[j];
			++j;

			char_type t = static_cast< unsigned char >( c );

			if ( t >= 0x7f )
			{
				int ncont;

				if ( ( t & 0xE0 ) == 0xC0 )
				{
					ncont = 1;
				}
				else if ( ( t & 0xF0 ) == 0xE0 )
				{
					ncont = 2;
				}
				else if ( ( t & 0xF8 ) == 0xF0 )
				{
					ncont = 3;
				}
				else
				{
					throw std::runtime_error("Invalid UTF8 byte");
				}

				t &= ( 1 << ( 6 - ncont ) ) - 1;

				for (int i = 0; i < ncont; ++i)
				{
					const char y = s_[j];
					++j;

					if ( y != 0 )
					{
						const char_type x = static_cast< unsigned char >( y );

						if ( ( x & 0xC0 ) != 0x80 )
						{
							throw std::runtime_error("Expected UTF8 continuation character");
						}

						t = ( t << 6 ) | ( x & 0x3f );
					}
				}
			}

			data.push_back(t);
		}
	}
}

ustring::ustring(const char* s)
	: ustring(s, std::strlen(s) )
{
}

ustring::ustring(const char32_t* s)
	: data(s, s + std::char_traits< char32_t >::length(s) )
{
}

const ustring::char_type& ustring::operator[](size_type i) const
{
	return data[i];
}

ustring::char_type& ustring::operator[](size_type i)
{
	return data[i];
}

bool ustring::empty() const
{
	return data.empty();
}

ustring::size_type ustring::length() const
{
	return data.size();
}

ustring ustring::substr(
	size_type i,
	size_type n
) const
{
	const size_type m = data.size();

	ustring u;

	if ( i < m )
	{
		if ( n > m - i )
		{
			n = m - i;
		}

		u.data.reserve(n);
		u.data.assign(data.begin() + i, data.begin() + ( i + n ) );
	}

	return u;
}

ustring& ustring::assign(
	size_type n,
	char_type c
)
{
	data.assign(n, c);
	return *this;
}

ustring& ustring::append(const ustring& x)
{
	data.insert(data.end(), x.data.begin(), x.data.end() );

	return *this;
}

ustring& ustring::append(ustring::char_type c)
{
	data.push_back(c);
	return *this;
}

ustring& ustring::operator+=(const ustring& x)
{
	return append(x);
}

/// @todo Normalization with ICU
ustring ustring::from_utf8(std::istream& is)
{
	std::vector< char_type > data;

	char c;

	while ( is.get(c) )
	{
		char_type t = static_cast< unsigned char >( c );

		if ( t >= 0x7f )
		{
			int ncont;

			if ( ( t & 0xE0 ) == 0xC0 )
			{
				ncont = 1;
			}
			else if ( ( t & 0xF0 ) == 0xE0 )
			{
				ncont = 2;
			}
			else if ( ( t & 0xF8 ) == 0xF0 )
			{
				ncont = 3;
			}
			else
			{
				throw std::runtime_error("Invalid UTF8 byte");
			}

			t &= ( 1 << ( 6 - ncont ) ) - 1;

			for (int i = 0; i < ncont; ++i)
			{
				if ( is.get(c) )
				{
					const char_type x = static_cast< unsigned char >( c );

					if ( ( x & 0xC0 ) != 0x80 )
					{
						throw std::runtime_error("Expected UTF8 continuation character");
					}

					t = ( t << 6 ) | ( x & 0x3f );
				}
			}
		}

		data.push_back(t);
	}

	ustring u;

	u.data.swap(data);
	return u;
}

// p, q not
int compare_private(
	const ustring::char_type* p,
	std::size_t               n,
	const ustring::char_type* q,
	std::size_t               m
)
{
	const std::size_t l = n < m ? n : m;

	std::size_t i = 0;

	while ( i < l && p[i] == q[i] )
	{
		++i;
	}

	if ( i == n )
	{
		return i == m ? 0 : -1;
	}

	if ( i == m )
	{
		return 1;
	}

	return p[i] < q[i] ? -1 : 1;
}

int ustring::compare(const ustring& u) const
{
	return compare_private(data.data(), data.size(), u.data.data(), u.data.size() );
}

int ustring::compare(
	size_type      i,
	size_type      n,
	const ustring& u
) const
{
	const std::size_t m = data.size();

	if ( i > m )
	{
		throw std::out_of_range("Bad string index");
	}

	return compare_private(data.data() + i, n < m - i ? n : m - i, u.data.data(), u.data.size() );
}

int ustring::compare(
	size_type                 i,
	size_type                 n,
	const ustring::char_type* s,
	size_type                 m
) const
{
	const std::size_t l = data.size();

	if ( i > l )
	{
		throw std::out_of_range("Bad string index");
	}

	return compare_private(data.data(), n < l - i ? n : l - i, s, m);
}

void ustring::push_back(ustring::char_type c)
{
	data.push_back(c);
}

ustring::iterator ustring::begin()
{
	return data.begin();
}

ustring::const_iterator ustring::begin() const
{
	return data.begin();
}

ustring::iterator ustring::end()
{
	return data.end();
}

ustring::const_iterator ustring::end() const
{
	return data.end();
}

bool ustring::starts_with(char_type c) const
{
	return !data.empty() && data.front() == c;
}

bool ustring::ends_with(char_type c) const
{
	return !data.empty() && data.back() == c;
}

ustring::size_type ustring::find_last_of(char_type c) const
{
	const size_type n = data.size();

	for (size_type i = 0; i < n; ++i)
	{
		if ( data[n - i - 1] == c )
		{
			return n - i - 1;
		}
	}

	return npos;
}

ustring::size_type ustring::find(char_type c) const
{
	for (size_type i = 0, n = data.size(); i < n; ++i)
	{
		if ( data[i] == c )
		{
			return i;
		}
	}

	return npos;
}

std::ostream& operator<<(
	std::ostream&  os,
	const ustring& u
)
{
	for (const char32_t c : u)
	{
		if ( c < 0x80 )
		{
			os << static_cast< char >( c );
		}
		else
		{
			os << '?';
		}
	}

	return os;
}

bool operator==(
	const ustring& l,
	const ustring& r
)
{
	return l.length() == r.length() && l.compare(r) == 0;
}

bool operator!=(
	const ustring& l,
	const ustring& r
)
{
	return l.length() != r.length() || l.compare(r) != 0;
}

bool operator<(
	const ustring& l,
	const ustring& r
)
{
	return l.compare(r) < 0;
}

bool operator<=(
	const ustring& l,
	const ustring& r
)
{
	return l.compare(r) <= 0;
}

bool operator>(
	const ustring& l,
	const ustring& r
)
{
	return l.compare(r) > 0;
}

bool operator>=(
	const ustring& l,
	const ustring& r
)
{
	return l.compare(r) >= 0;
}

ustring operator+(
	const ustring& l,
	const ustring& r
)
{
	ustring t(l);

	t.append(r);
	return t;
}

ustring operator+(
	const ustring&     l,
	ustring::char_type r
)
{
	ustring t(l);

	t.append(r);
	return t;
}
