#include "dft.h"

#include <cmath>
#include <numbers>

template< typename T >
void bitreversesort(
	const T     x[][2],
	std::size_t N,
	T           F[][2]
)
{
	unsigned l2N = 0;

	while ( ( N >> l2N ) > 1 )
	{
		++l2N;
	}

	for (std::size_t n = 0; n < N / 2; n++) // Re-sort the input
	{
		/// \bug m assumes N is 16 bits
		std::size_t m;
		m   = ( n >> 1 & 0x5555U ) | ( n << 1 & 0xAAAAU );
		m   = ( m >> 2 & 0x3333U ) | ( m << 2 & 0xCCCCU );
		m   = ( m >> 4 & 0x0F0FU ) | ( m << 4 & 0xF0F0U );
		m   = ( m >> 8 & 0x00FFU ) | ( m << 8 & 0xFF00U );
		m >>= ( 16 - l2N );

		if ( n <= m )
		{
			F[n][0] = x[m][0];
			F[n][1] = x[m][1];
		}
		else
		{
			F[N - n - 1][0] = x[N - m - 1][0];
			F[N - n - 1][1] = x[N - m - 1][1];
		}

		if ( n < m )
		{
			F[m][0] = x[n][0];
			F[m][1] = x[n][1];
		}
		else
		{
			F[N - m - 1][0] = x[N - n - 1][0];
			F[N - m - 1][1] = x[N - n - 1][1];
		}
	} // F = bitreversesort(x)

}

/***************************************************************************/ /**
 *  \fn          void DFT(double[][2], unsigned, double[][2], bool)
 *  \brief       Discrete Fourier Transform
 *
 *  A very general function for performing Discrete Fourier Transforms on an array
 *  of complex data. Contains a Cooley-Tukey implementation for the case that the
 *  input is a size that is a poer of two. Defaults to brute force for other cases.
 *
 *  The DFT is mostly included for interest's sake. It is not used in Vorbis (or in
 *  MP3 from what i remember). It was, however, a good place to start when writing
 *  the MDCT. Since i first wrote this though, the MDCT has been rewritten using
 *  a specialized factorization. So this was basically 'left over'. Because of this
 *  it is low on my list of things to optimize.
 *
 *  The inverse DFT is currently implented using the relationship that
 *  F^{-1}(x) = F(x*)* / N, i.e., the inverse is equal to the conjugate of the
 *  FFT of the conjugate of the input, divided by N.
 *
 *  \todo The DFT call should allow to omit the output array X, in which case the
 *     DFT will be done in place. Could do some of the first few butterflies by
 *     hand
 *  \todo Unrol the inner loop for symmetry.
 *  \todo It may be beneficial to write this as a template
 *  \todo Could use twiddles array like the other functions in this library
 *  \todo May want to use <complex>
 *******************************************************************************/
void DFT(
	double      x[][2],
	std::size_t N,
	double      F[][2]
)
{
	if ( N == 0 )
	{
		return; // nothing to do
	}

	if ( ( N & ( N - 1 ) ) == 0 ) // Use Cooley-Tukey when N = 2 ** k
	{
		bitreversesort(x, N, F);

		// Recursively calculate the DFTs
		double dt = -2.0 * std::numbers::pi;

		for (std::size_t k = 1; k < N; k <<= 1)
		{
			dt *= 0.5;

			for (std::size_t n = 0; n < k; n++)
			{
				const double theta = dt * n;

				/// @todo Table lookup
				const double wr = std::cos(theta);
				const double wi = std::sin(theta);

				for (unsigned long i = n; i < N; i += k + k)
				{
					double*      Y  = F[i];
					const double yr = Y[0];
					const double yi = Y[1];
					const double zr = wr * Y[k + k] - wi * Y[k + k + 1];
					const double zi = wi * Y[k + k] + wr * Y[k + k + 1];
					Y[0]         = yr + zr;
					Y[1]         = yi + zi;
					Y[k + k]     = yr - zr;
					Y[k + k + 1] = yi - zi;
				}
			}
		}
	}
	else // Brute force (no specialized FFT for n)
	{
		for (std::size_t k = 0; k < N; k++)
		{
			double yr = 0.0;

			double yi = 0.0;

			for (std::size_t n = 0; n < N; n++)
			{
				const double theta = ( -0.5 * std::numbers::pi * n * k ) / N;
				const double zr    = cos(theta);
				const double zi    = sin(theta);
				yr += x[n][0] * zr - x[n][1] * zi;
				yi += x[n][0] * zi + x[n][1] * zr;
			}

			F[k][0] = yr;
			F[k][1] = yi;
		}
	}
}

/// @bug Modifies x
void IDFT(
	double      x[][2],
	std::size_t N,
	double      F[][2]
)
{
	for (std::size_t n = 0; n < N; n++)
	{
		x[n][1] = -x[n][1];
	}

	DFT(x, N, F);

	const double invN = 1.0 / N;

	for (std::size_t n = 0; n < N; n++)
	{
		F[n][0] *= invN;
		F[n][1] *= -invN;
	}
}
