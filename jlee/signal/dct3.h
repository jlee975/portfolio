#ifndef DCT3_H
#define DCT3_H

#include <algorithm>
#include <array>
#include <cmath>
#include <cstddef>

/***************************************************************************/ /**
 *  \fn          DCTIII
 *
 *  Calculate the Type 3 Discrete Cosine Transform. Defined as
 *
 *  \sum_{n=0}^{N-1} x_n * cos [PI * n * (k + 0.5) / N]
 *
 *  \bug --------------------------------------------------------------------------
 *  - Eliminate the need for the scratch array, at least for the closing code.
 *
 *  \todo ---------------------------------------------------------------------------
 *
 *  - Use cache oblivious algorithm to accomplish the transpose starting this
 *  function. See Frigo.
 *  - Implement the scaling feature described by Shao and Johnson's paper.
 *  Scaled DCT should reduce the flops by 5%
 *  - Merge the DST post-processing with the twiddles.
 *  - Flatten the function to be non-recursive and calculate same sized DCTs in
 *  parallel.
 *  - Consider moving the special cases to the sub-call. That is, before iterating
 *  check if N2 or N4 equals 4 or 8 and do them in place.
 *
 *******************************************************************************/
constexpr double cas_pi8[2] = { 0.923879532511287, // cos(pi/8)
	                            0.382683432365090  // sin(pi/8)
};

constexpr double cas16[5][2] = // cosine and sine for multiples of pi/16
{
	{ 1.0, 0.0 },                             // cos(     0), sin(     0)
	{ 0.980785280403230, 0.195090322016128 }, // cos( pi/16), sin( pi/16)
	{ 0.923879532511287, 0.382683432365090 }, // cos(2pi/16), sin(2pi/16)
	{ 0.831469612302545, 0.555570233019602 }, // cos(3pi/16), sin(3pi/16)
	{ 0.707106781186548, 0.707106781186548 } // cos(4pi/16), sin(4pi/16)
};

template< typename T >
void DCTIII_4(T* X)
{
	T a = X[0];

	T c = X[1];

	T d = X[2] * ( std::numbers::sqrt2_v< T >/ 2. );

	T f = X[3];
	T b = c + f;

	b *= cas_pi8[0] + cas_pi8[1];
	c *= cas_pi8[1];
	f *= cas_pi8[0];
	b -= c;
	b -= f;
	c -= f;
	f  = a;
	f += d;
	a -= d;

	// Write output
	X[0] = f + b;
	X[1] = a + c;
	X[2] = a - c;
	X[3] = f - b;
}

template< typename T >
void DCTIII_8(T* X)
{
	T a = X[0];

	T b = X[2];

	T c = X[4];

	T d = X[6];

	T t;

	t  = ( b + d ) * ( cas16[2][0] + cas16[2][1] );
	b *= cas16[2][1];
	d *= cas16[2][0];
	t -= b + d;
	b -= d;
	c *= cas16[4][0];
	d  = c + a;
	a -= c;
	c  = a + b;
	a -= b;
	b  = d + t;
	d -= t;

	T w = X[7];
	T v = X[5] - w;
	T x = X[3] - v;
	T u = X[1] - x;

	v *= std::numbers::sqrt2_v< T >;
	x -= w;
	w *= std::numbers::sqrt2_v< T >;
	t  = u - v;
	v += u;
	u  = ( x + w ) * ( 2.0 * cas16[2][0] );
	w  = ( x - w ) * ( 2.0 * cas16[2][1] );
	x  = ( t + w ) * cas16[3][0];
	t  = ( t - w ) * cas16[3][1];
	w  = ( v - u ) * cas16[1][1];
	u  = ( v + u ) * cas16[1][0];

	// Write output
	X[0] = b + u;
	X[1] = c + x;
	X[2] = a + t;
	X[3] = d + w;
	X[4] = d - w;
	X[5] = a - t;
	X[6] = c - x;
	X[7] = b - u;
}

template< typename T >
void DCTIII(
	T*          X,
	std::size_t N
)
{
	if ( N == 4 ) // 4 muls, 10 adds
	{
		DCTIII_4(X);
	}
	else if ( N == 8 ) // 12 muls, 30 adds
	{
		DCTIII_8(X);
	}
	else if ( ( N % 8 ) == 0 )
	{
		std::size_t N2 = N >> 1;
		std::size_t N4 = N >> 2;

		// Deinterlace and butterfly
		T* scratch = new T[N2];

		for (std::size_t i = 0; i + i < N; i++)
		{
			X[i]       = X[i + i];
			scratch[i] = X[i + i + 1];
		}

		X[N2] = scratch[0];

		for (std::size_t i = 0; i + 1 < N4; i++)
		{
			T a = scratch[i + i + 1];
			T b = scratch[i + i + 2];
			X[N2 + i + 1]  = a + b;
			X[N2 + i + N4] = a - b;
		}

		delete[] scratch;

		DCTIII(X, N2);
		DCTIII(X + N2, N4);

		// Reverse array, and negate every other input for 3rd DCT
		for (std::size_t i = 0; i + i < N4; i++)
		{
			std::swap(X[N2 + N4 + i], X[N - i - 1]);
		}

		// \todo: The effect of the following for loop is to reverse the output. Code it directly?
		for (std::size_t i = 3 * N4 + 1; i < N; i += 2)
		{
			X[i] = -X[i];
		}

		DCTIII(X + 3 * N4, N4);

		double       t     = std::numbers::pi / ( N + N );
		double       s     = sin(t);
		double       c     = cos(t);
		const double alpha = 2.0 * s * s;
		const double beta  = 2.0 * s * c;
		T*           Y     = X + N2;

		for (std::size_t i = 0, j = N2 - 1; i < N4;)
		{
			T v = Y[i];
			T w = Y[j];
			T u = c * w - s * v;
			v = c * v + s * w;

			Y[i]  = X[j] + u;
			X[j] -= u;
			Y[j]  = X[i] - v;
			X[i] += v;

			++i;
			--j;

			double b = ( beta * s + alpha * c ) - c;
			s += ( beta * c - alpha * s );

			v = Y[i];
			w = Y[j];
			u = w * b - v * s;
			v = w * s + v * b;

			Y[i]  = X[j] + u;
			X[j] -= u;
			Y[j]  = X[i] + v;
			X[i] -= v;

			c  = ( alpha * b - beta * s ) - b;
			s -= ( beta * b + alpha * s );

			++i;
			--j;
		}
	} // \bug else { }

}

template< typename T, std::size_t N >
std::array< T, N >& DCTIII(std::array< T, N >& x)
{
	DCTIII(x.data(), x.size() );
	return x;
}

#endif // DCT3_H
