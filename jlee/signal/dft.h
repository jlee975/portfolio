#ifndef DFT_H
#define DFT_H

#include <cstddef>

void DFT(double[][2], std::size_t N, double[][2]);
void IDFT(double[][2], std::size_t N, double[][2]);

#endif // DFT_H
