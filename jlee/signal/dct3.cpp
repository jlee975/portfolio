/*
   std::vector< double > naive_DCTIII(
    const double x[],
    std::size_t  n
   )
   {
    std::vector< double > y(n, 0.0);

    for ( std::size_t i = 0; i < n; ++i )
    {
        for ( std::size_t j = 0; j < n; ++j )
        {
            y[i] += x[j] * cos( ( pi / n ) * j * ( i + 0.5 ) );
        }
    }

    return y;
   }
 */
