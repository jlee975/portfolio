#include "imdct.h"

/*
   std::vector< double > naive_IMDCT(
    const double x[],
    std::size_t  n
   )
   {
    std::vector< double > y(2 * n, 0.0);

    for ( std::size_t i = 0; i < 2 * n; i++ )
    {
        for ( std::size_t j = 0; j < n; ++j )
        {
            y[i] += x[j] * cos( ( pi / n ) * ( i + 0.5 + 0.5 * n ) * ( j + 0.5 ) );
        }
    }

    return y;
   }
 */
