#ifndef IMDCT_H
#define IMDCT_H

#include "dct3.h"
#include <ccomplex>
#include <cmath>
#include <cstddef>

/***************************************************************************/ /**
 *  \fn          IMDCT
 *  \brief       Inverse Modified Discrete Cosine Transform
 *
 *  Calculates the Inverse Modified Discrete Cosine Transform of the input. The code
 *  is derived from the paper "Type-IV DCT, DST, and MDCT algorithms with reduced
 *  numbers of arithmetic operations</link>", Xuancheng Shao and Steve Johnson
 *  (except i have not implemented the scaling feature).
 *
 *  The algorithm works by breaking the IMDCT into a DCT-III of half the size and a
 *  DST-III of half the size. The final output is calculated by symmetry. Also, the
 *  DST-III is implemented as a DCT-III with the inputs reversed and every other
 *  output negated.
 *
 *  For Vorbis, this function accounts for nearly 50% of total decode time so it's
 *  clearly worth optimizing. Most of the time is spent in DCTIII so it is best to
 *  refer to the comments there.
 *
 *  y_{n} = \sum_{k=0}^{N-1} x_{k} \cos \left[
 *           \frac{\pi}{N} \left(n+\frac{1}{2} + \frac{N}{2} \right)
 *    \left( k + \frac{1}{2} \right) \right]
 *
 *  References ---------------------------------------------------------------------
 *
 *  Type-IV DCT, DST, and MDCT algorithms with reduced numbers of arithmetic
 *  operations, Xuancheng Shao and Steve Johnson,
 *  http://portal.acm.org/citation.cfm?id=1347459.1347681&amp;coll=GUIDE&amp;dl=&amp;CFID=15151515&amp;CFTOKEN=6184618
 *
 *******************************************************************************/
template< typename T >
void IMDCT(
	T*          X,
	std::size_t N // number of _outputs_ (aka size of array, twice _inputs_)
)
{
	const std::size_t N4 = N >> 2;
	const std::size_t N2 = N >> 1;

	X[N2] = X[0];

	for (std::size_t k = 1; k < N4; k++)
	{
		const double a = X[2 * k - 1];

		const double b = X[2 * k];
		X[N2 + k] = a + b;
		X[N - k]  = a - b;
	}

	X[N / 2 + N4] = X[N / 2 - 1];

	/// @todo: This move could be eliminated i'm sure, by careful scheduling
	DCTIII(X + N2, N4);

	for (std::size_t i = 0; i < N4; i++)
	{
		X[N4 + i] = X[N2 + i];
	}

	/// @todo: If N2 is even roll this into the code block below
	DCTIII(X + ( 3 * N4 ), N4);

	for (std::size_t i = 3 * N4 + 1; i < N; i += 2)
	{
		X[i] = -X[i];
	}

	// constants for recurrance of (2 * pi * n / N') (but N' == 4N)
	const T                 theta = ( 2.0 * std::numbers::pi_v< T > ) / ( 4.0 * static_cast< T >( N ) );
	std::complex< T >       trigs(std::cos(theta), std::sin(theta) );
	const std::complex< T > spin(2.0 * trigs.imag() * trigs.imag(), -2.0 * trigs.imag() * trigs.real() );

	// = (0.0, -2.0 * trigs.imag()) * trigs;

	// post-process (apply twiddles)
	// Basically sn = sin(2*pi*(i - N2 + 1) / (4N)) and cos similarly
	// So I need sin/cos tables up to 4N
	for (std::size_t i = N4; i < N2; i++)
	{
		std::complex< T > w(X[N2 + i], X[i]);

		w        *= trigs;
		X[i]      = w.real(), X[( N2 - 1 ) - i] = -w.real();
		X[N2 + i] = -w.imag(), X[( N - 1 ) - i] = -w.imag();

		trigs -= spin * trigs; // Recurrance relation for next set of twiddles
	}
}

#endif // IMDCT_H
