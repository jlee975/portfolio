/* PNG Decoder Class ===========================================================

   FIXME --------------------------------------------------------------------------

   - 4 byte unsigned integers have a limitation in PNG [-2^31 + 1, 2^31 - 1]
   - In general, the whole unsigned short/char/int casts are a nuisance

   ==============================================================================*/
#ifndef NDEBUG
#include <iostream>
#endif

#include <climits>
#include <cstddef>
#include <fstream>
#include <stdexcept>
#include <vector>

#include "crc/crc.h"
#include "endian/endian.h"
#include "memorymap/mmap.h"
#include "png.hpp"

constexpr std::byte paeth(
	std::byte a_,
	std::byte b_,
	std::byte c_
)
{
	const auto a = std::to_integer< unsigned >(a_);
	const auto b = std::to_integer< unsigned >(b_);
	const auto c = std::to_integer< unsigned >(c_);

	const unsigned pa = ( b > c ? b - c : c - b );
	const unsigned pb = ( a > c ? a - c : c - a );
	const unsigned pc = ( ( ( a + b ) > ( c + c ) ) ? ( ( a + b ) - ( c + c ) ) : ( ( c + c ) - ( a + b ) ) );

	return ( ( pa <= pb ) && ( pa <= pc ) ) ? a_ : ( ( pb <= pc ) ? b_ : c_ );
}

// PNG constants ---------------------------------------------------------------
// A PNG stream begins with the hex values 89,50,4E,47,0D,0A,1A,0A
constexpr std::uint_least64_t PNG_SIG = UINT64_C(0x89504E470D0A1A0A);

/* PNG::PNG --------------------------------------------------------------------

   Default constructor

   ------------------------------------------------------------------------------*/
PNG::PNG() = default;

/* PNG::Transform --------------------------------------------------------------
   TODO: Handle Adam7
   FIXME: All IDATs must appear consecutively
   ------------------------------------------------------------------------------*/
PNG::error PNG::process_chunks(
	const std::byte* pinfo,
	std::size_t      ninfo
)
{
	if ( ( pinfo == nullptr ) || ninfo < 8 || beread< 64 >(pinfo + 0) != PNG_SIG )
	{
		return error::SIGNATURE;
	}

	// Process chunks ------------------------------------------------------
	std::size_t si = 8; // index into source

	zl.reset(); // Holds idat chunks which form a zlib object

	bool iend = false; // false until IEND chunk is successfully handled

	while ( !iend ) // Process chunks until IEND is encountered
	{
		if ( ( si + 3 ) >= ninfo )
		{
			return error::EOS;
		}

		const std::uint_least32_t cbData = beread< 32 >(pinfo + si);

		if ( ( si + 12 + cbData ) > ninfo )
		{
			return error::INCOMPLETE_CHUNK;
		}

		// Checksum
		const std::uint_least32_t checksum
		    = chest::crc32(pinfo + ( si + 4 ), cbData + 4, chest::crc::png, chest::crc::flags::png);

		if ( checksum != beread< 32 >(pinfo + si + 8 + cbData) )
		{
			return error::CRC;
		}

		const std::uint_least32_t chunk = beread< 32 >(pinfo + si + 4);

		switch ( chunk ) // Handle known chunks
		{
		case IHDR:
		{
			const auto res = ProcessIHDR(pinfo, ninfo, si);

			if ( res != error::NONE )
			{
				return res;
			}
		}
		break;
		case IEND:
			iend = true;
			break;
		case IDAT:
		{
			/// @todo technically zlib, and the first two bytes have meaning
			bs.append(pinfo + ( si + 8 ), cbData);

			zl.exec(bs);
		}
		break;
		default:

			if ( ( chunk & Ancillary ) == 0 )
			{
				return error::UNKNOWN_CHUNK;
			}

			if ( ( chunk & Reserved ) != 0 )
			{
				return error::RESERVED_CHUNK;
			}
		} // switch

		si += 12 + cbData;
	}

	return error::NONE;
}

pixel_storage PNG::transform2_inner(
	const std::vector< std::byte >& derp,
	std::size_t                     w,
	std::size_t                     h,
	colour_configuration            X,
	unsigned                        bpp
) const
{
	// bytes per line
	const std::size_t bpl = w * bpp;

	auto* im = new std::byte[w * h * bpp];

	// Process each scanline -----------------------------------------------
	/// @todo Could store straight to im instead of copying it later from curr
	const std::byte* prev = nullptr;
	std::size_t      k    = 0;

	for (std::size_t i = 0; i < h; i++)
	{
		std::byte*        curr        = im + i * bpl;
		const PNG::Filter filter_type = toFilter(derp[k++]);

		switch ( filter_type ) // TODO: default is an error
		{
		case PNG::NoFilter:

			for (std::size_t j = 0; j < bpl; j++)
			{
				curr[j] = derp[k + j];
			}

			break;
		case PNG::Sub:

			for (unsigned j = 0; j < bpp; ++j)
			{
				curr[j] = derp[k + j];
			}

			for (std::size_t j = bpp; j < bpl; j++)
			{
				curr[j] = static_cast< std::byte >( std::to_integer< unsigned >(curr[j - bpp])
				                                    + std::to_integer< unsigned >(derp[k + j]) );
			}

			break;
		case PNG::Up:

			for (std::size_t j = 0; j < bpl; j++)
			{
				curr[j] = static_cast< std::byte >( std::to_integer< unsigned >(curr[j])
				                                    + std::to_integer< unsigned >(derp[k + j]) );
			}

			break;
		case PNG::Average:

			if ( prev != nullptr )
			{
				for (unsigned j = 0; j < bpp; ++j)
				{
					curr[j] = static_cast< std::byte >( std::to_integer< unsigned >(prev[j]) / 2
					                                    + std::to_integer< unsigned >(derp[k + j]) );
				}

				for (std::size_t j = bpp; j < bpl; j++)
				{
					curr[j] = static_cast< std::byte >(
						( std::to_integer< unsigned >(prev[j])
						  + std::to_integer< unsigned >(curr[j - bpp]) )
						/ 2
						+ std::to_integer< unsigned >(derp[k + j]) );
				}
			}
			else
			{
				for (unsigned j = 0; j < bpp; ++j)
				{
					curr[j] = derp[k + j];
				}

				for (std::size_t j = bpp; j < bpl; j++)
				{
					curr[j] = static_cast< std::byte >( ( std::to_integer< unsigned >(curr[j - bpp]) )
					                                    / 2
					                                    + std::to_integer< unsigned >(derp[k + j]) );
				}
			}

			break;
		case PNG::Paeth:

			if ( prev != nullptr )
			{
				for (unsigned j = 0; j < bpp; ++j)
				{
					curr[j] = static_cast< std::byte >( std::to_integer< unsigned >(prev[j])
					                                    + std::to_integer< unsigned >(derp[k + j]) );
				}

				for (std::size_t j = bpp; j < bpl; j++)
				{
					/// @todo Poor initialization
					curr[j] = static_cast< std::byte >(
						std::to_integer< unsigned >( paeth(curr[j - bpp], prev[j], prev[j - bpp]) )
						+ std::to_integer< unsigned >(derp[k + j]) );
				}
			}
			else
			{
				for (unsigned j = 0; j < bpp; ++j)
				{
					curr[j] = derp[k + j];
				}

				for (std::size_t j = bpp; j < bpl; j++)
				{
					/// @todo Poor initialization
					curr[j] = static_cast< std::byte >(
						std::to_integer< unsigned >( paeth(curr[j - bpp], std::byte{ 0 }, std::byte{ 0 }) )
						+ std::to_integer< unsigned >(derp[k + j]) );
				}
			}

			break;
		default:
			break;
		} // switch

		k += bpl;

		prev = curr;
	}

	return pixel_storage(im, w, h, X);
}

/// \todo Handle the interlace, colour_type, and bitdepth from standard
/// \todo k is directly related to j
pixel_storage PNG::build_pixel_data() const
{
	if ( interlace == Adam7 )
	{
		throw std::logic_error("Adam7 not implemented");
	}

	const std::vector< std::byte >& derp = zl.view();

	if ( colour_type == TrueColour )
	{
		if ( bitdepth == 8 )
		{
			return transform2_helper< RGB8 >(derp, width, height);
		}

		return transform2_helper< RGB16 >(derp, width, height);
	}

	if ( colour_type == TrueColourAlpha )
	{
		if ( bitdepth == 8 )
		{
			return transform2_helper< RGBA8 >(derp, width, height);
		}

		return transform2_helper< RGBA16 >(derp, width, height);
	}

	throw std::logic_error("colour mode not supported");
}

/*
   unsigned long bpp, bpl;
   bpp = ((bitdepth >= 8) ? (static_cast<unsigned long>(bitdepth) >> 3) : 1UL);
   bpl = (width * bitdepth + 7) >> 3; // bytes per line

   if (bitdepth >= 8) { // byte-sized colour components
    switch (colour_type) {
        case GreyscaleAlpha:
            bpl *= 2, bpp *= 2; break;
        case TrueColour:
            bpl *= 3, bpp *= 3; break;
        case TrueColourAlpha:
            bpl *= 4, bpp *= 4; break;
        default: break;
    }
   }
   // */

/* PNG::ProcessIHDR ------------------------------------------------------------
   ------------------------------------------------------------------------------*/
PNG::error PNG::ProcessIHDR(
	const std::byte* pchunk,
	std::size_t      nchunk,
	std::size_t      i
)
{
	if ( ihdr )
	{
		return error::MULTIPLE_IHDR;
	}

	if ( i > nchunk || ( nchunk - i ) < 21 )
	{
		return error::INCOMPLETE_CHUNK;
	}

	if ( beread< 32 >(pchunk + i) != 13 )
	{
		return error::INCOMPLETE_CHUNK;
	}

	if ( beread< 32 >(pchunk + i + 4) != IHDR )
	{
		return error::CHUNK;
	}

	const auto width_    = beread< 32 >(pchunk + i + 8); // Read dimensions
	const auto height_   = beread< 32 >(pchunk + i + 12);
	const auto bitdepth_ = std::to_integer< unsigned >(pchunk[i + 16]); // bits per plane

	const ColourType        colour_type_ = toColourType(pchunk[i + 17]);
	const CompressionMethod cm_          = toCompressionMethod(pchunk[i + 18]);
	const FilterMethod      filter_      = toFilterMethod(pchunk[i + 19]);
	const Interlace         interlace_   = toInterlace(pchunk[i + 20]);

	// Bounds checking -----------------------------------------------------
	if ( width_ == 0 )
	{
		return error::WIDTH;
	}

	if ( height_ == 0 )
	{
		return error::HEIGHT;
	}

	if ( ( ( ( bitdepth_ - 1 ) & bitdepth_ ) != 0 ) || ( bitdepth_ > 16 ) )
	{
		return error::BIT_DEPTH;
	}

	if ( colour_type_ == InvalidColourType )
	{
		return error::COLOUR_TYPE;
	}

	if ( cm_ == InvalidCompression )
	{
		return error::COMPRESSION_METHOD;
	}

	if ( filter_ == InvalidFilterMethod )
	{
		return error::FILTER;
	}

	if ( interlace_ == InvalidInterlace )
	{
		return error::INTERLACE;
	}

	if ( ( ( bitdepth_ < 8 )
	       && ( ( colour_type_ == TrueColour ) || ( colour_type_ == TrueColourAlpha )
	            || ( colour_type_ == GreyscaleAlpha ) ) )
	     || ( bitdepth_ == 16 && colour_type_ == Indexed )
                                                           )
	{
		return error::COLOUR_TYPE_DEPTH;
	}

	// Save parameters
	/// \todo Should call underlying Image functions resize(), depth(), etc.
	width       = width_;
	height      = height_;
	bitdepth    = bitdepth_;
	colour_type = colour_type_;

	cm        = cm_;
	filter    = filter_;
	interlace = interlace_;

	ihdr = true;
	return error::NONE;
}

/* PNG::ProcessPLTE ------------------------------------------------------------
   ------------------------------------------------------------------------------*/
PNG::error PNG::ProcessPLTE(
	const std::byte* pChunk,
	unsigned long    nChunk
)
{
	if ( pChunk == nullptr )
	{
		return error::NULL_POINTER;
	}

	if ( ( nChunk % 3 ) != 0 ) // Chunk size must be divisible by 3
	{
		return error::PLTE_SIZE;
	}

	return error::NONE;
}

/* PNG::Open -------------------------------------------------------------------

   Provides a Chill object from a PNG file.

   ------------------------------------------------------------------------------*/
pixel_storage PNG::open(const std::filesystem::path& p)
{
	return open(p.c_str() );
}

pixel_storage PNG::open(const char* szfilename)
{
	PNG p;

	memory_map m(szfilename);

	if ( m.is_open() )
	{
		p.process_chunks(m.data(), m.size() );
	}
	else
	{
		std::ifstream            ifs(szfilename);
		char                     c;
		std::vector< std::byte > filedata;

		/// @todo Am I seriously reading byte-by-byte!?
		while ( ifs.get(c) )
		{
			filedata.push_back(static_cast< std::byte >( c ) );
		}

		ifs.close();

		p.process_chunks(filedata.data(), filedata.size() );
	}

	return p.build_pixel_data();
}

PNG::Filter PNG::toFilter(std::byte c)
{
	switch ( std::to_integer< unsigned char >(c) )
	{
	case NoFilter:
		return NoFilter;

	case Sub:
		return Sub;

	case Up:
		return Up;

	case Average:
		return Average;

	case Paeth:
		return Paeth;

	default:
		return InvalidFilter;
	}
}

PNG::CompressionMethod PNG::toCompressionMethod(std::byte c)
{
	return std::to_integer< unsigned char >(c) == PNG::Deflate ? PNG::Deflate : PNG::InvalidCompression;
}

PNG::ColourType PNG::toColourType(std::byte c)
{
	switch ( std::to_integer< unsigned char >(c) )
	{
	case Greyscale:
		return Greyscale;

	case TrueColour:
		return TrueColour;

	case Indexed:
		return Indexed;

	case GreyscaleAlpha:
		return GreyscaleAlpha;

	case TrueColourAlpha:
		return TrueColourAlpha;

	default:
		return InvalidColourType;
	}
}

PNG::Interlace PNG::toInterlace(std::byte c)
{
	switch ( std::to_integer< unsigned char >(c) )
	{
	case NoInterlace:
		return NoInterlace;

	case Adam7:
		return Adam7;

	default:
		return InvalidInterlace;
	}
}

PNG::FilterMethod PNG::toFilterMethod(std::byte c)
{
	return std::to_integer< unsigned char >(c) == Adaptive ? Adaptive : InvalidFilterMethod;
}
