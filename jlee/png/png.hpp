/* png.hpp =====================================================================
 *
 * The following header contains functions, macros, constants, and the the class
 * declaration for using PNG functions.
 *
 * Note that PNG is largely a big endian format, so the numerical constants
 * assume that. (This is largely ensured by using the BIGENDIAN32 macro
 */

#ifndef PNG_HPP
#define PNG_HPP

#include <filesystem>
#include <vector>

#include "chill/pixels.h"
#include "zlib/zlibdecoder.h"

// Error codes -----------------------------------------------------------------
// Class definition ------------------------------------------------------------
class PNG
{
public:
	enum ColourType
	{
		InvalidColourType = -1,
		Greyscale = 0,
		TrueColour = 2,
		Indexed = 3,
		GreyscaleAlpha = 4,
		TrueColourAlpha = 6
	};

	enum Filter
	{
		InvalidFilter = -1,
		NoFilter = 0,
		Sub = 1,
		Up = 2,
		Average = 3,
		Paeth = 4
	};

	enum Interlace
	{
		InvalidInterlace = -1,
		NoInterlace = 0,
		Adam7 = 1
	};
	enum class error
	{
		NONE,
		SIGNATURE,          // The PNG signature was not found
		INCOMPLETE_CHUNK,   // Not enough input data for a complete chunk
		WIDTH,              // Width was specified as zero
		HEIGHT,             // Height was specified as zero
		BIT_DEPTH,          // Disallowed bit depth specified
		COLOUR_TYPE,        // An invalid colour model was given
		COLOUR_TYPE_DEPTH,  // mismatch between colour model and bit depth
		COMPRESSION_METHOD, // The compression method is unknown/unsupported
		FILTER,             // The filter method is unknown/unsupported
		INTERLACE,          // The interlace method is unknown/unsupported
		UNKNOWN_CHUNK,      // A critical, but unknown, chunk was encountered
		RESERVED_CHUNK,     // A reserved chunk was encountered
		MULTIPLE_IHDR,      // Multiple copies of IHDR are not allowed
		DEFLATE,            // The deflated data is invalid
		EOS,                // Stream ended unexpectedly
		PLTE_SIZE,          // The size of PLTE was wrong
		NULL_POINTER,       // A null pointer was passed in
		CRC,                // A stored CRC did not match the calculated value
		CHUNK,              // Mismatch chunk w/ ProcessXXX
		UNSUPPORTED
	};

	PNG();

	static pixel_storage open(const std::filesystem::path&);
	static pixel_storage open(const char*);
private:
	enum Flags
	{
		Copy = 0x20,
		Reserved = 0x2000,
		Private = 0x200000,
		Ancillary = 0x20000000
	};

	enum FilterMethod
	{
		InvalidFilterMethod = -1,
		Adaptive = 0
	};

	enum CompressionMethod
	{
		InvalidCompression = -1,
		Deflate = 0
	};

	enum ChunkType
	{
		IDAT = UINT32_C(0x49444154),
		IEND = UINT32_C(0x49454E44),
		IHDR = UINT32_C(0x49484452),
		PLTE = UINT32_C(0x504C5445)
	};

	static Filter toFilter(std::byte);
	static ColourType toColourType(std::byte);
	static Interlace toInterlace(std::byte);
	static CompressionMethod toCompressionMethod(std::byte);
	static FilterMethod toFilterMethod(std::byte);

	error process_chunks(const std::byte*, std::size_t);
	pixel_storage build_pixel_data() const;

	template< colour_configuration X >
	pixel_storage transform2_helper(
		const std::vector< std::byte >& derp,
		std::size_t                     w,
		std::size_t                     h
	) const
	{
		return transform2_inner(derp, w, h, X,
			colour_configuration_traits< X >::num_channels
			* colour_configuration_traits< X >::bitdepth / 8);
	}

	pixel_storage transform2_inner(const std::vector< std::byte >& derp, std::size_t width, std::size_t height, colour_configuration X, unsigned bpp) const;

	error ProcessIHDR(const std::byte*, std::size_t, std::size_t);
	error ProcessPLTE(const std::byte*, unsigned long);

	std::uint_least32_t width{ 0 };
	std::uint_least32_t height{ 0 };
	unsigned            bitdepth{ 0 };
	ColourType          colour_type{ InvalidColourType };
	CompressionMethod   cm{ InvalidCompression };
	FilterMethod        filter{ InvalidFilterMethod };
	Interlace           interlace{ InvalidInterlace };

	bool ihdr{ false }; // true if IHDR chunk has been successfully processed
	bool plte{ false }; // true if PLTE chunk has been successfully processed
	bool idat{ false }; // true if (any) IDAT chunk has been successfully processed

	ILEBitstream bs;
	ZLibDecoder  zl;
};

#endif // #ifndef PNG_HPP
