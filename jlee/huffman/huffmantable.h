#ifndef HUFFMANTABLE_H
#define HUFFMANTABLE_H

#include <utility>
#include <vector>

#include "bitstream/bitstream.h"

/** @brief Represents a Huffman tree
 *
 * A quick note about how this works. The tree is not stored as a straight binary tree, but has a jump table
 * allowing quick handling of short codes. The jump table covers the first N bits, and if the code is short
 * enough, we will have the result right away. If it is longer, the jump table will point into the binary
 * tree, which will then be traversed in a bit-by-bit fashion.
 *
 * @todo Template on code type. Choose length_type appropriately (uint8_t should basically always work)
 * @todo Canonical Huffman codes are more efficient to store, and faster to decode
 * @todo Generating the codewords from lengths and arranging into the tree structure could be done in the same step.
 * @todo The acceleration (jump) table should be sized as appropriate for the tree.
 * @todo May want to accept underspecified or overspecified Huffman trees, so long as the code is never encountered
 */
class HuffmanTable
{
public:
	using bits = bits_;

	enum schema
	{
		canonical, ///< Shorter codes have smaller values. Ex., in deflate
		minimal    ///< Codes assigned, in order, lowest unused prefix. Ex., in vorbis
	};

	/** @brief Default constructor does not store a table
	 *
	 * @note valid() returns false.
	 */
	HuffmanTable();

	/** @brief Create Huffman table from array of lenths
	 *
	 * @param plengths Array of lengths of bit codes, for each value 0..n-1
	 * @param n Length of the array plengths
	 * @param s Method used to create the actual codes
	 *
	 * plengths cannot be nullptr. Must point to an array of size at least n. No entry may be larger than
	 * bits::maxlength. An entry of length 0 is permitted, meaning that value is not used.
	 *
	 * @todo template on length
	 */
	HuffmanTable(const bits::length_type* plengths, std::size_t n, schema s = canonical);

	/** @brief Check if this represents a Huffman tree
	 */
	bool valid() const;

	/** @brief Extract the Huffman code and return the value.
	 * @param w Value to parse for a huffman code
	 * @returns Value decoded, and number of bits of input consumed
	 *
	 * Bits are read beginning at the least significant bit. Thus, on return, the caller will want to shift
	 * right by the number of bits indicated (to discard the bits).
	 *
	 * On error, the number of bits extracted (second member of the returned pair) will be zero. An error can
	 * really only happen if there were not enough bits in the input.
	 *
	 * @todo A "fast" extract where we know we have all the bits
	 */
	std::pair< std::size_t, bits::length_type > extract(const bits& w) const;

	/** @brief Reset the table to an invalid state
	 */
	void clear();
private:
	/** @brief Construct table from provided codes
	 * @param codes A Huffman code for each value to encode.
	 * @param dummy Unused
	 *
	 * This version does not check the validity of the codes, as they will have been created by
	 * codes_from_schema.
	 *
	 * A code with a bit length of zero indicates that the value is not used.
	 */
	HuffmanTable(const std::vector< bits >& codes, int dummy);

	using index_type = std::size_t;

	/** @brief Checks the provided lengths array and creates codes if everything is ok
	 * @param plengths Array of lengths of bit codes, for each value 0..n-1
	 * @param n Length of the array plengths
	 * @param s Method used to create the actual codes
	 *
	 * Returns an empty vector if there's an error (ex., overspecified lengths, null pointer, bad schema)
	 */
	static std::vector< bits > codes_from_schema(const bits::length_type* plengths, std::size_t n, schema s);

	/** @brief Create Huffman codes from lengths using the canonical method
	 * @param plengths Array of lengths of bit codes, for each value 0..n-1
	 * @param n Length of the array plengths
	 *
	 * In the canonical method, codes are assigned values according to the following rules:
	 *    - codewords of the same lengths are consecutive
	 *    - codewords of smaller lengths are smaller in value
	 *    - earlier codewords are assigned smaller values
	 */
	static std::vector< bits > make_canonical_codes(const bits::length_type* plengths, std::size_t n);

	/** @brief Create Huffman codes from lengths using the minimal method
	 * @param plengths Array of lengths of bit codes, for each value 0..n-1
	 * @param n Length of the array plengths
	 *
	 * In the minimal method, codes are assigned values in order, always assigning the least available code
	 */
	static std::vector< bits > LengthsToMinimalCodes(const bits::length_type* plengths, std::size_t n);

	/// Holds the binary tree, minus the nodes covered by the acceleration table
	std::vector< index_type > ptable;

	/// Acceleration table records the result of trying to extract a word from 0..(2**n-1).
	std::vector< std::pair< index_type, bits::length_type > > paccel;

	/// Number of values that have Huffman codes in this table
	index_type number_of_codes{ 0 };
};

#endif // HUFFMANTABLE_H
