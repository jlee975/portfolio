#include "huffmantable.h"

#include <algorithm>
#include <stdexcept>

constexpr HuffmanTable::bits::length_type ACCEL_BITS = 6;
constexpr HuffmanTable::bits::word_type   ACCEL_MASK = 0x3F; /*((1UL << ACCEL_BITS) - 1UL)*/

HuffmanTable::HuffmanTable() = default;

/* Create the codes and forward to the other constructor
 */
HuffmanTable::HuffmanTable(
	const bits::length_type* plengths,
	std::size_t              n,
	schema                   s
)
	: HuffmanTable(codes_from_schema(plengths, n, s), 0)
{
}

/* Build the acceleration table and portion of the binary tree. The acceleration table handles the first
 * ACCEL_BITS of input. For short enough codes, this means we will simply index into the acceleration table
 * and be able to return immediately. When this isn't the case, the acceleration table will point into the
 * binary tree where we can continue traversing.
 *
 * Strictly speaking, we don't store the whole binary tree. We only store those values which aren't covered
 * by the acceleration table.
 *
 * In order to keep shorter codes closer to the front of the binary tree, the codes are processed in order
 * of length.
 */
HuffmanTable::HuffmanTable(
	const std::vector< bits >& pcodes,
	int
)
	: number_of_codes(0)
{
	bits::length_type max_codelen = 0;

	// create acceleration table
	std::vector< std::pair< index_type, bits::length_type > > pbleh(1 << ACCEL_BITS, { 0, 0 });

	for (std::size_t i = 0; i < pcodes.size(); i++)
	{
		if ( 0 < pcodes[i].length )
		{
			++number_of_codes;

			if ( pcodes[i].length > max_codelen )
			{
				max_codelen = pcodes[i].length;
			}

			if ( pcodes[i].length <= ACCEL_BITS )
			{
				// jump straight to value
				for (std::size_t j = 0; j < ( 1u << ( ACCEL_BITS - pcodes[i].length ) ); ++j)
				{
					// fill in all affected entries
					pbleh[( j << pcodes[i].length ) | pcodes[i].value] = { i, pcodes[i].length };
				}
			}
		}
	}

	// create binary tree for the rest of the codes
	std::vector< index_type > pCB(2 * number_of_codes, 0);
	index_type                freenode = 0;

	for (bits::length_type k = ACCEL_BITS + 1; k <= max_codelen; ++k)
	{
		for (std::size_t i = 0; i < pcodes.size(); i++)
		{
			if ( pcodes[i].length == k )
			{
				bits::word_type cw = pcodes[i].value;

				std::size_t currnode = pbleh[cw & ACCEL_MASK].first;

				if ( pbleh[cw & ACCEL_MASK].second == 0 )
				{
					pbleh[cw & ACCEL_MASK] = { freenode, ACCEL_BITS + 1 };
					currnode               = freenode;
					freenode              += 2;
				}

				cw >>= ACCEL_BITS;

				for (std::size_t j = ACCEL_BITS; j < pcodes[i].length - 1; ++j)
				{
					if ( ( cw & 1 ) != 0 )
					{
						++currnode;
					}

					if ( pCB[currnode] == 0 )
					{
						pCB[currnode] = freenode;
						currnode      = freenode;
						freenode     += 2;
					}
					else
					{
						currnode = pCB[currnode];
					}

					cw >>= 1;
				}

				pCB[currnode + cw] = 2 * number_of_codes + i;
			}
		}
	}

	pCB.resize(freenode);

	paccel = std::move(pbleh);
	ptable = std::move(pCB);
}

bool HuffmanTable::valid() const
{
	return number_of_codes != 0;
}

/* Extracting a code from input bits means first checking the acceleration table. For short codes, we'll be
 * able to return immediately. Otherwise, we jump into the binary tree and traverse it one bit at a time.
 */
std::pair< std::size_t, HuffmanTable::bits::length_type > HuffmanTable::extract(const bits& p) const
{
	const bits::length_type nb = p.length;
	// Initial read and number of bits consumed
	const auto& q = paccel[p.value & ACCEL_MASK];

	if ( q.second > p.length )
	{
		return { 0, 0 };
	}

	if ( q.second <= ACCEL_BITS )
	{
		return { q.first, q.second };
	}

	bits::word_type   cw = p.value >> ACCEL_BITS;
	bits::length_type cb = ACCEL_BITS;
	index_type        i  = q.first;

	// Bit by bit, read the last of the huffman code
	const index_type isymbols = 2 * number_of_codes;

	while ( i < isymbols ) // Follow the tree
	{
		if ( nb <= cb )
		{
			return { 0, 0 };
		}

		i = ptable[i + ( cw & 1 )];
		++cb;
		cw >>= 1;
	}

	return { i - isymbols, cb };
}

void HuffmanTable::clear()
{
	paccel.clear();
	ptable.clear();
	number_of_codes = 0;
}

/* Check the lengths provided and then call the appropriate code generator. Lengths are checked to see that
 * they create a valid Huffman tree, and that they are not too long (ex., longer than 64 bits)
 */
std::vector< HuffmanTable::bits > HuffmanTable::codes_from_schema(
	const bits::length_type* plengths,
	std::size_t              n,
	schema                   s
)
{
	if ( plengths != nullptr )
	{
		// Check the lengths provided. A binary tree must satisfy certain conditions
		std::size_t counts[bits::maxlength] = { 0 };

		for (std::size_t i = 0; i < n; ++i)
		{
			if ( plengths[i] > bits::maxlength )
			{
				return {};
			}

			if ( plengths[i] != 0 )
			{
				counts[plengths[i] - 1] += 1;
			}
		}

		// Check if there are too many, or too few codes for any valid length
		std::size_t rem = 2;

		for (unsigned long count : counts)
		{
			if ( count > rem )
			{
				return {};
			}

			rem -= count;
			rem *= 2;
		}

		// If the lengths are okay, assign codes
		if ( rem == 0 )
		{
			switch ( s )
			{
			case canonical:
				return make_canonical_codes(plengths, n);

			case minimal:
				return LengthsToMinimalCodes(plengths, n);

			default:
				break;
			}
		}
	}

	return {};
}

std::vector< HuffmanTable::bits > HuffmanTable::LengthsToMinimalCodes(
	const bits::length_type* plengths,
	std::size_t              n
)
{
	std::vector< bits > pCW(n);

	for (std::size_t j = 0; j < n; j++)
	{
		bits::word_type       code = 0; // start at the lowest code
		const bits::word_type mask = ~( bits::word_type(-1) << ( bits::maxlength - plengths[j] ) );

		bool found; // termination condition

		do // check to see if the codeword is taken
		{
			found = true;
			code &= ~mask;

			for (std::size_t k = 0; k < j; k++)
			{
				const bits::word_type p = pCW[k].value;

				if ( p != 0 && ( ( p & ( p - 1 ) ) <= code ) && ( code <= ( p | ( p - 1 ) ) ) )
				{
					// the current codeword is in use...
					found = false;
					code  = ( p ^ ( p - 1 ) ) >= mask ? ( p | ( p - 1 ) ) + 1 : ( code & ~mask ) + mask + 1;
				}
			}
		}
		while ( !found );

		pCW[j] = reverse_bits(bits{ code >> ( bits::maxlength - plengths[j] ), plengths[j] });
	}

	return pCW;
}

std::vector< HuffmanTable::bits > HuffmanTable::make_canonical_codes(
	const bits::length_type* plengths,
	std::size_t              n
)
{
	std::vector< bits > pCW(n);

	bits::word_type code = 0;

	// Run over each bit length
	for (bits::length_type i = 1; i < bits::maxlength; ++i)
	{
		// Assign smaller code words to earlier table entries
		for (std::size_t j = 0; j < n; ++j)
		{
			if ( plengths[j] == i )
			{
				pCW[j] = reverse_bits(bits{ code >> ( bits::maxlength - i ), i });
				code  += ( bits::word_type(1) << ( bits::maxlength - i ) );
			}
		}
	}

	return pCW;
}
