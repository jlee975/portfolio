/*=========================================================================*//**
*  \file        huffman.h
*  \author      Jonathan Lee <jonathan.lee.975@gmail.com>
*  \brief       Provides the Huffman template class
*
*  \note Requires huffman.cpp for static member functions that perform common
*     Huffman table constructions.
*
*  \todo Try to remove dependence on bitstream.h -- use an internal "codeword"
*     class instead of "codeword". Then overload <<
*  \todo Merge accel_table and tree
*  \todo Be more consistent with accepting std::vectors or arrays. It's a bit of both.
*  \todo Change lengths arrays to unsigned int
*  \bug  Ensure size 0 and size 1 tables are handled properly
*  \bug  Ensure symbols and codes arrays are the same length before merging into
*     index
*//*==========================================================================*/
#ifndef HUFFMAN_HPP
#define HUFFMAN_HPP

#ifndef NDEBUG
#include <iostream>
#endif

#include <algorithm>
#include <cstddef>
#include <map>
#include <stdexcept>
#include <utility>
#include <vector>

#include "bitstream/lebitstream.h"

/*=========================================================================*//**
*  \class       HuffmanTree
*
*  This class is simply a wrapper for functions that are common to all Huffman<T>
*  implemetations. Also, it provides the Schema enumerated type, which can be
*  used to construct particular kinds of Huffman<T> tables.
*//*==========================================================================*/
class HuffmanTree
{
public:
	struct codeword
	{
		unsigned long value;
		unsigned bits;
	};

	enum Schema
	{
		Any,
		Canonical,
		Minimal
	};

	virtual ~HuffmanTree() = default;
protected:
	static std::vector< codeword > makeCanonicalCodes(const unsigned char*, std::size_t);

	static std::vector< codeword > makeMinimalCodes(const unsigned char*, std::size_t);

	static std::vector< codeword > makeGreedyCodes(const unsigned long*, std::size_t);
};

/*=========================================================================*//**
*  \class       Huffman
*  \brief       Template class for en-/decoding symbols of type T
*
*  \bug  What if symbols has duplicates? Or psym?
*  \todo When bitstream.cpp has a more complete big endian class, we can
*     probably move away from the little endian requirement.
*//*==========================================================================*/
template< class T >
class Huffman
	: HuffmanTree
{
	typedef std::pair< T, codeword > valueT;

	std::vector< std::size_t > accel_table;
	unsigned                   accel_width;
	std::vector< std::size_t > tree;
	unsigned                   maxlen;

	std::vector< valueT > index;

	void buildTables(const std::vector< codeword >&, const T*);
	void createTree();

	static bool less(const valueT&, const valueT&);
public:
	Huffman(const Huffman&);
	Huffman(const unsigned long*, std::size_t, Schema           = Any, const T* = 0);
	explicit Huffman(const std::vector< codeword >&, const T*   = 0);
	Huffman(const unsigned char*, std::size_t, Schema, const T* = 0);

	~Huffman() override;

	Huffman& operator=(const Huffman&);
	void swap(Huffman&);

	codeword operator()(const T& s) const;
	T read(ILEBitstream& bs) const;

	bool has_symbol(T) const;
};

template< typename T >
bool Huffman< T >::less(
	const valueT& a,
	const valueT& b
)
{
	return a.first < b.first;
}

/***************************************************************************/ /**
 *  \fn          Huffman(const Huffman&)
 *  \brief       Copy constructor
 *******************************************************************************/
template< class T >
Huffman< T >::Huffman(const Huffman< T >& h)
	: accel_table(h.accel_table), accel_width(h.accel_width), tree(h.tree), maxlen(h.maxlen), index(h.index)
{
}

/***************************************************************************/ /**
 *  \fn          Huffman(std::vector<codeword>&, const T*)
 *  \brief       Construct from given codes, lengths, symbols
 *******************************************************************************/
template< class T >
Huffman< T >::Huffman(
	const std::vector< codeword >& codes,
	const T*                       psym // pointer to symbol table
)
	: accel_width(0), maxlen(0), index()
{
	buildTables(codes, psym);
}

/***************************************************************************/ /**
 *  \fn          Huffman(const unsigned char*, std::size_t, Schema, const T*)
 *  \brief       Construct from lengths, schema and match with symbols
 *******************************************************************************/
template< class T >
Huffman< T >::Huffman(
	const unsigned char* lengths,
	std::size_t          nl,
	Schema               tabletype,
	const T*             psym // pointer to symbol table
)
	: accel_width(0), maxlen(0), index()
{
	std::vector< codeword > codes;

	// Build "codes" using the appropriate heuristic
	switch ( tabletype )
	{
	case Canonical:
	case Any:
		codes = makeCanonicalCodes(lengths, nl);
		break;
	case Minimal:
		codes = makeMinimalCodes(lengths, nl);
		break;
	default:
		throw std::invalid_argument("Invalid table type");
	}

	buildTables(codes, psym);
}

/***************************************************************************/ /**
 *  \fn          Huffman(const unsigned long*, std::size_t, Schema, const T*)
 *  \brief       Construct from weights, schema and match with symbols
 *******************************************************************************/
template< class T >
Huffman< T >::Huffman(
	const unsigned long* weights,
	std::size_t          nw,
	Schema               tabletype,
	const T*             psym
)
	: accel_width(0), maxlen(0), index()
{
	std::vector< codeword > codes = makeGreedyCodes(weights, nw);

	// Use a specific heuristic?
	if ( tabletype != Any )
	{
		std::vector< unsigned char > lens;

		lens.reserve(codes.size() );

		for (const auto& code : codes)
		{
			lens.push_back(static_cast< unsigned char >( code.bits ) );
		}

		switch ( tabletype )
		{
		case Minimal:
			codes = makeMinimalCodes(&lens[0], lens.size() );
			break;
		case Canonical:
			codes = makeCanonicalCodes(&lens[0], lens.size() );
			break;
		default:
			break;
		}
	}

	buildTables(codes, psym);
}

/***************************************************************************/ /**
 *  \fn          ~Huffman()
 *  \brief       Destructor
 *******************************************************************************/
template< class T > Huffman< T >::~Huffman() = default;

/***************************************************************************/ /**
 *  \fn          Huffman operator=(const Huffman&)
 *  \brief       Assignment
 *******************************************************************************/
template< class T >
Huffman< T >& Huffman< T >::operator=(const Huffman< T >& h)
{
	if ( &h != this )
	{
		Huffman< T >(h).swap(*this);
	}

	return *this;
}

/***************************************************************************/ /**
 *  \fn          void swap(Huffman&)
 *  \brief       Swap contents
 *******************************************************************************/
template< class T >
void Huffman< T >::swap(Huffman< T >& h)
{
	if ( &h != this )
	{
		accel_table.swap(h.accel_table);
		std::swap(accel_width, h.accel_width);
		tree.swap(h.tree);
		std::swap(maxlen, h.maxlen);
		index.swap(h.index);
	}
}

/***************************************************************************/ /**
 *  \fn          void buildTables(const std::vector<codeword>&)
 *  \brief       Constructs tree, and accel_table. Sorts symbols and codes. Sets maxlen
 *******************************************************************************/
template< class T >
void Huffman< T >::buildTables(
	const std::vector< codeword >& codes,
	const T*                       psym
)
{
	// Combine symbols and codewords into "index"
	unsigned minlen = -1U;

	maxlen = 0;
	T s = T();

	for (std::size_t i = 0, n = codes.size(); i < n; ++i)
	{
		if ( unsigned k = codes[i].bits )
		{
			if ( k > maxlen )
			{
				maxlen = k;
			}

			if ( k < minlen )
			{
				minlen = k;
			}

			index.push_back(valueT(psym ? psym[i] : s, codes[i]) );
		}

		++s;
	}

	if ( index.empty() )
	{
		throw std::runtime_error("Zero size alphabet");
	}

	std::sort(index.begin(), index.end(), less);

	// Build the decoding tree and accompanying acceleration table
	createTree();

	std::vector< std::size_t >::size_type ntree = tree.size();

	if ( minlen < 6 )
	{
		minlen = 6;
	}

	accel_table.resize(1UL << minlen);

	for (std::size_t j = 0; ( j >> minlen ) == 0; ++j)
	{
		std::size_t i = 0;

		for (unsigned cb = 0; cb < minlen && i < ntree; ++cb)
		{
			i = tree[i + ( ( j >> cb ) & 1U )];
		}

		accel_table[j] = i;
	}

	accel_width = minlen;
}

/***************************************************************************/ /**
 *  \fn          T read(Bitstream&) const
 *  \brief       Read huffman coded word from stream and return the matching symbol
 *
 *  This code searches the binary tree associated with book for the appropriate
 *  symbol. See the description of CreateDecodeTable for a detailed description of
 *  the tree structure.
 *
 *  On a 64-bit system there are enough cachebits to cover the max bits about 85% of
 *  the time.
 *
 *  \todo We could save incrementing cb in the loop by storing the end result at
 *     index i
 *******************************************************************************/
template< class T >
T Huffman< T >::read(ILEBitstream& bs) const
{
	unsigned long cw = bs.peek().value;

	std::size_t i = accel_table[cw & ( ( 1UL << accel_width ) - 1 )];

	std::vector< std::size_t >::size_type ntree = tree.size();

	if ( i < ntree )
	{
		cw >>= accel_width;

		do
		{
			i    = tree[i + ( cw % 2 )];
			cw >>= 1;
		}
		while ( i < ntree );
	}

	const valueT& v = index[i - ntree];

	bs.discard(v.second.bits);
	return v.first;
}

/***************************************************************************/ /**
 *  \fn          codeword operator()(const T&) const
 *  \brief       Return codeword corresponding to the provided symbol
 *
 *  The codeword is bit reversed so that they appear in the correct order for
 *  little endian bitstreams.
 *
 *  \todo Write a big endian version, possibly using operator[], which is not
 *     reversed
 *******************************************************************************/
template< class T >
HuffmanTree::codeword Huffman< T >::operator()(const T& s) const
{
	if ( index.empty() )
	{
		throw std::runtime_error("Empty Huffman tree");
	}

	std::size_t count = index.size();
	std::size_t j     = 0;

	while ( count > 1 )
	{
		std::size_t step = count / 2;

		if ( s < index[j + step].first )
		{
			count = step;
		}
		else
		{
			j     += step;
			count -= step;
		}
	}

	if ( index[j].first != s )
	{
		throw std::runtime_error("Symbol not represented in tree");
	}

	return index[j].second;
}

/***************************************************************************/ /**
 *  \fn          bool has_symbol(T) const
 *  \brief       Checks if the given symbol has a huffman code
 *  \returns     true if the symbol has a code word
 *
 *  \todo Combine code with operator()
 *******************************************************************************/
template< class T >
bool Huffman< T >::has_symbol(T s) const
{
	if ( index.empty() )
	{
		return false;
	}

	std::size_t count = index.size();
	std::size_t j     = 0;

	while ( count > 1 )
	{
		std::size_t step = count / 2;

		if ( s < index[j + step].first )
		{
			count = step;
		}
		else
		{
			j     += step;
			count -= step;
		}
	}

	return index[j].first == s;
}

/***************************************************************************/ /**
 *  \fn          std::vector<std::size_t> createTree(const std::vector<codeword>&)
 *  \brief       Builds the decoder-tree structure from the "codes" array
 *
 *  \todo Skipping a few words at the beginning (freenode = 4, to begin)
 *  \todo It should be possible to move bitreverse out of the other functions and
 *     into this one.
 *  \bug  This doesn't actually check that codes is well defined
 *******************************************************************************/
template< typename T >
void Huffman< T >::createTree()
{
	using sizeT = typename std::vector< valueT >::size_type;

	sizeT ncodes = index.size();

	std::vector< std::size_t > t1(2 * ncodes, 0);

	// Add each symbol, code pair to the tree
	std::size_t freenode = 2;

	for (sizeT i = 0; i < ncodes; ++i)
	{
		const unsigned long cw = index[i].second.value;
		const unsigned      k  = index[i].second.bits;

		std::size_t currnode = 0;

		for (unsigned j = k - 1; j > 0; --j)
		{
			if ( ( ( cw >> j ) % 2 ) != 0 )
			{
				++currnode;
			}

			if ( t1[currnode] == 0 )
			{
				t1[currnode] = freenode;
				currnode     = freenode;
				freenode    += 2;
			}
			else
			{
				currnode = t1[currnode];
			}
		}

		t1[currnode + ( cw % 2 )] = 2 * ncodes + i;
	}

	tree.swap(t1);
}

#endif // #ifndef HUFFMAN_HPP
