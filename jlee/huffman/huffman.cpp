/*=========================================================================*//**
*  \file        huffman.cpp
*  \author      Jonathan Lee <jonathan.lee.975@gmail.com>
*  \brief       Provides the common functions for Huffman<T>
*
*  Much of the functionality of Huffman trees can be implemented without reference
*  to the type of symbol being coded. This class encapsulates that functionality.
*
*  \todo In general there is no checking if a length is longer than longbits
*
*//*==========================================================================*/
#include <algorithm>
#include <limits>

#include "huffman.h"

/*=========================================================================*//**
*  \class       Node
*  \brief       Just a struct used by HuffmanTree::makeGreedyCodes()
*
*  If Node::left is not zero, then the Node represents a branch in the binary tree
*  with left and right children stored in their respective fields. The data field
*  is set to the sum of the weights of the left and right sub trees.
*
*  If Node::left is zero, then the node is a leaf and Node::right is used to hold
*  the position in c corresponding to the leaf's value. This is so the order can
*  be recovered using sort(). Also, the data field is set to the weight of the
*  respective symbol.
*//*==========================================================================*/
struct Node // POD used by greedy algorithm
{
	std::size_t left;   // left child, or 0 if a leaf
	std::size_t right;  // right child, or extra data if a leaf
	unsigned long data; // user data
	unsigned depth;     // depth within tree

	bool operator<(const Node& b) const
	{
		return data < b.data;
	}

	bool operator<=(const Node& b) const
	{
		return data <= b.data;
	}

	bool is_leaf() const
	{
		return left == 0;
	}

};

namespace
{
/***************************************************************************/ /**
 *  \fn          DepthFirstSearch(std::vector<Node>&, std::size_t, unsigned long, unsigned)
 *  \brief       Traverses the Node-s and fills in depth and position information
 *
 *******************************************************************************/
void DepthFirstSearch(
	std::vector< Node >& f,
	std::size_t          i,
	unsigned long        pos,
	unsigned             depth
)
{
	if ( i < f.size() )
	{
		if ( !f[i].is_leaf() )
		{
			DepthFirstSearch(f, f[i].left, 2 * pos, depth + 1);
			DepthFirstSearch(f, f[i].right, 2 * pos + 1, depth + 1);
		}
		else
		{
			f[i].data  = pos;
			f[i].depth = depth;
		}
	}
}

} // end anonymous namespace

/*=========================================================================*//**
*  \class       HuffmanTree
*//*==========================================================================*/

/***************************************************************************/ /**
 *  \fn          std::vector<codeword> makeGreedyCodes(const unsigned long*, std::size_t)
 *  \brief       Builds a set of codewords from weights, using the greedy algorithm
 *
 *  The greedy algorithm does the usual huffman construction. The weights
 *  are sorted largest to smallest. Then we iterate over the following rules
 *  1) pull off the least two weights
 *  2) Create a subtree (Node) from these two, heavier weight on the left
 *  3) Add the Node back into the list of sorted weights
 *
 *  Once the tree is constructed in "freq", the depth of each leaf is recorded in
 *  its corresponding Node. This is done by traversing the tree depth first, filling
 *  in fields as they are encountered.
 *
 *  \todo Rename "codewords" since it is too close to codeword
 *******************************************************************************/
std::vector< HuffmanTree::codeword > HuffmanTree::makeGreedyCodes(
	const unsigned long* weights,
	std::size_t          nw
)
{
	if ( ( weights == nullptr ) && nw != 0 )
	{
		throw std::invalid_argument("Null pointer");
	}

	// Build preliminary tree of leaves only -------------------------------
	std::vector< Node > freq;

	for (std::size_t i = 0, j = 0; i < nw; ++i)
	{
		if ( weights[i] != 0 )
		{
			Node t = { 0, j, weights[i], 0 };
			freq.push_back(t);
			++j;
		}
	}

	if ( freq.empty() )
	{
		throw std::runtime_error("Size zero alphabet.");
	}

	// Fill in the tree structure using greedy algorithm -------------------
	std::size_t n = freq.size();

	std::sort(freq.begin(), freq.end() );
	std::reverse(freq.begin(), freq.end() );

	freq.resize(n + ( n - 1 ) );

	for (std::size_t j = n; j > 1; --j)
	{
		freq[2 * j - 2] = freq[j - 1]; // Move two lightest nodes to end
		freq[2 * j - 3] = freq[j - 2];

		freq[j - 2].left  = 2 * j - 3; // Add in merged node
		freq[j - 2].right = 2 * j - 2;
		freq[j - 2].data  = freq[2 * j - 3].data + freq[2 * j - 2].data;

		// Move the node past as many leaves as possible
		for (std::size_t i = j - 2; i > 0 && freq[i - 1] <= freq[i]; --i)
		{
			std::swap(freq[i - 1], freq[i]);
		}
	}

	// Assign codes and bitlengths
	DepthFirstSearch(freq, 0, 0, 0);

	// Copy leaf data into "codes"
	std::vector< codeword > codewords(n);

	for (const auto& it : freq)
	{
		if ( it.is_leaf() )
		{
			codewords[it.right].value = it.data;
			codewords[it.right].bits  = it.depth;
		}
	}

	/// \todo This is a hack to re-insert the zero length codes
	std::vector< codeword > codes(nw);

	for (std::size_t i = 0, j = 0; i < nw; ++i)
	{
		if ( weights[i] != 0 )
		{
			codes[i] = codewords[j++];
		}
	}

	return codes;
}

/***************************************************************************/ /**
 *  \fn          std::vector<codeword> makeMinimalCodes(const unsigned char, std::size_t)
 *  \brief       Create codewords using the smallest value available
 *
 *  Create Huffman codewords from the table of lengths given, using the strategy
 *  of assigning the (numerically) smallest possible codeword.
 *
 *  This is the scheme used in Vorbis
 *
 *  \todo    Unnecessarily complicated
 *
 *  p & (p - 1)  -- the code itself without the 1 bit pad
 *  p | (p - 1)  -- max value of code prefixed word
 *  p ^ (p - 1)  -- all 1s where the code isn't
 *
 *******************************************************************************/
std::vector< HuffmanTree::codeword > HuffmanTree::makeMinimalCodes(
	const unsigned char* lengths,
	std::size_t          n
)
{
	if ( ( lengths == nullptr ) && n != 0 )
	{
		throw std::invalid_argument("Null pointer");
	}

	static const unsigned longbits = std::numeric_limits< unsigned long >::digits;

	std::vector< codeword > codewords;

	for (std::size_t j = 0; j < n; ++j)
	{
		if ( lengths[j] == 0 )
		{
			continue;
		}

		const unsigned c    = lengths[j];
		unsigned long  code = 0; // start at the lowest code
		unsigned long  mask = ~( ( ~0UL ) >> c );

		bool found; // termination condition

		do // check to see if the codeword is taken
		{
			found = true;
			code &= mask; // Truncate to bit length

			for (const auto& codeword : codewords)
			{
				unsigned long p;

				unsigned long q;

				p = ( ( codeword.value << 1 ) + 1 ) << ( ( longbits - 1 ) - codeword.bits );
				q = p | ( p - 1 );

				if ( ( p & ( p - 1 ) ) <= code && code <= q )
				{
					// the current codeword is in use...
					found = false;
					// Is this a shorter or longer codeword?
					code = ( ( p ^ ( p - 1 ) ) >= ~mask ? q // shorter or equal bit length
					                   : ( code | ~mask ) )
					       + 1UL; // longer
				}
			}
		}
		while ( !found );

		// Set LSb to mark the end of the codeword
		codeword temp = { code >> ( longbits - c ), c };
		codewords.push_back(temp);
	}

	/// \todo This is a hack to re-insert the zero length codes
	std::vector< codeword > codes(n);

	for (std::size_t i = 0, j = 0; i < n; ++i)
	{
		if ( lengths[i] != 0 )
		{
			codes[i] = codewords[j++];
		}
	}

	return codes;
}

/***************************************************************************/ /**
 *  \fn          std::vector<codeword> makeCanonicalCodes(const unsigned char*, std::size_t)
 *  \brief       Create codes using the canonical heuristic
 *
 *  Create Huffman codewords from the table of lengths given, using the canonical
 *  heuristic. That is:
 *
 * codewords of the same lengths are consecutive
 * codewords of smaller lengths are smaller in value
 * earlier codewords are assigned smaller values
 *
 *******************************************************************************/
std::vector< HuffmanTree::codeword > HuffmanTree::makeCanonicalCodes(
	const unsigned char* lengths,
	std::size_t          n
)
{
	if ( ( lengths == nullptr ) && n != 0 )
	{
		throw std::invalid_argument("Null pointer");
	}

	// Count the number of times each bit length appears
	unsigned long count[64] = { 0 };

	for (std::size_t i = 0; i < n; ++i)
	{
		if ( lengths[i] >= 64 )
		{
			throw std::runtime_error("Code length not supported.");
		}

		if ( lengths[i] != 0 )
		{
			++count[lengths[i]];
		}
	}

	// Calculate the minimum code word for each bit length
	unsigned long codemin = 0;

	for (unsigned i = 1; i < 64; ++i)
	{
		unsigned long b = count[i];
		count[i] = codemin;

		if ( b != 0 && codemin > ~( ~0UL << i ) )
		{
			throw std::runtime_error("Impossible to construct Canonical table");
		}

		codemin  += b;
		codemin <<= 1;
	}

	// Assign each code the correct value
	std::vector< codeword > codewords(n);

	for (std::size_t i = 0; i < n; ++i)
	{
		if ( unsigned k = lengths[i] )
		{
			codeword temp = { count[k]++, k };
			codewords[i] = temp;
		}
	}

	return codewords;
}

/* -----------------------------------------------------------------------------
 *  Possible new binary tree structure
 *
 *  Store left subtrees immediately after parent node. Parent contains size of left
 *  subtree.
 *
 *     z
 *  a-----+-----y
 |
 |       x---+---d
 |
 |     b-+-c
 |
 |          z a y x b c d
 |  i       0 1 2 3 4 5 6
 |  tree[i]    1 0 3 1 0 0 0
 |
 |  To traverse the tree:
 |
 |       input: cw
 |       cb = 0;
 |       left = 0;
 |       right = ntree - 1; // In the above example, 6
 |       while (left < right) {
 |               i = left + tree[left];
 |               if (cw % 2 == 0) {
 |                       right = i;
 |               } else left = i;
 ||||++left;
 |               cw >>= 1, ++cb; // eat a bit
 |       }
 |
 |  or
 |
 |       left = 0;
 |       right = ntree;
 |       while (right != 0) { // could use do..while if ntree guaranteed not 0
 |               i = tree[left] + 1;  // store the +1
 |               if (cw % 2 == 0) {
 |                       right = i - 1;
 ||||++left;
 |               } else {
 |                       left += i;
 |                       right -= i;
 |               }
 |               cw >>= 1, ++cb;
 |       }
 |
 |  We may be able to get away with storing only the nodes, and not the leaves.
 |  By counting the number of sizes of left subtrees skipped,  we can infer the
 |  position of the leaf. This may be of great benefit if the structure of a leaf
 |  is not the same as a node (i.e., an unsigned integral value). The leaf could
 |  be stored elsewhere.
 |
 |         z y x
 |  i      0 1 2
 |  tree[i]   0 1 0
 |
 |       left = 0;   // top node of current subtree
 |       j = size;   // size of current subtree
 |       k = 0;      // position of leaf
 |       while (j != 0) {
 |               i = tree[left] + 1;
 |               if (cw % 2 == 0) { // left subtree
 |                       j = i - 1;
 ||||++left;
 |               } else {
 |                       left += i, j -= i, k += i;
 |               }
 |               cw >>= 1;
 |       }
 |
 |  Every time we take a right subtree, we skip the left subtree. If the left
 |  subtree had n nodes, then it had n + 1 leaves, thus, we skipped n + 1 leaves.
 |  The leaf we arrive at, then, is the sum of this skips, which is accumulated
 |  in j.
 |
 |  Furthermore, if we store the bitsize in the leaf array, we don't need to
 |  track cb in the loop.
 |
 |  ------------------------------------------------------------------------------*/
