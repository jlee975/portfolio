/*=========================================================================*//**
   \file        settings.cpp

   Eventually this will be a platform independent^ way of storing and accessing
   settings. In Linux, the usual ~/.appname will be used to store settings. On
   Windows, the registry.

   ^By "platform independent" I basically mean Linux and Windows :/

*//*==========================================================================*/

#include <cstdlib>
#include <iostream>
#include <stdexcept>

#if defined( unix ) || defined( __unix ) || defined( __unix__ )
#include <cerrno>
#include <sys/stat.h> // mkdir
#include <unistd.h>
#endif

#include "settings.h"

namespace
{
bool make_dir(const std::string&);
}

/***************************************************************************/ /**
   \bug Watch for injection in name. Enforce some rules about formation
 *******************************************************************************/
Settings::Settings(const std::string& s)
	: name(s), appdir_(getappdir(s) )
{
	make_dir(appdir_);
}

/***************************************************************************/ /**
   \todo Validate "name"
 *******************************************************************************/
variant_t Settings::operator[](const std::string& name_) const
{
	variant_t x;

	auto it = values.find(name_);

	if ( it != values.end() )
	{
		x = it->second;
	}

	return x;
}

void Settings::insert(
	const std::string& name_,
	variant_t          val
)
{
	std::cout << "Settings[" << name_ << "] = ";

	if ( std::holds_alternative< std::string >(val) )
	{
		std::cout << std::get< std::string >(val);
	}

	std::cout << std::endl;
	values[name_] = val;
}

std::string Settings::appdir() const
{
	return appdir_;
}

/***************************************************************************/ /**
   \bug Make this safe
   \todo Windows: SHGetFolderPath w/ CSIDL_APPDATA
   \todo Possibly move to anonymous namespace
 *******************************************************************************/
std::string Settings::getappdir(const std::string& appname)
{
	std::string s;

	#if ( defined( _POSIX_VERSION ) && ( _POSIX_VERSION >= 200112L ) )

	if ( char* d = getenv("HOME") )
	{
		s  = d;
		s += "/." + appname;
	}

	#else
	#error "OS not supported"
	#endif
	return s;
}

namespace
{
bool make_dir(const std::string& dir)
{
	if ( dir.empty() )
	{
		return true;
	}

	#if ( defined( _POSIX_VERSION ) && ( _POSIX_VERSION >= 200112L ) )
	struct stat buffer;

	int err = stat(dir.c_str(), &buffer);

	if ( err == 0 ) // stat was successful -> directory already exists
	{
		return true;
	}

	// stat failed
	if ( errno == ENOENT )
	{
		// ensure parent directory exists
		std::size_t i = dir.find_last_of('/');

		if ( i == std::string::npos )
		{
			if ( !make_dir(dir.substr(0, i) ) )
			{
				return false;
			}
		}

		// try to create the dir
		err = mkdir(dir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
		return err == 0;
	}

	return false; // Unexpected error

	#else
	#error "OS not supported"
	#endif // if ( defined( _POSIX_VERSION ) && ( _POSIX_VERSION >= 200112L ) )
}

}
