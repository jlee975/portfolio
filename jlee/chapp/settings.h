#ifndef SETTINGS_HPP
#define SETTINGS_HPP

#include <map>
#include <string>
#include <variant>

using variant_t = std::variant< std::monostate, long, double, std::string >;

class Settings
{
	typedef std::map< std::string, variant_t > value_map;
	value_map   values;
	std::string name;
	std::string appdir_;

	Settings(const Settings&)            = delete;
	Settings& operator=(const Settings&) = delete;

	static std::string getappdir(const std::string&);
public:
	explicit Settings(const std::string&);

	variant_t operator[](const std::string&) const;
	void insert(const std::string&, variant_t);
	std::string appdir() const;
};

#endif // SETTINGS_HPP
