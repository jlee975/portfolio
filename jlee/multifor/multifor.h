#ifndef MULTIFOR_H
#define MULTIFOR_H

#include <tuple>
#include <utility>

template< typename...X >
class multifor
{
public:
	class const_iterator
	{
public:
		const_iterator(
			std::tuple< X... > curr_,
			std::tuple< X... > last_
		)
			: curr(std::move(curr_) ), last(std::move(last_) )
		{
		}

		const std::tuple< X... >& operator*() const
		{
			return curr;
		}

		bool operator==(const const_iterator& o) const
		{
			return curr == o.curr;
		}

		bool operator!=(const const_iterator& o) const
		{
			return curr != o.curr;
		}

		const_iterator& operator++()
		{
			do_something< sizeof...( X ) >();
			return *this;
		}
private:
		template< std::size_t N >
		void do_something()
		{
			if constexpr ( N != 0 )
			{
				if ( ++std::get< N - 1 >(curr) == std::get< N - 1 >(last) )
				{
					do_something< N - 1 >();
				}
				else
				{
					reset< N >();
				}
			}
		}

		template< std::size_t N >
		void reset()
		{
			if constexpr ( N < sizeof...( X ) )
			{
				using T             = typename std::tuple_element< N - 1, std::tuple< X... > >::type;
				std::get< N >(curr) = T();
			}
		}

		std::tuple< X... > curr;
		std::tuple< X... > last;
	};

	multifor()
		: last()
	{
	}

	explicit multifor(X&&... x)
		: last(std::forward< X >(x)...)
	{
	}

	const_iterator begin() const
	{
		return const_iterator(std::tuple< X... >(), last);
	}

	const_iterator end() const
	{
		return const_iterator(last, last);
	}
private:
	std::tuple< X... > last;
};

template< typename...X >
multifor< X... > make_multifor(X&&... x)
{
	return multifor< X... >(std::forward< X >(x)...);
}

#endif // MULTIFOR_H
