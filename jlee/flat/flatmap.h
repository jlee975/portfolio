#ifndef FLATMAP_H
#define FLATMAP_H

#include <algorithm>
#include <stdexcept>
#include <utility>
#include <vector>

#include "containers/iterator_wrapper.h"

/// @todo Obviously, back with vector
template< typename K, typename V >
class flat_map
{
public:
	typedef iterator_wrapper< typename std::vector< std::pair< K, V > >::iterator, flat_map > iterator;
	typedef iterator_wrapper< typename std::vector< std::pair< K, V > >::const_iterator, flat_map > const_iterator;

	const_iterator find(const K& k) const
	{
		auto last = m.end();
		auto it   = std::lower_bound(m.begin(), last, k, compare);

		if ( it == last || k < it->first )
		{
			return const_iterator(last);
		}

		return const_iterator(it);
	}

	const_iterator begin() const
	{
		return const_iterator(m.begin() );
	}

	const_iterator end() const
	{
		return const_iterator(m.end() );
	}

	V& operator[](const K& k)
	{
		auto last = m.end();
		auto it   = std::lower_bound(m.begin(), last, k, compare);

		if ( it == last || k < it->first )
		{
			it = m.emplace(it, k, V{});
		}

		return it->second;
	}
private:
	static bool compare(
		const std::pair< K, V >& p,
		const K&                 k
	)
	{
		return p.first < k;
	}

	std::vector< std::pair< K, V > > m;
};

#endif // FLATMAP_H
