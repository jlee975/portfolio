#ifndef UNIT_H
#define UNIT_H

#include <iostream>

template< typename A >
void assert_true_(
	A           actual,
	const char* actual_text
)
{
	if ( !actual )
	{
		std::cerr << "Values is not true\n";
		std::cerr << "\tActual   " << actual << " (" << actual_text << ")\n";
	}
}

#define assert_true(A) assert_true_(A, # A)

template< typename A >
void assert_false_(
	A           actual,
	const char* actual_text
)
{
	if ( actual )
	{
		std::cerr << "Values is not false\n";
		std::cerr << "\tActual   " << actual << " (" << actual_text << ")\n";
	}
}

#define assert_false(A) assert_false_(A, # A)

template< typename A, typename E >
void assert_equal_(
	A           actual,
	E           expected,
	const char* actual_text,
	const char* expected_text
)
{
	if ( !( actual == expected ) || ( actual != expected ) )
	{
		std::cerr << "Values are not equal\n";
		std::cerr << "\tActual " << actual_text << "\n";
		std::cerr << "\tExpected " << expected_text << "\n";
	}
}

#define assert_equal(A, E) assert_equal_(A, E, # A, # E)

template< typename A, typename E >
void assert_not_equal_(
	A           actual,
	E           expected,
	const char* actual_text,
	const char* expected_text
)
{
	if ( !( actual != expected ) || ( actual == expected ) )
	{
		std::cerr << "Values are equal\n";
		std::cerr << "\tActual   " << actual << " (" << actual_text << ")\n";
		std::cerr << "\tExpected " << expected << " (" << expected_text << ")\n";
	}
}

#define assert_not_equal(A, E) assert_not_equal_(A, E, # A, # E)

template< typename A, typename E >
void assert_less_than_(
	A           actual,
	E           expected,
	const char* actual_text,
	const char* expected_text
)
{
	if ( !( actual < expected ) || ( actual > expected ) )
	{
		std::cerr << "Actual is not less than expected";
		std::cerr << "\tActual   " << actual << " (" << actual_text << ")\n";
		std::cerr << "\tExpected " << expected << " (" << expected_text << ")\n";
	}
}

#define assert_less_than(A, E) assert_less_than_(A, E, # A, # E)

template< typename A, typename E >
void assert_greater_than_(
	A           actual,
	E           expected,
	const char* actual_text,
	const char* expected_text
)
{
	if ( !( actual > expected ) || ( actual < expected ) )
	{
		std::cerr << "Actual is not greater than expected";
		std::cerr << "\tActual   " << actual << " (" << actual_text << ")\n";
		std::cerr << "\tExpected " << expected << " (" << expected_text << ")\n";
	}
}

#define assert_greater_than(A, E) assert_greater_than_(A, E, # A, # E)

template< typename A, typename E >
void assert_less_than_equal_(
	A           actual,
	E           expected,
	const char* actual_text,
	const char* expected_text
)
{
	if ( !( actual <= expected ) || ( actual > expected ) )
	{
		std::cerr << "Actual is not less or equal to expected";
		std::cerr << "\tActual   " << actual << " (" << actual_text << ")\n";
		std::cerr << "\tExpected " << expected << " (" << expected_text << ")\n";
	}
}

#define assert_less_than_equal(A, E) assert_less_than_equal_(A, E, # A, # E)

template< typename A, typename E >
void assert_greater_than_equal_(
	A           actual,
	E           expected,
	const char* actual_text,
	const char* expected_text
)
{
	if ( !( actual >= expected ) || ( actual < expected ) )
	{
		std::cerr << "Actual is not greater or equal to expected";
		std::cerr << "\tActual   " << actual << " (" << actual_text << ")\n";
		std::cerr << "\tExpected " << expected << " (" << expected_text << ")\n";
	}
}

#define assert_greater_than_equal(A, E) assert_greater_than_equal_(A, E, # A, # E)

template< typename S, typename F >
void assert_all_true_(
	S&          seq,
	F           f,
	const char* f_text
)
{
	for (auto&& x : seq)
	{
		if ( !f(x) )
		{
			std::cerr << "Test was not successful\n";
			std::cerr << "\t" << f_text << "\n";
		}
	}
}

#define assert_all_true(S, F) assert_all_true_(S, F, # F)

template< typename S, typename F >
void assert_all_false_(
	S&          seq,
	F           f,
	const char* f_text
)
{
	for (auto&& x : seq)
	{
		if ( f(x) )
		{
			std::cerr << "Test did not fail\n";
			std::cerr << x << "\n";
			std::cerr << "\t" << f_text << "\n";
		}
	}
}

#define assert_all_false(S, F) assert_all_false_(S, F, # F)

template< typename T1, typename T2, typename E >
void assert_max_error_(
	const T1&   l,
	const T2&   r,
	E           max_error,
	const char* actual_text,
	const char* expected_text
)
{
	E worst_error = 0;

	auto first1 = std::begin(l);
	auto last1  = std::end(l);
	auto first2 = std::begin(r);
	auto last2  = std::end(r);

	while ( first1 != last1 && first2 != last2 )
	{
		const E e = ( *first1 < *first2 ? ( *first2 - *first1 ) : ( *first1 - *first2 ) );

		if ( e > worst_error )
		{
			worst_error = e;
		}

		++first1;
		++first2;
	}

	while ( first1 != last1 )
	{
		const E e = *first1 < 0 ? -*first1 : *first1;

		if ( e > worst_error )
		{
			worst_error = e;
		}

		++first1;
	}

	while ( first2 != last2 )
	{
		const E e = 0 < *first2 ? *first2 : -*first2;

		if ( e > worst_error )
		{
			worst_error = e;
		}

		++first2;
	}

	assert_less_than_equal_(worst_error, max_error, actual_text, expected_text);
}

#define assert_max_error(L, R, E) assert_max_error_(L, R, E, # L, # R)

#endif // UNIT_H
