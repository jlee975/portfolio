#ifndef INDEX_TYPE_H
#define INDEX_TYPE_H

#include <functional>
#include <iosfwd>

#include "strong_typedef.h"

template< typename U, typename = void >
class index_type
	: public strong_typedef< U, op_eq, op_ne, op_inc >
{
public:
	using strong_typedef< U, op_eq, op_ne, op_inc >::strong_typedef;
};

template< typename U, typename X >
std::ostream& operator<<(
	std::ostream&             os,
	const index_type< U, X >& i
)
{
	return os << i.get();
}

namespace std
{
template< typename U, typename X >
struct hash< index_type< U, X > >
{
	std::size_t operator()(const index_type< U, X >& i) const noexcept
	{
		hash< U > h;

		return h(i.get() );
	}

};
}

#endif // INDEX_TYPE_H
