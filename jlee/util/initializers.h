#ifndef INITIALIZERS_H
#define INITIALIZERS_H

/// @todo Maybe an integer_initialized_type { integer_initialized_value_0, integer_initialized_value_last }
/// That uses templated variables to initialized like integer_initialized<3>

// for when the object should do as little initialization as possible, for performance
enum class uninitialized_type
{
	uninitialized_value
};

constexpr uninitialized_type uninitialized = uninitialized_type::uninitialized_value;

// for when the object should be initialized with a "zero" state
enum class zero_initialized_type
{
	zero_initialized_value
};

constexpr zero_initialized_type zero_initialized = zero_initialized_type::zero_initialized_value;

// for when the object should be initialized with its default state
enum class default_initialized_type
{
	default_initialized_value
};

constexpr default_initialized_type default_initialized = default_initialized_type::default_initialized_value;

// for when the object should be initialized as an invalid value
enum class invalid_initialized_type
{
	invalid_initialized_value
};

constexpr invalid_initialized_type invalid_initialized = invalid_initialized_type::invalid_initialized_value;

#endif // INITIALIZERS_H
