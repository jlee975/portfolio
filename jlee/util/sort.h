#ifndef SORT_HPP
#define SORT_HPP

#include <algorithm>

/* sort ------------------------------------------------------------------------

   This sort is implemented with CombSort11. It is not a totally robust
   implementation, but it works fine. In particular, there is a bunch of talk
   about what the appropriate shrink factor for the gap should be. Usually
   this factor is 1.3, but there is a suggestion that 1.24something is
   better. Here, 1.3 is used (actually 256/197 = 1.2995.. so i can take
   advantage of the right shift).

   Sorts ascending

   ------------------------------------------------------------------------------*/
template< typename T >
void sort(
	T             arr[],
	unsigned long n
)
{
	unsigned long gap   = n;
	bool          swaps = true;

	do
	{
		if ( gap > 1 )
		{
			// approx division by 1.3
			gap = ( gap * 197 ) >> 8;

			if ( gap == 10 || gap == 9 )
			{
				gap = 11;
			}
		}

		swaps = false;

		for (unsigned long j = 0; ( j + gap ) < n; j++)
		{
			if ( arr[j] > arr[j + gap] )
			{
				std::swap(arr[j], arr[j + gap]);
				swaps = true;
			}
		}
	}
	while ( swaps || ( gap > 1 ) );
}

#endif // #ifndef SORT_HPP
