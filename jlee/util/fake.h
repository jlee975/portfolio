#ifndef FAKE_H
#define FAKE_H

template< typename U >
struct fake_uint
{
	fake_uint() = default;

	explicit fake_uint(U x)
		: value(x)
	{
	}

	fake_uint& operator|=(const fake_uint& r)
	{
		value |= r.value;
		return *this;
	}

	fake_uint& operator&=(const fake_uint& r)
	{
		value &= r.value;
		return *this;
	}

	fake_uint& operator>>=(unsigned long s)
	{
		value >>= s;
		return *this;
	}

	fake_uint& operator<<=(unsigned long s)
	{
		value <<= s;
		return *this;
	}

	fake_uint& operator+=(U x)
	{
		value += x;
		return *this;
	}

	fake_uint& operator+=(const fake_uint& x)
	{
		value += x.value;
		return *this;
	}

	fake_uint& operator-=(const fake_uint& x)
	{
		value += x.value;
		return *this;
	}

	fake_uint& operator++()
	{
		++value;
		return *this;
	}

	U value;
};

template< typename U >
fake_uint< U > operator~(const fake_uint< U >& x)
{
	return fake_uint< U >(~x.value);
}

template< typename T, typename U >
fake_uint< U > operator*(
	T                     l,
	const fake_uint< U >& r
)
{
	return fake_uint< U >(l * r.value);
}

template< typename U >
fake_uint< U > operator<<(
	const fake_uint< U >& l,
	unsigned long         r
)
{
	return fake_uint< U >(l.value << r);
}

template< typename U >
fake_uint< U > operator>>(
	const fake_uint< U >& l,
	unsigned long         r
)
{
	return fake_uint< U >(l.value >> r);
}

template< typename U >
fake_uint< U > operator|(
	const fake_uint< U >& l,
	const fake_uint< U >& r
)
{
	return fake_uint< U >(l.value | r.value);
}

template< typename U >
fake_uint< U > operator^(
	const fake_uint< U >& l,
	const fake_uint< U >& r
)
{
	return fake_uint< U >(l.value ^ r.value);
}

template< typename U >
fake_uint< U > operator&(
	const fake_uint< U >& l,
	const fake_uint< U >& r
)
{
	return fake_uint< U >(l.value & r.value);
}

template< typename U >
fake_uint< U > operator+(
	const fake_uint< U >& l,
	const fake_uint< U >& r
)
{
	return fake_uint< U >(l.value + r.value);
}

template< typename U >
fake_uint< U > operator-(
	const fake_uint< U >& l,
	const fake_uint< U >& r
)
{
	return fake_uint< U >(l.value - r.value);
}

template< typename U >
bool operator<(
	const fake_uint< U >& l,
	const fake_uint< U >& r
)
{
	return l.value < r.value;
}

template< typename U >
bool operator>(
	const fake_uint< U >& l,
	const fake_uint< U >& r
)
{
	return l.value > r.value;
}

template< typename U >
bool operator==(
	const fake_uint< U >& l,
	const fake_uint< U >& r
)
{
	return l.value == r.value;
}

template< typename U >
bool operator!=(
	const fake_uint< U >& l,
	const fake_uint< U >& r
)
{
	return l.value != r.value;
}

template< typename U >
bool operator<=(
	const fake_uint< U >& l,
	const fake_uint< U >& r
)
{
	return l.value <= r.value;
}

template< typename U >
bool operator>=(
	const fake_uint< U >& l,
	const fake_uint< U >& r
)
{
	return l.value >= r.value;
}

#endif // FAKE_H
