#ifndef CLONE_H
#define CLONE_H

class ICloneable
{
public:
	virtual ~ICloneable()             = default;
	virtual ICloneable* clone() const = 0;
};

// Helper class for managing a T that derives from ICloneable
template< typename T >
class cloning_ptr
{
public:
	explicit cloning_ptr(T* x = nullptr)
		: p(x)
	{
	}

	cloning_ptr(const cloning_ptr& o)
		: p(o.p ? o.p->clone() : nullptr)
	{
	}

	cloning_ptr(cloning_ptr&& o) noexcept
		: p(o.p)
	{
		o.p = nullptr;
	}

	explicit cloning_ptr(const T& x)
		: cloning_ptr(x.clone() )
	{
	}

	~cloning_ptr()
	{
		delete p;
	}

	cloning_ptr& operator=(const cloning_ptr& o)
	{
		T* t = p;

		p = ( o.p ? o.p->clone() : nullptr );
		delete t;
		return *this;
	}

	cloning_ptr& operator=(cloning_ptr&& o) noexcept
	{
		T* t = p;

		p   = o.p;
		o.p = t;
		return *this;
	}

	cloning_ptr& operator=(const T& x)
	{
		T* t = p;

		p = x.clone();
		delete t;
		return *this;
	}

	T* operator->()
	{
		return p;
	}

	const T* operator->() const
	{
		return p;
	}
private:
	T* p;
};

#endif // CLONE_H
