#ifndef STRONG_TYPEDEF_H
#define STRONG_TYPEDEF_H

#include <utility>

enum strong_typedef_options : unsigned long long
{
	op_arrow = 1 << 0,
	op_dereference = 1 << 1,
	op_eq = 1 << 2,
	op_ne = 1 << 3,
	op_lt = 1 << 4,
	op_gt = 1 << 5,
	op_lte = 1 << 6,
	op_gte = 1 << 7,
	op_postinc = 1 << 8,
	op_preinc = 1 << 9,
	op_shl = 1 << 10,
	op_shr = 1 << 11,
	op_postdec = 1 << 12,
	op_predec = 1 << 13,
	op_equal_comparable = op_eq | op_ne,
	op_ordered = op_eq | op_ne | op_lt | op_gt | op_lte | op_gte,
	op_inc = op_postinc | op_preinc,
	op_dec = op_postdec | op_predec
};

namespace strong_typedef_details
{
// Without arrow member function
template< typename T, unsigned long long Os, bool Arrow = ( Os & op_arrow ) != 0 >
class basic_strong_typedef
{
public:
	explicit constexpr basic_strong_typedef()
		: value()
	{
	}

	explicit constexpr basic_strong_typedef(const T& value_)
		: value(value_)
	{
	}

	explicit constexpr basic_strong_typedef(T&& value_)
		: value(std::move(value_) )
	{
	}

	T& get()
	{
		return value;
	}

	const T& get() const
	{
		return value;
	}
private:
	T value;
};

// with arrow member function
/// @todo Arrow operator
template< typename T, unsigned long long Os >
class basic_strong_typedef< T, Os, true >
{
public:
	explicit constexpr basic_strong_typedef()
		: value()
	{
	}

	explicit constexpr basic_strong_typedef(const T& value_)
		: value(value_)
	{
	}

	explicit constexpr basic_strong_typedef(T&& value_)
		: value(std::move(value_) )
	{
	}

	T& get()
	{
		return value;
	}

	const T& get() const
	{
		return value;
	}

	auto operator->()
	{
		return value.operator->();
	}

	auto operator->() const
	{
		return value.operator->();
	}
private:
	T value;
};

enum class dummy_enabler
{
};

template< typename T, unsigned long long Os, bool B >
std::integral_constant< unsigned long long, Os > derp(const basic_strong_typedef< T, Os, B >&);

template< typename T, strong_typedef_options O >
using EnableIf = std::enable_if_t< ( decltype( derp(std::declval< T >() ) )::value& O ) != 0, dummy_enabler >;

template< typename T, EnableIf< T, op_dereference >... >
auto operator*(const T& x)
{
	return *( x.get() );
}

/// @todo Detect if the operator exists with https://stackoverflow.com/a/18603716
template< typename T, EnableIf< T, op_eq >... >
auto operator==(
	const T& l,
	const T& r
)
{
	return l.get() == r.get();
}

template< typename T, EnableIf< T, op_ne >... >
auto operator!=(
	const T& l,
	const T& r
)
{
	return l.get() != r.get();
}

template< typename T, EnableIf< T, op_lt >... >
auto operator<(
	const T& l,
	const T& r
)
{
	return l.get() < r.get();
}

template< typename T, EnableIf< T, op_lte >... >
auto operator<=(
	const T& l,
	const T& r
)
{
	return l.get() <= r.get();
}

template< typename T, EnableIf< T, op_gt >... >
auto operator>(
	const T& l,
	const T& r
)
{
	return l.get() > r.get();
}

template< typename T, EnableIf< T, op_gte >... >
auto operator>=(
	const T& l,
	const T& r
)
{
	return l.get() >= r.get();
}

template< typename T, EnableIf< T, op_preinc >... >
T& operator++(T& x)
{
	++x.get();
	return x;
}

template< typename T, EnableIf< T, op_postinc >... >
T operator++(
	T& x,
	int
)
{
	return T(x.get()++);
}

template< typename T, EnableIf< T, op_predec >... >
T& operator--(T& x)
{
	--x.get();
	return x;
}

template< typename T, EnableIf< T, op_postdec >... >
T operator--(
	T& x,
	int
)
{
	return T(x.get()--);
}

}

/// @todo By default, inherit everything, if possible
template< typename T, strong_typedef_options... Os >
using strong_typedef = strong_typedef_details::basic_strong_typedef< T, ( Os | ... ) >;

#endif // STRONG_TYPEDEF_H
