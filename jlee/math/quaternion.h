#ifndef MATH_QUATERNION_HPP
#define MATH_QUATERNION_HPP

#include <algorithm>
#include <cmath>
#include <cstddef>
#include <iostream>
#include <stdexcept>

/// @todo Move to matrix class
template< std::size_t N, typename R >
R trace(const R ( & M )[N][N])
{
	R x = 0;

	for (std::size_t i = 0; i < N; ++i)
	{
		x += M[i][i];
	}

	return x;
}

/** Provides interface for working with quaternion numbers
 *
 * Quaternions are often used for doing rotations and orientations in computer
 * graphics. As complex numbers can be used to represent rotations in 2d,
 * quaternions can represent rotations in three dimensions.
 *
 * Quaternions obey component wise addition. Multiplication is more complicated
 * and, in particular, not commutative.
 *
 * @todo Need a float version of hypot
 * @todo Surely the number of ops for rotate, and toDCM can be improved
 */// ==========================================================================
template< typename R >
class quaternion
{
public:
	using real_type = R;

	/** Default constructor sets value to zero
	 */
	quaternion()
		: Q()
	{
	}

	/** Directly construct a value
	 */
	explicit quaternion(
		real_type w,
		real_type x = 0.f,
		real_type y = 0.f,
		real_type z = 0.f
	)
	{
		Q[0] = w, Q[1] = x, Q[2] = y, Q[3] = z;
	}

	quaternion& assign(
		real_type w,
		real_type x,
		real_type y,
		real_type z
	)
	{
		Q[0] = w, Q[1] = x, Q[2] = y, Q[3] = z;
		return *this;
	}

	const real_type& operator[](std::size_t i) const
	{
		#ifndef NDEBUG

		if ( i >= 4 )
		{
			throw std::out_of_range("Component not defined for quaternion");
		}

		#endif

		return Q[i];
	}

	real_type& operator[](std::size_t i)
	{
		#ifndef NDEBUG

		if ( i >= 4 )
		{
			throw std::out_of_range("Component not defined for quaternion");
		}

		#endif

		return Q[i];
	}

	quaternion& operator+=(const quaternion& r)
	{
		Q[0] += r.Q[0], Q[1] += r.Q[1], Q[2] += r.Q[2], Q[3] += r.Q[3];

		return *this;
	}

	quaternion& operator-=(const quaternion& r)
	{
		Q[0] -= r.Q[0], Q[1] -= r.Q[1], Q[2] -= r.Q[2], Q[3] -= r.Q[3];

		return *this;
	}

	/** Multiplication with assignment
	 *
	 * @note Multiplication is not commutative. That is, a * b != b * a, in general.
	 * @todo There are some fast multiplication schemes, but they don't look all that
	 *       good. See: http://www.gamedev.net/reference/articles/article398.asp
	 */
	quaternion& operator*=(const quaternion& r)
	{
		return assign(Q[0] * r.Q[0] - Q[1] * r.Q[1] - Q[2] * r.Q[2] - Q[3] * r.Q[3],
			Q[0] * r.Q[1] + Q[1] * r.Q[0] + Q[2] * r.Q[3] - Q[3] * r.Q[2],
			Q[0] * r.Q[2] - Q[1] * r.Q[3] + Q[2] * r.Q[0] + Q[3] * r.Q[1],
			Q[0] * r.Q[3] + Q[1] * r.Q[2] - Q[2] * r.Q[1] + Q[3] * r.Q[0]);
	}

	quaternion& operator*=(real_type x)
	{
		Q[0] *= x, Q[1] *= x, Q[2] *= x, Q[3] *= x;

		return *this;
	}

	quaternion& operator/=(real_type x)
	{
		return this->operator*=(1 / x);
	}

	/** Rotate the quaternion about a vector (x, y, z), through an angle alpha
	 */
	quaternion& rotate(
		real_type alpha,
		real_type x,
		real_type y,
		real_type z
	)
	{
		const real_type s = std::sin(alpha / 2) / std::hypot(std::hypot(x, y), z);
		const real_type c = std::cos(alpha / 2);

		// Quaternion multiplication. P = R * Q
		quaternion r(c * Q[0] - s * ( x * Q[1] + y * Q[2] + z * Q[3] ),
		             c * Q[1] + s * ( x * Q[0] + y * Q[3] - z * Q[2] ),
		             c * Q[2] - s * ( x * Q[3] - y * Q[0] - z * Q[1] ),
		             c * Q[3] + s * ( x * Q[2] - y * Q[1] + z * Q[0] ) );

		/// @todo Use normalize
		r *= ( 1 / r.norm() );
		this->operator=(r);

		return *this;
	}

	quaternion& rotatex(real_type phi)
	{
		const real_type r = 2 / this->norm();

		return rotate(phi, 1 - r * ( Q[2] * Q[2] + Q[3] * Q[3] ), r * ( Q[1] * Q[2] + Q[0] * Q[3] ),
			r * ( Q[1] * Q[3] - Q[0] * Q[2] ) );
	}

	quaternion& rotatey(real_type phi)
	{
		const real_type r = 2 / this->norm();

		return rotate(phi, r * ( Q[1] * Q[2] - Q[0] * Q[3] ), 1 - r * ( Q[1] * Q[1] + Q[3] * Q[3] ),
			r * ( Q[2] * Q[3] + Q[0] * Q[1] ) );
	}

	quaternion& rotatez(real_type phi)
	{
		const real_type r = 2 / this->norm();

		return rotate(phi, r * ( Q[1] * Q[3] + Q[0] * Q[2] ), r * ( Q[2] * Q[3] - Q[0] * Q[1] ),
			1 - r * ( Q[1] * Q[1] + Q[2] * Q[2] ) );
	}

	/** Named constructor to create quaternion from a cosine matrix
	 *
	 * @todo May want to ensure M is of the appropriate form
	 * @todo There's also an algorithm to calculate the closest quaternion by
	 *       eigenvectors of a related 4x4 matrix. Stable against roundoff
	 * @todo Should we worry about normalization?
	 */
	static quaternion fromCosineMatrix(const real_type ( & M )[3][3])
	{
		if ( const real_type t = trace(M);t > -0.5f )
		{
			const real_type r = std::sqrt(1.0f + t);
			const real_type s = 0.5f / r;
			return quaternion(0.5f * r, ( M[2][1] - M[1][2] ) * s, ( M[0][2] - M[2][0] ) * s,
				( M[1][0] - M[0][1] ) * s);
		}

		if ( ( M[0][0] >= M[1][1] ) && ( M[0][0] >= M[2][2] ) )
		{
			// M[0][0] is largest
			const real_type r = 2 * std::sqrt(1.0f + M[0][0] - M[1][1] - M[2][2]);

			return quaternion( ( M[2][1] - M[1][2] ) / r, 0.25f * r, ( M[0][1] + M[1][0] ) / r,
				( M[2][0] + M[0][2] ) / r);
		}

		if ( M[1][1] >= M[2][2] )
		{
			// M[1][1] is largest
			const real_type r = 2 * std::sqrt(1.0f + M[1][1] - M[2][2] - M[0][0]);

			return quaternion( ( M[0][2] - M[2][0] ) / r, ( M[0][1] + M[1][0] ) / r, 0.25f * r,
				( M[1][2] + M[2][1] ) / r);
		}

		// M[2][2] is largest
		const real_type r = 2 * std::sqrt(1.0f + M[2][2] - M[0][0] - M[1][1]);

		return quaternion( ( M[1][0] - M[0][1] ) / r, ( M[2][0] + M[0][2] ) / r, ( M[1][2] + M[2][1] ) / r, 0.25f * r);
	}

	/** Returns the norm of the quaternion
	 */
	real_type norm() const
	{
		return std::hypot(Q[0], std::hypot(Q[1], std::hypot(Q[2], Q[3]) ) );
	}

	quaternion operator-() const
	{
		return quaternion(-Q[0], -Q[1], -Q[2], -Q[3]);
	}
private:
	real_type Q[4];
};

/// @bug Possibly division by zero
template< typename R >
quaternion< R > normalize(const quaternion< R >& q)
{
	return q / q.norm();
}

template< typename R >
quaternion< R > operator+(
	const quaternion< R >& q,
	const quaternion< R >& r
)
{
	return quaternion< R >(q[0] + r[0], q[1] + r[1], q[2] + r[2], q[3] + r[3]);
}

template< typename R >
quaternion< R > operator-(
	const quaternion< R >& q,
	const quaternion< R >& r
)
{
	return quaternion< R >(q[0] - r[0], q[1] - r[1], q[2] - r[2], q[3] - r[3]);
}

template< typename R >
quaternion< R > operator*(
	const quaternion< R >& q,
	const quaternion< R >& r
)
{
	return quaternion< R >(q.Q[0] * r.Q[0] - q.Q[1] * r.Q[1] - q.Q[2] * r.Q[2] - q.Q[3] * r.Q[3],
		q.Q[0] * r.Q[1] + q.Q[1] * r.Q[0] + q.Q[2] * r.Q[3] - q.Q[3] * r.Q[2],
		q.Q[0] * r.Q[2] - q.Q[1] * r.Q[3] + q.Q[2] * r.Q[0] + q.Q[3] * r.Q[1],
		q.Q[0] * r.Q[3] + q.Q[1] * r.Q[2] - q.Q[2] * r.Q[1] + q.Q[3] * r.Q[0]);
}

template< typename R >
quaternion< R > operator*(
	const quaternion< R >& q,
	R                      x
)
{
	return quaternion< R >(q[0] * x, q[1] * x, q[2] * x, q[3] * x);
}

template< typename R >
quaternion< R > operator*(
	R                      x,
	const quaternion< R >& q
)
{
	return quaternion< R >(q[0] * x, q[1] * x, q[2] * x, q[3] * x);
}

template< typename R >
quaternion< R > operator/(
	const quaternion< R >& q,
	const quaternion< R >& r
)
{
	const R rr = 1 / ( r[0] * r[0] + r[1] * r[1] + r[2] * r[2] + r[3] * r[3] );
	const R t0 = q[0] * r[0] + q[1] * r[1] + q[2] * r[2] + q[3] * r[3];
	const R t1 = q[1] * r[0] - q[0] * r[1] - q[3] * r[2] + q[2] * r[3];
	const R t2 = q[2] * r[0] + q[3] * r[1] - q[0] * r[2] - q[1] * r[3];
	const R t3 = q[3] * r[0] - q[2] * r[1] + q[1] * r[2] - q[0] * r[3];

	return quaternion< R >(t0 * rr, t1 * rr, t2 * rr, t3 * rr);
}

template< typename R >
quaternion< R > operator/(
	const quaternion< R >& q,
	R                      x
)
{
	return q * ( 1 / x );
}

template< typename R >
quaternion< R > operator/(
	R                      x,
	const quaternion< R >& r
)
{
	const R rr = 1 / ( r[0] * r[0] + r[1] * r[1] + r[2] * r[2] + r[3] * r[3] );
	const R t0 = x * r[0];
	const R t1 = x * -r[1];
	const R t2 = x * -r[2];
	const R t3 = x * -r[3];

	return quaternion< R >(t0 * rr, t1 * rr, t2 * rr, t3 * rr);
}

template< typename R >
R angle(
	const quaternion< R >& q_,
	const quaternion< R >& r_
)
{
	const quaternion< R > q = normalize(q_);
	const quaternion< R > r = normalize(r_);
	const R               x = ( q[0] * r[0] + q[1] * r[1] + q[2] * r[2] + q[3] * r[3] );

	/// @todo Use atan2 for better numerical stability
	return std::acos(std::clamp< R >(x, -1, 1) );
}

// slerp, when we know q, r to be unit quaternions
/// @todo Replace .9995 with a better bound based on the actual floating point performance
/// @todo Supposedly, when y is negative there is an issue with slerping the wrong way around
/// Doesn't seem to bare out in practice.
template< typename R >
quaternion< R > slerpu(
	const quaternion< R >& q,
	const quaternion< R >& r,
	R                      t
)
{
	const R y = ( q[0] * r[0] + q[1] * r[1] + q[2] * r[2] + q[3] * r[3] );

	if ( std::abs(y) < 0.9995 )
	{
		// The usual case
		/// @todo Use atan2 for better numerical stability
		const R phi0 = std::acos(y);
		const R phi  = phi0 * t;

		const R s1 = std::sin(phi) / std::sin(phi0);
		const R s0 = std::cos(phi) - y * s1;

		return s0 * q + s1 * r;
	}

	if ( y >= 0 )
	{
		// quaternions are very close together, so sin(phi) goes to zero. Linearly interpolate instead
		return normalize(q + ( r - q ) * t);
	}

	// q and r are antiparallel, so there is no unique interpolation through the smallest angle.
	// Choose another quaternion, u, that minimizes |cos(q, u)|. Then go from q to u to r
	std::size_t i = 0;

	if ( std::abs(q[1]) < std::abs(q[i]) )
	{
		i = 1;
	}

	if ( std::abs(q[2]) < std::abs(q[i]) )
	{
		i = 2;
	}

	if ( std::abs(q[3]) < std::abs(q[i]) )
	{
		i = 3;
	}

	quaternion< R > u(0);

	u[i] = 1;

	const R theta = std::acos(q[i]);
	const R p     = theta / std::acos(R(-1) );

	if ( t < p )
	{
		// interpolate from q to u
		return slerpu(q, u, t / p);
	}

	// interpolate from u to r
	return slerpu(u, r, ( t - p ) / ( 1 - p ) );
}

/**
   @todo Since we will usually want to slerp continuously, create an object to save
   intermediate calculations
   t should be in [0,1]
 */
template< typename R >
quaternion< R > slerp(
	const quaternion< R >& q,
	const quaternion< R >& r,
	R                      t
)
{
	return slerpu(normalize(q), normalize(r), t);
}

template< typename R >
std::ostream& operator<<(
	std::ostream&          o,
	const quaternion< R >& q
)
{
	return o << '{' << q[0] << ',' << q[1] << ',' << q[2] << ',' << q[3] << '}';
}

#endif // MATH_QUATERNION_H
