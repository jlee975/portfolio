#ifndef MATH_GEOMETRY_AFFINE_HPP
#define MATH_GEOMETRY_AFFINE_HPP

#include <array>
#include <cmath>
#include <iosfwd>
#include <iostream>

#include "../linear_algebra/matrices.h"

#include "primitives/ball.h"
#include "primitives/box.h"
#include "primitives/plane.h"
#include "primitives/point.h"
#include "primitives/polygon.h"
#include "primitives/ray.h"

#ifdef DEBUG_MATH_GEOMETRY
#define DEBUG_MATH_GEOMETRY_AFFINE
#endif

constexpr float MAGNITUDE_TOLERANCE = 1e-6f;

namespace geo
{
template< dimension_type D, typename Real >
class affine;

template< dimension_type D, typename Real >
std::ostream& operator<<(std::ostream& os, const affine< D, Real >& a);

/** @brief An affine transformation
 *
 * This class encapsulates an affine transformation. Specifically, those
 * transformations that can be produced by translation, rotation, and
 * uniform scaling.
 *
 * In addition to representing the group of affine transformations, each object
 * is able to transform geometric primitives from one frame to another.
 *
 * @note Not all affine transformations are representable by the Affine<3>
 *  specialization. It's interface may be slightly different.
 *
 * @todo inverse of point, plane, etc.
 * @todo Provide class specialization for 3d
 * @todo http://en.wikipedia.org/wiki/Euler%E2%80%93Rodrigues_parameters
 * @todo Move more functions to matrix class
 * @todo Split conceptually in to the linear algebra object and the transforming-geometries object
 * @todo https://en.wikipedia.org/wiki/Affine_transformation#Augmented_matrix
 */
template< dimension_type D, typename Real = float >
class affine
{
public:
	using real_type = Real;
	static const dimension_type dimension = D;

	/// @todo Bundle these types as R3 and inherit to get them
	using point_type    = point< D, real_type >;
	using plane_type    = plane< D, real_type >;
	using box_type      = box< D, real_type >;
	using ball_type     = ball< D, real_type >;
	using triangle_type = triangle< D, real_type >;
	using ray_type      = ray< D, real_type >;

	struct initializer
	{
		/// @todo matrix::initializer
		real_type data[D][D];
		real_type scale;
		typename point< D, real_type >::initializer orig;
	};

	/** @brief Create the identity transformation
	 *
	 * This transformation does nothing to the primitives it operates on.
	 */
	explicit affine(real_type x)
		: data(1), scale_(x), origin_(zero_initialized)
	{
	}

	explicit affine(uninitialized_type)
		: data(uninitialized), origin_(uninitialized)
	{
	}

	explicit affine(default_initialized_type)
		: affine(1)
	{
	}

	/// @todo data(x.first), origin_(x.second)
	affine(initializer x)
		: data(uninitialized), scale_(x.scale), origin_(x.orig)
	{
		for (dimension_type i = 0; i < D; ++i)
		{
			for (dimension_type j = 0; j < D; ++j)
			{
				data(i, j) = x.data[i][j];
			}
		}

		debug_form();
	}

	// Assignment
	/** @brief Store the transformation as a 4x4 matrix in row major order
	 * @param p Pointer to an array of 16 floats
	 * @todo Provide a double version
	 */
	void copy_row_major(real_type* p) const
	{
		for (dimension_type i = 0; i < D; ++i)
		{
			for (dimension_type j = 0; j < D; ++j)
			{
				*p++ = data(i, j) * scale_;
			}

			*p++ = origin_[i];
		}

		for (dimension_type i = 0; i < D; ++i)
		{
			*p++ = 0;
		}

		*p = 1;
	}

	/** @brief Store the transformation as a 4x4 matrix in column major order
	 * @param p Pointer to an array of 16 floats
	 * @todo Provide a double version
	 */
	void copy_col_major(real_type* p) const
	{
		for (dimension_type i = 0; i < D; ++i)
		{
			for (dimension_type j = 0; j < D; ++j)
			{
				*p++ = data(j, i) * scale_;
			}

			*p++ = 0;
		}

		for (dimension_type j = 0; j < D; ++j)
		{
			*p++ = origin_[j];
		}

		*p++ = 1;
	}

	/** @name Manipulating transformations
	 */
	///@{
	/** @brief Combine two transformations using multiplicative-group notation
	 *
	 * The result of T * U is a single transformation that is the same as
	 * applying U, then T. For example, given a vector v, (T * U)(v) = T(U(v))
	 *
	 * @todo Non-member function
	 */
	affine operator*(const affine& b) const
	{
		affine c(uninitialized);

		la::detail::multiply(data, b.data, c.data);
		c.scale_ = scale_ * b.scale_;

		/// @todo Avoid copy and store in destination
		c.origin_ = this->operator()(b.origin_);

		c.debug_form();

		return c;
	}

	/// @brief Assign the result of (*this * b) back to *this
	affine& operator*=(const affine& b)
	{
		*this = this->operator*(b);
		debug_form();
		return *this;
	}

	void scale(real_type s)
	{
		scale_ *= s;
		debug_form();
	}

	void rescale(real_type s)
	{
		scale_ = s;
		debug_form();
	}

	template< typename...Args >
	void translate(Args&&... args)
	{
		if constexpr ( std::is_constructible_v < point_type, Args && ... > )
		{
			origin_ += point_type(args...);
		}
		else
		{
			origin_ += point_type({ args... });
		}
	}

	// rotate theta radians within plane pq (p != q)
	/// @todo Document as rotation without scaling, etc.
	/// @todo Name pre_rotate, or otherwise clarify order of scale, rotate, translate
	/// @todo specify p and q as vectors, the span of which form the plane
	void rotate(
		real_type      theta,
		dimension_type p,
		dimension_type q
	)
	{
		const real_type c = std::cos(theta);
		const real_type s = std::sin(theta);

		// recalculate columns p and q
		for (dimension_type i = 0; i < D; ++i)
		{
			const real_type x = data(i, q);
			const real_type y = data(i, p);
			data(i, q) = x * c - y * s;
			data(i, p) = x * s + y * c;
		}

		debug_form();
	}

	/** @brief Get the transformation that has the opposite effect of this one
	 *
	 * Basically a * a.inverse() is a transformation of no effect (the identity
	 * transformation, also made by the default constructor).
	 *
	 * @todo Take advantage of the fact that data = U * D, where U is orthogonal and D
	 * is scalar. Then inverse is then D'U' where A' is inverse of A. Both of
	 * these are easy to calculate. In fact, we can use a cheat to simply transpose
	 * "data" and divide the rows by the squares of the sums of the columns.
	 */
	affine inverse() const
	{
		affine inv(uninitialized);

		inv.data = la::invert(data, la::orthogonal_matrix);

		inv.scale_ = 1 / scale_;

		/// @todo This looks like matrix-vector multiplication except with sign flipped
		for (dimension_type i = 0; i < D; ++i)
		{
			real_type x = 0;

			for (dimension_type j = 0; j < D; ++j)
			{
				x -= origin_[j] * inv.data(i, j);
			}

			inv.origin_[i] = inv.scale_ * x;
		}

		inv.debug_form();

		return inv;
	}

	///@}

	/** @name Conversion
	 */
	///@{
	/** @brief Map points of A to B
	 *
	 * "(A => global) => B" === "B.inverse() * A"
	 */
	/// @todo Direct calculation
	/// @todo static member function
	/// @todo Calculate directly without intermediate inverse calculation
	affine to(const affine& a) const
	{
		return a.inverse() * ( *this );
	}

	affine from(const affine& a) const
	{
		return ( *this ) * a.inverse();
	}

	///@}

	/** @name Transformations of geometric primitives
	 *
	 * Change the given primitive to the "global" coordinate system. These are
	 * all implemented as functors.
	 */
	///@{
	/** @brief Origin in global coordinates
	 *
	 * Same as operator()(point_type{ {0, 0, 0} });
	 */
	const point_type& origin() const
	{
		return origin_;
	}

	/** @brief Transformation of a scalar
	 *
	 * Returns scale * x. Basically, the length of (1, 0, 0) after
	 * transform. Has same sign as x.
	 */
	real_type operator()(real_type x) const
	{
		real_type y = 0;

		for (dimension_type i = 0; i < D; ++i)
		{
			y += data(0, i) * data(0, i);
		}

		return x * scale_ * std::sqrt(y);
	}

	/** @brief Apply the transformation to the point given
	 *
	 * If this is the matrix A, and v is a column vector (x,y,z,1) then
	 * the result is A*v (projected back into 3D by dropping the 4th coord)
	 */
	point_type operator()(const point_type& v) const
	{
		point_type u(uninitialized);

		/// @todo transform_point
		transform(v.raw(), u.raw() );

		return u + origin_;
	}

	ray_type operator()(const ray_type& v) const
	{
		ray_type u(uninitialized);

		transform(v.raw(), u.raw() );

		return u;
	}

	/** @brief Apply the transformation to the plane given
	 *
	 * @note This normal of the resultant plane has unit length, even if this
	 * transformation would otherwise shrink or expand vectors
	 *
	 * @todo Can we calculate r more directly?
	 */
	plane_type operator()(const plane_type& p) const
	{
		// a point on the plane
		const point_type r = this->operator()(p.foot() );

		// the normal to the plane
		/// @todo The subtracted origin just cancels, may as well have a simple function to rotate
		/// directions (as opposed to points)
		ray_type n = this->operator()(p.n);

		return make_plane(r, n);
	}

	template< size_type N >
	polygon< N, D, Real > operator()(const polygon< N, D, Real >& p) const
	{
		polygon< N, D, Real > q;

		for (size_type i = 0; i < N; ++i)
		{
			q.v[i] = this->operator()(p.v[i]);
		}

		return q;
	}

	/// @todo Rename b/c does not preserve box
	box_type operator()(const box_type& x) const
	{
		box_type b(origin_, origin_);

		// What's the min/max in each dimension?
		for (dimension_type j = 0; j < D; ++j)
		{
			real_type l = 0;
			real_type u = 0;

			for (dimension_type k = 0; k < D; ++k)
			{
				const real_type t = data(j, k) * x.min[k];
				const real_type s = data(j, k) * x.max[k];

				l += std::min(t, s);
				u += std::max(t, s);
			}

			b.min[j] += scale_ * l;
			b.max[j] += scale_ * u;
		}

		return b;
	}

	ball_type operator()(const ball_type& x) const
	{
		return { this->operator()(x.center() ), this->operator()(x.radius() ) };
	}

	///@}

	/** @name Transformations to this frame
	 */
	///@{
	/// @todo Calculate directly, using the special form of the matrix
	point_type invert(const point_type& v) const
	{
		const affine& i = inverse();

		return i(v);
	}

	/// @todo Calculate directly, using the special form of the matrix
	/// @todo Scale doesn't matter, so we don't need the full inverse
	ray_type invert(const ray_type& v) const
	{
		const affine& i = inverse();

		return i(v);
	}

	plane_type invert(const plane_type& p) const
	{
		const affine& i = inverse();

		return i(p);
	}

	///@}
private:
	friend std::ostream& operator<<<>( std::ostream&, const affine< D, Real >& );

	// Check that data is symmetric and has the same magnitude for every row
	void debug_form()
	{
		#ifdef DEBUG_MATH_GEOMETRY_AFFINE
		orthogonalize(data);

		/// @todo Extract as is_orthonormal
		real_type min = INFINITY;
		real_type max = 0;

		// Check that matrix is orthogonal (up to a factor)
		for (dimension_type i = 0; i < D; ++i)
		{
			for (dimension_type j = 0; j < D; ++j)
			{
				real_type x = 0;

				for (dimension_type k = 0; k < D; ++k)
				{
					x += data(i, k) * data(j, k);
				}

				if ( i == j )
				{
					if ( x < min )
					{
						min = x;
					}

					if ( x > max )
					{
						max = x;
					}
				}
			}
		}

		if ( max - 1 > MAGNITUDE_TOLERANCE || 1 - min > MAGNITUDE_TOLERANCE )
		{
			std::cerr << min << std::endl;
			std::cerr << max << std::endl;
			// throw std::logic_error("Affine matrix is not normalized");
		}

		#endif // ifdef DEBUG_MATH_GEOMETRY_AFFINE
	}

	/// @todo This is matrix-vector multiplication
	void transform(
		const real_type ( & v )[D],
		real_type ( & u )[D]
	) const
	{
		for (dimension_type i = 0; i < D; ++i)
		{
			real_type x = 0;

			for (dimension_type j = 0; j < D; ++j)
			{
				x += v[j] * data(i, j);
			}

			u[i] = scale_ * x;
		}
	}

	/// @todo Prove this is scaled_orthonal_matrix and use the hint invoking the matrix
	/// @todo la::linear_map
	la::matrix< Real, D, D > data; // transformation as a 4x4 matrix in row-major order

	Real scale_;

	/// @todo Should be la::vector type
	/// @todo la::translation
	point_type origin_;
};

template< typename Real >
void rotate(
	affine< 3, Real >& a,
	Real               pitch,
	Real               yaw,
	Real               roll
)
{
	if ( std::fabs(roll) > 0 )
	{
		a.rotate(roll, 0, 1);
	}

	if ( std::fabs(yaw) > 0 )
	{
		a.rotate(yaw, 2, 0);
	}

	if ( std::fabs(pitch) > 0 )
	{
		a.rotate(pitch, 1, 2);
	}
}

}

#endif // ifndef MATH_GEOMETRY_AFFINE_HPP
