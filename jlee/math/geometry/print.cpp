#include "print.h"

namespace geo
{

std::ostream& operator<<(
	std::ostream&     os,
	intersection_type t
)
{
	switch ( t )
	{
	case no_intersection:
		os << "no collision";
		break;
	case intersects:
		os << "collision";
		break;
	default:
		throw std::logic_error("Unknown collision type");
	}

	return os;
}

}
