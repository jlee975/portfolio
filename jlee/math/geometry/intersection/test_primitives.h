#ifndef MATH_GEOMETRY_INTERSECTION_HPP
#define MATH_GEOMETRY_INTERSECTION_HPP

#include "../primitives/ball.h"
#include "../primitives/box.h"
#include "../primitives/obb.h"
#include "../primitives/polygon.h"

#include "intersection_result.h"

namespace geo
{
/// @todo Rename test_intersect as test_incident, or similar
// check if abc and def intersect
template< typename Real >
constexpr bool test_intersect(
	const triangle< 3, Real >& t1,
	const triangle< 3, Real >& t2
)
{
	const point< 3, Real >& a = t1[0];
	const point< 3, Real >& b = t1[1];
	const point< 3, Real >& c = t1[2];
	const point< 3, Real >& d = t2[0];
	const point< 3, Real >& e = t2[1];
	const point< 3, Real >& f = t2[2];

	const point< 3, Real > n1 = perp(a - c, b - a);

	// Test which half-space each point lies in, relative to P1, P2
	const Real h1[3] = { n1* ( a - d ), n1* ( a - e ), n1* ( a - f ) };

	// If all points lie in the same half space, there is obviously no intersection
	if ( ( h1[0] > 0 && h1[1] > 0 && h1[2] > 0 ) || ( h1[0] < 0 && h1[1] < 0 && h1[2] < 0 ) )
	{
		return false;
	}

	const point< 3, Real > n2    = perp(d - f, e - d);
	const Real             h2[3] = { n2* ( d - a ), n2* ( d - b ), n2* ( d - c ) };

	if ( ( h2[0] > 0 && h2[1] > 0 && h2[2] > 0 ) || ( h2[0] < 0 && h2[1] < 0 && h2[2] < 0 ) )
	{
		return false;
	}

	/// @bug Coplanarity test - Method is probably not robust when two
	/// triangles are in the same plane.

	// Two points must lie on one half space, one in the other
	// This defines two crossing: Q and R
	point< 3, Real > Q = point< 3, Real >::ORIGIN;

	point< 3, Real > R = point< 3, Real >::ORIGIN;

	// Check the three edges ab, bc, ca
	if ( ( h2[0] < 0 && h2[1] >= 0 ) || ( h2[0] >= 0 && h2[1] < 0 ) )
	{
		R = Q;
		Q = a + ( b - a ) * ( h2[0] / ( n2 * ( b - a ) ) );
	}

	if ( ( h2[1] < 0 && h2[2] >= 0 ) || ( h2[1] >= 0 && h2[2] < 0 ) )
	{
		R = Q;
		Q = b + ( c - b ) * ( h2[1] / ( n2 * ( c - b ) ) );
	}

	if ( ( h2[2] < 0 && h2[0] >= 0 ) || ( h2[2] >= 0 && h2[0] < 0 ) )
	{
		R = Q;
		Q = c + ( a - c ) * ( h2[2] / ( n2 * ( a - c ) ) );
	}

	// Similarly for the other plane
	point< 3, Real > S = point< 3, Real >::ORIGIN;

	point< 3, Real > T = point< 3, Real >::ORIGIN;

	// Check the three edges de, ef, fd
	if ( ( h1[0] < 0 && h1[1] >= 0 ) || ( h1[0] >= 0 && h1[1] < 0 ) )
	{
		T = S;
		S = d + ( e - d ) * ( h1[0] / ( n1 * ( e - d ) ) );
	}

	if ( ( h1[1] < 0 && h1[2] >= 0 ) || ( h1[1] >= 0 && h1[2] < 0 ) )
	{
		T = S;
		S = e + ( f - e ) * ( h1[1] / ( n1 * ( f - e ) ) );
	}

	if ( ( h1[2] < 0 && h1[0] >= 0 ) || ( h1[2] >= 0 && h1[0] < 0 ) )
	{
		T = S;
		S = f + ( d - f ) * ( h1[2] / ( n1 * ( d - f ) ) );
	}

	// collision if the following test is true:
	// Q is in [S,T] or R is in [S,T] or S in [R,Q]
	if ( ( S - Q ) * ( T - Q ) <= 0 || ( S - R ) * ( T - R ) <= 0 || ( R - S ) * ( Q - S ) <= 0 )
	{
		return true;
	}

	return false;
}

// Check AABBs for collision
template< dimension_type N, typename Real >
constexpr bool test_intersect(
	const box< N, Real >& b1,
	const box< N, Real >& b2
)
{
	for (dimension_type i = 0; i < N; ++i)
	{
		if ( b1.max[i] < b2.min[i] || b2.max[i] > b1.min[i] )
		{
			return false;
		}
	}

	return true;
}

/// @todo D-dimensional
// check if def intersects with obb
/// @bug Doesn't properly handle overlap. False positives.
template< typename Real >
constexpr bool test_intersect(
	const OBB< 3, Real >&      b,
	const triangle< 3, Real >& tri
)
{
	const point< 3, Real >& d = tri[0];
	const point< 3, Real >& e = tri[1];
	const point< 3, Real >& f = tri[2];

	const Real m[3][3] = {{ d* b.faces[0], d* b.faces[1], d* b.faces[2] },
		{ e* b.faces[0], e* b.faces[1], d* b.faces[2] },
		{ f* b.faces[0], f* b.faces[1], f* b.faces[2] }};

	// currently checking if all three points are completely outside of the bb
	if ( m[0][0] > b.max[0] && m[1][0] > b.max[0] && m[2][0] > b.max[0] )
	{
		return false;
	}

	if ( m[0][0] < b.min[0] && m[1][0] < b.min[0] && m[2][0] < b.min[0] )
	{
		return false;
	}
	else if ( m[0][1] > b.max[1] && m[1][1] > b.max[1] && m[2][1] > b.max[1] )
	{
		return false;
	}
	else if ( m[0][1] < b.min[1] && m[1][1] < b.min[1] && m[2][1] < b.min[1] )
	{
		return false;
	}
	else if ( m[0][2] > b.max[2] && m[1][2] > b.max[2] && m[2][2] > b.max[2] )
	{
		return false;
	}
	else if ( m[0][2] < b.min[2] && m[1][2] < b.min[2] && m[2][2] < b.min[2] )
	{
		return false;
	}

	return true;
}

template< dimension_type D, typename Real >
intersection_result< D, Real > test_intersect(
	const ball< D, Real >& b1,
	const ball< D, Real >& b2
)
{
	const auto& p1 = b1.center();
	const auto& p2 = b2.center();

	const auto r1 = b1.radius();
	const auto r2 = b2.radius();

	const auto v_ = la::normalize(p2 - p1);
	const auto v  = make_ray(v_);

	// Projection of sphere centers along vector v
	const auto d1 = la::inner(p1, v_);
	const auto d2 = la::inner(p2, v_);

	if ( d1 < d2 )
	{
		if ( d1 + r1 < d2 - r2 )
		{
			// separated
			return geo::separation_info< D, Real >{ v, d1 + r1, d2 - r2 };
		}

		return geo::intersection_info< D, Real >{ v };
	}

	// d1 >= d2
	if ( d1 - r1 > d2 + r2 )
	{
		return geo::separation_info< D, Real >{ v, d1 - r1, d2 + r2 };
	}

	return geo::intersection_info< D, Real >{ v };
}

}

#endif // MATH_GEOMETRY_INTERSECTION_HPP
