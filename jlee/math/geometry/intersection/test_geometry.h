#ifndef MATH_GEOMETRY_INTERSECTION_TEST_GEOMETRY_HPP
#define MATH_GEOMETRY_INTERSECTION_TEST_GEOMETRY_HPP

#include "../primitives/dimension.h"

namespace geo
{
template< dimension_type, typename >
struct intersection_result;

class Geometry;

template< dimension_type, typename >
class affine;

/** @brief Return whether or not objects a and b have at least one point in common
 *
 * Passes parameters to specialized functions
 */
intersection_result< 3, float > test_intersect(const Geometry& a, const affine< 3, float >& aframe, const Geometry& b, const affine< 3, float >& bframe);
}

#endif // MATH_GEOMETRY_INTERSECTION_TEST_GEOMETRY_HPP
