#include "test_geometry.h"

#include "../affine.h"
#include "../geometry.h"
#include "../primitives/angle.h"
#include "../primitives/ball.h"
#include "../primitives/flat.h"
#include "../primitives/point.h"
#include "../primitives/ray.h"
#include "../print.h"

#include "test_primitives.h"

#ifdef DEBUG_MATH
#define DEBUG_MATH_GEOMETRY
#endif
#ifdef DEBUG_MATH_GEOMETRY
#define DEBUG_MATH_GEOMETRY_INTERSECTION
#endif
#ifdef DEBUG_MATH_GEOMETRY_INTERSECTION
#define DEBUG_MATH_GEOMETRY_INTERSECTION_COLLISIONS
#define DEBUG_MATH_GEOMETRY_INTERSECTION_COLLISIONS_PRINT
#endif
#ifdef DEBUG_MATH_GEOMETRY_INTERSECTION_COLLISIONS
constexpr float ORTHOGONALITY_TOLERANCE      = 1e-4f;
constexpr float DISTANCE_TO_ORIGIN_TOLERANCE = 1e-4f;
#endif
#ifdef DEBUG_MATH_GEOMETRY_INTERSECTION_COLLISIONS_PRINT
constexpr float NULL_VECTOR_TOLERANCE = 1e-6f;
#endif

namespace
{
/** @brief Helper class for GJK-style intersection testing
 *
 * @invariant W[i] is older than W[j] if i < j. The Wi are kept in the relative order that they were added via add
 * @invariant There is an x <= 0 such that for all i, dir_ * W[i] == x
 *
 * @todo Reimplement as a matrix and use matrix_view class to work on the sub-matrix
 * @todo It seems that many calculations center around barycentric coordinates,
 *   or at least the associated transformation matrix. Calculate once and reuse
 *   especially to encourage consistent results (sometimes floating point
 *   calculations produce contradictory results).
 * @todo inheritence hierarchy simplex< 0 >, simplex< 1 >, simplex< 2 >, simplex< 3 >
 *  so that we have templated types. Use those here.
 * @todo The simplex<1> intersection case line-line intersection
 * @todo The simplex<2> intersection case is triangle-triangle intersection
 * @todo Is it more robust to calculate distance_from_origin_ using multiple points?
 * Ex., dir * (W[0]+W[1]) / 2. Technically dir*W[0] and dir*W[1] should be the same,
 * but numerically... not so much. Or take the min value?
 */
class Simplex
{
	static const geo::dimension_type dimension = 3;

	/** Use these definitions to a) help with the jump table, b) make the
	 * code slightly clearer, c) index into W
	 */
	enum simplex_type
	{
		Empty = 0,
		Point = 1,
		Line = 2,
		Triangle = 3,
		Tetrahedron = 4
	};
public:
	using real_type  = float;
	using point_type = geo::point< dimension, real_type >;

	/* Dir can be anything non-zero
	 */
	Simplex()
		: dir_({ 1, 0, 0 }), W{point_type(uninitialized), point_type(uninitialized), point_type(uninitialized),
		                       point_type(uninitialized)}
	{
	}

	/** Extend the simplex by one point
	 *
	 * @param w the point we're adding
	 *
	 * @todo Could probably take advantage of w.dir since we would have used it to determine
	 * if there was a separating plane
	 * @todo Take exact w0 and w1 -- the point in the original frame plus the (affine) frame
	 */
	void add(
		const point_type& w0,
		const point_type& w1
	)
	{
		#ifdef DEBUG_MATH_GEOMETRY_INTERSECTION_COLLISIONS

		if ( nW > dimension )
		{
			throw std::logic_error("Simplex is already as large as it can be.");
		}

		#endif

		W[nW] = w0 - w1;

		#ifdef DEBUG_MATH_GEOMETRY_INTERSECTION_COLLISIONS
		// "Backup" W, 'cause we need to check that W is in the correct order on exit (oldest first)
		const std::array< point_type, dimension + 1 > oldW  = W;
		const simplex_type                            oldnW = nW;
		#endif

		nW = static_cast< simplex_type >( nW + 1 );
		debug_reduce();

		/* Reduce the simplex to the smallest subsimplex that is closest to the origin
		 *
		 * The following routines will set nW and dir_, and discover a collision (i.e., if
		 * the simplex contains the origin. If the origin *is* contained by the simplex,
		 * then dir_ will be set to zero.
		 *
		 * Theorem: The new point is always kept by the reduction process.
		 * Proof: Needs some finesse, but the general idea is:
		 *
		 * From Empty, the proof is trivial. We have only one point, so we keep it.
		 *
		 * For the non-empty case, assume we don't keep Wi. But then we are keeping what
		 * we already had: {W0,...,W{i-1}}. But we had that the closest point to O was in
		 * the interior of {W0,...,W{i-1}}. Call this point P. The line OP defines a vector
		 * n such that n dot Wj >= 0 for 0 <= j < i, and n dot Wi <= 0. Because we didn't
		 * have linear dependence in the previous iteration, n dot Wj > 0, strictly. Thus
		 * Wi cannot lie in the previous flat as {W0,...,W{i-1}}. Thus OP intersects this
		 * new simplex in a line segment of non-zero length and there is a closer point
		 * than P. So P is no longer the closest point, and we will not discard Wi.
		 */
		switch ( nW )
		{
		case Empty:
			throw std::logic_error("reduce() is never called on an empty simplex");
		case Point:
			reduce_point();
			break;
		case Line:
			reduce_line();
			break;
		case Triangle:
			reduce_triangle();
			break;
		case Tetrahedron:
			reduce_tetrahedron();
			break;
		}

		#ifdef DEBUG_MATH_GEOMETRY_INTERSECTION_COLLISIONS
		{
			std::size_t indices[dimension + 1];

			for (std::size_t i = 0; i < nW; ++i)
			{
				std::size_t j = ( i == 0 ? 0 : indices[i - 1] + 1 );

				while ( j <= oldnW && oldW[j] != W[i] )
				{
					++j;
				}

				if ( j > oldnW )
				{
					for (std::size_t k = 0; k < oldnW; ++k)
					{
						std::cout << "W'[" << k << "] = " << oldW[k] << std::endl;
					}

					for (std::size_t k = 0; k < nW; ++k)
					{
						std::cout << "W[" << k << "] = " << W[k] << std::endl;
					}

					throw std::logic_error("W not in proper order");
				}

				indices[i] = j;
			}
		}

		#endif // ifdef DEBUG_MATH_GEOMETRY_INTERSECTION_COLLISIONS
		dir_ = la::normalize(dir_);
		debug_reduce_check();
	}

	bool contains_origin() const
	{
		return la::zero(dir_);
	}

	[[nodiscard]] float distance_from_origin() const
	{
		return -la::inner(dir_, W[0]);
	}

	/** @brief Direction for the GJK search to go in
	 *
	 * The returned ray is unit length.
	 *
	 * This direction will be perpendicular to the simplex and pointing toward the origin.
	 * That is, there exists x <= 0 st for all points w in the simplex inner(dir(),w) = x
	 *
	 * @todo Lift the requirement that the returned ray is unit length
	 */
	[[nodiscard]] geo::ray< dimension, real_type > dir() const
	{
		return make_ray(dir_);
	}
private:
	/** @brief "Reduces" a point simplex
	 *
	 * Doesn't actually reduce anything, because we can't go to an empty simplex. If W[0]
	 * happens to be 0, dir_ is consequently also 0.
	 */
	void reduce_point()
	{
		nW   = Point;
		dir_ = -W[0];
	}

	/** @brief Reduce a line, possibly to an end point
	 *
	 * It's possible that a line could be reduced to W[1], if all other points on the line (W[0],W[1])
	 * are further from the origin. This can happen iff the angle at W[1] in the triangle (O,W[0],W[1])
	 * is >= 90 degrees. Consequently, if cos(W[1]) <= 0 or
	 *
	 *     inner(W[0]-W[1], -W[1]) <= 0
	 *     inner(W[0],-W[1])-inner(W[1],-W[1]) <= 0
	 *     -inner(W[0],W[1])+inner(W[1],W[1]) <= 0
	 *     inner(W[1],W[1])-inner(W[0],W[1]) <= 0
	 *
	 * Because of the ordering invariant of W, we can never reduce to W[0]: The reduced simplex always contains the
	 * newest point.
	 *
	 * The direction to search in will be (W[1]-W[0])x(W[0]xW[1]), which is pointing in the "correct" direction (i.e.,
	 * toward the origin). It is a vector perpendicular to the line (W[0],W[1]) and it points "toward" the origin. It
	 * is calculated after some rearrangement, to reuse the dot products we already have
	 *
	 *     (W[1]-W[0])x(W[0]xW[1])
	 *        = ((W[1]-W[0]).W[1]) * W[0] - ((W[1]-W[0]).W[0]) * W[1]     by triple product expansion/Lagrange's formula
	 *        = (W[1].W[1] - W[1].W[0]) * W[0] - (W[1].W[0] - W[0].W[0]) * W[1]
	 *        = (W[1].W[1] - W[1].W[0]) * W[0] + (W[0].W[0] - W[1].W[0]) * W[1]
	 */
	void reduce_line()
	{
		const auto y = la::inner(W[1], W[0]);
		const auto t = la::inner(W[1]) - y;

		if ( t <= 0 )
		{
			// W[1] is closest point to O on the line (W[0], W[1])
			W[0] = W[1];
			reduce_point();
			return;
		}

		dir_ = -flat_normal(W[0], W[1]);
		nW   = Line;
	}

	/** @brief Reduce triangle to lines, or stay with a triangle
	 *
	 * As previously proven, W[2] must be a part of the final simplex, so we can reduce to either {W[0],W[2]} or
	 * {W[1],W[2]}. We'll derive a straightforward test for reducing to {W[0],W[2]}:
	 *
	 * We can reduce to {W[0],W[2]} if the foot of O on {W[0],W[1],W[2]} lies on the opposite side of {W[0],W[2]}
	 * as W[1]. Equivalently, if the angle between the planes {W[0],W[1],W[2]} and {O,W[0],W[2]} is not acute. This
	 * can be seen by "looking along" the line {W[0],W[2]}, which is the line of intersection of the two planes. If
	 * the angle is not acute, the triangle made by projecting O, W[2] and {W[0],W[1]} onto this view plane clearly
	 * shows that O is not supported by the {W[0],W[1],W[2]} triangle. Thus, {W[0],W[2]} is the closest subsimplex.
	 *
	 * This test reduces to checking the sign of the dot product of the normals for the two planes. They are
	 * (W[1]-W[2])x(W[1]-W[0]) and (O-W[2])x(O-W[0]) == (W[2]xW[0]). The first of these can be calculated in 6 muls
	 * and 6 subtracts:
	 *
	 *    {
	 *       (W[0][1]-W[1][1])*(W[0][2]-W[2][2])-(W[0][2]-W[1][2])*(W[0][1]-W[2][1]),
	 *       (W[0][2]-W[1][2])*(W[0][0]-W[2][0])-(W[0][0]-W[1][0])*(W[0][2]-W[2][2]),
	 *       (W[0][0]-W[1][0])*(W[0][1]-W[2][1])-(W[0][1]-W[1][1])*(W[0][0]-W[2][0]
	 *    }
	 *
	 * @todo There may be simpler tests
	 * @todo Another thought: n, the normal to the plane, is also a multiple of the projection of O onto the plane.
	 * So, equivalently we are testing if this point P is in the triangle or outside it
	 * @todo If coming from reduce_tetrahedron, forward some of the calculations so we don't have to redo them.
	 * @todo Forward some calculations to reduce_line
	 * @todo If coming from add() (i.e., expanding a line to a triangle) then dir_ should be the projection of O onto
	 * W[0],W[1]. Could be useful.
	 */
	void reduce_triangle()
	{
		/// @todo exact vectors
		const auto n0 = cross(W[0], W[1]);
		const auto n1 = cross(W[1], W[2]);
		const auto n2 = cross(W[2], W[0]);

		// n = W[0]xW[1]+W[1]xW[2]+W[2]xW[0]
		const auto n = n0 + n1 + n2;

		// n2 . n <= 0
		/// @todo dot_product_sign once we have exact vectors
		if ( la::inner(n2, n) <= 0 )
		{
			// Can reduce to (W[0],W[2])
			W[1] = W[2];
			reduce_line();
			return;
		}

		// n1 . n <= 0
		if ( la::inner(n1, n) <= 0 )
		{
			// Can reduce to (W[1], W[2])
			W[0] = W[1];
			W[1] = W[2];
			reduce_line();
			return;
		}

		const auto x = la::inner(n, W[0]);

		if ( x == 0 )
		{
			// Linearly dependent (and origin is not outside the triangle)
			dir_ = zero_initialized;
		}
		else if ( x < 0 )
		{
			// n is already pointing in the right direction
			dir_ = n;
		}
		else
		{
			// Flip the sign, since n is in the "wrong" direction
			dir_ = -n;
		}

		// Can't reduce to line, so keep the whole triangle
		nW = Triangle;
	}

	/* Reducing a tetrahedron to a triangle
	 *
	 * The addition of a fourth point creates three triangles. As proven above, W[3] must participate in the reduced
	 * simplex, so we must determine which of these triangles to keep. The test is actually fairly simple. Consider
	 * the triangle {W[0],W[1],W[3]} and it's normal n. WLOG suppose n.W[3] >= 0. Then, if n.W[2] > n.W[3], this is
	 * the plane to keep. Intuitively, this means that O and W[2] are on opposite sides of the plane defined by
	 * {W[0],W[1],W[3]}. Consequently, the closest point of the simplex to the origin must lie within that plane.
	 *
	 * There are two possible complications:
	 *   1) W[3] is coplanar with W[0],W[1],W[2]. The algorithm prevents this from ever happening (we would have
	 *      encountered a separating plane
	 *   2) If no such situation exists, then O is, in fact, in the tetrahedron
	 *
	 * It should be noted that n.W[3] above simplifies nicely:
	 *
	 *     n.W[3] = ((W[0]-W[3])x(W[1]-W[3])).W[3]
	 *            = (W[0]xW[1]+W[1]xW[3]+W[3]xW[0]).W[3]
	 *            = (W[0]xW[1]).W[3]+(W[1]xW[3]).W[3]+(W[3]xW[0]).W[3]
	 *            = (W[0]xW[1]).W[3]+0+0
	 *            = (W[0]xW[1]).W[3]
	 *
	 * Unfortunately, n.W[2] does not similarly simplify, but many of the terms can be kept for later if they are
	 * needed
	 */
	void reduce_tetrahedron()
	{
		// First check if we can reduce to {W[0],W[1],W[3]}
		{
			const auto n01 = cross(W[0], W[1]);
			const auto n13 = cross(W[1], W[3]);
			const auto n30 = cross(W[3], W[0]);

			const auto n013 = n01 + n13 + n30;

			// W[3].n013
			const auto x = n013[0] * W[3][0];

			// W[2].n013
			const auto y = la::inner(n013, W[2]);

			if ( ( 0 <= x && x <= y ) || ( y <= x && x <= 0 ) )
			{
				W[2] = W[3];
				reduce_triangle();
				return;
			}
		}

		// Now check { W[1],W[2],W[3] }
		{
			const auto n12 = cross(W[1], W[2]);
			const auto n23 = cross(W[2], W[3]);
			const auto n31 = cross(W[3], W[1]);

			const auto n123 = n12 + n23 + n31;

			// W[3].n123
			const auto x = n123[0] * W[3][0];

			// W[0].n123
			const auto y = la::inner(n123, W[0]);

			if ( ( 0 <= x && x <= y ) || ( y <= x && x <= 0 ) )
			{
				W[0] = W[1];
				W[1] = W[2];
				W[2] = W[3];
				reduce_triangle();
				return;
			}
		}

		// Finally, check { W[2], W[0], W[3] }
		{
			const auto n20 = cross(W[2], W[0]);
			const auto n03 = cross(W[0], W[3]);
			const auto n32 = cross(W[3], W[2]);

			const auto n203 = n20 + n03 + n32;

			// W[3].n203
			const auto x = n203[0] * W[3][0];

			// W[1].n203
			const auto y = la::inner(n203, W[1]);

			if ( ( 0 <= x && x <= y ) || ( y <= x && x <= 0 ) )
			{
				W[1] = W[2];
				W[2] = W[3];
				reduce_triangle();
				return;
			}
		}

		// Origin must be inside tetrahedron
		dir_ = zero_initialized;
		nW   = Tetrahedron;
	}

	void print(const char* msg) const
	{
		#ifdef DEBUG_MATH_GEOMETRY_INTERSECTION_COLLISIONS_PRINT
		std::cerr << std::endl;
		std::cerr << msg << std::endl;

		std::cerr << "dir = " << dir_ << std::endl;
		std::cerr << "dir length = " << dir_.length() << std::endl;

		for (std::size_t i = 0; i < nW; ++i)
		{
			std::cerr << "W[" << i << "] = " << W[i] << std::endl;
			std::cerr << "dir * W[" << i << "] = " << la::inner(dir_, W[i]) << std::endl;
		}

		std::cerr << "distance = " << distance_from_origin() << std::endl;
		#endif
	}

	void debug_reduce()
	{
		#ifdef DEBUG_MATH_GEOMETRY_INTERSECTION_COLLISIONS
		#ifdef DEBUG_MATH_GEOMETRY_INTERSECTION_COLLISIONS_PRINT
		std::cerr << std::endl;
		std::cerr << "Manually searching for minimum" << std::endl;
		print("Starting with:");
		#endif
		min  = INFINITY;
		nmin = 0;

		// Find the closest point
		for (std::size_t i = 0; i < nW; ++i)
		{
			if ( W[i].length() < min )
			{
				min     = W[i].length();
				mini[0] = i;
				nmin    = 1;
				#ifdef DEBUG_MATH_GEOMETRY_INTERSECTION_COLLISIONS_PRINT
				std::cerr << "New minimum " << i << ": " << W[i] << "; min = " << min << std::endl;
				#endif
			}
		}

		// Find the closest line
		for (std::size_t i = 0; i < nW; ++i)
		{
			for (std::size_t j = 0; j < i; ++j)
			{
				if ( is_acute(-W[i], ( W[j] - W[i] ) ) && is_acute(-W[j], ( W[i] - W[j] ) ) )
				{
					// perpendicular to O is in (W[i],W[j])
					const auto x = flat_closest_point_to_origin(W[i], W[j]);

					if ( x.length() < min )
					{
						min     = x.length();
						mini[0] = i;
						mini[1] = j;
						nmin    = 2;
						#ifdef DEBUG_MATH_GEOMETRY_INTERSECTION_COLLISIONS_PRINT
						std::cerr << "New minimum " << i << ": " << W[i] << "; " << j << ": "
						          << W[j] << "; min = " << min << std::endl;
						#endif
					}
				}
			}
		}

		// Find the closest plane
		for (std::size_t i = 0; i < nW; ++i)
		{
			for (std::size_t j = 0; j < i; ++j)
			{
				for (std::size_t k = 0; k < j; ++k)
				{
					const auto A = W[i];
					const auto B = W[j];
					const auto C = W[k];
					const auto n = flat_normal(W[i], W[j], W[k]);

					la::matrix< float, 3, 3 > X(uninitialized);

					for (std::size_t l = 0; l < 3; ++l)
					{
						X(l, 0) = ( A - B )[l];
						X(l, 1) = ( A - C )[l];
						X(l, 2) = n[l];
					}

					const auto                Y = la::invert(X);
					la::matrix< float, 3, 1 > Z(uninitialized);
					Z(0, 0) = A[0];
					Z(1, 0) = A[1];
					Z(2, 0) = A[2];

					const auto V = Y * Z;

					const auto t = V(0, 0);
					const auto u = V(1, 0);

					const auto P = A + t * ( B - A ) + u * ( C - A );

					/// @todo specifically P = V(2,0)*n
					/// @todo Extract a function to determine if two vectors are parallel (or anti parallel)
					if ( cross(P, n).length2() > NULL_VECTOR_TOLERANCE )
					{
						#ifdef DEBUG_MATH_GEOMETRY_INTERSECTION_COLLISIONS_PRINT
						std::cerr << P << std::endl;
						std::cerr << n << std::endl;
						std::cerr << cross(P, n).length2() << std::endl;
						#endif
						throw std::logic_error("P should be a multiple of n");
					}

					if ( 0 <= t && t <= 1 && 0 <= u && u <= 1 && t + u <= 1 )
					{
						// point is in the triangle
						if ( P.length() < min )
						{
							min     = P.length();
							mini[0] = i;
							mini[1] = j;
							mini[2] = k;
							nmin    = 3;
							#ifdef DEBUG_MATH_GEOMETRY_INTERSECTION_COLLISIONS_PRINT
							std::cerr << "New minimum " << i << ": " << W[i] << "; " << j
							          << ": " << W[j] << "; " << k << ": " << W[k]
							          << "; min = " << min << std::endl;
							std::cerr << "t = " << t << "; u = " << u << "; P = " << P
							          << "; n = " << n << std::endl;
							#endif
						}
					}
				}
			}
		}

		#endif // ifdef DEBUG_MATH_GEOMETRY_INTERSECTION_COLLISIONS
	}

	void debug_reduce_check()
	{
		#ifdef DEBUG_MATH_GEOMETRY_INTERSECTION_COLLISIONS

		for (int i = 0; i < nW; ++i)
		{
			if ( is_acute(dir_, W[i]) )
			{
				print("dir is pointing in the wrong direction");
				std::cerr << "Should be <= 0: " << la::inner(dir_, W[i]) << std::endl;
				throw std::logic_error("dir is pointing in the wrong direction");
			}
		}

		{
			auto mind = la::inner(dir_, W[0]);
			auto maxd = la::inner(dir_, W[0]);

			for (std::size_t i = 1; i < nW; ++i)
			{
				const auto x = la::inner(dir_, W[i]);

				if ( x < mind )
				{
					mind = x;
				}

				if ( x > maxd )
				{
					maxd = x;
				}
			}

			if ( maxd - mind > ORTHOGONALITY_TOLERANCE )
			{
				print("Normal does not fit the flat well");
				std::cerr << "min distance = " << mind << std::endl;
				std::cerr << "max distance = " << maxd << std::endl;
				std::cerr << "difference = " << ( maxd - mind ) << std::endl;
				std::cerr << "max allowable error = " << ( ORTHOGONALITY_TOLERANCE ) << std::endl;
				throw std::runtime_error("Normal does not fit the flat well");
			}
		}

		if ( nW == Triangle )
		{
			const auto n = flat_normal(W[0], W[1], W[2]);

			if ( is_acute(n, W[0]) || is_acute(n, W[1]) || is_acute(n, W[2]) )
			{
				print("Triangle has incorrect orientation");
				std::cerr << n << std::endl;

				std::cerr << "Should be greater than zero: " << la::inner_sign(n.raw(), W[0].raw() )
				          << std::endl;
				std::cerr << "Should be greater than zero: " << la::inner_sign(n.raw(), W[1].raw() )
				          << std::endl;
				std::cerr << "Should be greater than zero: " << la::inner_sign(n.raw(), W[2].raw() )
				          << std::endl;
				throw std::logic_error("Triangle has incorrect orientation");
			}
		}

		if ( nW != Tetrahedron
		     && ( nW != nmin || std::abs(min - distance_from_origin() ) > DISTANCE_TO_ORIGIN_TOLERANCE )
		)
		{
			#ifdef DEBUG_MATH_GEOMETRY_INTERSECTION_COLLISIONS_PRINT
			std::cerr << "Manually found min: " << min << std::endl;
			std::cerr << "Calculated min: " << distance_from_origin() << std::endl;
			std::cerr << "Error: " << std::abs(min - distance_from_origin() ) << std::endl;
			std::cerr << "Max allowable error: " << DISTANCE_TO_ORIGIN_TOLERANCE << std::endl;
			print("Using: ");
			throw std::runtime_error("Calculated error");
			#endif
		}

		/// @todo Check mini, too

		#endif // ifdef DEBUG_MATH_GEOMETRY_INTERSECTION_COLLISIONS
	}

	geo::point< dimension, real_type > dir_;

	/** @brief Points in the simplex
	 *
	 * Points are kept in order of oldest to newest
	 */
	std::array< point_type, dimension + 1 > W;
	simplex_type                            nW{ Empty };

	#ifdef DEBUG_MATH_GEOMETRY_INTERSECTION_COLLISIONS
	float       min = INFINITY;
	std::size_t mini[4];
	std::size_t nmin = 0;
	#endif
};

/** @brief Test two objects for collision/intersection
 *
 * The goal of GJK is to "surround" the origin of the Minowski difference. You
 * only need 4 points to do this, so it keeps track of as few points as
 * possible
 *
 * Reuse the GJK object with the same pair of objects to take advantage of
 * frame coherency.
 *
 * @invariant v is not null
 *
 * @todo Rather than an interface (IExtremum) template the type
 * @todo EPA - Expanding polytope algorithm
 * @todo If no collision, store distance or SAT so we can avoid calling this
 *       object until time T has passed.
 * @todo Template on dimension. Might need Gino van der Bergen's method for
 *       Cramers rule
 * @todo Collapse separating plane and v, since that plane makes a great initial
 *       search direction. Also, it saves us storage.
 * @todo We can easily search for a movement that would cause collision by
 *       tweaking the simplex with the motion vector. If the search direction is
 *       in the "same direction" as movement, every extremum of b is moved the
 *       same amount. Consequently, so is the simplex W. Once collision is
 *       found, keep moving b until O is just outside the simplex and then
 *       reduce the simplex. Maybe equivalently, this is like checking if the
 *       motion vector is inside the simplex instead of O (?)
 * @todo Maybe use Gram Schmidt method for generating subsequent v
 * @todo for moving objects, we want to find if simplex contains any point on
 *       the line ku, where u is the direction of motion and k is a scalar.
 *       Stationary objects can be considered a special case where u = (0,0,0)
 */
}

namespace
{
/**
 * @todo Build polytope to determine separation distance along motion vector
 *   (use same algorithm as EPA)
 * @todo Can we get away with extremum just returning distance?
 */
geo::intersection_result< 3 > gjk(
	const geo::Geometry&    a,
	const geo::affine< 3 >& aframe,
	const geo::Geometry&    b,
	const geo::affine< 3 >& bframe,
	float                   dist
)
{
	Simplex simplex;

	// In practice, after 25 iterations there will not be a collision
	/// @todo Arbitrary looping? I think this is to handle things like sphere/sphere intersection
	/// where we can walk the simplex indefinitely
	for (unsigned loop = 0; loop < 25; ++loop)
	{
		// Choose a search direction
		const geo::ray< 3 > v = simplex.dir();

		// Find the extremum of each object in that direction
		const auto wa = aframe(a.extremum(geo::support(aframe.invert(v) ) ) );
		const auto wb = bframe(b.extremum(geo::support(bframe.invert(-v) ) ) );
		const auto d1 = la::inner(topoint(v), wa);
		const auto d2 = la::inner(topoint(v), wb);

		if ( d1 < d2 )
		{
			return geo::separation_info< 3 >{ v, d1, d2 };
		}

		simplex.add(wa, wb);

		if ( simplex.distance_from_origin() <= dist )
		{
			/// @todo Determine contact features and normal
			/// http://mollyrocket.com/forums/viewtopic.php?t=438&sid=2cfe4cfd1602f0d3da5b0009c74f100e
			return geo::intersection_info< 3 >{ v };
		}
	}

	// There's no collision, but also SAT didn't work
	/// @bug Shouldn't this be treated as a collision? Weren't able to separate the objects
	return geo::separation_info< 3 >{ zero_initialized, 0, 0 };
}

// a is a plane, b is not
/// @todo Use bounding sphere to avoid extremum and affine::inverse calculations
/// @todo Simplify to avoid all the change of frames
geo::intersection_result< 3 > test_plane(
	const geo::plane< 3 >&  p,
	const geo::Geometry&    b,
	const geo::affine< 3 >& bframe
)
{
	#ifdef DEBUG_MATH_GEOMETRY_INTERSECTION_COLLISIONS
	#if 0
	const auto& face = a.face(0);
	const auto  p0   =
		make_plane( a.vertex(face.vertices[0]), a.vertex(face.vertices[1]), a.vertex(face.vertices[2]) );

	if ( p0.p != 0 || p0.n.p[0] != 1 || p0.n.p[1] != 0 || p0.n.p[2] != 0 )
	{
		throw std::logic_error("Plane is assumed to have a particular orientation, and pass through the origin");
	}

	#endif
	#endif

	// translate into b
	const auto nb = bframe.invert(p.n);

	// check the extremum and the reverse extremum if b crossed the plane
	const auto x1 = bframe(b.extremum(geo::support< 3 >(nb) ) );
	const auto d1 = la::inner(topoint(p.n), x1);

	if ( d1 < -p.p )
	{
		// There is a separating plane
		return geo::separation_info< 3 >{ -p.n, p.p, -d1 };
	}

	const auto x2 = bframe(b.extremum(geo::support< 3 >(-nb) ) );
	const auto d2 = la::inner(topoint(p.n), x2);

	if ( d2 > -p.p )
	{
		// There is a separating plane
		return geo::separation_info< 3 >{ p.n, -p.p, d2 };
	}

// object crosses plane
	#if 0

	// plane should be oriented so normal points "toward" b
	if ( d1 + p.p < -( d2 + p.p ) )
	{
		collinfo.interx.flip();
	}

	#endif

	return geo::intersection_info< 3 >{ p.n };
}

// a is a sphere, b is not
/// @todo Simply replace a with a point geometry, and call gjk with a distance of
/// a.physicalRadius()
geo::intersection_result< 3 > test_sphere(
	const geo::ball< 3 >&   b1,
	const geo::Geometry&    b,
	const geo::affine< 3 >& bframe
)
{
	Simplex simplex;

	// In practice, after 25 iterations there will not be a collision
	/// @todo Arbitrary looping? I think this is to handle things like sphere/sphere intersection
	/// where we can walk the simplex indefinitely
	for (unsigned loop = 0; loop < 25; ++loop)
	{
		const geo::ray< 3 > v = simplex.dir();

		const auto wa = b1.center() + topoint(v, b1.radius() );
		const auto wb = bframe(b.extremum(geo::support< 3 >(bframe.invert(-v) ) ) );

		const auto d1 = la::inner(topoint(v, 1), wa);
		const auto d2 = la::inner(topoint(v, 1), wb);

		if ( d1 < d2 )
		{
			return geo::separation_info< 3 >{ v, d1, d2 };
		}

		simplex.add(wa, wb);

		if ( simplex.contains_origin() )
		{
			/// @todo Determine contact features and normal
			/// http://mollyrocket.com/forums/viewtopic.php?t=438&sid=2cfe4cfd1602f0d3da5b0009c74f100e
			return geo::intersection_info< 3 >{ v };
		}
	}

	// There's no collision, but also SAT didn't work
	/// @bug Shouldn't this be treated as a collision? Weren't able to separate the objects
	return geo::separation_info< 3 >{ zero_initialized, 0, 0 };
}

geo::intersection_result< 3 > test_inner(
	const geo::Geometry&    a,
	const geo::affine< 3 >& aframe,
	const geo::Geometry&    b,
	const geo::affine< 3 >& bframe
)
{
	const auto h1 = a.get_hint();
	const auto h2 = b.get_hint();

	if ( h1 == geo::Plane )
	{
		if ( h2 != geo::Plane )
		{
			const geo::plane< 3 > p0 = { 0, {{ 1, 0, 0 }}};

			return test_plane(aframe(p0), b, bframe);
		}

		// both are planes
		/// @todo Maybe rethink this. At the moment, it seems a bit meaningless for planes to be intersecting
		/// It will make more sense we we are returning the features involved in the collision. In this case,
		/// we can return the line of intersection.
		return geo::separation_info< 3 >{ zero_initialized, 0, 0 };
	}

	if ( h2 == geo::Plane )
	{
		// Call self with arguments reversed. We'll hit the special cases above.
		auto res = test_inner(b, bframe, a, aframe);

		res.flip();
		return res;
	}

	if ( h1 == geo::Sphere )
	{
		const geo::ball< 3 > b1(a.physical_radius() );

		if ( b.get_hint() == geo::Sphere )
		{
			const geo::ball< 3 > b2(b.physical_radius() );
			return geo::test_intersect(aframe(b1), bframe(b2) );
		}

		return test_sphere(aframe(b1), b, bframe);
	}

	if ( h2 == geo::Sphere )
	{
		// Call self with arguments reversed. We'll hit the special cases above.
		auto res = test_inner(b, bframe, a, aframe);

		res.flip();
		return res;
	}

	return gjk(a, aframe, b, bframe, 0);
}

}

namespace geo
{
intersection_result< 3 > test_intersect(
	const Geometry&    a,
	const affine< 3 >& aframe,
	const Geometry&    b,
	const affine< 3 >& bframe
)
{
	// This function is just a hook to test the collision information. Forward
	// to real function
	const auto res = test_inner(a, aframe, b, bframe);

	#ifdef DEBUG_MATH_GEOMETRY_INTERSECTION_COLLISIONS
	#if 0

	// if separating plane, normal should point "toward" a
	if ( !res && collinfo.n.isValid() )
	{
		if ( !( collinfo.d1 < collinfo.d2 ) )
		{
			#ifdef DEBUG_MATH_GEOMETRY_INTERSECTION_COLLISIONS_PRINT
			const auto p1 = aframe( a.extremum( aframe.invert(collinfo.n) ) );
			const auto p2 = bframe( b.extremum( bframe.invert(-collinfo.n) ) );
			std::cerr << collinfo << std::endl;
			std::cerr << p1 << "; " << collinfo.n.topoint() * p1 << "; " << collinfo.d1 << std::endl;
			std::cerr << p2 << "; " << collinfo.n.topoint() * p2 << "; " << collinfo.d2 << std::endl;
			#endif
			test_inner(a, aframe, b, bframe);
			throw std::logic_error("Separating plane should place first object in positive half-space.");
		}
	}

	#endif
	#endif // ifdef DEBUG_MATH_GEOMETRY_INTERSECTION_COLLISIONS

	return res;
}

}
