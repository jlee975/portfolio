#ifndef MATH_GEOMETRY_INTERSECTION_INFO_H
#define MATH_GEOMETRY_INTERSECTION_INFO_H

#include "../primitives/ray.h"

#ifdef DEBUG_MATH_GEOMETRY
#define DEBUG_MATH_GEOMETRY_INTERSECTION
#endif

namespace geo
{
/// @todo separate collision into vertex-plane, edge-edge, etc.
enum intersection_type
{
	no_intersection,
	intersects
};

/// @bug We have no guarantee that d1,d2 were calculated with n (esp., not considering if n was normalized or not
/// at the time d1,d2 were calculated).
template< dimension_type D, typename R = float >
struct separation_info
{
	#ifdef DEBUG_MATH_GEOMETRY_INTERSECTION
	separation_info(const ray< D, R >& n_, R d1_, R d2_)
		: n(n_), d1(d1_), d2(d2_)
	{
		if ( d1 >= d2 )
		{
			throw std::logic_error("Values ordered incorrectly");
		}
	}

	#endif

	/// separating axis
	ray< D, R > n;

	/// max extent of first object along n
	R d1;

	/// max extend of second object along n
	R d2;

	separation_info flip() const
	{
		return { -n, -d2, -d1 };
	}

};

template< dimension_type D, typename R = float >
struct intersection_info
{
	ray< D, R > n;
};

/// @brief Information about a collision
/// @todo Feature information (faces, edges, vertices involved)
/// @todo Enforce d1 < d2 for a separating plane
/// @todo I think n, d1, d2 are unused when type != no_intersection. Probably want feature information instead
template< dimension_type D, typename R = float >
struct intersection_result
{
	intersection_result(separation_info< D, R > x)
		: type(no_intersection), separation_m(x)
	{
	}

	intersection_result(intersection_info< D, R > x)
		: type(intersects), intersection_m(x)
	{
	}

	intersection_type type; // collision (true) or separating plane (false)

	union
	{
		separation_info< D, R > separation_m;
		intersection_info< D, R > intersection_m;
	};

	void flip()
	{
		switch ( type )
		{
		case no_intersection:
			separation_m = separation_m.flip();
			break;
		default:
			break;
		}
	}

};
}

#endif // MATH_GEOMETRY_INTERSECTION_INFO_H
