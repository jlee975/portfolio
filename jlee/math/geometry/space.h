#ifndef MATH_GEOMETRY_SPACE_HPP
#define MATH_GEOMETRY_SPACE_HPP

#include "primitives/ball.h"
#include "primitives/box.h"
#include "primitives/line.h"
#include "primitives/plane.h"
#include "primitives/point.h"
#include "primitives/ray.h"

namespace geo
{
/// @todo possibly rename as vector space
template< dimension_type D, typename Real = float >
struct space
{
	static const dimension_type dimension = D;
	using real_type = Real;
	typedef point< D, Real > point_type;
	typedef line< D, Real > line_type;
	typedef ray< D, Real > ray_type;
	typedef plane< D, Real > plane_type;
	typedef ball< D, Real > ball_type;
	typedef box< D, Real > box_type;
};
}
#endif // MATH_GEOMETRY_SPACE_HPP
