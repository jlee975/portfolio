#ifndef MATH_GEOMETRY_PRINT_H
#define MATH_GEOMETRY_PRINT_H

#include <iostream>

#include "affine.h"
#include "intersection/intersection_result.h"
#include "primitives/plane.h"
#include "primitives/point.h"
#include "primitives/ray.h"

namespace geo
{
template< dimension_type D, typename Real >
std::ostream& operator<<(
	std::ostream&           os,
	const point< D, Real >& v
)
{
	os << "{ \"point\": [";

	for (dimension_type i = 0; i < D; ++i)
	{
		if ( i > 0 )
		{
			os << ", ";
		}

		os << v[i] << std::hexfloat << " (" << v[i] << ")" << std::defaultfloat;
	}

	os << "] }";
	return os;
}

template< dimension_type D, typename Real >
std::ostream& operator<<(
	std::ostream&         os,
	const ray< D, Real >& r
)
{
	os << "{ \"ray\": [";

	for (dimension_type i = 0; i < D; ++i)
	{
		if ( i > 0 )
		{
			os << ", ";
		}

		os << r.p[i] << std::hexfloat << " (" << r.p[i] << ")" << std::defaultfloat;
	}

	os << "] }";
	return os;
}

template< dimension_type D, typename Real >
std::ostream& operator<<(
	std::ostream&           os,
	const plane< D, Real >& p
)
{
	os << "{ type = plane; p = " << p.p << "; normal = " << p.n << "}";
	return os;
}

/** @brief Formatted output for an instance of an Affine
 */
template< dimension_type D, typename Real >
std::ostream& operator<<(
	std::ostream&            os,
	const affine< D, Real >& a
)
{
	os << "{ \"affine\": [";

	for (dimension_type i = 0; i < D; ++i)
	{
		os << "[";

		for (dimension_type j = 0; j < D; ++j)
		{
			os << a.data(i, j) << std::hexfloat << " (" << a.data(i, j) << std::defaultfloat << "),";
		}

		os << a.origin()[i] << std::hexfloat << " (" << a.origin()[i] << ")" << std::defaultfloat;
		os << "],";
	}

	os << "[";

	for (dimension_type j = 0; j < D; ++j)
	{
		os << "0,";
	}

	os << "1]";
	os << "] }";
	return os;
}

std::ostream& operator<<(std::ostream& os, intersection_type t);

template< dimension_type D, typename R >
std::ostream& operator<<(
	std::ostream&                      os,
	const intersection_result< D, R >& ci
)
{
	os << "{ \"collision\": {";
	os << "\"type\": " << ci.type << ", \"n\": " << ci.n << ", \"d1\": " << ci.d1 << ", \"d2\": " << ci.d2;
	os << "}}";
	return os;
}

}

#endif // MATH_GEOMETRY_PRINT_H
