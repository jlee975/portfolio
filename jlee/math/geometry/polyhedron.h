#ifndef MATH_GEOMETRY_POLYHEDRON_H
#define MATH_GEOMETRY_POLYHEDRON_H

#include <vector>

#include "algorithms/subdivide.h"
#include "primitives/box.h"
#include "primitives/face.h"
#include "primitives/geos.h"

namespace geo
{
/**
 * @brief The Polyhedron class
 *
 * @todo Template on dimensions and Real. Rename to polytope
 * @todo Convex hull
 * @todo Faces of arbitrary polygonal form
 * @todo Save some memory by using uint32_t instead of size_t
 * @todo geodesic polyhedron
 */
class Polyhedron
{
public:
	using vertex_type = point< 3 >;

	static Polyhedron create(geo_t);
	size_type number_of_vertices() const;
	const vertex_type& vertex(size_type) const;
	size_type number_of_faces() const;
	void subdivide(subdivision_t);

	const face_type< 3 >& face(size_type f) const;

	box< 3 > bounding_box() const;
	float radius() const;
private:
	/// @todo vertices, faces, edges should be designed in a way that supports
	/// a Graph class and related algorithms
	std::vector< vertex_type >    vertices;
	std::vector< face_type< 3 > > faces;
};
}
#endif // MATH_GEOMETRY_POLYHEDRON_H
