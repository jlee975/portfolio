#ifndef MATH_GEOMETRY_TRIANGLE_H
#define MATH_GEOMETRY_TRIANGLE_H

#include "primitives/point.h"

namespace geo
{
/// @todo General triangle categorization
enum class obtuse_cat
{
	SIDEA,
	SIDEB,
	SIDEC,
	NOSIDE
};

/// @brief Check if a triangle, with sides a, b, and c, has an obtuse angle
/// @todo Take triangle
/// @todo Only use hypot if we have to. Ex., if largest side < UPPER_BOUND we can probably use straight up
/// sqrt.
/// Undefined if any value is <= 0. Undefined if 3 values do not make a triangle (up to floating point error).
/// This version is careful to avoid overflow, but it is slower than is_obtuse_fast
template< typename Real >
constexpr obtuse_cat is_obtuse_accurate(
	Real a,
	Real b,
	Real c
)
{
	// This branch is careful to avoid overflow by using hypot
	if ( a >= b )
	{
		if ( a >= c )
		{
			// a >= b >= c
			// a >= c >= b
			return a >= std::hypot(b, c) ? obtuse_cat::SIDEA : obtuse_cat::NOSIDE;
		}
	}
	else
	{
		if ( b >= c )
		{
			// b >= a >= c
			// b >= c >= c
			return b >= std::hypot(a, c) ? obtuse_cat::SIDEB : obtuse_cat::NOSIDE;
		}
	}

	// c >= a >= b
	// c >= b >= a
	return c >= std::hypot(a, b) ? obtuse_cat::SIDEC : obtuse_cat::NOSIDE;
}

/// @brief Check if a triangle, with sides a, b, and c, has an obtuse angle
/// @todo Take triangle
/// @todo Maybe check a >= b, then check a*a > b*b+c*c === a*a-b*b>c*c === (a-b)*(a+b)>c*c
/// Undefined if any value is <= 0. Undefined if 3 values do not make a triangle (up to floating point error).
/// This version should usually work, but will overflow sooner than is_obtuse_accurate (it calculates x*x
/// for all sides x).
template< typename Real >
constexpr obtuse_cat is_obtuse_fast(
	Real a,
	Real b,
	Real c
)
{
	const Real a2 = a * a;
	const Real b2 = b * b;
	const Real c2 = c * c;

	if ( a2 > b2 + c2 )
	{
		return obtuse_cat::SIDEA;
	}

	if ( b2 > a2 + c2 )
	{
		return obtuse_cat::SIDEB;
	}

	if ( c2 > a2 + b2 )
	{
		return obtuse_cat::SIDEC;
	}

	return obtuse_cat::NOSIDE;
}

/// @brief Check if a triangle, with sides a, b, and c, has an obtuse angle
///
/// Undefined if any value is <= 0. Undefined if 3 values do not make a triangle (up to floating point error).
/// @todo Compile time flag to switch between accurate and fast versions
/// @todo Take triangle
template< typename Real >
constexpr obtuse_cat is_obtuse(
	Real a,
	Real b,
	Real c
)
{
	return is_obtuse_fast(a, b, c);
}

}

#endif // MATH_GEOMETRY_TRIANGLE_H
