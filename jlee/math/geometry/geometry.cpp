/// @todo Move named constructors to a ModelLibrary class
/// @todo Ensure the origin is the center of mass
/// @todo Orient so minimum bounding box (by volume) is axis aligned
#include "geometry.h"

#include <cfloat>
#include <cmath>
#include <forward_list>
#include <iostream>
#include <list>
#include <map>
#include <stdexcept>
#include <tuple>

#include "affine.h"
#include "intersection.h"
#include "intersection/test_geometry.h"
#include "primitives/angle.h"
#include "primitives/obb.h"

// A point is considered on a plane if its distance is <= COPLANARITY_TOLERANCE
// const int COPLANARITY_TOLERANCE = 0.0f;

namespace
{
/// @todo Make generic, D+1 dimensional to D dimensional. Spherical projection
geo::point< 2 > spherical_projection(const geo::point< 3 >& p)
{
	const geo::point< 2 > t = {{ static_cast< float >( std::atan2(p[1], p[0]) / ( 2. * M_PI ) + .5 ),
		( ( p[2] / p.length() ) + 1.f ) * 0.5f }};

	return t;
}

}

namespace geo
{
/// @todo For a convex object, faces/edges can be calculated by finding convex hull
Geometry::Geometry(geo_t g)
	: hint(g), bb(uninitialized), radius(0)
{
	poly = Polyhedron::create(g);

	switch ( g )
	{
	case Point:
		hint = Polytope;
		break;
	case Box:
		hint = Polytope;
		break;
	case Plane:
		hint = Plane;
		break;
	case Icosahedron:
		hint = Polytope;
		break;
	case Sphere:
		hint = Sphere;
		break;
	default:
		throw std::logic_error("Don't know how to create the requested object");
	} // switch

	geometry_changed();
}

/*
 *
 *  h strips, each strip will have 2w triangles
 *
 *  4x4 looks like:
 *     w
 *  ________
 *  \/\/\/\/\
 *  /\/\/\/\/    h
 *  \/\/\/\/\
 *  /\/\/\/\/
 *
 *
 * TriangleMesh* TriangleMesh::landscape()
 * {
 *  // hexagonal tiling. Randomly permute z
 *  ...
 * }
 */
std::vector< point< 2 > > Geometry::getSphericalCoords() const
{
	std::vector< point< 2 > > u;

	const size_type n = poly.number_of_vertices();

	u.reserve(n);

	for (size_type i = 0; i < n; ++i)
	{
		u.push_back(spherical_projection(poly.vertex(i) ) );
	}

	return u;
}

const point< 3 >& Geometry::vertex(size_type i) const
{
	return poly.vertex(i);
}

void Geometry::subdivide(subdivision_t s)
{
	poly.subdivide(s);
	geometry_changed();
}

size_type Geometry::num_faces() const
{
	return poly.number_of_faces();
}

const face_type< 3 >& Geometry::face(size_type f) const
{
	return poly.face(f);
}

const box< 3 >& Geometry::bounding_box() const
{
	return bb;
}

void Geometry::rebuild_separating_faces()
{
	/*
	   for ( size_type i = 0, n = faces.size(); i < n; ++i )
	   {
	    const size_type u = faces[i].u;
	    const size_type v = faces[i].v;
	    const size_type w = faces[i].w;

	    // Hessian normal form
	    const plane<3> q =
	    {
	        vertices[v],
	    perp1(vertices[v] - vertices[u], vertices[w] - vertices[v])
	    };

	    bool pos = false;
	    bool neg = false;

	    for ( size_type j = 0, m = vertices.size(); j < m; ++j )
	    {
	        if ( j != u && j != v && j != w )
	        {
	            const float d = q(vertices[j]);

	            if ( d > COPLANARITY_TOLERANCE )
	            {
	                pos = true;
	            }
	            else if ( d < -COPLANARITY_TOLERANCE )
	            {
	                neg = true;
	            }
	        }
	    }

	    // does face separate mesh?
	    if ( !pos || !neg )
	    {
	        // Get 2d axes of plane containing face i
	        const plane<3> YZ =
	        {
	            vertices[v], normalize( vertices[w] - vertices[v] )
	        };
	        const plane<3> XZ =
	        {
	        vertices[v], perp1(YZ.n, q.n)
	        };

	        std::vector< point<2> > proj;

	        // Project all points onto the plane of the face
	        for ( size_type j = 0, m = vertices.size(); j < m; ++j )
	        {
	            const point<2> t = { { YZ(vertices[j]), XZ(vertices[j]) } };
	            proj.push_back(t);
	        }

	        // Find the bounding circle
	        const circle c = bounding_circle(proj);

	        // Map the bounding circle back to 3d
	        const point< 3 > o = vertices[v] + YZ.n * c.center[0] + XZ.n * c.center[1];
	   //			faces[i].sepcenter = o;
	   //			faces[i].sepradius = c.r;
	    }
	    else
	    {
	   //			faces[i].sepradius = 0;
	    }
	   }
	 */
}

point< 3 > Geometry::extremum(const support< 3 >& f) const
{
	// Replace the support function according to the "true" object
	switch ( hint )
	{
	case Sphere:
		return f.sphere_extremum(radius);

	default:

		// The fallback for non-specialized objects
		/// @todo Walk edges instead of testing against every single vertex. Should be able
		/// to prove that we can go face to face until we start "backtracking" wrt u
		/// @todo For nodes that are very nearly as far as the current best, switch to doubles
		/// to be extra sure
		/// @todo When the extremum is a face (or edge), return the center of the face (or edge)
		if ( const size_type n = poly.number_of_vertices() )
		{
			size_type best = 0;

			for (size_type i = 1; i < n; ++i)
			{
				if ( f.better(poly.vertex(i), poly.vertex(best) ) )
				{
					best = i;
				}
			}

			return poly.vertex(best);
		}

		return zero_initialized;
	} // switch

}

float Geometry::physical_radius() const
{
	return radius;
}

void Geometry::geometry_changed()
{
	reorder_faces_and_vertices();
	rebuild_bounding_box();
	rebuild_separating_faces();

	// recalculate the physical radius
	radius = poly.radius();
}

void Geometry::reorder_faces_and_vertices()
{
	/// @todo Store vertices and faces in a cache-optimized order
}

void Geometry::rebuild_bounding_box()
{
	bb = poly.bounding_box();
}

}
