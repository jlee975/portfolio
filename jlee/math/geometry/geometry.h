/**
 * @todo Attach normals and colours to vertices
 */

#ifndef MATH_GEOMETRY_GEOMETRY_HPP
#define MATH_GEOMETRY_GEOMETRY_HPP

#include <array>
#include <memory>
#include <vector>

#include "affine.h"
#include "algorithms/subdivide.h"
#include "algorithms/support.h"
#include "intersection/intersection_result.h"
#include "polyhedron.h"

namespace geo
{
/** @brief An approximation of a 3D object using triangle faces
 *
 * @todo Since we are always using shared_ptr< Geometry > we may as well have Geometry be a base
 * class and have derived classes like Sphere, Plane, Polytope. We'd be switching on the type
 * a lot anyway (ex., extremum, get_hint, the different storage requirements)
 * @todo Maybe template on faces of 3 vertices, 4 vertices
 * @todo extract a point set class
 * @todo template on dimension, and real type
 * @todo Implement as a wrapper to shared data
 * @todo Meets requirements of a planar graph
 * @todo Treat as true geometry (ex., sphere) that can return the surface as
 * a triangle mesh
 * @todo Often used within a shared pointer. Probably want non-const functions to effectively
 * be copy-on-write. So we don't modify someone else's geometry
 */
class Geometry
{
public:
	explicit Geometry(geo_t);

	Geometry(const Geometry&)            = delete;
	Geometry(Geometry&&)                 = delete;
	Geometry& operator=(const Geometry&) = delete;
	Geometry& operator==(Geometry&&)     = delete;

	// Creation and editing
	void subdivide(subdivision_t);

	// properties
	size_type num_faces() const;

	// bounds testing
	/// @todo Want a function that returns extrema in directions v and -v. Often need
	/// both, and should be able to calculate at the same time
	point< 3 > extremum(const support< 3 >&) const;
	const box< 3 >& bounding_box() const;

	geo_t get_hint() const
	{
		return hint;
	}

	/** This is the radius of the sphere centered at (0,0,0) that entirely
	 * contains all the vertices of this object. Note that since the origin is
	 * taken to be the center of mass, this radius encompasses all orientations
	 * of *this. For example, it can be used to check the distance between two
	 * trajectories to see if objects collide.
	 */
	float physical_radius() const;
	// intersection_result< 3 > collision(const TriangleMesh&, const affine< 3, float>&, const affine< 3, float>&) const;

	// Projection
	/// @todo Rename to projectSpherical or something
	std::vector< point< 2 > > getSphericalCoords() const;

	// Drawing
	const point< 3 >& vertex(size_type) const;
	const face_type< 3 >& face(size_type) const;
private:
	void geometry_changed();
	void rebuild_bounding_box();
	void rebuild_separating_faces();
	void reorder_faces_and_vertices();

	Polyhedron poly;

	geo_t hint; ///< The shape the mesh is approximating

	// Cached information useful for rendering
	box< 3 > bb;
	float    radius;
};
}
#endif // ifndef MATH_GEOMETRY_GEOMETRY_HPP
