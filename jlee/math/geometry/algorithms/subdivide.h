/// @todo Can subdivision work on other faces besides triangles? I think
/// Catmull Clark can
#ifndef MATH_GEOMETRY_ALGORITHMS_SUBDIVIDE_H
#define MATH_GEOMETRY_ALGORITHMS_SUBDIVIDE_H

#include "../primitives/face.h"
#include "../primitives/point.h"
#include <vector>

namespace geo
{
enum subdivision_t
{
	Loop,
	Root3
};

/// @todo Take iterators that allow traversal of the vertices and faces
std::pair< std::vector< point< 3 > >, std::vector< face_type< 3 > > > Root3Subdivision(const std::vector< point< 3 > >&, const std::vector< face_type< 3 > >&);
std::pair< std::vector< point< 3 > >, std::vector< face_type< 3 > > > LoopSubdivision(const std::vector< point< 3 > >&, const std::vector< face_type< 3 > >&);
}

#endif // MATH_GEOMETRY_ALGORITHMS_SUBDIVIDE_H
