#include "subdivide.h"

#include <map>

#include "../primitives/size_type.h"

namespace
{
/// @todo Pull this from a graph class
struct edge_type
{
	geo::size_type u;

	geo::size_type v;

	bool operator<(const edge_type& e) const
	{
		geo::size_type x = std::min(u, v);
		geo::size_type y = std::min(e.u, e.v);

		return ( x < y ) || ( ( x == y ) && ( u + ( v - x ) < e.u + ( e.v - y ) ) );
	}

};

/// Given f= UVW and XWV then this returns V.. the common point that is
/// "first" wrt clockwise order
geo::size_type first_cw(
	const geo::face_type< 3 >& f,
	const geo::face_type< 3 >& g
)
{
	geo::size_type i = 0;

	// Find U, the point in f that is not in g
	while ( i < 3
	        && ( f.vertices[i] == g.vertices[0] || f.vertices[i] == g.vertices[1]
	             || f.vertices[i] == g.vertices[2] )
	)
	{
		++i;
	}

	if ( i == 3 )
	{
		throw std::runtime_error("blah called on self");
	}

	// Return V (the vertex after U)
	return i == 2 ? f.vertices[0] : f.vertices[i + 1];
}

/// @todo Use table lookup for first 10 values or so
float Root3Beta(int n)
{
	return static_cast< float >( ( 4. - 2. * std::cos(2. * std::numbers::pi / n) ) / ( 9. * n ) );
}

// Used in calculating loop subdivision
/// @todo Use table lookup for first 10 values or so
/// @note LoopBeta(6) == 1/16
float LoopBeta(int n)
{
	const double x = 3. + 2. * std::cos(2. * std::numbers::pi / n);

	return static_cast< float >( ( 40. - x * x ) / ( 64. * n ) );
}

}

namespace geo
{
std::pair< std::vector< point< 3 > >, std::vector< face_type< 3 > > > Root3Subdivision(
	const std::vector< point< 3 > >&     vertices,
	const std::vector< face_type< 3 > >& faces
)
{
	// edge -> (face 1, face 2)
	typedef std::map< edge_type, std::pair< size_type, size_type > > Edgelist;

	/// @todo special container that does not initialize elements of vector of points
	std::vector< point< 3 > > pert(vertices.size() + faces.size(), zero_initialized);
	std::vector< int >        degree(vertices.size(), 0);

	Edgelist edges;

	// perturb original points and add new points
	for (size_type i = 0; i < faces.size(); ++i)
	{
		const size_type u = faces[i].vertices[0];
		const size_type v = faces[i].vertices[1];
		const size_type w = faces[i].vertices[2];

		pert[u]                  += vertices[v];
		pert[v]                  += vertices[w];
		pert[w]                  += vertices[u];
		degree[u]                += 1;
		degree[v]                += 1;
		degree[w]                += 1;
		pert[vertices.size() + i] = ( vertices[u] + vertices[v] + vertices[w] ) * ( 1.f / 3.f );

		Edgelist::iterator it;

		const edge_type a = { u, v };
		it = edges.find(a);

		if ( it == edges.end() )
		{
			edges.emplace(a, std::make_pair(i, i) );
		}
		else
		{
			it->second.first = i;
		}

		const edge_type b = { v, w };
		it = edges.find(b);

		if ( it == edges.end() )
		{
			edges.emplace(b, std::make_pair(i, i) );
		}
		else
		{
			it->second.first = i;
		}

		const edge_type c = { w, u };
		it = edges.find(c);

		if ( it == edges.end() )
		{
			edges.emplace(c, std::make_pair(i, i) );
		}
		else
		{
			it->second.first = i;
		}
	}

	for (size_type i = 0; i < vertices.size(); ++i)
	{
		float b = Root3Beta(degree[i]);

		pert[i] *= b;
		pert[i] += vertices[i] * ( 1.f - static_cast< float >( degree[i] ) * b );
	}

	// redraw faces
	// each edge creates two new faces, one for each of the faces that
	// share it
	/// @bug Watch for edges that only have one face
	std::vector< face_type< 3 > > newfaces;

	for (const auto& p : edges)
	{
		const size_type f = p.second.first;
		const size_type g = p.second.second;
		const size_type x = vertices.size() + f;
		const size_type y = vertices.size() + g;

		const face_type< 3 > t1 = {{ first_cw(faces[f], faces[g]), y, x }};
		const face_type< 3 > t2 = {{ first_cw(faces[g], faces[f]), x, y }};
		newfaces.push_back(t1);
		newfaces.push_back(t2);
	}

	return { std::move(pert), std::move(newfaces) };
}

/**
 * @bug Must only be used on closed surfaces. Throw if used on open surface
 * @todo Precalculate beta(n) for n = 2..100 or something. Note in particular
 *       that beta(6) = 1/16, and the majority of degrees will be 1/16. Store
 *       m, the original number of vertices. Any vertex past m will have degree
 *       6.
 * @todo Compute normals
 */
std::pair< std::vector< point< 3 > >, std::vector< face_type< 3 > > > LoopSubdivision(
	const std::vector< point< 3 > >&     vertices,
	const std::vector< face_type< 3 > >& faces
)
{
	typedef std::map< edge_type, std::pair< point< 3 >, size_type > > Edgelist;

	// list all edges
	Edgelist edgelist;

	/// @todo structured bindings?
	for (const auto& f : faces)
	{
		const size_type u = f.vertices[0];
		const size_type v = f.vertices[1];
		const size_type w = f.vertices[2];

		{
			const auto p  = ( vertices[w] + ( vertices[u] + vertices[v] ) * 1.5f ) * .125f;
			auto       it = edgelist.find(edge_type{ u, v });

			if ( it != edgelist.end() )
			{
				it->second.first += p;
			}
			else
			{
				edgelist.emplace(edge_type{ u, v }, std::make_pair(p, 0) );
			}
		}
		{
			const auto p  = ( vertices[u] + ( vertices[v] + vertices[w] ) * 1.5f ) * .125f;
			auto       it = edgelist.find(edge_type{ v, w });

			if ( it != edgelist.end() )
			{
				it->second.first += p;
			}
			else
			{
				edgelist.emplace(edge_type{ v, w }, std::make_pair(p, 0) );
			}
		}
		{
			const auto p  = ( vertices[v] + ( vertices[w] + vertices[u] ) * 1.5f ) * .125f;
			auto       it = edgelist.find(edge_type{ w, u });

			if ( it != edgelist.end() )
			{
				it->second.first += p;
			}
			else
			{
				edgelist.emplace(edge_type{ w, u }, std::make_pair(p, 0) );
			}
		}
	}

	/// @todo Avoid initialization cost
	std::vector< point< 3 > > pert(vertices.size(), zero_initialized);
	std::vector< int >        degree(vertices.size() );

	// perturb existing points
	for (const auto& it : edgelist)
	{
		size_type u = it.first.u;

		size_type v = it.first.v;
		pert[u] += vertices[v];
		degree[u]++;
		pert[v] += vertices[u];
		degree[v]++;
	}

	for (size_type i = 0; i < vertices.size(); ++i)
	{
		float b = LoopBeta(degree[i]);
		pert[i] *= b;
		pert[i] += vertices[i] * ( 1.f - static_cast< float >( degree[i] ) * b );
	}

	// calculate new points along edges
	/// @todo Could be folded into the original edgelist construction
	for (auto& p : edgelist)
	{
		p.second.second = pert.size();
		pert.push_back(p.second.first);
	}

	// replace all faces
	std::vector< face_type< 3 > > newfaces;

	for (const auto& f : faces)
	{
		const size_type u  = f.vertices[0];
		const size_type v  = f.vertices[1];
		const size_type w  = f.vertices[2];
		size_type       uv = edgelist.at(edge_type{ u, v }).second;
		size_type       vw = edgelist.at(edge_type{ v, w }).second;
		size_type       wu = edgelist.at(edge_type{ w, u }).second;

		const face_type< 3 > t1 = {{ u, uv, wu }};
		const face_type< 3 > t2 = {{ v, vw, uv }};
		const face_type< 3 > t3 = {{ w, wu, vw }};
		const face_type< 3 > t4 = {{ uv, vw, wu }};

		newfaces.push_back(t1);
		newfaces.push_back(t2);
		newfaces.push_back(t3);
		newfaces.push_back(t4);
	}

	return { std::move(pert), std::move(newfaces) };
}

}
