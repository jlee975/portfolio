#ifndef MATH_GEOMETRY_ALGORITHMS_BOUNDING_BALL_HPP
#define MATH_GEOMETRY_ALGORITHMS_BOUNDING_BALL_HPP

#include <algorithm>
#include <random>
#include <vector>

#include "../primitives/ball.h"
#include "../primitives/point.h"
#include "../primitives/size_type.h"

namespace geo
{
/** Welzl's algorithm for calculating a bounding circle
 */
namespace detail
{
/// @todo Take r as pointer to points
/// @todo r2 should be constructed in place
/// @todo Prove r has 3 or fewer elements so that we can simplify the logic
/// @todo Remove the vectors
/// @todo There are specialized algorithms for calculating incircle(A,B,C,X) where A,B,C are points
/// that determine a circle, and X is the point being queried. https://www.cs.cmu.edu/~quake/robust.html
template< typename Real >
circle< Real > b_minidisk(
	const std::vector< point< 2, Real > >& p,
	size_type                              n,
	const std::vector< point< 2, Real > >& r
)
{
	if ( n == 0 || r.size() == 3 )
	{
		circle< Real > d = {{{ 0, 0 }}, 0 };

		if ( r.size() == 0 )
		{
		}
		else if ( r.size() == 1 )
		{
			d.center = r[0];
		}
		else if ( r.size() == 2 )
		{
			d.center = ( r[0] + r[1] ) * 0.5f;
			d.r      = ( r[0] - r[1] ).length() * 0.5f;
		}
		else
		{
			// r.size() == 3
			// lengths of the 3 edges
			const Real a = ( r[0] - r[1] ).length();
			const Real b = ( r[1] - r[2] ).length();
			const Real c = ( r[2] - r[0] ).length();

			/// @todo This call to is_obtuse squares a,b,c, which we effectively would have had
			/// calling length
			/// @todo May want a version of is_obtuse where we call with the points and we can
			/// use a more accurate implementation
			switch ( is_obtuse(a, b, c) )
			{
			case obtuse_cat::SIDEA:
				d.center = ( r[0] + r[1] ) * 0.5f;
				d.r      = a * 0.5f;
				break;
			case obtuse_cat::SIDEB:
				d.center = ( r[1] + r[2] ) * .5f;
				d.r      = b * 0.5f;
				break;
			case obtuse_cat::SIDEC:
				d.center = ( r[2] + r[0] ) * .5f;
				d.r      = c * 0.5f;
				break;
			default:
				d = circumscribed_circle(r[0], r[1], r[2], a, b, c);
				break;
			}
		}

		return d;
	}

	const circle< Real > d = b_minidisk(p, n - 1, r);

	if ( ( p[n - 1] - d.center ).length() <= d.r )
	{
		return d;
	}

	std::vector< point< 2, Real > > r2 = r;

	r2.push_back(p[n - 1]);
	return b_minidisk(p, n - 1, r2);
}

}

/** Find the circle of smallest radius covering all the given points
 *
 * An implementation of Welzl's algorithm. Basically picks three points and finds the circumscribed circle.
 * Then expands it intelligently to include more points outside that circle.
 *
 * @todo bounding_sphere
 * @todo Eliminate the copying
 * @todo Don't need to hit the random device
 * @todo Take any container of points
 */
template< typename Real >
circle< Real > bounding_circle(const std::vector< point< 2, Real > >& p_)
{
	circle< Real > c = {{{ 0, 0 }}, 0 };

	if ( !p_.empty() )
	{
		std::random_device         rd;
		std::default_random_engine g(rd() );

		/// @todo Shuffle copy
		std::vector< point< 2, Real > > p = p_;
		std::shuffle(p.begin(), p.end(), g);

		std::vector< point< 2, Real > > r;

		c = detail::b_minidisk(p, p.size(), r);
	}

	return c;
}

}

#endif // MATH_GEOMETRY_ALGORITHMS_BOUNDING_BALL_HPP
