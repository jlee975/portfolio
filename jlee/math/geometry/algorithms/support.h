#ifndef SUPPORT_H
#define SUPPORT_H

#include "../primitives/ray.h"

namespace geo
{
template< dimension_type D, typename Real = float >
class support
{
public:
	explicit support(const ray< D, Real >& u_)
		: u(u_)
	{
	}

	point< D, Real > sphere_extremum(Real r) const
	{
		return topoint(u, r);
	}

	bool better(
		const point< D, Real >& v,
		const point< D, Real >& old
	) const
	{
		return projected_scale(u, v) > projected_scale(u, old);
	}
private:
	ray< D, Real > u;
};
}

#endif // SUPPORT_H
