#include "polyhedron.h"

#include <array>
#include <cmath>
#include <map>

#include "algorithms/subdivide.h"

const std::array< geo::point< 3 >, 1 > pointv = {{{{ 0.0f, 0.0f, 0.0f }}}};

constexpr std::array< geo::face_type< 3 >, 0 > pointf = {{}};

const std::array< geo::point< 3 >, 8 > boxv = {{{{ 1.0f, 1.0f, 1.0f }},
	{{ 1.0f, 1.0f, -1.0f }},
	{{ 1.0f, -1.0f, 1.0f }},
	{{ 1.0f, -1.0f, -1.0f }},
	{{ -1.0f, 1.0f, 1.0f }},
	{{ -1.0f, 1.0f, -1.0f }},
	{{ -1.0f, -1.0f, 1.0f }},
	{{ -1.0f, -1.0f, -1.0f }}}};

constexpr std::array< geo::face_type< 3 >, 12 > boxf = {{{{ 6, 2, 3 }},
	{{ 6, 3, 7 }},
	{{ 2, 0, 1 }},
	{{ 2, 1, 3 }},
	{{ 4, 6, 5 }},
	{{ 6, 7, 5 }},
	{{ 4, 2, 6 }},
	{{ 4, 0, 2 }},
	{{ 5, 7, 3 }},
	{{ 5, 3, 1 }},
	{{ 1, 0, 4 }},
	{{ 1, 4, 5 }}}};

/// @bug Plane is "large", but not infinite
const std::array< geo::point< 3 >, 4 > planev
    = {{{{ 0, -1024, -1024 }}, {{ 0, 1024, -1024 }}, {{ 0, 1024, 1024 }}, {{ 0, -1024, 1024 }}}};

constexpr std::array< geo::face_type< 3 >, 2 > planef = {{{{ 0, 1, 2 }}, {{ 2, 3, 0 }}}};

const std::array< geo::point< 3 >, 12 > icov = {{{{ 0.f, 1.f, std::numbers::phi_v< float >}},
	{{ 0.f, 1.f, -std::numbers::phi_v< float >}},
	{{ 0.f, -1.f, -std::numbers::phi_v< float >}},
	{{ 0.f, -1.f, std::numbers::phi_v< float >}},
	{{ 1.f, std::numbers::phi_v< float >, 0.f }},
	{{ 1.f, -std::numbers::phi_v< float >, 0.f }},
	{{ -1.f, -std::numbers::phi_v< float >, 0.f }},
	{{ -1.f, std::numbers::phi_v< float >, 0.f }},
	{{ std::numbers::phi_v< float >, 0.f, 1.f }},
	{{ std::numbers::phi_v< float >, 0.f, -1.f }},
	{{ -std::numbers::phi_v< float >, 0.f, -1.f }},
	{{ -std::numbers::phi_v< float >, 0.f, 1.f }}}};

constexpr std::array< geo::face_type< 3 >, 20 > icof
    = {{{{ 3, 0, 8 }}, {{ 3, 8, 5 }}, {{ 3, 5, 6 }}, {{ 3, 6, 11 }}, {{ 3, 11, 0 }},
	{{ 2, 1, 10 }}, {{ 2, 10, 6 }}, {{ 2, 6, 5 }}, {{ 2, 5, 9 }}, {{ 2, 9, 1 }},
	{{ 5, 8, 9 }}, {{ 6, 10, 11 }}, {{ 4, 9, 8 }}, {{ 7, 11, 10 }}, {{ 0, 4, 8 }},
	{{ 0, 7, 4 }}, {{ 0, 11, 7 }}, {{ 4, 1, 9 }}, {{ 7, 1, 4 }}, {{ 1, 7, 10 }}}};

namespace geo
{
Polyhedron Polyhedron::create(geo_t g)
{
	Polyhedron p;

	switch ( g )
	{
	case Point:
		p.vertices.assign(std::begin(pointv), std::end(pointv) );
		p.faces.assign(std::begin(pointf), std::end(pointf) );
		break;
	case Box:
		p.vertices.assign(std::begin(boxv), std::end(boxv) );
		p.faces.assign(std::begin(boxf), std::end(boxf) );
		break;
	case Plane:
		p.vertices.assign(std::begin(planev), std::end(planev) );
		p.faces.assign(std::begin(planef), std::end(planef) );
		break;
	case Icosahedron:
		p.vertices.assign(std::begin(icov), std::end(icov) );
		p.faces.assign(std::begin(icof), std::end(icof) );
		break;
	case Sphere:
		p.vertices.assign(std::begin(icov), std::end(icov) );
		p.faces.assign(std::begin(icof), std::end(icof) );

		for (int i = 0; i < 2; ++i)
		{
			p.subdivide(Root3);
		}

		for (auto& v : p.vertices)
		{
			v = la::normalize(v);
		}

		break;
	default:
		throw std::logic_error("Don't know how to create the requested object");
	} // switch

	return p;
}

size_type Polyhedron::number_of_vertices() const
{
	return vertices.size();
}

const Polyhedron::vertex_type& Polyhedron::vertex(size_type i) const
{
	return vertices[i];
}

size_type Polyhedron::number_of_faces() const
{
	return faces.size();
}

void Polyhedron::subdivide(subdivision_t t)
{
	switch ( t )
	{
	case Loop:
	{
		auto [v, f] = geo::LoopSubdivision(vertices, faces);
		vertices    = std::move(v);
		faces       = std::move(f);
	}
	break;
	case Root3:
	{
		auto [v, f] = geo::Root3Subdivision(vertices, faces);
		vertices    = std::move(v);
		faces       = std::move(f);
	}
	break;
	}
}

const face_type< 3 >& Polyhedron::face(size_type f) const
{
	return faces[f];
}

geo::box< 3 > Polyhedron::bounding_box() const
{
	geo::box< 3 > bb(uninitialized);

	// Determine the bounding box
	if ( !vertices.empty() )
	{
		bb.min = vertices[0];
		bb.max = vertices[0];

		for (size_type i = 1, n = vertices.size(); i < n; ++i)
		{
			bb.min[0] = std::min(vertices[i][0], bb.min[0]);
			bb.max[0] = std::max(vertices[i][0], bb.max[0]);
			bb.min[1] = std::min(vertices[i][1], bb.min[1]);
			bb.max[1] = std::max(vertices[i][1], bb.max[1]);
			bb.min[2] = std::min(vertices[i][2], bb.min[2]);
			bb.max[2] = std::max(vertices[i][2], bb.max[2]);
		}
	}
	else
	{
		bb = zero_initialized;
	}

	return bb;
}

float Polyhedron::radius() const
{
	// recalculate the physical radius
	float maxr = 0;

	for (const geo::point< 3 >& p : vertices)
	{
		maxr = std::max(p.length2(), maxr);
	}

	return std::sqrt(maxr);
}

}
