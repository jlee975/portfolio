#ifndef MATH_GEOMETRY_PRIMITIVES_PLANE_HPP
#define MATH_GEOMETRY_PRIMITIVES_PLANE_HPP

#include "ray.h"

constexpr float UNIT_RAY_TOLERANCE = 1e-6f;

namespace geo
{
/** @brief Describes a plane in Hessian Normal Form
 *
 * This structure encapsulates a plane in an D-dimensional space. From a
 * mathematical perspective, this is better called a hyperplane. Formally, this
 * structure describes the set of points that satisfy x * n + p = 0, given a
 * normal vector n, and a constant p.
 *
 * In R3, a hyperplane is the usual "plane". In 2D, it is a line.
 *
 * A plane can be degenerate if the normal is the zero vector. This is done
 * intentionally in some cases (to signal error, failure, etc.)
 *
 * @note n should be a unit vector, if you are constructing this explicitly. IF
 * you instantiate a plane with a non-unit vector, behavior is undefined. It is
 * best to use the make_plane functions to create a plane.
 *
 * @todo Static constructors from point-point-point
 * @todo Might want to use unitray class
 * @todo Dihedral angle
 * @todo N point form of a plane (i.e., a flat)
 */
template< dimension_type D, typename Real = float >
struct plane
{
	using real_type = Real;
	static const dimension_type dimension = D;

	static const plane INVALID;

	Real p;           ///< Distance of plane to origin
	ray< D, Real > n; ///< Unit normal

	bool isValid() const
	{
		Real x = 0;

		for (dimension_type i = 0; i < D; ++i)
		{
			x += n.p[i] * n.p[i];
		}

		return std::abs(1 - std::sqrt(x) ) < UNIT_RAY_TOLERANCE;
	}

	// distance of given point to the plane
	constexpr Real operator()(const point< D >& v) const
	{
		return projected_scale(n, v) + p;
	}

	/** Distance that v displaces the plane. This can be though of in a
	 * a number of ways:
	 *  - the distance from v to the plane if the plane passed through O
	 *  - the (relative) distance of q+v to the plane for q in v
	 *  - a cheap way to calculation v * n
	 * @todo Think this can be named "rejection" (of v off of the plane)
	 */
	constexpr Real displacement(const point< D >& v) const
	{
		return projected_scale(n, v);
	}

	// return perpendicular foot of point (specialized to O)
	/// @todo Provide general value
	/// @todo Maybe rename projection
	point< D, Real > foot() const
	{
		point< D, Real > q(uninitialized);

		for (dimension_type i = 0; i < D; ++i)
		{
			q[i] = -p * n[i];
		}

		return q;
	}

	// flip the orientation of the plane
	void flip()
	{
		p *= -1;
		n  = -n;
	}

};

/// @todo Special geo::initialize type with appropriate constructor (and, of course,
/// geo::uninitialized)
template< dimension_type D, typename Real >
const plane< D, Real > plane< D, Real >::INVALID = { 0, {{ 0, 0, 0 }}};

// Make plane from point and normal
template< dimension_type D, typename Real >
constexpr plane< D, Real > make_plane(
	const point< D, Real >& p,
	const ray< D, Real >&   n
)
{
	plane< D, Real > t = { 0, {{ 1, 0, 0 }}};

	const Real mag = 1 / la::pnorm2(n.raw() );

	Real dot = 0;

	for (dimension_type i = 0; i < D; ++i)
	{
		const Real x = n[i] * mag;
		t.n[i] = x;
		dot   -= p[i] * x;
	}

	t.p = dot;

	return t;
}

/// @todo For any number of dimensions
/// Points are clockwise (a,b,c) and normal points out from the clock at the person looking at it
template< typename Real >
constexpr plane< 3, Real > make_plane(
	const point< 3, Real >& a,
	const point< 3, Real >& b,
	const point< 3, Real >& c
)
{
	auto n = flat_normal1(a, b, c);

	plane< 3, Real > t = { 0, {{ 1, 0, 0 }}};

	Real dot = 0;

	for (dimension_type i = 0; i < 3; ++i)
	{
		const Real x = n[i];
		t.n.p[i] = x;
		dot     -= a[i] * x;
	}

	t.p = dot;

	return t;
}

/// @note The distance can be negative, depending on the order the points are given
/// @todo Accept plane as argument
template< typename Real >
constexpr Real distance_from_origin(
	const point< 3, Real >& a,
	const point< 3, Real >& b,
	const point< 3, Real >& c
)
{
	/// @todo Maybe calculated via scalar_triple_product
	auto n = flat_normal(a, b, c);

	return -( ( n * a ) / n.length() );
}

}
#endif // MATH_GEOMETRY_PRIMITIVES_PLANE_HPP
