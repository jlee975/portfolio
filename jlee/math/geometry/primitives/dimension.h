#ifndef MATH_GEOMETRY_PRIMITIVES_DIMENSION_H
#define MATH_GEOMETRY_PRIMITIVES_DIMENSION_H

#include <cstdint>

namespace geo
{
/// @todo Throughout the project, be careful to use this type, esp. to save space and avoid
/// conversion to size_t, for example.
using dimension_type = std::uint_least16_t;
}

#endif // MATH_GEOMETRY_PRIMITIVES_DIMENSION_H
