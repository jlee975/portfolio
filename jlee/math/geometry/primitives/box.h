#ifndef MATH_GEOMETRY_PRIMITIVES_BOX_HPP
#define MATH_GEOMETRY_PRIMITIVES_BOX_HPP

#include "point.h"

namespace geo
{
/** The cartesian product of intervals. A right rectangular prism aligned to
 * the axes (aka, axis aligned bounding box)
 *
 * @todo Use intervals instead of points "interval_t dims[D]". Enforce the
 *    min/max assumption
 * @todo typedef aabb (axis-aligned bounding box)
 */
template< dimension_type D, typename Real = float >
struct box
{
	using real_type = Real;
	static const dimension_type dimension = D;

	explicit box(uninitialized_type x)
		: min(x), max(x)
	{
	}

	constexpr box(zero_initialized_type x)
		: min(x), max(x)
	{
	}

	constexpr box(const point< D, Real >& min_, const point< D, Real >& max_)
		: min(min_), max(max_)
	{
	}

	point< D, Real > min;
	point< D, Real > max;
};
}

#endif // MATH_GEOMETRY_PRIMITIVES_BOX_HPP
