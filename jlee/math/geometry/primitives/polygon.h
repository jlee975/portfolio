#ifndef MATH_GEOMETRY_PRIMITIVES_POLYGON_HPP
#define MATH_GEOMETRY_PRIMITIVES_POLYGON_HPP

#include "point.h"
#include "size_type.h"

namespace geo
{
template< size_type N, dimension_type D, typename Real = float >
struct polygon
{
	using real_type = Real;
	static const dimension_type dimension = D;
	static const size_type degree         = N;

	static_assert(N > 2, "A polygon has 3 or more points");

	point< D, Real > v[N];

	point< D, Real >& operator[](size_type i)
	{
		return v[i];
	}

	constexpr const point< D, Real >& operator[](size_type i) const
	{
		return v[i];
	}

};

template< dimension_type D, typename Real = float >
using triangle = polygon< 3, D, Real >;
}

#endif // MATH_GEOMETRY_PRIMITIVES_POLYGON_HPP
