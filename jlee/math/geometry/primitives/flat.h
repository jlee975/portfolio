/// @todo move to linear_algebra module
/// @todo planes and lines should refer here, mostly

#ifndef FLAT_H
#define FLAT_H

#include "../../linear_algebra/determinants.h"
#include "point.h"

namespace geo
{
template< dimension_type D, typename Real >
constexpr point< D, Real > flat_closest_point_to_origin(
	const point< D, Real >& x,
	const point< D, Real >& y
)
{
	const auto v = y - x;

	// (y.y-y.x) / (y.y-x.y+x.x-x.y)
	const auto t = la::inner(y, v) / la::inner(v);

	return lerp(x, y, t);
}

/** Find the normal to the line defined by a,b
 *
 * There is an alternative formulation: N == (b-a)x(axb). It suffers from round
 * off error, though. The exact expressions are:
 *
 * a1 a1 b0 - a0 a1 b1 + a2 a2 b0 - a0 a2 b2 + a0 b1 b1 - a1 b0 b1 + a0 b2 b2 - a2 b0 b2,
 *   == (a1 - b1) * (a1 b0 - a0 b1) + (a2 - b2) * (a2 b0 - a0 b2)
 * a1 b0 b0 - a0 b0 b1 + a2 a2 b1 - a1 a2 b2 + a0 a0 b1 - a0 a1 b0 + a1 b2 b2 - a2 b1 b2,
 *   == (a0-b0)(a0b1-a1b0)+(a2-b2)(a2b2-a1b2)
 * a0 a0 b2 - a0 a2 b0 + a1 a1 b2 - a1 a2 b1 + a2 b0 b0 - a0 b0 b2 + a2 b1 b1 - a1 b1 b2
 *   == (a0-b0)(a0b2-a2b0)+(a1-b1)(a1b2-a2b1)
 *
 * @todo If possible, when the normal is nearly zero, use the cross product formulation
 * with accuration summation. We know there is catastrophic cancellation, but if we sum
 * the terms correctly, we could use that to our advantage (by summing values of similar
 * magnitude and opposite sign)
 * @todo Guarantee that N.a >= 0 and N.b >= 0 and N.a == N.b
 */
template< dimension_type D, typename Real >
point< D, Real > flat_normal(
	const point< D, Real >& a,
	const point< D, Real >& b
)
{
	#if 1
	const auto n = flat_closest_point_to_origin(a, b);

	if ( la::inner_sign(n, a) >= 0 && la::inner_sign(n, b) >= 0 )
	{
		return n;
	}

	// Need higher degree of accuracy
	if constexpr ( !std::is_same_v< Real, long double >)
	{
		/*
		   const point< D, long double > x(a);
		   const point< D, long double > y(b);

		   const auto v = y - x;

		   const auto t = inner(y, v);
		   const auto u = inner(x, v);

		   return x * t - u * y;
		 */
		return point< D, Real >( flat_normal< D, long double >(point< D, long double >(a), point< D, long double >(b) ) );
	}

	return n;

	#else
	// both q and r below suffer from round off error
	const auto v = b - a;

	const auto t = inner(b, v) / inner(v, v);
	const auto u = inner(a, v) / inner(v, v);

	auto q = a * t - u * b;
	auto r = cross(b - a, cross(a, b) );

	return -r;

	#endif // if 1
}

/// @todo Ideally, this would ensure the returned vector is actually the closest
/// to the true vector.
template< dimension_type D, typename Real >
point< D, Real > flat_normal1(
	const point< D, Real >& a,
	const point< D, Real >& b
)
{
	return la::normalize(flat_normal(a, b) );
}

/* Find a vector that is orthogonal to the plane defined by a, b, c
 *
 * If the returned vector is x, then x * a == x * b == x * c (x != 0)
 *
 * Points are clockwise (a,b,c) and normal points out from the clock at the person looking at it
 *
 * perp(a,b,c) == perp(b,c,a) == perp(c,a,b)
 *
 * This generalizes to Newell's method. Ex., see cs.haifa.ac.il/~gordon/plane.pdf
 * This code was derived from the rather straightforward calculation that n=(a-c)x(b-c)
 * =axb+bxc+cxa, then expanded. The result was recognized as the same as the orient2d
 * determinant and so the corresponding routine is called.
 *
 * ---------------------------------------------------------------------------------
 *
 * Also, solution to the linear system
 *
 *  [ a0 a1 a2 ] [ x ]   [ 1 ]
 *  [ b0 b1 b2 ] [ y ] = [ 1 ]
 *  [ c0 c1 c2 ] [ z ]   [ 1 ]
 *
 * By Cramer's rule
 *
 *       |  1 a1 a2 |        | a0  1 a2 |        | a0 a1  1 |
 *       |  1 b1 b2 |        | b0  1 b2 |        | b0 b1  1 |
 *       |  1 c1 c2 |        | c0  1 c2 |        | c0 c1  1 |
 *  x = --------------, y = --------------, z = --------------,
 *       | a0 a1 a2 |        | a0 a1 a2 |        | a0 a1 a2 |
 *       | b0 b1 b2 |        | b0 b1 b2 |        | b0 b1 b2 |
 *       | c0 c1 c2 |        | c0 c1 c2 |        | c0 c1 c2 |
 *
 * Since we can take any multiple of (x,y,z), we can drop the denominator.
 * This gives us the same result as above.
 *
 * May be able to improve floating point error using Iterative refinement (https://en.wikipedia.org/wiki/Iterative_refinement)
 */
/// @bug Cannot drop bottom determinant if it is negative. This flips the sign of the result!!!
/// @todo Check bottom determinant for 0 (exactly). Then the points are collinear and the
/// normal should be (0,0,0)
/*
   Instead of equal to 1 above in Cramers rule, set column equal to 1/determinant. Then get x,y,z directly. Might be able
   to avoid division by zero (when determinant is zero) by multiplying through. Should also be able to get point on plane
   closest to origin. Might be able to avoid switching on sign of result. Point on plane dot point on plane is always >= 0
   because they cannot be more than 180 degrees apart.
   What if we add the condition that x * x == x * a == x * b == x * c, making x unique (in particular, it'll be the closest
   point on the plane to the origin)
 */
template< typename Real >
constexpr point< 3, Real > flat_normal(
	const point< 3, Real >& a,
	const point< 3, Real >& b,
	const point< 3, Real >& c
)
{
	#if 0
	const auto t = 1 / la::determinant(a[0], a[1], a[2], b[0], b[1], b[2], c[0], c[1], c[2]);

	return {{ la::determinant(t, a[1], a[2], t, b[1], b[2], t, c[1], c[2]),
		la::determinant(a[0], t, a[2], b[0], t, b[2], c[0], t, c[2]),
		la::determinant(a[0], a[1], t, b[0], b[1], t, c[0], c[1], t) }};

	#endif
//

/* In six subtracts, six multiplications
 *
       const auto ab0 = (a[0] - b[0]);
       const auto ab1 = (a[1] - b[1]);
       const auto ab2 = (a[2] - b[2]);
       const auto ac0 = (a[0] - c[0]);
       const auto ac1 = (a[1] - c[1]);
       const auto ac2 = (a[2] - c[2]);

       return {{
       ab1 * ac2 - ab2 * ac1,
       ab2 * ac0 - ab0 * ac2,
       ab0 * ac1 - ab1 * ac0
       }};
 */
	#if 1
	return {{ la::orientation_determinant(a[1], a[2], b[1], b[2], c[1], c[2]),
		la::orientation_determinant(a[2], a[0], b[2], b[0], c[2], c[0]),
		la::orientation_determinant(a[0], a[1], b[0], b[1], c[0], c[1]) }};

	#else
	// FAILS when a,b,c are in the same plane as O. In this case, we don't actually
	// want to solve the linear system, we want the determinants (equivalently, the
	// null space
	Real A[3][3] = {{ a[0], a[1], a[2] }, { b[0], b[1], b[2] }, { c[0], c[1], c[2] }};
	Real X[3]    = { 1, 1, 1 };

	int                    ipiv;
	auto                   res    = LAPACKE_sgesv(LAPACK_ROW_MAJOR, 3, 1, &A[0][0], 3, &ipiv, X, 1);
	const point< 3, Real > theirs = {{ X[0], X[1], X[2] }};

	std::cout << "a = " << a << "; b = " << b << "; c = " << c << std::endl;
	std::cout << "Mine = " << mine << "; Theirs = " << theirs << std::endl;
	std::cout << "Res = " << res << std::endl;
	return theirs;

	#endif // if 1
}

/// @todo Ideally, this would ensure the returned vector is actually the closest
/// to the true vector.
template< typename Real >
constexpr point< 3, Real > flat_normal1(
	const point< 3, Real >& a,
	const point< 3, Real >& b,
	const point< 3, Real >& c
)
{
	return la::normalize(flat_normal(a, b, c) );
}

}
#endif // FLAT_H
