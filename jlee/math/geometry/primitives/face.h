#ifndef MATH_GEOMETRY_PRIMITIVES_FACE_H
#define MATH_GEOMETRY_PRIMITIVES_FACE_H

#include "size_type.h"
#include <array>

namespace geo
{
template< size_type N >
struct face_type
{
	std::array< size_type, N > vertices;
};
}

#endif // MATH_GEOMETRY_PRIMITIVES_FACE_H
