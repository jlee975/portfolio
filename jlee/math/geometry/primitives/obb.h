#ifndef MATH_GEOMETRY_PRIMITIVES_OBB_HPP
#define MATH_GEOMETRY_PRIMITIVES_OBB_HPP

#include "point.h"

namespace geo
{
/** Oriented bounding box
 * @todo A OBB can be fully determined by 4 points (middle, and center of
 *   three faces.
 * @todo D-dimensional
 * @todo Rename
 */
template< dimension_type D, typename Real = float >
struct OBB
{
	// center of faces
	point< D, Real > faces[D];

	// min/max for each of the dimensions
	point< D, Real > min;
	point< D, Real > max;
};
}
#endif // MATH_GEOMETRY_PRIMITIVES_OBB_HPP
