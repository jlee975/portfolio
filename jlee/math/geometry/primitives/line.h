#ifndef MATH_GEOMETRY_PRIMITIVES_LINE_HPP
#define MATH_GEOMETRY_PRIMITIVES_LINE_HPP

#include "point.h"

namespace geo
{
/** @brief A line in D dimensions
 *
 * A line is defined by two points in D dimensions.
 *
 * @note An alternative form of a line may be a point plus a direction. That
 * representation is provided by the ray structure.
 */
template< dimension_type D, typename Real = float >
struct line
{
	using real_type = Real;
	static const dimension_type dimension = D;

	point< D, Real > p; ///< A point on the line
	point< D, Real > q; ///< Another point on the line

	constexpr bool isValid() const
	{
		return p != q;
	}

};

/// @todo Generalize for any number of dimensions
/// @todo Accept line as an argument
template< typename Real >
constexpr Real distance_from_origin(
	const point< 3, Real >& p,
	const point< 3, Real >& q
)
{
	return cross(p, q).length() / ( q - p ).length();
}

}
#endif // MATH_GEOMETRY_PRIMITIVES_LINE_HPP
