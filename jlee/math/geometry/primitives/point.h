#ifndef MATH_GEOMETRY_PRIMITIVES_POINT_HPP
#define MATH_GEOMETRY_PRIMITIVES_POINT_HPP

#include <cmath>
#include <cstdlib>
#include <type_traits>

#include "../../linear_algebra/euclidean.h"
#include "util/initializers.h"

#include "dimension.h"

namespace geo
{
/** @brief The building block for D-dimensional objects
 *
 * Elements of R^D (ex., R2, R3, etc.)
 *
 * @todo rescale to a length of [1,2)
 * @todo promote or embed a lower dimensional point
 * @todo Remove arithmetic. These aren't vectors. They're points
 */
template< dimension_type D, typename Real = float >
class point
{
	static_assert(D != 0, "Dimension cannot be zero");
	static_assert(std::is_floating_point_v< Real >, "Field must be a floating point");
public:
	using real_type = Real;
	static constexpr dimension_type dimension = D;
	using raw_type = Real[D];

	struct initializer
	{
		raw_type p;
	};

	explicit point(uninitialized_type)
	{
	}

	/// @todo Appropriate type that will copy elements automatically
	constexpr point(const initializer& x)
		: p(x)
	{
	}

	constexpr point(zero_initialized_type)
		: p{}
	{
	}

	// Embed a point from a lower dimension into this dimension by padding with 0s
	template< dimension_type D1, typename Real1, std::enable_if_t < ( D1 < D ), int > = 0 >
	explicit constexpr point(const point< D1, Real1 >& q)
		: p{}
	{
		for (dimension_type i = 0; i < D1; ++i)
		{
			p.p[i] = q.p[i];
		}
	}

	// Convert a point of the same dimension but different coordinate type
	template< typename Real1, std::enable_if_t< !std::is_same_v< Real, Real1 >, int > = 0 >
	explicit constexpr point(const point< D, Real1 >& q)
	{
		for (dimension_type i = 0; i < D; ++i)
		{
			p.p[i] = q[i];
		}
	}

	constexpr const real_type& operator[](dimension_type i) const
	{
		return p.p[i];
	}

	constexpr real_type& operator[](dimension_type i)
	{
		return p.p[i];
	}

	constexpr point& operator+=(const point& q)
	{
		for (dimension_type i = 0; i < D; ++i)
		{
			p.p[i] += q.p.p[i];
		}

		return *this;
	}

	constexpr point& operator-=(const point& q)
	{
		for (dimension_type i = 0; i < D; ++i)
		{
			p[i] -= q.p[i];
		}

		return *this;
	}

	constexpr point& operator*=(real_type s)
	{
		for (dimension_type i = 0; i < D; ++i)
		{
			p.p[i] *= s;
		}

		return *this;
	}

	constexpr point& operator/=(real_type s)
	{
		return this->operator*=(1 / s);
	}

	/// Length is defined in terms of inner product
	/// @todo L2norm for hypot
	/// @todo Rename distance_from_origin or something. Try to think of these as points, rather than vectors
	constexpr real_type length() const
	{
		return la::pnorm2(p.p);
	}

	/* squared length
	 * cheaper than length, but will overflow before length. Probably not
	 * an issue in practice
	 */
	constexpr real_type length2() const
	{
		return la::inner(*this);
	}

	raw_type& raw()
	{
		return p.p;
	}

	const raw_type& raw() const
	{
		return p.p;
	}
private:
	initializer p; /// @todo rename "coord"
};

template< dimension_type D, typename Real >
constexpr point< D, Real > operator-(const point< D, Real >& p)
{
	point< D, Real > q(uninitialized);

	for (dimension_type i = 0; i < D; ++i)
	{
		q[i] = -p[i];
	}

	return q;
}

template< dimension_type D, typename Real >
constexpr bool operator==(
	const point< D, Real >& l,
	const point< D, Real >& r
)
{
	return la::equal(l, r);
}

template< dimension_type D, typename Real >
constexpr bool operator!=(
	const point< D, Real >& l,
	const point< D, Real >& r
)
{
	return !la::equal(l, r);
}

template< dimension_type D, typename Real >
constexpr point< D, Real > operator+(
	const point< D, Real >& l,
	const point< D, Real >& r
)
{
	point< D, Real > q(uninitialized);

	for (dimension_type i = 0; i < D; ++i)
	{
		q[i] = l[i] + r[i];
	}

	return q;
}

template< dimension_type D, typename Real >
constexpr point< D, Real > operator-(
	const point< D, Real >& l,
	const point< D, Real >& r
)
{
	point< D, Real > q(uninitialized);

	for (dimension_type i = 0; i < D; ++i)
	{
		q[i] = l[i] - r[i];
	}

	return q;
}

/// @todo Gonna wanna find instances where Real and Real2 don't match
template< dimension_type D, typename Real >
constexpr point< D, Real > operator*(
	const point< D, Real >& l,
	Real                    r
)
{
	point< D, Real > q(uninitialized);

	for (dimension_type i = 0; i < D; ++i)
	{
		q[i] = l[i] * r;
	}

	return q;
}

template< dimension_type D, typename Real >
constexpr point< D, Real > operator*(
	Real                    s,
	const point< D, Real >& p
)
{
	return p * s;
}

template< dimension_type D, typename Real >
constexpr point< D, Real > operator/(
	const point< D, Real >& l,
	Real                    r
)
{
	return l * ( 1 / r );
}

#if 0
/** This is dot product because it is defined for all D. It also is used
 *  significantly more than, say, cross product. Mathematically, it also
 *  naturally corresponds to an inner product space, so our class
 *  corresponds to an algebraic object.
 */
template< dimension_type D, typename Real >
constexpr Real operator*(
	const point< D, Real >& a,
	const point< D, Real >& b
)
{
	return la::inner(a, b);
}

#endif

template< typename Real >
constexpr fp::robust_vector< Real, 4, 3 > cross_exact(
	const point< 3, Real >& a,
	const point< 3, Real >& b
)
{
	return { fp::robust_product(a[1], b[2]) - fp::robust_product(a[2], b[1]),
	         fp::robust_product(a[2], b[0]) - fp::robust_product(a[0], b[2]),
	         fp::robust_product(a[0], b[1]) - fp::robust_product(a[1], b[0]) };
}

/// @todo cross is more of a "vector" thing than a "point" thing
/// @todo https://people.eecs.berkeley.edu/~wkahan/MathH110/Cross.pdf
template< typename Real >
constexpr point< 3, Real > cross(
	const point< 3, Real >& a,
	const point< 3, Real >& b
)
{
	const auto x = cross_exact(a, b);

	return {{ approximate(x[0]), approximate(x[1]), approximate(x[2]) }};
}

/// @todo Numerically stable method. Equivalent to 3x3 determinant
template< typename Real >
constexpr Real scalar_triple_product(
	const point< 3, Real >& a,
	const point< 3, Real >& b,
	const point< 3, Real >& c
)
{
	return inner(a, cross(b, c) );
}

/// @todo Smarter handling
template< dimension_type D, typename Real >
Real distance2(
	const point< D, Real >& a,
	const point< D, Real >& b
)
{
	return la::inner(b - a);
}

/// @todo Confusing name compared to acute, obtuse. Does this function test or generate
template< typename Real >
constexpr point< 2, Real > perp(const point< 2, Real >& a)
{
	return {{ -a[1], a[0] }};
}

/** @brief Return a vector perpendicular to a and b, unspecified length
 *
 * @note (a,b,c) is a right handed triple
 * @todo Maybe this should return a ray
 */
template< typename Real >
constexpr point< 3, Real > perp(
	const point< 3, Real >& a,
	const point< 3, Real >& b
)
{
	point< 3, Real > c(uninitialized);

	la::perp(a.raw(), b.raw(), c.raw() );
	return c;
}

template< typename Real >
constexpr point< 3, Real > perp1(
	const point< 3, Real >& a,
	const point< 3, Real >& b
)
{
	point< 3, Real > c(uninitialized);

	la::perp1(a.raw(), b.raw(), c.raw() );
	return c;
}

/// @todo projection of point onto line, onto plane
template< dimension_type D, typename Real >
constexpr point< D, Real > projection(
	const point< D, Real >& a,
	const point< D, Real >& b
)
{
	// find x * b such that (a - x * b) is perpendicular to b
	// i.e., a.b - (x * b).b == 0
	// a.b == x * (b.b)
	// x == a.b / b.b
	// because of the division, this function cannot be exact
	return ( la::inner(a, b) / la::inner(b) ) * b;
}

/// @todo If we allow multiples of the result, we should be able to do this exactly
template< dimension_type D, typename Real >
constexpr point< D, Real > rejection(
	const point< D, Real >& a,
	const point< D, Real >& b
)
{
	return a - projection(a, b);
}

template< dimension_type D, typename Real >
constexpr point< D, Real > lerp(
	const point< D, Real >& x,
	const point< D, Real >& y,
	Real                    t
)
{
	return x * t + ( 1 - t ) * y;
}

}

namespace la
{
template< geo::dimension_type D, typename R >
struct vector_dimension< geo::point< D, R > >
{
	static constexpr std::size_t value = D;
};
}

#endif // MATH_GEOMETRY_PRIMITIVES_POINT_HPP
