#ifndef MATH_GEOMETRY_PRIMITIVES_GEOS_HPP
#define MATH_GEOMETRY_PRIMITIVES_GEOS_HPP

namespace geo
{
/// @todo Some of these are specializations of Polytope, and should probably be a separate enum
enum geo_t
{
	Point,      ///< A single point
	Polytope,   ///< A generic polygon soup
	Plane,      ///< A rectangle
	Sphere,     ///< A sphere
	Box,        ///< A rectangular prism
	Icosahedron ///< An icosahedron
};
}

#endif // MATH_GEOMETRY_PRIMITIVES_GEOS_HPP
