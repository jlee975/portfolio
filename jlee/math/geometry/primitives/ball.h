#ifndef MATH_GEOMETRY_PRIMITIVES_BALL_HPP
#define MATH_GEOMETRY_PRIMITIVES_BALL_HPP

#include "../../linear_algebra/determinants.h"
#include "point.h"

namespace geo
{
/** @brief A ball in D dimensions
 *
 * This structure represents a hypersphere, where ball<3> is what is commonly
 * called a sphere, and ball<2> is a circle (or disk). More generally, in
 * D-dimensional space, it is the set of points that are a fixed distance from
 * a given point. These values are the radius and center respectively.
 */
template< dimension_type D, typename Real = float >
class ball
{
public:
	using real_type = Real;
	static constexpr dimension_type dimension = D;

	explicit ball(uninitialized_type)
	{
	}

	constexpr ball(
		const point< D, Real >& c,
		real_type               r_
	)
		: center_(c), r(r_)
	{
	}

	constexpr explicit ball(real_type r_)
		: center_(zero_initialized), r(r_)
	{
	}

	constexpr bool isValid() const
	{
		return r >= 0;
	}

	const point< D, Real >& center() const
	{
		return center_;
	}

	Real radius() const
	{
		return r;
	}
private:
	point< D, Real > center_;
	Real             r; // radius
};

template< typename Real = float >
using circle = ball< 2, Real >;

// Of a triangle of sides length a, b, c
template< typename Real >
Real circumradius(
	Real a,
	Real b,
	Real c
)
{
	return a * b * c / std::sqrt( ( a + b + c ) * ( -a + b + c ) * ( a - b + c ) * ( a + b - c ) );
}

template< typename Real >
constexpr point< 2, Real > circumcenter(
	const point< 2, Real >& u,
	const point< 2, Real >& v,
	const point< 2, Real >& w
)
{
	const Real s = 2.0f * la::orientation_determinant(u[0], u[1], v[0], v[1], w[0], w[1]);

	/// @todo I think these can use the incircle functions from predicates.c
	return { la::orientation_determinant(u * u, u[1], v * v, v[1], w * w, w[1]) / s,
	         la::orientation_determinant(u[0], u * u, v[0], v * v, w[0], w * w) / s };
}

namespace detail
{
// circumscribed circle of 3 points, with precalculated side lengths
template< typename Real >
constexpr circle< Real > circumscribed_circle(
	const point< 2, Real >& u,
	const point< 2, Real >& v,
	const point< 2, Real >& w,
	Real                    a,
	Real                    b,
	Real                    c
)
{
	return { circumcenter(u, v, w), circumradius(a, b, c) };
}

}

// Circumscribed circle of three points
/// @todo Take triangle
/// @todo Higher dimensions
template< typename Real >
constexpr circle< Real > circumscribed_circle(
	const point< 2, Real >& a,
	const point< 2, Real >& b,
	const point< 2, Real >& c
)
{
	return detail::circumscribed_circle(a, b, c, ( a - b ).length(), ( b - c ).length(), ( c - a ).length() );
}

}

#endif // MATH_GEOMETRY_PRIMITIVES_BALL_HPP
