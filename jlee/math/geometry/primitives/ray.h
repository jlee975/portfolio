#ifndef MATH_GEOMETRY_PRIMITIVES_RAY_H
#define MATH_GEOMETRY_PRIMITIVES_RAY_H

#include "point.h"
#include "util/initializers.h"

namespace geo
{
/** @brief A direction
 *
 * @todo Define how ray is different from point.
 * @todo May want a unit_ray class
 */
template< dimension_type D, typename Real = float >
class ray
{
public:
	using real_type = Real;
	using raw_type  = Real[D];
	static const dimension_type dimension = D;

	struct initializer
	{
		real_type p[D];
	};

	explicit ray(uninitialized_type)
	{
	}

	constexpr ray(const initializer& x)
		: p(x)
	{
	}

	constexpr ray(zero_initialized_type)
		: p()
	{
	}

	constexpr bool is_valid() const
	{
		return !la::zero(p.p);
	}

	real_type& operator[](dimension_type i)
	{
		return p.p[i];
	}

	constexpr const real_type& operator[](dimension_type i) const
	{
		return p.p[i];
	}

	raw_type& raw()
	{
		return p.p;
	}

	const raw_type& raw() const
	{
		return p.p;
	}
private:
	initializer p;
};

template< dimension_type D, typename Real >
ray< D, Real > operator-(const ray< D, Real >& p)
{
	ray< D, Real > r(uninitialized);

	for (dimension_type i = 0; i < D; ++i)
	{
		r[i] = -p[i];
	}

	return r;
}

/// @brief Embed ray as point with given norm
/// @todo topoint(1) specialization if we guarantee p is unit
template< dimension_type D, typename Real, typename Real2 >
point< D, Real > topoint(
	const ray< D, Real >& r,
	Real2                 len
)
{
	const Real x = len / la::pnorm2(r.raw() );

	point< D, Real > t(uninitialized);

	for (dimension_type i = 0; i < D; ++i)
	{
		t[i] = r[i] * x;
	}

	return t;
}

template< dimension_type D, typename Real >
point< D, Real > topoint(const ray< D, Real >& r)
{
	point< D, Real > t(uninitialized);

	for (dimension_type i = 0; i < D; ++i)
	{
		t[i] = r[i];
	}

	return t;
}

template< dimension_type D, typename R2, typename R1 = R2 >
ray< D, R1 > make_ray(const point< D, R2 >& p)
{
	ray< D, R1 > t(uninitialized);

	for (dimension_type i = 0; i < D; ++i)
	{
		t[i] = static_cast< R1 >( p[i] );
	}

	return t;
}

// Relative distance of p along r. Relative to length of r; still provides an ordering for points.
template< dimension_type D, typename R >
constexpr R projected_scale(
	const ray< D, R >&   r,
	const point< D, R >& p
)
{
	return la::inner(r, p);
}

// Absolute distance of p along r.
/// @todo constexpr when pnorm2 is constexpr
template< dimension_type D, typename R >
R projected_scale1(
	const ray< D, R >&   r,
	const point< D, R >& p
)
{
	const R x = la::inner(r, p);

	return x != 0 ? x / la::pnorm2(r) : 0;
}

}

namespace la
{
template< geo::dimension_type D, typename R >
struct vector_dimension< geo::ray< D, R > >
{
	static constexpr std::size_t value = D;
};
}

#endif // MATH_GEOMETRY_PRIMITIVES_RAY_H
