#ifndef MATH_GEOMETRY_PRIMITIVES_SIZE_TYPE_H
#define MATH_GEOMETRY_PRIMITIVES_SIZE_TYPE_H

#include <cstdint>

namespace geo
{
/// @todo Use std::uint_least32_t, for example, to shrink some structs
using size_type = std::uint_least32_t;
}

#endif // MATH_GEOMETRY_PRIMITIVES_SIZE_TYPE_H
