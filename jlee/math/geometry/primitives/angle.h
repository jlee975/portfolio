#ifndef MATH_GEOMETRY_PRIMITIVES_ANGLE_H
#define MATH_GEOMETRY_PRIMITIVES_ANGLE_H

#include "point.h"
#include "ray.h"

namespace geo
{
// Check if the angle between u and v is acute (strictly less than pi/2 radians)
/// @todo Angle between two points
/// @todo Use epsilon for a quick test, and switch to https://math.stackexchange.com/a/1782769
/// when necessary. SO references https://people.eecs.berkeley.edu/~wkahan/Mindless.pdf section 12
template< typename V1, typename V2 >
constexpr bool is_acute(
	const V1& u,
	const V2& v
)
{
	return la::inner_sign(u, v) > 0;
}

// Check if the angle between u and v is obtuse (strictly greater than pi/2 radians)
template< typename V1, typename V2 >
constexpr bool is_obtuse(
	const V1& u,
	const V2& v
)
{
	return la::inner_sign(u, v) < 0;
}

}

#endif // MATH_GEOMETRY_PRIMITIVES_ANGLE_H
