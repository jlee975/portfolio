/** @brief Implementation of Veltkamp/Dekker exact floating point routines
 *
 * Exact arithmetic for fixed sizes of values (i.e., the number of words needed is known at compile time). Results of
 * calculations are stored in a robust_t, which is a multi-word representation of an exact value.
 *
 * Largely based around Shewchuk's paper [1], which itself is based on some work by Dekker, Knuth. Other techniques by
 * Ozaki, et. al. [2] may be implemented as time permits.
 *
 * References:
 * [1] "Adaptive Precision Floating-Point Arithmetic and Fast Robust Geometric Predicates", Jonathan Richard Shewchuk
 * [2] "Adaptive and Efficient Algorithm for 2D Orientation Problem", by Ozaki, Ogita, Rump, Oishi
 *
 * @todo For single precision floats, most operations could simply be promotions to double (ex., robust_add(float, float)
 * could simply return a double). 3 floats could fit in a long double. __float128 is also available
 * @todo Refer to the Shewchuk paper about the non-adjacency property. If that is true of an expansion, we don't need
 * to call approximate, we can just take the top (nonzero) word (I believe)
 */
#ifndef ROBUST_H
#define ROBUST_H

#include <cmath>
#include <limits>
#include <tuple>

#include "constexprfns.h"
#include "util/initializers.h"

#define HAS_FMA

namespace fp
{
/** The result of a robust calculation.
 *
 * The sum of the values is the true result.
 *
 * @todo Ensure ordered in increasing order of magnitude
 * @todo Ensure values are non-overlapping. i.e., that x[j] < x[i] * epsilon for j > i
 * @todo May want to add an adjacency indicator, esp so approximating can be replaced with taking the top word in a
 * non-adjacent sequence. Adding and multiplying are non-adjacent if round-to-nearest with round to even tie breaking
 * is used on the FPU. Detect this at compile time and ensure it can't be changed (esp using #pragma STD FENV_ACCESS OFF)
 * @todo Debugging code to check that top word is within 1 ulp of true sum
 */
template< typename Real, std::size_t N >
struct robust_t
{
	Real x[N];
};

#if 0
template< typename Real, std::size_t N >
std::ostream& operator<<(
	std::ostream&              os,
	const robust_t< Real, N >& x
)
{
	os << "{";

	for ( std::size_t i = 0; i < N; ++i )
	{
		if ( i != 0 )
		{
			os << ", ";
		}

		os << x.x[i];
	}

	os << "}";
	return os;
}

#endif // if 0
}

namespace std
{
template< typename Real, std::size_t N >
class tuple_size< fp::robust_t< Real, N > > : public integral_constant< std::size_t, N >
{
};

template< std::size_t I, typename Real, std::size_t N >
struct tuple_element< I, fp::robust_t< Real, N > >
{
	using type = Real;
};
}

namespace fp
{
template< std::size_t I, typename Real, std::size_t N >
constexpr Real& get(robust_t< Real, N >& a) noexcept
{
	static_assert(I < N);
	return a.x[I];
}

template< std::size_t I, typename Real, std::size_t N >
constexpr const Real& get(const robust_t< Real, N >& a) noexcept
{
	static_assert(I < N);
	return a.x[I];
}

template< std::size_t I, typename Real, std::size_t N >
constexpr Real&& get(robust_t< Real, N >&& a) noexcept
{
	static_assert(I < N);
	return std::move(a.x[I]);
}

template< std::size_t I, typename Real, std::size_t N >
constexpr const Real&& get(const robust_t< Real, N >&& a) noexcept
{
	static_assert(I < N);
	return std::move(a.x[I]);
}

namespace details
{
template< typename Real, std::size_t N, std::size_t... I >
constexpr Real approximate_helper(const robust_t< Real, N >& e, std::index_sequence< I... >)
{
	return (... + get< I >(e));
}

}

// Get an approximate value of the sum of elements of an array, quickly. If needed,
// follow up with a robust algorithm. Relies on the property of robust_t that ei are in increasing order
// Error is less than one ulp ([1] p.26)
/// @todo Make a (const) member
template< typename Real, std::size_t N >
constexpr Real approximate(const robust_t< Real, N >& e)
{
	return details::approximate_helper(e, std::make_index_sequence< N >{});
}

// Approximate value of sum, correct up to sign
template< typename Real, std::size_t N >
constexpr Real approximate_sign(const robust_t< Real, N >& e)
{
	return approximate(e);
}

namespace details
{
#ifndef HAS_FMA
template< typename Real >
constexpr Real splitter = jl::pow2< Real >((std::numeric_limits< Real >::digits + 1) / 2) + 1;

template< typename Real >
constexpr robust_t< Real, 2 > split_factor(const Real a)
{
	const auto c = (splitter< Real > * a);
	const auto abig = (c - a);
	const auto ahi = c - abig;

	return { a - ahi, ahi };
}

#endif

template< typename Real >
constexpr Real robust_add_correction(Real a, Real b, Real x)
{
	const Real z = (x - a);

	return (a - (x - z)) + (b - z);
}

template< typename Real >
constexpr Real robust_add_fast_correction(Real a, Real b, Real x)
{
	const Real bvirt = x - a;

	return b - bvirt;
}

template< typename Real >
constexpr Real robust_difference_correction(Real a, Real b, Real x)
{
	const Real z = (a - x);

	return (a - (x + z)) + (z - b);
}

/// @todo FMA can be used to replace this. Just do FMA(a,b,-x)
template< typename Real >
constexpr robust_t< Real, 2 > robust_product_correction(const Real a, const Real b, const Real x)
{
#ifndef HAS_FMA
	const auto [alo, ahi] = details::split_factor(a);
	const auto [blo, bhi] = details::split_factor(b);
	const Real err1 = x - (ahi * bhi);
	const Real err2 = err1 - (alo * bhi);
	const Real err3 = err2 - (ahi * blo);

	return { (alo * blo) - err3, x };

#else
	return { std::fma(a, b, -x), x };

#endif
}

/// @todo FMA can be used to replace this. Just do FMA(a,b,-x)
template< typename Real >
constexpr robust_t< Real, 2 > robust_square_correction(const Real a, const Real x)
{
#ifndef HAS_FMA
	const auto [alo, ahi] = details::split_factor(a);
	const Real err1 = x - (ahi * ahi);
	const Real err3 = err1 - ((ahi + ahi) * alo);

	return { (alo * alo) - err3, x };

#else
	return { std::fma(a, a, -x), x };

#endif
}

}

template< typename Real >
constexpr robust_t< Real, 2 > robust_add(Real a, Real b)
{
	const auto x = a + b;

	return { details::robust_add_correction(a, b, x), x };
}

// a + b. Requires |a| >= |b|
template< typename Real >
constexpr robust_t< Real, 2 > robust_add_fast(Real a, Real b)
{
	const Real x = a + b;

	return { details::robust_add_fast_correction(a, b, x), x };
}

/* a - b when we already have two robust values, i.e., (a0 + a1) - (b0 + b1)
 *
 * This function is a unrolling of the ExpansionSum algorithm presented in Shewchuk's paper. The resulting sequence will
 * be in increasing order of magnitude, possibly with interspersed zeros. See the paper for a proof. Similarly for
 * robust_difference.
 */
/** @brief Sum two robust values
 *
 * @todo Inline grow_expansion
 */
template< typename Real, std::size_t M, std::size_t N >
constexpr robust_t< Real, M + N > operator+(const robust_t< Real, M >& a, const robust_t< Real, N >& b)
{
	if constexpr ( M == 2 && N == 2 )
	{
		// x0 + y0 == a0 + b0
		const auto [x0, y0] = robust_add(get< 0 >(a), get< 0 >(b));

		// y2 + y1 == a1 + y0
		const auto [y2, y1] = robust_add(get< 1 >(a), y0);

		// x1 + y3 == y2 + b1
		const auto [x1, y3] = robust_add(y2, get< 1 >(b));

		// x2 + x3 == y1 + y3
		const auto [x2, x3] = robust_add(y1, y3);

		// x0 + x1 + x2 + x3 == x0 + x1 + y1 + y3 == x0 + y2 + b1 + y1 == x0 + a1 + y0 + b1 == a0 + a1 + b0 + b1
		return { x0, x1, x2, x3 };
	}
	else
	{
		robust_t< Real, M + N > h = {};

		for ( std::size_t i = 0; i < M; ++i )
		{
			h.x[i] = a.x[i];
		}

		for ( std::size_t i = 0; i < N; ++i )
		{
			Real q = b.x[i];

			for ( std::size_t j = 0; j < M; ++j )
			{
				const auto [hj, qi] = robust_add(q, h.x[i + j]);
				q = qi;
				h.x[i + j] = hj;
			}

			h.x[i + M] = q;
		}

		return h;
	}
}

// a - b
template< typename Real >
constexpr robust_t< Real, 2 > robust_difference(Real a, Real b)
{
	const auto x = (a - b);

	return { details::robust_difference_correction(a, b, x), x };
}

// a - b when we already have two robust values, i.e., (a0 + a1) - (b0 + b1)
// Equivalent to a + (-b)
template< typename Real, std::size_t M, std::size_t N >
constexpr robust_t< Real, M + N > operator-(const robust_t< Real, M >& a, const robust_t< Real, N >& b)
{
	if constexpr ( M == 2 && N == 2 )
	{
		// x0 + y0 == a0 - b0
		const auto [x0, y0] = robust_difference(get< 0 >(a), get< 0 >(b));

		// y2 + y1 == a1 + y0
		const auto [y2, y1] = robust_add(get< 1 >(a), y0);

		// x1 + y3 == y2 - b1
		const auto [x1, y3] = robust_difference(y2, get< 1 >(b));

		// x2 + x3 == y1 + y3
		const auto [x2, x3] = robust_add(y1, y3);

		// x0 + x1 + x2 + x3 == x0 + x1 + y1 + y3 == x0 + y2 - b1 + y1 == x0 + a1 + y0 - b1 == a0 + a1 - b0 - b1
		return { x0, x1, x2, x3 };
	}
	else
	{
		robust_t< Real, M + N > h = {};

		for ( std::size_t i = 0; i < M; ++i )
		{
			h.x[i] = a.x[i];
		}

		for ( std::size_t i = 0; i < N; ++i )
		{
			Real q = -b.x[i];

			for ( std::size_t j = 0; j < M; ++j )
			{
				const auto [hj, qi] = robust_add(q, h.x[i + j]);
				q = qi;
				h.x[i + j] = hj;
			}

			h.x[i + M] = q;
		}

		return h;
	}
}

template< typename Real >
constexpr robust_t< Real, 2 > robust_product(Real a, Real b)
{
	return details::robust_product_correction(a, b, a * b);
}

/// @todo constexpr
template< typename Real, std::size_t N >
robust_t< Real, N + N > operator*(const robust_t< Real, N >& l, Real r)
{
	robust_t< Real, N + N > h;

	auto [h0, q2im1] = robust_product(l.x[0], r);
	h.x[0] = h0;

	for ( std::size_t i = 1; i < N; ++i )
	{
		const auto [ti, Ti] = robust_product(l.x[i], r);
		const auto [h2im1, q2i] = robust_add(q2im1, ti);
		h.x[2 * i - 1] = h2im1;
		const auto [h2i, q2ip1] = robust_add_fast(Ti, q2i);
		h.x[2 * i] = h2i;
		q2im1 = q2ip1;
	}

	h.x[2 * N - 1] = q2im1;
	return h;
}

namespace details
{
template< typename Real, std::size_t M, std::size_t N, std::size_t... Is >
robust_t< Real, 2 * M * N >
multiplication_helper(const robust_t< Real, M >& l, const robust_t< Real, N >& r, std::index_sequence< Is... >)
{
	return (... + (l * get< Is >(r)));
}

}

template< typename Real, std::size_t M, std::size_t N >
robust_t< Real, 2 * M * N > operator*(const robust_t< Real, M >& l, const robust_t< Real, N >& r)
{
	return details::multiplication_helper(l, r, std::make_index_sequence< N >());
}

/// @todo Any optimization that might be had from a being the same argument
template< typename Real >
constexpr robust_t< Real, 2 > robust_square(Real a)
{
	return details::robust_square_correction(a, a * a);
}

/// @todo Use divide and conquer to sum up everything "evenly"
template< typename... Xs >
auto sum(const Xs&... xs)
{
	return (... + xs);
}

// Sum two robust terms and reduce to an approximation in one step
/// @todo Avoid the intermediate storage
template< typename... Xs >
auto approximate_sum(const Xs&... xs)
{
	/// @todo Can we get away with just using get< N + N - 1 >(x)
	return approximate(sum(xs...));
}

/// @todo Forward to a compare function
/// @todo Accept any arithmetic type for r and promote to appropriate robust_t
/// @todo Implement ==, !=, etc.
/// @todo constexpr
/// @todo If top word is within 1 ulp of true sum, do a quick Real/Real check against the possible error. i.e,.
/// if (top(l) - r > top_error(l)) return true
template< typename Real, std::size_t N >
bool operator>=(const robust_t< Real, N >& l, Real r)
{
	const robust_t< Real, 1 > rlift = { r };

	const auto diff = l - rlift;
	const auto x = approximate(diff);

	return x >= 0;
}

/// @todo Should always be able to use the top word. Tail should always be <= 1ulp with top >= 1ulp. In the case of
/// top == 1ulp, (i.e., a power of two) then the tail should be zero. Or the top is zero and the rest should be zero
template< typename Real, std::size_t N >
constexpr int sign(const robust_t< Real, N >& x)
{
	const auto y = approximate_sign(x);

	if ( y == 0 )
	{
		return 0;
	}

	return y > 0 ? 1 : -1;
}

template< typename Real, std::size_t N, std::size_t D >
class robust_vector
{
public:
	using value_type = robust_t< Real, N >;

	robust_vector(uninitialized_type) {}

	template< typename... Ts >
	robust_vector(const value_type& first, Ts&&... ts) : v{ first, std::forward< Ts >(ts)... }
	{
	}

	robust_vector(const robust_vector& o) = default;
	robust_vector(robust_vector&& o) = default;

	value_type& operator[](std::size_t i) { return v[i]; }

	const value_type& operator[](std::size_t i) const { return v[i]; }

private:
	value_type v[D];
};

template< typename Real, std::size_t N, std::size_t M, std::size_t D >
robust_vector< Real, N + M, D > operator+(const robust_vector< Real, N, D >& l, const robust_vector< Real, M, D >& r)
{
	robust_vector< Real, N + M, D > res(uninitialized);

	for ( std::size_t i = 0; i < D; ++i )
	{
		res[i] = l[i] + r[i];
	}

	return res;
}

template< typename Real, std::size_t N, std::size_t D, std::size_t M >
robust_vector< Real, 2 * N * M, D > operator*(const robust_vector< Real, N, D >& l, const robust_t< Real, M >& r)
{
	robust_vector< Real, 2 * N * M, D > res(uninitialized);

	for ( std::size_t i = 0; i < D; ++i )
	{
		res[i] = l[i] * r;
	}

	return res;
}

template< typename Real, std::size_t N, std::size_t D, std::size_t M >
robust_vector< Real, 2 * N * M, D > operator*(const robust_t< Real, M >& l, const robust_vector< Real, N, D >& r)
{
	return r * l;
}

template< typename Real, std::size_t N, std::size_t D >
robust_vector< Real, 2 * N, D > operator*(const robust_vector< Real, N, D >& l, Real r)
{
	robust_vector< Real, 2 * N, D > res;

	for ( std::size_t i = 0; i < D; ++i )
	{
		res[i] = l[i] * r;
	}

	return res;
}

template< typename Real, std::size_t N, std::size_t D >
robust_vector< Real, 2 * N, D > operator*(Real l, const robust_vector< Real, N, D >& r)
{
	return r * l;
}

}
#endif // ROBUST_H
