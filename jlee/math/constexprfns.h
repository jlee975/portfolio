#ifndef CONSTEXPRFNS_H
#define CONSTEXPRFNS_H

#include <cmath>
#include <concepts>
#include <limits>

namespace jl
{
/// @todo General power function
/// @bug Not correct if n < 0
/// @todo Accept any integer type for n
/// @todo Throw if overflow occurs
/// @todo Bound n by numeric_limits min/max exponent
template< typename Real >
constexpr Real pow2(int n)
{
	return std::ldexp(Real(1), n);
}

template< std::integral Real >
constexpr Real abs(Real x)
{
	// Obvious implementation
	return x >= 0 ? x : -x;
}

template< std::floating_point Real >
constexpr Real abs(Real x)
{
	// Floating point types can have negative zero, so we have to be a bit more careful
	return x > 0 ? x : ( x < 0 ? -x : Real(0) );
}

template< typename Real >
constexpr Real max(Real x)
{
	return x;
}

template< typename Real, typename...Args >
constexpr Real max(
	Real      x0,
	Real      x1,
	Args&&... args
)
{
	const Real y = ::jl::max(x1, args...);

	return y < x0 ? x0 : y;
}

}

#endif // CONSTEXPRFNS_H
