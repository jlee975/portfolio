#ifndef MATH_LINEAR_ALGEBRA_ORIENTATION_H
#define MATH_LINEAR_ALGEBRA_ORIENTATION_H

#include "../robust.h"
#include "predicates.h"

namespace la
{
/** @brief 2x2 determinant, accurate up to floating point precision
 *
 * Determinant of
 *   | a b |
 *   | c d |
 * i.e., ad-bc
 */
template< typename Real >
constexpr Real determinant(
	Real a,
	Real b,
	Real c,
	Real d
)
{
	const auto l = fp::robust_product(a, d);
	const auto r = fp::robust_product(-b, c);

	return fp::approximate_sum(l, r);
}

template< typename Real >
Real determinant(
	Real a,
	Real b,
	Real c,
	Real d,
	Real e,
	Real f,
	Real g,
	Real h,
	Real i
)
{
	const double t[4][3] = {{ a, b, c }, { d, e, f }, { g, h, i }, { 0, 0, 0 }};

	return orient3dexact(t[0], t[1], t[2], t[3]);
}

/** @brief Helper function that calculates a special determinant
 *
 * Determinant of
 *    | a b 1 |
 *    | d e 1 |
 *    | g h 1 |
 *
 * Also twice area of triangle in the plane (a,b),(d,e),(g,h).
 * Also can be used to determine orientation of the three points.
 * Also points are collinear if result is zero
 * @todo Does the row of 1s correspond to embedding in an affine space. If so, rename.
 */
template< typename Real >
Real orientation_determinant(
	Real a,
	Real b,
	Real d,
	Real e,
	Real g,
	Real h
)
{
	const double ps[3][2] = {{ a, b }, { d, e }, { g, h }};

	return orient2dexact(ps[0], ps[1], ps[2]);
}

template< typename Real >
constexpr Real orientation_determinant_fast(
	Real a,
	Real b,
	Real d,
	Real e,
	Real g,
	Real h
)
{
	return ( a - g ) * ( e - h ) - ( b - h ) * ( d - g );
}

template< typename Real >
Real orientation_determinant_sign(
	Real a,
	Real b,
	Real d,
	Real e,
	Real g,
	Real h
)
{
	const double ps[3][2] = {{ a, b }, { d, e }, { g, h }};

	return orient2d(ps[0], ps[1], ps[2]);
}

/** @brief Calculate the Gram determinant (Gramian) of a set of points
 *
 * Requires that inner(T,T) is defined. Each
 * version calculates the Gram determinant of the set of vectors which is zero
 * iff the vectors are linearly dependent. The determinant is "definitely not
 * zero" if it is larger than the value LINEAR_DEPENDENCE_EPSILON. If it less
 * than or equal to this amount it is either 1) treated as zero, or 2) improved
 * upon by utilizing a more robust algorithm.
 *
 * @note It may be important to note that all terms are >= 0. Could play a
 *   part in robust calculations
 * @note These are all determinants (of symmetrix matrices)
 */
template< typename V >
constexpr auto gramian(const V& x)
{
	return inner(x);
}

// Derived from cos theta = <x,y>/(<x,x>*<y,y>). For parallel and anti-parallel x, y this will be +-1.
// Thus |cos(theta)|=1    ->   |<x,y>|=<x,x>*<y,y>   ->    <x,x>*<y,y>-|<x,y>| == 0
template< typename V >
constexpr auto gramian(
	const V& x,
	const V& y
)
{
	const auto a = inner(x, y);

	return inner(x) * inner(y) - a * a;
}

template< typename V >
constexpr auto gramian(
	const V& x,
	const V& y,
	const V& z
)
{
	auto a = inner(x, y);
	auto b = inner(x, z);
	auto c = inner(y, z);

	const auto t = a * b * c;

	a = a * a;
	b = b * b;
	c = c * c;

	const auto d = inner(x);
	const auto e = inner(y);
	const auto f = inner(z);

	return 2 * t + d * e * f - f * a - e * b - d * c;
}

}

#endif // MATH_LINEAR_ALGEBRA_ORIENTATION_H
