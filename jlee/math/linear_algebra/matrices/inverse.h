#ifndef INVERSE_H
#define INVERSE_H

#include "matrix.h"
#include "transpose.h"

namespace la
{
template< typename T >
matrix< T, 3, 3 > invert(const matrix< T, 3, 3 >& data)
{
	matrix< T, 3, 3 > inv(uninitialized);

	const auto a = data(0, 0);
	const auto b = data(0, 1);
	const auto c = data(0, 2);
	const auto d = data(1, 0);
	const auto e = data(1, 1);
	const auto f = data(1, 2);
	const auto g = data(2, 0);
	const auto h = data(2, 1);
	const auto i = data(2, 2);

	const auto x = -c * e * g + b * f * g + c * d * h - a * f * h - b * d * i + a * e * i;

	inv(0, 0) = ( -f * h + e * i ) / x;
	inv(0, 1) = ( c * h - b * i ) / x;
	inv(0, 2) = ( -c * e + b * f ) / x;
	inv(1, 0) = ( f * g - d * i ) / x;
	inv(1, 1) = ( -c * g + a * i ) / x;
	inv(1, 2) = ( c * d - a * f ) / x;
	inv(2, 0) = ( -e * g + d * h ) / x;
	inv(2, 1) = ( b * g - a * h ) / x;
	inv(2, 2) = ( -b * d + a * e ) / x;

	return inv;
}

// inverse of an orthogonal matrix is just the transpose
template< typename T, std::size_t D >
matrix< T, D, D > invert(
	const matrix< T, D, D >& data,
	orthogonal_matrix_hint
)
{
	return transpose(data);
}

}

#endif // INVERSE_H
