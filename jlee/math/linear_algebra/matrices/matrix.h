#ifndef MATH_LINEAR_ALGEBRA_MATRIX_H
#define MATH_LINEAR_ALGEBRA_MATRIX_H

#include <cmath>
#include <cstddef>

#include "util/initializers.h"

namespace la
{
enum orthogonal_matrix_hint
{
	orthogonal_matrix
};

/// @todo Take hint as template parameter and have a generic_matrix hint, square_matrix hint,
/// symmetric_matrix
template< typename T, std::size_t R, std::size_t C >
class matrix
{
public:
	explicit matrix(uninitialized_type)
	{
	}

	// create a diagonal matrix. Works for rectangular matrices as well as square
	explicit matrix(T x)
		: data()
	{
		for (std::size_t i = 0; i < ( R < C ? R : C ); ++i)
		{
			data[i][i] = x;
		}
	}

	T& operator()(
		std::size_t i,
		std::size_t j
	)
	{
		return data[i][j];
	}

	const T& operator()(
		std::size_t i,
		std::size_t j
	) const
	{
		return data[i][j];
	}

	matrix& operator*=(T s)
	{
		for (unsigned i = 0; i < R; ++i)
		{
			for (unsigned j = 0; j < C; ++j)
			{
				data[i][j] *= s;
			}
		}

		return *this;
	}
private:
	T data[R][C];
};

namespace detail
{
// multiplication and place result in m. m must not be the same as l or r
template< typename T, std::size_t R, std::size_t X, std::size_t C >
void multiply(
	const matrix< T, R, X >& l,
	const matrix< T, X, C >& r,
	matrix< T, X, C >&       m
)
{
	for (std::size_t i = 0; i < R; ++i)
	{
		for (std::size_t j = 0; j < C; ++j)
		{
			T x = 0;

			for (std::size_t k = 0; k < X; ++k)
			{
				x += l(i, k) * r(k, j);
			}

			m(i, j) = x;
		}
	}
}

}

template< typename T, std::size_t R, std::size_t C >
matrix< T, R, C > operator+(
	const matrix< T, R, C >& l,
	const matrix< T, R, C >& r
)
{
	matrix< T, R, C > m(uninitialized);

	for (std::size_t i = 0; i < R; ++i)
	{
		for (std::size_t j = 0; j < C; ++j)
		{
			m(i, j) = l(i, j) + r(i, j);
		}
	}

	return m;
}

template< typename T, std::size_t R, std::size_t C >
matrix< T, R, C > operator-(
	const matrix< T, R, C >& l,
	const matrix< T, R, C >& r
)
{
	matrix< T, R, C > m(uninitialized);

	for (std::size_t i = 0; i < R; ++i)
	{
		for (std::size_t j = 0; j < C; ++j)
		{
			m(i, j) = l(i, j) - r(i, j);
		}
	}

	return m;
}

template< typename T, std::size_t R, std::size_t X, std::size_t C >
matrix< T, R, C > operator*(
	const matrix< T, R, X >& l,
	const matrix< T, X, C >& r
)
{
	matrix< T, R, C > m(uninitialized);

	detail::multiply(l, r, m);

	return m;
}

template< typename S, typename T, std::size_t R, std::size_t C >
matrix< T, R, C > operator*(
	S                        s,
	const matrix< T, R, C >& r
)
{
	matrix< T, R, C > m = r;

	m *= s;

	return m;
}

template< typename S, typename T, std::size_t R, std::size_t C >
matrix< T, R, C > operator*(
	const matrix< T, R, C >& r,
	S                        s
)
{
	matrix< T, R, C > m = r;

	m *= s;

	return m;
}

}

#endif // MATH_LINEAR_ALGEBRA_MATRIX_H
