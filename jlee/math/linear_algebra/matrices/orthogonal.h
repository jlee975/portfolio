#ifndef ORTHOGONAL_H
#define ORTHOGONAL_H

#include "matrix.h"
#include "transpose.h"

namespace la
{
namespace detail
{
constexpr float ORTHOGONAL_TOLERANCE = 1e-6f;

/// @todo Essentially testing nearness of transpose(m)*m to I. May want to decompose into other functions
template< typename R, std::size_t D >
bool is_orthogonal(const matrix< R, D, D >& data)
{
	for (std::size_t i = 0; i < D; ++i)
	{
		for (std::size_t j = 0; j < D; ++j)
		{
			if ( i != j )
			{
				R x = 0;

				for (std::size_t k = 0; k < D; ++k)
				{
					x += data(i, k) * data(j, k);
				}

				// should be zero
				if ( std::abs(x) > ORTHOGONAL_TOLERANCE )
				{
					return false;
				}
			}
		}
	}

	return true;
}

}

// compute the nearest orthogonal matrix
template< typename R, std::size_t D >
void orthogonalize(matrix< R, D, D >& m)
{
	/// @bug This has the potential to be an infinite loop. After a certain number of steps, use SVD or reset the matrix to I
	while ( !detail::is_orthogonal(m) )
	{
		// Iterative improvement
		/// @todo n could probably be calculated more efficiently. Especially since we've probably done most of the work in is_orthogonal
		auto n = transpose(m) * m;

		auto p = 0.5 * m * n;
		m = 2 * m + p * n - 3 * p;
	}
}

}

#endif // ORTHOGONAL_H
