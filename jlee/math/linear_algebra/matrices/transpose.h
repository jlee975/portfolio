#ifndef TRANSPOSE_H
#define TRANSPOSE_H

#include "matrix.h"

namespace la
{
/// @todo Move this and other "utility" functions to their own headers
template< typename T, std::size_t D >
matrix< T, D, D > transpose(const matrix< T, D, D >& data)
{
	matrix< T, D, D > inv(uninitialized);

	for (std::size_t i = 0; i < D; ++i)
	{
		for (std::size_t j = 0; j < D; ++j)
		{
			inv(i, j) = data(j, i);
		}
	}

	return inv;
}

}

#endif // TRANSPOSE_H
