#ifndef COORDINATES_H
#define COORDINATES_H

#include <array>

namespace la
{
/// @todo Specialization for homogeneous tuples and pairs
template< typename >
struct vector_dimension;

template< typename T, std::size_t N >
struct vector_dimension< T [N] >
{
	static constexpr std::size_t value = N;
};

template< typename T, std::size_t N >
struct vector_dimension< std::array< T, N > >
{
	static constexpr std::size_t value = N;
};

template< typename T >
constexpr std::size_t vector_dimension_v = vector_dimension< T >::value;

template< std::size_t I, typename T >
constexpr auto coordinate(const T& t)
{
	return t[I];
}

}
#endif // COORDINATES_H
