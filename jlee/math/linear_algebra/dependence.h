/** @todo Separate linear algebra stuff from geometry stuff
 */
#ifndef MATH_LINEAR_ALGEBRA_DEPENDENCE_HPP
#define MATH_LINEAR_ALGEBRA_DEPENDENCE_HPP

#include "determinants.h"
#include <stdexcept>

namespace la
{
// If Gram determinant is > this value, we know the vectors are linearly independent
// If <= this value, we might treat as linearly dependent or use a stronger test
const float LINEAR_DEPENDENCE_TOLERANCE = 1e-6f;

/** @brief Test if points are linearly dependent
 *
 * @note that gramian is >= 0 (mathematically)
 *
 * At this time, there is no alternative calculation.
 *
 * @todo Provide a variadic template for more than 3 args
 */
template< typename V >
constexpr bool linearly_dependent(const V& x)
{
	return gramian(x) <= LINEAR_DEPENDENCE_TOLERANCE;
}

template< typename V >
constexpr bool linearly_dependent(
	const V& x,
	const V& y
)
{
	return gramian(x, y) <= LINEAR_DEPENDENCE_TOLERANCE;
}

template< typename V >
constexpr bool linearly_dependent(
	const V& x,
	const V& y,
	const V& z
)
{
	return gramian(x, y, z) <= LINEAR_DEPENDENCE_TOLERANCE;
}

template< typename V >
bool linearly_dependent(
	const V*    p,
	std::size_t n
)
{
	throw std::logic_error("Not implemented");
}

}

#endif // MATH_LINEAR_ALGEBRA_DEPENDENCE_HPP
