#ifndef MATH_LINEAR_ALGEBRA_EUCLIDEAN_H
#define MATH_LINEAR_ALGEBRA_EUCLIDEAN_H

#include <array>
#include <cmath>
#include <stdexcept>
#include <type_traits>

#include "../robust.h"
#include "coordinates.h"

namespace la
{
namespace details
{
template< typename V1, typename V2, std::size_t... Is >
constexpr auto inner_helper(const V1& a, const V2& b, std::index_sequence< Is... >)
{
	return fp::sum(fp::robust_product(coordinate< Is >(a), coordinate< Is >(b))...);
}

template< typename V1, typename V2, std::size_t... Is >
constexpr auto inner_fast_helper(const V1& a, const V2& b, std::index_sequence< Is... >)
{
	return (... + (coordinate< Is >(a) * coordinate< Is >(b)));
}

template< typename V, std::size_t... Is >
constexpr auto inner_helper(const V& a, std::index_sequence< Is... >)
{
	return fp::sum(fp::robust_square(coordinate< Is >(a))...);
}

template< typename V, std::size_t... Is >
constexpr auto pnorm1_helper(const V& a, std::index_sequence< Is... >)
{
	return (... + jl::abs(coordinate< Is >(a)));
}

template< typename V, std::size_t... Is >
constexpr auto pnorminf_helper(const V& a, std::index_sequence< Is... >)
{
	return jl::max(jl::abs(a[Is])...);
}

}

template< typename V1, typename V2, std::size_t N = std::min(vector_dimension_v< V1 >, vector_dimension_v< V2 >) >
constexpr auto inner_exact(const V1& a, const V2& b)
{
	return details::inner_helper(a, b, std::make_index_sequence< N >());
}

template< typename V, std::size_t N = vector_dimension_v< V > >
constexpr auto inner_exact(const V& a)
{
	return details::inner_helper(a, std::make_index_sequence< N >());
}

/** Inner product for vector-likes
 *
 * Exact up to floating point precision, and may be relatively expensive.
 *
 * @todo Kahan summation for accuracy
 * @todo Want a version of inner product that switches to double precision when the result is
 * nearly zero. Many functions test dot <= 0 etc.
 * @todo accept different types so we can for example do dot products with rays and points, or points and arrays, etc.
 * @todo Allow mixing of arithmetic types. Ex., float and double mixed will promote to double
 */
/* Some bad vectors. n.Wi are not *quite* zero
   const geo::point< 3 > n = { {0x1.9e377ap+3,  0x1p+3, -0x1.4f1bbcp+4} };
   const geo::point< 3 > W0 = { {0x1.1e377ap+1, -0x1p+0, 0x1p+0} };
   const geo::point< 3 > W1 = { {-0x1.0f1bbcp+2, -0x1p+0,-0x1.8p+1}};
   const geo::point< 3 > W2 = { {-0x1.8p+1, 0x1.1e377ap+1,-0x1p+0}};

   n.W0 = 1.54096*10^-6
   n.W1 = -3.62156*10^-7
   n.W2 = -9.53674*10^-7
 */
template< typename V1, typename V2 >
constexpr auto inner(const V1& a, const V2& b)
{
	return fp::approximate(inner_exact(a, b));
}

// inner product with self. Avoids the extra loads from memory
template< typename V >
constexpr auto inner(const V& a)
{
	return fp::approximate(inner_exact(a));
}

/** @brief Inner product using straight-forward math
 *
 * This is the straightforward implementation of the dot product (ex., a0*b0+a1*b1+a2*c2 using regular floating point
 * math). It is usually good enough, but may have floating point error. Especially around zero, the sign may change
 * or the result may not be exactly zero.
 */
template< typename V1, typename V2, std::size_t N = std::min(vector_dimension_v< V1 >, vector_dimension_v< V2 >) >
constexpr auto inner_fast(const V1& a, const V2& b)
{
	return details::inner_fast_helper(a, b, std::make_index_sequence< N >());
}

/** @brief Inner product calculated with enough precision to guarantee sign is correct
 *
 * @todo Adaptable implementation that only uses exact math if needed
 */
template< typename V1, typename V2 >
constexpr auto inner_sign(const V1& a, const V2& b)
{
	return inner(a, b);
}

/** p-norm of L2
 *
 * Also Euclidean norm, Euclidean distance, etc. Equivalent to square root of the inner product.
 *
 * Implemented using hypot.
 *
 * @todo Accept any vector-like
 * @todo Specialization for 2D by just calling hypot
 * @todo Specialization for 3D can probably be done cheaper. Branchless sort the three values, divide through
 * by the max, take sqrt and multiply by abs(max).
 */
template< typename V, std::size_t N = vector_dimension_v< V > >
auto pnorm2(const V& a)
{
	auto x = std::fabs(a[0]);

	for ( std::size_t i = 1; i < N; ++i )
	{
		x = std::hypot(x, a[i]);
	}

	return x;
}

template< typename V, std::size_t N = vector_dimension_v< V > >
constexpr auto pnorm1(const V& a)
{
	return details::pnorm1_helper(a, std::make_index_sequence< N >());
}

template< typename V, std::size_t N = vector_dimension_v< V > >
constexpr auto pnorminf(const V& a)
{
	return details::pnorminf_helper(a, std::make_index_sequence< N >());
}

/** Check for a zero array
 *
 * @todo Prefer pnorminf since it has no chance of overflow
 * @todo Could bail on the first non-zero element
 */
template< typename V >
constexpr bool zero(const V& a)
{
	return pnorminf(a) == 0;
}

/// @todo constexpr then make perp1 constexpr
template< typename V, std::size_t N = vector_dimension_v< V > >
V normalize(const V& a)
{
	const auto h = pnorm2(a);

	if ( h > 0 )
	{
		const auto x = 1 / h;

		V b(uninitialized);

		for ( std::size_t i = 0; i < N; ++i )
		{
			b[i] = a[i] * x;
		}

		return b;
	}

	return a;
}

namespace details
{
template< typename V, std::size_t... Is >
constexpr auto equal_helper(const V& a, const V& b, std::index_sequence< Is... >)
{
	return (... + jl::abs(coordinate< Is >(a) - coordinate< Is >(b))) == 0;
}

}

template< typename V, std::size_t N = vector_dimension_v< V > >
constexpr auto equal(const V& a, const V& b)
{
	return details::equal_helper(a, b, std::make_index_sequence< N >());
}

/**
 * @todo Increased accuracy would only be needed for vectors that are nearly
 *    the same in every component (so that return is O). I which case we are
 *    calculating ae-bd and ae~bd. Define x = sqrt(ae). Rewrite ae as
 *    (x+y)(x+z) and bd as (x+u)(x+v). In subtracting, x^2 terms cancel, which
 *    gets rid of high bits, leaving the low bits
 */
template< typename V >
constexpr void perp(const V& a, const V& b, V& c)
{
	static_assert(std::extent_v< V > == 3);

	c[0] = a[1] * b[2] - a[2] * b[1];
	c[1] = a[2] * b[0] - a[0] * b[2];
	c[2] = a[0] * b[1] - a[1] * b[0];

#if 0
// This is Gaussian elimination with pivoting to find the null space of
// the matrix [a b]^T. It's not "hands down" better than cross product
// Might want to look at it again in the future.

	point< 3, Real > p, q;

	std::size_t idxa = ( std::fabs(a.p[0]) > std::fabs(a.p[1]) ? 0 : 1 );

	if ( std::fabs(a.p[2]) > std::fabs(a.p[idxa]) )
	{
		idxa = 2;
	}

	for ( unsigned j = 0; j < 3; ++j )
	{
		if ( j == idxa )
		{
			p.p[j] = 1;
			q.p[j] = 0;
		}
		else
		{
			const Real t = a.p[j] / a.p[idxa];
			p.p[j] = t;
			q.p[j] = b.p[j] - b.p[idxa] * t;
		}
	}

	const std::size_t idxb = ( idxa + 1 ) % 3;
	const std::size_t jdxb = ( idxa + 2 ) % 3;

	point< 3, Real > r;

	if ( std::fabs(q.p[idxb]) > std::fabs(q.p[jdxb]) )
	{
		const Real t = q.p[idxb];
		q.p[jdxb] /= t;
		p.p[jdxb]  = p.p[jdxb] - p.p[idxb] * q.p[jdxb];

		// jdxb has the non-identity values
		r.p[idxa] = -p.p[jdxb];
		r.p[idxb] = -q.p[jdxb];
		r.p[jdxb] = 1;
	}
	else
	{
		const Real t = q.p[jdxb];
		q.p[idxb] /= t;
		p.p[idxb]  = p.p[idxb] - p.p[jdxb] * q.p[idxb];

		// idxb has the non-identity values
		r.p[idxa] = -p.p[idxb];
		r.p[jdxb] = -q.p[idxb];
		r.p[idxb] = 1;
	}

/// @todo Scale r
	return r;

#endif // if 0
}

/** @brief Return a vector perpendicular to a and b of unit length
 *
 * The reason to not simply use the cross product is that this might be replaced
 * later with a more numerically stable algorithm
 *
 * @todo Normalize a and b
 *
 * v1 = a b c
 * v2 = d e f
 *      x y z
 *
 * ax+by+cz = 0
 * dx+ey+fz = 0
 * xx+yy+zz = 1
 */
template< typename V >
void perp1(const V& a, const V& b, V& c)
{
	perp(a, b, c);
	c = normalize(c);

	/// @todo Fixup with Gram-Schmidt process, eigenvalue algorithm, QR decomp...
}

}

#endif // MATH_LINEAR_ALGEBRA_EUCLIDEAN_H
