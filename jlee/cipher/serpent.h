#ifndef SERPENT_HPP
#define SERPENT_HPP

#include "cipher.h"

class Serpent
	: public BlockCipher
{
public:
	Serpent(const unsigned char*, unsigned, unsigned);
	~Serpent() override;

	static constexpr unsigned supported_key_sizes[5]   = { 128, 160, 192, 224, 256 };
	static constexpr unsigned supported_block_sizes[1] = { 128 };
private:
	void inverseTransform(std::uint_least32_t*) override;
	void transform(std::uint_least32_t*) override;
	unsigned long keySchedule[140];
};

#endif // #ifndef SERPENT_HPP
