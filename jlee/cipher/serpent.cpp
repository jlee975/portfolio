/*=========================================================================*//**

   AES Candidate

   Notes

   1. The paper "Speeding Up Serpent" by Dag Arne Osvik describes how the Serpent
   sboxes can be calculated using bitwise operations only. In that paper he uses
   the register name r0, r1, r2, r3, r4. Here, I have implemented his ideas but
   have renamed the registers r, s, t, u, and v respectively. I did this mostly to
   make the lines shorter in the source, and really for no other reason.

   Also note that I have not arranged the S-Boxes in a switch statement, but as
   nested ifs. This is for performance reasons; the function executes about 25%
   faster by NOT using the switch. I'm sure this is because of branch prediction,
   or a similar effect.

   2. The Serpent specification allows any non-zero bit length for the key. I have
   implemented the same key lengths as Rijndael here, i.e., 128, 160, 192, 224, and
   256 bits. Support for other key lengths may be added in the future.

   In the mean time, a wrapper function can implement any key length easily. This
   is largely why I haven't broke down and done this. Every key less than 256 bits
   is merely padded to make a full 256. Padding is done by adding a 1 bit after the
   MSB and then enough 0 bits to make a total of 256 bits. "Serpent" can then be
   called with this 256-bit key.

   Optimization

   I think this is mostly down to tweaks although it doesn't perform as well as I'd
   hoped. However, it does perform similarly to the figures quoted in the
   submission (i.e., 1480 clocks per 128-bit block). Neither the S-boxes nor the
   linear transformation seem to have room for optimization. So there's just a list
   of little things:

   \todo

   - The order in which bytes are read from arrays is ugly.
   - There is room for parallelization, but the question is how to applyit? In ECB
   mode, it would benefit us greatly to calculate neighbouring blocks. Or in
   counter mode. Otherwise I'm not sure.
   - Certain variables, like temp and Nk, are so short lived that I wonder if
   there's a better way to implement them. With temp, at least, there is the
   possibility of storing the non-rotated keys (remember to shift phi and i xors)
   and applying the rotate after the S-box. Remember that the S-box does not care
   what order the bytes are in.
   - Each S-box has a few instructions appended to ensure that the data comes out
   in the right order. The S-boxes as provided do not guarantee that so it has
   been done manually. This is mostly straight-forward assignment, but some can
   probably be removed by changing operations around
   - It would be nice if the Key S-box and encryption S-box were overlapped to
   avoid the extra code

   References

   "Serpent: A Candidate Block Cipher for the Advanced Encryption Standard",
   Anderson, Biham, Knudsen, http://www.cl.cam.ac.uk/~rja14/serpent.html

   "Speeding Up Serpent", Dag Arne Osvik, http://www.ii.uib.no/~osvik/pub/aes3.pdf

   \todo: Ensure no operations overflow the 32bits in an unsigned long
*//*==========================================================================*/

#include "serpent.h"

#include <stdexcept>

#include "bits/rotate.h"

namespace
{
void SBOX(unsigned, unsigned long&, unsigned long&, unsigned long&, unsigned long&);
void InverseSBOX(unsigned, unsigned long&, unsigned long&, unsigned long&, unsigned long&);
}

// \todo: document key format. i.e., "0x1234567890" is stored as {0x12, 0x34,...}
// Big endian. first unsigned char has msb. Anything in first char which
// is past msb is discarded. This means lsb is aligned to 0 bit of last
// char.
// \todo: key class?
Serpent::Serpent(
	const unsigned char* key,
	unsigned             ks,
	unsigned             bs
)
	: BlockCipher(128, false)
{
	if ( ( key == nullptr ) || ks == 0 || ks > 256 || bs != 128 )
	{
		throw std::invalid_argument("Unsupported keysize for Serpent");
	}

	// Pad key to 256 bits
	for (unsigned i = 0; i < 8; ++i)
	{
		keySchedule[i] = 0;
	}

	for (unsigned i = 0; i < ( ( ks + 7 ) / 8 ); ++i)
	{
		keySchedule[i / 4] |= ( static_cast< unsigned long >( key[( ( ks + 7 ) / 8 ) - i - 1] ) << ( ( i % 4 ) * 8 ) );
	}

	if ( ks < 256 )
	{
		keySchedule[ks / 32] &= ( 1UL << ( ks % 32 ) ) - 1UL;
		keySchedule[ks / 32] |= ( 1UL << ( ks % 32 ) );
	}

	for (unsigned i = 0; i < 132; i++) // Key Expansion
	{
		keySchedule[i + 8] = rol32(0x9E3779B9 ^ i ^ keySchedule[i] ^ keySchedule[i + 3] ^ keySchedule[i + 5]
			^ keySchedule[i + 7],
			11);
	}

	unsigned long r;

	unsigned long s;

	unsigned long t;

	unsigned long u;

	for (unsigned i = 0; i < 33; i++)
	{
		r = keySchedule[4 * i + 8];
		s = keySchedule[4 * i + 9];
		t = keySchedule[4 * i + 10];
		u = keySchedule[4 * i + 11];

		SBOX(4 ^ ( 71 - i ), r, s, t, u);

		keySchedule[4 * i]     = r;
		keySchedule[4 * i + 1] = s;
		keySchedule[4 * i + 2] = t;
		keySchedule[4 * i + 3] = u;
	}

	r = s = t = u = 0; // security clear
}

Serpent::~Serpent()
{
	for (unsigned i = 0; i < 140; i++)
	{
		keySchedule[i] = 0;
	}
}

void Serpent::inverseTransform(std::uint_least32_t* C // \todo: was an array of 4 longs
)
{
	unsigned long u = C[0] ^ keySchedule[131];
	unsigned long t = C[1] ^ keySchedule[130];
	unsigned long s = C[2] ^ keySchedule[129];
	unsigned long r = C[3] ^ keySchedule[128];

	for (unsigned i = 31; i > 0; i--)
	{
		InverseSBOX(i, r, s, t, u);

		r ^= keySchedule[4 * i];
		s ^= keySchedule[4 * i + 1];
		t ^= keySchedule[4 * i + 2];
		u ^= keySchedule[4 * i + 3];

		t  = rol32(t, 10); // Inverse linear transformation
		r  = rol32(r, 27);
		t ^= ( u ^ ( s << 7 ) ) & 0xFFFFFFFFUL;
		r ^= s ^ u;
		u  = rol32(u, 25);
		s  = rol32(s, 31);
		u ^= ( t ^ ( r << 3 ) ) & 0xFFFFFFFFUL;
		s ^= t ^ r;
		t  = rol32(t, 29);
		r  = rol32(r, 19);
	}

	InverseSBOX(0, r, s, t, u);

	C[0] = u ^ keySchedule[3]; // Round key subtraction and store
	C[1] = t ^ keySchedule[2];
	C[2] = s ^ keySchedule[1];
	C[3] = r ^ keySchedule[0];

	r = s = t = u = 0; // security clear
}

void Serpent::transform(   // encrypt
	std::uint_least32_t* C // \todo: was an array of 4 longs
)
{
	unsigned long u = C[0] ^ keySchedule[3];
	unsigned long t = C[1] ^ keySchedule[2];
	unsigned long s = C[2] ^ keySchedule[1];
	unsigned long r = C[3] ^ keySchedule[0];

	SBOX(0, r, s, t, u);

	for (unsigned i = 1; i < 32; i++) // Substitution-Permutation Network
	{
		r  = rol32(r, 13); // Linear Transform
		t  = rol32(t, 3);
		s ^= r ^ t;
		u ^= ( t ^ ( r << 3 ) ) & 0xFFFFFFFFUL;
		s  = rol32(s, 1);
		u  = rol32(u, 7);
		r ^= s ^ u;
		t ^= ( u ^ ( s << 7 ) ) & 0xFFFFFFFFUL;
		r  = rol32(r, 5);
		t  = rol32(t, 22);

		r ^= keySchedule[4 * i];
		s ^= keySchedule[4 * i + 1];
		t ^= keySchedule[4 * i + 2];
		u ^= keySchedule[4 * i + 3];

		SBOX(i, r, s, t, u);
	}

	C[0] = u ^ keySchedule[131]; // Round key addition and store
	C[1] = t ^ keySchedule[130];
	C[2] = s ^ keySchedule[129];
	C[3] = r ^ keySchedule[128];
	r    = s = t = u = 0; // security clear
}

namespace
{
void SBOX(
	unsigned       i,
	unsigned long& r,
	unsigned long& s,
	unsigned long& t,
	unsigned long& u
)
{
	unsigned long v = 0;

	switch ( i % 8 )
	{
	case 0:
		u ^= r;
		v  = s;
		s &= u;
		v ^= t;
		s ^= r;
		r |= u;
		r ^= v;
		v ^= u;
		u ^= t;
		t |= s;
		t ^= v;
		v ^= 0xFFFFFFFFUL;
		v |= s;
		s ^= u;
		s ^= v;
		u |= r;
		s ^= u;
		v ^= u;
		u  = r;
		r  = s;
		s  = v;
		break;
	case 1:
		r ^= 0xFFFFFFFFUL;
		t ^= 0xFFFFFFFFUL;
		v  = r;
		r &= s;
		t ^= r;
		r |= u;
		u ^= t;
		s ^= r;
		r ^= v;
		v |= s;
		s ^= u;
		t |= r;
		t &= v;
		r ^= s;
		s &= t;
		s ^= r;
		r &= t;
		v ^= r;
		r  = t;
		t  = u;
		u  = s;
		s  = v;
		break;
	case 2:
		v  = r;
		r &= t;
		r ^= u;
		t ^= s;
		t ^= r;
		u |= v;
		u ^= s;
		v ^= t;
		s  = u;
		u |= v;
		u ^= r;
		r &= s;
		v ^= r;
		s ^= u;
		s ^= v;
		v ^= 0xFFFFFFFFUL;
		r  = t;
		t  = s;
		s  = u;
		u  = v;
		break;
	case 3:
		v  = r;
		r |= u;
		u ^= s;
		s &= v;
		v ^= t;
		t ^= u;
		u &= r;
		v |= s;
		u ^= v;
		r ^= s;
		v &= r;
		s ^= u;
		v ^= t;
		s |= r;
		s ^= t;
		r ^= u;
		t  = u;
		u |= s;
		r ^= u;
		u  = v;
		break;
	case 4:
		s ^= u;
		u ^= 0xFFFFFFFFUL;
		t ^= u;
		u ^= r;
		v  = s;
		s &= u;
		s ^= t;
		v ^= u;
		r ^= v;
		t &= v;
		t ^= r;
		r &= s;
		u ^= r;
		v |= s;
		v ^= r;
		r |= u;
		r ^= t;
		t &= u;
		r ^= 0xFFFFFFFFUL;
		v ^= t;
		t  = r;
		r  = s;
		s  = v;
		break;
	case 5:
		r ^= s;
		s ^= u;
		u ^= 0xFFFFFFFFUL;
		v  = s;
		s &= r;
		t ^= u;
		s ^= t;
		t |= v;
		v ^= u;
		u &= s;
		u ^= r;
		v ^= s;
		v ^= t;
		t ^= r;
		r &= u;
		t ^= 0xFFFFFFFFUL;
		r ^= v;
		v |= u;
		v ^= t;
		t  = r;
		r  = s;
		s  = u;
		u  = v;
		break;
	case 6:
		t ^= 0xFFFFFFFFUL;
		v  = u;
		u &= r;
		r ^= v;
		u ^= t;
		t |= v;
		s ^= u;
		t ^= r;
		r |= s;
		t ^= s;
		v ^= r;
		r |= u;
		r ^= t;
		v ^= u;
		v ^= r;
		u ^= 0xFFFFFFFF;
		t &= v;
		u ^= t;
		t  = v;
		break;
	case 7:
		v  = s;
		s |= t;
		s ^= u;
		v ^= t;
		t ^= s;
		u |= v;
		u &= r;
		v ^= t;
		u ^= s;
		s |= v;
		s ^= r;
		r |= v;
		r ^= t;
		s ^= v;
		t ^= s;
		s &= r;
		s ^= v;
		t ^= 0xFFFFFFFFUL;
		t |= r;
		v ^= t;
		t  = s;
		s  = u;
		u  = r;
		r  = v;
		break;
	} // switch

	// S-box substitution - See Note 1
}

void InverseSBOX(
	unsigned       i,
	unsigned long& r,
	unsigned long& s,
	unsigned long& t,
	unsigned long& u
)
{
	unsigned long v = 0;

	// Inverse S-Box - See Note 1
	switch ( i % 8 )
	{
	case 0:
		t ^= 0xFFFFFFFFUL;
		v  = s;
		s |= r;
		v ^= 0xFFFFFFFFUL;
		s ^= t;
		t |= v;
		s ^= u;
		r ^= v;
		t ^= r;
		r &= u;
		v ^= r;
		r |= s;
		r ^= t;
		u ^= v;
		t ^= s;
		u ^= r;
		u ^= s;
		t &= u;
		v ^= t;
		t  = s;
		s  = v;
		break;
	case 1:
		v  = s;
		s ^= u;
		u &= s;
		v ^= t;
		u ^= r;
		r |= s;
		t ^= u;
		r ^= v;
		r |= t;
		s ^= u;
		r ^= s;
		s |= u;
		s ^= r;
		v ^= 0xFFFFFFFFUL;
		v ^= s;
		s |= r;
		s ^= r;
		s |= v;
		s ^= u;
		u  = t;
		t  = s;
		s  = r;
		r  = v;
		break;
	case 2:
		t ^= u;
		u ^= r;
		v  = u;
		u &= t;
		u ^= s;
		s |= t;
		s ^= v;
		v &= u;
		t ^= u;
		v &= r;
		v ^= t;
		t &= s;
		t |= r;
		u ^= 0xFFFFFFFFUL;
		t ^= u;
		r ^= u;
		r &= s;
		u ^= v;
		u ^= r;
		r  = s;
		s  = v;
		break;
	case 3:
		v  = t;
		t ^= s;
		r ^= t;
		v &= t;
		v ^= r;
		r &= s;
		s ^= u;
		u |= v;
		t ^= u;
		r ^= u;
		s ^= v;
		u &= t;
		u ^= s;
		s ^= r;
		s |= t;
		r ^= u;
		s ^= v;
		r ^= s;
		v  = u;
		u  = r;
		r  = t;
		t  = v;
		break;
	case 4:
		v  = t;
		t &= u;
		t ^= s;
		s |= u;
		s &= r;
		v ^= t;
		v ^= s;
		s &= t;
		r ^= 0xFFFFFFFFUL;
		u ^= v;
		s ^= u;
		u &= r;
		u ^= t;
		r ^= s;
		t &= r;
		u ^= r;
		t ^= v;
		t |= u;
		u ^= r;
		t ^= s;
		s  = u;
		u  = v;
		break;
	case 5:
		s ^= 0xFFFFFFFFUL;
		v  = u;
		t ^= s;
		u |= r;
		u ^= t;
		t |= s;
		t &= r;
		v ^= u;
		t ^= v;
		v |= r;
		v ^= s;
		s &= t;
		s ^= u;
		v ^= t;
		u &= v;
		v ^= s;
		u ^= v;
		v ^= 0xFFFFFFFFUL;
		u ^= r;
		r  = s;
		s  = v;
		v  = u;
		u  = t;
		t  = v;
		break;
	case 6:
		r ^= t;
		v  = t;
		t &= r;
		v ^= u;
		t ^= 0xFFFFFFFFUL;
		u ^= s;
		t ^= u;
		v |= r;
		r ^= t;
		u ^= v;
		v ^= s;
		s &= u;
		s ^= r;
		r ^= u;
		r |= t;
		u ^= s;
		v ^= r;
		r  = s;
		s  = t;
		t  = v;
		break;
	case 7:
		v  = t;
		t ^= r;
		r &= u;
		v |= u;
		t ^= 0xFFFFFFFFUL;
		u ^= s;
		s |= r;
		r ^= t;
		t &= v;
		u &= v;
		s ^= t;
		t ^= r;
		r |= t;
		v ^= s;
		r ^= u;
		u ^= v;
		v |= r;
		u ^= t;
		v ^= t;
		t  = s;
		s  = r;
		r  = u;
		u  = v;
		break;
	} // switch

}

}
