/*=========================================================================*//**
   \file        arcfour.hpp
   \author      Jonathan Lee
*//*==========================================================================*/

#ifndef ARC4_HPP
#define ARC4_HPP

#include "cipher.h" // For StreamCipher class
#include <array>
#include <cstddef>

class ARC4
	: public StreamCipher
{
public:
	explicit ARC4(const char* s);
	ARC4(const std::byte*, std::size_t, unsigned = 0);
	~ARC4() override;
private:
	std::byte next() override;

	std::array< std::byte, 256 > S;
	unsigned                     ii;
	unsigned                     jj;
};

#endif // #ifndef ARC4_HPP
