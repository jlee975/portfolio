/*=========================================================================*//**

   Code specific to the Rijndael Block Cipher and AES

*//*==========================================================================*/

#ifndef RIJNDAEL_HPP
#define RIJNDAEL_HPP

#include "cipher.h"

class Rijndael
	: public BlockCipher
{
public:
	Rijndael(const unsigned char*, unsigned, unsigned);
	~Rijndael() override;

	static constexpr unsigned supported_key_sizes[5]   = { 128, 160, 192, 224, 256 };
	static constexpr unsigned supported_block_sizes[5] = { 128, 160, 192, 224, 256 };
private:
	// Virtual members from BlockCipher
	void transform(std::uint_least32_t*) override;
	void inverseTransform(std::uint_least32_t*) override;

	// Rijndael helper functions
	unsigned long mixColumns(unsigned long) const;
	unsigned long invMixColumns(unsigned long) const;
	unsigned long subBytes(unsigned long) const;

	unsigned long keySchedule[120];

	static const unsigned char gf254[256];
	static const unsigned char invShiftRows[9][8][3];
	static const unsigned char shiftRows[9][8][3];
	static const unsigned long U[256];
	static const unsigned long T[256];

	unsigned Nr;
	unsigned Nb;
};
#endif // ifndef RIJNDAEL_HPP
