#ifndef MOO_H
#define MOO_H

namespace cipher
{
// TODO EAX, CTR, ICM, SIC, CCM, Galois Counter Mode
enum Mode
{
	ECB,
	OFB,
	CFB,
	CBC
};
}

#endif // MOO_H
