/*=========================================================================*//**

   An implementation of the Rijndael block Cipher which is used in the
   Advanced Encryption Standard (FIPS PUB 197). Although AES is much more
   restricted than Rijndael in terms of block size and key size, here the full
   algorithm is provided. Some wrapper functions for AES are also provided for
   those who prefer it.

   The first four parameters indicate what buffers to use and how big they are.
   The key parameter is a pointer to a unsigned char array which has the value
   of the key stored _in big endian byte order_.

   The direction of the encryption can be set with the CIPHER_ENCRYPT or
   CIPHER_DECRYPT flag. Simply "or" the value with any other flags.

   IMPORTANT: Rijndael accepts five sizes of keys and five sizes of blocks.
   There is _no_ default value for either parameter. They _must_ both be set
   in the flags in order to operate. Use one of these flags for block length:

   RIJNDAEL_BLOCK_128
   RIJNDAEL_BLOCK_160
   RIJNDAEL_BLOCK_192
   RIJNDAEL_BLOCK_224
   RIJNDAEL_BLOCK_256

   and one of these to specify the key length:

   RIJNDAEL_KEY_128
   RIJNDAEL_KEY_160
   RIJNDAEL_KEY_192
   RIJNDAEL_KEY_224
   RIJNDAEL_KEY_256

   This algorithm is most commonly used in the Advanced Encryption Standard and
   so three wrapper functions have been provided. The arguments and return
   values are the same as Rijndael; the wrappers only adjust the flag argument
   so the appropriate blocksize and keysize is used.

   AES128(...)
   AES192(...)
   AES256(...)

   If using one of these functions it is _not_ necessary to specifiy the block
   and key size as they are set when Rijndael is called within them. AES only
   uses 128-bit blocks and only 128-, 192-, and 256-bit key sizes. The function
   name thus determines the parameters. Note that calling these functions with
   different block and key lengths will not change the 128-bit block size or
   the respective key size. Those flags are masked out and then set explicitly.
   Use "Rijndael" if you need to do that.

   Return Value

   CIPHER_OK - No errors. Data was successfully en/de-crypted

   CIPHER_ERROR_LENGTH - The input was not an even number of blocks and no
   padding was specified

   CIPHER_ERROR_DESTSIZE - The output buffer is not big enough to contain
   the ciphered text.

   CIPHER_ERROR_BLOCKSIZE - It is possible to pass an invalide blocksize (or
   alternatively, not specifiy one). If this is the case, the algorithm
   fails with this error message. The output is not touched

   CIPHER_ERROR_KEYSIZE - It is possible to pass an invalid keysize (or to
   not specify one).

   Notes

   1. The arrays T and U have been introduced to simplify the round transforms.
   As in the Rijndael proposal, T is an application of the S-Box and the
   MixColumn operation. U is similarly designed, but for the inverse
   transform, and thus is the application of the inverse S-Box and
   InvMixColumn.

   Since MixColumn and InvMixColumn are applied _automatically_ in this way,
   the last round must undo this effect (since neither is applied in the
   last round). That is why the functions appear in the _opposite_ branch
   from what they usually would; InvMixColumn must be applied in the forward
   transform to undo the implicit MixColumn. The same is true for the
   reverse transform.



   2. Shifting the rows is accomplished by table as well. For each _output_
   column, the _input_ column is provided by the table. The table is
   indexed by the value of Nb (0-8), then the row (0-2), and finally
   the column (0-Nb).

   Note that the initial row is omitted since it is the identity operation
   and the table lookup is costly. Therefore, although the table is indexed
   by 0-2 these rows correspond to rows 1-3 in the State.

   The InvShiftRow array is exactly the same in implementation, but of
   course gives the columns for the reverse transform.

   \todo

   Use a switching pointer for State/Work instead of copying back and forth

   Shrink the ShiftRows and InvShiftRows table as each entry only requires 3 bits

   Annoyed with the size of the T and U tables - more than 2KB of tables for this
   routine (twice that if long is 64 bits). T could basically be halved since we
   have it storing [3a a a 2a] so the middle bytes are the same and 3a = a xor 2a

   There are several different kinds of "InvMixColumn" and "MixColumn" in this
   source. Try to resolve this since it is confusing.

   References

   "AES Proposal: Rijndael", Joan Daemen and Vincent Rijmen
   http://www.iaik.tugraz.at/research/krypto/aes/old/~rijmen/rijndael/

*//*==========================================================================*/

#include "rijndael.h"
#include "bits/rotate.h"
#include "endian/endian.h"
#include <stdexcept>

// \todo: Extend syntax with AES variants, or subclass with AES
// \bug do I really want default sizes??
Rijndael::Rijndael(
	const unsigned char* key,
	unsigned             ks,
	unsigned             bs
)
	: BlockCipher(bs, true), Nr(0), Nb(0)
{
	if ( ks != 128 && ks != 160 && ks != 192 && ks != 224 && ks != 256 )
	{
		throw std::invalid_argument("Keysize invalid for Rijndael()");
	}

	if ( bs != 128 && bs != 160 && bs != 192 && bs != 224 && bs != 256 )
	{
		throw std::invalid_argument("Blocksize invalid for Rijndael()");
	}

	unsigned Nk = ks / 32;

	Nb = bs / 32;

	Nr = 6 + ( Nk > Nb ? Nk : Nb ); // Number of rounds = 6 + max(Nk, Nb)

	unsigned rcon = 0x8d; // as in the specification

	for (unsigned i = 0; i < Nk; i++) // Initialize roundkeys
	{
		keySchedule[i] = leread< 32 >(key + 4 * i);
	}

	for (unsigned i = Nk; i < Nb * ( Nr + 1 ); i++) // Calculate roundkeys
	{
		unsigned long K = keySchedule[i - 1];

		if ( i % Nk == 0 ) // Handle special rounds
		{
			rcon = ( ( rcon << 1 ) ^ ( ( rcon & 0x80 ) != 0u ? 0x11b : 0 ) );
			K    = rol32(K, 24);
			K    = subBytes(K) ^ rcon;
		}
		else if ( Nk > 6 && ( i % Nk == 4 ) )
		{
			K = subBytes(K);
		}

		keySchedule[i] = keySchedule[i - Nk] ^ K;
		K              = 0;
	}

	rcon = 0;
	Nk   = 0;
}

Rijndael::~Rijndael()
{
	for (unsigned i = 0; i < 120; ++i)
	{
		keySchedule[i] = 0;
	}

	Nb = 0;
	Nr = 0;
}

void Rijndael::transform(std::uint_least32_t* C // \todo: C must be 8 unsigned longs in size
)
{
	unsigned long work[8];

	for (unsigned j = 0; j < Nb; j++) // Add Round Key
	{
		C[j] ^= keySchedule[j];
	}

	// Substitution-Permutation Network
	for (unsigned r = 1; r <= Nr; r++)
	{
		unsigned long x = 0;

		for (unsigned j = 0; j < Nb; j++)
		{
			// GF inversion, affine transform, and mix columns
			x = T[C[shiftRows[Nb][j][0]] >> 24 & 0xFF];
			x = rol32(x, 8) ^ T[C[shiftRows[Nb][j][1]] >> 16 & 0xFF];
			x = rol32(x, 8) ^ T[C[shiftRows[Nb][j][2]] >> 8 & 0xFF];
			x = rol32(x, 8) ^ T[C[j] & 0xFF];

			// The last round must UNDO the mix columns - See Note 1
			if ( r == Nr )
			{
				x = invMixColumns(x);
			}

			// Add Round Key
			work[j] = x ^ keySchedule[r * Nb + j] ^ 0x63636363UL;
		}

		x = 0;

		// Copy work to state
		for (unsigned j = 0; j < Nb; j++)
		{
			C[j] = work[j];
		}
	}

	for (int i = 0; i < 8; i++)
	{
		work[i] = 0; // security clear
	}
}

// \todo: If we apply MixColumns to U we might be able to
// save the MixCol call for round == 1. To do this
// we'd also need to pull x into InvMixCol for
// round != 1
void Rijndael::inverseTransform(std::uint_least32_t* C)
{
	unsigned long work[8];

	// Add Round Key
	for (unsigned j = 0; j < Nb; j++)
	{
		C[j] ^= keySchedule[Nr * Nb + j];
	}

	// Substitution-Permutation Network
	for (unsigned round = Nr; round > 0; round--)
	{
		for (unsigned j = 0; j < Nb; j++)
		{
			unsigned long x;

			// Apply InvSubBytes and InvMixColumn by table lookup
			x = U[C[invShiftRows[Nb][j][0]] >> 24 & 0xFF];
			x = U[C[invShiftRows[Nb][j][1]] >> 16 & 0xFF] ^ rol32(x, 8);
			x = U[C[invShiftRows[Nb][j][2]] >> 8 & 0xFF] ^ rol32(x, 8);
			x = U[C[j] & 0xFF] ^ rol32(x, 8);

			// Last round must UNDO InvMixColumn - See Note 1
			if ( round != 1 )
			{
				work[j] = x ^ invMixColumns(keySchedule[( round - 1 ) * Nb + j]);
			}
			else
			{
				work[j] = mixColumns(x) ^ keySchedule[( round - 1 ) * Nb + j];
			}

			x = 0; // security clear
		}

		// Copy work to state
		for (unsigned j = 0; j < Nb; j++)
		{
			C[j] = work[j];
		}
	}

	for (int i = 0; i < 8; i++)
	{
		work[i] = 0; // security clear
	}
}

unsigned long Rijndael::subBytes(unsigned long t) const
{
	unsigned long x;

	unsigned long y;

	x = ( ( static_cast< unsigned long >( gf254[t & 0xFF] ) ) )
	    + ( ( static_cast< unsigned long >( gf254[t >> 8 & 0xFF] ) ) << 8 )
	    + ( ( static_cast< unsigned long >( gf254[t >> 16 & 0xFF] ) ) << 16 )
	    + ( ( static_cast< unsigned long >( gf254[t >> 24 & 0xFF] ) ) << 24 );
	y  = ( ( x << 1 & 0xFEFEFEFEUL ) | ( x >> 7 & 0x01010101UL ) ) ^ ( ( x << 2 & 0xFCFCFCFCUL ) | ( x >> 6 & 0x03030303UL ) );
	x ^= y ^ 0x63636363UL ^ ( ( y << 2 & 0xFCFCFCFCUL ) | ( y >> 6 & 0x03030303UL ) );
	return x;
}

unsigned long Rijndael::mixColumns(unsigned long x) const
{
	unsigned long y;

	y = rol32(x, 24) ^ ( rol32(x, 20) & 0x08080808UL ) ^ rol32(x, 16) ^ ( rol32(x, 18) & 0x02020202UL ) ^ rol32(x, 8)
	    ^ ( rol32(x, 17) & 0x01010101UL ) ^ rol32(x, 25) ^ ( rol32(x, 21) & 0x10101010UL ) ^ ( x << 1 & 0xFEFEFEFEUL )
	    ^ ( x >> 3 & 0x10101010UL ) ^ ( x >> 4 & 0x08080808UL ) ^ ( x >> 6 & 0x02020202UL );
	return y;
}

unsigned long Rijndael::invMixColumns(unsigned long t) const
{
	unsigned long w;

	unsigned long x;

	unsigned long y;

	unsigned long z;

	w  = t;
	x  = ( w << 1 & 0xFEFEFEFEUL ) ^ ( ( w >> 7 & 0x1010101UL ) * 27 );
	y  = ( x << 1 & 0xFEFEFEFEUL ) ^ ( ( x >> 7 & 0x1010101UL ) * 27 );
	z  = ( y << 1 & 0xFEFEFEFEUL ) ^ ( ( y >> 7 & 0x1010101UL ) * 27 );
	w ^= z;
	x ^= ( w >> 16 & 0xFFFFUL ) | ( w << 16 & 0xFFFF0000UL );
	w ^= x;
	x ^= y ^ z ^ ( ( y >> 16 & 0XFFFFUL ) | ( y << 16 & 0xFFFF0000UL ) )
	     ^ ( ( w >> 8 & 0xFFFFFFUL ) | ( w << 24 & 0xFF000000UL ) );

	return x;
}

// The multiplicative inverse of the index in Rijndael's Finite Field
// (which is equal to the index raised to 254th power)
const unsigned char Rijndael::gf254[256]
    = { 0, 1, 141, 246, 203, 82, 123, 209, 232, 79, 41, 192, 176, 225, 229, 199, 116, 180, 170, 75,
	    153, 43, 96, 95, 88, 63, 253, 204, 255, 64, 238, 178, 58, 110, 90, 241, 85, 77, 168, 201,
	    193, 10, 152, 21, 48, 68, 162, 194, 44, 69, 146, 108, 243, 57, 102, 66, 242, 53, 32, 111,
	    119, 187, 89, 25, 29, 254, 55, 103, 45, 49, 245, 105, 167, 100, 171, 19, 84, 37, 233, 9,
	    237, 92, 5, 202, 76, 36, 135, 191, 24, 62, 34, 240, 81, 236, 97, 23, 22, 94, 175, 211,
	    73, 166, 54, 67, 244, 71, 145, 223, 51, 147, 33, 59, 121, 183, 151, 133, 16, 181, 186, 60,
	    182, 112, 208, 6, 161, 250, 129, 130, 131, 126, 127, 128, 150, 115, 190, 86, 155, 158, 149, 217,
	    247, 2, 185, 164, 222, 106, 50, 109, 216, 138, 132, 114, 42, 20, 159, 136, 249, 220, 137, 154,
	    251, 124, 46, 195, 143, 184, 101, 72, 38, 200, 18, 74, 206, 231, 210, 98, 12, 224, 31, 239,
	    17, 117, 120, 113, 165, 142, 118, 61, 189, 188, 134, 87, 11, 40, 47, 163, 218, 212, 228, 15,
	    169, 39, 83, 4, 27, 252, 172, 230, 122, 7, 174, 99, 197, 219, 226, 234, 148, 139, 196, 213,
	    157, 248, 144, 107, 177, 13, 214, 235, 198, 14, 207, 173, 8, 78, 215, 227, 93, 80, 30, 179,
	    91, 35, 56, 52, 104, 70, 3, 140, 221, 156, 125, 160, 205, 26, 65, 28 };

// The column index of the inverse shifted rows - See Note 2
const unsigned char Rijndael::invShiftRows[9][8][3] = {
	{{ 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }},
	{{ 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }},
	{{ 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }},
	{{ 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }},
	{{ 1, 2, 3 }, { 2, 3, 0 }, { 3, 0, 1 }, { 0, 1, 2 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }},
	{{ 2, 3, 4 }, { 3, 4, 0 }, { 4, 0, 1 }, { 0, 1, 2 }, { 1, 2, 3 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }},
	{{ 3, 4, 5 }, { 4, 5, 0 }, { 5, 0, 1 }, { 0, 1, 2 }, { 1, 2, 3 }, { 2, 3, 4 }, { 0, 0, 0 }, { 0, 0, 0 }},
	{{ 3, 5, 6 }, { 4, 6, 0 }, { 5, 0, 1 }, { 6, 1, 2 }, { 0, 2, 3 }, { 1, 3, 4 }, { 2, 4, 5 }, { 0, 0, 0 }},
	{{ 4, 5, 7 }, { 5, 6, 0 }, { 6, 7, 1 }, { 7, 0, 2 }, { 0, 1, 3 }, { 1, 2, 4 }, { 2, 3, 5 }, { 3, 4, 6 }}};

// The column index of the shifted rows - See Note 2
const unsigned char Rijndael::shiftRows[9][8][3] = {
	{{ 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }},
	{{ 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }},
	{{ 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }},
	{{ 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }},
	{{ 3, 2, 1 }, { 0, 3, 2 }, { 1, 0, 3 }, { 2, 1, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }},
	{{ 3, 2, 1 }, { 4, 3, 2 }, { 0, 4, 3 }, { 1, 0, 4 }, { 2, 1, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }},
	{{ 3, 2, 1 }, { 4, 3, 2 }, { 5, 4, 3 }, { 0, 5, 4 }, { 1, 0, 5 }, { 2, 1, 0 }, { 0, 0, 0 }, { 0, 0, 0 }},
	{{ 4, 2, 1 }, { 5, 3, 2 }, { 6, 4, 3 }, { 0, 5, 4 }, { 1, 6, 5 }, { 2, 0, 6 }, { 3, 1, 0 }, { 0, 0, 0 }},
	{{ 4, 3, 1 }, { 5, 4, 2 }, { 6, 5, 3 }, { 7, 6, 4 }, { 0, 7, 5 }, { 1, 0, 6 }, { 2, 1, 7 }, { 3, 2, 0 }}};

/* Lookup table for inverse transforms. U was calculated as follows:

   for (int i = 0; i < 256; i++) {
    unsigned long x;

    // Inverse affine Transform
    x = ((i << 1 & 0xFE) | (i >> 7 & 0x01))
        ^ ((i << 3 & 0xF8) | (i >> 5 & 0x07))
        ^ ((i << 6 & 0xC0) | (i >> 2 & 0x3F)) ^ 0x05;

    // S-Box lookup
    x = gf254[x];

    // Inverse Mix Columns
    x  = (x << 8 & 0xFF00) + (x & 0xFF);
    x  = (x << 16 & 0xFFFF0000UL) + (x & 0xFFFF);
    x  = (x & 0xFF) ^ (x << 1 & 0xFEFEFE00UL)
       ^ ((x >> 7 & 0x01010100UL) * 27);
    x  = (x & 0xFFFF) ^ (x << 1 & 0xFEFE0000UL)
       ^ ((x >> 7 & 0x01010000UL) * 27);
    x  = (x & 0xFFFFFF) ^ (x << 1 & 0xFE000000UL)
       ^ ((x >> 7 & 0x01000000UL) * 27);
    x ^= (x << 16 & 0xFF000000UL) ^ (x << 24 & 0xFF000000UL);
    x ^= (x >> 16 & 0xFF00);

    x ^= (x << 8 & 0xFF0000);
    x ^= (x >> 24 & 0xFF) ^ (x >> 16 & 0xFF) ^ (x >> 8 & 0xFF);
    U[i] = x; // represents column [13x 11x  9x 14x]
   }
 */
const unsigned long Rijndael::U[256] = { 0x50a7f451UL, 0x5365417eUL, 0xc3a4171aUL, 0x965e273aUL, 0xcb6bab3bUL, 0xf1459d1fUL, 0xab58faacUL, 0x9303e34bUL,
	                                     0x55fa3020UL, 0xf66d76adUL, 0x9176cc88UL, 0x254c02f5UL, 0xfcd7e54fUL, 0xd7cb2ac5UL, 0x80443526UL, 0x8fa362b5UL,
	                                     0x495ab1deUL, 0x671bba25UL, 0x980eea45UL, 0xe1c0fe5dUL, 0x02752fc3UL, 0x12f04c81UL, 0xa397468dUL, 0xc6f9d36bUL,
	                                     0xe75f8f03UL, 0x959c9215UL, 0xeb7a6dbfUL, 0xda595295UL, 0x2d83bed4UL, 0xd3217458UL, 0x2969e049UL, 0x44c8c98eUL,
	                                     0x6a89c275UL, 0x78798ef4UL, 0x6b3e5899UL, 0xdd71b927UL, 0xb64fe1beUL, 0x17ad88f0UL, 0x66ac20c9UL, 0xb43ace7dUL,
	                                     0x184adf63UL, 0x82311ae5UL, 0x60335197UL, 0x457f5362UL, 0xe07764b1UL, 0x84ae6bbbUL, 0x1ca081feUL, 0x942b08f9UL,
	                                     0x58684870UL, 0x19fd458fUL, 0x876cde94UL, 0xb7f87b52UL, 0x23d373abUL, 0xe2024b72UL, 0x578f1fe3UL, 0x2aab5566UL,
	                                     0x0728ebb2UL, 0x03c2b52fUL, 0x9a7bc586UL, 0xa50837d3UL, 0xf2872830UL, 0xb2a5bf23UL, 0xba6a0302UL, 0x5c8216edUL,
	                                     0x2b1ccf8aUL, 0x92b479a7UL, 0xf0f207f3UL, 0xa1e2694eUL, 0xcdf4da65UL, 0xd5be0506UL, 0x1f6234d1UL, 0x8afea6c4UL,
	                                     0x9d532e34UL, 0xa055f3a2UL, 0x32e18a05UL, 0x75ebf6a4UL, 0x39ec830bUL, 0xaaef6040UL, 0x069f715eUL, 0x51106ebdUL,
	                                     0xf98a213eUL, 0x3d06dd96UL, 0xae053eddUL, 0x46bde64dUL, 0xb58d5491UL, 0x055dc471UL, 0x6fd40604UL, 0xff155060UL,
	                                     0x24fb9819UL, 0x97e9bdd6UL, 0xcc434089UL, 0x779ed967UL, 0xbd42e8b0UL, 0x888b8907UL, 0x385b19e7UL, 0xdbeec879UL,
	                                     0x470a7ca1UL, 0xe90f427cUL, 0xc91e84f8UL, 0x00000000UL, 0x83868009UL, 0x48ed2b32UL, 0xac70111eUL, 0x4e725a6cUL,
	                                     0xfbff0efdUL, 0x5638850fUL, 0x1ed5ae3dUL, 0x27392d36UL, 0x64d90f0aUL, 0x21a65c68UL, 0xd1545b9bUL, 0x3a2e3624UL,
	                                     0xb1670a0cUL, 0x0fe75793UL, 0xd296eeb4UL, 0x9e919b1bUL, 0x4fc5c080UL, 0xa220dc61UL, 0x694b775aUL, 0x161a121cUL,
	                                     0x0aba93e2UL, 0xe52aa0c0UL, 0x43e0223cUL, 0x1d171b12UL, 0x0b0d090eUL, 0xadc78bf2UL, 0xb9a8b62dUL, 0xc8a91e14UL,
	                                     0x8519f157UL, 0x4c0775afUL, 0xbbdd99eeUL, 0xfd607fa3UL, 0x9f2601f7UL, 0xbcf5725cUL, 0xc53b6644UL, 0x347efb5bUL,
	                                     0x7629438bUL, 0xdcc623cbUL, 0x68fcedb6UL, 0x63f1e4b8UL, 0xcadc31d7UL, 0x10856342UL, 0x40229713UL, 0x2011c684UL,
	                                     0x7d244a85UL, 0xf83dbbd2UL, 0x1132f9aeUL, 0x6da129c7UL, 0x4b2f9e1dUL, 0xf330b2dcUL, 0xec52860dUL, 0xd0e3c177UL,
	                                     0x6c16b32bUL, 0x99b970a9UL, 0xfa489411UL, 0x2264e947UL, 0xc48cfca8UL, 0x1a3ff0a0UL, 0xd82c7d56UL, 0xef903322UL,
	                                     0xc74e4987UL, 0xc1d138d9UL, 0xfea2ca8cUL, 0x360bd498UL, 0xcf81f5a6UL, 0x28de7aa5UL, 0x268eb7daUL, 0xa4bfad3fUL,
	                                     0xe49d3a2cUL, 0x0d927850UL, 0x9bcc5f6aUL, 0x62467e54UL, 0xc2138df6UL, 0xe8b8d890UL, 0x5ef7392eUL, 0xf5afc382UL,
	                                     0xbe805d9fUL, 0x7c93d069UL, 0xa92dd56fUL, 0xb31225cfUL, 0x3b99acc8UL, 0xa77d1810UL, 0x6e639ce8UL, 0x7bbb3bdbUL,
	                                     0x097826cdUL, 0xf418596eUL, 0x01b79aecUL, 0xa89a4f83UL, 0x656e95e6UL, 0x7ee6ffaaUL, 0x08cfbc21UL, 0xe6e815efUL,
	                                     0xd99be7baUL, 0xce366f4aUL, 0xd4099feaUL, 0xd67cb029UL, 0xafb2a431UL, 0x31233f2aUL, 0x3094a5c6UL, 0xc066a235UL,
	                                     0x37bc4e74UL, 0xa6ca82fcUL, 0xb0d090e0UL, 0x15d8a733UL, 0x4a9804f1UL, 0xf7daec41UL, 0x0e50cd7fUL, 0x2ff69117UL,
	                                     0x8dd64d76UL, 0x4db0ef43UL, 0x544daaccUL, 0xdf0496e4UL, 0xe3b5d19eUL, 0x1b886a4cUL, 0xb81f2cc1UL, 0x7f516546UL,
	                                     0x04ea5e9dUL, 0x5d358c01UL, 0x737487faUL, 0x2e410bfbUL, 0x5a1d67b3UL, 0x52d2db92UL, 0x335610e9UL, 0x1347d66dUL,
	                                     0x8c61d79aUL, 0x7a0ca137UL, 0x8e14f859UL, 0x893c13ebUL, 0xee27a9ceUL, 0x35c961b7UL, 0xede51ce1UL, 0x3cb1477aUL,
	                                     0x59dfd29cUL, 0x3f73f255UL, 0x79ce1418UL, 0xbf37c773UL, 0xeacdf753UL, 0x5baafd5fUL, 0x146f3ddfUL, 0x86db4478UL,
	                                     0x81f3afcaUL, 0x3ec468b9UL, 0x2c342438UL, 0x5f40a3c2UL, 0x72c31d16UL, 0x0c25e2bcUL, 0x8b493c28UL, 0x41950dffUL,
	                                     0x7101a839UL, 0xdeb30c08UL, 0x9ce4b4d8UL, 0x90c15664UL, 0x6184cb7bUL, 0x70b632d5UL, 0x745c6c48UL, 0x4257b8d0UL };

/* Lookup table for forward transforms
   T was calculated as follows:

    for (int i = 0; i < 256; i++) {
        unsigned long x = gf254[i];
        x = (x << 24) ^ (x << 16) ^ (x << 8) ^ x;

        // This actually applies the affine transform and MixColumn:
        x = (x << 5 & 0xE0000000UL) ^ (x << 1 & 0x10000000UL)
          ^ (x      & 0xE7FFFF18UL) ^ (x >> 1 & 0x18000018UL)
          ^ (x >> 2 & 0x1A00001AUL) ^ (x >> 3 & 0x050000E5UL)
          ^ (x >> 4 & 0x0AFFFFF5UL) ^ (x >> 5 & 0x02FFFFFDUL)
          ^ (x >> 6 & 0x02FFFFFDUL) ^ (x >> 7 & 0x00FFFFEFUL);
        T[i] = x;
    }
 */
const unsigned long Rijndael::T[256] = { 0x00000000UL, 0x211f1f3eUL, 0x3c141428UL, 0x28181830UL, 0xa8919139UL, 0x18080810UL, 0x140c0c18UL, 0xf1a6a657UL,
	                                     0xf55353a6UL, 0xa66262c4UL, 0x0c040408UL, 0xd8484890UL, 0xbc9d9d21UL, 0xc7b4b473UL, 0x43c8c88bUL, 0x3f15152aUL,
	                                     0xe0a9a949UL, 0x38e1e1d9UL, 0xe5aaaa4fUL, 0x221e1e3cUL, 0xb0999929UL, 0x4e3a3a74UL, 0x6c242448UL, 0xae93933dUL,
	                                     0x49cece87UL, 0xc2b7b775UL, 0x58c1c199UL, 0x4fcccc83UL, 0x1affffe5UL, 0x52c7c795UL, 0x33111122UL, 0xfea3a35dUL,
	                                     0x67d4d4b3UL, 0xb99e9e27UL, 0x0bf0f0fbUL, 0xcf45458aUL, 0xff5555aaUL, 0xe45c5cb8UL, 0xa7949433UL, 0xeaafaf45UL,
	                                     0xf95757aeUL, 0x51c6c697UL, 0x91868617UL, 0xad92923fUL, 0x36121224UL, 0xd6bbbb6dUL, 0xf65252a4UL, 0x9a7676ecUL,
	                                     0xa96767ceUL, 0xf7a4a453UL, 0xc0404080UL, 0xfba0a05bUL, 0x8d7b7bf6UL, 0x04f5f5f1UL, 0xaa6666ccUL, 0x10f9f9e9UL,
	                                     0xac6464c8UL, 0x937171e2UL, 0x3ee3e3ddUL, 0x98818119UL, 0x8388880bUL, 0xcc444488UL, 0x68d1d1b9UL, 0x3a16162cUL,
	                                     0xbe6a6ad4UL, 0x3be0e0dbUL, 0xd14f4f9eUL, 0x8b7979f2UL, 0x887878f0UL, 0x170d0d1aUL, 0x4b393972UL, 0x5ec3c39dUL,
	                                     0x53313162UL, 0xe85858b0UL, 0xc4b5b571UL, 0x6bd0d0bbUL, 0xde4a4a94UL, 0x9b80801bUL, 0xd44c4c98UL, 0x32e7e7d5UL,
	                                     0x50303060UL, 0xcdb2b27fUL, 0xa56363c6UL, 0x898e8e07UL, 0xc5434386UL, 0xba9f9f25UL, 0x6dd2d2bfUL, 0x48383870UL,
	                                     0x1b090912UL, 0xe3a8a84bUL, 0x7cdddda1UL, 0xee5a5ab4UL, 0x7b292952UL, 0x712f2f5eUL, 0x4d3b3b76UL, 0xefacac43UL,
	                                     0xceb3b37dUL, 0x8f8c8c03UL, 0x40c9c989UL, 0xb398982bUL, 0x60202040UL, 0x722e2e5cUL, 0xf05050a0UL, 0x31e6e6d7UL,
	                                     0x6a26264cUL, 0xb59a9a2fUL, 0xa36161c2UL, 0x241c1c38UL, 0x55333366UL, 0xe15f5fbeUL, 0x1ffcfce3UL, 0x46cbcb8dUL,
	                                     0x56323264UL, 0x5bc0c09bUL, 0x65232346UL, 0x2fececc3UL, 0x08f1f1f9UL, 0x19fefee7UL, 0xed5b5bb6UL, 0xa1969637UL,
	                                     0x7adfdfa5UL, 0x64d5d5b1UL, 0xd0b9b969UL, 0xc6424284UL, 0x957373e6UL, 0xbf9c9c23UL, 0xab90903bUL, 0xc8b1b179UL,
	                                     0xe9aeae47UL, 0xb16f6fdeUL, 0x907070e0UL, 0x8a8f8f05UL, 0x443c3c78UL, 0x07f4f4f3UL, 0x6927274eUL, 0x9c7474e8UL,
	                                     0xf2a7a755UL, 0x57c4c493UL, 0x271d1d3aUL, 0xe25e5ebcUL, 0x0907070eUL, 0x423e3e7cUL, 0x8e7a7af4UL, 0x30101020UL,
	                                     0x05030306UL, 0x3de2e2dfUL, 0x742c2c58UL, 0xdabfbf65UL, 0xc3414182UL, 0xdb494992UL, 0x0ef3f3fdUL, 0x26ebebcdUL,
	                                     0x6f25254aUL, 0x8c8d8d01UL, 0x76dbdbadUL, 0x997777eeUL, 0xdcbdbd61UL, 0x473d3d7aUL, 0xb86868d0UL, 0xd3b8b86bUL,
	                                     0x9e83831dUL, 0xf35151a2UL, 0xeb5959b2UL, 0xbb6969d2UL, 0x7e2a2a54UL, 0xaf6565caUL, 0xc947478eUL, 0x413f3f7eUL,
	                                     0xf8a1a159UL, 0xcbb0b07bUL, 0x4acfcf85UL, 0x03010102UL, 0x0df2f2ffUL, 0x01f6f6f7UL, 0x92878715UL, 0x2e1a1a34UL,
	                                     0x97848413UL, 0xe6abab4dUL, 0xfc5454a8UL, 0x120e0e1cUL, 0x29eeeec7UL, 0xc1b6b677UL, 0x772d2d5aUL, 0x45caca8fUL,
	                                     0x110f0f1eUL, 0x5f35356aUL, 0xa2979735UL, 0x80898909UL, 0x0a06060cUL, 0x2b191932UL, 0x4ccdcd81UL, 0xbd6b6bd6UL,
	                                     0x70d9d9a9UL, 0x2d1b1b36UL, 0xca46468cUL, 0xd74d4d9aUL, 0x817f7ffeUL, 0x54c5c591UL, 0x62d7d7b5UL, 0xf4a5a551UL,
	                                     0x868b8b0dUL, 0xd9bebe67UL, 0x3917172eUL, 0x847c7cf8UL, 0x78282850UL, 0x79dedea7UL, 0x23e8e8cbUL, 0x20e9e9c9UL,
	                                     0x35131326UL, 0xe75d5dbaUL, 0x61d6d6b7UL, 0x0f05050aUL, 0x7d2b2b56UL, 0xa06060c0UL, 0xa4959531UL, 0xb76d6ddaUL,
	                                     0x06020204UL, 0xfa5656acUL, 0x5c343468UL, 0x75dadaafUL, 0x34e5e5d1UL, 0xfda2a25fUL, 0x827e7efcUL, 0x1cfdfde1UL,
	                                     0x9d82821fUL, 0xb69b9b2dUL, 0x16fbfbedUL, 0x967272e4UL, 0x1e0a0a14UL, 0xd5baba6fUL, 0x2cededc1UL, 0x02f7f7f5UL,
	                                     0x13f8f8ebUL, 0x877d7dfaUL, 0x37e4e4d3UL, 0x858a8a0fUL, 0xecadad41UL, 0x5a36366cUL, 0xdd4b4b96UL, 0xdfbcbc63UL,
	                                     0x2aefefc5UL, 0x5dc2c29fUL, 0x25eaeacfUL, 0xb26e6edcUL, 0x7fdcdca3UL, 0x94858511UL, 0x63212142UL, 0x1d0b0b16UL,
	                                     0x66222244UL, 0x15fafaefUL, 0xd24e4e9cUL, 0xb46c6cd8UL, 0x6ed3d3bdUL, 0x5937376eUL, 0x73d8d8abUL, 0x9f7575eaUL };
