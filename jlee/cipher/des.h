#ifndef DES_HPP
#define DES_HPP

#include "cipher.h"

class DES
	: public BlockCipher
{
public:
	DES(const unsigned char*, std::size_t, std::size_t);
	~DES() override;

	static constexpr unsigned supported_key_sizes[1]   = { 64 };
	static constexpr unsigned supported_block_sizes[1] = { 64 };
private:
	// implemented virtual functions
	void transform(std::uint_least32_t*) override;
	void inverseTransform(std::uint_least32_t*) override;

	// DES specific functions
	void IP(unsigned long&, unsigned long&);
	void IIP(unsigned long&, unsigned long&);
	void PC1(unsigned long&, unsigned long&);

	// DES specific data
	static const unsigned PC2[64];

	static const unsigned long SBOX[64];
	unsigned long              keySchedule[32];
};

#endif // #ifndef DES_HPP
