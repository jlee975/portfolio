#ifndef CIPHER_HPP
#define CIPHER_HPP

#include <cstddef>
#include <cstdint>

#include "moo.h"

namespace cipher
{

enum Padding
{
	NoPadding,
	NullBytes,
	PKCS7,
	CTS
};
}

class BlockCipher
{
public:
	BlockCipher(unsigned, bool);
	virtual ~BlockCipher() = default;
	void encrypt(const unsigned char*, std::size_t, unsigned char*, std::size_t, const unsigned char* = nullptr);
	void decrypt(const unsigned char*, std::size_t, unsigned char*, std::size_t, const unsigned char* = nullptr);
	bool setMode(cipher::Mode);
	bool setPadding(cipher::Padding);
private:
	virtual void transform(std::uint_least32_t*)        = 0;
	virtual void inverseTransform(std::uint_least32_t*) = 0;

	cipher::Mode    mode;
	cipher::Padding padding;
	bool            l_endian;
	unsigned        blocksize;
};

class StreamCipher
{
public:
	virtual ~StreamCipher() = default;
	void transform(const std::byte*, std::size_t, std::byte*, std::size_t);
private:
	virtual std::byte next() = 0;
};

#endif // ifndef CIPHER_HPP
