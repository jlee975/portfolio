/*=========================================================================*//**
   \file        cipher.cpp
   \brief       Contains the BlockCipher and StreamCipher base classes

   This source code defines the BlockCipher base class from which specific ciphers
   may be derived. It provides mode of operation, and padding functionality that
   derived classes may take advantage of, and a common interface for encryption
   and decryption.

   SetMode()    - changes the mode of operation. Check the CHIPHER_MODE enum
                 for supported modes. Use of ECB is discouraged.
                 The default is CBC.
   SetPadding() - changes the method used to pad input data which is not a whole
                 multiple of cipher blocks. This is ignored for the modes of
                 operation that do not require padding (ex., CFB, OFB, CTR).
   Encrypt()    - Takes input and output buffers, and an optional initialization
                 vector, then encrypts the data.
   Decrypt()    - Recovers data encrypted by encrypt.

   Derived classes ----------------------------------------------------------------

   MUST   accept the blocksize in their constructor, and pass this to the
         BlockCipher constructor.
   MUST   implement the Transform() and InverseTransform() functions which are
         pure virtual in BlockCipher. These should perform encryption and
         decryption, respectively, of a single block of data.
   MUST   manage their own key object.
   MUST   throw an exception in the constructor if the key object cannot be created
         successfully (ex., invalid key size, parity bits fail for DES)
   SHOULD inherit _publicly_ from BlockCipher so that SetMode(), SetPadding(),
         Encrypt(), and Decrypt() will be available to the end user.
   SHOULD make the key an argument of the constructor.

   \todo Encrypt chunks at a time
   \todo Derived classes should provide static member functions indicating supported
       blocksizes, modes, etc.
   \todo Key generation
   \todo IV generation
   \todo Provide both the SetMode(), SetKey(), SetIV() functions _and_ a
       all purpose encrypt/decrypt with all parameters
   \todo Pass initvec length
   \todo Strong choice of default MoO - CTR or CBC
   \todo Additional supported modes: http://csrc.nist.gov/groups/ST/toolkit/BCM/modes_development.html
   \bug: Derived classes should agree on how to load a key

*//*==========================================================================*/
#include <climits>
#include <iostream>
#include <stdexcept>
#if ( CHAR_BIT != 8 )
#error Only 8 bit chars are supported.
#endif

#include "cipher.h" // Class declaration
#include "endian/endian.h"

/***************************************************************************/ /**
 *******************************************************************************/
BlockCipher::BlockCipher(
	unsigned bs,
	bool     le
)
	: mode(cipher::CBC), padding(cipher::NoPadding), l_endian(le), blocksize(bs)
{
}

bool BlockCipher::setMode(cipher::Mode m)
{
	mode = m;
	return true;
}

bool BlockCipher::setPadding(cipher::Padding p)
{
	padding = p;
	return true;
}

/// @todo Return size of output
/// @todo CTS needs at least one block
void BlockCipher::encrypt(
	const unsigned char* input,  // The input
	std::size_t          cbIn,   // How many bytes of input there are
	unsigned char*       output, // The output array
	std::size_t          cbOut,  // # bytes are available in the output
	const unsigned char* IV      // An initialization vector for CBC, etc
)
{
	const std::size_t bs = blocksize / 8;

	// check that destination buffer is large enough
	if ( ( cbOut < cbIn )
	     || ( ( padding != cipher::CTS ) && ( mode == cipher::ECB || mode == cipher::CBC )
	          && ( cbOut < ( ( ( cbIn + bs - 1 ) / bs ) * bs ) ) )
                                                                   )
	{
		throw std::invalid_argument("[BlockCipher::encrypt(...)] "
			"Destination buffer is not large enough for output.");
	}

	// check that initialization vector exists for modes that need it
	if ( ( mode == cipher::CBC || mode == cipher::OFB || mode == cipher::CFB ) && ( IV == nullptr ) )
	{
		throw std::invalid_argument("[BlockCipher::encrypt(...)] "
			"Initialization Vector cannot be null for the specified"
			" mode of operation.");
	}

	// Process Blocks ------------------------------------------------------
	const std::size_t    lbs  = blocksize / 32; // \bug ensure it is at least one
	auto*                pbuf = new std::uint_least32_t[3 * lbs];
	std::uint_least32_t* A    = pbuf;
	std::uint_least32_t* M    = pbuf + lbs;
	std::uint_least32_t* C    = pbuf + 2 * lbs;

	if ( IV != nullptr )
	{
		for (std::size_t j = 0; j < lbs; j++)
		{
			A[j] = static_cast< std::uint_least32_t >( l_endian ? leread< 32 >(IV + 4 * j)
			                           : beread< 32 >(IV + 4 * j) );
		}
	}

	if ( cbIn % bs != 0 )
	{
		throw std::logic_error("Not implemented");
	}

	for (std::size_t i = 0; i < cbIn; i += bs)
	{
		if ( l_endian ) // Read the input block
		{
			for (std::size_t j = 0; j < lbs; j++)
			{
				M[j] = static_cast< std::uint_least32_t >( leread< 32 >(input + i + 4 * j) );
			}
		}
		else
		{
			for (std::size_t j = 0; j < lbs; j++)
			{
				M[j] = static_cast< std::uint_least32_t >( beread< 32 >(input + i + 4 * j) );
			}
		}

		switch ( mode ) // Process block depending on block mode
		{
		case cipher::CFB:
			std::swap(A, C);
			transform(C);

			for (std::size_t j = 0; j < lbs; j++)
			{
				A[j] = ( C[j] ^= M[j] );
			}

			break;
		case cipher::OFB:
			transform(A);

			for (std::size_t j = 0; j < lbs; j++)
			{
				C[j] = A[j] ^ M[j];
			}

			break;
		case cipher::CBC:

			for (std::size_t j = 0; j < lbs; j++)
			{
				C[j] = M[j] ^ A[j];
			}

			transform(C);

			for (std::size_t j = 0; j < lbs; j++)
			{
				A[j] = C[j];
			}

			break;
		case cipher::ECB:
			std::swap(M, C);
			transform(C);
			break;
		} // switch

		// Store the output to the buffer
		// \todo: variable long size
		if ( l_endian )
		{
			for (unsigned j = 0; j < bs; j++)
			{
				output[i + j] = static_cast< unsigned char >( ( C[j >> 2] >> ( 8 * ( j & 3 ) ) ) & 0xFF );
			}
		}
		else
		{
			for (unsigned j = 0; j < bs; ++j)
			{
				output[i + j] = static_cast< unsigned char >( ( C[j / 4] >> ( ( 3 - ( j & 3 ) ) * 8 ) ) & 0xFF );
			}
		}
	}

	// security clear
	for (unsigned j = 0; j < 3 * lbs; j++)
	{
		pbuf[j] = 0;
	}

	delete[] pbuf;
	pbuf = nullptr;
	A    = nullptr;
	C    = nullptr;
	M    = nullptr;
}

void BlockCipher::decrypt(
	const unsigned char* input,  // The input
	std::size_t          cbIn,   // How many bytes of input there are
	unsigned char*       output, // The output array
	std::size_t          cbOut,  // # bytes are available in the output
	const unsigned char* IV      // An initialization vector for CBC, etc
)
{
	unsigned bs = blocksize / 8;

	// check that destination buffer is large enough
	if ( ( ( cbOut < ( ( ( cbIn + bs - 1 ) / bs ) * bs ) ) && ( mode == cipher::ECB || mode == cipher::CBC )
	       && ( padding != cipher::CTS ) )
	     || ( cbOut < cbIn )
                             )
	{
		throw std::invalid_argument("[BlockCipher::decrypt(...)] "
			"Destination buffer is not large enough for output.");
	}

	// check that initialization vector exists for modes that need it
	if ( ( mode == cipher::CBC || mode == cipher::OFB || mode == cipher::CFB ) && ( IV == nullptr ) )
	{
		throw std::invalid_argument("[BlockCipher::decrypt(...)] "
			"Initialization vector cannot be NULL for the specified"
			" mode of operation.");
	}

	// Process Blocks ------------------------------------------------------
	unsigned             lbs  = blocksize / 32;
	auto*                pbuf = new std::uint_least32_t[3 * lbs];
	std::uint_least32_t* A    = pbuf;
	std::uint_least32_t* M    = pbuf + lbs;
	std::uint_least32_t* C    = pbuf + 2 * lbs;

	if ( IV != nullptr )
	{
		if ( l_endian )
		{
			for (unsigned j = 0; j < lbs; j++)
			{
				A[j] = static_cast< std::uint_least32_t >( leread< 32 >(IV + 4 * j) );
			}
		}
		else
		{
			for (unsigned j = 0; j < lbs; j++)
			{
				A[j] = static_cast< std::uint_least32_t >( beread< 32 >(IV + 4 * j) );
			}
		}
	}

	for (unsigned long i = 0; i < cbIn; i += bs)
	{
		// Read the input block
		// if (full block) {
		if ( l_endian )
		{
			for (unsigned j = 0; j < lbs; j++)
			{
				M[j] = static_cast< std::uint_least32_t >( leread< 32 >(input + i + 4 * j) );
			}
		}
		else
		{
			for (unsigned j = 0; j < lbs; j++)
			{
				M[j] = static_cast< std::uint_least32_t >( beread< 32 >(input + i + 4 * j) );
			}
		}

		// } else {
		// for (int j = 0; j < lbs; j++) M[j] = 0;
		// for (int j = 0; j < rem bytes; j++) {
		// M[j / 4] += ((unsigned long) input[i * bs+ j]) << (24 - 8 * (j % 4));
		// pad
		// }

		switch ( mode ) // Pre-process block depending on block mode
		{
		case cipher::CFB:
			std::swap(A, C);
			transform(C);

			for (unsigned j = 0; j < lbs; j++)
			{
				C[j] ^= ( A[j] = M[j] );
			}

			break;
		case cipher::OFB:
			transform(A);

			for (unsigned j = 0; j < lbs; j++)
			{
				C[j] = A[j] ^ M[j];
			}

			break;
		case cipher::CBC:

			for (unsigned j = 0; j < lbs; j++)
			{
				C[j] = M[j];
			}

			inverseTransform(C);

			for (unsigned j = 0; j < lbs; j++)
			{
				C[j] ^= A[j];
			}

			std::swap(A, M);
			break;
		case cipher::ECB:
			std::swap(M, C);
			inverseTransform(C);
			break;
		} // switch

		// * Save the output
		if ( l_endian )
		{
			for (unsigned j = 0; j < bs; j++)
			{
				output[i + j] = static_cast< unsigned char >( ( C[j >> 2] >> ( 8 * ( j & 3 ) ) ) & 0xFF );
			}
		}
		else
		{
			for (unsigned j = 0; j < bs; ++j)
			{
				output[i + j] = static_cast< unsigned char >( ( C[j / 4] >> ( 8 * ( 3 - ( j & 3 ) ) ) ) & 0xFF );
			}
		}

		// */
	}

	for (unsigned j = 0; j < 3 * lbs; j++)
	{
		pbuf[j] = 0;
	}

	delete[] pbuf;
	pbuf = nullptr;
	A    = nullptr;
	M    = nullptr;
	C    = nullptr;
}

/***************************************************************************/ /**
 *******************************************************************************/
void StreamCipher::transform(
	const std::byte* lpIn,
	std::size_t      cbIn,
	std::byte*       lpOut,
	std::size_t      cbOut
)
{
	if ( cbOut >= cbIn && lpIn != nullptr && lpOut != nullptr && cbIn != 0 )
	{
		for (unsigned long i = 0; i < cbIn; i++)
		{
			// Encrypt
			lpOut[i] = ( lpIn[i] ^ next() )& std::byte{ 0xFF };
		}
	}
	else // Error handling -- report specific errors
	{
		if ( lpIn == nullptr || cbIn == 0 )
		{
			throw std::invalid_argument("[StreamCipher::transform(...)]"
				" No source to transform.");
		}

		if ( lpOut == nullptr )
		{
			throw std::invalid_argument("[StreamCipher::transform(...)]"
				" No destination buffer given.");
		}

		if ( cbOut < cbIn )
		{
			throw std::invalid_argument("[StreamCipher::transform(...)]"
				" Destination buffer is too small.");
		}
	}
}
