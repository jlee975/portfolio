/*=========================================================================*//**
   \file        arcfour.cpp
   \author      Jonathan Lee

   This library provides a class, ARC4, which can be used to encrypt and
   decrypt message data.

*//*==========================================================================*/

#include <algorithm>
#include <cstring>
#include <stdexcept>

#include "arcfour.h"

ARC4::ARC4(const char* s)
	: ARC4(reinterpret_cast< const std::byte* >( s ), std::strlen(s), 0)
{
}

/*=========================================================================*//**
   \class       ARC4

   Encrypts source data using the ARCFOUR stream cipher, under the given key. Note
   that because ARCFOUR is a stream cipher, encryption and decryption work exactly
   the same.

   Due to a weakness in ARCFOUR, it is advisable to set the ARC4_DROP_XXX flag.
   The first bytes output by ARCFOUR can be used to discover the key, and thus
   decrypt a secret message. The ARC4_DROP_XXX flag will drop the first XXX bytes
   from the cipher output before mixing it with the plaintext. Values currently
   defined are ARC4_DROP_256, ARC4_DROP_768, ARC4_DROP_3072.

   If you should need a custom amount dropped, call the function without the flag
   set, and pre-pad your message with N bytes (N being the amount that should
   be dropped). Then after calling the function, discard the pad.

   \todo        Should save key and drop count so that the state can be reset.
*//*==========================================================================*/
ARC4::ARC4(
	const std::byte* key,
	std::size_t      nkey,
	unsigned         drop
)
	: ii(0), jj(0)
{
	if ( ( key == nullptr ) || nkey == 0 )
	{
		throw std::invalid_argument("ARC4 key cannot be empty.");
	}

	for (unsigned i = 0; i < 256; i++)
	{
		S[i] = static_cast< std::byte >( i );
	}

	for (std::size_t i = 0, j = 0; i < 256; i++)
	{
		j = ( j + std::to_integer< unsigned char >(S[i]) + std::to_integer< unsigned char >(key[i % nkey]) )
		    % 256;
		std::swap(S[j], S[i]);
	}

	while ( drop-- != 0 ) // Discard leading bytes
	{
		next();
	}
}

ARC4::~ARC4()
{
	for (unsigned i = 0; i < 256; i++)
	{
		S[i] = std::byte{ 0 };
	}

	ii = 0;
	jj = 0;
}

/***************************************************************************/ /**
   \fn          unsigned char ARC4::next()

   Returns the next byte from the ARCFOUR stream, using the state array S.

   \todo: Does this have to be done a byte at a time? Can we do a long at a time?
 *******************************************************************************/
std::byte ARC4::next()
{
	ii = ( ii + 1 ) % 256;
	jj = ( jj + std::to_integer< unsigned char >(S[ii]) ) % 256;
	std::swap(S[ii], S[jj]);

	return S[( std::to_integer< unsigned char >(S[ii]) + std::to_integer< unsigned char >(S[jj]) ) % 256];
}
