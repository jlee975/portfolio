/*=========================================================================*//**
   \file        des.cpp

   The Data Encryption Standard (DES) is an older block cipher that was once very
   widespread because of its adoption by the US Government. No longer secure for
   modern purposes, it is still a good place to begin examining block cipher
   performance. The algorithm has a number of opportunities for optimization.

   I just want to give a brief overview of the cipher here, and refer the reader to
   the specification for details. First off, the DES is a block cipher in that
   it handles its input in blocks. That is, when encrypting (or decrypting) a
   stream of information, it handles small fixed sized values. This particular
   algorithm breaks the input into 64-bit blocks which are transformed
   independently. This is as opposed to a stream cipher, which does not group
   together input bits to form larger values.

   Secondly, the basic structure of the DES is a Feistel network. A very cursory
   description of which would be:

   1. break up the input block into left and right halves
   2. apply a function to the right half
   3. XOR the result of this function to the left half
   4. exchange the two halves
   5. Do steps 2-4 a bunch of times

   Steps 2-4 are known as the round, and step 2 specifically is the round function.
   DES has 16 rounds. A nice advantage of this structure is that it is easily
   reversed. In fact, encrypting with DES is virtually identical to decrypting.

   DES adds a twist to the basic Fiestel scheme by slipping in one extra step
   before and after the rounds. Think of them as step 1a and step 6. These steps
   are called the initial permutation and the inverse initial permutation.
   The functions simply scramble the order of the 64 bits in our block.

   Finally, DES is keyed. In addition to the input, the user must supply
   a 56-bit key which mixes in with the round function, obfuscating it. If you
   don't have the key, the message cannot be deciphered. This is what makes the
   cipher secure. There are 72,057,594,037,927,936 keys, so guessing it is out of
   the question. If you guessed one per second and tried it, it would take 2.2
   billion years to go through them all.

   References ---------------------------------------------------------------------

   "FIPS 46-3, Data Encryption Standard", U.S. Department of Commerce/National
   Institute of Standards and Technology, 1977-1999,
   http://csrc.nist.gov/publications/fips/fips46-3/fips46-3.pdf

   "Efficient Implementation of the Data Encryption Standard", Dag Arne Osvik,
   2003, http://www.ii.uib.no/~osvik/pub/hf.pdf

   "Details of the Data Encryption Standard", John Savard, 2005
   http://www.quadibloc.com/crypto/co040201.htm

   "Javascript DES Example", Eugene Styer, 2006
   http://people.eku.edu/styere/Encrypt/JS-DES.html

*//*==========================================================================*/
#include "des.h"

#include <stdexcept>

#include "bits/parity.h"
#include "bits/rotate.h"
#include "endian/endian.h"

// \bug security clear the keyschedule
DES::DES(
	const unsigned char* key,
	std::size_t          nkey,
	std::size_t          nblock
)
	: BlockCipher(64, false)
{
	if ( nkey != 64 || nblock != 64 )
	{
		throw std::invalid_argument("Key and block size must be 8-bytes");
	}

	// Check parity of key bytes
	for (std::size_t j = 0; j < 8; j++)
	{
		if ( parity(key[j]) == 0 )
		{
			throw std::invalid_argument("Parity invalid for key");
		}
	}

	// Initialize our work variables with the provided key material
	unsigned long D = beread< 32 >(key);
	unsigned long C = beread< 32 >(key + 4);

	PC1(C, D);

	// Calculate the Round Keys
	for (unsigned i = 0; i < 16; i++)
	{
		unsigned long K = 0;

		unsigned long k = 0; // temporary variables

		// Each round rotates C and D by one or two bits
		if ( ( 0x7EFC >> i & 1 ) != 0 ) // Two-bit rotate signified by set bit
		{
			C = ( C << 2 & 0xFFFFFFCUL ) | ( C >> 26 & 0x3UL );
			D = ( D << 2 & 0xFFFFFFCUL ) | ( D >> 26 & 0x3UL );
		}
		else // One-bit rotate signified by clear bit
		{
			C = ( C << 1 & 0xFFFFFFEUL ) | ( C >> 27 & 0x1UL );
			D = ( D << 1 & 0xFFFFFFEUL ) | ( D >> 27 & 0x1UL );
		}

		// Apply (modified) Permuted Choice 2 -- See Note 1
		for (int j = 0; j < 16; j++)
		{
			const unsigned* p = PC2 + ( 4 * j );
			k |= ( ( D >> p[0] ) & 1UL ) << j;
			K |= ( ( D >> p[1] ) & 1UL ) << j;
			k |= ( ( C >> p[2] ) & 1UL ) << ( j + 16 );
			K |= ( ( C >> p[3] ) & 1UL ) << ( j + 16 );
		}

		// Save the (modified) roundkey -- See Note 1
		keySchedule[2 * i]     = k & 0x3F3F3F3FUL;
		keySchedule[2 * i + 1] = K & 0x3F3F3F3FUL;

		K = k = 0; // security clear
	}

	C = D = 0; // security clear
}

DES::~DES()
{
	for (unsigned i = 0; i < 32; ++i)
	{
		keySchedule[i] = 0;
	}
}

/***************************************************************************/ /**

   This function provides the core DES transformation of a single block. Since C++
   does not officially support a 64-bit type, the block is input via the array B.
   The function expects the high 32-bits of the input to be in B[0], and the low
   32-bits of the input to be in B[1].

   The result of the transform is stored back in B in the same "high word first"
   order.

   The second parameter points to the key schedule which must be created with
   DES_KeySchedule().

   In general, this function should not be called directly by an application.
   DES() should be called instead.

 *******************************************************************************/
void DES::inverseTransform(std::uint_least32_t* B)
{
	if ( B == nullptr )
	{
		return;
	}

	unsigned long L = B[0];
	unsigned long R = B[1];

	IP(L, R);

	// Save rotates inside loop by doing ROL(R,1) and ROL(L,1) here
	L = rol32(L, 1);
	R = rol32(R, 1);

	// Feistel Network
	for (unsigned j = 0; j < 8; j++) // 16 rounds unrolled once
	{
		unsigned long E;

		E  = keySchedule[30 - 4 * j] ^ L;
		R ^= ( SBOX[E & 0x3F] & 0x10041040UL ) ^ ( SBOX[E >> 8 & 0x3F] & 0x20404010UL )
		     ^ ( SBOX[E >> 16 & 0x3F] & 0x00802081UL ) ^ ( SBOX[E >> 24 & 0x3F] & 0x80108020UL );
		E  = keySchedule[31 - 4 * j] ^ rol32(L, 28);
		R ^= ( SBOX[E & 0x3F] & 0x04200802UL ) ^ ( SBOX[E >> 8 & 0x3F] & 0x42080100UL )
		     ^ ( SBOX[E >> 16 & 0x3F] & 0x08020208UL ) ^ ( SBOX[E >> 24 & 0x3F] & 0x01010404UL );

		// Unrolled round to save L and R exchange
		E  = keySchedule[28 - 4 * j] ^ R;
		L ^= ( SBOX[E & 0x3F] & 0x10041040UL ) ^ ( SBOX[E >> 8 & 0x3F] & 0x20404010UL )
		     ^ ( SBOX[E >> 16 & 0x3F] & 0x00802081UL ) ^ ( SBOX[E >> 24 & 0x3F] & 0x80108020UL );
		E  = keySchedule[29 - 4 * j] ^ rol32(R, 28);
		L ^= ( SBOX[E & 0x3F] & 0x04200802UL ) ^ ( SBOX[E >> 8 & 0x3F] & 0x42080100UL )
		     ^ ( SBOX[E >> 16 & 0x3F] & 0x08020208UL ) ^ ( SBOX[E >> 24 & 0x3F] & 0x01010404UL );

		E = 0; // security clear
	}

	// Fix up L and R for lack of rotates inside loop
	L = rol32(L, 31);
	R = rol32(R, 31);

	IIP(L, R);

	B[0] = R;
	B[1] = L;

	R = L = 0;

	return;
}

void DES::transform(std::uint_least32_t* B)
{
	if ( B == nullptr )
	{
		return;
	}

	unsigned long L = B[0];
	unsigned long R = B[1];

	IP(L, R);

	// Save rotates inside loop by doing ROL(R,1) and ROL(L,1) here
	L = rol32(L, 1);
	R = rol32(R, 1);

	// Feistel Network
	for (unsigned j = 0; j < 8; j++) // 16 rounds unrolled once
	{
		unsigned long E;

		E  = keySchedule[4 * j] ^ L;
		R ^= ( SBOX[E & 0x3F] & 0x10041040UL ) ^ ( SBOX[E >> 8 & 0x3F] & 0x20404010UL )
		     ^ ( SBOX[E >> 16 & 0x3F] & 0x00802081UL ) ^ ( SBOX[E >> 24 & 0x3F] & 0x80108020UL );
		E  = keySchedule[4 * j + 1] ^ rol32(L, 28);
		R ^= ( SBOX[E & 0x3F] & 0x04200802UL ) ^ ( SBOX[E >> 8 & 0x3F] & 0x42080100UL )
		     ^ ( SBOX[E >> 16 & 0x3F] & 0x08020208UL ) ^ ( SBOX[E >> 24 & 0x3F] & 0x01010404UL );

		// Unrolled round to save L and R exchange
		E  = keySchedule[4 * j + 2] ^ R;
		L ^= ( SBOX[E & 0x3F] & 0x10041040UL ) ^ ( SBOX[E >> 8 & 0x3F] & 0x20404010UL )
		     ^ ( SBOX[E >> 16 & 0x3F] & 0x00802081UL ) ^ ( SBOX[E >> 24 & 0x3F] & 0x80108020UL );
		E  = keySchedule[4 * j + 3] ^ rol32(R, 28);
		L ^= ( SBOX[E & 0x3F] & 0x04200802UL ) ^ ( SBOX[E >> 8 & 0x3F] & 0x42080100UL )
		     ^ ( SBOX[E >> 16 & 0x3F] & 0x08020208UL ) ^ ( SBOX[E >> 24 & 0x3F] & 0x01010404UL );

		E = 0; // security clear
	}

	// Fix up L and R for lack of rotates inside loop
	L = rol32(L, 31);
	R = rol32(R, 31);

	IIP(L, R);

	B[0] = R;
	B[1] = L;

	R = L = 0;

	return;
}

void DES::IP(
	// Initial Permutation using Hoey's method
	unsigned long& L,
	unsigned long& R
)
{
	unsigned long W; // a temporary variable

	L  = rol32(L, 4);
	W  = ( L ^ R ) & 0xF0F0F0F0UL;
	L ^= W;
	R ^= W;
	L  = rol32(L, 12);
	W  = ( R ^ L ) & 0xFFFF0000UL;
	L ^= W;
	R ^= W;
	L  = rol32(L, 18);
	W  = ( R ^ L ) & 0xCCCCCCCCUL;
	L ^= W;
	R ^= W;
	L  = rol32(L, 6);
	W  = ( R ^ L ) & 0xFF00FF00UL;
	L ^= W;
	R ^= W;
	L  = rol32(L, 25);
	W  = ( R ^ L ) & 0xAAAAAAAAUL;
	L ^= W;
	R ^= W;
	L  = rol32(L, 31);
	W  = 0; // security clear
}

void DES::IIP(
	// Inverse Initial Permutation using Hoey's Method
	unsigned long& L,
	unsigned long& R
)
{
	unsigned long W;

	R  = rol32(R, 1);
	W  = ( L ^ R ) & 0xAAAAAAAAUL;
	R ^= W;
	L ^= W;
	R  = rol32(R, 7);
	W  = ( L ^ R ) & 0xFF00FF00UL;
	R ^= W;
	L ^= W;
	R  = rol32(R, 26);
	W  = ( L ^ R ) & 0xCCCCCCCCUL;
	R ^= W;
	L ^= W;
	R  = rol32(R, 14);
	W  = ( L ^ R ) & 0xFFFF0000UL;
	R ^= W;
	L ^= W;
	R  = rol32(R, 20);
	W  = ( L ^ R ) & 0xF0F0F0F0UL;
	R ^= W;
	L ^= W;
	R  = rol32(R, 28);

	W = 0; // security clear
}

void DES::PC1(
	// Permuted Choice 1
	unsigned long& C,
	unsigned long& D
)
{
	unsigned long A;

	unsigned long B;

	unsigned long x;

	unsigned long y; // temporary variables

	// The following calculates PC-1 through masks and shifts
	A = C ^ ( ( C ^ ( D << 4 ) ) & 0xF0F0F0F0UL );
	B = D ^ ( ( D ^ ( C >> 4 ) ) & 0x0F0F0F0FUL );

	/* 64-bit version
	   A  = (A << 32) | (B & 0xFFFFFFFF);
	   x  = (A << 14) & 0xFFFFC000FFFFC000UL;
	   x  = (A ^ x)   & 0x3333000033330000UL;
	   A ^= x;
	   A ^= (x >> 14) & 0x0003FFFF0003FFFFUL;
	   x  = (A <<  7) & 0xFFFFFF80FFFFFF80UL;
	   x  = (A ^ x)   & 0x5500550055005500UL;
	   A ^= x;
	   A ^= (x >>  7) & 0x01FFFFFF01FFFFFFUL;
	   B  = A & 0xFFFFFFFF;
	   A  = (A >> 32) & 0xFFFFFFFF;
	   // */
	x  = A << 14;
	y  = B << 14;
	x  = ( A ^ x ) & 0x33330000UL;
	y  = ( B ^ y ) & 0x33330000UL;
	A ^= x;
	B ^= y;
	A ^= ( x >> 14 ) & 0x0003FFFFUL;
	B ^= ( y >> 14 ) & 0x0003FFFFUL;

	x  = A << 7;
	y  = B << 7;
	x  = ( A ^ x ) & 0x55005500UL;
	y  = ( B ^ y ) & 0x55005500UL;
	A ^= x;
	B ^= y;
	A ^= x >> 7;
	B ^= y >> 7;

	C = ( B >> 4 ) & 0xFFFFFFFUL;
	D = ( ( A << 12 ) & 0xFF00000UL ) + ( ( A >> 4 ) & 0xFF000UL ) + ( ( A >> 20 ) & 0xFF0UL ) + ( B & 0xFUL );

	A = B = 0; // Security clear
}

const unsigned DES::PC2[64] = { 24, 3, 26, 20, 27, 22, 15, 2, 20, 0, 8, 24, 6, 17, 1, 16, 14, 7, 21, 9, 10, 12,
	                            12, 5, 0, 0, 0, 0, 0, 0, 0, 0, 8, 1, 18, 23, 23, 9, 7, 27, 11, 19, 22, 4,
	                            5, 25, 13, 17, 16, 4, 0, 11, 26, 15, 25, 14, 0, 0, 0, 0, 0, 0, 0, 0 };

const unsigned long DES::SBOX[64] = { // Rearranged S-Box
	0xb1b1b779UL, 0xae6ab383UL, 0x060de883UL, 0x7b57d5fcUL, 0x39d92a84UL, 0x14811df7UL, 0xe0f24a7fUL, 0x9e69aa21UL,
	0xe42e4d6aUL, 0x99fde438UL, 0x9bd5b54cUL, 0xe48e3593UL, 0xdb46979fUL, 0x671f504cUL, 0x2daa11a2UL, 0xc0904a7fUL,
	0x1e148c05UL, 0x51782c7aUL, 0xf9aad632UL, 0x04817e41UL, 0xc4475782UL, 0x6fafc150UL, 0x5f3fa979UL, 0xb376379eUL,
	0x6ef912fdUL, 0xc1020b25UL, 0x01426896UL, 0x7ef5caceUL, 0x1228e978UL, 0x9ad2f68eUL, 0xe6d576c5UL, 0x292c89a1UL,
	0x2c8f52c0UL, 0x4bb58d35UL, 0xb0f6313cUL, 0x07d17a8bUL, 0xe543ccf3UL, 0x9f7e0242UL, 0xcf30d312UL, 0x608cb552UL,
	0x93713386UL, 0xe483c0c8UL, 0x7e8b0ee9UL, 0x9b3cc335UL, 0x7cbcad79UL, 0x30406bb7UL, 0x0364ec87UL, 0xfe6b1cccUL,
	0x4bc9efbfUL, 0xb45fd1c7UL, 0x4f350843UL, 0xff6a277cUL, 0x13bc083dUL, 0x9002f62dUL, 0xf0cb36e6UL, 0x4ff14cbbUL,
	0x181ef545UL, 0x0b643f9aUL, 0xe5a4d718UL, 0x088ab861UL, 0xa44352ceUL, 0xe49d0c68UL, 0x9a5aa938UL, 0x71b7f396UL };

/* DES

   The wrapper function for performing DES encryption/decryption over an
   entire set of data. It accepts an input and output array, a key, flags, and
   an initialization vector if required. Note that the arrays are assumed to be
   in big endian order (most significant byte first). So if your key is the hex
   number 0x1122334455667788, it should be stored with 0x11 in the lowest byte,
   followed by 0x22, up to 0x88 at the highest byte. The same is true of the
   IV.

   Return Value

   If there are no errors, the function returns CIPHER_OK. It can also return
   CIPHER_ERROR_IV if an initialization vector is required, but not specified;
   CIPHER_ERROR_DESTSIZE if the destination buffer is not large enough to hold the
   encrypted data; DES_ERROR_KEY if the provided key fails the parity check.

   Mode of Operation

   This function accepts ECB, CBC, CFB, and OFB modes of operation. Just specify
   the appropriate flag (ex., CIPHERBLOCK_CHAINING for cipher block chaining).

   Key Parity

   Please be aware that DES requires each key byte to have odd parity, with the
   lowest bit in each byte being the parity bit. By default, the code below
   ensures that this is the case. This check can be skipped by specifying the
   flag DES_IGNORE_KEY.

   ------------------------------------------------------------------------------*/

/*
   <sect1><title>Implementation of the Block Transformation</title>

   <para>Before anything, we must establish some preliminary values. We will assume the
    data we are transforming is in the array of 8-bit bytes, <varname>input</varname>. The particular
    64-bit block we are reading will be numbered <varname>block</varname> (starting at zero). Finally,
    the "left and right halves" of the Feistel network will be labelled <varname>L</varname>, and
    <varname>R</varname>, respectively. We can then load the block with the following code:</para>

   <para>In other words, <varname>L</varname> is the first 32 bits, and <varname>R</varname> is the next 32 bits.
    <emphasis role="strong">Note:</emphasis> one might be tempted, for each value, to use a long cast and read all four
    bytes at once. This will result in better performance BUT it will not be portable. If you
    decide to do this, ensure that the platform you are working on is <emphasis>little endian</emphasis> and
    that <emphasis>long is 32-bits</emphasis> - this is by no means guaranteed.</para>

   <sect2>
   <title>Initial Permutation</title>
   <para>As mentioned above, the first thing the function "does" is the initial permutation.
    Mathematically, this is exactly what it says it is: a permutation (re-ordering) of the
    64 input bits<superscript><link xlink:href="#rem03">3</link></superscript>. The specification lists exactly which bits end up where. For example,
    bit 58 goes to bit 1, and bit 1 goes to bit 40, etc. It is reasonable enough to write
    something like this:</para>

   <programlisting><phrase class="type">unsigned</phrase> bitmapping<phrase class="operator">[</phrase><phrase class="int">64</phrase><phrase class="operator">] = { ... };</phrase>

   pre_perm<phrase class="operator"> = ...;</phrase><phrase class="comment"> // the value before we permute
   </phrase>permuted<phrase class="operator"> =</phrase><phrase class="int"> 0</phrase><phrase class="operator">;</phrase><phrase class="comment">   // the permuted value
   </phrase><phrase class="flow">for</phrase><phrase class="operator"> (</phrase><phrase class="type">int</phrase> i<phrase class="operator"> =</phrase><phrase class="int"> 0</phrase><phrase class="operator">;</phrase> i<phrase class="operator"> &lt;</phrase><phrase class="int"> 64</phrase><phrase class="operator">;</phrase> i<phrase class="operator">++)</phrase>
    permuted<phrase class="operator"> |= (((</phrase>pre_perm<phrase class="operator"> &gt;&gt;</phrase> i<phrase class="operator">) &amp;</phrase><phrase class="int"> 1</phrase><phrase class="operator">) &lt;&lt;</phrase> bitmapping<phrase class="operator">[</phrase>i<phrase class="operator">]);</phrase></programlisting>

   <para>This is a simple loop which gets each bit of the input in turn, and "slides it over" to
    its final position (indicated by the array <varname>bitmapping</varname>). This is a straight-forward
    enough solution but suffers from a couple of drawbacks. None of them are a barrier to success,
    but they all suggest a better way of doing things.</para>

   <para>The first drawback is that we would need <varname>permuted</varname> and <varname>pre_perm</varname> to
    be 64-bit values. This is not impossible on modern architectures, but it certainly isn't
    guaranteed by C++. Even if you're using another language with a 64-bit data type (C, for
    instance), there are properties about the loop that discourage this approach. Besides,
    we already have <varname>L</varname> and <varname>R</varname> broken up neatly into 32 bits.</para>

   <para>The second drawback is the loop overhead. Small as it may be, the looping requires us
    to have a counter variable, an array of bit positions, and a couple shifts <emphasis>per</emphasis>
    bit. This is a lot considering that we are only getting 1/8th of a byte per loop. If
    you were working in assembly you <emphasis>might</emphasis> be able to improve on this method
    slightly by using bit test and bit set instructions. Most high-level languages do not
    provide this functionality, though.</para>

   <para>As it turns out, the permutation is highly regular and corresponds to transposing an
    8x8 matrix of bits, followed by some row exchanges. Luckily this can be done with only
    one extra word of storage using <emphasis role="strong">Hoey's method</emphasis>. Instead of performing
    64 loops we move whole blocks of bits at a time.</para>
   <para>Consider transposing an actual matrix. Although you could simply exchange elements
    across the main diagonal, you could also do it recursively: break the matrix into four
    smaller matrices of the same size. Then exchange the top-right and bottom-left submatrices.
    Then transpose the four by breaking <emphasis>those</emphasis> into four matrices, etc.</para>
   <para>For a regular matrix this would actually be a pretty terrible way of doing the
    transposition. However, in our case our matrix is packed into only two words and entire
    "submatrices" can be moved with masking and shifting. This is exactly how Hoey's method
    works (along with some rotations to accomplish the "row exchanges"). Dag Arne Osvik has
    a great visualizition in <link xlink:href="http://www.ii.uib.no/~osvik/pub/hf.pdf">a paper he
        has written</link> about optimizing DES. The code is as follows:</para>

   <programlisting><phrase class="comment">// Initial Permutation using Hoey's Method</phrase>
   <phrase class="type">unsigned long</phrase> W<phrase class="operator">;</phrase><phrase class="comment"> // a temporary variable</phrase>

   L<phrase class="operator"> = (</phrase>L<phrase class="operator"> &lt;&lt;</phrase><phrase class="int">  4</phrase><phrase class="operator"> |</phrase> L<phrase class="operator"> &gt;&gt;</phrase><phrase class="int"> 28</phrase><phrase class="operator">) &amp;</phrase><phrase class="int"> 0xFFFFFFFFUL</phrase><phrase class="operator">;</phrase>
   W<phrase class="operator"> = (</phrase>L<phrase class="operator"> ^</phrase> R<phrase class="operator">) &amp;</phrase><phrase class="int"> 0xF0F0F0F0UL</phrase><phrase class="operator">;</phrase> L<phrase class="operator"> ^=</phrase> W<phrase class="operator">;</phrase> R<phrase class="operator"> ^=</phrase> W<phrase class="operator">;</phrase>
   L<phrase class="operator"> = (</phrase>L<phrase class="operator"> &lt;&lt;</phrase><phrase class="int"> 12</phrase><phrase class="operator"> |</phrase> L<phrase class="operator"> &gt;&gt;</phrase><phrase class="int"> 20</phrase><phrase class="operator">) &amp;</phrase><phrase class="int"> 0xFFFFFFFFUL</phrase><phrase class="operator">;</phrase>
   W<phrase class="operator"> = (</phrase>R<phrase class="operator"> ^</phrase> L<phrase class="operator">) &amp;</phrase><phrase class="int"> 0xFFFF0000UL</phrase><phrase class="operator">;</phrase> L<phrase class="operator"> ^=</phrase> W<phrase class="operator">;</phrase> R<phrase class="operator"> ^=</phrase> W<phrase class="operator">;</phrase>
   L<phrase class="operator"> = (</phrase>L<phrase class="operator"> &lt;&lt;</phrase><phrase class="int"> 18</phrase><phrase class="operator"> |</phrase> L<phrase class="operator"> &gt;&gt;</phrase><phrase class="int"> 14</phrase><phrase class="operator">) &amp;</phrase><phrase class="int"> 0xFFFFFFFFUL</phrase><phrase class="operator">;</phrase>
   W<phrase class="operator"> = (</phrase>R<phrase class="operator"> ^</phrase> L<phrase class="operator">) &amp;</phrase><phrase class="int"> 0xCCCCCCCCUL</phrase><phrase class="operator">;</phrase> L<phrase class="operator"> ^=</phrase> W<phrase class="operator">;</phrase> R<phrase class="operator"> ^=</phrase> W<phrase class="operator">;</phrase>
   L<phrase class="operator"> = (</phrase>L<phrase class="operator"> &lt;&lt;</phrase><phrase class="int">  6</phrase><phrase class="operator"> |</phrase> L<phrase class="operator"> &gt;&gt;</phrase><phrase class="int"> 26</phrase><phrase class="operator">) &amp;</phrase><phrase class="int"> 0xFFFFFFFFUL</phrase><phrase class="operator">;</phrase>
   W<phrase class="operator"> = (</phrase>R<phrase class="operator"> ^</phrase> L<phrase class="operator">) &amp;</phrase><phrase class="int"> 0xFF00FF00UL</phrase><phrase class="operator">;</phrase> L<phrase class="operator"> ^=</phrase> W<phrase class="operator">;</phrase> R<phrase class="operator"> ^=</phrase> W<phrase class="operator">;</phrase>
   L<phrase class="operator"> = (</phrase>L<phrase class="operator"> &lt;&lt;</phrase><phrase class="int"> 25</phrase><phrase class="operator"> |</phrase> L<phrase class="operator"> &gt;&gt;</phrase><phrase class="int">  7</phrase><phrase class="operator">) &amp;</phrase><phrase class="int"> 0xFFFFFFFFUL</phrase><phrase class="operator">;</phrase>
   W<phrase class="operator"> = (</phrase>R<phrase class="operator"> ^</phrase> L<phrase class="operator">) &amp;</phrase><phrase class="int"> 0xAAAAAAAAUL</phrase><phrase class="operator">;</phrase> L<phrase class="operator"> ^=</phrase> W<phrase class="operator">;</phrase> R<phrase class="operator"> ^=</phrase> W<phrase class="operator">;</phrase>
   L<phrase class="operator"> = (</phrase>L<phrase class="operator"> &gt;&gt;</phrase><phrase class="int"> 1</phrase><phrase class="operator"> |</phrase> L<phrase class="operator"> &lt;&lt;</phrase><phrase class="int"> 1</phrase><phrase class="operator">) &amp;</phrase><phrase class="int"> 0xFFFFFFFFUL</phrase><phrase class="operator">;</phrase>
   </programlisting>

   <para>A smart compiler will recognize the rotates for what they are and replace the 'two
    shifts plus an OR' with a single rotate left or right instruction. Also, some may wonder
    why there is a mask immediately after each rotate. The reason is that systems with longs
    greater than 32 bits will mix in extra bits. Again, a smart compiler should recognize
    this as a nop.<superscript><link xlink:href="#rem04">4</link></superscript></para>
   </sect2>
   <sect2>
   <title id="rf">Round Function</title>
   <para>After the initial permutation, sixteen Feistel rounds are performed. Only briefly
    alluded to above, the round actually involves several fine steps. In particular, the
    round function performs:</para>
   <orderedlist>
    <listitem>A bit expansion function, E(), of <varname>R</varname> into 48 bits</listitem>
    <listitem>XOR addition of the expanded value with an element of the <emphasis role="strong">Key Schedule.</emphasis></listitem>
    <listitem>SBOX substitution, reducing the 48-bit value back to 32-bits.</listitem>
    <listitem>Bit permutation, P()</listitem>
   </orderedlist>
   <para>The bit expansion, <varname>SBOX</varname>, and permutation will be described below in their own sections.
    It is worth mentioning a bit about the Key Schedule here. As stated at the beginning, the
    cipher is keyed with a 56-bit value. This is used to create a table of 16 values called
    the key schedule. There is one value in the table for each round. the algorithm for
    generating the key schedule is complicated in its own right and is discussed below. For
    now, just be aware that there is a table of 16 values, <varname>KS</varname>, and each value is 48-bits.</para>
   <para>Mathematically, the round function does this:</para>
   <para><code>f(R,K) = P(  SBOX(  E(R) <emphasis role="strong">xor</emphasis> KS[round]  )  )</code></para>
   <para>For reasons that will become clear later, I'm going to talk about the inner functions
    in reverse order.</para>
   <sect3>
   <title>Permutation</title>
   <para>There isn't much to say about the permutation function, <varname>P</varname>, except that it
    permutes the 32-bit output of <varname>SBOX</varname>. The function will actually be absorbed into
    the S-box below, so there truly is no need to say more about it.</para>
   </sect3>
   <sect3>
   <title>The Substitution Box</title>
   <para>A substitution box, or S-box, is a way of messing with the linearity of the cipher.
    Maybe that doesn't make any sense. Let me explain.</para>
   <para>By the time most people are 15 they've heard of linear functions. These are the very
    simple functions which take the form
    <math xmlns="http://www.w3.org/1998/Math/MathML"><mrow><mi>a</mi><mo>*</mo><mi>x</mi><mo>+</mo><mi>b</mi></mrow></math>. So if we defined p(x) = 3x+4 we would have
    p(3) = 13, p(9) = 31, p(-12) = -32, etc. Again: very simple. What if we were to ask, then,
    what is p(p(x))? We could try some specific examples: p(p(1)) = 25, p(p(3)) = 43,
    p(p(-2)) = -2. Just calculate the inner function and pop it in as the new output to the
    outer function.</para>

   <para>However, we can do better by substituting the general function, instead of an explicit value:
    p(p(y)) = p(3y+4) = 3(3y+4) +4 = 9y + 16. So our function applied twice is still a linear
    function. It is not hard to see that, in general, a linear function of a linear function
    is still linear. In this way, we can collapse many applications of a function into
    one.</para>
   <para>Much of DES is linear. It may not seem like it to someone who has a casual acquaintance
    with mathematics, but functions like xor, and shift, and permutations behave linearly.
    So if we had 16 rounds of <emphasis>just these functions</emphasis> we could collapse them. From that
    point it would be pretty easy to reverse the function - find an input given the output.
    We'd have to be clever, but someone dedicated to doing it could figure it out.</para>
   <para>This is where the S-box comes in. As I said, it "messes with" the linearity of the
    cipher. It makes the round <emphasis>non-linear</emphasis> so that the rounds cannot be collapsed,
    and working backwards is very difficult. It is a step that <emphasis>must be done</emphasis> and
    <emphasis>in a particular order</emphasis>.</para>
   <para>So we come now to what it is. Simply, it is a function that takes a number, and gives
    you another number to substitute in its place. In DES, 6-bit numbes are replaced by 4-bit
    numbers. This is how we go from 48 bits back down to 32 bits. The 48 bits are split into
    eight 6-bit blocks which are replaced by eight 4-bit blocks.</para>
   <para>There are, in reality, 8 S-boxes in DES - one for each of the eight 6-bit blocks. They
    are stored in a group of tables, <varname>S[8][4][16]</varname>. That is, eight tables with four
    rows of 16 columns. The specification says that a binary number, <emphasis>(abcdef)<subscript>b</subscript></emphasis>, will be
    substituted with the value located at row <emphasis>(af)<subscript>b</subscript></emphasis>, column <emphasis>(bcde)<subscript>b</subscript></emphasis>. The first
    block of six will use the first table for substitution, the second block will use the
    second table, etc. until we have replaced the entire 48-bit word.</para>
   <para>Personally, I find this method very strange. What's this about rows and columns? Think:
    there are 64 possibilities for the input number, 0-63. Why use 4 rows of 16 columns
    when we could use 1 row of 64 columns? And why would we arrange the tables in such a
    way as to require the bizarre 'first-bit-with-last-bit' row indexing? If we simply
    <emphasis>rearranged</emphasis> where the values were in the table we could just read the value
    straight. So let's do away with the strange indexing and define a new S-box,
    <varname>T</varname>:</para>
   <para><code>T[i][j] = S[i][(j >> 4 &amp; 2) + (j &amp; 1)][(j >> 1 &amp; 0xF)];</code></para>
   <para>This way, <varname>T[i][(abcdef)<subscript>b</subscript>] = S[i][(af)<subscript>b</subscript>][(bcde)<subscript>b</subscript>]</varname> and we don't have to do any
    extra calculations. Since the table is constant, the work can be done once while
    writing the code.</para>
   <para><emphasis>But</emphasis> I'm not satisfied, just yet. I mentioned above that the permutation
    at the end could be absorbed into the <varname>SBOX</varname> function. In some sense, the
    S-box <emphasis>is</emphasis> performing a permutation - it's just very simple. The first S-box
    puts the output into bits 0, 1, 2, and 3. The second S-box puts the output into bits
    4, 5, 6, and 7. The third S-box... well you get the idea. The obvious improvement
    then is to simply put the bits in the final, permuted places. Why put a value into
    bit 2 when bit 2 will just be moved to bit j? It makes more sense to put it there
    to begin with.</para>
   <para>To achieve this, we first tranpose the S-box and pre-shift all the values into
    the appropriate 4-bit block:</para>
   <para><code>U[i][j] = T[j][i] &lt;&lt; 4 * i;</code></para>
   <para>Then we collapse these all together by OR'ing all the shifted Sboxes into one 32
    bit value:</para>
   <para><code>W[i] = U[i][0] | U[i][1] | ... | U[i][7];</code></para>
   <para>Now the permutation is applied to each 32-bit value so bits end up in their
    final place:</para>
   <para><code>X[i] = P(W[i]);</code></para>
   <para>The next (and final) operation I ask the reader to take on faith for now. The
    step just previous would be enough to produce a S-box with our desired properties.
    Nonetheless, by rotating the values one bit to the left we can save ourselves
    some operations in the round loop<superscript><link xlink:href="#rem05">5</link></superscript>.</para>
   <para><code>SBOX[i] = (X[i] &lt;&lt; 1 | X[i] >> 31) &amp; 0xFFFFFFFFUL;</code></para>
   <para>Now, given a six bit value, we do a straight lookup and combine with the
    running (xor) sum according to the appropriate mask (which are just
    the permutations of 0xf, 0xf0, 0xf00, etc. rotated one to the left):</para>
   <para><code>mask[8] = {0x10041040UL, 0x20404010UL, 0x00802081UL, 0x80108020UL,
        0x04200802UL, 0x42080100UL, 0x08020208UL, 0x01010404UL};</code></para>
   <para>In summary, where we were previously computing <varname>P(S[j][(af)<subscript>b</subscript>][(bcde)<subscript>b</subscript>])</varname>
    we are now computing instead <varname>SBOX[(abcdef)<subscript>b</subscript>] &amp; mask[j]</varname>.
    The final SBOX<superscript><link xlink:href="#rem06">6</link></superscript> table looks like this:</para>

   <programlisting><phrase class="keyword">static const</phrase><phrase class="type"> unsigned long</phrase> SBOX<phrase class="operator">[</phrase><phrase class="int">64</phrase><phrase class="operator">] =  {</phrase><phrase class="int">
    0xb1b1b779UL</phrase><phrase class="operator">,</phrase><phrase class="int">0xae6ab383UL</phrase><phrase class="operator">,</phrase><phrase class="int">0x060de883UL</phrase><phrase class="operator">,</phrase><phrase class="int">0x7b57d5fcUL</phrase><phrase class="operator">,</phrase><phrase class="int">
    0x39d92a84UL</phrase><phrase class="operator">,</phrase><phrase class="int">0x14811df7UL</phrase><phrase class="operator">,</phrase><phrase class="int">0xe0f24a7fUL</phrase><phrase class="operator">,</phrase><phrase class="int">0x9e69aa21UL</phrase><phrase class="operator">,</phrase><phrase class="int">
    0xe42e4d6aUL</phrase><phrase class="operator">,</phrase><phrase class="int">0x99fde438UL</phrase><phrase class="operator">,</phrase><phrase class="int">0x9bd5b54cUL</phrase><phrase class="operator">,</phrase><phrase class="int">0xe48e3593UL</phrase><phrase class="operator">,</phrase><phrase class="int">
    0xdb46979fUL</phrase><phrase class="operator">,</phrase><phrase class="int">0x671f504cUL</phrase><phrase class="operator">,</phrase><phrase class="int">0x2daa11a2UL</phrase><phrase class="operator">,</phrase><phrase class="int">0xc0904a7fUL</phrase><phrase class="operator">,</phrase><phrase class="int">
    0x1e148c05UL</phrase><phrase class="operator">,</phrase><phrase class="int">0x51782c7aUL</phrase><phrase class="operator">,</phrase><phrase class="int">0xf9aad632UL</phrase><phrase class="operator">,</phrase><phrase class="int">0x04817e41UL</phrase><phrase class="operator">,</phrase><phrase class="int">
    0xc4475782UL</phrase><phrase class="operator">,</phrase><phrase class="int">0x6fafc150UL</phrase><phrase class="operator">,</phrase><phrase class="int">0x5f3fa979UL</phrase><phrase class="operator">,</phrase><phrase class="int">0xb376379eUL</phrase><phrase class="operator">,</phrase><phrase class="int">
    0x6ef912fdUL</phrase><phrase class="operator">,</phrase><phrase class="int">0xc1020b25UL</phrase><phrase class="operator">,</phrase><phrase class="int">0x01426896UL</phrase><phrase class="operator">,</phrase><phrase class="int">0x7ef5caceUL</phrase><phrase class="operator">,</phrase><phrase class="int">
    0x1228e978UL</phrase><phrase class="operator">,</phrase><phrase class="int">0x9ad2f68eUL</phrase><phrase class="operator">,</phrase><phrase class="int">0xe6d576c5UL</phrase><phrase class="operator">,</phrase><phrase class="int">0x292c89a1UL</phrase><phrase class="operator">,</phrase><phrase class="int">
    0x2c8f52c0UL</phrase><phrase class="operator">,</phrase><phrase class="int">0x4bb58d35UL</phrase><phrase class="operator">,</phrase><phrase class="int">0xb0f6313cUL</phrase><phrase class="operator">,</phrase><phrase class="int">0x07d17a8bUL</phrase><phrase class="operator">,</phrase><phrase class="int">
    0xe543ccf3UL</phrase><phrase class="operator">,</phrase><phrase class="int">0x9f7e0242UL</phrase><phrase class="operator">,</phrase><phrase class="int">0xcf30d312UL</phrase><phrase class="operator">,</phrase><phrase class="int">0x608cb552UL</phrase><phrase class="operator">,</phrase><phrase class="int">
    0x93713386UL</phrase><phrase class="operator">,</phrase><phrase class="int">0xe483c0c8UL</phrase><phrase class="operator">,</phrase><phrase class="int">0x7e8b0ee9UL</phrase><phrase class="operator">,</phrase><phrase class="int">0x9b3cc335UL</phrase><phrase class="operator">,</phrase><phrase class="int">
    0x7cbcad79UL</phrase><phrase class="operator">,</phrase><phrase class="int">0x30406bb7UL</phrase><phrase class="operator">,</phrase><phrase class="int">0x0364ec87UL</phrase><phrase class="operator">,</phrase><phrase class="int">0xfe6b1cccUL</phrase><phrase class="operator">,</phrase><phrase class="int">
    0x4bc9efbfUL</phrase><phrase class="operator">,</phrase><phrase class="int">0xb45fd1c7UL</phrase><phrase class="operator">,</phrase><phrase class="int">0x4f350843UL</phrase><phrase class="operator">,</phrase><phrase class="int">0xff6a277cUL</phrase><phrase class="operator">,</phrase><phrase class="int">
    0x13bc083dUL</phrase><phrase class="operator">,</phrase><phrase class="int">0x9002f62dUL</phrase><phrase class="operator">,</phrase><phrase class="int">0xf0cb36e6UL</phrase><phrase class="operator">,</phrase><phrase class="int">0x4ff14cbbUL</phrase><phrase class="operator">,</phrase><phrase class="int">
    0x181ef545UL</phrase><phrase class="operator">,</phrase><phrase class="int">0x0b643f9aUL</phrase><phrase class="operator">,</phrase><phrase class="int">0xe5a4d718UL</phrase><phrase class="operator">,</phrase><phrase class="int">0x088ab861UL</phrase><phrase class="operator">,</phrase><phrase class="int">
    0xa44352ceUL</phrase><phrase class="operator">,</phrase><phrase class="int">0xe49d0c68UL</phrase><phrase class="operator">,</phrase><phrase class="int">0x9a5aa938UL</phrase><phrase class="operator">,</phrase><phrase class="int">0x71b7f396UL</phrase><phrase class="operator">};</phrase>
   </programlisting>
   </sect3>
   <sect3>
   <title>The Expansion function, E</title>
   <para>It is impossible to talk about the expansion function without showing what <emphasis>exactly</emphasis>
    happens. The function does create a 48-bit value out a 32-bit value by "doubling up" many of
    the digits. How it does this is important. Assuming bits are numbered 0 to 31, lsb to msb, the
    result is the value given by the following bits (starting at lsb and moving to msb):</para>
   <para><code>{31, 0, 1, 2, 3, 4 | 3, 4, 5, 6, 7, 8 | 7, 8, 9, 10, 11, 12 | 11, 12, 13, 14, 15, 16 |
        15, 16, 17, 18, 19, 20 | 19, 20, 21, 22, 23, 24 | 23, 24, 25, 26, 27, 28 | 27, 28, 29, 30, 31, 0}
    </code></para>
   <para>The vertical bars have no special meaning except to group 6 bits at a time.</para>
   <para>Calculating this value as is would require a number of shifts, masks, etc. and would also require
    us to have an integer capable of holding 48 bits. Instead, we break the value in two by selecting
    alternating groups:</para>
   <para><code>{31, 0, 1, 2, 3, 4 | 7, 8, 9, 10, 11, 12 | 15, 16, 17, 18, 19, 20 | 23, 24, 25, 26, 27, 28}</code>
   </para>
   <para>and</para>
   <para><code>{3, 4, 5, 6, 7, 8 | 11, 12, 13, 14, 15, 16 | 19, 20, 21, 22, 23, 24 | 27, 28, 29, 30, 31, 0}</code>
   </para>
   <para>This doesn't immediately look any better than before BUT we notice something very special. Every
    place there is a vertical bar there are two numbers "missing". Consider the first bar in the first
    value, between 4 and 7. If we were counting we'd be skipping 5 and 6. If we put those values back in,
    however, we would have the original value, rotated by 1 position.</para>
   <para>This seems to work pretty well, especially considering what we've learned about the SBOX above.
    The value going into the SBOX is 6 bits - so we'll be masking off everything above 6 bits anyways. It
    really doesn't matter what is in those values. Might as well leave them as is. So now, with the bits
    'back in', the vertical bars will mark off 8 bit groups:</para>
   <para><code>{31, 0, 1, 2, 3, 4, 5, 6 | 7, 8, 9, 10, 11, 12, 13, 14 | 15, 16, 17, 18, 19, 20, 21, 22
 | 23, 24, 25, 26, 27, 28, 29, 30}</code>
   </para>
   <para>Similarly for the other value.</para>
   <para>Understanding this, we can save ourselves the calculation of <varname>E</varname>. But we do have to
    remember that <varname>E(R)</varname> is supposed to be xor'd with the round key, <varname>KS[round]</varname>.
    Not to worry - if we <link xlink:href="#repackrk">prepare <varname>KS</varname> in the same way</link> we will be
    perfectly in the clear.</para>
   </sect3>
   </sect2>
   </sect1>

   <sect1>

   <title id="ks">Key Schedule</title>

   <sect2>
   <title id="repackrk">Repacking the Round Keys</title>
   <para>The round keys and PC-2 have been modified to make the innermost loop
    simpler. The <varname>E</varname> function consists of 8 groups of 6 bits which have
    significant overlap between them. However, if we separate the odd groups
    from the even groups in <varname>E</varname>, then all the bits are in place. Since <varname>E</varname> gets
    xor'd with the roundkey, we do the same rearranging of the roundkeys.</para>

   <para>In order to facilitate this, the PC-2 function has been modified so that
    bits end up in the right odd/even byte. This basically involves padding
    PC-2 to 8-bit groups instead of 6-bit groups, and exchanging row 2 with 3
    and 6 with  7.</para>

   <para>To be completed...</para>
   </sect2>
   </sect1>

   <sect1><title id="rem">Remarks</title>
   <orderedlist>
    <listitem id="rem01">
        <para>Although I refer the reader to the spec for details, I don't really
            want to. The actual specification, FIPS 46-3, is unnecessarily confusing in many
            respects, so I've avoided reproducing many of the tables, functions, etc.
            For example, the bit numbering scheme is that bits are numbered 1 through
            64, starting at the most significant bit down to the least significant
            bit. Perhaps. It's hard to know for sure because text doesn't explicitly
            say. I had to refer to external documents and cross-reference various clues
            in the text to arrive at my conclusion.</para>
        <para>Furthermore, the actual functions described perform horribly and are
            mathematically equivalent to other, simpler, transforms. Strictly speaking,
            some of them are ambiguous.The tables are largely unnecessary (except for
            PC-2 and SBOX). All of this is, I feel, because the spec is some thirty
            years old and many common conventions might not have been established then.
            Though I find this hard to believe considering many other papers from the
            same era are perfectly rigorous.</para>
    </listitem>
    <listitem id="rem02">
        <para>Although I said that DES cannot be deciphered without the key, this is
            a bit of an exaggeration. After 30 years of scrutiny, and advancements in
            technology, the DES is considered a pretty weak cipher. That is, it can
            be deciphered without the key relatively easily. However, "guessing" is
            still a silly approach. Differential cryptanalysis is a much better
            tool.</para>
    </listitem>
    <listitem id="rem03">
        <para>A careful reader may note that the initial permutation and inverse IP
            serve no secure purpose whatsoever. This is true. Reordering bits does not
            increase the security of a transmitted block of data when the ordering is
            fixed and known. My understanding is that this was introduced simply to
            slow down devices meant to break the DES back in the late 70s. This is no
            longer a significant deterrant</para>
        <para>Note that this is similar to, but different from, whitening. Whitening
            has a security purpose; the IP does not.</para>
    </listitem>
    <listitem id="rem04">
        <para>I have made several comments toward what a "smart compiler" should do.
            Even good compilers do not recognize some obvious optimizations. For
            example, performing a bitwise AND with a value where all bits are set. This
            is a nop - it has no effect. GCC will happily compile this code with
            optimizations set to the max. If you're serious about performance, it
            is best to check the disassembled code.</para>
    </listitem>
    <listitem id="rem05">
        <para>The extra mysterious rotate in the <varname>SBOX</varname> calculation balances a single
            rotate in the <varname>E</varname> function. It turns out that we can pull these
            rotates out of the loop into a simple pre- and post-rotate. Inside the loop the
            rotates can balance each other out. Outside, they actually cancel with the
            operations done with Hoey's method.</para>
    </listitem>
    <listitem id="rem06">
        <para>Note that this S-box takes half the size of the original. The new S-box
            takes 4*64=256 bytes versus 8*4*16=512 bytes. It also has word-aligned
            accesses. The <varname>mask</varname> array takes another 32 bytes but we are still
            saving 224 bytes and getting many performance advantages.</para>
    </listitem>
   </orderedlist>
   </sect1>

   </article>
   <!-- */// -->
