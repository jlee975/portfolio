#ifndef EVENT_QUEUE
#define EVENT_QUEUE

#include <queue>
#include <string>

#include <condition_variable>
#include <mutex>

#include "concurrency/queue.h"

#include "event.h"

class EventQueue
{
public:
	EventQueue();
	virtual ~EventQueue();

	template< typename...Args >
	void post(Args&&... args)
	{
		post_inner(std::make_unique< Event >(std::forward< Args >(args)...) );
	}

	void sleep();
	void wake();
	void quit(bool);
	void operator()();
private:
	static const Event sleepEvent;
	static const Event wakeEvent;
	static const Event quitEvent;

	void clear();
	void post_inner(std::unique_ptr< const Event >);

	concurrent_queue< std::unique_ptr< const Event > > events;

	std::mutex              wait_mutex;
	std::condition_variable event_condition;

	virtual void handler(const Event*);
	virtual bool idle();
	virtual void init();
	virtual void leave();
};

#endif // ifndef EVENT_QUEUE
