/***************************************************************************/ /**
   @class       EventQueue

   A C++ wrapper class for a POSIX thread with its own event loop.

   This class has no functionality in and of itself; it must be subclassed. The
   two virtual functions EventHandler() and Idle() must be implemented in the
   derived class.

   @todo Start up sleeping
 *******************************************************************************/
#include "eventqueue.h"

EventQueue::EventQueue() = default;

EventQueue::~EventQueue() = default;

/// @todo What kind of synchronization do we need at the front? Only one
/// consumer.
/**
   The event loop; the core of the thread's execution. Called by boost.

   @todo: If handler indicates it didn't handle the event, go to idle
   @todo: The wait time is kind of arbitrary
   @todo: If this isn't called by a thread, it starts an infinite loop
 */
void EventQueue::operator()()
{
	clear(); // Clear the queue
	init();

	sleep(); // Put a sleep event in the queue

	while ( true )
	{
		std::unique_ptr< const Event > e;

		if ( events.try_pop(e) )
		{ // Handle the event
			switch ( e->type() )
			{
			case Event::Quit:
				leave();
				clear();
				return;

			case Event::Sleep:
			{
				std::unique_lock< std::mutex > lk(wait_mutex);
				event_condition.wait(lk);
			}
			break;
			case Event::WakeUp:
				break;
			default:
				handler(e.get() );
				break;
			}
		}
		else if ( !idle() )
		{ // Run the idle code and pause if needed
			std::unique_lock< std::mutex > lk(wait_mutex);
			event_condition.wait_for(lk, std::chrono::milliseconds(1) );
		}
	}
}

// These should never be called unless the derived class is destructed while
// run() is still in its loop. To prevent this, call wait() from derived
// destructor
bool EventQueue::idle()
{
	return false;
}

void EventQueue::handler(const Event*)
{
}

void EventQueue::init()
{
}

void EventQueue::leave()
{
}

/* PostEvent -------------------------------------------------------------------

   Adds an event to the queue. Internally, this is considered an event of type
   EVENT_USER. Events added this way will be passed to the virtual function
   EventHandler().
   ------------------------------------------------------------------------------*/
void EventQueue::post_inner(std::unique_ptr< const Event > ev)
{
	events.push(std::move(ev) );
	event_condition.notify_one();
}

// Sleep until an event is posted
void EventQueue::sleep()
{
	post(Event::Sleep);
}

// Simply posts an event that will exit from sleep
void EventQueue::wake()
{
	post(Event::WakeUp);
}

/** @brief Put a message in the event queue to quit, and terminate the thread.
 */
void EventQueue::quit(bool immediate)
{
	if ( immediate )
	{
		clear();
	}

	post(Event::Quit);
}

void EventQueue::clear()
{
	events.clear();
}
