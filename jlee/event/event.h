#ifndef EVENT_HPP
#define EVENT_HPP

#include <string>

/** @brief A simple class for describing a system event
 */
class Event
{
public:
	enum Type
	{
		Idle,
		Quit,
		Sleep,
		WakeUp,
		Stop,
		Play,
		Pause,
		User = 1000
	};

	Event();
	explicit Event(Type, int = 0, std::string = {});
	Event(Type, std::string);
	virtual ~Event();

	static Type registerEvent();

	Type type() const;
	std::string message() const;
	int data() const;
private:
	static Type nextUserEvent;

	Type type_;

	/// Event data
	int         data_;
	std::string message_;
};

#endif // ifndef EVENT_HPP
