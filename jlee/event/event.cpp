/***************************************************************************/ /**
   @file        event.cpp
 *******************************************************************************/
#include "event.h"

#include <stdexcept>

#include <mutex>

std::mutex register_mutex;

Event::Event()
	: Event(Idle)
{
}

Event::Event(
	Type        t,
	std::string u
)
	: Event(t, 0, std::move(u) )
{
}

Event::Event(
	Type        et,
	int         i,
	std::string u
)
	: type_(et), data_(i), message_(std::move(u) )
{
}

Event::~Event() = default;

Event::Type Event::type() const
{
	return type_;
}

std::string Event::message() const
{
	return message_;
}

int Event::data() const
{
	return data_;
}

Event::Type Event::registerEvent()
{
	std::lock_guard< std::mutex > lk(register_mutex);

	if ( nextUserEvent < User )
	{
		throw std::runtime_error("Too many events registered!");
	}

	Type reg = nextUserEvent;

	nextUserEvent = static_cast< Type >( nextUserEvent + 1 );
	return reg;

	/** @todo Replace mutex with atomic access
	   static std::atomic<int> nextUserEvent(User);

	   Type reg = nextUserEvent++;
	   if (static_cast<Event::Type>(reg) < User)
	    throw std::runtime_error("Too many events registered");
	 */
}

Event::Type Event::nextUserEvent = User;
