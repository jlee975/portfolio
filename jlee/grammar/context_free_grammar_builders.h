#ifndef CONTEXT_FREE_GRAMMAR_BUILDERS_H
#define CONTEXT_FREE_GRAMMAR_BUILDERS_H

#include <span>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include "containers/containers.h"
#include "containers/interval_set.h"

#include "production_rules.h"
#include "repeat_info.h"

namespace jl
{
namespace grammar
{
namespace details
{
using sequence_id = std::size_t;

/// @todo Consider making a LITERAL for each Terminal and/or string of Terminals
/// @todo ETA production? empty LITERAL or empty cat? Currently empty cat
/// @todo LITERAL for single characters, short strings, and strings that should be tested with memcmp
enum rule_kind
{
	LITERAL,         ///< Match a specific sequence of characters
	CHARACTER_CLASS, ///< Match one character, from a list
	ALT,             ///< Match rule A or rule B
	CAT,             ///< Match rule A, then rule B
	EXCEPT,          ///< Match rule A and @em not rule B
	NESTED,
	NONTERMINAL ///< Indirection for another sequence, where we need to record the non-terminal
};

struct oc_info
{
	std::size_t openlit;
	std::size_t closelit;

	/// Total length taken by both open and closed literal
	/// @todo This is only accessed when building the minimum match length. It shouldn't
	/// be stored. Furthermore, it could probably be calculated by finding the min_match_length
	/// for openlit and closelit
	std::size_t totallen;
};

struct rule
{
	/// The kind of rule this element represents
	rule_kind kind;

	/// The next rule in the matching, based on the type. For literals and character classes, refers
	/// to "lits" and "classes" members, respectively
	std::size_t A;

	friend bool operator==(
		const rule& l,
		const rule& r
	)
	{
		return l.kind == r.kind && l.A == r.A;
	}

};

/// @todo Can probalby just put the ocinfo directly in here instead of another indirection
struct nested_info
{
	/// Index of the open and close literals
	std::size_t ocindex;

	/// sequence id of cat of sequence ids between the open and close literals
	sequence_id cat;
};

class cibuilder
{
public:
	explicit cibuilder(std::size_t n);

	void init_non_terminal_alts(std::vector< std::vector< sequence_id > > allrules);
	void record_nested_pair(sequence_id o, sequence_id c, std::size_t len);
	sequence_id add_rule(rule_kind, std::size_t);
	sequence_id add_cat(std::vector< sequence_id > terms);
	void set_literal_length(sequence_id i, std::size_t len);
	void build_min_match_length();
	sequence_id add_exception(sequence_id l, sequence_id l2);
	sequence_id add_repeat(sequence_id l, const repeat_info& repeat);
	/// @todo Inline alts
	/// @todo Merge heads, including common string prefixes
	/// @todo Determine if alts are mutually exclusive
	/// @todo Combine character classes and single character strings
	/// @todo If one of the alt rules is itself an alt, inline the other alts.
	// returns { false, i } if rule i already contains the sequence of alts for v
	// returns { true, i } if a rule needs to be created for alt i
	sequence_id add_alt(std::vector< sequence_id > v);
	bool rule_requires(sequence_id i, sequence_id k, std::unordered_set< sequence_id >& seen) const;

	sequence_id add_eta();

	/* Add the rules r ::= e  and r ::= { l, r }
	 *
	 * If e == l, you get Kleene +. If e == eta production, you get Kleene *
	 */
	sequence_id add_kleene(sequence_id l, sequence_id e);

	sequence_id add_kleene_star(sequence_id);

	sequence_id add_kleene_plus(sequence_id);
	sequence_id add_sequence_between_nested(std::size_t, std::vector< sequence_id >);

	/// @todo Remove eta productions in a cat
	/// @todo Inline sub-cats
	/// @todo Simplify optionals and kleene star followed by a term
	/// @todo Combine strings
	sequence_id add_cat_no_nesting(std::vector< sequence_id > v);

	std::vector< oc_info > open_to_close;

	std::vector< std::vector< sequence_id > >            alts;
	std::vector< std::vector< sequence_id > >            cats;
	std::vector< std::pair< sequence_id, sequence_id > > exceptions;

	// (index into nested_pairs, inner sequence)
	std::vector< nested_info > nested;

	std::vector< rule > subs;

	/// @todo Can infer this value from the NONTERMINAL rules in subs
	std::size_t number_of_nonterminals{ 0 };

	/// @todo Should be parallel to (or combined with) subs
	/// @todo Could reorder sequences so that those with a min length of zero are at the tail of
	/// this map. Then we only need to actually store the non-zero min lengths. Consequently,
	/// can_begin_with might be shortened (atm, it is only called if min_length != 0)
	std::unordered_map< sequence_id, std::size_t > min_match_length;
};

template< typename Terminal >
class tibuilder
	: public cibuilder
{
public:
	using character_class    = interval_set< Terminal >;
	using character_sequence = std::vector< Terminal >;

	/// @todo Need two versions of this. One taking vector< Terminal > that can be used for construction
	/// and moved into this object. A second for get_all_nested that just has a span< const Terminal >
	struct nested_pair
	{
		character_sequence open;
		character_sequence close;
	};

	using nested_pair_list = std::vector< nested_pair >;

	tibuilder(
		nested_pair_list open_to_close_,
		std::size_t      num_nonterminals
	)
		: cibuilder(num_nonterminals)
	{
		for (auto& [o, c] : open_to_close_)
		{
			const std::size_t n      = o.size() + c.size();
			const sequence_id first  = add_lit_no_split(std::move(o) );
			const sequence_id second = add_lit_no_split(std::move(c) );
			record_nested_pair(first, second, n);
		}
	}

	sequence_id add_class(const character_class& allowed)
	{
		std::size_t k = 0;

		while ( k < classes.size() && classes[k] != allowed )
		{
			++k;
		}

		if ( k == classes.size() )
		{
			classes.push_back(allowed);
		}

		return add_rule(CHARACTER_CLASS, k);
	}

	void optimize()
	{
		init_min_match_length();
		init_can_begin_with();
	}

	sequence_id add_lit(character_sequence v)
	{
		std::vector< sequence_id > sublits;

		for ( auto& vi : split_on_nested_tokens(std::move(v) ) )
		{
			sublits.push_back(add_lit_no_split(std::move(vi) ) );
		}

		if ( sublits.size() )
		{
			return sublits[0];
		}

		return add_cat_no_nesting(std::move(sublits) );
	}

	void init_min_match_length()
	{
		for (std::size_t i = 0; i < subs.size(); ++i)
		{
			if ( subs[i].kind == LITERAL )
			{
				set_literal_length(i, lits[subs[i].A].size() );
			}
		}

		build_min_match_length();
	}

	void init_can_begin_with()
	{
		/// @todo This could probalby be done smarter
		while ( true )
		{
			/// @todo Avoid the copy, use some other mechanism for detecting the change
			const auto orig = can_begin_with_map;

			for (std::size_t i = 0; i < subs.size(); ++i)
			{
				std::unordered_set< sequence_id > seen;
				update_can_begin_with(i, seen);
			}

			if ( can_begin_with_map == orig )
			{
				break;
			}
		}

		for (std::size_t i = 0; i < subs.size(); ++i)
		{
			if ( can_begin_with_map.find(i) == can_begin_with_map.end() )
			{
				throw std::logic_error("Starting terminals should be known for all rules");
			}
		}
	}

	/// @todo Maybe we just stack these character classes on top of the classes member
	void update_can_begin_with(
		sequence_id                        i,
		std::unordered_set< sequence_id >& seen
	)
	{
		// Recursive -- nothing to add
		if ( seen.count(i) != 0 )
		{
			return;
		}

		seen.insert(i);

		switch ( subs[i].kind )
		{
		case LITERAL:

			if ( const auto& v = lits[subs[i].A];!v.empty() )
			{
				can_begin_with_map[i].insert(v[0]);
			}

			break;
		case CHARACTER_CLASS:
			can_begin_with_map[i].insert(classes[subs[i].A]);
			break;
		case ALT:

			for (const auto j : alts[subs[i].A])
			{
				update_can_begin_with(j, seen);
				can_begin_with_map[i].insert(can_begin_with_map[j]);
			}

			break;
		case CAT:

			for (const auto j : cats[subs[i].A])
			{
				update_can_begin_with(j, seen);
				can_begin_with_map[i].insert(can_begin_with_map[j]);

				if ( min_match_length[j] != 0 )
				{
					break;
				}
			}

			break;
		case EXCEPT:
			/// @todo minus must_begin_with(exceptions[subs[i].A].second
		{
			const sequence_id j = exceptions[subs[i].A].first;
			update_can_begin_with(j, seen);
			can_begin_with_map[i].insert(can_begin_with_map[j]);
		}
		break;
		case NONTERMINAL:
		{
			const sequence_id j{ subs[i].A };
			update_can_begin_with(j, seen);
			can_begin_with_map[i].insert(can_begin_with_map[j]);
		}
		break;
		case NESTED:
		{
			const auto& v = lits[open_to_close.at(nested.at(subs[i].A).ocindex).openlit];
			can_begin_with_map[i].insert(v[0]);
		}
		break;
		default:
			throw std::logic_error("Unhandled case");
		} // switch

		seen.erase(i);
	}

	std::vector< character_sequence > split_on_nested_tokens(character_sequence v)
	{
		// If the literal has open and close strings, split it
		const auto&       otoc = open_to_close;
		const std::size_t m    = otoc.size();

		std::vector< character_sequence > split;

		const std::size_t n = v.size();

		std::size_t last = 0;

		for (std::size_t i = 0; i < n;)
		{
			std::size_t len = 0;

			for (std::size_t j = 0; j < m; ++j)
			{
				len = common_prefix_length(v, i, lits[otoc[j].openlit]);

				if ( len != 0 )
				{
					break;
				}

				len = common_prefix_length(v, i, lits[otoc[j].closelit]);

				if ( len != 0 )
				{
					break;
				}
			}

			if ( len != 0 )
			{
				// Found an open or close terminal sequence
				if ( i != last )
				{
					split.emplace_back(v.begin() + last, v.begin() + i);
				}

				split.emplace_back(v.begin() + i, v.begin() + i + len);
				i   += len;
				last = i;
			}
			else
			{
				++i;
			}
		}

		if ( last != n )
		{
			split.emplace_back(v.begin() + last, v.begin() + n);
		}

		return split;
	}

	sequence_id add_lit_no_split(character_sequence v)
	{
		const std::size_t n = lits.size();

		std::size_t k = 0;

		while ( k < n && lits[k] != v )
		{
			++k;
		}

		if ( k == n )
		{
			lits.push_back(std::move(v) );
		}

		return add_rule(LITERAL, k);
	}

	static std::size_t common_prefix_length(
		const character_sequence& v,
		std::size_t               i,
		const character_sequence& s
	)
	{
		const std::size_t n = s.size();

		if ( n <= v.size() - i )
		{
			for (std::size_t j = 0; j < n; ++j)
			{
				if ( v[i + j] != s[j] )
				{
					return 0;
				}
			}

			return n;
		}

		return 0;
	}

	/// @todo Support compile time constant arrays ex. one large character_sequence and then views/spans into it
	std::vector< character_sequence > lits;

	std::vector< character_class > classes;

	/// @todo Should be parallel to (or combined with) subs
	/// @todo Put character_class into "classes" member and store ID instead. May have some deduplication
	std::unordered_map< sequence_id, character_class > can_begin_with_map;
};

template< typename Terminal, typename NonTerminal >
class cfgbuilder
	: public details::tibuilder< Terminal >
{
	/// @bug There could be nonterminals mentioned that don't have rules. Should throw.
	static std::vector< NonTerminal > get_sorted_nonterminals(const std::span< const production_rule< Terminal, NonTerminal > >& productions)
	{
		std::vector< NonTerminal > nonterminals;

		// Reserve a member in subs for each non-terminal
		for (const auto& [nt, v] : productions)
		{
			if ( !v.empty() )
			{
				nonterminals.push_back(nt);
			}
		}

		std::sort(nonterminals.begin(), nonterminals.end() );
		nonterminals.erase(std::unique(nonterminals.begin(), nonterminals.end() ), nonterminals.end() );

		nonterminals.shrink_to_fit();

		return nonterminals;
	}
public:
	using nested_pair_list = typename tibuilder< Terminal >::nested_pair_list;

	explicit cfgbuilder(
		std::span< const production_rule< Terminal, NonTerminal > > productions,
		nested_pair_list                                            open_to_close_
	)
		: cfgbuilder(
			productions,
			std::move(open_to_close_),
			get_sorted_nonterminals(productions)
		)
	{
	}

	cfgbuilder(
		const std::span< const production_rule< Terminal, NonTerminal > >& productions,
		nested_pair_list                                                   open_to_close_,
		std::vector< NonTerminal >                                         nonterminals_
	)
		: details::tibuilder< Terminal >(std::move(open_to_close_), nonterminals_.size() ),
		nonterminals(std::move(nonterminals_) )
	{
		std::vector< std::vector< sequence_id > > allrules(nonterminals.size() );

		for (const auto& production : productions)
		{
			append(allrules[getseq(production.start_symbol)], insert(production) );
		}

		this->init_non_terminal_alts(std::move(allrules) );
		this->optimize();
	}

	std::vector< sequence_id > insert(const production_rule< Terminal, NonTerminal >& ri)
	{
		std::vector< sequence_id > added;

		for (const auto& substitution : ri.substitutions)
		{
			added.push_back(insert(substitution) );
		}

		return added;
	}

	/// @todo Reduce cats with a covering set or similar to benefit from rematches
	sequence_id insert(const substitution_string< Terminal, NonTerminal >& substitution)
	{
		const std::size_t n = substitution.size();

		std::vector< sequence_id > terms;

		for (std::size_t i = 0; i < n; ++i)
		{
			terms.push_back(insert(substitution[i]) );
		}

		return this->add_cat(std::move(terms) );
	}

	sequence_id insert(const substitution_term< Terminal, NonTerminal >& t)
	{
		const sequence_id l = insert(t.mi);

		if ( t.exception )
		{
			return this->add_exception(l, insert(*t.exception) );
		}

		return l;
	}

	sequence_id insert(const std::pair< substitution_atom< Terminal, NonTerminal >, details::repeat_info >& mi)
	{
		return this->add_repeat(insert(mi.first), mi.second);
	}

	sequence_id insert(const substitution_atom< Terminal, NonTerminal >& mi)
	{
		switch ( mi.type )
		{
		case details::match_type::LITERAL:
			return this->add_lit(mi.lit);

		case details::match_type::ALLOW_CHARACTERS:
			return this->add_class(mi.allowed);

		case details::match_type::RULE:
			return getseq(mi.id);

		default:
			throw std::logic_error("Empty token");
		}
	}

	sequence_id getseq(NonTerminal r) const
	{
		const auto first = nonterminals.begin();
		const auto last  = nonterminals.end();
		auto       it    = std::lower_bound(first, last, r);

		if ( it != last && *it == r )
		{
			return sequence_id(it - first);
		}

		throw std::runtime_error("Unknown nonterminal");
	}

	std::vector< NonTerminal > nonterminals;
};
}
}
}

#endif // CONTEXT_FREE_GRAMMAR_BUILDERS_H
