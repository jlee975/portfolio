/// @todo ebnf module
#ifndef CONTEXT_FREE_GRAMMAR_H
#define CONTEXT_FREE_GRAMMAR_H

#include "context_free_grammar_builders.h"

namespace jl
{
namespace grammar
{

/// @todo Should accept seq - seq instead of token - token
/// @todo Slidable/shrinkable should be calculated offline to infinite depth
/// @todo Find and eliminate subsequences (AB)C and A(BC) might not be reduced
/// @todo Reduce things that are effectively A*. Found (A?)A* in a grammar
/// @todo cat a, eta should be reduced
/// @todo Generally, a match should match at least one character so we can limit recursion (ex., recursion
/// depth of 1000 doesn't make any sense on a string only 20 characters long -- clearly aren't advancing the
/// match)
/// @todo EXCEPT should be replaced with INTERSECTION and NEGATION operations
template< typename Terminal, typename NonTerminal >
class context_free_grammar
{
public:
	using sequence_id        = details::sequence_id;
	using character_class    = typename details::tibuilder< Terminal >::character_class;
	using character_sequence = typename details::tibuilder< Terminal >::character_sequence;
	using nested_pair_list   = typename details::tibuilder< Terminal >::nested_pair_list;

	/// @todo Split character classes and string literals if we can get some
	/// duplication
	/// @bug Production rules with common prefixes should be merged
	/// ex., S -> A; S -> AB should be condensed to S -> AB?
	context_free_grammar(std::initializer_list< production_rule< Terminal, NonTerminal > > productions)
		: context_free_grammar(details::cfgbuilder< Terminal, NonTerminal >( productions, {} ) )
	{
	}

	explicit context_free_grammar(
		const production_set< Terminal, NonTerminal >& productions,
		nested_pair_list                               open_to_close_ = {})
		: context_free_grammar( details::cfgbuilder< Terminal, NonTerminal >( productions, std::move(open_to_close_) ) )
	{
	}

	std::span< const Terminal > open_literal(sequence_id i) const
	{
		return literal(get_oc(i).openlit);
	}

	std::span< const Terminal > close_literal(sequence_id i) const
	{
		return literal(get_oc(i).closelit);
	}

	/// @todo Once totallen is removed from ocinfo, we can simply pass
	/// through get_open_to_close()
	auto get_all_nested() const
	{
		nested_pair_list v;

		for ( const auto [l, r, len] : get_open_to_close() )
		{
			v.emplace_back(lits[l], lits[r]);
		}

		return v;
	}

	bool can_begin_with(
		sequence_id i,
		Terminal    t
	) const
	{
		if ( auto it = can_begin_with_map.find(i);it != can_begin_with_map.end() )
		{
			return it->second.count(t) != 0;
		}

		return false;
	}

	std::span< const Terminal > getliteral(sequence_id i) const
	{
		return literal(assoc(i) );
	}

	const character_class& getcharacterclass(sequence_id i) const
	{
		return classes.at(assoc(i) );
	}

	/// @todo Rename
	std::size_t get_subs_size() const
	{
		return subs.size();
	}

	std::size_t get_min_match_length(sequence_id i) const
	{
		return min_match_length.at(i);
	}

	sequence_id get_nested(sequence_id i) const
	{
		return nested.at(assoc(i) ).cat;
	}

	details::rule_kind getkind(sequence_id i) const
	{
		return subs.at(i).kind;
	}

	std::size_t assoc(sequence_id i) const
	{
		return subs.at(i).A;
	}

	std::span< const sequence_id > getalts(sequence_id i) const
	{
		return alts.at(assoc(i) );
	}

	std::span< const sequence_id > getcat(sequence_id i) const
	{
		return cats.at(assoc(i) );
	}

	const std::pair< sequence_id, sequence_id >& getexception(sequence_id i) const
	{
		return exceptions.at(assoc(i) );
	}

	sequence_id getseq(NonTerminal r) const
	{
		const auto first = nonterminals.begin();
		const auto last  = nonterminals.end();
		auto       it    = std::lower_bound(first, last, r);

		if ( it != last && *it == r )
		{
			return sequence_id(it - first);
		}

		throw std::runtime_error("Unknown nonterminal");
	}

	/// @todo std::optional
	std::optional< NonTerminal > to_nonterminal(sequence_id i) const
	{
		if ( i < nonterminals.size() )
		{
			return nonterminals[i];
		}

		return {};
	}

	void print() const
	{
		for (std::size_t i = 0; i < lits.size(); ++i)
		{
			std::cout << "lits[" << i << "] = " << lits[i] << std::endl;
		}

		for (std::size_t i = 0; i < classes.size(); ++i)
		{
			std::cout << "cc[" << i << "] = " << classes[i] << std::endl;
		}

		for (std::size_t i = 0; i < alts.size(); ++i)
		{
			bool first = true;

			const auto& v = alts[i];
			std::cout << "alts[" << i << "] = ";

			for (const auto j : v)
			{
				if ( !first )
				{
					std::cout << "|";
				}

				first = false;
				std::cout << j;
			}

			std::cout << std::endl;
		}

		for (std::size_t i = 0; i < cats.size(); ++i)
		{
			const auto& v = cats[i];

			std::cout << "cats[" << i << "] = ";

			bool first = true;

			for (const auto j : v)
			{
				if ( !first )
				{
					std::cout << " ";
				}

				first = false;
				std::cout << j;
			}

			std::cout << std::endl;
		}

		for (std::size_t i = 0; i < exceptions.size(); ++i)
		{
			std::cout << "exceptions[" << i << "] = ";
			const auto& p = exceptions[i];
			std::cout << p.first << "-" << p.second << std::endl;
		}

		for (std::size_t i = 0; i < nonterminals.size(); ++i)
		{
			std::cout << "nonterminals[" << i << "] = " << nonterminals[i] << std::endl;
		}

		std::cout << std::endl;

		for (std::size_t i = 0; i < subs.size(); ++i)
		{
			const auto& si = subs[i];

			std::cout << "subs[" << i << "] = ";

			switch ( si.kind )
			{
			case details::LITERAL:
				std::cout << "'" << lits[si.A] << "'";
				break;
			case details::CHARACTER_CLASS:
				std::cout << "[" << classes[si.A] << "'";
				break;
			case details::ALT:
			{
				bool first = true;

				for (const auto j : alts[si.A])
				{
					if ( !first )
					{
						std::cout << "|";
					}

					first = false;
					std::cout << j;
				}
			}
			break;
			case details::CAT:
			{
				bool first = true;

				for (const auto j : cats[si.A])
				{
					if ( !first )
					{
						std::cout << " ";
					}

					first = false;
					std::cout << j;
				}
			}
			break;
			case details::EXCEPT:
			{
				std::cout << exceptions[si.A].first << "-" << exceptions[si.A].second;
			}
			break;
			case details::NESTED:
				std::cout << nested[si.A].ocindex << "-" << nested[si.A].cat;
				break;
			} // switch

			std::cout << std::endl;
		}
	}
private:
	// Indicate that l and r always appear together in a rule, so that they form nested pairs (ex.,
	// like (), {}, []
	/// @todo Auto-detect nested
	explicit context_free_grammar(details::cfgbuilder< Terminal, NonTerminal >&& b)
		: open_to_close(std::move(b.open_to_close) ), alts(std::move(b.alts) ), cats(std::move(b.cats) ),
		exceptions(std::move(b.exceptions) ), nested(std::move(b.nested) ), subs(std::move(b.subs) ),
		min_match_length(std::move(b.min_match_length) ), lits(std::move(b.lits) ),
		classes(std::move(b.classes) ), can_begin_with_map(std::move(b.can_begin_with_map) ),
		nonterminals(std::move(b.nonterminals) )
	{
	}

	std::span< const details::oc_info > get_open_to_close() const
	{
		return open_to_close;
	}

	const details::oc_info& get_oc(sequence_id i) const
	{
		return open_to_close.at(nested.at(assoc(i) ).ocindex);
	}

	std::span< const Terminal > literal(std::size_t i) const
	{
		return lits.at(i);
	}

	#if 0
	/* A rule i is shrinkable if there are strings matching i that can be
	 * truncated and still match i. A trivial example: 0+ is shrinkable
	 * because 0000 could be shortened to 000 and still match.
	 * On the other hand '"'[^"]'"' cannot be shrunk. The closing quote
	 * could not appear any earlier
	 *
	 * If the code cannot prove a rule is shrinkable, it will default to "yes"
	 */
	std::vector< bool > shrinkable;

	/* An AB rule is "slidable" if A could be shrunk and B matched at the
	 * end of the smaller matching. This is useful for failed matchings
	 * where backtracking would necessarily fail.
	 *
	 * Ex., '<' NAME (S+ NAME EQ VALUE)* S '/>' is an xml rule. Failure to
	 * match '/>' means we need to scrap the whole match from the beginning.
	 * It is not possible for '/>' to match any (already matched) text.
	 *
	 * If the code cannot prove a rule is slidable, it will default to "yes"
	 */
	std::vector< bool > slidable;
	#endif

	/// @todo flat_map
	const std::vector< details::oc_info > open_to_close;

	const std::vector< std::vector< sequence_id > >            alts;
	const std::vector< std::vector< sequence_id > >            cats;
	const std::vector< std::pair< sequence_id, sequence_id > > exceptions;

	// (index into nested_pairs, inner sequence)
	const std::vector< details::nested_info > nested;

	const std::vector< details::rule > subs;

	/// @todo Should be parallel to (or combined with) subs
	/// @todo Could reorder sequences so that those with a min length of zero are at the tail of
	/// this map. Then we only need to actually store the non-zero min lengths. Consequently,
	/// can_begin_with might be shortened (atm, it is only called if min_length != 0)
	const std::unordered_map< sequence_id, std::size_t > min_match_length;

	// trie?
	/// @todo Support compile time constant arrays ex. one large character_sequence and then views/spans into it
	const std::vector< character_sequence > lits;

	const std::vector< character_class > classes;

	/// @todo Should be parallel to (or combined with) subs
	/// @todo Put character_class into "classes" member and store ID instead. May have some deduplication
	/** @todo This is trivial for a) literals and b) character classes. Ideally, we could save some
	 *  storage by arranging sequence ids so that those occur in a range and can be handled
	 *  specially.
	 */
	const std::unordered_map< sequence_id, character_class > can_begin_with_map;

	const std::vector< NonTerminal > nonterminals;
};
}
}

#endif // CONTEXT_FREE_GRAMMAR_H
