#include "context_free_grammar_builders.h"

namespace jl::grammar::details
{
cibuilder::cibuilder(std::size_t n)
{
	number_of_nonterminals = n;

	/* Reserve rules for each nonterminal. The expectation is that each
	 * nonterminal will have one or more rules associated with it, so we
	 * prep an invalid alt rule. This array has to be initialized before
	 * any calls to insert
	 */
	subs.assign(number_of_nonterminals, rule{ NONTERMINAL, std::size_t(-1) });
}

void cibuilder::init_non_terminal_alts(std::vector< std::vector< sequence_id > > allrules)
{
	for (std::size_t i = 0, n = allrules.size(); i < n; ++i)
	{
		subs[i].A = add_alt(std::move(allrules[i]) );
	}
}

void cibuilder::record_nested_pair(
	sequence_id o,
	sequence_id c,
	std::size_t len
)
{
	open_to_close.emplace_back(oc_info{ subs[o].A, subs[c].A, len });
}

sequence_id cibuilder::add_rule(
	rule_kind   k,
	std::size_t a
)
{
	const rule r = { k, a };

	const std::size_t n = subs.size();

	for (std::size_t i = number_of_nonterminals; i < n; ++i)
	{
		if ( subs[i] == r )
		{
			return sequence_id(i);
		}
	}

	subs.push_back(r);

	return sequence_id(n);
}

sequence_id cibuilder::add_alt(std::vector< sequence_id > v)
{
	if ( v.empty() )
	{
		throw std::logic_error("Should never be called with an empty list");
	}

	if ( v.size() == 1 )
	{
		return v[0];
	}

	std::size_t i = 0;

	std::sort(v.begin(), v.end() );
	v.erase(std::unique(v.begin(), v.end() ), v.end() );

	const std::size_t n = alts.size();

	while ( i < n && alts[i] != v )
	{
		++i;
	}

	if ( i == n )
	{
		alts.push_back(std::move(v) );
	}

	return add_rule(ALT, i);
}

sequence_id cibuilder::add_repeat(
	sequence_id        l,
	const repeat_info& repeat
)
{
	if ( repeat.at_least == 1 && repeat.at_most == 1 )
	{
		// Return rule as is
		return l;
	}

	if ( repeat.at_least == 0 && repeat.at_most == 1 )
	{
		// A?
		return add_alt({ l, add_eta() });
	}

	if ( repeat.at_least == 1 && repeat.at_most == std::size_t(-1) )
	{
		return add_kleene_plus(l);
	}
	else if ( repeat.at_least == 0 && repeat.at_most == std::size_t(-1) )
	{
		return add_kleene_star(l);
	}
	else
	{
		throw std::logic_error("Need to handle generic repeating");
	}
}

sequence_id cibuilder::add_eta()
{
	return add_cat_no_nesting({});
}

sequence_id cibuilder::add_sequence_between_nested(
	std::size_t                open,
	std::vector< sequence_id > subseq
)
{
	// Add the inner sequence between the nested literals as a rule
	const auto seq = add_cat_no_nesting(std::move(subseq) );

	// Associate the inner sequence and pair index with a nested rule
	std::size_t k = 0;

	{
		const std::size_t m = nested.size();

		while ( k < m && ( nested[k].ocindex != open || nested[k].cat != seq ) )
		{
			++k;
		}

		if ( k == m )
		{
			nested.emplace_back(nested_info{ open, seq });
		}
	}

	return add_rule(NESTED, k);
}

/// @todo Merge the non-nested segments so they can be rematched quickly
sequence_id cibuilder::add_cat(std::vector< sequence_id > terms1)
{
	/// @todo If we're able to, move terms1 (i.e., if terms2 would simply be the
	/// exact same thing)
	std::vector< sequence_id > terms2;

	// (position of opening literal, index into open_to_close)
	std::vector< std::pair< std::size_t, std::size_t > > open;

	for (std::size_t i = 0; i < terms1.size(); ++i)
	{
		if ( const sequence_id j = terms1[i];subs[j].kind != LITERAL )
		{
			// Cannot be part of a nested pair
			terms2.push_back(j);
		}
		else if ( !open.empty() && subs[j].A == open_to_close[open.back().second].closelit )
		{
			// Found the closing nested literal to the currently open literal

			const auto j2 = add_sequence_between_nested(open.back().second, { terms2.begin() + open.back().first, terms2.end() });

			// Fix up "terms"
			terms2.resize(open.back().first);
			terms2.push_back(j2);

			// Drop the open pair
			open.pop_back();
		}
		else
		{
			std::size_t       k = 0;
			const std::size_t m = open_to_close.size();

			while ( k < m && open_to_close[k].openlit != subs[j].A )
			{
				++k;
			}

			if ( k == m )
			{
				// Not an opening
				terms2.push_back(j);
			}
			else
			{
				// Is an opening
				open.emplace_back(terms2.size(), k);
			}
		}
	}

	/// @todo Some false negatives. Could do some work to recognize more cases
	if ( !open.empty() )
	{
		throw std::runtime_error("Rules do not respect nested pairs");
	}

	return add_cat_no_nesting(std::move(terms2) );
}

sequence_id cibuilder::add_kleene(
	sequence_id l,
	sequence_id e
)
{
	const sequence_id cat  = subs.size();
	const sequence_id self = cat + 1; // self
	const std::size_t n    = cats.size();

	cats.push_back({ l, self });
	const std::size_t m = alts.size();

	alts.push_back({ e, cat });
	subs.push_back({ CAT, n });
	subs.push_back({ ALT, m });
	return self;
}

sequence_id cibuilder::add_kleene_star(sequence_id l)
{
	// A* is equivalent to adding { { eta }, { l, self } }
	return add_kleene(l, add_eta() );
}

sequence_id cibuilder::add_kleene_plus(sequence_id l)
{
	// A+ is equivalent to adding { { l }, { l, self } }
	return add_kleene(l, l);
}

sequence_id cibuilder::add_exception(
	sequence_id l,
	sequence_id l2
)
{
	const std::pair< sequence_id, sequence_id > p(l, l2);
	const std::size_t                           n = exceptions.size();

	std::size_t i = 0;

	while ( i < n && exceptions[i] != p )
	{
		++i;
	}

	if ( i == n )
	{
		exceptions.push_back(p);
	}

	return add_rule(EXCEPT, i);
}

void cibuilder::build_min_match_length()
{
	// Loop through, determining any rules if we can
	while ( true )
	{
		/// @todo Some kind of wrapper to detect the change, rather than copying the whole thing
		const auto orig = min_match_length;

		for (std::size_t i_ = 0; i_ < subs.size(); ++i_)
		{
			const sequence_id i(i_);

			switch ( subs[i].kind )
			{
			case CHARACTER_CLASS:
				min_match_length[i] = 1;
				break;
			case ALT:
				// Min of an alt is the min of the mins
			{
				bool one_unknown = false;

				std::size_t min = std::size_t(-1);

				for ( const auto& j : alts.at(subs[i].A) )
				{
					if ( auto it = min_match_length.find(j);it != min_match_length.end() )
					{
						if ( it->second < min )
						{
							min = it->second;
						}
					}
					else
					{
						std::unordered_set< sequence_id > seen = { j };

						if ( !rule_requires(j, i, seen) )
						{
							one_unknown = true;
							break;
						}
					}
				}

				if ( min == 0 || !one_unknown )
				{
					// None unknown, none could be empty
					min_match_length[i] = min;
				}

				break;
			}
			case CAT:
				// Min of an alt term is the sum of the terms
			{
				std::size_t sum = 0;

				for ( const auto j : cats.at(subs[i].A) )
				{
					if ( auto it = min_match_length.find(j);it != min_match_length.end() )
					{
						sum += it->second;
					}
				}

				min_match_length[i] = sum;
			}
			break;
			case EXCEPT:
			{
				const auto [s, e] = exceptions[subs[i].A];

				if ( auto it = min_match_length.find(s);it != min_match_length.end() )
				{
					min_match_length[i] = it->second;
				}
			}
			break;
			case NONTERMINAL:

				if ( auto it = min_match_length.find(sequence_id{ subs[i].A });
				     it != min_match_length.end()
				)
				{
					min_match_length[i] = it->second;
				}

				break;
			case NESTED:
			{
				std::size_t sum = 0;

				sum += open_to_close.at(nested.at(subs[i].A).ocindex).totallen;

				if ( auto it = min_match_length.find(nested.at(subs[i].A).cat);
				     it != min_match_length.end()
				)
				{
					sum += it->second;
				}

				min_match_length[i] = sum;
			}
			break;
			} // switch

		}

		if ( min_match_length == orig )
		{
			break;
		}
	}

	for (std::size_t i = 0; i < subs.size(); ++i)
	{
		if ( min_match_length.find(i) == min_match_length.end() )
		{
			throw std::logic_error("Min string length should be known for all rules");
		}
	}
}

void cibuilder::set_literal_length(
	sequence_id i,
	std::size_t len
)
{
	min_match_length[i] = len;
}

/// @todo If v.size() == 1, shouldn't we skip this?
/// @todo If v.size() == 0, change to be a literal of 0 length
sequence_id cibuilder::add_cat_no_nesting(std::vector< sequence_id > v)
{
	if ( v.size() == 1 )
	{
		return v[0];
	}

	const std::size_t n = cats.size();

	std::size_t i = 0;

	while ( i < n && cats[i] != v )
	{
		++i;
	}

	if ( i == n )
	{
		cats.push_back(std::move(v) );
	}

	return add_rule(CAT, i);
}

bool cibuilder::rule_requires(
	sequence_id                        i,
	sequence_id                        k,
	std::unordered_set< sequence_id >& seen
) const
{
	if ( i == k )
	{
		return true;
	}

	if ( seen.count(i) != 0 )
	{
		return false;
	}

	seen.insert(i);

	bool ret = false;

	switch ( subs[i].kind )
	{
	case LITERAL:
	case CHARACTER_CLASS:
		break;
	case ALT:
		ret = true;

		for (const auto& j : alts[subs[i].A])
		{
			if ( !rule_requires(j, k, seen) )
			{
				ret = false;
				break;
			}
		}

		break;
	case CAT:

		// Min of an alt term is the sum of the terms
		for (const auto& j : cats[subs[i].A])
		{
			if ( rule_requires(j, k, seen) )
			{
				ret = true;
				break;
			}
		}

		break;
	case EXCEPT:
	{
		const auto j = exceptions[subs[i].A].first;
		ret = rule_requires(j, k, seen);
		break;
	}
	case NONTERMINAL:
	{
		const sequence_id j{ subs[i].A };
		ret = rule_requires(j, k, seen);
		break;
	}
	case NESTED:
		ret = rule_requires(nested.at(subs[i].A).cat, k, seen)
		      || ( subs[k].kind == LITERAL
		           && ( subs[k].A == open_to_close.at(nested.at(subs[i].A).ocindex).openlit
		                || subs[k].A == open_to_close.at(nested.at(subs[i].A).ocindex).closelit ) );
		break;
	} // switch

	seen.erase(i);

	return ret;
}

}
#if 0
void context_free_grammar::simplify()
{
	std::vector< bool > changed(subs.size(), true);

	while ( true )
	{
		bool cont = false;

		for (std::size_t i = 0; i < changed.size(); ++i)
		{

			if ( changed[i] )
			{
				cont = true;
				break;
			}
		}

		if ( !cont )
		{
			break;
		}

		for (std::size_t i = 0, n = subs.size(); i < n; ++i)
		{
			if ( !changed[i] )
			{
				continue;
			}

			bool        rem = false;
			std::size_t rep = 0;

			if ( !rem && subs[i].kind == AB && subs[i].B == 0 )
			{
				// Simply passes through to A
				rem = true;
				rep = subs[i].A;
			}

			if ( !rem )
			{
				// Look for a duplicate
				for (std::size_t j = 0; j < n; ++j)
				{
					if ( j != i && compare(subs[j], subs[i]) )
					{
						rem = true;
						rep = j;
						break;
					}
				}
			}

			if ( rem )
			{
				// replace all references to i with rep
				for (std::size_t j = 0; j < subs.size(); ++j)
				{
					const rule_kind k = subs[j].kind;

					if ( k == AB || k == ALT || k == EXCEPT || k == KLEENE_STAR )
					{
						if ( subs[j].A == i )
						{
							subs[j].A  = rep;
							changed[j] = true;
						}

						if ( k != KLEENE_STAR && subs[j].B == i )
						{
							subs[j].B  = rep;
							changed[j] = true;
						}
					}
				}

				for (auto it = g.begin(); it != g.end(); ++it)
				{
					if ( it->second == i )
					{
						it->second = rep;
					}
				}

				// Move subs.back() to subs[i] and update all references
				if ( i != n - 1 )
				{
					subs[i]    = subs[n - 1];
					changed[i] = true;

					for (std::size_t j = 0; j < subs.size(); ++j)
					{
						const rule_kind k = subs[j].kind;

						if ( k == AB || k == ALT || k == EXCEPT || k == KLEENE_STAR )
						{
							if ( subs[j].A == n - 1 )
							{
								subs[j].A  = i;
								changed[j] = true;
							}

							if ( k != KLEENE_STAR && subs[j].B == n - 1 )
							{
								subs[j].B  = i;
								changed[j] = true;
							}
						}
					}

					for (auto it = g.begin(); it != g.end(); ++it)
					{
						if ( it->second == n - 1 )
						{
							it->second = i;
						}
					}
				}

				subs.pop_back();
				changed.pop_back();
				break;
			}
			else
			{
				changed[i] = false;
			}
		}
	}

#endif // if 0
#if 0
std::vector< int > setshrink(subs.size(), 0);
std::vector< int > setslide(subs.size(), 0);
shrinkable.assign(subs.size(), true);
slidable.assign(subs.size(), true);

for (std::size_t i = 0; i < subs.size(); ++i)
{
	update_slide(i, setslide, setshrink, 0);
}

}
#endif

#if 0
void context_free_grammar::update_slide(
	std::size_t         i,
	std::vector< int >& setslide,
	std::vector< int >& setshrink,
	int                 depth
)
{
	if ( setslide[i] == 2 || depth == 1000 )
	{
		// Cycle detected. Give up and just call id slidable
		slidable[i] = true;
		setslide[i] = true;
		return;
	}

	if ( setslide[i] == 1 )
	{
		return;
	}

	setslide[i] = 2;

	if ( subs[i].kind == AB )
	{
		slidable[i] = can_slide(subs[i].A, subs[i].B, setslide, setshrink, depth + 1);
	}
	else
	{
		slidable[i] = false;
	}

	setslide[i] = true;
}

void context_free_grammar::update_shrink(
	std::size_t         i,
	std::vector< int >& setslide,
	std::vector< int >& setshrink,
	int                 depth
)
{
	if ( setshrink[i] == 2 || depth == 1000 )
	{
		// Cycle detected. Give up and assume shrinkable
		shrinkable[i] = true;
		setshrink[i]  = true;
		return;
	}

	if ( setshrink[i] == 1 )
	{
		return;
	}

	setshrink[i] = 2;

	switch ( subs[i].kind )
	{
	case ETA:
	case LITERAL:
	case CHARACTER_CLASS:
		shrinkable[i] = false;
		break;
	case ALT:

		if ( eta_production(subs[i].A) || eta_production(subs[i].B) )
		{
			shrinkable[i] = true;
		}
		else
		{
			update_shrink(subs[i].A, setslide, setshrink, depth + 1);

			if ( shrinkable[subs[i].A] )
			{
				shrinkable[i] = true;
			}
			else
			{
				update_shrink(subs[i].B, setslide, setshrink, depth + 1);
				shrinkable[i] = shrinkable[subs[i].B];
			}
		}

		break;
	case AB:
		update_shrink(subs[i].B, setslide, setshrink, depth + 1);

		if ( shrinkable[subs[i].B] )
		{
			shrinkable[i] = true;
		}
		else
		{
			update_slide(i, setslide, setshrink, depth + 1);
			shrinkable[i] = slidable[i];
		}

		break;
	case KLEENE_STAR:
		shrinkable[i] = true;
		break;
	case EXCEPT:
		update_shrink(subs[i].A, setslide, setshrink, depth + 1);
		shrinkable[i] = shrinkable[subs[i].A];
		break;
	} // switch

	setshrink[i] = 0;
}

// Supposing we already have a match for a,b. Can we shrink a to rematch b?
bool context_free_grammar::can_slide(
	std::size_t         x,
	std::size_t         y,
	std::vector< int >& setslide,
	std::vector< int >& setshrink,
	int                 depth
)
{
	if ( depth == 1000 )
	{
		return true;
	}

	update_shrink(x, setslide, setshrink, depth + 1);

	if ( !shrinkable[x] )
	{
		return false;
	}

	switch ( subs[x].kind )
	{
	case ETA:
		return false;

	case LITERAL:
	case CHARACTER_CLASS:
		return can_match(x, y, depth + 1);

	case ALT:
		return can_slide(subs[x].A, y, setslide, setshrink, depth + 1)
		       || can_slide(subs[x].B, y, setslide, setshrink, depth + 1);

	case AB:
		return can_slide(subs[x].B, y, setslide, setshrink, depth + 1)
		       || can_slide(subs[x].A, subs[x].B, setslide, setshrink, depth + 1);

	case KLEENE_STAR:
		return can_slide(subs[x].A, y, setslide, setshrink, depth + 1)
		       || can_match(subs[x].A, y, depth + 1);

	case EXCEPT:
		return can_slide(subs[x].A, y, setslide, setshrink, depth + 1);
	} // switch

	throw std::logic_error("Unreachable");
}

bool context_free_grammar::can_match(
	std::size_t a,
	std::size_t b,
	int         depth
) const
{
	if ( depth == 1000 )
	{
		return true;
	}

	const match_item l = { a, nullptr, 0 };
	const match_item r = { b, nullptr, 0 };

	return can_match(&l, &r, depth + 1);
}

// Is it possible for some string to matchint A to also match B
/// @todo The more cases, the better
bool context_free_grammar::can_match(
	const match_item* l,
	const match_item* r,
	int               depth
) const
{
	if ( depth == 1000 )
	{
		return true;
	}

	if ( !l || !r )
	{
		return true;
	}

	const std::size_t a = l->idx;
	const std::size_t b = r->idx;

	if ( a == b )
	{
		// shift
		return can_match(l->next, r->next, depth + 1);
	}

	switch ( subs[b].kind )
	{
	case ETA:
		return can_match(l, r->next, depth + 1);

	case ALT:
	{
		const match_item r0 = { subs[b].A, r->next, 0 };
		const match_item r1 = { subs[b].B, r->next, 0 };

		return can_match(l, &r0, depth + 1) || can_match(l, &r1, depth + 1);
	}
	case AB:
	{
		const match_item r1 = { subs[b].B, r->next, 0 };
		const match_item r0 = { subs[b].A, &r1, 0 };

		return can_match(l, &r0, depth + 1);
	}
	case KLEENE_STAR:
	{
		const match_item r0 = { subs[b].A, r->next, 0 };

		return can_match(l, &r0, depth + 1) || can_match(l, r->next, depth + 1);
	}
	case EXCEPT:
	{
		const match_item r0 = { subs[b].A, r->next, 0 };

		return can_match(l, &r0, depth + 1);
	}
	} // switch

	switch ( subs[a].kind )
	{
	case ETA:
		return can_match(l->next, r, depth + 1);

	case ALT:
	{
		const match_item l0 = { subs[a].A, l->next, 0 };
		const match_item l1 = { subs[a].B, l->next, 0 };

		return can_match(&l0, r, depth + 1) || can_match(&l1, r, depth + 1);
	}
	case AB:
	{
		const match_item l1 = { subs[a].B, l->next, 0 };
		const match_item l0 = { subs[a].A, &l1, 0 };

		return can_match(&l0, r, depth + 1);
	}
	case KLEENE_STAR:
	{
		const match_item l0 = { subs[a].A, l->next, 0 };

		return can_match(&l0, r, depth + 1) || can_match(l->next, r, depth + 1);
	}
	case EXCEPT:
	{
		const match_item l0 = { subs[a].A, l->next, 0 };

		return can_match(&l0, r, depth + 1);
	}
	} // switch

	if ( subs[a].kind == LITERAL )
	{
		const ustring&    s1 = lits[subs[a].A];
		const std::size_t n1 = s1.length();

		if ( l->off == n1 )
		{
			return can_match(l->next, r, depth + 1);
		}

		if ( subs[b].kind == LITERAL )
		{
			const ustring& s2 = lits[subs[b].A];

			const std::size_t n2 = s2.length();

			if ( r->off == n2 )
			{
				return can_match(l, r->next, depth + 1);
			}

			std::size_t i = l->off;
			std::size_t j = r->off;

			for (; i < n1 && j < n2; ++i, ++j)
			{
				if ( s1[i] != s2[j] )
				{
					return false;
				}
			}

			if ( i == n1 )
			{
				if ( j == n2 )
				{
					return can_match(l->next, r->next, depth + 1);
				}

				const match_item r0 = { r->idx, r->next, j };

				return can_match(l->next, &r0, depth + 1);
			}
			else
			{
				const match_item l0 = { l->idx, l->next, i };

				return can_match(&l0, r->next, depth + 1);
			}
		}
		else
		{
			const character_class& c = classes[subs[b].A];

			if ( c.count(s1[l->off]) != 0 )
			{
				if ( l->off + 1 < n1 )
				{
					const match_item l0 = { l->idx, l->next, l->off + 1 };
					return can_match(&l0, r->next, depth + 1);
				}
				else
				{
					return can_match(l->next, r->next, depth + 1);
				}
			}

			return false;
		}
	}
	else
	{
		const character_class& c = classes[subs[a].A];

		if ( subs[b].kind == LITERAL )
		{
			const ustring& s2 = lits[subs[b].A];

			const std::size_t n2 = s2.length();

			if ( r->off == n2 )
			{
				return can_match(l, r->next, depth + 1);
			}

			if ( c.count(s2[r->off]) != 0 )
			{
				if ( r->off + 1 < n2 )
				{
					const match_item r0 = { r->idx, r->next, r->off + 1 };
					return can_match(l->next, &r0, depth + 1);
				}
				else
				{
					return can_match(l->next, r->next, depth + 1);
				}
			}
		}
		else
		{
			if ( intersect(classes[subs[a].A], classes[subs[b].A]) )
			{
				return can_match(l->next, r->next, depth + 1);
			}
		}

		return false;
	}

	return true;
}

#endif // if 0

#if 0
bool context_free_grammar::is_shrinkable(std::size_t i) const
{
	return shrinkable[i];
}

bool context_free_grammar::is_slidable(std::size_t i) const
{
	return slidable[i];
}

#endif
