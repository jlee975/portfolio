#include "context_free_grammar.h"

#include <algorithm>
#include <iostream>

#if 0
void context_free_grammar::simplify()
{
	std::vector< bool > changed(subs.size(), true);

	while ( true )
	{
		bool cont = false;

		for (std::size_t i = 0; i < changed.size(); ++i)
		{

			if ( changed[i] )
			{
				cont = true;
				break;
			}
		}

		if ( !cont )
		{
			break;
		}

		for (std::size_t i = 0, n = subs.size(); i < n; ++i)
		{
			if ( !changed[i] )
			{
				continue;
			}

			bool        rem = false;
			std::size_t rep = 0;

			if ( !rem && subs[i].kind == AB && subs[i].B == 0 )
			{
				// Simply passes through to A
				rem = true;
				rep = subs[i].A;
			}

			if ( !rem )
			{
				// Look for a duplicate
				for (std::size_t j = 0; j < n; ++j)
				{
					if ( j != i && compare(subs[j], subs[i]) )
					{
						rem = true;
						rep = j;
						break;
					}
				}
			}

			if ( rem )
			{
				// replace all references to i with rep
				for (std::size_t j = 0; j < subs.size(); ++j)
				{
					const rule_kind k = subs[j].kind;

					if ( k == AB || k == ALT || k == EXCEPT || k == KLEENE_STAR )
					{
						if ( subs[j].A == i )
						{
							subs[j].A  = rep;
							changed[j] = true;
						}

						if ( k != KLEENE_STAR && subs[j].B == i )
						{
							subs[j].B  = rep;
							changed[j] = true;
						}
					}
				}

				for (auto it = g.begin(); it != g.end(); ++it)
				{
					if ( it->second == i )
					{
						it->second = rep;
					}
				}

				// Move subs.back() to subs[i] and update all references
				if ( i != n - 1 )
				{
					subs[i]    = subs[n - 1];
					changed[i] = true;

					for (std::size_t j = 0; j < subs.size(); ++j)
					{
						const rule_kind k = subs[j].kind;

						if ( k == AB || k == ALT || k == EXCEPT || k == KLEENE_STAR )
						{
							if ( subs[j].A == n - 1 )
							{
								subs[j].A  = i;
								changed[j] = true;
							}

							if ( k != KLEENE_STAR && subs[j].B == n - 1 )
							{
								subs[j].B  = i;
								changed[j] = true;
							}
						}
					}

					for (auto it = g.begin(); it != g.end(); ++it)
					{
						if ( it->second == n - 1 )
						{
							it->second = i;
						}
					}
				}

				subs.pop_back();
				changed.pop_back();
				break;
			}
			else
			{
				changed[i] = false;
			}
		}
	}

#endif // if 0
#if 0
std::vector< int > setshrink(subs.size(), 0);
std::vector< int > setslide(subs.size(), 0);
shrinkable.assign(subs.size(), true);
slidable.assign(subs.size(), true);

for (std::size_t i = 0; i < subs.size(); ++i)
{
	update_slide(i, setslide, setshrink, 0);
}

}
#endif

#if 0
void context_free_grammar::update_slide(
	std::size_t         i,
	std::vector< int >& setslide,
	std::vector< int >& setshrink,
	int                 depth
)
{
	if ( setslide[i] == 2 || depth == 1000 )
	{
		// Cycle detected. Give up and just call id slidable
		slidable[i] = true;
		setslide[i] = true;
		return;
	}

	if ( setslide[i] == 1 )
	{
		return;
	}

	setslide[i] = 2;

	if ( subs[i].kind == AB )
	{
		slidable[i] = can_slide(subs[i].A, subs[i].B, setslide, setshrink, depth + 1);
	}
	else
	{
		slidable[i] = false;
	}

	setslide[i] = true;
}

void context_free_grammar::update_shrink(
	std::size_t         i,
	std::vector< int >& setslide,
	std::vector< int >& setshrink,
	int                 depth
)
{
	if ( setshrink[i] == 2 || depth == 1000 )
	{
		// Cycle detected. Give up and assume shrinkable
		shrinkable[i] = true;
		setshrink[i]  = true;
		return;
	}

	if ( setshrink[i] == 1 )
	{
		return;
	}

	setshrink[i] = 2;

	switch ( subs[i].kind )
	{
	case ETA:
	case LITERAL:
	case CHARACTER_CLASS:
		shrinkable[i] = false;
		break;
	case ALT:

		if ( eta_production(subs[i].A) || eta_production(subs[i].B) )
		{
			shrinkable[i] = true;
		}
		else
		{
			update_shrink(subs[i].A, setslide, setshrink, depth + 1);

			if ( shrinkable[subs[i].A] )
			{
				shrinkable[i] = true;
			}
			else
			{
				update_shrink(subs[i].B, setslide, setshrink, depth + 1);
				shrinkable[i] = shrinkable[subs[i].B];
			}
		}

		break;
	case AB:
		update_shrink(subs[i].B, setslide, setshrink, depth + 1);

		if ( shrinkable[subs[i].B] )
		{
			shrinkable[i] = true;
		}
		else
		{
			update_slide(i, setslide, setshrink, depth + 1);
			shrinkable[i] = slidable[i];
		}

		break;
	case KLEENE_STAR:
		shrinkable[i] = true;
		break;
	case EXCEPT:
		update_shrink(subs[i].A, setslide, setshrink, depth + 1);
		shrinkable[i] = shrinkable[subs[i].A];
		break;
	} // switch

	setshrink[i] = 0;
}

// Supposing we already have a match for a,b. Can we shrink a to rematch b?
bool context_free_grammar::can_slide(
	std::size_t         x,
	std::size_t         y,
	std::vector< int >& setslide,
	std::vector< int >& setshrink,
	int                 depth
)
{
	if ( depth == 1000 )
	{
		return true;
	}

	update_shrink(x, setslide, setshrink, depth + 1);

	if ( !shrinkable[x] )
	{
		return false;
	}

	switch ( subs[x].kind )
	{
	case ETA:
		return false;

	case LITERAL:
	case CHARACTER_CLASS:
		return can_match(x, y, depth + 1);

	case ALT:
		return can_slide(subs[x].A, y, setslide, setshrink, depth + 1)
		       || can_slide(subs[x].B, y, setslide, setshrink, depth + 1);

	case AB:
		return can_slide(subs[x].B, y, setslide, setshrink, depth + 1)
		       || can_slide(subs[x].A, subs[x].B, setslide, setshrink, depth + 1);

	case KLEENE_STAR:
		return can_slide(subs[x].A, y, setslide, setshrink, depth + 1)
		       || can_match(subs[x].A, y, depth + 1);

	case EXCEPT:
		return can_slide(subs[x].A, y, setslide, setshrink, depth + 1);
	} // switch

	throw std::logic_error("Unreachable");
}

bool context_free_grammar::can_match(
	std::size_t a,
	std::size_t b,
	int         depth
) const
{
	if ( depth == 1000 )
	{
		return true;
	}

	const match_item l = { a, nullptr, 0 };
	const match_item r = { b, nullptr, 0 };

	return can_match(&l, &r, depth + 1);
}

// Is it possible for some string to matchint A to also match B
/// @todo The more cases, the better
bool context_free_grammar::can_match(
	const match_item* l,
	const match_item* r,
	int               depth
) const
{
	if ( depth == 1000 )
	{
		return true;
	}

	if ( !l || !r )
	{
		return true;
	}

	const std::size_t a = l->idx;
	const std::size_t b = r->idx;

	if ( a == b )
	{
		// shift
		return can_match(l->next, r->next, depth + 1);
	}

	switch ( subs[b].kind )
	{
	case ETA:
		return can_match(l, r->next, depth + 1);

	case ALT:
	{
		const match_item r0 = { subs[b].A, r->next, 0 };
		const match_item r1 = { subs[b].B, r->next, 0 };

		return can_match(l, &r0, depth + 1) || can_match(l, &r1, depth + 1);
	}
	case AB:
	{
		const match_item r1 = { subs[b].B, r->next, 0 };
		const match_item r0 = { subs[b].A, &r1, 0 };

		return can_match(l, &r0, depth + 1);
	}
	case KLEENE_STAR:
	{
		const match_item r0 = { subs[b].A, r->next, 0 };

		return can_match(l, &r0, depth + 1) || can_match(l, r->next, depth + 1);
	}
	case EXCEPT:
	{
		const match_item r0 = { subs[b].A, r->next, 0 };

		return can_match(l, &r0, depth + 1);
	}
	} // switch

	switch ( subs[a].kind )
	{
	case ETA:
		return can_match(l->next, r, depth + 1);

	case ALT:
	{
		const match_item l0 = { subs[a].A, l->next, 0 };
		const match_item l1 = { subs[a].B, l->next, 0 };

		return can_match(&l0, r, depth + 1) || can_match(&l1, r, depth + 1);
	}
	case AB:
	{
		const match_item l1 = { subs[a].B, l->next, 0 };
		const match_item l0 = { subs[a].A, &l1, 0 };

		return can_match(&l0, r, depth + 1);
	}
	case KLEENE_STAR:
	{
		const match_item l0 = { subs[a].A, l->next, 0 };

		return can_match(&l0, r, depth + 1) || can_match(l->next, r, depth + 1);
	}
	case EXCEPT:
	{
		const match_item l0 = { subs[a].A, l->next, 0 };

		return can_match(&l0, r, depth + 1);
	}
	} // switch

	if ( subs[a].kind == LITERAL )
	{
		const ustring&    s1 = lits[subs[a].A];
		const std::size_t n1 = s1.length();

		if ( l->off == n1 )
		{
			return can_match(l->next, r, depth + 1);
		}

		if ( subs[b].kind == LITERAL )
		{
			const ustring& s2 = lits[subs[b].A];

			const std::size_t n2 = s2.length();

			if ( r->off == n2 )
			{
				return can_match(l, r->next, depth + 1);
			}

			std::size_t i = l->off;
			std::size_t j = r->off;

			for (; i < n1 && j < n2; ++i, ++j)
			{
				if ( s1[i] != s2[j] )
				{
					return false;
				}
			}

			if ( i == n1 )
			{
				if ( j == n2 )
				{
					return can_match(l->next, r->next, depth + 1);
				}

				const match_item r0 = { r->idx, r->next, j };

				return can_match(l->next, &r0, depth + 1);
			}
			else
			{
				const match_item l0 = { l->idx, l->next, i };

				return can_match(&l0, r->next, depth + 1);
			}
		}
		else
		{
			const character_class& c = classes[subs[b].A];

			if ( c.count(s1[l->off]) != 0 )
			{
				if ( l->off + 1 < n1 )
				{
					const match_item l0 = { l->idx, l->next, l->off + 1 };
					return can_match(&l0, r->next, depth + 1);
				}
				else
				{
					return can_match(l->next, r->next, depth + 1);
				}
			}

			return false;
		}
	}
	else
	{
		const character_class& c = classes[subs[a].A];

		if ( subs[b].kind == LITERAL )
		{
			const ustring& s2 = lits[subs[b].A];

			const std::size_t n2 = s2.length();

			if ( r->off == n2 )
			{
				return can_match(l, r->next, depth + 1);
			}

			if ( c.count(s2[r->off]) != 0 )
			{
				if ( r->off + 1 < n2 )
				{
					const match_item r0 = { r->idx, r->next, r->off + 1 };
					return can_match(l->next, &r0, depth + 1);
				}
				else
				{
					return can_match(l->next, r->next, depth + 1);
				}
			}
		}
		else
		{
			if ( intersect(classes[subs[a].A], classes[subs[b].A]) )
			{
				return can_match(l->next, r->next, depth + 1);
			}
		}

		return false;
	}

	return true;
}

#endif // if 0

#if 0
bool context_free_grammar::is_shrinkable(std::size_t i) const
{
	return shrinkable[i];
}

bool context_free_grammar::is_slidable(std::size_t i) const
{
	return slidable[i];
}

#endif
