#ifndef REPEAT_INFO_H
#define REPEAT_INFO_H

#include <cstddef>

namespace jl
{
namespace grammar
{
namespace details
{
/** Describes the allowed number of times a term can repeat in a production rule
 */
struct repeat_info
{
	repeat_info() = default;

	/** Construct repeat information from common character abbreviations
	 *
	 *  "?" - Term is optional. Appears 0 or 1 times
	 *  "*" - Term can appear any number of times, zero include
	 *  "+" - Term must appear at least once (no upper bound)
	 *
	 *  If mod is anything else, term appears exactly once
	 */
	constexpr explicit repeat_info(char mod)
		: at_least(mod == '?' || mod == '*' ? 0 : 1), at_most(mod == '*' || mod == '+' ? std::size_t(-1) : 1)
	{
	}

	std::size_t at_least{ 1 };
	std::size_t at_most{ 1 };
};
}
}
}

#endif // REPEAT_INFO_H
