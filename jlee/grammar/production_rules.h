#ifndef PRODUCTION_RULES_H
#define PRODUCTION_RULES_H

#include <optional>
#include <sstream>

#include "unicode/ustring.h"

#include "containers/interval_set.h"

#include "parse_atom_string.h"
#include "repeat_info.h"

namespace jl
{
namespace grammar
{
namespace details
{
enum class match_type
{
	LITERAL,
	ALLOW_CHARACTERS,
	RULE
};

}

template< typename Terminal, typename NonTerminal >
struct substitution_atom
{
	constexpr explicit substitution_atom(Terminal t)
		: type(details::match_type::LITERAL), lit(1, t), id()
	{
	}

	constexpr explicit substitution_atom(NonTerminal id_)
		: type(details::match_type::RULE), id(id_)
	{
	}

	template< typename Sequence >
	constexpr explicit substitution_atom(const Sequence& s_)
	{
		// character sequences can themselves be parsed
		if constexpr ( std::is_same_v< Sequence, ustring >|| std::is_same_v< Sequence, std::string >
		               || std::is_same_v< Sequence, std::string_view >
		)
		{
			const std::size_t len = s_.length();

			if ( len == 0 )
			{
				throw std::runtime_error("Expected token");
			}

			switch ( s_[0] )
			{
			case '\'': // string literal
			case '"':
			{
				type = details::match_type::LITERAL;
				lit  = details::parse_string_literal(s_);
			}
			break;
			case '[': // range
			{
				allowed = details::parse_range(s_);
				type    = details::match_type::ALLOW_CHARACTERS;
			}
			break;
			case '#': // character given by code
			{
				lit.assign(1, details::parse_number(s_) );
				type = details::match_type::LITERAL;
			}
			break;
			default:
			{
				/* ostringstream not allowed in constexpr
				   std::ostringstream oss;
				   oss << "Unrecognized token. Got: " << s_
				   << "\nExpected:\n\t\"literal with quotes\"\n"
				   "\t'literal with single quotes'\n"
				   "\t[A-Za-z1-9] (character class)\n"
				   "\t#32 (code point)\n"
				   "\t#x20 (hex code point)";
				   throw std::runtime_error( oss.str() );
				 */

				throw std::runtime_error("Unrecognized grammar atom");
			}
			} // switch

		}
		else
		{
			// Treat as a literal
			using std::begin;
			using std::end;

			type = details::match_type::LITERAL;
			lit.assign(begin(s_), end(s_) );
		}
	}

	/// @todo variant
	details::match_type type;

	// string to match if LITERAL;
	std::vector< Terminal > lit;

	// sorted list of characters allowed/disallowed if ALLOW_CHARACTERS/DISALLOW_CHARACTERS
	interval_set< Terminal > allowed;

	NonTerminal id;
};

template< typename Terminal, typename NonTerminal >
struct substitution_term
{
	template< typename X >
	constexpr substitution_term(X && x)
		: substitution_term(std::forward< X >(x), '\0')
	{
	}

	template< typename X >
	constexpr substitution_term(X && x, char xmod_)
		: mi(std::forward< X >(x), xmod_), exception()
	{
	}

	template< typename X, typename Y >
	constexpr substitution_term(X && x_, char xmod_, Y && y_, char ymod_)
		: mi(std::forward< X >(x_), xmod_), exception(std::in_place, std::forward< Y >(y_) )
	{
		if ( ymod_ != '-' )
		{
			throw std::runtime_error("Second token must be 'not'");
		}
	}

	std::pair< substitution_atom< Terminal, NonTerminal >, details::repeat_info > mi;

	/// @todo Generally would like a way to represent INTERSECTION and NOT
	std::optional< substitution_atom< Terminal, NonTerminal > > exception;
};

template< typename Terminal, typename NonTerminal >
using substitution_string = std::vector< substitution_term< Terminal, NonTerminal > >;

template< typename Terminal, typename NonTerminal >
struct production_rule
{
	/// The symbol that can be replaced
	NonTerminal start_symbol;

	/// A list of substitutions allowable for the start symbol
	std::vector< substitution_string< Terminal, NonTerminal > > substitutions;
};

template< typename Terminal, typename NonTerminal >
using production_set = std::vector< production_rule< Terminal, NonTerminal > >;

}
}

#endif // PRODUCTION_RULES_H
