/** Parse things like literal strings (ex., "INCLUDINGQUOTES"), character classes (ex., [A-Z]),
 *  and character codes (ex., #x20)
 */
#ifndef PARSE_ATOM_STRING_H
#define PARSE_ATOM_STRING_H

#include <stdexcept>
#include <type_traits>
#include <vector>

namespace jl::grammar::details
{
template< typename Iterator, typename Char = std::remove_cvref_t< decltype( *std::declval< Iterator >() ) > >
std::vector< Char > parse_string_literal(
	Iterator first,
	Iterator last
)
{
	if ( first == last )
	{
		throw std::runtime_error("Expected opening quote");
	}

	const auto c = *first;

	if ( c != '"' && c != '\'' )
	{
		throw std::runtime_error("Invalid format -- expected single or double quote");
	}

	std::vector< Char > t;

	++first;

	while ( first != last )
	{
		// Check for closing quote
		if ( *first == c )
		{
			return t;
		}

		// Check for escape sequence
		if ( *first == '\\' )
		{
			++first;

			if ( first != last && ( *first == '\\' || *first == c ) )
			{
				t.push_back(*first);
				++first;
			}
			else
			{
				throw std::runtime_error("Invalid escape sequence");
			}
		}
		else
		{
			t.push_back(*first);
			++first;
		}
	}

	throw std::runtime_error("Unterminated quoted string");
}

template< typename Sequence >
auto parse_string_literal(const Sequence& seq)
{
	using std::begin;
	using std::end;

	return parse_string_literal(begin(seq), end(seq) );
}

/// @todo Check for overflow
template< typename Iterator, typename Char = std::remove_cvref_t< decltype( *std::declval< Iterator >() ) > >
std::pair< Iterator, Char > parse_number(
	Iterator first,
	Iterator last
)
{
	if ( first == last || *first != '#' )
	{
		throw std::invalid_argument("Expected opening hash-mark");
	}

	Char c = 0;

	++first;

	if ( first != last && *first == 'x' )
	{
		// Hexadecimal notation
		++first;

		const auto it = first;

		while ( first != last )
		{
			if ( '0' <= *first && *first <= '9' )
			{
				c = 16 * c + ( *first - '0' );
			}
			else if ( 'A' <= *first && *first <= 'F' )
			{
				c = 16 * c + ( *first - 'A' + 10 );
			}
			else if ( 'a' <= *first && *first <= 'f' )
			{
				c = 16 * c + ( *first - 'a' + 10 );
			}
			else
			{
				break;
			}

			++first;
		}

		if ( first == it )
		{
			throw std::runtime_error("Expected at least one hexadecimal digit after '#x'");
		}
	}
	else
	{
		// Assume decimal notation

		const auto it = first;

		while ( first != last && '0' <= *first && *first <= '9' )
		{
			c = 10 * c + ( *first - '0' );
			++first;
		}

		if ( first == it )
		{
			throw std::runtime_error("Expected at least one digit after '#'");
		}
	}

	return { first, c };
}

template< typename Sequence >
auto parse_number(const Sequence& seq)
{
	using std::begin;
	using std::end;

	return parse_number(begin(seq), end(seq) ).second;
}

template< typename Iterator, typename Char = std::remove_cvref_t< decltype( *std::declval< Iterator >() ) > >
std::pair< Iterator, Char > parse_range_char(
	Iterator first,
	Iterator last
)
{
	if ( first == last )
	{
		throw std::invalid_argument("Expected character");
	}

	auto c = *first;

	if ( c == '#' )
	{
		// number
		return parse_number(first, last);
	}

	if ( c == '\\' )
	{
		// escaped value
		if ( ++first != last )
		{
			c = *first;

			if ( c != '\\' && c != ']' && c != '^' && c != '-' && c != '#' )
			{
				throw std::runtime_error("Invalid escape sequence");
			}
		}
	}

	// single character
	++first;
	return { first, c };
}

template< typename Iterator, typename Char = std::remove_cvref_t< decltype( *std::declval< Iterator >() ) > >
interval_set< Char > parse_range(
	Iterator first,
	Iterator last
)
{
	interval_set< Char > cl;

	if ( first == last || *first != '[' )
	{
		throw std::runtime_error("Expected opening bracket for range");
	}

	++first;

	bool disallow = false;

	if ( first != last && *first == '^' )
	{
		disallow = true;
		++first;
	}

	while ( first != last )
	{
		if ( *first == ']' )
		{
			++first;

			if ( disallow )
			{
				cl = invert(cl);
			}

			return cl;
		}

		auto res = parse_range_char(first, last);

		first = res.first;

		const auto t = res.second;

		if ( first != last && *first == '-' )
		{
			// range -- expect another character
			++first;
			res   = parse_range_char(first, last);
			first = res.first;
			cl.insert(t, res.second);
		}
		else
		{
			cl.insert(t);
		}
	}

	throw std::runtime_error("Unterminated character class");
}

template< typename Sequence >
auto parse_range(const Sequence& s)
{
	using std::begin;
	using std::end;

	return parse_range(begin(s), end(s) );
}

}

#endif // PARSE_ATOM_STRING_H
