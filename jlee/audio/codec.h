#ifndef CODEC_H
#define CODEC_H

#include "sample_off.h"

namespace audio
{
template< typename >
class samples;
}

class MetaData;
class Packet;

namespace audio
{
/*=========================================================================*//**
*  \class       Codec
*  \brief       Interface class for audio codecs
*//*==========================================================================*/
class Codec
{
public:
	using samples_type = samples< std::int_least16_t >;

	virtual ~Codec() = default;

	virtual void DecodePacket(const Packet&)     = 0;
	virtual samples_type DumpAudio()             = 0;
	virtual void seek()                          = 0;
	virtual bool getMeta(MetaData&) const        = 0;
	virtual sample_rate_type sample_rate() const = 0;
};
}

#endif // CODEC_H
