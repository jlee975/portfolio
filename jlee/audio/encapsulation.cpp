#include "encapsulation.h"

#include "codec.h"
#include "packet.h"

namespace audio
{
Encapsulation& operator>>(
	Encapsulation& e,
	Codec&         c
)
{
	c.DecodePacket(e.ReadPacket() );
	return e;
}

}
