#ifndef ENCAPSULATION_H
#define ENCAPSULATION_H

#include "sample_off.h"

namespace audio
{
class Codec;
}

class Packet;

namespace audio
{
/*=========================================================================*//**
*  \class       Encapsulation
*  \brief       Interface class for audio framing
*//*==========================================================================*/
class Encapsulation
{
public:
	virtual ~Encapsulation() = default;

	virtual void prepare() = 0;

	virtual Packet ReadPacket() = 0;

	virtual sample_off position() const = 0;
	virtual sample_off length() const   = 0;
	virtual void seek(sample_off)       = 0;
	virtual bool eos() const            = 0;
};

Encapsulation& operator>>(Encapsulation&, Codec&);
}

#endif // ENCAPSULATION_H
