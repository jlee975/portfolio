#ifndef SINK_H
#define SINK_H

#include <cstdint>

namespace audio
{
template< typename >
class samples;

/*=========================================================================*//**
*  \class       Sink
*  \brief       Interface class for audio output devices
*//*==========================================================================*/
class Sink
{
public:
	using samples_type = samples< std::int_least16_t >;
	Sink()            = default;
	Sink(const Sink&) = delete;
	Sink(Sink&&)      = delete;

	virtual ~Sink()              = default;
	Sink& operator=(const Sink&) = delete;
	Sink& operator=(Sink&&)      = delete;

	virtual bool open()                     = 0;
	virtual bool isOpen()                   = 0;
	virtual void stop()                     = 0;
	virtual void write(const samples_type&) = 0;
	virtual void pause()                    = 0;
	virtual void resume()                   = 0;
};
}

#endif // SINK_H
