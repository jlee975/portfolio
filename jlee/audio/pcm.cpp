/// \bug if short is not 16-bit..., float is not 32-bit, etc.
#include <cstddef>

class audio
{
public:
	enum Format
	{
		FORMAT_S8,
		FORMAT_S16,
		FORMAT_FLOAT32
	};

	audio(unsigned, Format);
	audio(const audio&);

	~audio();

	audio& operator=(const audio&);
	void swap(audio&);

	std::size_t append(unsigned, std::size_t, float**);
private:
	unsigned    channels;
	std::size_t nframes;
	std::size_t arrsize;
	Format      format;
	void*       p;

	void reserve(std::size_t);
};

#include <algorithm>
#include <iostream>
#include <limits>
#include <stdexcept>

/*=========================================================================*//**
*
*  Internally stores samples in interleaved format.
*
*  Invariant: nframes * channels <= arrsize
*//*==========================================================================*/

/***************************************************************************/ /**
 *******************************************************************************/
audio::audio(
	unsigned ch,
	Format   f
)
	: channels(ch), nframes(0), arrsize(0), format(f), p(nullptr)
{
	if ( channels == 0 )
	{
		throw std::invalid_argument("Cannot create zero-channel audio object");
	}

	std::size_t mframes = std::numeric_limits< std::size_t >::max() / ch;

	if ( 8192 < mframes )
	{
		mframes = 8192;
	}

	arrsize = mframes * channels;

	switch ( format ) // Allocate the internal buffer
	{
	case FORMAT_S8:
		p = new signed char [arrsize];
		break;
	case FORMAT_S16:
		p = new short [arrsize];
		break;
	case FORMAT_FLOAT32:
		p = new float [arrsize];
		break;
	default:
		throw std::invalid_argument("An invalid format was requested");
	}
}

/***************************************************************************/ /**
 *******************************************************************************/
audio::audio(const audio& a)
	: channels(a.channels), nframes(a.nframes), arrsize(0), format(a.format), p(nullptr)
{
	std::size_t n = nframes * channels;

	arrsize = ( n | 8191U ) + 1U;

	if ( arrsize == 0 )
	{
		arrsize = a.arrsize;
	}

	if ( format == FORMAT_S8 )
	{
		auto* c  = new signed char [arrsize];
		auto* ac = static_cast< signed char* >( a.p );
		std::copy(ac, ac + n, c);
		p = c;
	}
	else if ( format == FORMAT_S16 )
	{
		auto* s  = new short [arrsize];
		auto* as = static_cast< short* >( a.p );
		std::copy(as, as + n, s);
		p = s;
	}
	else if ( format == FORMAT_FLOAT32 )
	{
		auto* f  = new float [arrsize];
		auto* af = static_cast< float* >( a.p );
		std::copy(af, af + n, f);
		p = f;
	}
	else
	{
		throw std::runtime_error("Invalid format in copy constructor");
	}
}

/***************************************************************************/ /**
 *******************************************************************************/
audio::~audio()
{
	switch ( format )
	{
	case FORMAT_S8:
		delete[] static_cast< signed char* >( p );
		break;
	case FORMAT_S16:
		delete[] static_cast< short* >( p );
		break;
	case FORMAT_FLOAT32:
		delete[] static_cast< float* >( p );
		break;
	default:
		break;
	}
}

/***************************************************************************/ /**
 *******************************************************************************/
audio& audio::operator=(const audio& a)
{
	if ( &a != this )
	{
		audio(a).swap(*this);
	}

	return *this;
}

/***************************************************************************/ /**
 *******************************************************************************/
void audio::swap(audio& a)
{
	std::swap(channels, a.channels);
	std::swap(nframes, a.nframes);
	std::swap(arrsize, a.arrsize);
	std::swap(format, a.format);
	std::swap(p, a.p);
}

/***************************************************************************/ /**
 *  \fn          std::size_t audio::append(unsigned, std::size_t, float**)
 *  \brief       Appends non-interleaved audio data in buffers
 *  \param       ch Number of channels. Must be at least "channels" large
 *  \returns     Number of samples appended. Could be less than "samples".
 *
 *  This function can return zero if there are null pointers in v, or ch is less
 *  than the number of channels in *this.
 *
 *  The parameter "v" is expected to be an array of "ch" pointers, which each point
 *  to an array of floats at least "samples" in size.
 *
 *  \todo Instead of throwing on out of memory, take what we can and return number
 *     of frames taken
 *******************************************************************************/
std::size_t audio::append(
	unsigned    ch,
	std::size_t nsamples,
	float**     v
)
{
	if ( ( ch < channels ) || ( v == nullptr ) )
	{
		return 0;
	}

	for (unsigned i = 0; i < ch; ++i)
	{
		if ( v[i] == nullptr )
		{
			return 0;
		}
	}

	if ( nsamples >= std::numeric_limits< std::size_t >::max() / channels - nframes )
	{
		throw std::runtime_error("Too many samples");
	}

	reserve( ( nframes + nsamples ) * channels);

	// Copy in the data
	if ( format == FORMAT_FLOAT32 ) // Just a straight copy
	{
		float* f = static_cast< float* >( p ) + ( nframes * channels );

		for (std::size_t i = 0; i < nsamples; ++i)
		{
			for (unsigned j = 0; j < channels; ++j)
			{
				*f++ = v[j][i];
			}
		}

		nframes += nsamples;
	}
	else if ( format == FORMAT_S16 ) // Copy and convert
	{
		short* s = static_cast< short* >( p ) + ( nframes * channels );

		for (std::size_t i = 0; i < nsamples; ++i)
		{
			for (unsigned j = 0; j < channels; ++j)
			{
				float x = v[j][i] * 32767.0f;

				if ( x >= 32767.0f )
				{
					x = 32767.0f;
				}

				if ( x <= -32767.0f )
				{
					x = -32767.0f;
				}

				*s++ = static_cast< short >( x );
			}
		}

		nframes += nsamples;
	}
	else if ( format == FORMAT_S8 ) // Copy and convert
	{
		signed char* c = static_cast< signed char* >( p ) + ( nframes * channels );

		for (std::size_t i = 0; i < nsamples; ++i)
		{
			for (unsigned j = 0; j < channels; ++j)
			{
				float x = v[j][i] * 127.0f;

				if ( x >= 127.0f )
				{
					x = 127.0f;
				}

				if ( x <= -127.0f )
				{
					x = -127.0f;
				}

				*c++ = static_cast< signed char >( x );
			}
		}

		nframes += nsamples;
	}
	else
	{
		return 0;
	}

	return nsamples;
}

/***************************************************************************/ /**
 *******************************************************************************/
void audio::reserve(std::size_t n)
{
	if ( arrsize >= n )
	{
		// do nothing
	}
	else if ( format == FORMAT_S8 )
	{
		auto* c  = new signed char [n];
		auto* tc = static_cast< signed char* >( p );
		std::copy(tc, tc + ( nframes * channels ), c);
		p = c;
		delete[] tc;
		arrsize = n;
	}
	else if ( format == FORMAT_S16 )
	{
		auto* s  = new short [n];
		auto* ts = static_cast< short* >( p );
		std::copy(ts, ts + ( nframes * channels ), s);
		p = s;
		delete[] ts;
		arrsize = n;
	}
	else if ( format == FORMAT_FLOAT32 )
	{
		auto* f  = new float [n];
		auto* tf = static_cast< float* >( p );
		std::copy(tf, tf + ( nframes * channels ), f);
		p = f;
		delete[] tf;
		arrsize = n;
	}
}

float  a[] = { 0.2, 0.5 };
float  b[] = { -0.3, 0.6 };
float* c[] = { a, b };

int main()
{
	audio d(2, audio::FORMAT_S8);

	d.append(2, 2, c);
	return 0;
}
