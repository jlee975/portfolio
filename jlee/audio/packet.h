#ifndef PACKET_H
#define PACKET_H

#include <vector>

/// @todo Use std::byte instead of unsigned char.
/// @todo Reserve
/// @todo Resize doesn't need to initialize data. Do new/delete ourselves
class Packet
{
public:
	Packet() = default;

	explicit Packet(std::size_t n)
		: data_(n)
	{
	}

	const unsigned char* data() const
	{
		return data_.data();
	}

	unsigned char* data()
	{
		return data_.data();
	}

	bool empty() const
	{
		return data_.empty();
	}

	std::size_t size() const
	{
		return data_.size();
	}

	void resize(std::size_t n)
	{
		data_.resize(n);
	}

	unsigned char& operator[](std::size_t i)
	{
		return data_[i];
	}

	const unsigned char& operator[](std::size_t i) const
	{
		return data_[i];
	}

	void push_back(unsigned char c)
	{
		data_.push_back(c);
	}
private:
	std::vector< unsigned char > data_;
};

#endif // PACKET_H
