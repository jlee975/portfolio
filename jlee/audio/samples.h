#ifndef SAMPLES_H
#define SAMPLES_H

#include <vector>

namespace audio
{
template< typename S >
class samples
{
public:
	using channel_type = unsigned;
	using value_type   = S;

	samples()
		: samples(0)
	{
	}

	explicit samples(channel_type n)
		: channels(n), samples_()
	{
	}

	samples(const samples&) = default;

	samples& operator=(const samples&) = default;

	void push_back(value_type s)
	{
		samples_.push_back(s);
	}

	void set(std::vector< value_type > v)
	{
		samples_ = std::move(v);
	}

	bool empty() const
	{
		return samples_.empty();
	}

	std::size_t size() const
	{
		return samples_.size();
	}

	const std::vector< value_type >& data() const
	{
		return samples_;
	}
private:
	channel_type              channels;
	std::vector< value_type > samples_; /// \todo Rename
};
}

#endif // SAMPLES_H
