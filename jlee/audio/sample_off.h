#ifndef SAMPLE_OFF_H
#define SAMPLE_OFF_H

#include <chrono>
#include <climits>
#include <cstdint>

namespace audio
{
/*=========================================================================*//**
*  \class       sample_off
*  \brief       An unsigned integer type
*
*  The function toSec() is also provided to convert a sample offest and sample
*  rate to a long.
*
* @todo sample_off should probably be signed
*//*==========================================================================*/
struct sample_off
{
	std::uintmax_t value;
};

constexpr bool operator==(
	const sample_off& l,
	const sample_off& r
)
{
	return l.value == r.value;
}

constexpr bool operator!=(
	const sample_off& l,
	const sample_off& r
)
{
	return l.value != r.value;
}

constexpr bool operator<(
	const sample_off& l,
	const sample_off& r
)
{
	return l.value < r.value;
}

constexpr bool operator<=(
	const sample_off& l,
	const sample_off& r
)
{
	return l.value <= r.value;
}

constexpr bool operator>(
	const sample_off& l,
	const sample_off& r
)
{
	return l.value > r.value;
}

constexpr bool operator>=(
	const sample_off& l,
	const sample_off& r
)
{
	return l.value >= r.value;
}

// samples per second
struct sample_rate_type
{
	sample_off value;
};

constexpr bool operator==(
	const sample_rate_type& l,
	const sample_rate_type& r
)
{
	return l.value == r.value;
}

constexpr bool operator!=(
	const sample_rate_type& l,
	const sample_rate_type& r
)
{
	return l.value != r.value;
}

template< class Rep, class Period >
constexpr sample_off operator*(
	const sample_rate_type&                     rate,
	const std::chrono::duration< Rep, Period >& dur
)
{
	/// @todo Be careful to avoid overflow
	return { rate.value.value * dur.count() * Period::num / Period::den };
}

template< class Rep, class Period >
constexpr sample_off operator*(
	const std::chrono::duration< Rep, Period >& dur,
	const sample_rate_type&                     rate
)
{
	// Simply use commutative property
	return rate * dur;
}

constexpr std::chrono::seconds toSec(
	sample_off       p,
	sample_rate_type sample_rate
)
{
	if ( sample_rate != sample_rate_type{ 0 } )
	{
		/// @todo Define division for these two
		const auto x = p.value / sample_rate.value.value;

		if ( x <= LONG_MAX )
		{
			return std::chrono::seconds{ x };
		}

		return std::chrono::seconds{ LONG_MAX };
	}

	return std::chrono::seconds{ 0 };
}

}
#endif // ifndef SAMPLE_OFF_H
