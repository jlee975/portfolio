#ifndef JLEE_BITS_POPCOUNT_H
#define JLEE_BITS_POPCOUNT_H

#include <cstdint>

// population count
constexpr std::uint32_t popcount(std::uint32_t x)
{
	x -= ( x >> 1 ) & UINT32_C(0x55555555);
	x  = ( x & UINT32_C(0x33333333) ) + ( ( x >> 2 ) & UINT32_C(0x33333333) );
	x  = ( x + ( x >> 4 ) ) & UINT32_C(0x0f0f0f0f);
	x += ( x >> 8 );
	x += ( x >> 16 );

	return x & UINT32_C(0x3f);
}

constexpr std::uint64_t popcount(std::uint64_t x)
{
	x -= ( ( x >> 1 ) & UINT64_C(0x5555555555555555) );
	x  = ( x & UINT64_C(0x3333333333333333) ) + ( ( x >> 2 ) & UINT64_C(0x3333333333333333) );
	x  = ( x + ( x >> 4 ) ) & UINT64_C(0x0F0F0F0F0F0F0F0F);
	x += ( x >> 8 );
	x += ( x >> 16 );
	x += ( x >> 32 );

	return x & UINT64_C(0x7F);
}

#endif // JLEE_BITS_POPCOUNT_H
