#ifndef UABS_H
#define UABS_H

/** @brief Calcalate the absolute value of a signed built-in type
 * @return      |a|
 *
 * The following two functions calculate the absolute value of a long and int. The
 * return value is unsigned long. There are here merely for convenience.
 */
constexpr unsigned long uabs(long a)
{
	return a >= 0 ? static_cast< unsigned long >( a ) : -static_cast< unsigned long >( a );
}

constexpr unsigned uabs(int a)
{
	return a >= 0 ? static_cast< unsigned >( a ) : -static_cast< unsigned >( a );
}

#endif // UABS_H
