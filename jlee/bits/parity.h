#ifndef PARITY_H
#define PARITY_H

constexpr unsigned parity(unsigned char c)
{
	return ( 0x6996 >> ( ( c ^ ( c >> 4 ) ) & 0xF ) ) & 1;
}

#endif // PARITY_H
