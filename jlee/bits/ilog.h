#ifndef JLEE_BITS_ILOG_H
#define JLEE_BITS_ILOG_H

#include "popcount.h"

/***************************************************************************/ /**
 *  Calculates one plus the log base-2 of the input. Returns zero if the input is
 *  less than or equal to zero.
 *******************************************************************************/
constexpr std::uint32_t ilog2p1(std::uint32_t a)
{
	// propogate 1 bit
	std::uint32_t x = a | ( a >> 1 );

	x |= x >> 2;
	x |= x >> 4;
	x |= x >> 8;
	x |= x >> 16;

	return popcount(x);
}

#endif // JLEE_BITS_ILOG_H
