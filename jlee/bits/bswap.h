#ifndef BSWAP_H
#define BSWAP_H

#include <cstdint>

constexpr std::uint_least32_t bswap32(std::uint_least32_t x)
{
	return ( ( x & UINT32_C(0xff000000) ) >> 24 ) | ( ( x & UINT32_C(0xff0000) ) >> 8 ) | ( ( x & UINT32_C(0xff00) ) << 8 )
	       | ( ( x & UINT32_C(0xff) ) << 24 );
}

#endif // BSWAP_H
