#ifndef ROTATE_H
#define ROTATE_H

#include <cstdint>

constexpr unsigned long rol32(
	unsigned long a,
	unsigned      c
)
{
	return ( ( a << c ) | ( ( a & UINT32_C(0xFFFFFFFF) ) >> ( 32 - c ) ) ) & UINT32_C(0xFFFFFFFF);
}

#endif // ROTATE_H
