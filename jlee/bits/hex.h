#ifndef HEX_H
#define HEX_H

#include <stdexcept>

constexpr char to_hex(int c)
{
	return ( 0 <= c && c < 16 ) ? static_cast< char >( c < 10 ? ( '0' + c ) : ( 'a' + ( c - 10 ) ) )
	              : throw std::invalid_argument("Value is not hex");
}

constexpr int from_hex(char c)
{
	if ( '0' <= c && c <= '9' )
	{
		return c - '0';
	}

	if ( 'a' <= c && c <= 'f' )
	{
		return ( c - 'a' ) + 10;
	}

	return ( 'A' <= c && c <= 'F' ) ? ( c - 'A' ) + 10 : throw std::invalid_argument("Character is not hex");
}

#endif // HEX_H
