/*=========================================================================*//**
*  \file        chirp.cpp
*//*==========================================================================*/
#include <climits>
#include <cstddef>
#include <fstream>
#include <functional>
#include <iostream>
#include <limits>
#include <locale>

#include "audio/samples.h"
#include "audio/sink.h"
#include "flac/flac.h"
#include "ogg/ogg.h"
#include "vorbis/vorbis.h"
#include "wav/wav.h"

#include "chirp.h"

namespace
{
Chirp::Format formatByFileName(const std::string&);
}

const Event::Type StopEvent    = Event::Stop;
const Event::Type PlayEvent    = Event::Play;
const Event::Type PauseEvent   = Event::Pause;
const Event::Type SeekEvent    = Event::User;
const Event::Type GetMetaEvent = static_cast< Event::Type >( Event::User + 1 );

/*=========================================================================*//**
*  The Chirp class brings together the disparate elements of audio processing for
*  convenient audio playback. It combines the functionality of the input sources,
*  encapsulation protocol, audio codec, and destination devices with the goal
*  that a user implementing this object can simply call
*
*       myChirpObject.Play("/home/user/music/song.ogg")
*       myChirpObject.Play("http://last.fm/username/favourites.rtp")
*
*  and have the music play.
*
*  \todo Cleanup the whole input_mutex thing
*  \todo Support threadless operation
*//*==========================================================================*/

/***************************************************************************/ /**
 *******************************************************************************/
Chirp::Chirp()
	: position(), len(), sample_rate{0}
{
}

/***************************************************************************/ /**
 *******************************************************************************/
Chirp::~Chirp()
{
	join();
	delete th;
	setCallback(nullptr);
	closeoutput();
	closeinput();
}

/***************************************************************************/ /**
 *  \todo Might want to move to EventQueue
 *******************************************************************************/
void Chirp::start()
{
	if ( th == nullptr )
	{
		th = new std::thread(std::ref(*this) ); /// \todo else throw?
	}
}

/***************************************************************************/ /**
 *******************************************************************************/
void Chirp::init()
{
	state_ = Ready;
}

/***************************************************************************/ /**
 *******************************************************************************/
void Chirp::leave()
{
	state_ = Uninitialized;
}

/***************************************************************************/ /**
 *  \todo Might want to move to EventQueue
 *******************************************************************************/
void Chirp::join()
{
	if ( th != nullptr )
	{
		th->join();
	}
}

/***************************************************************************/ /**
 *  \todo Could introduce indeterminate state, or simply wrap state in a mutex
 *******************************************************************************/
void Chirp::handler(const Event* e)
{
	if ( e == nullptr )
	{
		return;
	}

	switch ( e->type() )
	{
	case PlayEvent:
		output_mutex.lock();

		if ( output != nullptr )
		{
			output->stop(); // \todo: Reset audio
		}

		output_mutex.unlock();

		if ( openinput(e->message() ) )
		{
			state_ = Playing;
			input_mutex.lock();
			input->prepare();
			len = input->getEnd();
			input_mutex.unlock();
		}
		else
		{
			state_ = Ready;
		}

		break;
	case PauseEvent:

		if ( state_ == Playing )
		{
			state_ = Paused;
			output_mutex.lock();

			if ( output != nullptr )
			{
				output->pause();
			}

			output_mutex.unlock();
		}
		else if ( state_ == Paused )
		{
			output_mutex.lock();

			if ( output != nullptr )
			{
				output->resume();
			}

			output_mutex.unlock();
			state_ = Playing;
		}

		break;
	case SeekEvent:
		/// \todo Seek should convert from int to sample_off
		input_mutex.lock();

		if ( input != nullptr )
		{
			input->seek(e->data() );
		}

		input_mutex.unlock();
		break;
	case StopEvent:
		state_ = Ready;
		output_mutex.lock();

		if ( output != nullptr )
		{
			output->stop();
		}

		output_mutex.unlock();
		closeinput();
		break;
	case GetMetaEvent:

		if ( openinput(e->message() ) )
		{
			state_   = FindingMeta;
			metaname = e->message();
		}
		else // failure
		{
			state_ = Ready;
			callback_mutex.lock();

			if ( callback != nullptr )
			{
				callback->meta(e->message(), MetaData(), false);
			}

			callback_mutex.unlock();
		}

		break;
	default:
		break;
	} // switch

}

/***************************************************************************/ /**
 *  \todo Rename FindingMeta to "Searching"
 *  \todo mutex is tied up quite a lot
 *  \bug Converting sample_off to int via static_cast
 *******************************************************************************/
bool Chirp::idle()
{
	bool     didwork = false;
	MetaData m;

	switch ( state_ )
	{
	case Playing:
		input_mutex.lock();

		if ( input == nullptr )
		{
			// do nothing
		}
		else if ( !input->eos() )
		{
			output_mutex.lock();

			if ( output != nullptr )
			{
				*input >> *output;
			}
			else
			{
				input->getPCM();
			}

			output_mutex.unlock();

			/// \todo get position from _output_
			position    = input->getPos();
			sample_rate = input->getSampleRate();

			didwork = true;

			callback_mutex.lock();

			if ( callback != nullptr )
			{
				callback->time(pos(), length() );
			}

			callback_mutex.unlock();
		}
		else // Finished playing
		{
			state_ = Ready;

			callback_mutex.lock();

			if ( callback != nullptr )
			{
				callback->done();
			}

			callback_mutex.unlock();
		}

		input_mutex.unlock();
		break;
	case FindingMeta:
		input_mutex.lock();

		if ( input == nullptr )
		{
			state_ = Ready;
		}
		else if ( input->eos() )
		{
			callback_mutex.lock();

			if ( callback != nullptr )
			{
				callback->meta(metaname, MetaData(), false);
			}

			callback_mutex.unlock();
		}
		else if ( !input->getMeta(m) )
		{
			input->getPCM();
			didwork = true;
		}
		else // Wait until outer program asks for meta
		{
			state_ = Ready;
			callback_mutex.lock();

			if ( callback != nullptr )
			{
				callback->meta(metaname, m, true);
			}

			callback_mutex.unlock();
		}

		input_mutex.unlock();
		break;
	default:
		break;
	} // switch

	return didwork;
}

/***************************************************************************/ /**
 *******************************************************************************/
void Chirp::setCallback(ChirpCallback* c)
{
	std::lock_guard< std::mutex > lk(callback_mutex);

	callback = c;
}

/***************************************************************************/ /**
 *  \todo input.mimetype() == "audio/flac"
 *  \todo when mimetype == "audio/ogg" could be vorbis or flac. There is underlying
 *     Codec mime types for audio/vorbis, etc. Determining this should be the
 *     responsibility of the Ogg class, and may depend on the stream
 *******************************************************************************/
bool Chirp::openinput(const std::string& s)
{
	closeinput();

	if ( s.empty() )
	{
		return true;
	}

	auto* ip = new Input(s.c_str() );

	bool success = ip->isOpen();

	if ( success )
	{
		std::lock_guard< std::mutex > lk(input_mutex);

		input    = ip;
		position = { 0 };
		len      = { 0 };
	}
	else
	{
		delete ip;
	}

	return success;
}

/***************************************************************************/ /**
 *******************************************************************************/
void Chirp::closeinput()
{
	std::lock_guard< std::mutex > l(input_mutex);

	Input* ip = input;

	input    = nullptr;
	position = { 0 };
	len      = { 0 };
	delete ip;
}

/***************************************************************************/ /**
 *  \bug This function is a bit of a bug itself.
 *
 *  \note Takes ownership of AudioOutput object and deletes it when no longer
 *     needed, or deletes it immediately if it can't be opened.
 *******************************************************************************/
bool Chirp::openoutput(audio::Sink* op)
{
	closeoutput();

	if ( op == nullptr )
	{
		return true;
	}

	bool success = op->open();

	if ( success )
	{
		std::lock_guard< std::mutex > lk(output_mutex);
		output = op;
	}
	else
	{
		delete op;
	}

	return success;
}

/***************************************************************************/ /**
 *******************************************************************************/
void Chirp::closeoutput()
{
	std::lock_guard< std::mutex > lk(output_mutex);

	audio::Sink* op = output;

	output = nullptr;
	delete op;
}

/***************************************************************************/ /**
 *******************************************************************************/
void Chirp::play(const std::string& s)
{
	post(PlayEvent, s);
}

/***************************************************************************/ /**
 *******************************************************************************/
void Chirp::stop()
{
	post(StopEvent, 0);
}

/***************************************************************************/ /**
 *******************************************************************************/
void Chirp::pause()
{
	post(PauseEvent, 0);
}

/***************************************************************************/ /**
 *******************************************************************************/
void Chirp::seekTime(int i)
{
	/// @todo Accept seconds as argument
	/// @todo Should be able to post the sample_off directly
	post(SeekEvent, ( std::chrono::seconds(i) * sample_rate ).value);
}

/***************************************************************************/ /**
 *******************************************************************************/
long Chirp::pos() const
{
	/// @todo return seconds
	return audio::toSec(position, sample_rate).count();
}

/***************************************************************************/ /**
 *******************************************************************************/
long Chirp::length() const
{
	/// @todo return seconds
	return audio::toSec(len, sample_rate).count();
}

/***************************************************************************/ /**
 *  \todo Rename the first of these
 *******************************************************************************/
void Chirp::getMeta(const std::string& s)
{
	post(GetMetaEvent, s);
}

/***************************************************************************/ /**
 *******************************************************************************/
bool Chirp::supported(const std::string& s)
{
	return formatByFileName(s) != Unsupported;
}

/*=========================================================================*//**
*  \bug suppose "fmap = new MemoryMap(...)" throws
*  \bug Is source going to throw on empty data? If fmap were empty?
*//*==========================================================================*/

/***************************************************************************/ /**
 *  Object for inputting audio from a local file
 *******************************************************************************/
Chirp::Input::Input(const std::string& path)
	: source(nullptr), codec(nullptr)
{
	if ( path.empty() )
	{
		return;
	}

	Chirp::Format t = formatByFileName(path);

	file.open(path.c_str(), std::ios_base::in | std::ios_base::out | std::ios_base::binary);

	if ( file.is_open() )
	{
		switch ( t )
		{
		case Chirp::RawFlac:
			source = new FlacFrame(file);
			codec  = new Flac;
			break;
		case Chirp::OggFlac:
			source = new Ogg(file);
			codec  = new Flac;
			break;
		case Chirp::OggVorbis:
			source = new Ogg(file);
			codec  = new Vorbis;
			break;
		case Chirp::Wav:
			source = new WavFrame(file);
			codec  = new ::Wav;
			break;
		default:
			file.close();
			break;
		} // switch

	}
}

/***************************************************************************/ /**
 *******************************************************************************/
Chirp::Input::~Input()
{
	delete codec;
	delete source;
	file.close();
}

/***************************************************************************/ /**
 *******************************************************************************/
bool Chirp::Input::isOpen()
{
	return file.is_open();
}

/***************************************************************************/ /**
 *******************************************************************************/
audio::sample_rate_type Chirp::Input::getSampleRate() const
{
	return codec != nullptr ? codec->sample_rate() : audio::sample_rate_type{ 0 };
}

/***************************************************************************/ /**
 *******************************************************************************/
audio::sample_off Chirp::Input::getPos() const
{
	return source != nullptr ? source->position() : audio::sample_off();
}

/***************************************************************************/ /**
 *******************************************************************************/
audio::sample_off Chirp::Input::getEnd() const
{
	/// @todo audio::sample_off::max instead of 1UL initialization
	return source != nullptr ? source->length() : audio::sample_off{ 1UL };
}

/***************************************************************************/ /**
 *******************************************************************************/
void Chirp::Input::seek(unsigned long i)
{
	if ( source != nullptr )
	{
		/// @todo Should probably take an offset as an argument
		source->seek(audio::sample_off{ i });
	}

	if ( codec != nullptr )
	{
		codec->seek();
	}
}

/***************************************************************************/ /**
 *******************************************************************************/
audio::samples< std::int_least16_t > Chirp::Input::getPCM()
{
	audio::samples< std::int_least16_t > data;

	if ( ( source != nullptr ) && ( codec != nullptr ) )
	{
		*source >> *codec;
		data = codec->DumpAudio(); /// \todo *codec >> data
	}

	return data;
}

/***************************************************************************/ /**
 *******************************************************************************/
bool Chirp::Input::getMeta(MetaData& m) const
{
	return codec != nullptr ? codec->getMeta(m) : false;
}

/***************************************************************************/ /**
 *******************************************************************************/
bool Chirp::Input::eos() const
{
	return source != nullptr ? source->eos() : true;
}

/***************************************************************************/ /**
 *******************************************************************************/
Chirp::Input& Chirp::Input::operator>>(audio::Sink& o)
{
	o.write(getPCM() );
	return *this;
}

/***************************************************************************/ /**
 *******************************************************************************/
void Chirp::Input::prepare()
{
	if ( source != nullptr )
	{
		source->prepare();
	}
}

namespace
{
/***************************************************************************/ /**
 *******************************************************************************/
Chirp::Format formatByFileName(const std::string& s)
{
	std::size_t i = s.find_last_of('.');

	if ( i == std::string::npos )
	{
		return Chirp::Unsupported;
	}

	Chirp::Format format = Chirp::Unsupported;

	std::size_t n = s.size();
	std::string t;

	for (; i < n; ++i)
	{
		t.push_back(static_cast< char >( tolower(s[i]) ) );
	}

	if ( t == ".flac" )
	{
		format = Chirp::RawFlac;
	}
	else if ( t == ".oga" )
	{
		format = Chirp::OggFlac;
	}
	else if ( t == ".ogg" )
	{
		format = Chirp::OggVorbis;
	}
	else if ( t == ".wav" )
	{
		format = Chirp::Wav;
	}

	return format;
}

}
