#ifndef PLAYTHREAD_HPP
#define PLAYTHREAD_HPP

#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include <mutex>
#include <thread>

#include "chirp/metadata.h"
#include "event/eventqueue.h"

#include "flac/flac.h"
#include "ogg/ogg.h"
#include "playlist/playlist.h"
#include "vorbis/vorbis.h"
#include "wav/wav.h"

namespace audio
{
class Sink;
}

class ChirpCallback
{
public:
	virtual ~ChirpCallback() = default;

	virtual void time(long, long) = 0;

	virtual void meta(std::string, MetaData, bool) = 0;
	virtual void done()                            = 0;
};

class Chirp
	: public EventQueue
{
	class Input;

	enum
	{
		Uninitialized,
		Ready,
		Playing,
		FindingMeta,
		Paused
	} state_{ Uninitialized };

	Input*       input{ nullptr };
	audio::Sink* output{ nullptr };

	std::mutex input_mutex;
	std::mutex output_mutex;
	std::mutex callback_mutex;

	ChirpCallback* callback{ nullptr };

	audio::sample_off       position;
	audio::sample_off       len;
	audio::sample_rate_type sample_rate;
	std::string             metaname;
	std::thread*            th{ nullptr };

	Chirp(const Chirp&)            = delete; // non-copyable
	Chirp& operator=(const Chirp&) = delete; // non-copyable

	void handler(const Event*) override;
	bool idle() override;
	void init() override;
	void leave() override;

	bool openinput(const std::string&);
	void closeinput();
	void closeoutput();
public:
	enum Format
	{
		Unsupported,
		OggVorbis,
		OggFlac,
		RawFlac,
		Wav
	};

	enum Notifications
	{
		UpdatePos,
		Finished,
		HaveMeta,
		MetaError
	};

	Chirp();
	~Chirp() override;

	// threading
	void start();
	void join();

	bool openoutput(audio::Sink*);

	void play(const std::string&);
	void pause();
	void stop();
	void getMeta(const std::string&);
	void seekTime(int);
	long pos() const;
	long length() const;

	void setCallback(ChirpCallback*);

	static bool supported(const std::string&);
};

class Chirp::Input
{
	std::fstream          file;
	audio::Encapsulation* source;
	audio::Codec*         codec;

	Input(const Input&)            = delete; // non-copyable
	Input& operator=(const Input&) = delete; // non-copyable
public:
	explicit Input(const std::string&);
	~Input();

	audio::samples< std::int_least16_t > getPCM();
	Input& operator>>(audio::Sink&);

	audio::sample_rate_type getSampleRate() const;
	bool getMeta(MetaData&) const;

	bool isOpen();
	bool eos() const;
	void prepare();
	audio::sample_off getPos() const;
	audio::sample_off getEnd() const;
	void seek(unsigned long);
};

#endif // ifndef PLAYTHREAD_HPP
