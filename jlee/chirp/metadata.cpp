#include "metadata.h"

/*=========================================================================*//**
*  \class       MetaData
*  \brief       Contains meta data and provides access via enumerators
*
*  \todo Maybe rename Tags? MetaData might be too general. Esp w/ Flac MetaData structures
*//*==========================================================================*/
const int MetaData::numFields = 5;

/***************************************************************************/ /**
 *******************************************************************************/
void MetaData::swap(MetaData& m)
{
	values.swap(m.values);
}

/***************************************************************************/ /**
 *******************************************************************************/
void MetaData::clear()
{
	values.clear();
}

/***************************************************************************/ /**
 *******************************************************************************/
bool MetaData::operator!=(const MetaData& m) const
{
	return values != m.values;
}

/***************************************************************************/ /**
 *******************************************************************************/
const std::string& MetaData::operator[](Field f) const
{
	auto it = values.find(f);

	if ( it != values.end() )
	{
		return it->second;
	}

	static const std::string dummy;

	return dummy;
}

/***************************************************************************/ /**
 *******************************************************************************/
void MetaData::assign(
	Field       f,
	std::string s
)
{
	if ( f != Invalid_ )
	{
		values[f] = std::move(s);
	}
}

/***************************************************************************/ /**
 *******************************************************************************/
MetaData::Field MetaData::intToField(int i)
{
	switch ( i )
	{
	case Title:
		return Title;

	case Album:
		return Album;

	case Artist:
		return Artist;

	case Duration:
		return Duration;

	case Comment:
		return Comment;

	default:
		break;
	}

	return Invalid_;
}

/***************************************************************************/ /**
 *******************************************************************************/
std::string displayName(MetaData::Field f)
{
	std::string s;

	switch ( f )
	{
	case MetaData::Title:
		s.assign("Title", 5);
		break;
	case MetaData::Album:
		s.assign("Album", 5);
		break;
	case MetaData::Artist:
		s.assign("Artist", 6);
		break;
	case MetaData::Duration:
		s.assign("Duration", 8);
		break;
	case MetaData::Comment:
		s.assign("Comment", 7);
		break;
	default:
		break;
	}

	return s;
}
