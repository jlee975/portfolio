#ifndef META_HPP
#define META_HPP

#include <map>
#include <string>

class MetaData
{
public:
	enum Field
	{
		Invalid_ = -1,
		Title = 0,
		Album,
		Artist,
		Duration,
		Comment
	};

	void assign(Field, std::string);
	void swap(MetaData&);
	void clear();

	bool operator!=(const MetaData&) const;

	const std::string& operator[](Field) const;

	static Field intToField(int);

	static const int numFields;
private:
	std::map< Field, std::string > values;
};

/// @todo Return string_view
std::string displayName(MetaData::Field);

#endif // ifndef META_HPP
