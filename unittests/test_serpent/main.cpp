#include <array>

#include "cipher/serpent.h"
#include "serpent_test_strings.h"
#include "unit/unit.h"

int main()
{
	for (const auto& i : serpent_kat_vk_128)
	{
		check_vk< Serpent >(i);
	}

	for (const auto& i : serpent_kat_vk_192)
	{
		check_vk< Serpent >(i);
	}

	for (const auto& i : serpent_kat_vk_256)
	{
		check_vk< Serpent >(i);
	}

	for (const auto& i : serpent_kat_vt_128)
	{
		check_vt< Serpent, 128 >(i);
	}

	for (const auto& i : serpent_kat_vt_192)
	{
		check_vt< Serpent, 192 >(i);
	}

	for (const auto& i : serpent_kat_vt_256)
	{
		check_vt< Serpent, 256 >(i);
	}

	for (unsigned long i = 0; i < 20; i++)
	{
		check_monte_carlo< Serpent >();
	}

	std::cout << "Tests ok" << std::endl;
}
