#include "sha/sha256.h"
#include "unit/unit.h"

int main()
{
	char* buf = new char [1000000];

	for (std::size_t i = 0; i < 1000000; ++i)
	{
		buf[i] = '\0';
	}

	// #1) 1 byte 0xbd
	assert_equal(sha256_raw("\xbd", 1), "68325720aabd7c82f30f554b313d0570c95accbb7dc4b5aae11204c08ffe732b");

	// #2) 4 bytes 0xc98c8e55
	assert_equal(sha256_raw("\xc9\x8c\x8e\x55", 4),
		"7abc22c0ae5af26ce93dbb94433a0e0b2e119d014f8e7f65bd56c61ccccd9504");

	// #3) 55 bytes of zeros
	assert_equal(sha256_raw(buf, 55), "02779466cdec163811d078815c633f21901413081449002f24aa3e80f0b88ef7");

	// #4) 56 bytes of zeros
	assert_equal(sha256_raw(buf, 56), "d4817aa5497628e7c77e6b606107042bbba3130888c5f47a375e6179be789fbb");

	// #5) 57 bytes of zeros
	assert_equal(sha256_raw(buf, 57), "65a16cb7861335d5ace3c60718b5052e44660726da4cd13bb745381b235a1785");

	// #6) 64 bytes of zeros
	assert_equal(sha256_raw(buf, 64), "f5a5fd42d16a20302798ef6ed309979b43003d2320d9f0e8ea9831a92759fb4b");

	// #7) 1000 bytes of zeros
	assert_equal(sha256_raw(buf, 1000), "541b3e9daa09b20bf85fa273e5cbd3e80185aa4ec298e765db87742b70138a53");

	// #10) 1000000 bytes of zeros
	assert_equal(sha256_raw(buf, 1000000), "d29751f2649b32ff572b5e0a9f541ea660a50f94ff0beedfb0b692b924cc8025");

	// #8) 1000 bytes of 0x41 'A'
	for (std::size_t i = 0; i < 1000; ++i)
	{
		buf[i] = 'A';
	}

	assert_equal(sha256_raw(buf, 1000), "c2e686823489ced2017f6059b8b239318b6364f6dcd835d0a519105a1eadd6e4");

	// #9) 1005 bytes of 0x55 'U'
	for (std::size_t i = 0; i < 1005; ++i)
	{
		buf[i] = 'U';
	}

	assert_equal(sha256_raw(buf, 1005), "f4d62ddec0f3dd90ea1380fa16a5ff8dc4c54b21740650f24afc4120903552b0");

	/*
	 #11) 0x20000000 (536870912) bytes of 0x5a 'Z'
	   "15a1868c12cc53951e182344277447cd0979536badcc512ad24c67e9b2d4f3dd"
	 #12) 0x41000000 (1090519040) bytes of zeros
	   "461c19a93bd4344f9215f5ec64357090342bc66b15a148317d276e31cbc20b53"
	 #13) 0x6000003e (1610612798) bytes of 0x42 'B'
	   "c23ce8a7895f4b21ec0daf37920ac0a262a220045a03eb2dfed48ef9b05aabea"
	 */
	delete[] buf;
	std::cout << "Tests ok" << std::endl;
}
