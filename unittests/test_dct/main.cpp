#include "signal/dct3.h"
#include "unit/unit.h"

int main()
{
	std::array< double, 1 >       x0 = { 1 };
	const std::array< double, 1 > y0 = { 1 };

	std::array< double, 8 >       x1 = { 0, 1, 2, 3, 4, 5, 6, 7 };
	const std::array< double, 8 > y1
	    = { 14.5909643204810773992452, -16.1530568419829384762123, 6.3584364935997197718660,
		    -5.4952018128194874098933, 2.8643367438856433198458, -2.4594700824170179804484,
		    0.9403819318154762943812, -0.6463907525624729187842 };

	assert_max_error(DCTIII(x0), y0, 0);
	assert_max_error(DCTIII(x1), y1, 7.11e-15);
	std::cout << "Tests ok" << std::endl;
}
