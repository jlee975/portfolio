#include "tableau/tableau.h"
#include "unit/unit.h"

int main()
{
	std::vector< int > rows;
	std::vector< int > cols;

	cols.reserve(25);

	for (int i = 0; i < 25; ++i)
	{
		cols.push_back(1200);
	}

	cols.push_back(1000);

	rows.push_back(6209);
	rows.push_back(6212);
	rows.push_back(6167);
	rows.push_back(6223);
	rows.push_back(6189);

	Tableau tableau(rows, cols);

	tableau.solve();

	std::vector< int > rsum;
	std::vector< int > csum;

	for (std::size_t r = 0; r < rows.size(); ++r)
	{
		int x = 0;

		for (std::size_t c = 0; c < cols.size(); ++c)
		{
			x += tableau.at(r, c);
		}

		rsum.push_back(x);
	}

	for (std::size_t c = 0; c < cols.size(); ++c)
	{
		int x = 0;

		for (std::size_t r = 0; r < rows.size(); ++r)
		{
			x += tableau.at(r, c);
		}

		csum.push_back(x);
	}

	assert_equal(rows, rsum);
	assert_equal(cols, csum);
	std::cout << "Tests ok" << std::endl;
}
