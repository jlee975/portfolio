#include "unit/unit.h"

#include "math/geometry/intersection/test_geometry.h"
#include "physics/body.h"
#include <cmath>

struct static_test_info
{
	geo::geo_t o1;
	geo::point< 3 >::initializer p1;
	geo::geo_t o2;
	geo::point< 3 >::initializer p2;
	geo::intersection_type result;
};

// A list of objects in various positions, and whether or not they collide
const static_test_info static_tests[] = {
	{ geo::Plane, { 0, 0, 0 }, geo::Sphere, { 0, 0, 0 }, geo::intersects },
	{ geo::Plane, { 0, 0, 0 }, geo::Sphere, { 1, 0, 0 }, geo::intersects },
	{ geo::Plane, { 0, 0, 0 }, geo::Sphere, { -1, 0, 0 }, geo::intersects },
	{ geo::Plane, { 0, 0, 0 }, geo::Sphere, { std::nextafterf(1, INFINITY), 0, 0 }, geo::no_intersection },
	{ geo::Plane, { 0, 0, 0 }, geo::Sphere, { std::nextafterf(-1, -INFINITY), 0, 0 }, geo::no_intersection },
	{ geo::Plane, { 0, 0, 0 }, geo::Icosahedron, { 0, 0, 0 }, geo::intersects },
	{ geo::Plane, { 0, 0, 0 }, geo::Icosahedron, { std::numbers::phi_v< float >, 0, 0 }, geo::intersects },
	{ geo::Plane,
	  { 0, 0, 0 },
	  geo::Icosahedron,
	  { std::nextafterf(std::numbers::phi_v< float >, INFINITY), 0, 0 },
	  geo::no_intersection },
	{ geo::Plane,
	  { 0, 0, 0 },
	  geo::Icosahedron,
	  { -std::nextafterf(std::numbers::phi_v< float >, INFINITY), 0, 0 },
	  geo::no_intersection },
	{ geo::Sphere, { 0, 0, 0 }, geo::Sphere, { 0, 0, 0 }, geo::intersects },
	{ geo::Sphere, { -1, 0, 0 }, geo::Sphere, { 1, 0, 0 }, geo::intersects },
	{ geo::Sphere, { -1, 0, 0 }, geo::Sphere, { std::nextafterf(1, INFINITY), 0, 0 }, geo::no_intersection },
	{ geo::Sphere, { 0, 0, 0 }, geo::Box, { 0, 0, 0 }, geo::intersects },
	{ geo::Sphere, { -1, 0, 0 }, geo::Box, { 1, 0, 0 }, geo::intersects },
	{ geo::Sphere, { -1, 0, 0 }, geo::Box, { std::nextafterf(1, INFINITY), 0, 0 }, geo::no_intersection },
	{ geo::Box, { 0, 0, 0 }, geo::Box, { 0, 0, 0 }, geo::intersects },
	{ geo::Box, { -1, 0, 0 }, geo::Box, { 1, 0, 0 }, geo::intersects },
	{ geo::Box, { -1, 0, 0 }, geo::Box, { std::nextafterf(1, INFINITY), 0, 0 }, geo::no_intersection },
	{ geo::Icosahedron, { 0, 0, 0 }, geo::Icosahedron, { 0, 0, 0 }, geo::intersects },
	{ geo::Icosahedron, { 0, 0, 0 }, geo::Icosahedron, { 1, 1, 1 }, geo::intersects },
	{ geo::Icosahedron,
	  { -std::numbers::phi_v< float >, 0, 0 },
	  geo::Icosahedron,
	  { std::numbers::phi_v< float >, 0, 0 },
	  geo::intersects },
	{ geo::Icosahedron,
	  { -std::numbers::phi_v< float >, 0, 0 },
	  geo::Icosahedron,
	  { std::nextafterf(std::numbers::phi_v< float >, INFINITY), 0, 0 },
	  geo::no_intersection }, };

/// @todo When we have point-point collisions, automatically test the case where one object has been
/// moved by FLT_EPSILON (or std::nextafterf)
/// @todo Test spheres, planes in different orientations
void intersection_tests()
{
	for (const auto& sti : static_tests)
	{
		geo::Geometry o1(sti.o1);
		Affine        a1(default_initialized);
		a1.translate(sti.p1);

		geo::Geometry o2(sti.o2);
		Affine        a2(default_initialized);
		a2.translate(sti.p2);

		assert_equal(test_intersect(o1, a1, o2, a2).type, sti.result);
		assert_equal(test_intersect(o2, a2, o1, a1).type, sti.result);
	}
}

void test_sphere_plane_collision()
{
	// Create a YZ plane
	auto g1 = std::make_shared< geo::Geometry >(geo::Plane);
	Body plane(g1);

	plane.setMass(INFINITY);

	// Create a sphere at (-50,0,0) with velocity (1,0,0)
	auto   g2 = std::make_shared< geo::Geometry >(geo::Sphere);
	Body   sphere(g2);
	Affine f(default_initialized);

	f.translate(-50.f, 0.f, 0.f);
	sphere.setVelocity(1, 0, 0);
	sphere.setFrame(f);

	// Check that collision occurs at time 49s
	{
		const auto res = Body::do_retest_inner(plane, sphere);
		assert_true(res.first);
		assert_equal(res.second, 49);
	}

	{
		const auto res = Body::do_retest_inner(sphere, plane);
		assert_true(res.first);
		assert_equal(res.second, 49);
	}
}

void test_sphere_sphere_collision()
{
	auto g = std::make_shared< geo::Geometry >(geo::Sphere);

	// Create a sphere at (-50,0,0) with velocity (1,0,0)
	Body   sphere1(g);
	Affine f1(default_initialized);

	f1.translate(-50.f, 0.f, 0.f);
	sphere1.setVelocity(1, 0, 0);
	sphere1.setFrame(f1);

	Body   sphere2(g);
	Affine f2(default_initialized);

	f2.translate(50.f, 0.f, 0.f);
	sphere2.setVelocity(-1, 0, 0);
	sphere2.setFrame(f2);

	// Check that collision occurs at time 49s
	const auto res = Body::do_retest_inner(sphere1, sphere2);

	assert_true(res.first);
	assert_equal(res.second, 49);
}

int main()
{
	intersection_tests();
	test_sphere_plane_collision();
	test_sphere_sphere_collision();
}
