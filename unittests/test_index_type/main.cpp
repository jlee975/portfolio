#include "util/index_type.h"

#include "unit/unit.h"

int main()
{
	index_type< unsigned long > x(3);
	index_type< unsigned long > y(2);

	assert_not_equal(x, y);
	++y;
	assert_equal(x, y);
	std::cout << "Tests ok" << std::endl;
}
