#include "multifor/multifor.h"
#include <iostream>

int main()
{
	for ( const auto& [first, second] : multifor(3, 4) )
	{
		std::cout << first << "," << second << std::endl;
	}

	std::cout << "Tests ok" << std::endl;
}
