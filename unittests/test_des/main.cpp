#include <array>
#include <ctime>

#include "bits/parity.h"
#include "cipher/des.h"
#include "des_test_strings.h"
#include "unit/unit.h"

/*=========================================================================*//**

   TESTSUITE

   The following code section is only compiled if TESTSUITE is defined. It contains
   code and data for testing the accuracy of the DES routines above. There are two
   sets of tests: known answer tests (KAT), and Monte Carlo tests (MCT).

   Declaring TESTSUITE causes this source to become a stand alone,
   mini-application. The section below contains a main() function, a (cheap) RNG,
   and the data for the KATs. It's not necessary to link against other files.

   The Known Answer Tests consist of (key,plaintext,cipher) triples that have
   been externally verified. The test suite simply loops through them all,
   confirming that both the forward and reverse transforms match the known data.
   There are approximately 400 sets of such triples in des_kat.

   The Monte Carlo Tests consist of 100 trials of encrypting and decrypting
   randomly generated data. Each trial processes 256 kilobytes of data, comparing
   the original data against the recovered.

   At the time of this writing, the code passes all the tests for all
   supported modes of operation.
*//*==========================================================================*/

// A simple linear congruential random number generator
namespace
{
class DESKeyGen
{
public:
	DESKeyGen()
		: gen(rd() ), dis(0, 255)
	{
	}

	unsigned char operator()()
	{
		const unsigned char c = dis(gen);

		if ( parity(c) == 0 )
		{
			return c ^ 1;
		}

		return c;
	}
private:
	std::random_device                             rd;
	std::default_random_engine                     gen;
	std::uniform_int_distribution< unsigned char > dis;
};
}

int main()
{
	for (const auto& i : des_kat)
	{
		check_kat_all< DES >(i);
	}

	// Monte Carlo Tests ---------------------------------------------------
	for (unsigned long i = 0; i < 100; i++)
	{
		check_monte_carlo< DES >(DESKeyGen() );
	}

	std::cout << "Tests ok" << std::endl;
}
