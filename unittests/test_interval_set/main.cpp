#include <iostream>

#include "containers/interval_set.h"
#include "unit/unit.h"

int main()
{
	{
		interval_set< int > x;
		x.insert(3);

		assert_equal(x.count(2), 0u);
		assert_equal(x.count(3), 1u);
		assert_equal(x.count(4), 0u);
	}

	{
		interval_set< int > x;
		x.insert(10, 20);
		assert_equal(x.count(9), 0u);

		for (int i = 10; i <= 20; ++i)
		{
			assert_equal(x.count(i), 1u);
		}
	}

	{
		interval_set< int > x;
		x.insert(3, 5);
		x.insert(4, 6);

		interval_set< int > y;
		y.insert(3, 6);

		assert_equal(x, y);
	}

	std::cout << "Tests ok" << std::endl;

	return 0;
}
