#include "bits/ilog.h"
#include "unit/unit.h"

int main()
{
	assert_equal(popcount(UINT32_C(0) ), 0u);
	assert_equal(popcount(UINT32_C(1) ), 1u);
	assert_equal(popcount(UINT32_C(2) ), 1u);
	assert_equal(popcount(UINT32_C(3) ), 2u);
	assert_equal(popcount(UINT32_C(0xffffffff) ), 32u);
	assert_equal(popcount(UINT64_C(0xffffffffffffffff) ), 64u);
	assert_equal(ilog2p1(0), 0u);
	assert_equal(ilog2p1(1), 1u);
	assert_equal(ilog2p1(2), 2u);
	assert_equal(ilog2p1(3), 2u);
	assert_equal(ilog2p1(4), 3u);
	assert_equal(ilog2p1(5), 3u);
	std::cout << "Tests ok" << std::endl;
}
