#include <cstdlib>
#include <iostream>
#include <limits>
#include <stdexcept>

#include "bigint/zinteger.h"
#include "unit/unit.h"

using jl::bignum::ZInteger;

#if 0
class NonNegative::UnitTest
{
public:
	void run()
	{
		NonNegative special[] = {
			// zero
			NonNegative(),
			NonNegative(0),

			// construct from built in types
			NonNegative(1),        // int constructor
			NonNegative(24489l),   // long constructor
			NonNegative(420492u),  // unsigned constructor
			NonNegative(402831ul), // unsigned long constructor

			// bounds checking
			NonNegative( std::numeric_limits< int >::max() ),
			NonNegative( std::numeric_limits< long >::max() ),
			NonNegative( std::numeric_limits< unsigned >::max() ),
			NonNegative( std::numeric_limits< unsigned long >::max() ) };

		for (std::size_t i = 0; i < sizeof( special ) / sizeof( special[0] ); ++i)
		{
			test(special[i]);
		}
	}
private:
	// Ensure that x and y are equal in all ways
	bool test_equal(
		const NonNegative& x,
		const NonNegative& y
	) const
	{
		if ( x < y || x != y || x > y || y < x || y != x || y > x )
		{
			return false;
		}

		if ( !( x == y && y == x && y <= x && y >= x && x <= y && x >= y ) )
		{
			return false;
		}

		if ( x.size() != y.size() )
		{
			return false;
		}

		return true;
	}

	bool test_not_equal(
		const NonNegative& x,
		const NonNegative& y
	) const
	{
		if ( x == y || y == x )
		{
			std::cout << x << " has tested equal to " << y << std::endl;
			return false;
		}

		if ( ( x < y || y > x ) && ( x >= y || y <= x ) )
		{
			return false;
		}

		if ( ( x > y || y < x ) && ( x <= y || y >= x ) )
		{
			return false;
		}

		if ( !( x != y && y != x ) )
		{
			std::cout << x << " failed to be not equal to " << y << std::endl;
			return false;
		}

		if ( !( x < y || x > y ) )
		{
			std::cout << x << " was neither larger nor greater than " << y << std::endl;
			return false;
		}

		if ( !( y < x || y > x ) )
		{
			std::cout << x << " was neither larger nor greater than " << y << std::endl;
			return false;
		}

		return true;
	}

	bool test_comparison_identies(
		const NonNegative& x,
		const NonNegative& y
	) const
	{
		if ( ( x < y && x > y ) || ( y < x && y > x ) )
		{
			return false;
		}

		if ( ( x == y || y == x ) && ( x != y || y != x ) )
		{
			return false;
		}

		if ( ( y == x ) != ( x == y ) )
		{
			return false;
		}

		if ( ( y != x ) != ( x != y ) )
		{
			return false;
		}

		if ( ( y < x ) != ( x > y ) )
		{
			return false;
		}

		if ( ( x < y ) != ( y > x ) )
		{
			return false;
		}

		if ( ( x <= y ) != ( y >= x ) )
		{
			return false;
		}

		if ( ( x >= y ) != ( y <= x ) )
		{
			return false;
		}

		return true;
	}

	void test(const NonNegative& x) const
	{
		std::cout << "Testing x = " << x << std::endl;

		const NonNegative zero;
		const NonNegative one(1);

		std::cout << "Running comparison tests" << std::endl;

		if ( !test_equal(x, x) )
		{
			throw std::runtime_error("Value does not compare equal to self");
		}

		if ( x )
		{
			std::cout << "x is non-zero" << std::endl;

			if ( !x )
			{
				throw std::runtime_error("Nonzero value casts to false");
			}

			if ( !test_not_equal(x, zero) )
			{
				throw std::runtime_error("Nonzero value compares equal to zero");
			}

			if ( x.size() == 0 )
			{
				throw std::runtime_error("Nonzero value does not have bits");
			}
		}
		else
		{
			std::cout << "x is zero" << std::endl;

			if ( !!x )
			{
				throw std::runtime_error("Zero value casts to true");
			}

			if ( !test_equal(x, zero) )
			{
				throw std::runtime_error("Zero value does not compare equal to zero");
			}

			if ( x.size() != 0 )
			{
				throw std::runtime_error("Zero value does not have zero bits");
			}
		}

		std::cout << "Comparison tests passed" << std::endl;

		std::cout << "Testing copying and assignent" << std::endl;

		{
			NonNegative t1(x);
			NonNegative t2 = x;
			NonNegative t3;
			t3 = x;

			if ( !test_equal(x, t1) || !test_equal(x, t2) || !test_equal(x, t3)
			     || !test_equal(t1, t2) || !test_equal(t1, t3) || !test_equal(t2, t3)
			)
			{
				throw std::runtime_error("Values that are equal do not compare equal");
			}

			if ( test_not_equal(x, t1) || test_not_equal(x, t2) || test_not_equal(x, t3)
			     || test_not_equal(t1, t2) || test_not_equal(t1, t3)
			     || test_not_equal(t2, t3)
			)
			{
				throw std::runtime_error("Values that are equal do not compare equal");
			}
		}

		std::cout << "Copying test passed" << std::endl;

		std::cout << "Test shift operators" << std::endl;

		if ( x.size() )
		{
			NonNegative t1(x);

			t1 <<= 1000;

			if ( t1.size() != x.size() + 1000 )
			{
				throw std::runtime_error("Shift was not successful");
			}

			if ( t1 != ( x << 1000 ) )
			{
				throw std::runtime_error("Shift assign is not the same as shift");
			}

			if ( t1 != ( ( x << 500 ) << 500 ) )
			{
				throw std::runtime_error("Shift assign is not the same as shift");
			}

			if ( ( t1 >> 1000 ) != x )
			{
				throw std::runtime_error("Shift right did not produce expected result");
			}

			t1 >>= 1000;

			if ( t1 != x )
			{
				throw std::runtime_error("Shift assign is not the same as shift");
			}

			if ( t1.size() != x.size() )
			{
				throw std::runtime_error("Size changed after shifts");
			}

			t1 >>= ( x.size() - 1 );

			if ( t1 != one )
			{
				throw std::runtime_error("Could not isolate high bit");
			}

			if ( ( x >> x.size() ) != zero )
			{
				std::cout << t1 << std::endl;
				std::cout << zero << std::endl;
				throw std::runtime_error("Value should be zero");
			}
		}
		else
		{
			NonNegative t1(x);
			t1 <<= 1000;

			if ( t1.size() != 0 || t1 != zero || t1 != x )
			{
				throw std::runtime_error("Expected zero");
			}

			t1 >>= 1000;

			if ( t1.size() != 0 || t1 != zero || t1 != x )
			{
				throw std::runtime_error("Expected zero");
			}

			if ( ( x << 1000 ) != zero )
			{
				throw std::runtime_error("Expected zero");
			}

			if ( ( x >> 1000 ) != zero )
			{
				throw std::runtime_error("Expected zero");
			}
		}

		std::cout << "Shift operators passed" << std::endl;
	}

};
#endif // if 0

class BignumTest
{
public:
	void run()
	{
		for (int i = 0; i < 100; ++i)
		{
			// Two values guaranteed to be different
			ZInteger a = zrand(3000);
			ZInteger b = zrand(3001);

			// copy constructed
			ZInteger c(a);
			test_equal(a, c);

			// copy assignment
			c = b;
			test_equal(b, c);

			test_not_equal(a, b);
			test_relations(a, b);
			test_arithmetic(a, b);
		}
	}
private:
	ZInteger zrand(unsigned n)
	{
		ZInteger x = 1;

		for (unsigned i = 0; i < n; ++i)
		{
			x <<= 1;
			x  += rand() % 2;
		}

		return x;
	}

	// test two values that should be equal
	void test_equal(
		const ZInteger& a,
		const ZInteger& b
	) const
	{
		// Check == and !=
		assert_equal(a, b);
		assert_equal(b, a);

		assert_less_than_equal(a, b);
		assert_greater_than_equal(a, b);
		assert_less_than_equal(b, a);
		assert_greater_than_equal(b, a);

		assert_equal(a - b, 0);
		assert_equal(b - a, 0);

		// Addition should produce twice the result
		const ZInteger twice = a + b;

		assert_equal(b + a, twice);
		assert_equal(twice, 2 * a);
		assert_equal(twice, 2 * b);

		// Multiplication should produce square
		const ZInteger square = a * a;

		assert_equal(b * b, square);
		assert_equal(a * b, square);
		assert_equal(b * a, square);

		// Division should yield one
		assert_equal(a / b, 1);
		assert_equal(b / a, 1);

		assert_equal(a * a / b, b);
		assert_equal(b * b / a, a);
	}

	// test two values that should not be equal
	void test_not_equal(
		const ZInteger& a,
		const ZInteger& b
	) const
	{
		assert_not_equal(a, b);
		assert_not_equal(b, a);
		assert_false(a == b);
		assert_false(b == a);
		assert_false(!( a < b ) && !( a > b ) );
		assert_false(!( b < a ) && !( b > a ) );
	}

	// test <,>,<=,>=,==,!=
	void test_relations(
		const ZInteger& a,
		const ZInteger& b
	) const
	{
		// test symmetry
		assert_false(a == b && !( b == a ) );
		assert_false(a != b && !( b != a ) );

		// test asymmetric
		assert_false(a < b && ( !( b > a ) || !( a <= b ) || !( b >= a ) || a == b || b == a ) );
	}

	// test +,-,*,/
	void test_arithmetic(
		const ZInteger& a,
		const ZInteger& b
	) const
	{
	}

};

int main()
{
	BignumTest test;

	test.run();

	std::cout << "Tests OK" << std::endl;
}
