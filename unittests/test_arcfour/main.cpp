#include <array>
#include <cstring>

#include "cipher/arcfour.h"
#include "unit/unit.h"

struct kat
{
	const char* key;
	std::array< std::byte, 16 > sbytes;
};

const kat KAT[] = {{ "Key",
	{ std::byte{ 0xeb }, std::byte{ 0x9f }, std::byte{ 0x77 }, std::byte{ 0x81 }, std::byte{ 0xb7 },
	  std::byte{ 0x34 }, std::byte{ 0xca }, std::byte{ 0x72 }, std::byte{ 0xa7 }, std::byte{ 0x19 },
	  std::byte{ 0x4a }, std::byte{ 0x28 }, std::byte{ 0x67 }, std::byte{ 0xb6 }, std::byte{ 0x42 },
	  std::byte{ 0x95 }}},
	{ "Secret",
	  { std::byte{ 0x04 }, std::byte{ 0xd4 }, std::byte{ 0x6b }, std::byte{ 0x05 }, std::byte{ 0x3c },
		std::byte{ 0xa8 }, std::byte{ 0x7b }, std::byte{ 0x59 }, std::byte{ 0x41 }, std::byte{ 0x72 },
		std::byte{ 0x30 }, std::byte{ 0x2a }, std::byte{ 0xec }, std::byte{ 0x9b }, std::byte{ 0xb9 },
		std::byte{ 0x92 }}},
	{ "Peruvian",
	  { std::byte{ 0xbd }, std::byte{ 0x74 }, std::byte{ 0x54 }, std::byte{ 0x9d }, std::byte{ 0x0f },
		std::byte{ 0x98 }, std::byte{ 0xc3 }, std::byte{ 0xe0 }, std::byte{ 0x53 }, std::byte{ 0xa2 },
		std::byte{ 0x5b }, std::byte{ 0x83 }, std::byte{ 0xcb }, std::byte{ 0x99 }, std::byte{ 0xc2 },
		std::byte{ 0x99 }}},
	{ "Archaeopteryx",
	  { std::byte{ 0x34 }, std::byte{ 0x85 }, std::byte{ 0xaf }, std::byte{ 0xaf }, std::byte{ 0x55 },
		std::byte{ 0x86 }, std::byte{ 0x4d }, std::byte{ 0x15 }, std::byte{ 0xd3 }, std::byte{ 0x17 },
		std::byte{ 0xd6 }, std::byte{ 0x8b }, std::byte{ 0xb1 }, std::byte{ 0xe4 }, std::byte{ 0xfe },
		std::byte{ 0xad }}},
	{ "hippopotomonstrosesquippedaliophobia",
	  { std::byte{ 0xf6 }, std::byte{ 0x57 }, std::byte{ 0x78 }, std::byte{ 0x66 }, std::byte{ 0x99 },
		std::byte{ 0xac }, std::byte{ 0x4c }, std::byte{ 0x00 }, std::byte{ 0xa1 }, std::byte{ 0x25 },
		std::byte{ 0x59 }, std::byte{ 0xe8 }, std::byte{ 0x4e }, std::byte{ 0x5f }, std::byte{ 0x67 },
		std::byte{ 0x2e }}}};

int main()
{
	const std::byte             zeros[16] = {};
	std::array< std::byte, 16 > gen;

	for (const auto& i : KAT)
	{
		ARC4 encrypter(i.key);

		encrypter.transform(zeros, 16, gen.data(), 16);

		assert_equal(gen, i.sbytes);
	}

	std::cout << "Tests OK" << std::endl;
}
