#include <array>
#include <cstring>

#include <list>

#include "adler/adler32.h"
#include "unit/unit.h"

int main()
{
	const char empty[1] = {};
	const char abcd[4]  = { 'a', 'b', 'c', 'd' };

	std::list< char > derp = { 'a', 'b', 'c', 'd' };

	assert_equal(adler32(empty, 0), 1u);
	assert_equal(adler32(abcd), 0x03d8018bu);
	assert_equal(adler32(derp), 0x03d8018bu);
	std::cout << "ADLER32 tests ok" << std::endl;
}
