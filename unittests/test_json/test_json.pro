include(../../vars.pri)

TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt c++11 c++1z c++14 c++17 c++2a

SOURCES += \
        main.cpp

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../../jlee/release/ -ljlee
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../../jlee/debug/ -ljlee
else:unix: LIBS += -L$$OUT_PWD/../../jlee/ -ljlee -lsimdjson

INCLUDEPATH += $$PWD/../../jlee
DEPENDPATH += $$PWD/../../jlee

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../jlee/release/libjlee.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../jlee/debug/libjlee.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../jlee/release/jlee.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../../jlee/debug/jlee.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../../jlee/libjlee.a
