#include <chrono>
#include <iostream>
#include <list>
#include <sstream>

#include "memorymap/mmap.h"
#include "unit/unit.h"
#include "json/json.h"

// nlohmann json for comparison
#include "json.hpp"

// Rapidjson
#include <rapidjson/reader.h>

#include <simdjson.h>

const std::list< std::string > good = { "[true, true, false,[ true,null],null, 22]",
	                                    "\"\xc2\xa2\"",
	                                    "\"\xe2\x82\xac\"",
	                                    "\"\xf0\x90\x8d\x88\"",
	                                    "-0.0",
	                                    "{ \"hello\" : \"world\" }",
	                                    "true",
	                                    "false",
	                                    "null",
	                                    "[]",
	                                    "{}",
	                                    "0",
	                                    "-0",
	                                    "1234567890",
	                                    "-1234567890",
	                                    "0.123456789",
	                                    "-0.987654321",
	                                    "5.2e+123456789",
	                                    "-5.2e+123456789",
	                                    "2.718e0123456789",
	                                    "-2.718e0123456789",
	                                    "3.1e-987654321",
	                                    "-3.1e-987654321",
	                                    "0e123456789",
	                                    "0e000",
	                                    "\"\\u002f\"",
	                                    "\"\\u002F\"",
	                                    "\"\\/\"",
	                                    "\"/\"",
	                                    "\"\\ud834\\udd1e\"",
	                                    "\"\x24\"",
	                                    "[1,2]" };

const std::list< std::string > bad = {
	// Total junk
	"lj",

	// Bad literals
	"tru",
	"truetrue", // literal cannot be followed by more text
	"falSE",    // literal must be lowercase
	"nul",      // incomplete

	// Bad numbers
	"03",    // no superfluous leading zeros
	"+941",  // only negative sign is allowed
	"0..32", // two decimals
	"123&",  // catch signed arithmetic in parsing number
	"3.1.4", // two decimals
	"1.24e", // incomplete floating point
	"1.50E", "1.282e+", "1.29024e-", "1.2482E+", "1014.E-", "3.5ea", ".38", "-a", "3.",

	// Bad strings
	"\"\x00\x01\x02\x03\x04\0x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f\"", // control characters must be escaped
	"\"\x10\x11\x12\x13\x14\0x15\x16\x17\x18\x19\x1a\x1b\x1c\x1d\x1e\x1f\"", // control characters must be escaped
	"\"\xf0\x90\x8d",                                                        // missing byte of multibyte encoding
	"\"\xf0\x90\x8d\xff\"",                                                  // illegal byte in multibyte encoding
	"\"\\\""                                                                 // Incomplete escape sequence
	"\"\\udd1e\\ud834",                                                      // Surrogate pair is ordered incorrectly
	"\"\\ud834\\ue000\"",                                                    // Second part of surrogate pair is invalid
	"\"\\ud834\"",                                                           // Missing second half of surrogate pair
	"\"\\u002\"",                                                            // Partial unicode
	"\"\\u002",                                                              // Partial unicode
	"\"\\u002z\"",                                                           // Non-hex digit in escape
	"\"\\a\\b\\c\\d\\e\\f\\g\"",                                             // Invalid escape sequence

	// Bad object
	"{ \"field1\":3, }",           // incomplete
	"{ \"field1\" 3 }",            // missing colon
	"{ \"field\": }",              // missing value
	"{ \"field\": 6",              // incomplete
	"{ \"field\": 6 \"lol\": 7 }", // missing field separator
	"{ \"fiel",                    // incomplete field name
	"{,\"a\":\"b\"}", "{\"a\":\"b\",,\"c\":42}",

	// Bad arrays
	"[", "[ 1", "[ 24, ]", "[ 1 1]", "[,4]", "[1,,2]" };

namespace rapidjson
{
struct MyHandler
{
	bool Null()
	{
		return true;
	}

	bool Bool(bool b)
	{
		return true;
	}

	bool Int(int i)
	{
		return true;
	}

	bool Uint(unsigned u)
	{
		return true;
	}

	bool Int64(int64_t i)
	{
		return true;
	}

	bool Uint64(uint64_t u)
	{
		return true;
	}

	bool Double(double d)
	{
		return true;
	}

	bool RawNumber(
		const char* str,
		SizeType    length,
		bool        copy
	)
	{
		return true;
	}

	bool String(
		const char* str,
		SizeType    length,
		bool        copy
	)
	{
		return true;
	}

	bool StartObject()
	{
		return true;
	}

	bool Key(
		const char* str,
		SizeType    length,
		bool        copy
	)
	{
		return true;
	}

	bool EndObject(SizeType memberCount)
	{
		return true;
	}

	bool StartArray()
	{
		return true;
	}

	bool EndArray(SizeType elementCount)
	{
		return true;
	}

};
}

int main()
{
	#if 0
	assert_all_true(good, static_cast< bool ( * ) (const std::string&) >( jl::json::validate ) );
	assert_all_false(bad, static_cast< bool ( * ) (const std::string&) >( jl::json::validate ) );
	std::cout << "Tests ok" << std::endl;

	#else
	// const std::string filepath("/home/jonathan/source/nativejson-benchmark/data/canada.json");
	const std::string filepath("//home/jonathan/source/3rdparty/test-data/large-file.json");

	const memory_map m(filepath);
	char*            t = new char [m.size() + 32];

	for (std::size_t i = 0; i < m.size(); ++i)
	{
		t[i] = static_cast< unsigned char >( m[i] );
	}

	t[m.size()] = 0;

	for (std::size_t trial = 0; trial < 100; ++trial)
	{
		// My json
		{
			const auto t0  = std::chrono::high_resolution_clock::now();
			const bool res = jl::json::validate(reinterpret_cast< const std::byte* >( t ), m.size() );
			const auto t1  = std::chrono::high_resolution_clock::now();

			if ( res )
			{
				std::cout << "jlee: "
				          << std::chrono::duration_cast< std::chrono::duration< double > >(t1 - t0).count()
				          << std::endl;
			}
			else
			{
				std::cout << "jlee ERROR: "
				          << std::chrono::duration_cast< std::chrono::duration< double > >(t1 - t0).count()
				          << std::endl;
			}
		}
	}

	#if 1
	// SIMDJSON
	{
		simdjson::dom::parser parser;

		const auto t0  = std::chrono::high_resolution_clock::now();
		auto       res = parser.parse(t, m.size(), false);
		const auto t1  = std::chrono::high_resolution_clock::now();

		if ( res.error() == simdjson::SUCCESS )
		{
			std::cout << "simdjson: "
			          << std::chrono::duration_cast< std::chrono::duration< double > >(t1 - t0).count()
			          << std::endl;
		}
		else
		{
			std::cout << "simdjson ERROR: "
			          << std::chrono::duration_cast< std::chrono::duration< double > >(t1 - t0).count()
			          << std::endl;
		}
	}

	// Nlohmann
	{
		const auto t0  = std::chrono::high_resolution_clock::now();
		const bool res = nlohmann::json::accept(t);
		const auto t1  = std::chrono::high_resolution_clock::now();

		if ( res )
		{
			std::cout << "nlohmann: "
			          << std::chrono::duration_cast< std::chrono::duration< double > >(t1 - t0).count()
			          << std::endl;
		}
		else
		{
			std::cout << "nlohmann ERROR: "
			          << std::chrono::duration_cast< std::chrono::duration< double > >(t1 - t0).count()
			          << std::endl;
		}
	}

	// RapidJSON
	{
		rapidjson::MyHandler    handler;
		rapidjson::Reader       reader;
		rapidjson::StringStream ss(t);
		const auto              t0  = std::chrono::high_resolution_clock::now();
		const auto              res = reader.Parse< rapidjson::kParseFullPrecisionFlag >(ss, handler);
		const auto              t1  = std::chrono::high_resolution_clock::now();

		if ( !res.IsError() )
		{
			std::cout << "rapidjson: "
			          << std::chrono::duration_cast< std::chrono::duration< double > >(t1 - t0).count()
			          << std::endl;
		}
		else
		{
			std::cout << "rapidjson ERROR: "
			          << std::chrono::duration_cast< std::chrono::duration< double > >(t1 - t0).count()
			          << std::endl;
		}
	}
	#endif // if 1
	#endif // if 0
}
