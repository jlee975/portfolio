TEMPLATE = subdirs

SUBDIRS += \
    test_adler32 \
    test_arcfour \
    test_bigint \
    test_collision   \
    test_delaunay \
    test_des    \
    test_flat \
    test_index_type  \
    test_interval_set \
    test_multifor  \
    test_parse \
    test_nfa \
    test_quaternion \
    test_serpent  \
    test_tableau\
    test_base64  \
    test_bits    \
    test_dct      \
    test_gzip  \
    test_json   \
    test_point   \
    test_rijndael \
    test_sha256 \
    test_serialization
