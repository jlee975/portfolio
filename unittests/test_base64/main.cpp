#include "base64/base64.h"
#include "ravengz.h"
#include "ravengz64.h"
#include "unit/unit.h"

int main()
{
	std::byte buf[sizeof( raven_gz )];

	const std::size_t n = Base64Decode(ravengz64, sizeof( ravengz64 ), buf, sizeof( buf ) );

	assert_equal(n, sizeof( raven_gz ) );

	for (std::size_t i = 0; i < n; ++i)
	{
		assert_equal(static_cast< unsigned char >( buf[i] ), raven_gz[i]);
	}

	std::cout << "Tests OK" << std::endl;
}
