#include <iostream>
#include <string>

#include "parse/parse.h"

#include "parse/ebnf.h"

#include "parse/ebnf_private.h"

using namespace std::string_literals;

using namespace details;

namespace details
{
// Defined in ebnf.cpp
const jl::grammar::context_free_grammar< char32_t, ebnfnt >& ebnfgrammar();
}

namespace
{
enum rules
{
	A,
	B
};

template< typename T, typename N, typename S >
void recognize(
	jl::grammar::context_free_grammar< T, N > g,
	const S&                                  s,
	N                                         start
)
{
	if ( const auto d = parse(s, start, g);d.empty() )
	{
		std::cerr << s << std::endl;
		throw std::runtime_error("Test was not parsed");
	}
}

template< typename T, typename N, typename S >
void reject(
	jl::grammar::context_free_grammar< T, N > g,
	const S&                                  s,
	N                                         start
)
{
	if ( const auto d = parse(s, start, g);!d.empty() )
	{
		throw std::runtime_error("Test was parsed, but should not have been");
	}
}

void single_non_terminal()
{
	const jl::grammar::context_free_grammar< char, rules > g = {{ A, {{ "'A'"s }}}};

	recognize< char, rules >(g, "A"s, A);
	reject< char, rules >(g, "B"s, A);
}

void literal()
{
	const jl::grammar::context_free_grammar< char, rules > g = {{ A, {{ "'ABCD'"s }}}};

	recognize< char, rules >(g, "ABCD"s, A);
	reject< char, rules >(g, "ABC"s, A);
	reject< char, rules >(g, "ABCDE"s, A);
}

void character_class()
{
	const jl::grammar::context_free_grammar< char, rules > g = {{ A, {{ "[DE]"s }}}};

	recognize< char, rules >(g, "D"s, A);
	recognize< char, rules >(g, "E"s, A);
	reject< char, rules >(g, "F"s, A);
}

void optional()
{
	const jl::grammar::context_free_grammar< char, rules > g = {{ A, {{{ "'F'"s, '?' }}}}};

	recognize< char, rules >(g, "F"s, A);
	recognize< char, rules >(g, ""s, A);
	reject< char, rules >(g, "B"s, A);
}

void kleene_plus()
{
	const jl::grammar::context_free_grammar< char, rules > g = {{ A, {{{ "'G'"s, '+' }}}}};

	recognize< char, rules >(g, "G"s, A);
	recognize< char, rules >(g, "GGGG"s, A);
	reject< char, rules >(g, "B"s, A);
	reject< char, rules >(g, ""s, A);
}

void kleene_star()
{
	const jl::grammar::context_free_grammar< char, rules > g = {{ A, {{{ "'H'"s, '*' }}}}};

	recognize< char, rules >(g, ""s, A);
	recognize< char, rules >(g, "H"s, A);
	recognize< char, rules >(g, "HHHH"s, A);
	reject< char, rules >(g, "B, A"s, A);
}

void tail_recursion()
{
	const jl::grammar::context_free_grammar< char, rules > g = {{ A, {{ "'A'"s }, { "'A'"s, A }}}};

	recognize< char, rules >(g, "AAAA"s, A);
}

void head_recursion()
{
	recognize< char, rules >({{ A, {{ B }, { A, B }}}, { B, {{ "'A'"s }}}}, "AAAA"s, A);
}

void two_rules()
{
	const jl::grammar::context_free_grammar< char, rules > g
	    = {{ A, {{{ B, '+' }}}}, { B, {{ "'I'"s }, { "'J'"s }}}};

	recognize< char, rules >(g, "I"s, A);
	recognize< char, rules >(g, "J"s, A);
	recognize< char, rules >(g, "IIJIIJIJJJIJI"s, A);
	reject< char, rules >(g, "IJJIJKIJIK"s, A);
}

void test_nested()
{
	jl::grammar::context_free_grammar< char, rules > g(
		{{ A, {{ "'{'"s, B, "'}'"s }}}, { B, {{ A }, { "'B'"s }}}}, {{{ '{' }, { '}' }}});

	const std::string s = "{{{B}}}";

	if ( const auto d = parse(s, A, g);d.empty() )
	{
		throw std::runtime_error("Test was not parsed: '" + s + "'");
	}
}

void test_ebnf_any()
{
	const jl::grammar::context_free_grammar< char32_t, ebnfnt > g = {
		{ ANY, {{ ustring("[#x0-#x10ffff]") }}}, };

	recognize(g, ustring("\x0", 1), ANY);
	recognize(g, ustring("\x1", 1), ANY);
	recognize(g, ustring("a"), ANY);

	reject(g, ustring(""), ANY);
	reject(g, ustring("ab"), ANY);
}

void test_ebnf_escape()
{
	const jl::grammar::context_free_grammar< char32_t, ebnfnt > g = {
		{ ESCAPECHAR, {{ ustring("[\\#\\\\\\-\\]\\^]") }}}, };

	recognize(g, ustring("#", 1), ESCAPECHAR);
	recognize(g, ustring("\\", 1), ESCAPECHAR);
	recognize(g, ustring("-", 1), ESCAPECHAR);
	recognize(g, ustring("]", 1), ESCAPECHAR);
	recognize(g, ustring("^", 1), ESCAPECHAR);

	reject(g, ustring("a", 1), ESCAPECHAR);
}

void test_ebnf_space()
{
	const jl::grammar::context_free_grammar< char32_t, ebnfnt > g = {
		{ S, {{ ustring("[#x9#xA#xD#x20]") }}},
		{ S0, {{{ S, '*' }}}},
		{ S1, {{ S, S0 }}}, };

	recognize(g, ustring("\x9"), S);
	recognize(g, ustring("\xa"), S);
	recognize(g, ustring("\xd"), S);
	recognize(g, ustring("\x20"), S);

	recognize(g, ustring(""), S0);
	recognize(g, ustring("\x9"), S0);
	recognize(g, ustring("\xa"), S0);
	recognize(g, ustring("\xd"), S0);
	recognize(g, ustring("\x20"), S0);

	recognize(g, ustring("\x9"), S1);
	recognize(g, ustring("\xa"), S1);
	recognize(g, ustring("\xd"), S1);
	recognize(g, ustring("\x20"), S1);

	recognize(g, ustring("\x9\x9"), S0);
	recognize(g, ustring("\xa\xa"), S0);
	recognize(g, ustring("\xd\xd"), S0);
	recognize(g, ustring("\x20\x20"), S0);

	recognize(g, ustring("\x9\x9"), S1);
	recognize(g, ustring("\xa\xa"), S1);
	recognize(g, ustring("\xd\xd"), S1);
	recognize(g, ustring("\x20\x20"), S1);

	reject(g, ustring("a"), S);
	reject(g, ustring(""), S1);
}

void test_ebnf_symbol()
{
	const jl::grammar::context_free_grammar< char32_t, ebnfnt > g = {
		{ SYMBOL, {{ ustring("[A-Za-z_]"), { ustring("[A-Za-z_0-9]"), '*' }}}}, };

	recognize(g, ustring("A"), SYMBOL);
	recognize(g, ustring("ABCD_123"), SYMBOL);
	reject(g, ustring(), SYMBOL);
	reject(g, ustring("123456"), SYMBOL);
}

void test_ebnf_hex()
{
	const jl::grammar::context_free_grammar< char32_t, ebnfnt > g = {
		{ HEX, {{ ustring("'#x'"), { ustring("[0-9A-Fa-f]"), '+' }}}}, };

	recognize(g, ustring("#x00"), HEX);
	recognize(g, ustring("#xFFFF"), HEX);
	reject(g, ustring("#x"), HEX);
	reject(g, ustring("FFFF"), HEX);
}

void test_ebnf_literal()
{
	const jl::grammar::context_free_grammar< char32_t, ebnfnt > g = {
		{ LITERAL,
		  {{ ustring("'\"'"), { ustring("[^\"]"), '*' }, ustring("'\"'") },
			  { ustring("'\\''"), { ustring("[^']"), '*' }, ustring("'\\''") }}}, };

	recognize(g, ustring("'ABCD'"), LITERAL);
	recognize(g, ustring("\"ABCD\""), LITERAL);
	recognize(g, ustring("''"), LITERAL);
	recognize(g, ustring("\"\""), LITERAL);
	recognize(g, ustring("'\"'"), LITERAL);
	recognize(g, ustring("\"'\""), LITERAL);
	reject(g, ustring("\"'"), LITERAL);
	reject(g, ustring("'\""), LITERAL);
	reject(g, ustring("[ABCD]"), LITERAL);
	reject(g, ustring("XYZ"), LITERAL);
}

void test_ebnf_escape_seq()
{
	const jl::grammar::context_free_grammar< char32_t, ebnfnt > g = {
		{ ESCAPECHAR, {{ ustring("[\\#\\\\\\-\\]\\^]") }}},
		{ ESCAPESEQ, {{ ustring("'\\\\'"), ESCAPECHAR }}}, };

	recognize(g, ustring("\\#"), ESCAPESEQ);
	recognize(g, ustring("\\\\"), ESCAPESEQ);
	recognize(g, ustring("\\-"), ESCAPESEQ);
	recognize(g, ustring("\\]"), ESCAPESEQ);
	recognize(g, ustring("\\^"), ESCAPESEQ);

	reject(g, ustring("#"), ESCAPESEQ);
	reject(g, ustring("\\"), ESCAPESEQ);
	reject(g, ustring("-"), ESCAPESEQ);
	reject(g, ustring("]"), ESCAPESEQ);
	reject(g, ustring("^"), ESCAPESEQ);
}

void test_ebnf_range_char()
{
	const jl::grammar::context_free_grammar< char32_t, ebnfnt > g = {
		{ ESCAPECHAR, {{ ustring("[\\#\\\\\\-\\]\\^]") }}},
		{ ESCAPESEQ, {{ ustring("'\\\\'"), ESCAPECHAR }}},
		{ HEX, {{ ustring("'#x'"), { ustring("[0-9A-Fa-f]"), '+' }}}},
		{ RANGECHAR,
		  {{ ustring("[#x0-#x22#x24-#x2c#x2e-#x5b#x5f-#x10ffff]") },
			  { HEX },
			  { ESCAPESEQ }}}, // anything but #, -, \, ], or ^
	};

	recognize(g, ustring(" "), RANGECHAR);

	reject(g, ustring("#"), RANGECHAR);
	reject(g, ustring("\\"), RANGECHAR);
	reject(g, ustring("-"), RANGECHAR);
	reject(g, ustring("]"), RANGECHAR);
	reject(g, ustring("^"), RANGECHAR);
}

void test_ebnf_range()
{
	const jl::grammar::context_free_grammar< char32_t, ebnfnt > g = {
		{ ESCAPECHAR, {{ ustring("[\\#\\\\\\-\\]\\^]") }}},
		{ ESCAPESEQ, {{ ustring("'\\\\'"), ESCAPECHAR }}},
		{ HEX, {{ ustring("'#x'"), { ustring("[0-9A-Fa-f]"), '+' }}}},
		{ RANGECHAR,
		  {{ ustring("[#x0-#x22#x24-#x2c#x2e-#x5b#x5f-#x10ffff]") },
			  { HEX },
			  { ESCAPESEQ }}}, // anything but #, -, \, ], or ^
		{ RANGE, {{ RANGECHAR }, { RANGECHAR, ustring("'-'"), RANGECHAR }}}, };

	recognize(g, ustring("A-Z"), RANGE);
	recognize(g, ustring("0-9"), RANGE);
	recognize(g, ustring("#x0-#x22"), RANGE);

	reject(g, ustring("#"), RANGECHAR);
	reject(g, ustring("\\"), RANGECHAR);
	reject(g, ustring("-"), RANGECHAR);
	reject(g, ustring("]"), RANGECHAR);
	reject(g, ustring("^"), RANGECHAR);
}

void test_ebnf_character_class()
{
	const jl::grammar::context_free_grammar< char32_t, ebnfnt > g = {
		{ ESCAPECHAR, {{ ustring("[\\#\\\\\\-\\]\\^]") }}},
		{ ESCAPESEQ, {{ ustring("'\\\\'"), ESCAPECHAR }}},
		{ HEX, {{ ustring("'#x'"), { ustring("[0-9A-Fa-f]"), '+' }}}},
		{ RANGECHAR,
		  {{ ustring("[#x0-#x22#x24-#x2c#x2e-#x5b#x5f-#x10ffff]") },
			  { HEX },
			  { ESCAPESEQ }}}, // anything but #, -, \, ], or ^
		{ RANGE, {{ RANGECHAR }, { RANGECHAR, ustring("'-'"), RANGECHAR }}},
		{ CHARACTER_CLASS, {{ ustring("'['"), { ustring("'^'"), '?' }, { RANGE, '+' }, ustring("']'") }}}, };

	recognize(g, ustring("[ABC]"), CHARACTER_CLASS);
	recognize(g, ustring("[A-Z]"), CHARACTER_CLASS);
	recognize(g, ustring("[A-Za-z_]"), CHARACTER_CLASS);
	recognize(g, ustring("[^0-9]"), CHARACTER_CLASS);

	recognize(g, ustring("[\\#]"), CHARACTER_CLASS);
	recognize(g, ustring("[\\\\]"), CHARACTER_CLASS);
	recognize(g, ustring("[\\-]"), CHARACTER_CLASS);
	recognize(g, ustring("[\\]]"), CHARACTER_CLASS);
	recognize(g, ustring("[\\^]"), CHARACTER_CLASS);

	reject(g, ustring("[AB"), CHARACTER_CLASS);
	reject(g, ustring("AB]"), CHARACTER_CLASS);
	reject(g, ustring("[]]"), CHARACTER_CLASS);
	reject(g, ustring("[#]"), CHARACTER_CLASS);
	reject(g, ustring("[-]"), CHARACTER_CLASS);
	reject(g, ustring("[\\]"), CHARACTER_CLASS);
	reject(g, ustring("[^]"), CHARACTER_CLASS);
}

void test_ebnf_rule()
{
	const jl::grammar::context_free_grammar< char32_t, ebnfnt >& g = ebnfgrammar();

	const ustring s("X ::= 'a'?'a'");

	std::vector< bool > keep;

	for (const auto x :
	     { RULE, SYMBOL, ALT, CAT, SUB, STAR, PLUS, OPTIONAL, LITERAL, HEX, CHARACTER_CLASS, RANGE, RANGECHAR }
	)
	{
		keep.resize(x + 1);
		keep.at(x) = true;
	}

	const auto d = parse(s, RULE, g, keep);

	if ( d.child_count() != 1 )
	{
		throw std::runtime_error("Derivation tree is not as expected");
	}

	const auto root = d.child(0);

	if ( const auto& x = d.at(root);x.rule != RULE || x.offset != 0 || x.end - x.offset != 13 )
	{
		throw std::runtime_error("Derivation tree is not as expected");
	}

	if ( d.child_count(root) != 6 )
	{
		throw std::runtime_error("Derivation tree is not as expected");
	}

	if ( const auto k = d.child(root, 0);d.child_count(k) != 0 )
	{
		throw std::runtime_error("Derivation tree is not as expected");
	}

	if ( const auto& x = d.at(d.child(root, 0) );x.rule != S0 || x.offset != 0 || x.end - x.offset != 0 )
	{
		throw std::runtime_error("Derivation tree is not as expected");
	}

	if ( const auto& x = d.at(d.child(root, 1) );x.rule != SYMBOL || x.offset != 0 || x.end - x.offset != 1 )
	{
		throw std::runtime_error("Derivation tree is not as expected");
	}

	if ( const auto& x = d.at(d.child(root, 2) );x.rule != S0 || x.offset != 1 || x.end - x.offset != 1 )
	{
		throw std::runtime_error("Derivation tree is not as expected");
	}

	if ( const auto& x = d.at(d.child(root, 3) );x.rule != S0 || x.offset != 5 || x.end - x.offset != 1 )
	{
		throw std::runtime_error("Derivation tree is not as expected");
	}

	if ( const auto k = d.child(root, 4);d.child_count(k) != 1 )
	{
		throw std::runtime_error("Derivation tree is not as expected");
	}
	else
	{
		const auto k1 = d.child(k, 0);

		if ( const auto& x = d.at(k1);x.rule != CAT || x.offset != 6 || x.end - x.offset != 7 )
		{
			throw std::runtime_error("Derivation tree is not as expected");
		}
		else
		{
			if ( d.child_count(k1) != 3 )
			{
				throw std::runtime_error("Derivation tree is not as expected "
					+ std::to_string(d.child_count(k1) ) );
			}
		}
	}

	if ( const auto& x = d.at(d.child(root, 4) );x.rule != EXPRESSION || x.offset != 6 || x.end - x.offset != 7 )
	{
		throw std::runtime_error("Derivation tree is not as expected");
	}

	if ( const auto k = d.child(root, 5);d.child_count(k) != 0 )
	{
		throw std::runtime_error("Derivation tree is not as expected");
	}

	if ( const auto& x = d.at(d.child(root, 5) );x.rule != S0 || x.offset != 13 || x.end - x.offset != 0 )
	{
		throw std::runtime_error("Derivation tree is not as expected");
	}
}

void test_ebnf()
{
	test_ebnf_any();
	test_ebnf_escape();
	test_ebnf_space();
	test_ebnf_symbol();
	test_ebnf_hex();
	test_ebnf_literal();
	test_ebnf_escape_seq();
	test_ebnf_range_char();
	test_ebnf_range();
	test_ebnf_character_class();
	test_ebnf_rule();
}

void test_pathological_parse()
{
	// https://swtch.com/~rsc/regexp/regexp1.html
	for (unsigned i = 1; i < 100; ++i)
	{
		ustring r = "rule ::= ";

		for (unsigned j = 0; j < i; ++j)
		{
			r += "'a'?";
		}

		for (unsigned j = 0; j < i; ++j)
		{
			r += "'a'";
		}

		const auto [cfg, map] = ebnf_to_cfg({ r });

		ustring s(i, 'a');

		std::cout << i << " Matching " << s << std::endl;

		const derivation d = parse(s, map.at("rule"), cfg);

		if ( d.empty() )
		{
			std::cout << "Parse did not work" << std::endl;
		}
		else
		{
			std::cout << "Parsing worked" << std::endl;
		}
	}
}

}

int main()
{
	test_nested();
	{
		const jl::grammar::context_free_grammar< char, rules > g
		    = {{ A, {{ B, B, "'A'"s, B, B }}}, { B, {{ "'B'"s }}}};
		recognize< char, rules >(g, "BBABB"s, A);
	}

	optional();
	single_non_terminal();
	literal();
	character_class();
	tail_recursion();
	head_recursion();
	kleene_plus();
	kleene_star();
	two_rules();
	test_ebnf();
	test_pathological_parse();
	std::cout << "Tests ok" << std::endl;
	return 0;
}
