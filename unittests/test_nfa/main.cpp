#include "flat/flatmap.h"
#include "parse/nfaparser.h"

#include "unit/unit.h"

using E = NondeterministicFiniteAutomaton::E;

namespace
{
enum
{
	INT0,
	INT1,
	ZERO0,
	ZERO1,
	POSITIVE0,
	POSITIVE1,
	DIGITS0,
	DIGITS1
};

// nonnegative ::= 0 | [1-9][0-9]*
// INT0 ZERO0 0 ZERO1 POSITIVE0 [1-9] DIGITS0 [0-9]* DIGITS1 POSITIVE1 INT1
const NondeterministicFiniteAutomaton nonnegative_grammar = {{ INT0, ZERO0 },
	{ ZERO0, ZERO1, std::string("0") },
	{ ZERO1, INT1 },
	{ INT0, POSITIVE0 },
	{ POSITIVE0, DIGITS0, alt{ "123456789" }},
	{ DIGITS0, DIGITS0, alt{ "0123456789" }},
	{ DIGITS0, DIGITS1 },
	{ DIGITS1, POSITIVE1 },
	{ POSITIVE1, INT1 }};

enum
{
	AB0,
	AB1,
	AB2
};

const NondeterministicFiniteAutomaton abetc = {{ AB0, AB1, std::string("ab") }, { AB1, AB0 }, { AB1, AB2 }};

bool is_number(const std::string_view& v)
{
	nfaparser p(nonnegative_grammar);

	return p.validate(v, INT0);
}

bool is_ab(const std::string_view& v)
{
	nfaparser p(abetc);

	return p.validate(v, AB0);
}

bool is_pathologic(const std::string_view& v)
{
	using Edge = NondeterministicFiniteAutomaton::edge;

	std::vector< Edge > g;

	const int n = 30;

	for (int i = 0; i < n; ++i)
	{
		g.push_back(Edge{ i, i + 1 });
		g.push_back(Edge{ i, i + 1, std::string("a") });
	}

	g.push_back(Edge{ n, n + 1, std::string(n, 'a') });

	NondeterministicFiniteAutomaton t(g);

	nfaparser p(t);

	return p.validate(v, 0);
}

}

int main()
{
	assert_true(is_pathologic("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa") );

	assert_true(is_number("0") );
	assert_true(is_number("3141592") );
	assert_false(is_number("01") );
	assert_false(is_number("") );
	assert_false(is_number("23b90") );
	assert_true(is_ab("ab") );
	assert_true(is_ab("ababab") );
	assert_false(is_ab("abba") );
	assert_false(is_ab("aba") );
	assert_false(is_ab("") );

	std::cout << "Everything is ok" << std::endl;
	return EXIT_SUCCESS;
}
