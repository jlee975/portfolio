#include "unit/unit.h"

#include <iostream>
#include <random>

#include "math/geometry/primitives/point.h"
#include "math/geometry/print.h"
#include "math/linear_algebra/euclidean.h"

void test_dotproduct()
{
	// These vectors are nearly perpendicular. Calculating the dot product naively with floats
	// yields 93, while long doubles yields -25.4238. The actual value is -25.423828125
	// x1 = { -38337.96875, 53778.390625, 53433.703125 }
	const float x1[3] = { -0x1.2b83fp+15, 0x1.a424c8p+15, 0x1.a17368p+15 };
	// y1 = { 60239.34375, 42696.515625, 248.984375 }
	const float       y1[3] = { 0x1.d69ebp+15, 0x1.4d9108p+15, 0x1.f1f8p+7 };
	const long double x2[3] = { -0x9.5c1f8p+12, 0xd.21264p+12, 0xd.0b9b4p+12 };
	const long double y2[3] = { 0xe.b4f58p+12, 0xa.6c884p+12, 0xf.8fcp+4 };

	const float       z1 = la::inner(x1, y1);
	const long double z2 = la::inner(x2, y2);

	std::cout << z1 << " ? " << z2 << std::endl;

	if ( ( z1 < 0 && z2 >= 0 ) || ( z1 > 0 && z2 <= 0 ) )
	{
		std::cerr << "floats:" << std::endl;
		std::cerr << std::defaultfloat;
		std::cerr << x1[0] << ", " << x1[1] << ", " << x1[2] << std::endl;
		std::cerr << std::hexfloat;
		std::cerr << x1[0] << ", " << x1[1] << ", " << x1[2] << std::endl;
		std::cerr << std::defaultfloat;
		std::cerr << y1[0] << ", " << y1[1] << ", " << y1[2] << std::endl;
		std::cerr << std::hexfloat;
		std::cerr << y1[0] << ", " << y1[1] << ", " << y1[2] << std::endl;
		std::cerr << std::defaultfloat;
		std::cerr << z1 << std::endl;
		std::cerr << std::hexfloat;
		std::cerr << z1 << std::endl;

		std::cerr << "long doubles:" << std::endl;
		std::cerr << std::defaultfloat;
		std::cerr << x2[0] << ", " << x2[1] << ", " << x2[2] << std::endl;
		std::cerr << std::hexfloat;
		std::cerr << x2[0] << ", " << x2[1] << ", " << x2[2] << std::endl;
		std::cerr << std::defaultfloat;
		std::cerr << y2[0] << ", " << y2[1] << ", " << y2[2] << std::endl;
		std::cerr << std::hexfloat;
		std::cerr << y2[0] << ", " << y2[1] << ", " << y2[2] << std::endl;
		std::cerr << std::defaultfloat;
		std::cerr << z2 << std::endl;
		std::cerr << std::hexfloat;
		std::cerr << z2 << std::endl;
		throw std::runtime_error("Sign of product changed due to floating point precision");
	}

	#if 0
	// Using this block to find test values
	{
		std::random_device                      dev;
		std::default_random_engine              g( dev() );
		std::uniform_real_distribution< float > dist(-100000, 100000);

		for (std::size_t i = 0; i < -1; ++i)
		{
			const float       x1[3] = { dist(g), dist(g), dist(g) };
			const float       y1[3] = { dist(g), dist(g), dist(g) };
			const long double x2[3] = { x1[0], x1[1], x1[2] };
			const long double y2[3] = { y1[0], y1[1], y1[2] };

			const float       z1 = la::inner(x1, y1);
			const long double z2 = la::inner(x2, y2);

			if ( ( z1 < 0 && z2 >= 0 ) || ( z1 > 0 && z2 <= 0 ) )
			{
				std::cerr << "floats:" << std::endl;
				std::cerr << std::defaultfloat;
				std::cerr << x1[0] << ", " << x1[1] << ", " << x1[2] << std::endl;
				std::cerr << std::hexfloat;
				std::cerr << x1[0] << ", " << x1[1] << ", " << x1[2] << std::endl;
				std::cerr << std::defaultfloat;
				std::cerr << y1[0] << ", " << y1[1] << ", " << y1[2] << std::endl;
				std::cerr << std::hexfloat;
				std::cerr << y1[0] << ", " << y1[1] << ", " << y1[2] << std::endl;
				std::cerr << std::defaultfloat;
				std::cerr << z1 << std::endl;
				std::cerr << std::hexfloat;
				std::cerr << z1 << std::endl;

				std::cerr << "long doubles:" << std::endl;
				std::cerr << std::defaultfloat;
				std::cerr << x2[0] << ", " << x2[1] << ", " << x2[2] << std::endl;
				std::cerr << std::hexfloat;
				std::cerr << x2[0] << ", " << x2[1] << ", " << x2[2] << std::endl;
				std::cerr << std::defaultfloat;
				std::cerr << y2[0] << ", " << y2[1] << ", " << y2[2] << std::endl;
				std::cerr << std::hexfloat;
				std::cerr << y2[0] << ", " << y2[1] << ", " << y2[2] << std::endl;
				std::cerr << std::defaultfloat;
				std::cerr << z2 << std::endl;
				std::cerr << std::hexfloat;
				std::cerr << z2 << std::endl;
				throw std::runtime_error("Sign of product changed due to floating point precision");
			}
		}
	}
	#endif // if 0
}

void test_rejection()
{
	// u = { 3.236080169677734, 3.814697265625e-06, 1.989475250244141 }
	const geo::point< 3, float > u{{ 0x1.9e37ep+1, 0x1p-18, 0x1.fd4e4p+0 }};
	// v = { -3.236058950424194, 3.814697265625e-06, -2.010524749755859}
	const geo::point< 3, float > v{{ -0x1.9e372p+1, 0x1p-18, -0x1.0158ep+1 }};
	const auto                   w = geo::rejection(u, v - u);

	// w.(u-v) == 0

	if ( std::abs(la::inner(w, u) - la::inner(w, v) ) > 1e-6 )
	{
		std::cerr << w << std::endl;
		std::cerr << la::inner(w, u) << std::endl;
		std::cerr << la::inner(w, v) << std::endl;
		throw std::runtime_error("Bad implementation of vector rejection");
	}
}

int main()
{
	test_dotproduct();
	test_rejection();
}
