#include <cstdlib>
#include <iostream>

#include "gzip/gzip.h"
#include "ravengz.h"
#include "raventxt.h"

int main()
{
	GZip       mygzip;
	const auto err = mygzip.extract(reinterpret_cast< const std::byte* >( raven_gz ), sizeof( raven_gz ) );

	if ( err.ae == archive_error::none )
	{
		const auto& v = err.dataz;

		auto compare = [](std::byte l, unsigned char r){ return static_cast< unsigned char >( l ) == r; };

		if ( !std::equal(v.begin(), v.end(), std::begin(raventxt), std::end(raventxt), compare) )
		{
			std::cerr << "Something's wrong with GZIP\n";
			return EXIT_FAILURE;
		}
	}
	else
	{
		std::cerr << "GZIP Error: " << static_cast< unsigned >( err.de ) << std::endl;
		return EXIT_FAILURE;
	}

	std::cout << "Everything is OK" << std::endl;
	return EXIT_SUCCESS;
}
