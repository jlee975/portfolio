#include <iostream>
#include <random>

#include "math/quaternion.h"
#include "unit/unit.h"

#undef PRINT

template< typename R >
quaternion< R > random_unit_quaternion()
{
	static std::random_device         d;
	static std::default_random_engine g(d() );

	std::uniform_real_distribution< R > u(-1, 1);

	return normalize(quaternion< R >(u(g), u(g), u(g), u(g) ) );
}

template< typename R >
void test_quaternion_inner(
	R e1,
	R e2,
	R e3,
	R e4
)
{
	R worst_slerp_norm_error   = 0;
	R worst_angle_error        = 0;
	R worst_first_angle_error  = 0;
	R worst_second_angle_error = 0;

	for (int trial = 0; trial < 100; ++trial)
	{
		const quaternion< R > q = random_unit_quaternion< R >();
		const quaternion< R > r = random_unit_quaternion< R >();
		#ifdef PRINT
		std::cout << q << ":" << q.norm() << std::endl;
		#endif

		for (int t_ = 0; t_ <= 10; ++t_)
		{
			const R               t = static_cast< R >( t_ ) / 10;
			const quaternion< R > u = slerp(q, r, t);
			#ifdef PRINT
			std::cout << "u = " << u << ":" << u.norm() << std::endl;
			#endif
			worst_slerp_norm_error = std::max(worst_slerp_norm_error, std::abs(1 - u.norm() ) );
			const R theta1 = angle(q, r);
			const R theta2 = angle(q, u);
			const R theta3 = angle(r, u);
			#ifdef PRINT
			std::cout << theta1 << ":" << ( theta2 + theta3 ) << ":" << theta2 << ":" << theta3 << std::endl;
			#endif
			worst_angle_error = std::max(worst_angle_error, std::abs(1 - ( theta2 + theta3 ) / theta1) );

			if ( t > 0 )
			{
				worst_first_angle_error
				    = std::max(worst_first_angle_error, std::abs(1 - theta2 / ( theta1 * t ) ) );
			}

			if ( t < 1 )
			{
				worst_second_angle_error
				    = std::max(worst_second_angle_error, std::abs(1 - theta3 / ( theta1 * ( 1 - t ) ) ) );
			}
		}

		#ifdef PRINT
		std::cout << r << ":" << r.norm() << std::endl;
		std::cout << std::endl;
		#endif
	}

	#if 1
	assert_less_than(worst_slerp_norm_error, e1);
	assert_less_than(worst_angle_error, e2);
	assert_less_than(worst_first_angle_error, e3);
	assert_less_than(worst_second_angle_error, e4);
	#else
	std::cout << "worst slerp norm error " << worst_slerp_norm_error << std::endl;
	std::cout << "worst total angle error " << worst_angle_error << std::endl;
	std::cout << "worst first angle error " << worst_first_angle_error << std::endl;
	std::cout << "worst second angle error " << worst_second_angle_error << std::endl;
	#endif
}

int main()
{
	/// @todo These bounds could probably be tigh
	test_quaternion_inner< float >(1e-3f, 1e-3f, 1e-3f, 1e-3f);
	test_quaternion_inner< double >(1e-7, 1e-7, 1e-7, 1e-7);
}
