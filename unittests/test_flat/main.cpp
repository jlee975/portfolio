#include "unit/unit.h"

#include "math/geometry/primitives/flat.h"
#include "math/linear_algebra/euclidean.h"

void test_flat_2d()
{
	const geo::point< 3, float > p1 = {{ 1, 0, 0 }};
	const geo::point< 3, float > p2 = {{ 0, 1, 0 }};

	const auto n = flat_normal(p1, p2);

	assert_greater_than_equal(la::inner(n, p1), 0);
	assert_greater_than_equal(la::inner(n, p2), 0);
}

void test_flat_3d()
{
	const geo::point< 3, float > p1 = {{ 1, 0, 0 }};
	const geo::point< 3, float > p2 = {{ 0, 1, 0 }};
	const geo::point< 3, float > p3 = {{ 0, 0, 1 }};

	const auto n = flat_normal(p1, p2, p3);

	assert_greater_than_equal(la::inner(n, p1), 0);
	assert_greater_than_equal(la::inner(n, p2), 0);
	assert_greater_than_equal(la::inner(n, p3), 0);
}

void test_floating_point()
{
	const geo::point< 3, float > W0 = {{ 0x1.1e377ap+1, -1, 1 }};
	const geo::point< 3, float > W1 = {{ -0x1.0f1bbcp+2, -1, -3 }};
	const geo::point< 3, float > W2 = {{ -3, 0x1.1e377ap+1, -1 }};

	/* The result of flat_normal is { { 0x1.9e377ap+3, 8, -0x1.4f1bbcp+4 }}
	 *
	 * The true values are  { { 0x1.9e377ap+3, 0x1.000001p+3, -0x1.4f1bbc7fb755p+4 } }
	 *
	 * The second and third arguments are rounded, causing the test below to fail. Ideally, want some way to convert
	 * all 3 values to float from float[2] while being as close to the result in possible. Free to multiply, so, for
	 * example, might divide by 0x1.000001p+3
	 */
	const auto n = flat_normal(W0, W1, W2);

	assert_greater_than(la::inner_sign(n, W0), 0);
	assert_greater_than(la::inner_sign(n, W1), 0);
	assert_greater_than(la::inner_sign(n, W2), 0);
}

int main()
{
	test_flat_2d();
	test_flat_3d();
	test_floating_point();
	return 0;
}
