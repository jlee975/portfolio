#include <ctime>
#include <iomanip>
#include <iostream>

#include "cipher/rijndael.h"
#include "rijndael_test_strings.h"
#include "unit/unit.h"

int main()
{
	for (const auto& i : rijndael_kat_vk_128)
	{
		check_vk< Rijndael >(i);
	}

	for (const auto& i : rijndael_kat_vk_192)
	{
		check_vk< Rijndael >(i);
	}

	for (const auto& i : rijndael_kat_vk_256)
	{
		check_vk< Rijndael >(i);
	}

	for (const auto& i : rijndael_kat_vt_128)
	{
		check_vt< Rijndael, 128 >(i);
	}

	for (const auto& i : rijndael_kat_vt_192)
	{
		check_vt< Rijndael, 192 >(i);
	}

	for (const auto& i : rijndael_kat_vt_256)
	{
		check_vt< Rijndael, 256 >(i);
	}

	// Monte Carlo Tests ---------------------------------------------------
	for (unsigned long i = 0; i < 4; i++)
	{
		check_monte_carlo< Rijndael >();
	}

	std::cout << "Tests ok" << std::endl;
}
