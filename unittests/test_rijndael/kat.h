#ifndef KAT_H
#define KAT_H

#include <array>
#include <random>
#include <vector>

#include "cipher/moo.h"
#include "unit/unit.h"

template< std::size_t K, std::size_t B >
struct kat_all
{
	std::array< unsigned char, K / 8 > key;
	std::array< unsigned char, B / 8 > plaintext;
	std::array< unsigned char, B / 8 > ciphertext;
};

template< std::size_t K, std::size_t B >
struct kat_vk
{
	std::array< unsigned char, K / 8 > key;
	std::array< unsigned char, B / 8 > ciphertext;
};

template< std::size_t B >
struct kat_vt_1
{
	std::array< unsigned char, B / 8 > plaintext;
	std::array< unsigned char, B / 8 > ciphertext;
};

template< typename Cipher, std::size_t K, std::size_t B >
void check_vk(const kat_vk< K, B >& i)
{
	const std::array< unsigned char, B / 8 > plaintext = {};
	std::array< unsigned char, B / 8 >       ciphertext;
	std::array< unsigned char, B / 8 >       recovered;

	Cipher encrypter(i.key.data(), K, B);

	encrypter.setMode(cipher::ECB);
	encrypter.encrypt(plaintext.data(), B / 8, ciphertext.data(), B / 8);
	encrypter.decrypt(i.ciphertext.data(), B / 8, recovered.data(), B / 8);

	assert_equal(plaintext, recovered);
	assert_equal(ciphertext, i.ciphertext);
}

template< typename Cipher, std::size_t K, std::size_t B >
void check_vt(const kat_vt_1< B >& i)
{
	std::array< unsigned char, K / 8 > key = {};

	std::array< unsigned char, B / 8 > plaintext;
	std::array< unsigned char, B / 8 > ciphertext;

	Cipher encrypter(key.data(), K, B);

	encrypter.setMode(cipher::ECB);
	encrypter.encrypt(i.plaintext.data(), B / 8, ciphertext.data(), B / 8);
	encrypter.decrypt(i.ciphertext.data(), B / 8, plaintext.data(), B / 8);

	assert_equal(plaintext, i.plaintext);
	assert_equal(ciphertext, i.ciphertext);
}

class RandomBytes
{
public:
	RandomBytes()
		: gen(rd() ), dis(0, 255)
	{
	}

	unsigned char operator()()
	{
		return dis(gen);
	}
private:
	std::random_device                             rd;
	std::default_random_engine                     gen;
	std::uniform_int_distribution< unsigned char > dis;
};

template< typename Cipher, typename KeyGen = RandomBytes >
void check_monte_carlo(KeyGen keygen = KeyGen() )
{
	RandomBytes textgen;

	for (const auto keylength : Cipher::supported_key_sizes)
	{
		for (const auto blocksize : Cipher::supported_block_sizes)
		{
			const std::size_t test_size = ( blocksize / 8 ) * 4096;

			std::vector< unsigned char > plaintext(test_size);
			std::vector< unsigned char > ciphertext(test_size);
			std::vector< unsigned char > recovered(test_size);

			std::vector< unsigned char > kkey(keylength / 8);
			std::vector< unsigned char > iv(blocksize / 8);

			// Initialize some random data to cipher
			for (std::size_t j = 0; j < test_size; j++)
			{
				plaintext[j] = textgen();
			}

			// Make up a key
			for (std::size_t k = 0; k < keylength / 8; k++)
			{
				kkey[k] = keygen();
			}

			for (std::size_t j = 0; j < blocksize / 8; ++j)
			{
				iv[j] = textgen();
			}

			Cipher encrypter(kkey.data(), keylength, blocksize);

			// ECB
			/// @bug need extra bytes in destination for padding
			/// @bug Test padding
			// ... infer test_size from blocksize and test full multiples and partial multiples
			const std::size_t ecb_size = ( test_size / ( blocksize / 8 ) ) * ( blocksize / 8 );
			encrypter.setMode(cipher::ECB);
			encrypter.encrypt(plaintext.data(), ecb_size, ciphertext.data(), ecb_size);
			encrypter.decrypt(ciphertext.data(), ecb_size, recovered.data(), ecb_size);

			assert_equal(plaintext, recovered);

			// OFB
			encrypter.setMode(cipher::OFB);
			encrypter.encrypt(plaintext.data(), test_size, ciphertext.data(), test_size, iv.data() );
			encrypter.decrypt(ciphertext.data(), test_size, recovered.data(), test_size, iv.data() );

			assert_equal(plaintext, recovered);

			// CBC
			encrypter.setMode(cipher::CBC);
			encrypter.encrypt(plaintext.data(), test_size, ciphertext.data(), test_size, iv.data() );
			encrypter.decrypt(ciphertext.data(), test_size, recovered.data(), test_size, iv.data() );

			assert_equal(plaintext, recovered);

			// CFB
			encrypter.setMode(cipher::CFB);
			encrypter.encrypt(plaintext.data(), test_size, ciphertext.data(), test_size, iv.data() );
			encrypter.decrypt(ciphertext.data(), test_size, recovered.data(), test_size, iv.data() );

			assert_equal(plaintext, recovered);
		}
	}
}

template< typename Cipher, std::size_t K, std::size_t B >
void check_kat_all(const kat_all< K, B >& i)
{
	std::array< unsigned char, B / 8 > pt;
	std::array< unsigned char, B / 8 > ct;

	Cipher encrypter(i.key.data(), K, B);

	encrypter.setMode(cipher::ECB);
	encrypter.encrypt(i.plaintext.data(), B / 8, ct.data(), B / 8);
	encrypter.decrypt(i.ciphertext.data(), B / 8, pt.data(), B / 8);
	assert_equal(i.plaintext, pt);
	assert_equal(i.ciphertext, ct);
}

#endif // KAT_H
