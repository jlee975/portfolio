#include "serialization/stdtypes.h"
#include "json/streams.h"
#include "reflection/reflection.h"

template< typename T >
void check_simple(
	const T&                x,
	const std::string_view& s
)
{
	if ( const auto o = jl::json::to_string(x);o != s )
	{
		throw std::runtime_error("Observed '" + o + "'; Expected '" + std::string(s) + "'");
	}
}

template< typename T >
void check(
	const T&                x,
	const std::string_view& s
)
{
	T y;

	if ( !jl::json::from_string(s, y) || y != x )
	{
		throw std::runtime_error("Round trip failed");
	}

	if ( const auto o = jl::json::to_string(x);o != s )
	{
		throw std::runtime_error("Observed '" + o + "'; Expected '" + std::string(s) + "'");
	}
}

struct A
{
	int first;
	std::string second;

	friend bool operator!=(
		const A& l,
		const A& r
	)
	{
		return l.first != r.first || l.second != r.second;
	}

};

namespace jl::serialization
{
template<>
struct reflection_types< A >
{
	using type = pack< member< &A::first, "first" >, member< &A::second, "second" > >;
};
}

int main()
{
	// booleans
	check(true, "true");
	check(false, "false");

	// zero
	check(0, "0");
	check(0u, "0");
	check(0l, "0");
	check(0ul, "0");
	check(0ll, "0");
	check(0ull, "0");
	check(INT32_C(0), "0");
	check(INT64_C(0), "0");
	check(UINT32_C(0), "0");
	check(UINT64_C(0), "0");
	check(0.0, "0");

	// negative zero
	check(-0, "0");
	check(-0l, "0");
	check(-0ll, "0");
	check(INT32_C(-0), "0");
	check(INT64_C(-0), "0");

	// one
	check(1, "1");
	check(1u, "1");
	check(1l, "1");
	check(1ul, "1");
	check(1ll, "1");
	check(1ull, "1");
	check(UINT32_C(1), "1");
	check(UINT64_C(1), "1");

	check(9, "9");
	check(999999999, "999999999");

	// negative one
	check(-1, "-1");
	check(-1l, "-1");
	check(-1ll, "-1");
	check(UINT32_C(-1), "4294967295");
	check(UINT64_C(-1), "18446744073709551615");

	// 32 bit integers
	check(INT32_C(2147483647), "2147483647");
	check(INT32_C(-2147483648), "-2147483648");
	check(INT32_C(0x7fffffff), "2147483647");
	check(-INT32_C(0x7fffffff), "-2147483647");
	check(UINT32_C(0xffffffff), "4294967295");

	// 64 bit integers
	check(INT64_C(9223372036854775807), "9223372036854775807");
	check(INT64_C(-9223372036854775807) - 1, "-9223372036854775808");
	check(INT64_C(0x7fffffffffffffff), "9223372036854775807");
	check(-INT64_C(0x7fffffffffffffff), "-9223372036854775807");
	check(UINT64_C(0xffffffffffffffff), "18446744073709551615");

	// floating point integers
	check(7.0, "7");

	// general floating poing
	check(3.2, "3.2");

	// negative floating point
	check(-7.0, "-7");

	// floating point with exponents
	check(1e7, "1e+07");

	// check(-0.0, "0")

	// built in arrays
	{
		const std::string a[2] = { "hello", "world" };
		check_simple(a, R"(["hello","world"])");
	}

	{
		const std::string a[2][2] = {{ "hello", "world" }, { "foo", "bar" }};
		check_simple(a, R"([["hello","world"],["foo","bar"]])");
	}

	// standard types
	check(std::string("abcde"), "\"abcde\"");

	// Containers
	check(std::array< int, 5 >{{ 1, 2, 3, 4, 5 }}, "[1,2,3,4,5]");
	check(std::vector{ 6, 7, 8 }, "[6,7,8]");
	check(std::list{ 9, 10, 11 }, "[9,10,11]");
	check(std::deque{ 12, 13, 14, 15 }, "[12,13,14,15]");
	check(std::make_pair(16, 17), "[16,17]");
	check(std::make_tuple(18, 19, 20), "[18,19,20]");
	check(std::map< std::string, int >{{ "a", 21 }, { "b", 22 }}, R"([["a",21],["b",22]])");
	check(std::multimap< std::string, int >{{ "a", 23 }, { "b", 24 }, { "a", 25 }}, R"([["a",23],["a",25],["b",24]])");
	check(std::set{ 27, 26, 28, 26 }, "[26,27,28]");
	check(std::multiset{ 30, 29, 31, 29 }, "[29,29,30,31]");

	// Reflection
	const A a{ 32, "xyzzy" };
	check(a, R"({"first":32,"second":"xyzzy"})");

	A b{ 64, "frobozz" };

	if ( !jl::json::from_string(R"({"second":"xyzzy","first":32})", b) || b != a )
	{
		throw std::logic_error("Did not deserialize object");
	}

	// Pointer types
	A* p = nullptr;
	check_simple(p, "null");
	check_simple(&a, R"({"first":32,"second":"xyzzy"})");
	check_simple(std::optional< A >(a), R"({"first":32,"second":"xyzzy"})");
	check_simple(std::optional< A >(), "null");

	check(std::chrono::January, "1");
	check_simple(std::chrono::January / std::chrono::day(9), R"({"m":1,"d":9})");
	check_simple(std::chrono::year(2023) / std::chrono::month(4), R"({"y":2023,"m":4})");
	check(std::chrono::year(2023) / std::chrono::month(4) / std::chrono::day(5), R"({"y":2023,"m":4,"d":5})");

	std::cout << "OK" << std::endl;
}
