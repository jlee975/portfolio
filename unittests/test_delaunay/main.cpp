#include <array>
#include <cstring>

#include <list>

#include "delaunay/delaunay.hpp"
#include "unit/unit.h"

struct point
{
	double x_;
	double y_;

	double x() const
	{
		return x_;
	}

	double y() const
	{
		return y_;
	}

	void x(double x0)
	{
		x_ = x0;
	}

	void y(double y0)
	{
		y_ = y0;
	}

};

int main()
{
	undirected_graph< point > g;

	delaunay(g);
	/*
	   std::list< char > derp = { 'a', 'b', 'c', 'd' };

	   assert_equal(adler32(empty, 0), 1u);
	   assert_equal(adler32(abcd), 0x03d8018bu);
	   assert_equal(adler32(derp), 0x03d8018bu);
	   std::cout << "ADLER32 tests ok" << std::endl;
	 */
	std::cout << "Tests ok" << std::endl;
}
